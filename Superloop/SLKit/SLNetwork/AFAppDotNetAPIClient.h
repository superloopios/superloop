//
//  AFAppDotNetAPIClient.h
//  Superloop
//
//  Created by WangS on 16/7/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface AFAppDotNetAPIClient : AFHTTPSessionManager

+(AFHTTPSessionManager *)shareClient;

@end
