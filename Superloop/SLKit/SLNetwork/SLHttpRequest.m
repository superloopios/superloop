//
//  SLHttpRequest.m
//  Superloop
//
//  Created by Daniel Zhao on 16/5/19.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLHttpRequest.h"
#import "AFAppDotNetAPIClient.h"
#define HTTP_CONNECTION_TIMEOUT 15

NSString * StringFromHttpMethod(SLHttpMethod method) {
    switch (method) {
        case Get:
            return @"GET";
        case Post:
            return @"POST";
        case Put:
            return @"PUT";
        case Delete:
            return @"DELETE";
    }
}

@interface SLHttpRequest(){
    
}
@end

@implementation SLHttpRequest

#pragma mark Public Methods

+(void)failure:(NSURLSessionDataTask * _Nullable) task error:(NSError * _Nonnull) error failure:(SLHttpRequestFailureBlock)failure{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)task.response;
    NSInteger responseStatusCode = [httpResponse statusCode];
    NSData *data = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *dict = nil;
    if (data) {
        dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    }
    
    if (failure) {
        failure(dict ,responseStatusCode);
    }
}

+ (void)requestWhitUrl:(NSString *)url
               headers:(NSMutableDictionary *)headers
                params:(NSDictionary *)params
                method:(SLHttpMethod)method
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureBlock)failure {
  
    AFHTTPSessionManager *manager=[AFAppDotNetAPIClient shareClient];
    manager.requestSerializer.timeoutInterval=HTTP_CONNECTION_TIMEOUT;
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    if (headers) {
        for (NSString *key in headers) {
            [manager.requestSerializer setValue:headers[key] forHTTPHeaderField:key];
        }
    }else{
        [manager.requestSerializer setValue:nil forHTTPHeaderField:@"Authorization"];
    }
    if(method == Get){
        [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success) {
                success(task, responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self failure:task error:error failure:failure];
           
        }];
    }else if(method == Post){
        [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success) {
                success(task, responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            [self failure:task error:error failure:failure];

        }];
    }else if(method == Put){
        [manager PUT:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success) {
                success(task, responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            [self failure:task error:error failure:failure];

        }];
    }else if(method == Delete){
        [manager DELETE:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (success) {
                success(task, responseObject);
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            [self failure:task error:error failure:failure];
        }];
    }

}



@end
