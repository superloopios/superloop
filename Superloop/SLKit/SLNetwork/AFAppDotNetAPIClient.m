//
//  AFAppDotNetAPIClient.m
//  Superloop
//
//  Created by WangS on 16/7/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "AFAppDotNetAPIClient.h"

@implementation AFAppDotNetAPIClient

+(AFHTTPSessionManager *)shareClient{
    static AFHTTPSessionManager *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[[self class] alloc] initWithBaseURL:nil];
    });
    return instance;
}

@end
 