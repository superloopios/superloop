//
//  SLHttpRequest.h
//  Superloop
//
//  Created by Daniel Zhao on 16/5/19.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SLHttpRequestError;

typedef enum{
    Get = 0,
    Post,
    Put,
    Delete
} SLHttpMethod;

@interface SLHttpRequest : NSObject
typedef void (^SLHttpRequestSuccessBlock)(NSURLSessionDataTask *task, NSDictionary* data);
typedef void (^SLHttpRequestFailureBlock)(NSDictionary *dict, NSInteger statusCode);
typedef void (^SLHttpRequestFailureCodeBlock)(SLHttpRequestError *failure);


+ (void)requestWhitUrl:(NSString *)url
               headers:(NSMutableDictionary *)headers
                params:(NSDictionary *)params
                method:(SLHttpMethod)method
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureBlock)failure;

@end
