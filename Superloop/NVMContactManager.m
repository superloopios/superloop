//
//  NVMContact.m
//  contacts
//
//  Created by PhilCai on 15/7/29.
//  Copyright (c) 2015年 Phil. All rights reserved.
//

#import "NVMContactManager.h"
#import <AddressBookUI/AddressBookUI.h>
#import "SLAPIHelper.h"

@interface NVMContactManager()<UIAlertViewDelegate>

@end

@implementation NVMContactManager

+ (instancetype)manager {
    static NVMContactManager *manager=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!manager) {
            manager = [[NVMContactManager alloc] init];
        }
    });
    return manager;
}
// 添加超级圈到通讯录
-(void) addSuperloop2AddressBook{
    [self doAdd];
}

-(void)doAdd{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        if (![SLUserDefault getContactStatus] && ![SLUserDefault getAllowAddContactStatus]) {
            [SLAPIHelper getContactInfo:nil success:^(NSURLSessionDataTask *task, NSDictionary *data) {
                if (data && [[NSString stringWithFormat:@"%@",data[@"result"]] isEqualToString:@"1"]) {
                    [SLUserDefault setAllowAddContactStatus:YES];   // 允许添加超级圈到通讯录
                    if([self checkAddressBookPermission]){  // 权限允许
                        [self add2AddressBook];
                    }
                }
            } failure:nil];
        }else{
            ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
            if (status == kABAuthorizationStatusDenied){    // 不允许
                if([SLUserDefault getContactStatus]){
                    [SLUserDefault setContactStatus:NO];
                }
            }else if(status == kABAuthorizationStatusAuthorized){   // 允许
                if(![SLUserDefault getContactStatus]){
                    [self add2AddressBook];
                }
            }
        }
    });
}

// 获取通讯录权限
-(BOOL)checkAddressBookPermission{
    //这个变量用于记录授权是否成功，即用户是否允许我们访问通讯录
    BOOL __block allowed = false;
    
    //创建通讯簿的引用
    ABAddressBookRef addBook = ABAddressBookCreateWithOptions(NULL, NULL);
    //创建一个出事信号量为0的信号
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    //申请访问权限
    ABAddressBookRequestAccessWithCompletion(addBook, ^(bool granted, CFErrorRef error) {
        //greanted为YES是表示用户允许，否则为不允许
        allowed = granted;
        //发送一次信号
        dispatch_semaphore_signal(sema);
    });
    //等待信号触发
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    
    return allowed;
}

// 添加到通讯录
-(void)add2AddressBook {
    // 记录是否已添加状态
    [SLUserDefault setContactStatus:YES];
    //声明一个通讯簿的引用
    ABAddressBookRef addBook = ABAddressBookCreateWithOptions(NULL, NULL);
    CFErrorRef error = NULL;
    //获取所有联系人
    NSArray *array = (__bridge NSArray*)ABAddressBookCopyArrayOfAllPeople(addBook);
    // 遍历所有的联系人
    for (id obj in array) {
        ABRecordRef people = (__bridge ABRecordRef)obj;
        NSString *firstName = (__bridge NSString*)ABRecordCopyValue(people, kABPersonFirstNameProperty);
        if ([firstName isEqualToString:@"超级圈"]) {
            return;
        }
    }
    // 保存修改的通讯录对象
    ABAddressBookSave(addBook, NULL);
    
    
    //创建一条新的联系人纪录
    ABRecordRef newRecord = ABPersonCreate();
    
    //为新联系人记录添加属性值
    ABRecordSetValue(newRecord, kABPersonFirstNameProperty, (__bridge CFTypeRef)@"超级圈", &error);
    
    //创建一个多值属性
    ABMutableMultiValueRef multi = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multi, (__bridge CFTypeRef)@"13718039031", kABPersonPhoneMobileLabel, NULL);
    ABMultiValueAddValueAndLabel(multi, (__bridge CFTypeRef)@"13718039810", kABPersonPhoneMobileLabel, NULL);
    ABMultiValueAddValueAndLabel(multi, (__bridge CFTypeRef)@"13718039862", kABPersonPhoneMobileLabel, NULL);
    ABMultiValueAddValueAndLabel(multi, (__bridge CFTypeRef)@"13718350112", kABPersonPhoneMobileLabel, NULL);
    ABMultiValueAddValueAndLabel(multi, (__bridge CFTypeRef)@"13718501812", kABPersonPhoneMobileLabel, NULL);
    
    //将多值属性添加到记录
    ABRecordSetValue(newRecord, kABPersonPhoneProperty, multi, &error);
    CFRelease(multi);
    
    //添加记录到通讯录操作对象
    ABAddressBookAddRecord(addBook, newRecord, &error);
    
    //保存通讯录操作对象
    ABAddressBookSave(addBook, &error);
    CFRelease(newRecord);
    CFRelease(addBook);
}

@end
