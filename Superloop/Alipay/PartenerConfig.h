//
//  PartenerConfig.h
//  Superloop
//
//  Created by WangJiwei on 16/4/21.
//  Copyright © 2016年 Superloop. All rights reserved.
//


//合作身份者id，以2088开头的16位纯数字
#define PartnerID @"2088121848254788"

//收款支付宝账号
#define SellerID  @"zhangyachuan@superloop.com.cn"


//安全校验码（MD5）密钥，以数字和字母组成的32位字符


//商户私钥，自助生成
#define PartnerPrivKey @"MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBANNpeFL3LCuSM26P/9hahz4arhuteEBxz3CJvFigkzwr82uZ6d+AjfrrBPRFu/e1od4RQuB3coliTxK6j52zVsBpFkqGTnpa7s7pnEnD5dfjrMf5LNOqDZDxeK1nBka7qbO9fL2eL3PIP3RJBQSiRrFhPHE/As45Q+m1nIUHJCKHAgMBAAECgYBz9mpVt9a2n58uijYgCqgcXK4b6G8JimPqquJxCsM4QjNnVil+uxkmBTvWcVnFag+7q2Qvk9VznkUZVNPqALEYIkA2u4WxMXcVTSj/Cq619pPf3JWryPMwfvt6AeW6dDA9ij6uU+bQdVpykwMGPTXzFafOJLZTiynUWaMJLiJWQQJBAO6PS38557S43S+o5qpHa9yRLGemkRRJ0SnzOW1esn+QNUFoL9eHtbmJeTVPmxhP5v3dNzLJ18RvE1qnWs/7aO8CQQDi3hkfUY4u2qgeLNdS1XrjWGg0pogZhI7MFEXRmxJFFPPbKeKsU5CfK0jyOghCSv3r4gFqKiX8+5TJB8ZATm/pAkAezlRSfpjERqcRc38t+AEev4R7chAEDbA7+ZOt4u+r92RZKOocrAK0jeMgFkKR9id/GuT44wXA8RMPvtLETtPzAkBgRqiapd2U/K4DwDvQp0/zUv1F+rYfPiXrbV8DyK9EKdQi2e+b+gqRwai/QulmWLTReaPoDeL5LTrC5U241JChAkEAurJGVUf9CmNStC+btRfUbByhuYyhpdP66RgTOmniVUxvWPEguOD9ufHZolEb/BCwyVTMiB00rJYqRgbqikUKng=="
//
