//
//  SLValueUtils.h
//  Superloop
//
//  Created by 朱宏伟 on 16/5/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//   处理后台返回的值为null,或者字典字符串互转，时间戳处理相关

#import <Foundation/Foundation.h>

@interface SLValueUtils : NSObject

+ (NSString *)stringFromObject:(id)obj;
+ (NSNumber *)numberFromObject:(id)obj;

+ (NSString *)timeFormatter:(NSString *)timeStr;

+ (NSString *)timeFormatterWithTimeStamp:(int64_t )timeStamp;

+ (NSString *)timeFormatterForServiceWithTimeStamp:(NSTimeInterval )timeStamp;

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

+ (BOOL)periodTwoHours:(NSString *)created;
@end
