//
//  ALBBSDK.h
//  ALBBSDK
//
//  Created by 友和(lai.zhoul@alibaba-inc.com) on 14-8-2.
//  Copyright (c) 2014年 com.taobao. All rights reserved.
//

#import "TaeWebViewUISettings.h"

//当前ALBB基础SDK版本
#define ALBBSDKVersion @"2.0.0"



/** 初始化成功回调 */
typedef void (^ALBBSDKSuccess)();
/** 初始化失败回调 */
typedef void (^ALBBSDKFailure)(NSError *error);

@interface ALBBSDK : NSObject

#pragma mark SDK 基础API
/** 返回单例 */
+ (instancetype)sharedInstance;

/** ALBBSDK初始化，异步执行 */
- (void)asyncInit;

/**
 ALBBSDK初始化，异步执行
 @param sucessCallback 初始化成功回调
 @param failedCallback 初始化失败回调
 */
- (void)asyncInit:(ALBBSDKSuccess)success failure:(ALBBSDKFailure)failure;

/**
 用于处理其他App的回跳
 @param url
 @return 是否经过了ALBBSDK的处理
 */
- (BOOL)handleOpenURL:(NSURL *)url;

/**
 获取ALBBSDK以及所有插件SDK暴露的service 实例
 @param protocol service的协议
 @return service实例
 */
- (id)getService:(Protocol *)protocol;

/**
 打开SDK Debug日志
 @param isDebugLogOpen
 */
- (void)setDebugLogOpen:(BOOL)isDebugLogOpen;

/**
 *  主动关闭百川设置的crashHandler,建议在应用启动的第一行代码调用
 */
- (void)closeCrashHandler;




#pragma mark 业务设置

#warning move this to trade sdk
/**
 * app标识，和isvcode同义，用来追踪订单
 *@param tag <#tag description#>
 */
- (void)setISVCode:(NSString *)tag;
/**
 *
 *  设置打开detail页面是否优先跳转到手机淘宝
 *  @param isUseTaobaoNativeDetail
 */
- (void)setUseTaobaoNativeDetail:(BOOL)isUseTaobaoNativeDetail;



#define ALBB_ITEM_VIEWTYPE_BAICHUAN @"baichuanH5"
#define ALBB_ITEM_VIEWTYPE_TAOBAO  @"taobaoH5"


/**
 *  设置默认使用淘宝H5还是百川H5页面
 *
 *  @param viewType  值为taobaoH5或baichuanH5
 */
- (void)setViewType:(NSString *) viewType;


- (void)setWebViewUISettings:(TaeWebViewUISettings *)webViewUISettings;

- (TaeWebViewUISettings *)getWebViewUISettings;

#pragma mark 其他设置

/**
 指定当前APP的版本，以便关联相关日志和crash分析信息, 如果不设置默认会取plist里的Bundle version
 @param version <#version description#>
 */
- (void)setAppVersion:(NSString *)version;


/** 
 设置SDK发布渠道,包含渠道类型和渠道名 
 */
- (void)setChannel:(NSString *)type name:(NSString *)name;

/**
 指定身份图片的后缀,例如yw_1222_test.jpg
 @param postFix <#postFix description#>
 */
- (void)setSecGuardImagePostfix:(NSString *)postFix;

#pragma mark key获取
/** 如果引入了高德地图SDK, 返回对应的高德key; 否则返回nil. */
- (NSString *)getGaoDeAPIKey;

/** 如果引入了UmengSDK，TAE会返回对应的友盟的appkey */
- (NSString *)getUMengAPIKey;


/** SDK回调Code定义 */
typedef NS_ENUM (NSUInteger, ALBBSDKCode) {
    /** SDK初始化失败 */
    TAE_INIT_FAILED = 1000,
    /** 初始化下载服务端证书失败 */
    TAE_INIT_SERVER_CER_LOAD_FAILED = 1002,
    /** 服务端证书验证失败 */
    TAE_INIT_SERVER_CER_EVAL_FAIELD = 1003,
    /** 本地证书验证失败 */
    TAE_INIT_LOCAL_CER_EVAL_FAIELD = 1004,
    /** 刷新当前会话失败 */
    TAE_INIT_REFRESH_SESSION_FAIELD = 1005,
    
    /** 登录失败 */
    TAE_LOGIN_FAILED = 2001,
    /** 用户取消了登录 */
    TAE_LOGIN_CANCELLED = 2002,
    
    /** 交易链路失败 */
    TAE_TRADE_PROCESS_FAILED = 3001,
    /** 交易链路中用户取消了操作 */
    TAE_TRADE_PROCESS_CANCELLED = 3002,
    /** 交易链路中发生支付但是支付失败 */
    TAE_TRADE_PROCESS_PAY_FAILED = 3003,
    /** itemId无效 */
    TAE_TRADE_PROCESS_ITEMID_INVALID = 3004
};
@end
