//
//  ALITTID.h
//  ALBBSDK
//
//  Created by madding.lip on 4/13/15.
//  Copyright (c) 2015 alibaba. All rights reserved.
//

#ifndef ALBBSDK_ALBBUIDModel_h
#define ALBBSDK_ALBBUIDModel_h

@interface ALBBUIDModel : NSObject

@property(nonatomic, readwrite) NSString *channelName;
@property(nonatomic, readwrite) NSString *channelType;

+(instancetype) sharedInstance;

-(NSString *) getTTID;

-(NSString *) getChannel;

@end

#endif
