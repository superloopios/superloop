//
//  ALBBURL.h
//  ALBBKernel
//
//  Created by madding on 5/14/15.
//  Copyright (c) 2015 Alipay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALBBURL : NSObject

+ (NSString *)urlEncoded:(NSString *)string;

+ (NSString *)urlDecoded:(NSString *)string;
  
@end
