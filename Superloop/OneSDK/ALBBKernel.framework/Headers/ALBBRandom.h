//
//  ALBBRandom.h
//  ALBBKernel
//
//  Created by madding.lip on 8/11/15.
//  Copyright (c) 2015 alibaba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALBBRandom : NSObject

+ (NSData *)randomSeedKeyData;

+ (NSString *)randomSeedKeyString;

+ (NSString *)randomStringWithLength:(NSInteger)length;
  
@end
