//
//  ALBBRSA.h
//  ALBBDevice
//
//  Created by madding.lip on 8/10/15.
//  Copyright (c) 2015 alibaba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALBBRSA : NSObject

+ (NSString *)encryptRSA:(NSString *)plainText
            publicKeyRef:(SecKeyRef)publicKeyRef;

@end
