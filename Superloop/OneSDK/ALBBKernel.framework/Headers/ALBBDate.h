//
//  ALBBDate.h
//  ALBBKernel
//
//  Created by madding.lip on 8/11/15.
//  Copyright (c) 2015 alibaba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALBBDate : NSObject

+ (NSNumber *)currentTimeStampInMillSeconds;

+ (NSNumber *)currentTimeStampInSeconds;

+ (NSString *)getDefaultForamtDate;
  
@end
