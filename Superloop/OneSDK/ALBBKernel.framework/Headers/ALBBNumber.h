//
//  ALBBNumber.h
//  ALBBKernel
//
//  Created by madding.lip on 8/13/15.
//  Copyright (c) 2015 alibaba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALBBNumber : NSObject

+ (NSNumber *)stringToNumber:(NSString *)value;

+(BOOL)isANumber:(NSString *)string;

@end
