//
//  ALBBHttpClient.h
//  ALBBSystem
//
//  Created by madding.lip on 7/31/15.
//  Copyright (c) 2015 alibaba. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ALBBHttpHandler)(NSURLResponse *response, NSData *data,
                               NSError *error);

@interface ALBBHttpClient : NSObject

/**
*  同步发起http请求，参考doSyncHttpRequest:handler:
*
*  @param request <#request description#>
*  @param error   <#error description#>
*
*  @return <#return value description#>
*/
+ (NSData *)doHttpRequest:(NSMutableURLRequest *)request
                  reponse:(NSURLResponse **)response
                    error:(NSError **)error;

/**
*  同步发起http请求，参考doSyncHttpRequest:handler:
*
*  @param request <#request description#>
*  @param error   <#error description#>
*
*  @return <#return value description#>
*/
+ (void)doHttpRequest:(NSMutableURLRequest *)request
              handler:(ALBBHttpHandler)handler;

/**
*  同步发起http请求;建议不要做同步调用
*
*  @param request <#request description#>
*  @param error   <#error description#>
*
*  @return <#return value description#>
*/
+ (void)doSyncHttpRequest:(NSMutableURLRequest *)request
                  handler:(ALBBHttpHandler)handler;

/**
 *  异步请求
 *
 *  @param request <#request description#>
 *  @param handler <#handler description#>
 */
+ (void)doAsyncHttpRequest:(NSMutableURLRequest *)request
                   handler:(ALBBHttpHandler)handler;

@end
