//
//  ALBBJSON.h
//  ALBBKernel
//
//  Created by madding.lip on 8/11/15.
//  Copyright (c) 2015 Alipay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALBBJSON : NSObject

+ (id)jsonDataToObject:(NSData *)jsonData class:(Class)clazz;

+ (NSDictionary *)jsonDataToDictionary:(NSData *)jsonData;

+ (NSArray *)jsonDataToArray:(NSData *)jsonData;


+ (id)jsonStringToObject:(NSString *)jsonString class:(Class)clazz;

+ (NSDictionary *)jsonStringToDictionary:(NSString *)jsonString;

+ (NSArray *)jsonStringToArray:(NSString *)jsonString;


+ (NSString *)objectToJsonString:(id)object;

+ (NSData *)objectToJsonData:(id)object;

+ (id)dictionaryToClass:(NSDictionary *)dictionary class:(Class)clazz;
@end
