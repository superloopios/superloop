//
//  ALBBThread.h
//  ALBBKernel
//
//  Created by madding on 5/4/15.
//  Copyright (c) 2015 Alipay. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface ALBBThread : NSObject

/**
 * 提交到主线程运行
 */
+ (void)foreground:(dispatch_block_t)block;
/**
 * 提交到后台主线程
 */
+ (void)backgroundMain:(dispatch_block_t)block;

+ (void)backgroundConcurrentTask:(dispatch_block_t)block;

@end
