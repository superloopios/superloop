//
//  ALBBHttpUtil.h
//  ALBBSystem
//
//  Created by madding.lip on 7/31/15.
//  Copyright (c) 2015 alibaba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALBBHttpUtil : NSObject

+ (NSData *)encodeDictionaryToKV:(NSDictionary *)dict;
  
@end
