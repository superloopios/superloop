//
//  SecurityVerification.h
//  SecurityGuardSDKPro
//
//  Created by lifengzhong on 15/8/13.
//  Copyright (c) 2015年 alibaba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SecurityVerification : NSObject


/**
*  防垃圾注册校验接口
*
*  @param context        接口调用的场景
*  @param phone          用户的手机号，可选
*  @param nick           用户的昵称，可选
*  @param email          用户的email，可选
*  @param idNumber       用户的身份证号，可选
*  @param bankCardNumber 用户绑定的银行卡号，可选
*  @param address        用户绑定的地址，可选
*  @param company        用户绑定的公司名称，可选
*  @param info           其他需要的信息，以key-v形式存于info中，可选
*  @param timeout        接口调用超时时间，单位秒，最大不超过22，最小不小于1
*
*  @return 聚安全服务器返回的安全token
*/
+ (NSString*) verifySpamRegistrationSync: (NSString*) context
                                   phone: (NSString*) phone
                                    nick: (NSString*) nick
                                   email: (NSString*) email
                                      id: (NSString*) idNumber
                                bankCard: (NSString*) bankCardNumber
                                 address: (NSString*) address
                                 company: (NSString*) company
                                    info: (NSDictionary*) info
                                 timeout: (NSInteger) timeout;

@end


