//
//  SLValueUtils.m
//  Superloop
//
//  Created by 朱宏伟 on 16/5/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLValueUtils.h"

@implementation SLValueUtils

+ (NSString *)stringFromObject:(id)obj {
    if (obj == nil) {
        return @"";
    }
    if ([obj isKindOfClass:[NSString class]]) {
        return obj;
    } else if([obj isKindOfClass:[NSNull class]]) {
        return @"";
    }else if ([obj isKindOfClass:[NSArray class]] ||
              [obj isKindOfClass:[NSDictionary class]]) {
        return @"";
    } else {
        return [obj description];
    }
}

+ (NSNumber *)numberFromObject:(id)obj
{
    if ([obj isKindOfClass:[NSNumber class]]) {
        return obj;
    }
    if ([obj respondsToSelector:@selector(doubleValue)]) {
        return [NSNumber numberWithDouble:[obj doubleValue]];
    }
    return nil;
}

+ (NSString *)timeFormatter:(NSString *)timeStr {
    if (timeStr.length == 0) {
        return nil;
    }
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.S"];
    NSDate *inputDate = [inputFormatter dateFromString:timeStr];
    if (!inputDate) {
        return nil;
    }
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm"];
    //用[NSDate date]可以获取系统当前时间
    NSString *currentDateStr = [dateFormatter stringFromDate:inputDate];
    return currentDateStr;
}

+ (NSString *)timeFormatterWithTimeStamp:(int64_t )timeStamp {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeStamp];
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //用[NSDate date]可以获取系统当前时间
    NSString *currentDateStr = [dateFormatter stringFromDate:date];
    return currentDateStr;
}

+ (NSString *)timeFormatterForServiceWithTimeStamp:(NSTimeInterval )timeStamp {
    NSString *result;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeStamp];
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    //用[NSDate date]可以获取系统当前时间
    NSString *currentDateStr = [dateFormatter stringFromDate:date];
    NSString *todayStr = [dateFormatter stringFromDate:[NSDate date]];
    if ([todayStr isEqualToString:currentDateStr]) {
        [dateFormatter setDateFormat:@"HH:mm"];
        result = [dateFormatter stringFromDate:date];
    } else if ([[currentDateStr substringToIndex:4] isEqualToString:[todayStr substringToIndex:4]]) {
        [dateFormatter setDateFormat:@"MM-dd HH:mm"];
        result = [dateFormatter stringFromDate:date];
    } else {
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        result = [dateFormatter stringFromDate:date];
    }
    return result;
}

/*!
 * @brief 把格式化的JSON格式的字符串转换成字典
 * @param jsonString JSON格式的字符串
 * @return 返回字典
 */
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

//是否相差2小时
+ (BOOL)periodTwoHours:(NSString *)created{
    // 将时间戳字符串转成日期
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:created.doubleValue / 1000];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *dateStr = [fmt stringFromDate:date];
    NSDate *createdAtDate = [fmt dateFromString:dateStr];
    
    if (createdAtDate.sl_isThisYear) { // 今年
        if (createdAtDate.sl_isYesterday) { // 昨天
            fmt.dateFormat = @"昨天 HH:mm";
            return NO;
        } else if (createdAtDate.sl_isToday) { // 今天
            NSCalendar *calendar = [NSCalendar sl_calendar];
            NSCalendarUnit unit = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
            NSDateComponents *cmps = [calendar components:unit fromDate:createdAtDate toDate:[NSDate date] options:0];
            
            if (cmps.hour*60*60+cmps.minute*60+cmps.second<=7200) { // 时间间隔 <= 2小时
                return YES;
            }else{
                return NO;
            }
            
        }else {
            return NO;
        }
    } else { // 不是今年
        return NO;
    }
    
}

@end
