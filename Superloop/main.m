//
//  main.m
//  Superloop
//
//  Created by WangJiWei on 16/3/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "TFDatePickerView.h"
int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
 