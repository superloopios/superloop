//
//  SLBaseModel.m
//  Superloop
//
//  Created by WangJiWei on 16/3/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLBaseModel.h"

@implementation SLBaseModel
+ (instancetype)modelWithIcon:(NSString *)icon title:(NSString *)title type:(SLSettingCellType)type
{
    SLBaseModel *model = [[self alloc]init];
    model.icon = icon;
    model.title = title;
    model.type = type;
    
    return model;
}

+ (instancetype)modelWithTitle:(NSString *)title type:(SLSettingCellType)type
{
    return [self modelWithIcon:nil title:title type:type];
}
@end