//
//  SLBaseGroup.h
//  Superloop
//
//  Created by WangJiWei on 16/3/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLBaseGroup : NSObject
/** 描述当前组,能够显示多少个cell */
@property (nonatomic ,strong)NSArray *cells;
/** 描述当前组,头部标题是什么*/
@property(nonatomic ,copy)NSString *header;
/** 描述当前组,尾部标题是什么*/
@property(nonatomic ,copy)NSString *footer;

@end
