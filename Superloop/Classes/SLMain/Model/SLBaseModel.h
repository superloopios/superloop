//
//  SLBaseModel.h
//  Superloop
//
//  Created by WangJiWei on 16/3/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    SLSettingCellTypeNone,
    SLSettingCellTypeArrow,
    SLSettingCellTypeLabel,
    SLSettingCellTypeSwitch,
    SLSettingCellTypeTextField
}SLSettingCellType;
@interface SLBaseModel : NSObject
/** 描述SettingCell显示什么样的图片*/
@property(nonatomic ,copy)NSString *icon;
/** 描述SettingCell标题是什么*/
@property(nonatomic ,copy)NSString *title;
/** 描述cell右边视图的类型*/
@property(nonatomic ,assign)SLSettingCellType type;

/** 描述cell右边label的文字*/
@property(nonatomic ,copy)NSString *rightTitle;

/** 描述cell右边textfield的文字*/
@property(nonatomic ,copy)NSString *rightTextFieldText;

/** 显示cell的子标题的*/
@property(nonatomic ,copy)NSString *subTitle;

@property(nonatomic ,assign)BOOL hasActiveView;

/** 描述点击cell之后,应该做什么*/
@property(nonatomic ,copy) void(^action)();
// 快速创建model对象
+ (instancetype)modelWithIcon:(NSString *)icon title:(NSString *)title type:(SLSettingCellType)type;
// 快速创建model对象
+ (instancetype)modelWithTitle:(NSString *)title type:(SLSettingCellType)type;



@end
