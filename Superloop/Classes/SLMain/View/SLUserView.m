//
//  SLUserView.m
//  Superloop
//
//  Created by WangS on 16/7/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLUserView.h"
#import "AppDelegate.h"
#import "SLSearchUser.h"
#import "SLUserTableViewCell.h"
#import "SLPersonCollectionViewCell.h"

@interface SLUserView ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong)UICollectionViewFlowLayout *layout;
@property (nonatomic, strong)UILabel *tiplabel;
@property (nonatomic, strong)UILabel *bolangLabel;
@property (nonatomic, strong)UIImageView *swanImageview;
@end

@implementation SLUserView

static NSString *const indertifers = @"myCollectionview";
#define imageWH  (ScreenW - 10 - 15) / cols
#pragma mark - 懒加载
- (UILabel *)tiplabel{
    if (!_tiplabel) {
        _tiplabel = [[UILabel alloc] init];
        _tiplabel.font = [UIFont systemFontOfSize:17];
        _tiplabel.textAlignment = NSTextAlignmentCenter;
        [self.noNetworkOrDefaultView addSubview:_tiplabel];
    }
    return _tiplabel;
}
- (UILabel *)bolangLabel{
    if (!_bolangLabel) {
        _bolangLabel = [[UILabel alloc] init];
        _bolangLabel.font = [UIFont systemFontOfSize:12];
        _bolangLabel.textAlignment = NSTextAlignmentCenter;
        _bolangLabel.text = @"~~~";
        _bolangLabel.textColor = SLColor(213, 213, 213);
        [self.noNetworkOrDefaultView addSubview:_bolangLabel];
    }
    return _bolangLabel;
}

- (UIImageView *)swanImageview{
    if (!_swanImageview) {
        _swanImageview = [[UIImageView alloc] init];
        _swanImageview.image = [UIImage imageNamed:@"swan"];
        [self.noNetworkOrDefaultView addSubview:_swanImageview];
    }
    return _swanImageview;
}

- (UIView *)noNetworkOrDefaultView{
    if (!_noNetworkOrDefaultView) {
        _noNetworkOrDefaultView = [[UIView alloc]initWithFrame:CGRectMake(0, 0 , self.frame.size.width , self.frame.size.height)];
        _noNetworkOrDefaultView.backgroundColor = SLColor(245, 245, 245);
        [self.userTableView addSubview:_noNetworkOrDefaultView];
    }
    return _noNetworkOrDefaultView;
}
//- (UICollectionView *)collectionView{
//    if (!_collectionView) {
//        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
//        self.layout = layout;
//        layout.minimumLineSpacing = 10;
//        layout.itemSize = CGSizeMake(79, 140);
//        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 27, ScreenW, 147) collectionViewLayout:layout];
//        _collectionView.scrollEnabled = YES;
//        _collectionView.showsHorizontalScrollIndicator = NO;
//        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//        _collectionView.delegate = self;
//        _collectionView.dataSource = self;
//        _collectionView.scrollsToTop=NO;
//        _collectionView.backgroundColor = [UIColor clearColor];
//        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SLPersonCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:indertifers];
//    }
//    return _collectionView;
//}

-(NSMutableArray *)userDataSouresArr{
    if (!_userDataSouresArr) {
        _userDataSouresArr=[NSMutableArray new];
    }
    return _userDataSouresArr;
}
-(NSMutableArray *)suggestionArr{
    if (!_suggestionArr) {
        _suggestionArr=[NSMutableArray new];
    }
    return _suggestionArr;
}
-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        self.backgroundColor=SLColor(244, 244, 244);
       
//        _userTableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStyleGrouped];
//        _userTableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStylePlain];
        _userTableView=[[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_userTableView registerClass:[SLUserTableViewCell class] forCellReuseIdentifier:@"SLUserCell"];
        _userTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _userTableView.delegate = self;
        _userTableView.dataSource = self;
        _userTableView.scrollsToTop=YES;
        _userTableView.rowHeight = 240;
        _userTableView.backgroundColor=SLColor(244, 244, 244);
        
        UIView *footView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 0.1)];
        footView.backgroundColor=SLColor(244, 244, 244);
        _userTableView.tableFooterView=footView;
        [self addSubview:_userTableView];
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    self.userTableView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}
#pragma mark - UItableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.userDataSouresArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    self.userTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    SLUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SLUserCell"];
    SLSearchUser *user = self.userDataSouresArr[indexPath.row];
    cell.didClickFollowBtn = ^(UIButton *btn){
        user.following = btn.selected?@"1":@"0";
        //        [baseView.userTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        NSInteger follower = [user.followers integerValue];
        if (btn.selected) {
            follower++;
        }else{
            follower--;
        }
        user.followers = [NSString stringWithFormat:@"%ld",follower];
        //        NSLog(@"%@",NSStringFromClass([model.followers class]))
        [self.userTableView reloadData];
    };
    cell.user = user;
    cell.selectionStyle=UITableViewCellStyleDefault;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.userTableView deselectRowAtIndexPath:indexPath animated:YES];
    SLSearchUser *user = self.userDataSouresArr[indexPath.row];
    if ([self.delegate respondsToSelector:@selector(SLUserView:didSelectRowAtIndexPathWithModel:)]) {
        [self.delegate SLUserView:self didSelectRowAtIndexPathWithModel:user];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {
        UIView *bottomView=[[UIView alloc] init];
        bottomView.backgroundColor=[UIColor whiteColor];
        UILabel *lab2=[[UILabel alloc] init];
        UIView *sepatorlineView = [[UIView alloc]initWithFrame:CGRectMake(0, 34.5, screen_W, 0.5)];
        lab2.text=@"推荐超人";
        lab2.frame=CGRectMake(9, 0, 200, 20);
        if (self.isHomeVC) {
            lab2.hidden = NO;
            sepatorlineView.hidden = NO;
        }else{
            lab2.hidden = YES;
            sepatorlineView.hidden = YES;
        }
        lab2.textColor = SLBlackTitleColor;
        lab2.font=[UIFont systemFontOfSize:18];
        [bottomView addSubview:lab2];
        
        sepatorlineView.backgroundColor = UIColorFromRGB(0xebddd5);
        [bottomView addSubview:sepatorlineView];
        return bottomView;
    }
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 6)];
    lineView.backgroundColor = SLColor(240, 240, 240);
    return lineView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (self.isHomeVC) {
        return 35;
    }
    return 0.1;
}

//#pragma mark -- UICollectionViewDataSource
////定义展示的UICollectionViewCell的个数
//-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    return self.suggestionArr.count;
//}
////定义每个UICollectionView 的大小
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
//    return CGSizeMake(79, 140);
//    
//}
////每个UICollectionView展s示的内容
//-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    SLPersonCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:indertifers forIndexPath:indexPath];
////    cell.fansModel = self.suggestionArr[indexPath.row];
//    cell.userModel = self.suggestionArr[indexPath.row];
//    return cell;
//    
//}
////每个UICollectionView的点击事件
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
////    if ([self.delegate respondsToSelector:@selector(SLUserView:cellDidSelectedWithIndex:)]) {
////        [self.delegate SLUserView:self cellDidSelectedWithIndex:indexPath.row];
////    }
//    SLSearchUser *user = self.suggestionArr[indexPath.row];
//    if ([self.delegate respondsToSelector:@selector(SLUserView:didSelectRowAtIndexPathWithModel:)]) {
//        [self.delegate SLUserView:self didSelectRowAtIndexPathWithModel:user];
//    }
//}
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
//    return UIEdgeInsetsMake(0, 15, 0, 15);
//}
//其他制空页面
-(void)addNoDatasViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0 , self.frame.size.width , self.frame.size.height)];
    UILabel *tiplabel = [[UILabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    tiplabel.attributedText = AttributedStr;
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    self.noDatasViews = contentView;
    [self addSubview:self.noDatasViews];
    
}
//其他制空页面
-(void)addNetWorkViews:(NSString *)str{
    [self.userTableView addSubview:self.noNetworkOrDefaultView];
    CGSize tipLabelSize = [self.tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    self.tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        self.tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    self.tiplabel.attributedText = AttributedStr;
    
    self.bolangLabel.frame = CGRectMake(self.tiplabel.frame.origin.x+self.tiplabel.frame.size.width,self.tiplabel.frame.origin.y, 50, 10);
    
    self.swanImageview.frame = CGRectMake(ScreenW *0.5-96, self.tiplabel.frame.origin.y + 76, 192, 192);
    
}
-(void)removeViewFromSubviews{
    [self.noNetworkOrDefaultView removeFromSuperview];
}
@end
