//
//  SLTabBarButton.h
//  Superloop
//
//  Created by WangS on 16/6/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLTabBarButton : UIButton


@property (nonatomic,strong)UIImageView *imgView;
@property (nonatomic,strong)UILabel *titLab;

@end
