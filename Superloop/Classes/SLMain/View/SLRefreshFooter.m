//
//  SLRefreshFooter.m
//  Superloop
//
//  Created by WangJiwei on 16/4/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLRefreshFooter.h"

@implementation SLRefreshFooter
// 初始化
- (void)prepare
{
    [super prepare];
    
    self.stateLabel.textColor = SLColor(120, 120, 120);
    //    self.stateLabel.font
    //    [self setTitle:@"abc" forState:MJRefreshStateIdle];
    //    [self setTitle:@"ddd" forState:MJRefreshStateRefreshing];
    
      // 关闭自动显示和隐藏的功能(需要开发者自己去显示和隐藏footer)
    //    self.automaticallyHidden = NO;
}

// 摆放子控件
- (void)placeSubviews
{
    [super placeSubviews];
    
}

@end
