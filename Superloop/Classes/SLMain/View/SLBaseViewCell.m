//
//  SLBaseViewCell.m
//  Superloop
//
//  Created by WangJiWei on 16/3/7.
//  楼上的那位写的太乱了,请小心修改
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "SLBaseViewCell.h"
#import "SLBaseModel.h"
@interface SLBaseViewCell()
{
    UIImageView *_arrow;
    UISwitch *_toogle;
    //    UILabel *_label;
}

@property (nonatomic ,strong)UILabel *subTextLabel;
@property (nonatomic ,strong)UIView *seperatorView;
@end
@implementation SLBaseViewCell

- (UIView *)seperatorView{
    if (!_seperatorView) {
        _seperatorView = [[UIView alloc] init];
        [self.contentView addSubview:_seperatorView];
        [_seperatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.bottom.equalTo(self);
            make.height.equalTo(@0.5);
        }];
    }
    return _seperatorView;
}

- (UILabel *)subTextLabel{
    if (!_subTextLabel) {
        _subTextLabel = [[UILabel alloc] init];
//        _subTextLabel.backgroundColor = [UIColor redColor];
        _subTextLabel.font = [UIFont systemFontOfSize:12];
        _subTextLabel.textColor = [UIColor lightGrayColor];
        
        [self.contentView addSubview:_subTextLabel];
        [_subTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.right.equalTo(self.contentView).offset(-20);
        }];
    }
    return _subTextLabel;
}

- (UIActivityIndicatorView *)activeView{
    if (!_activeView) {
        _activeView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activeView.hidesWhenStopped = YES;
        [self.contentView addSubview:_activeView];
        [_activeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(-20);
            make.centerY.equalTo(self.contentView.mas_centerY);
        }];
    }
    return _activeView;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.textLabel.textColor = UIColorFromRGB(0x2b2124);
        self.textLabel.font = [UIFont systemFontOfSize:16];
        self.textLabel.numberOfLines = 0;
        UIView *bg = [[UIView alloc]init];
        bg.backgroundColor = [UIColor whiteColor];
        self.backgroundView = bg;
        UIView *selBg = [[UIView alloc]init];
        selBg.backgroundColor = SLColor(230, 230, 230);
        self.selectedBackgroundView = selBg;
    }
    return self;
}

- (void)setModel:(SLBaseModel *)model{
    _model = model;
    
    
    self.textLabel.text = model.title;
    
    // 判断icon属性是否有值
    if (model.icon) {
        self.imageView.image = [UIImage imageNamed:model.icon];
    }
    if (model.hasActiveView) {
        self.activeView.hidden = NO;
        [self.activeView startAnimating];
    }else{
        self.activeView.hidden = YES;
    }
    
    // 设置cell的子标题
    if (model.subTitle) {
        self.subTextLabel.text = model.subTitle;
    }
    // 设置cell没有选中背景色
    self.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    // 判断模型的类型
    if (model.type == SLSettingCellTypeArrow) {
        // 设置cell右边视图为右箭头
        [self setUpArrow];
    }else if (model.type == SLSettingCellTypeSwitch){
        [self setUpToggle];
    }else if (model.type == SLSettingCellTypeLabel){
        [self setUpLabel];
    }else if (model.type == SLSettingCellTypeTextField){
        [self setUpTextField];
    }else{
        // 防止cell的循环利用,造成界面混乱
        self.accessoryView = nil;
    }
    self.seperatorView.backgroundColor = SLColor(224, 224, 224);
}

// 添加右箭头
- (void)setUpArrow
{
    if (!_arrow) {
//        _arrow = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"right"]];
          _arrow = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Down-Arrow-outline"]];
    }
    
    self.accessoryView = _arrow;
}
// 添加开关
- (void)setUpToggle
{
    // 设置cell没有选中背景色
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    if (!_toogle) {
        _toogle = [[UISwitch alloc]init];
    }
    // 设置cell右边视图为开关
    self.accessoryView = _toogle;
}
- (void)setUpLabel
{
    if (!_label) {
        _label = [[SLLabel alloc]init];
        
//        _label.frame = CGRectMake(screenW - 180, 0, 150, self.frame.size.height);
        [self.contentView addSubview:_label];

        [_label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.right.equalTo(self.contentView).offset(-15);
            make.left.equalTo(self.textLabel.mas_right).offset(25);
        }];
        
        _label.textAlignment = NSTextAlignmentRight;
        _label.font = [UIFont systemFontOfSize:14];
        _label.textColor = UIColorFromRGB(0xb3907d);
        // 设置label为自动换行
        //_label.numberOfLines = 0;
    }
    
    _label.text = _model.rightTitle;
    
    // 添加右箭头
    [self setUpArrow];
}
- (void)setUpTextField
{
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        CGFloat screenW  = [UIScreen mainScreen].bounds.size.width;
        _textField.frame = CGRectMake(screenW - 180, 0, 150, self.frame.size.height);
        [self.contentView addSubview:_textField];
        
        _textField.textAlignment = NSTextAlignmentRight;
        _textField.font = [UIFont systemFontOfSize:14];
        _textField.textColor = [UIColor blackColor];

      }
    _textField.placeholder = _model.rightTitle;
    _model.rightTextFieldText = _textField.text;
 }


@end
