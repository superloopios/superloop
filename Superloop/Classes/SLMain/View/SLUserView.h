//
//  SLUserView.h
//  Superloop
//
//  Created by WangS on 16/7/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLSearchUser;
@class SLUserView;
@protocol SLUserViewDelegate <NSObject>
@optional
//cell
- (void)SLUserView:(SLUserView *)baseView didSelectRowAtIndexPathWithModel:(SLSearchUser *)model;
//热门超人
//- (void)SLUserView:(SLUserView *)baseView cellDidSelectedWithIndex:(NSInteger)index;
@end

@interface SLUserView : UIView
@property (nonatomic,weak)id<SLUserViewDelegate> delegate;
@property (nonatomic,strong) UITableView *userTableView;
@property (nonatomic,strong) NSMutableArray *userDataSouresArr;
@property (nonatomic,strong) NSMutableArray *suggestionArr;
@property (nonatomic,assign) BOOL isHomeVC;//首页
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong)UIView *noDatasViews;//其他置空视图
@property (nonatomic, strong) UIView *noNetworkOrDefaultView;
-(void)addNoDatasViews:(NSString *)title;
-(void)addNetWorkViews:(NSString *)title;
-(void)removeViewFromSubviews;
@end
