//
//  SLBaseViewCell.h
//  Superloop
//
//  Created by WangJiWei on 16/3/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLBaseModel;
@interface SLBaseViewCell : UITableViewCell
/** <#注释#> */
@property (nonatomic ,strong)SLBaseModel *model;
/** 右边显示的label */
@property (nonatomic ,strong)SLLabel *label;
/** 右边显示的textfield */
@property (nonatomic ,strong)UITextField *textField;
@property (nonatomic ,strong)UIActivityIndicatorView *activeView;
@end
