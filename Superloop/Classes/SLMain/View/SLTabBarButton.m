//
//  SLTabBarButton.m
//  Superloop
//
//  Created by WangS on 16/6/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTabBarButton.h"

@implementation SLTabBarButton


-(instancetype)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        
        
        _imgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 21, 21)];
        [self addSubview:_imgView];
        
        _titLab=[[UILabel alloc] initWithFrame:CGRectMake(0, 21, 21, 16)];
        _titLab.textAlignment=NSTextAlignmentCenter;
        _titLab.textColor=[UIColor lightGrayColor];
        _titLab.font=[UIFont systemFontOfSize:14];
        _titLab.adjustsFontSizeToFitWidth=YES;
        [self addSubview:_titLab];
        
        
    }
    return self;
}




@end
