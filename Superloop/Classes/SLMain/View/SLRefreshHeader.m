//
//  SLRefreshHeader.m
//  Superloop
//
//  Created by WangJiWei on 16/3/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLRefreshHeader.h"
@interface SLRefreshHeader()
@end
@implementation SLRefreshHeader

- (void)prepare
{
    [super prepare];
    self.automaticallyChangeAlpha = YES;
    self.stateLabel.textColor = SLColor(120, 120, 120);
    self.lastUpdatedTimeLabel.hidden = YES;
}
- (void)placeSubviews
{
    [super placeSubviews];
    
}

@end
