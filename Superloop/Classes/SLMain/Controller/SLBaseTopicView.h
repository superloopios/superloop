//
//  SLBaseTopicView.h
//  Superloop
//
//  Created by WangS on 16/7/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLTopicModel;
@class SLBaseTopicView;
@protocol SLBaseTopicViewDelegate <NSObject>
@optional
//热门超人
- (void)SLBaseTopicView:(SLBaseTopicView *)baseView CellDidSelectedWithIndex:(NSInteger)index;
//人物icon
- (void)SLBaseTopicView:(SLBaseTopicView *)baseView didUserIconClicked:(SLTopicModel *)model;
//cell以及评论按钮
- (void)SLBaseTopicView:(SLBaseTopicView *)baseView didSelectRowAtIndexPath:(SLTopicModel *)model;
//超人说登录跳转
- (void)SLBaseTopicView:(SLBaseTopicView *)baseView pushLoginVC:(BOOL)isPushing;
//删除话题
- (void)SLBaseTopicView:(SLBaseTopicView *)baseView deleteTopicAtIndexPath:(NSInteger)index;

@end

@interface SLBaseTopicView : UIView
@property (nonatomic,weak)id<SLBaseTopicViewDelegate> delegate;
@property (nonatomic,strong) UITableView *slTableView;
@property (nonatomic,strong) NSMutableArray *slDataSouresArr;
@property (nonatomic,strong) NSMutableArray *suggestionArr;
@property (nonatomic,strong) SLTopicModel *model;
@property (nonatomic,strong) NSIndexPath *indexsPath;
@property (nonatomic,assign) BOOL isHomeVC;//首页
@property (nonatomic,assign) BOOL isFollowVC;//超人说
@property (nonatomic,strong)UIView *notLoginView;//登录置空视图
@property (nonatomic,strong)UIView *noNetWorkViews;//其他置空视图
-(void)addNotLoginView:(NSString *)title;
-(void)addNetWorkViews:(NSString *)title;
@end
