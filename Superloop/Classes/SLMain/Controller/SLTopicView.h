//
//  SLBaseTopicView.h
//  Superloop
//
//  Created by WangS on 16/7/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLTopicModel;
@class SLTopicView;
@class MWPhotoBrowser;
@protocol SLTopicViewDelegate <NSObject>
@optional
//热门超人
- (void)SLTopicView:(SLTopicView *)baseView CellDidSelectedWithIndex:(NSInteger)index;
//人物icon
- (void)SLTopicView:(SLTopicView *)baseView didUserIconClicked:(SLTopicModel *)model;
//cell以及评论按钮
//- (void)SLTopicView:(SLTopicView *)baseView didSelectRowAtIndexPath:(SLTopicModel *)model;
- (void)SLTopicView:(SLTopicView *)baseView didSelectRowAtIndexPath:(NSInteger)index AndModel:(SLTopicModel *)model;
//超人说登录跳转
- (void)SLTopicView:(SLTopicView *)baseView pushLoginVC:(BOOL)isPushing;
//删除话题
- (void)SLTopicView:(SLTopicView *)baseView deleteTopicAtIndexPath:(NSInteger)index;
//点赞
- (void)SLTopicView:(SLTopicView *)baseView  didPraiseButtonClicked:(UIButton *)button praiseLab:(UILabel *)praiseLab model:(SLTopicModel *)model;
//选择图片
- (void)SLTopicView:(SLTopicView *)baseView didSelectCollectionByMWPhotoBrowser:(MWPhotoBrowser *)browser WithModel:(SLTopicModel *)topicModel;
//滚动时代理
- (void)SLScrollViewWillBeginDragging:(UIScrollView *)scrollView;
//停止滚动
- (void)SLScrollViewEndDragging:(UIScrollView *)scrollView;

- (void)slPhotoBrowser:(NSIndexPath *)indexPath withPhotos:(NSMutableArray *)photoes; //浏览图片的代理
@end

@interface SLTopicView : UIView
@property (nonatomic,weak)id<SLTopicViewDelegate> delegate;
@property (nonatomic,strong) UITableView *slTableView;
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableArray *slDataSouresArr;
@property (nonatomic,strong) NSMutableArray *suggestionArr;
@property (nonatomic,strong) SLTopicModel *model;
@property (nonatomic,strong) NSIndexPath *indexsPath;
//@property (nonatomic,assign) BOOL isHomeVC;//首页
@property (nonatomic,assign) BOOL isFollowVC;//超人说
@property (nonatomic,strong)UIView *notLoginView;//登录置空视图
@property (nonatomic,strong)UIView *noNetWorkViews;//其他置空视图
-(void)addNotLoginView:(NSString *)title;
-(void)addNetWorkViews:(NSString *)title;
-(void)removeViewFromSubviews;
@property (nonatomic, strong) UIView *noNetworkOrDefaultView; 
@end
