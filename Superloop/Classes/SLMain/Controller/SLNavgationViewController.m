//
//  SLNavgationViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLNavgationViewController.h"
#import "SLTopicDetailViewController.h"

#import "SLBlackListViewController.h"
#import "SLExperienceViewController.h"
#import "SLEducationViewController.h"
#import "SLMessageViewController.h"
#import "SLSearchTopicViewController.h"
#import "SLQuestionUsViewController.h"
#import "SLIntroduceViewController.h"
#import "SLCallJudgeViewController.h"
#import "SLComplainViewController.h"
#import "SLWebViewController.h"
#import "YWConversationListViewController+UIViewControllerPreviewing.h"
#import "SLMessageManger.h"
#import "SLFirstViewController.h"

@interface SLNavgationViewController ()<UIGestureRecognizerDelegate>

@property (nonatomic, strong) SLJavascriptBridgeEvent* bridgeEvent;

@property (nonatomic,assign)BOOL isLeft;

@end

@implementation SLNavgationViewController
+ (void)initialize
{
    UINavigationBar *bar = [UINavigationBar appearance];
    // 设置背景色
    UIColor *color = [UIColor whiteColor];
    //    UIColor *color = SLColor(240, 240, 240);
    [bar setBackgroundImage:[UIImage createImageWithColor:color] forBarMetrics:UIBarMetricsDefault];
    // 设置导航栏文字颜色以及大小
    NSDictionary *dict1 = @{
                            NSFontAttributeName : [UIFont systemFontOfSize:18],
                            NSForegroundColorAttributeName : [UIColor blackColor]
                            };
    [bar setTitleTextAttributes:dict1];
    
    //    bar.tintColor = [UIColor whiteColor];
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    NSDictionary *dict2 = @{
                            NSFontAttributeName : [UIFont systemFontOfSize:16],
                            NSForegroundColorAttributeName : [UIColor blackColor]
                            };
    [item setTitleTextAttributes:dict2 forState:UIControlStateNormal];
    [item setTitleTextAttributes:dict2 forState:UIControlStateHighlighted];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
//    id target = self.interactivePopGestureRecognizer.delegate;
//    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] init];
//    [pan addTarget:target action:NSSelectorFromString(@"handleNavigationTransition:")];
//    //3.设置代理
//    pan.delegate = self;
//    // 控制手势什么时候触发
//    pan.delegate = self;
//    // 全屏滑动返回
//    [self.view addGestureRecognizer:pan];
//    //禁止边缘手势
//    self.interactivePopGestureRecognizer.enabled = NO;

    self.interactivePopGestureRecognizer.delegate = self;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return self.childViewControllers.count != 1;
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated{
    if (self.ispopVc == YES) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    return [super popViewControllerAnimated:animated];
}


- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    // 不是根控制器才需要设置
    //    if (self.childViewControllers.count > 0) {
    if ( self.childViewControllers.count != 0) {
        
        UIBarButtonItem *backItem=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_black"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
        
        viewController.hidesBottomBarWhenPushed = YES;
//        viewController.navigationItem.leftBarButtonItem = backItem;
        viewController.navigationItem.backBarButtonItem = backItem;
        
    }
    //    [super pushViewController:viewController animated:animated];
    [super pushViewController:viewController animated:YES];
    
}
// 点击返回按钮,回到上一个界面
- (void)back{
    for (UIViewController *vw in  self.childViewControllers) {
        if ([vw isKindOfClass:[SLTopicDetailViewController class]]){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"tell" object:self];
        }
        //判断如果是登录控制器就返回pop回来
        if ([vw isKindOfClass:[SLFirstViewController class]]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:self];
        }
        //选择城市
        //判断如果是登录控制器就返回pop回来
        if ([vw isKindOfClass:[SLFirstViewController class]]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"s" object:self];
        }
    }
    [self popViewControllerAnimated:YES];
}


@end
