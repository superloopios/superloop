//
//  SLBaseTopicView.m
//  Superloop
//
//  Created by WangS on 16/7/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTopicView.h"
#import "SLTopicModel.h"
#import "SLTopicViewCell.h"
#import "SLTopicDetalModel.h"
#import "SLTopicDetailViewController.h"
#import "SLTopicCollectionViewCell.h"
#import "AppDelegate.h"
#import "SLAPIHelper.h"
#import "SLFirstViewController.h"
#import "SLFans.h"
#import "SLPersonCollectionViewCell.h"
#import <UIImageView+WebCache.h>
#import <UIButton+WebCache.h>
#import <Photos/Photos.h>
#import "MWPhoto.h"

#import "SLTopicListTableViewCell.h"

#define ScreenW [UIScreen mainScreen].bounds.size.width
#define ScreenH [UIScreen mainScreen].bounds.size.height

//#define cellWH  ((ScreenW - 50 - (cols - 1) * margin) / cols)
@interface SLTopicView()<SLTopicViewCellDelegate,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,UICollectionViewDelegateFlowLayout,SLTopicListTableViewCellDelegate>
@property (nonatomic,strong)UIButton *button;  //话题删除按钮
@property (nonatomic,assign)NSIndexPath *indexPath; //话题删除按钮得的索引
@property (nonatomic,strong) SLTopicModel *deleteModel;  //模型
@property (nonatomic,strong)SLTopicCollectionViewCell * cellCollectionViewCell;
@property (nonatomic,assign)NSIndexPath *index;
/** collectionViewLayout */
@property (nonatomic, strong)UICollectionViewFlowLayout *layout;
@property (nonatomic, strong) NSMutableArray *photos; //图片数组

@property (nonatomic, strong)UILabel *tiplabel;

@property (nonatomic, strong)UILabel *bolangLabel;

@property (nonatomic, strong)UIImageView *swanImageview;

@end
@implementation SLTopicView

static NSString *const indertifers = @"myCollectionview";
#define imageWH  (ScreenW - 10 - 15) / cols

#pragma mark - 懒加载
- (UILabel *)tiplabel{
    if (!_tiplabel) {
        _tiplabel = [[UILabel alloc] init];
        _tiplabel.font = [UIFont systemFontOfSize:17];
        _tiplabel.textAlignment = NSTextAlignmentCenter;
        [self.noNetworkOrDefaultView addSubview:_tiplabel];
    }
    return _tiplabel;
}

- (UILabel *)bolangLabel{
    if (!_bolangLabel) {
        _bolangLabel = [[UILabel alloc] init];
        _bolangLabel.font = [UIFont systemFontOfSize:12];
        _bolangLabel.textAlignment = NSTextAlignmentCenter;
        _bolangLabel.text = @"~~~";
        _bolangLabel.textColor = SLColor(213, 213, 213);
        [self.noNetworkOrDefaultView addSubview:_bolangLabel];
    }
    return _bolangLabel;
}

- (UIImageView *)swanImageview{
    if (!_swanImageview) {
        _swanImageview = [[UIImageView alloc] init];
        _swanImageview.image = [UIImage imageNamed:@"swan"];
        [self.noNetworkOrDefaultView addSubview:_swanImageview];
    }
    return _swanImageview;
}

- (UIView *)noNetworkOrDefaultView{
    if (!_noNetworkOrDefaultView) {
        _noNetworkOrDefaultView = [[UIView alloc]initWithFrame:CGRectMake(0, 0 , self.frame.size.width , self.frame.size.height)];
        _noNetworkOrDefaultView.backgroundColor = SLColor(245, 245, 245);
        [self.slTableView addSubview:_noNetworkOrDefaultView];
    }
    return _noNetworkOrDefaultView;
}

-(NSMutableArray *)photos{
    if (!_photos) {
        _photos=[NSMutableArray new];
    }
    return _photos;
}
-(NSMutableArray *)slDataSouresArr{
    if (!_slDataSouresArr) {
        _slDataSouresArr=[NSMutableArray new];
    }
    return _slDataSouresArr;
}
-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self=[super initWithFrame:frame]) {
        self.backgroundColor=SLColor(244, 244, 244);
        _slTableView=[[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_slTableView registerClass:[SLTopicListTableViewCell class] forCellReuseIdentifier:@"TableViewCell"];
        _slTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _slTableView.delegate = self;
        _slTableView.dataSource = self;
        _slTableView.backgroundColor=SLColor(244, 244, 244);
        [self addSubview:_slTableView];
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    self.slTableView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}
#pragma mark - tableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.slDataSouresArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLTopicModel *model = self.slDataSouresArr[indexPath.section];
    return model.cellHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLTopicListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewCell" forIndexPath:indexPath];
    SLTopicModel *model=self.slDataSouresArr[indexPath.section];
    if (!ApplicationDelegate.Basic) {
        model.has_thumbsuped=[NSString stringWithFormat:@"0"];
    }
    cell.topicModel = model;
    cell.indexpath = indexPath;
    cell.delegate = self;
    self.indexsPath = indexPath;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //每次回来之前取消选中
    [self.slTableView deselectRowAtIndexPath:indexPath animated:YES];
    SLTopicModel *model = self.slDataSouresArr[indexPath.section];
    if ([self.delegate respondsToSelector:@selector(SLTopicView:didSelectRowAtIndexPath:AndModel:)]) {
        [self.delegate SLTopicView:self didSelectRowAtIndexPath:indexPath.section AndModel:model];
    }
}

#pragma mark - SLTopicListTableViewCellDelegate
//点图片空白处
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectBlankPhotoWithModel:(SLTopicModel *)topicModel indexPath:(NSIndexPath *)indexpath{
    if ([self.delegate respondsToSelector:@selector(SLTopicView:didSelectRowAtIndexPath:AndModel:)]) {
        [self.delegate SLTopicView:self didSelectRowAtIndexPath:indexpath.section AndModel:topicModel];
    }
}
//点赞
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectPraiseBtnWithModel:(SLTopicModel *)topicModel praiseButtonClicked:(UIButton *)button praiseLabClicked:(UILabel *)praiseLab indexPath:(NSIndexPath *)indexpath{
    if (!ApplicationDelegate.Basic) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
        alert.tag=401;
        [alert show];
        return;
    }
    SLTopicModel *model = self.slDataSouresArr[indexpath.section];
    if ([self.delegate respondsToSelector:@selector(SLTopicView:didPraiseButtonClicked:praiseLab:model:)]) {
        [self.delegate SLTopicView:self didPraiseButtonClicked:button praiseLab:praiseLab model:model];
    }
}
//评论
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectCommentBtnWithModel:(SLTopicModel *)topicModel commentButtonClicked:(UIButton *)button commentLab:(UILabel *)commentLab indexPath:(NSIndexPath *)indexpath{
    //每次回来之前取消选中
    [self.slTableView deselectRowAtIndexPath:indexpath animated:YES];
    SLTopicModel *model = self.slDataSouresArr[indexpath.section];
    if ([self.delegate respondsToSelector:@selector(SLTopicView:didSelectRowAtIndexPath:AndModel:)]) {
        [self.delegate SLTopicView:self didSelectRowAtIndexPath:indexpath.section AndModel:model];
    }
}
//点击图像
- (void)SLTopicListTableViewCell:(id)topicListTableViewCell didSelectNicknameWithModel:(SLTopicModel *)topicModel indexPath:(NSIndexPath *)indexpath{
    SLTopicModel *model = self.slDataSouresArr[indexpath.section];
    if ([self.delegate respondsToSelector:@selector(SLTopicView:didUserIconClicked:)]) {
        [self.delegate SLTopicView:self didUserIconClicked:model];
    }
}
//删除话题
- (void)SLTopicListTableViewCell:(id)topicListTableViewCell didSelectDelegateBtnWithModel:(SLTopicModel *)topicModel deleteButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath{
    SLTopicModel *model = self.slDataSouresArr[indexpath.section];
    if ([[NSString stringWithFormat:@"%@", model.user[@"id"]] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"真的要删除吗?" delegate:self cancelButtonTitle:@"不了" otherButtonTitles:@"好", nil];
        self.button = button;
        self.indexPath = indexpath;
        self.deleteModel = model;
        alert.tag = 400;
        [alert show];
    }

}
//选择图片
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectCollectionByMWPhotoBrowser:(MWPhotoBrowser *)browser WithModel:(SLTopicModel *)topicModel{
    if ([self.delegate respondsToSelector:@selector(SLTopicView:didSelectCollectionByMWPhotoBrowser:WithModel:)]) {
        [self.delegate SLTopicView:self didSelectCollectionByMWPhotoBrowser:browser WithModel:topicModel];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag ==400) {
        if (buttonIndex==1) {
            [self deleteTopic:self.indexPath withMode:self.deleteModel withButton:self.button];
        }
    }else if (alertView.tag ==401) {
        if (buttonIndex==1) {
            [self loginAction];
        }
    }
}
#pragma mark --- 删除话题的点击事件
- (void)deleteTopic:(NSIndexPath *)indexpath withMode:(SLTopicModel *)model withButton:(UIButton *)button{
    [SLAPIHelper deleteTopics:model.id success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        button.selected = !button.selected;
        // 删除成功
        [self.slDataSouresArr removeObjectAtIndex:indexpath.section];
        if ([self.delegate respondsToSelector:@selector(SLTopicView:deleteTopicAtIndexPath:)]) {
            [self.delegate SLTopicView:self deleteTopicAtIndexPath:indexpath.section];
        }
        [self.slTableView reloadData];
    } failure:^(SLHttpRequestError *error) {
        if (error.httpStatusCode==404&&error.slAPICode==12) {
            [HUDManager showWarningWithText:@"删除失败，话题已被删除!"];
        }else{
        [HUDManager showWarningWithText:@"删除失败!"];
        }
    }];
}
// 添加没有登录的视图
-(void)addNotLoginView:(NSString *)title{
    if(!self.notLoginView){
        UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        UILabel *tiplabel = [[UILabel alloc]init];
        CGSize tipLabelSize = [tiplabel boundingRectWithString:title withSize:CGSizeMake(300,20) withFont:15];
        
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 320,tipLabelSize.width, 20);
        if (ScreenH == 480) {
            tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
        }
        contentView.backgroundColor = SLColor(245, 245, 245);
        tiplabel.font = [UIFont systemFontOfSize:15];
        tiplabel.textAlignment = NSTextAlignmentCenter;
        tiplabel.textColor = UIColorFromRGB(0x9e9e9e);
        NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:title];
        
        [AttributedStr addAttribute:NSFontAttributeName
         
                              value:[UIFont systemFontOfSize:15.0]
         
                              range:NSMakeRange(2, 2)];
        
        [AttributedStr addAttribute:NSForegroundColorAttributeName
         
                              value:UIColorFromRGB(0x00aeff)
         
                              range:NSMakeRange(2, 2)];
        
        tiplabel.attributedText = AttributedStr;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = tiplabel.frame;
        
        [btn addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:btn];
        
        [contentView addSubview:tiplabel];
        UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
        bolangLabel.text = @"~~~";
        bolangLabel.font = [UIFont systemFontOfSize:15];
        bolangLabel.textColor = UIColorFromRGB(0x9e9e9e);
        [contentView addSubview:bolangLabel];
        
        
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96,  80, 192, 192)];
        imgView.image = [UIImage imageNamed:@"swan"];
        [contentView addSubview:imgView];
        self.notLoginView  = contentView;
    }
    
    [self addSubview:self.notLoginView];
}
// 登录
-(void)loginAction{
    if ([self.delegate respondsToSelector:@selector(SLTopicView:pushLoginVC:)]) {
        [self.delegate SLTopicView:self pushLoginVC:YES];
    }
}
//其他制空页面
-(void)addNetWorkViews:(NSString *)str{
    [self.slTableView addSubview:self.noNetworkOrDefaultView];
    CGSize tipLabelSize = [self.tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    self.tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        self.tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    self.tiplabel.attributedText = AttributedStr;
    
    self.bolangLabel.frame = CGRectMake(self.tiplabel.frame.origin.x+self.tiplabel.frame.size.width,self.tiplabel.frame.origin.y, 50, 10);
    
    self.swanImageview.frame = CGRectMake(ScreenW *0.5-96, self.tiplabel.frame.origin.y + 76, 192, 192);

}
-(void)removeViewFromSubviews{
    [self.noNetworkOrDefaultView removeFromSuperview];
}
@end
