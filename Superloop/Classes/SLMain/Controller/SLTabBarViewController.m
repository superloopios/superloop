//
//  SLTabBarViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTabBarViewController.h"
#import "SLHomeViewController.h"
#import "SLContactViewController.h"
#import "SLMessageViewController.h"
#import "SLTopicViewController.h"
#import "SLNavgationViewController.h"
#import "YWConversationListViewController+UIViewControllerPreviewing.h"
#import "SLMessageManger.h"
#import "AppDelegate.h"
#import "SLFirstViewController.h"
#import "SLMessageViewController.h"
#import "SLAPIHelper.h"
#import "SLTabBarButton.h"
#import "SLMeViewController.h"

@interface SLTabBarViewController ()<UITabBarControllerDelegate>

@property (nonatomic,strong)NSArray *selectArr;
@property (nonatomic,strong)NSArray *normolArr;
@property (nonatomic,assign)NSInteger selectIndex;
@property (nonatomic,assign)NSInteger currentSelectedIndex;
@property (nonatomic,copy) NSString *messageNum;
//@property (nonatomic,assign)NSInteger previousGetMessageTime; //是否是重复点击
@end

@implementation SLTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 49)];
    backView.backgroundColor = [UIColor whiteColor];
    [self.tabBar insertSubview:backView atIndex:1];
//    self.previousGetMessageTime = [[NSDate date] timeIntervalSince1970];
    self.delegate = self;
    [self setUpAllChildViewController];
    [self setUpAllTabButton];
    [self initUnreadMessageCount];
    //    SLHomeViewController * home = [[SLHomeViewController alloc]init];
    //    [self tabBarController:self shouldSelectViewController:home];
    //登录成功刷新页面
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isLoginSuccessChangeTabbarVC) name:@"isLoginSuccessChangeTabbarVC" object:nil];
    //清除消息点赞数评论数
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMessageNum) name:@"reloadMessageNum" object:nil];
    //退出登录清除消息提示
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ClearMessage) name:@"ClearMessage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ClearMessage) name:@"exitSuperLoop" object:nil];
    
    //getGeTuiMessage
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUnreadMessageCount) name:@"getGeTuiMessage" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PushChangeTabbarVC) name:@"PushChangeTabbarVC" object:nil];
}
-(void)PushChangeTabbarVC{
    self.selectedIndex=3;
    self.currentSelectedIndex = 3;
}
-(void)ClearMessage{
    UIViewController *msgController = self.viewControllers[3];
    msgController.tabBarItem.badgeValue = nil;
    UIViewController *mineController = self.viewControllers[4];
    mineController.tabBarItem.badgeValue = nil;
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"newFansNumber"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    //self.currentSelectedIndex=0;
//    self.selectedIndex=0;
}
-(void)reloadMessageNum{
    [self getUnreadMessageCount];
}

-(void)isLoginSuccessChangeTabbarVC{
    self.selectedIndex=self.selectIndex;
    self.currentSelectedIndex = self.selectIndex;
    [self initUnreadMessageCount];
}
//添加所有的子控制器
- (void)setUpAllChildViewController
{
    SLHomeViewController *home = [[SLHomeViewController alloc] init];
    [self addChildViewController:home];
    
    SLTopicViewController *topic = [[SLTopicViewController alloc] init];
    [self addChildViewController:topic];
    SLContactViewController *contact = [[SLContactViewController alloc] init];
    [self addChildViewController:contact];
    
    /********************添加第三方的会话列表*************************/
    SLMessageViewController *message = [[SLMessageViewController alloc] init];
    [self addChildViewController:message];
    /********************华丽分割线*************************/
    SLMeViewController *mine = [[SLMeViewController alloc] init];
    [self addChildViewController:mine];
    
}
-(void)addChildViewController:(UIViewController *)childController{
    SLNavgationViewController *navVc = [[SLNavgationViewController alloc] initWithRootViewController:childController];
    [super addChildViewController:navVc];
}
- (void)initUnreadMessageCount {
    
    [self getUnreadMessageCount];
    
    [[SLMessageManger sharedInstance].ywIMKit setUnreadCountChangedBlock:^(NSInteger aCount) {
        //客服消息
        YWPerson *person=[[YWPerson alloc]initWithPersonId:@"超人助理" EServiceGroupId:0 baseContext:[SLMessageManger sharedInstance].ywIMKit.IMCore];
        NSInteger count = [[SLMessageManger sharedInstance].ywIMKit getUnreadCountOfPerson:person];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",(long)count] forKey:@"ServiceCount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSString *ServiceMsgStr=[[NSUserDefaults standardUserDefaults] objectForKey:@"ServiceCount"];
        ApplicationDelegate.serviceMsg=[ServiceMsgStr integerValue];
        [self setUnreadMessageCount:aCount];
    }];
}
-(void)getUnreadMessageCount{
    if(!ApplicationDelegate.Basic){
        return;
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"newFansNumber"]) {
        NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"newFansNumber"];
        if ([str integerValue] > 0 ) {
            if ([str integerValue] > 99) {
                str = @"99+";
            }
            UIViewController *MineController = self.viewControllers[4];
            MineController.tabBarItem.badgeValue = str;
        }
    }
    
    NSUInteger unreadCount  = [[SLMessageManger sharedInstance].ywIMKit getTotalUnreadCount];
    [self setUnreadMessageCount:unreadCount];
    //超人助理未读数
    NSString *ServiceMsgStr=[[NSUserDefaults standardUserDefaults] objectForKey:@"ServiceCount"];
    ApplicationDelegate.serviceMsg=[ServiceMsgStr integerValue];
}
-(void)setUnreadMessageCount:(NSInteger)countIM{
    UIViewController *msgController = self.viewControllers[3];
    __weak typeof(msgController) weakController = msgController;
    
//    NSInteger currentTime = [[NSDate date] timeIntervalSince1970];
//    if (currentTime - self.previousGetMessageTime < 3) {
//        return;
//    }else{
//        self.previousGetMessageTime = currentTime;
//    }
    
    [SLAPIHelper getMessageRemindsuccess:^(NSURLSessionDataTask *task, NSDictionary *data) {
        NSArray *dataArr=data[@"result"];
        NSLog(@"%@",data[@"result"]);
        NSString *msgCountStr=@"";
        NSInteger newsCount = 0, msgCount = 0,systemCount = 0;
        if(dataArr){
            for (NSDictionary *dict in dataArr) {
                if ([dict[@"message_type"] isEqual:@3]||[dict[@"message_type"] isEqual:@4]||[dict[@"message_type"] isEqual:@5]||[dict[@"message_type"] isEqual:@6]||[dict[@"message_type"] isEqual:@7]) {
                    systemCount += [dict[@"count"] integerValue];
                }else{
                    newsCount += [dict[@"count"] integerValue];
                }
            }
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",(long)newsCount] forKey:@"NewsCount"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSString *MsgStr=[[NSUserDefaults standardUserDefaults] objectForKey:@"NewsCount"];
            ApplicationDelegate.dynamicMsg=[MsgStr integerValue];
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",(long)systemCount] forKey:@"systemCount"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSString *systemStr=[[NSUserDefaults standardUserDefaults] objectForKey:@"systemCount"];
            ApplicationDelegate.systemMsg=[systemStr integerValue];
            
            if (self.selectedIndex==3) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DynamicMsg" object:MsgStr];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SystemMsg" object:systemStr];
            }
            msgCount = newsCount + countIM +systemCount;
            if(msgCount > 0){
                if (msgCount > 99) {
                    msgCountStr=@"99+";
                } else{
                    msgCountStr = [NSString stringWithFormat:@"%ld",(long)msgCount];
                }
            }
        }
        NSLog(@"msgCountStr-----------%@",msgCountStr);

        if (msgCount>0) {
            weakController.tabBarItem.badgeValue = msgCountStr;
            [UIApplication sharedApplication].applicationIconBadgeNumber = msgCount;
        }else{
            weakController.tabBarItem.badgeValue=nil;
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            
        }
    } failure:^(SLHttpRequestError *failure) {
        
    }];
}

//添加tabBar
- (void)setUpAllTabButton{
   
     [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: UIColorFromRGB(0xef1c00)} forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: UIColorFromRGB(0x2b2421)} forState:UIControlStateNormal];
    // 首页 -> 第0个按钮 -> 对应子控制器 -> self.childViewControllers[0]
    UIViewController *vc0 = self.childViewControllers[0];
    vc0.tabBarItem.title = @"首页";
//    vc0.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_nor"];
    vc0.tabBarItem.image = [UIImage imageNamedWithOriganlMode:@"tabbar_home_nor"];
    vc0.tabBarItem.selectedImage = [UIImage imageNamedWithOriganlMode:@"tabbar_home_sel"];
    vc0.tabBarItem.tag = 0;
    
    // 新联系
    UIViewController *vc1 = self.childViewControllers[1];
    vc1.tabBarItem.title = @"话题";
//    vc1.tabBarItem.image = [UIImage imageNamed:@"tabbar_topic_nor"];
    vc1.tabBarItem.image = [UIImage imageNamedWithOriganlMode:@"tabbar_topic_nor"];
    vc1.tabBarItem.selectedImage = [UIImage imageNamedWithOriganlMode:@"tabbar_topic_sel"];
    vc1.tabBarItem.tag = 1;
    
    // 话题
    UIViewController *vc2 = self.childViewControllers[2];
    vc2.tabBarItem.title = @"通讯录";
//    vc2.tabBarItem.image = [UIImage imageNamed:@"tabbar_contact_nor"];
    vc2.tabBarItem.image = [UIImage imageNamedWithOriganlMode:@"tabbar_contact_nor"];
    vc2.tabBarItem.selectedImage = [UIImage imageNamedWithOriganlMode:@"tabbar_contact_sel"];
    vc2.tabBarItem.tag = 2;
    
    // 消息
    UIViewController *vc3 = self.childViewControllers[3];
    vc3.tabBarItem.title = @"消息";
//    vc3.tabBarItem.image = [UIImage imageNamed:@"tabbar_message_nor"];
    vc3.tabBarItem.image = [UIImage imageNamedWithOriganlMode:@"tabbar_message_nor"];
    vc3.tabBarItem.selectedImage = [UIImage imageNamedWithOriganlMode:@"tabbar_message_sel"];
    vc3.tabBarItem.tag = 3;
    //我的
    UIViewController *vc4 = self.childViewControllers[4];
    vc4.tabBarItem.title = @"我";
//    vc4.tabBarItem.image = [UIImage imageNamed:@"tabbar_mine_nor"];
    vc4.tabBarItem.image = [UIImage imageNamedWithOriganlMode:@"tabbar_mine_nor"];
    vc4.tabBarItem.selectedImage = [UIImage imageNamedWithOriganlMode:@"tabbar_mine_sel"];
    vc4.tabBarItem.tag = 4;
    
    
}


- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    if (ApplicationDelegate.Basic) {
        //登录状态
        if(self.currentSelectedIndex == viewController.tabBarItem.tag){
            if(self.currentSelectedIndex == 0){ // 首页
                [[NSNotificationCenter defaultCenter] postNotificationName:@Home_Refreshing object:self];
            }
            if(self.currentSelectedIndex == 1){ // 话题
                [[NSNotificationCenter defaultCenter] postNotificationName:@Topic_Refreshing object:self];
            }
            if (self.currentSelectedIndex == 4) {//个人页
                [[NSNotificationCenter defaultCenter] postNotificationName:@Mine_Refreshing object:self];
            }
            if (self.currentSelectedIndex == 2) { //通讯录
                [[NSNotificationCenter defaultCenter] postNotificationName:@CallAndContact_Refreshing object:self];
            }
//            [self getUnreadMessageCount];
//            return NO;
            return YES;//test
        }
    }else{
        //未登录状态
        if(self.currentSelectedIndex == viewController.tabBarItem.tag){
            if(self.currentSelectedIndex == 0){ // 首页
                [[NSNotificationCenter defaultCenter] postNotificationName:@Home_Refreshing object:self];
            }
            if(self.currentSelectedIndex == 1){ // 话题
                [[NSNotificationCenter defaultCenter] postNotificationName:@Topic_Refreshing object:self];
            }
        }
    }
    self.currentSelectedIndex = viewController.tabBarItem.tag;
    if (viewController.tabBarItem.tag == 3 ) {
        if (!ApplicationDelegate.Basic) {
            SLFirstViewController *loginView=[[SLFirstViewController alloc] init];
            SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:loginView];
            self.selectIndex=viewController.tabBarItem.tag;
            loginView.isTabbarVC=YES;
            [self presentViewController:nav animated:YES completion:nil];
            return NO;
        }
    }
    if (ApplicationDelegate.Basic) {
        [self getUnreadMessageCount];
    }
    return YES;
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
