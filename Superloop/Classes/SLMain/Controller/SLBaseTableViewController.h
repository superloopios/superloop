//
//  SLBaseTableViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLBaseModel.h"
#import "SLBaseGroup.h"
@interface SLBaseTableViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    NSArray *_dataArray;
}
@property (nonatomic, strong) UITableView *tableView;


@end