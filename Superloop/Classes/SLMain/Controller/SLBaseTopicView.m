//
//  SLBaseTopicView.m
//  Superloop
//
//  Created by WangS on 16/7/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLBaseTopicView.h"
#import "SLTopicModel.h"
#import "SLTopicViewCell.h"
#import "SLPhotoBrowser.h"
#import "SLTopicDetalModel.h"
#import "SLTopicDetailViewController.h"
#import "SLTopicCollectionViewCell.h"
#import "AppDelegate.h"
#import "SLAPIHelper.h"
#import "SLMeTableViewController.h"
#import "SLOtherPersonViewController.h"
#import "SLMoreCategoryViewController.h"
#import "SLFirstViewController.h"
#import "SLHomeViewController.h"
#import "SLRecommend.h"
#import "SLFans.h"
#define ScreenW [UIScreen mainScreen].bounds.size.width
#define ScreenH [UIScreen mainScreen].bounds.size.height
#define cellWH  ((ScreenW - 70 - (cols - 1) * margin) / cols)
@interface SLBaseTopicView()<UICollectionViewDataSource, UICollectionViewDelegate, SLPhotoBrowserDelegate, SLTopicViewCellDelegate,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (nonatomic,strong)UIButton *button;  //话题删除按钮
@property (nonatomic,assign)NSIndexPath *indexPath; //话题删除按钮得的索引
@property (nonatomic,strong) SLTopicModel *deleteModel;  //模型
@end
@implementation SLBaseTopicView
static NSInteger const cols = 3;
static CGFloat const margin = 5;

-(NSMutableArray *)slDataSouresArr{
    if (!_slDataSouresArr) {
        _slDataSouresArr=[NSMutableArray new];
    }
    return _slDataSouresArr;
}
-(NSMutableArray *)suggestionArr{
    if (!_suggestionArr) {
        _suggestionArr=[NSMutableArray new];
    }
    return _suggestionArr;
}
-(instancetype)initWithFrame:(CGRect)frame{

    if (self=[super initWithFrame:frame]) {
        self.backgroundColor=SLColor(244, 244, 244);
         _slTableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStylePlain];
        [_slTableView registerNib:[UINib nibWithNibName:@"SLTopicViewCell" bundle:nil] forCellReuseIdentifier:@"TableViewCell"];
       _slTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _slTableView.delegate = self;
        _slTableView.dataSource = self;
        _slTableView.backgroundColor=SLColor(244, 244, 244);
        [self addSubview:_slTableView];
    }
    return self;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.slDataSouresArr.count;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
        SLTopicModel *model = self.slDataSouresArr[indexPath.section];
        return model.cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    SLTopicViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewCell" forIndexPath:indexPath];
    SLTopicModel *model=self.slDataSouresArr[indexPath.section];
    cell.Delegate = self;
    cell.indexpath = indexPath;
    self.indexsPath = indexPath;
    [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath andModel:model];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //每次回来之前取消选中
    [self.slTableView deselectRowAtIndexPath:indexPath animated:YES];
    SLTopicModel *model = self.slDataSouresArr[indexPath.section];
    
    if ([self.delegate respondsToSelector:@selector(SLBaseTopicView:didSelectRowAtIndexPath:)]) {
        [self.delegate SLBaseTopicView:self didSelectRowAtIndexPath:model];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (self.isHomeVC) {
        if (section == 0) {
            if (self.suggestionArr.count>0) {
                return 205;
            }
            return 20;
        }
    }
    if (section == 0) {
        return 0;
    }
    else {
        return 1;
    }

}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {
        
        UIView *vie=[[UIView alloc] init];
        vie.backgroundColor=[UIColor whiteColor];
        
        UILabel *lab2=[[UILabel alloc] init];
        lab2.text=@"热门话题";
        lab2.textColor=SLColor(55 , 55, 55);
        lab2.font=[UIFont systemFontOfSize:12];
        [vie addSubview:lab2];
        
        if (self.suggestionArr.count>0) {
            
            vie.frame=CGRectMake(0, 0, ScreenW, 205);
            
            UILabel *lab1=[[UILabel alloc] initWithFrame:CGRectMake(10, 8, 200, 12)];
            lab1.text=@"热门超人";
            lab1.textColor=SLColor(55 , 55, 55);
            lab1.font=[UIFont systemFontOfSize:12];
            [vie addSubview:lab1];
            
            UIScrollView *myScrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 29, ScreenW, 147)];
            myScrollView.scrollsToTop=NO;
            myScrollView.backgroundColor=[UIColor whiteColor];
            myScrollView.showsHorizontalScrollIndicator = FALSE;
            myScrollView.bounces = NO;
            myScrollView.contentSize = CGSizeMake(15 + self.suggestionArr.count * (79 + 15), 0);
            [vie addSubview:myScrollView];
            for (int i=0; i<self.suggestionArr.count; i++) {
                SLRecommend *scrollViewCell = [[NSBundle mainBundle] loadNibNamed:@"SLRecommend" owner:nil options:nil].firstObject;
                scrollViewCell.tag =300+i;
                
                UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
                scrollViewCell.userInteractionEnabled=YES;
                [scrollViewCell addGestureRecognizer:tap];
                
                scrollViewCell.fansModel=self.suggestionArr[i];
                scrollViewCell.frame = CGRectMake(15 + i * (79 + 15), 0, 79, 140);
                
                [myScrollView addSubview:scrollViewCell];
            }
            
            UIView *grayView=[[UIView alloc] initWithFrame:CGRectMake(0, 176, ScreenW, 6)];
            grayView.backgroundColor=SLColor(240 , 240, 240);
            [vie addSubview:grayView];
            
            lab2.frame=CGRectMake(10, 189, 200, 13);
            
            UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(vie.frame)-23-6-0.5, ScreenW, 0.5)];
            cutImgView.backgroundColor=SLColor(173, 173, 173);
            [vie addSubview:cutImgView];
            
        }else{
            
            vie.frame=CGRectMake(0, 0, ScreenW, 23);
            lab2.frame=CGRectMake(10, 8, 200, 12);
            
        }
        
        return vie;
    }
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 6)];
    lineView.backgroundColor = SLColor(240, 240, 240);
    return lineView;
}

#pragma mark -- 热门超人的人物点击事件
-(void)tap:(UIGestureRecognizer *)tap{
    SLRecommend *rec=(SLRecommend *)tap.view;
    NSInteger index =(NSInteger)rec.tag - 300;
    if ([self.delegate respondsToSelector:@selector(SLBaseTopicView:CellDidSelectedWithIndex:)]) {
        [self.delegate SLBaseTopicView:self CellDidSelectedWithIndex:index];
    }
}
-(BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    if (self.isHomeVC) {
        [scrollView setContentOffset:CGPointMake(0, 0)];
     }
    return YES;
}
//pragma mark --- 组头不停靠
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.isHomeVC) {
        CGFloat sectionHeaderHeight = 205;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            
            self.slTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
            
            
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            
            self.slTableView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
            
        }
    }
}

#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    SLIndexPathCollection *cellCollectionView = (SLIndexPathCollection *)collectionView;
    SLTopicModel *model = self.slDataSouresArr[cellCollectionView.indexPath.section];
    return (model.imgs.count+2)/3*3;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(cellWH, cellWH);
}
//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SLIndexPathCollection * cellCollectionView  = (SLIndexPathCollection *)collectionView;
    SLTopicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];
    SLTopicModel *Model = self.slDataSouresArr[cellCollectionView.indexPath.section];
    if (Model.imgs.count > 0) {
        if (indexPath.row<Model.imgs.count) {
            NSString *imageUrl = [Model.imgs[indexPath.row][@"url"] stringByAppendingString:@"@!topic"];
            [cell setupCellWithImageURl:imageUrl];
        }else{
            [cell setupCellWithImageURl:nil];
        }
    }
    return cell;
}
//每个UICollectionView的点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SLIndexPathCollection * cellCollectionView  = (SLIndexPathCollection *)collectionView;
    SLTopicModel *Model = self.slDataSouresArr[cellCollectionView.indexPath.section];
        self.model = Model;
    if (indexPath.row<Model.imgs.count) {
        SLPhotoBrowser *broswer = [[SLPhotoBrowser alloc] init];
        broswer.currentImageIndex = indexPath.row;
        broswer.sourceImagesContainerView = cellCollectionView;
        broswer.imageCount = Model.imgs.count;
        broswer.delegate = self;
        [broswer show];
    }else{
        SLTopicViewCell *cell = (SLTopicViewCell *)collectionView.superview.superview;
        NSIndexPath *cellIndexPatch = [self.slTableView indexPathForCell:cell];
        NSLog(@"%ld",cellIndexPatch.row);
        [self tableView:self.slTableView didSelectRowAtIndexPath:cellIndexPatch];
    }
}

#pragma mark - SDPhotoBrowserDelegate

- (NSURL *)photoBrowser:(SLPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index{
    NSString *imageUrl = self.model.imgs[index][@"url"];
    //    NSString *httpUrl = [@"http://img.superloop.com.cn/" stringByAppendingString:imageUrl];
    NSURL *url = [NSURL URLWithString:imageUrl];
    return url;
    
}


#pragma SLTopicViewCellDelegate
//点赞的按钮被点击的代理
- (void)didPraiseButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath
{
    if (!ApplicationDelegate.Basic) {
       UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
        alert.tag=401;
        [alert show];
        return;
    }
    
    button.selected = !button.selected;
    SLTopicModel *model = self.slDataSouresArr[indexpath.section];
    
    __block  NSInteger count = [model.thumbups_cnt integerValue];
    NSDictionary *parameter = @{@"id":model.id};
    if ([model.has_thumbsuped isEqualToString:@"0"]) {
        if (button.selected) {
            button.enabled=NO;
            [SLAPIHelper dGood:parameter method:Post success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                count ++ ;
                [button setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
                
                model.thumbups_cnt=[NSString stringWithFormat:@"%ld",count];
                model.has_thumbsuped=[NSString stringWithFormat:@"%d",1];
                
                [HUDManager showWarningWithText:@"点赞成功!"];
                button.enabled=YES;
                
            } failure:^(SLHttpRequestError *error) {
                button.enabled=YES;
                [HUDManager showWarningWithText:@"点赞失败!"];
            }];
            
        }else
        {
            button.enabled=NO;
            [SLAPIHelper dGood:parameter method:Delete success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                count--;
                [button setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
                
                model.thumbups_cnt=[NSString stringWithFormat:@"%ld",(long)count];
                model.has_thumbsuped=[NSString stringWithFormat:@"%d",0];
                
                [HUDManager showWarningWithText:@"点赞取消!"];
                button.enabled=YES;
            } failure:^(SLHttpRequestError *error) {
                button.enabled=YES;
                [HUDManager showWarningWithText:@"点赞取消失败!"];
            }];
            
        }
        
    }
    
    if([model.has_thumbsuped isEqualToString:@"1"])
    {
        if (!button.selected) {
            //取消单赞
            button.enabled=NO;
            [SLAPIHelper dGood:parameter method:Delete success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                count --;
            
                [button setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
                
                model.thumbups_cnt=[NSString stringWithFormat:@"%ld",(long)count];
                model.has_thumbsuped=[NSString stringWithFormat:@"%d",0];
                
                [HUDManager showWarningWithText:@"点赞取消!"];
                button.enabled=YES;
                
            } failure:^(SLHttpRequestError *error) {
                button.enabled=YES;
                [HUDManager showWarningWithText:@"点赞取消失败!"];
                
            }];
            
        }else
        {
            
            //单赞
            button.enabled=NO;
            
            [SLAPIHelper dGood:parameter method:Post success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                button.enabled=YES;
                
                count++;
                model.thumbups_cnt=[NSString stringWithFormat:@"%ld",(long)count];
                model.has_thumbsuped=[NSString stringWithFormat:@"%d",1];
                [button setTitle:[NSString stringWithFormat:@" %ld", (long)count] forState:UIControlStateNormal];
                
                [HUDManager showWarningWithText:@"点赞成功!"];
            } failure:^(SLHttpRequestError *error) {
                button.enabled=YES;
                [HUDManager showWarningWithText:@"点赞失败!"];
                
            }];
            
        }
        
    }
    
}

#pragma mark - 点击的代理事件
//评论
- (void)didCommentButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath{
    
    //每次回来之前取消选中
    [self.slTableView deselectRowAtIndexPath:indexpath animated:YES];
    SLTopicModel *model = self.slDataSouresArr[indexpath.section];
    
    if ([self.delegate respondsToSelector:@selector(SLBaseTopicView:didSelectRowAtIndexPath:)]) {
        [self.delegate SLBaseTopicView:self didSelectRowAtIndexPath:model];
    }
}
//图像
- (void)didUserIconClicked:(NSIndexPath *)indexpath
{
    SLTopicModel *model = self.slDataSouresArr[indexpath.section];
    if ([self.delegate respondsToSelector:@selector(SLBaseTopicView:didUserIconClicked:)]) {
        [self.delegate SLBaseTopicView:self didUserIconClicked:model];
    }
}
//删除话题
- (void)deleteTopicsButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath{
    SLTopicModel *model = self.slDataSouresArr[indexpath.section];
    if ([[NSString stringWithFormat:@"%@", model.user[@"id"]] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"真的要删除吗?" delegate:self cancelButtonTitle:@"不了" otherButtonTitles:@"好", nil];
        self.button = button;
        self.indexPath = indexpath;
        self.deleteModel = model;
        alert.tag = 400;
        [alert show];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag ==400) {
        if (buttonIndex==1) {
            [self deleteTopic:self.indexPath withMode:self.deleteModel withButton:self.button];
        }
    }else if (alertView.tag ==401) {
        if (buttonIndex==1) {
            [self loginAction];
        }
    }
}
#pragma mark --- 删除话题的点击事件
- (void)deleteTopic:(NSIndexPath *)indexpath withMode:(SLTopicModel *)model withButton:(UIButton *)button{
    
    [SLAPIHelper deleteTopics:model.id success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        button.selected = !button.selected;
        // 删除成功
        [self.slDataSouresArr removeObjectAtIndex:indexpath.section];
        if ([self.delegate respondsToSelector:@selector(SLBaseTopicView:deleteTopicAtIndexPath:)]) {
            [self.delegate SLBaseTopicView:self deleteTopicAtIndexPath:indexpath.section];
        }
        
        [self.slTableView reloadData];
        
    } failure:^(SLHttpRequestError *error) {
        
        [HUDManager showWarningWithText:@"删除失败!"];
    }];
}
// 添加没有登录的视图
-(void)addNotLoginView:(NSString *)title{
    if(!self.notLoginView){
        UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        UILabel *tiplabel = [[UILabel alloc]init];
        CGSize tipLabelSize = [tiplabel boundingRectWithString:title withSize:CGSizeMake(300,20) withFont:17];
        
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
        if (ScreenH == 480) {
            tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
        }
        contentView.backgroundColor = SLColor(245, 245, 245);
        tiplabel.font = [UIFont systemFontOfSize:17];
        tiplabel.textAlignment = NSTextAlignmentCenter;
        
        NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:title];
        
        [AttributedStr addAttribute:NSFontAttributeName
         
                              value:[UIFont systemFontOfSize:17.0]
         
                              range:NSMakeRange(2, 2)];
        //
        [AttributedStr addAttribute:NSForegroundColorAttributeName
         
                              value:SLColor(91, 133, 189)
         
                              range:NSMakeRange(2, 2)];
        
        tiplabel.attributedText = AttributedStr;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = tiplabel.frame;
        
        [btn addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:btn];
        
        [contentView addSubview:tiplabel];
        UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
        bolangLabel.text = @"~~~";
        bolangLabel.font = [UIFont systemFontOfSize:12];
        bolangLabel.textColor = SLColor(213, 213, 213);
        [contentView addSubview:bolangLabel];
        
        
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
        imgView.image = [UIImage imageNamed:@"swan"];
        [contentView addSubview:imgView];
        self.notLoginView  = contentView;
    }
    [self addSubview:self.notLoginView];
}

// 登录
-(void)loginAction{
    if ([self.delegate respondsToSelector:@selector(SLBaseTopicView:pushLoginVC:)]) {
        [self.delegate SLBaseTopicView:self pushLoginVC:YES];
    }
}
//其他制空页面
-(void)addNetWorkViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0 , self.frame.size.width , self.frame.size.height)];
    
    UILabel *tiplabel = [[UILabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    
    tiplabel.attributedText = AttributedStr;
    
    
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    self.noNetWorkViews = contentView;
    [self addSubview:self.noNetWorkViews];
    
}


@end
