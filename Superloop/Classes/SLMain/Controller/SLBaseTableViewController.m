//
//  SLBaseTableViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLBaseTableViewController.h"
#import "SLBaseViewCell.h"

@interface SLBaseTableViewController ()

@end

static NSInteger const ContentInset = -25;
@interface SLBaseTableViewController ()

@end

@implementation SLBaseTableViewController
- (void)loadView
{
    [super loadView];
    UITableView *tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    
    self.tableView  =  tableView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // 调整组之间间距
    self.tableView.sectionHeaderHeight = 10;
    self.tableView.sectionFooterHeight = 0;
    self.tableView.contentInset = UIEdgeInsetsMake(ContentInset, 0, 0, 0);

}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    SLBaseGroup *group = _dataArray[section];
    
    return group.cells.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *ID = @"Cell";
    SLBaseViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[SLBaseViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
    }
   
    SLBaseGroup *group = _dataArray[indexPath.section];
    SLBaseModel *model = group.cells[indexPath.row];
    
    
    cell.model = model;
    
    return cell;
}
// 实现cell的点击时间
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 取消cell选中背景色
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SLBaseGroup *group = _dataArray[indexPath.section];
    SLBaseModel *model = group.cells[indexPath.row];
    if (model.action) {
        model.action();
    }
}

#pragma mark - 返回尾部标题的文字
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    
    SLBaseGroup *group = _dataArray[section];
    
    return group.footer;
}
#pragma mark - 返回头部标题的文字
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    SLBaseGroup *group = _dataArray[section];
    return group.header;
}
- (void)dealloc{
    NSLog(@"dealloc");
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

@end
