//
//  SLContactCell.m
//  Superloop
//
//  Created by WangJiWei on 16/3/14.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "SLContactModel.h"
#import "SLContactCell.h"

@interface SLContactCell()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineImgView;


@end
@implementation SLContactCell
//- (void)setContactModel:(SLContactModel *)contactModel
//{
//    _contactModel = contactModel;
//    self.nameLabel.text = contactModel.nickname;
//    
//}
- (void)awakeFromNib {
    [super awakeFromNib];
    [self.lineImgView setConstant:0.5];
//    self.iconView.layer.cornerRadius = 5;
//    self.iconView.layer.masksToBounds = YES;
    //设置裁剪模式
    self.iconView.contentMode =  UIViewContentModeScaleAspectFill;
    // Initialization code
//    self.backgroundColor = [UIColor grayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)messageClick:(id)sender {
    if ([_Delegate respondsToSelector:@selector(didMessageButtonClicked:indexPath:)]){
        [_Delegate didMessageButtonClicked:sender indexPath:self.indexpath];
    }
}


- (IBAction)callClick:(id)sender {
    if ([_Delegate respondsToSelector:@selector(didCallButtonClicked:indexPath:)]){
        [_Delegate didCallButtonClicked:sender indexPath:self.indexpath];
    }
}


@end
