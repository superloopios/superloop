//
//  SLLastCallViewCell.m
//  Superloop
//
//  Created by WangJiWei on 16/3/27.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import <UIImageView+WebCache.h>
#import "SLLastCallViewCell.h"
#import "SLCallModel.h"
#import "AppDelegate.h"
#import "ColorUtility.h"

@interface SLLastCallViewCell()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentBtnY;
@property (weak, nonatomic) IBOutlet UIButton *commendBtn;
@property (weak, nonatomic) IBOutlet SLLabel *nickName;
//@property (weak, nonatomic) IBOutlet UILabel *position;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIImageView *icon;

@property (weak, nonatomic) IBOutlet UIImageView *dialImg;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineImgViewConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLabWidth;



@end
@implementation SLLastCallViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.lineImgViewConstraint setConstant:0.5
     ];
    self.commendBtn.titleLabel.font=[UIFont systemFontOfSize:13];
    self.commendBtn.layer.cornerRadius = 3;
    self.commendBtn.layer.masksToBounds = YES;
    self.commendBtn.layer.borderWidth = 0.7;
    self.commendBtn.layer.borderColor = [UIColor blackColor].CGColor;
}
- (void)setCallModel:(SLCallModel *)callModel
{
    NSLog(@"%@",callModel);
    _callModel = callModel;
    
    NSString *string1 = [NSString stringWithFormat:@"%@", ApplicationDelegate.userId];
    NSString *string2 = [NSString stringWithFormat:@"%@", callModel.from_user[@"id"]];
    if ([string1 isEqualToString:string2]) {
        _nickName.text = callModel.to_user[@"nickname"];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:callModel.to_user[@"avatar"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
        
    }else
    {
        _nickName.text = callModel.from_user[@"nickname"];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:callModel.from_user[@"avatar"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    }
    self.icon.layer.cornerRadius = 5;
    self.icon.layer.masksToBounds = YES;
    
    if (callModel.startTime && callModel.endTime) {
        NSInteger startTime = [callModel.startTime integerValue];
        NSInteger endTime = [callModel.endTime integerValue];
        NSInteger second = (endTime - startTime) / 1000;
        NSInteger seconds = second % 60;
        NSInteger minutes = (second / 60) % 60;
        NSInteger hours = second / 3600;
        NSString *str;
        
        if (hours == 0) {
            if (minutes == 0){
                str = [NSString stringWithFormat:@"00:00:%.2ld",(long)seconds];
            }else{
                str = [NSString stringWithFormat:@"00:%ld:%ld", (long)minutes, (long)seconds];
            }
        }else{
            str = [NSString stringWithFormat:@"00%ld:%ld:%ld",(long)hours, (long)minutes, (long)seconds];
            
        }
        self.time.text = [[ callModel.created stringByAppendingString:@" | "]  stringByAppendingString: str];
        
        if ([callModel.bye_type isEqualToString:@"0"]) {
            if ([callModel.type isEqualToString:@"0"]) {
                self.commendBtn.hidden=NO;
                _dialImg.image=[UIImage imageNamed:@"meDial"];
                if (callModel.complain_status.integerValue==1) {
                    _commendBtn.enabled=NO;
                    [_commendBtn setTitle:@"已申诉" forState:UIControlStateNormal];
                    [_commendBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                    _commendBtn.tag = 1000;
                    self.commendBtn.layer.borderColor = [UIColor whiteColor].CGColor;
                }
                else{
                    if (callModel.has_rated == 1) {
                        _commendBtn.enabled=NO;
                        
                        if ([callModel.anonymous_mode isEqual:@1]) {
                            [_commendBtn setTitle:@"已匿名评价" forState:UIControlStateNormal];
                        }else{
                            [_commendBtn setTitle:@"已评价" forState:UIControlStateNormal];
                        }
                        [_commendBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                        _commendBtn.tag = 1000;
                        self.commendBtn.layer.borderColor = [UIColor clearColor].CGColor;

                    }else if (callModel.has_rated == 0) {
                        _commendBtn.enabled=YES;
                        if ([callModel.anonymous_mode isEqual:@1]) {
                            [_commendBtn setTitle:@"匿名评价" forState:UIControlStateNormal];
                        }else{
                            [_commendBtn setTitle:@"评价" forState:UIControlStateNormal];
                        }
                        
                        [_commendBtn setTitleColor:[ColorUtility colorWithHexString:@"4f5359"] forState:UIControlStateNormal];
                        _commendBtn.tag = 1000;
                        self.commendBtn.layer.borderColor = [ColorUtility colorWithHexString:@"a1d1b3"].CGColor;
                        
                    }
                }
            }else{
                _dialImg.image=[UIImage imageNamed:@"otherDial"];
                _commendBtn.hidden = YES;
            }
        }
    }else{
        NSString *sec = @" | 00:00:00";
        self.time.text = [NSString stringWithFormat:@"%@%@",callModel.created,sec];
        
        self.time.textColor = [UIColor lightGrayColor];
        //未接通时屏蔽掉评价button
        self.commendBtn.hidden=YES;
        if ([callModel.bye_type isEqualToString:@"11"]) {
            _dialImg.image=[UIImage imageNamed:@"meNoConnect"];
        }
        if ([callModel.bye_type isEqualToString:@"12"]&&[callModel.type isEqualToString:@"0"]) {
            _dialImg.image=[UIImage imageNamed:@"meNoConnect"];
            
        }else if ([callModel.bye_type isEqualToString:@"12"]||[callModel.bye_type isEqualToString:@"10"]) {
            _dialImg.image=[UIImage imageNamed:@"otherNoConnect"];
        }
    }
    if ([callModel.anonymous_mode isEqual:@1] && ![string1 isEqualToString:string2]) {
        _nickName.textColor = UIColorFromRGB(0x848484);
    }else{
        _nickName.textColor = UIColorFromRGB(0x2b2124);
    }
    
}
- (IBAction)judgeOther:(UIButton *)sender {
    if ([_Delegate respondsToSelector:@selector(didJudgeButtonClicked:indexPath:)]) {
        [_Delegate didJudgeButtonClicked:sender indexPath:_indexpath];
    }
    
}
@end
