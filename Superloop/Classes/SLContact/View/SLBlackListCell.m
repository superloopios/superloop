//
//  SLBlackListCell.m
//  Superloop
//
//  Created by WangS on 16/5/19.
//  修改  by  小五  on  16/11/09
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLBlackListCell.h"
#import "SLBlackListModel.h"
#import <UIImageView+WebCache.h>


@interface SLBlackListCell ()
@property (nonatomic, strong) UIImageView     *avatarImg;
@property (nonatomic, strong) UILabel         *nickNameLab;
@property (nonatomic, strong) UILabel         *timeLab;
@property (nonatomic, strong) UIButton        *removeBtn;
@property (nonatomic, strong) UIView          *separatorView;
@end

@implementation SLBlackListCell

- (UIButton *)removeBtn{
    if (!_removeBtn) {
        _removeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_removeBtn setTitle:@"移出" forState:UIControlStateNormal];
        [_removeBtn setTitleColor:SLMainColor forState:UIControlStateNormal];
        _removeBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        _removeBtn.frame = CGRectMake(screen_W-58.5, 27.5, 48, 28);
        [self.contentView addSubview:_removeBtn];
        _removeBtn.layer.borderColor = SLMainColor.CGColor;
        _removeBtn.layer.borderWidth = 0.5;
        _removeBtn.layer.cornerRadius = 5;
        [_removeBtn addTarget:self action:@selector(removeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _removeBtn;
}

- (UIView *)separatorView{
    if (!_separatorView) {
        _separatorView = [[UIView alloc] init];
        _separatorView.frame = CGRectMake(0, 71.5, screen_W, 0.5);
        _separatorView.backgroundColor = SLColor(220, 220, 220);
        [self.contentView addSubview:_separatorView];
    }
    return _separatorView;
}
- (UIImageView *)avatarImg{
    if (!_avatarImg) {
        _avatarImg = [[UIImageView alloc] init];
        _avatarImg.frame = CGRectMake(10, 15, 43, 43);
        [self.contentView addSubview:_avatarImg];
    }
    return _avatarImg;
}
- (UILabel *)nickNameLab{
    if (!_nickNameLab) {
        _nickNameLab = [[UILabel alloc] init];
        _nickNameLab.font = [UIFont systemFontOfSize:17];
        _nickNameLab.textColor = SLBlackTitleColor;
        _nickNameLab.frame = CGRectMake(68, 15, screen_W-68-73, 19);
        [self.contentView addSubview:_nickNameLab];
    }
    return _nickNameLab;
}
- (UILabel *)timeLab{
    if (!_timeLab) {
        _timeLab = [[UILabel alloc] init];
        _timeLab.font = [UIFont systemFontOfSize:13];
        _timeLab.textColor = SLColor(88, 88, 88);
        _timeLab.frame = CGRectMake(68, 42, screen_W-68-73, 13);
        [self.contentView addSubview:_timeLab];
    }
    return _timeLab;
}

-(void)setModel:(SLBlackListModel *)model{
    _model=model;
    [self.avatarImg sd_setImageWithURL:[NSURL URLWithString:model.blocked[@"avatar"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    self.nickNameLab.text=model.blocked[@"nickname"];
    [self.contentView addSubview:self.separatorView];
    self.timeLab.text=model.created;
    [self.contentView addSubview:self.removeBtn];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}
- (void)removeBtnClick:(UIButton *)btn{
    if ([self.deletate respondsToSelector:@selector(blackListCellRemoveButtonClicked:indexPath:)]){
        [self.deletate blackListCellRemoveButtonClicked:btn indexPath:self.indexpath];
    }
}

@end
