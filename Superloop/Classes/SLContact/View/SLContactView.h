//
//  SLContactView.h
//  Superloop
//
//  Created by WangS on 16/8/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLCallModel;
@class SLContactView;
@protocol SLContactViewDelegate <NSObject>
@optional
//cell
- (void)SLContactView:(SLContactView *)baseView didSelectRowAtIndexPathWithModel:(NSString *)idStr;
//didJudgeButtonClicked
- (void)SLContactView:(SLContactView *)baseView didJudgeButtonClicked:(SLCallModel *)model viewController:(UIViewController *)viewController;
//- (void)SLContactView:(SLContactView *)baseView didJudgeButtonClicked:(UIButton *)judgeBtn callModel:(SLCallModel *)model viewController:(UIViewController *)viewController;
@end

@interface SLContactView : UIView

@property (nonatomic,weak)id <SLContactViewDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *contactDataSources;
@property (nonatomic,strong) UITableView *contactTableView;
@property (nonatomic,strong) UIView *haveNoDatasView;
//其他制空页面
-(void)addNetWorkViews:(NSString *)str;
@end
