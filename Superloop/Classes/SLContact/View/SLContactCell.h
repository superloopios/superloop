//
//  SLContactCell.h
//  Superloop
//
//  Created by WangJiWei on 16/3/14.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLContactModel;
@protocol SLContactCellDelegate <NSObject>

- (void)didMessageButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath;
- (void)didCallButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath;

@end
@interface SLContactCell : UITableViewCell
//cell模型
@property (nonatomic, strong) SLContactModel *contactModel;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet SLLabel *nameLabel;
@property (weak, nonatomic) IBOutlet SLLabel *positionLab;

@property (nonatomic, weak) id<SLContactCellDelegate>Delegate;
@property (nonatomic , strong) NSIndexPath *indexpath;
@end
