//
//  SLImageTopTitleBottomBtn.m
//  Superloop
//
//  Created by xiaowu on 16/10/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLImageTopTitleBottomBtn.h"

@implementation SLImageTopTitleBottomBtn

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //根据计算文字的大小
    self.imageView.x = 0;
    self.imageView.y = 10;
    self.imageView.width = self.width;
//    self.imageView.backgroundColor = [UIColor grayColor];
    self.imageView.contentMode = UIViewContentModeCenter;
    self.titleLabel.x = 0;
//    self.titleLabel.backgroundColor = [UIColor grayColor];
    self.titleLabel.y = self.imageView.height+20;
    self.titleLabel.width = self.width;
    self.titleLabel.height = 14;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
//    self.backgroundColor = [UIColor redColor];
    
}

@end
