//
//  SLBlackListCell.h
//  Superloop
//
//  Created by WangS on 16/5/19.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLBlackListModel;

@protocol SLBlackListCellDelegate <NSObject>

- (void)blackListCellRemoveButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexPath;

@end

@interface SLBlackListCell : UITableViewCell

@property(nonatomic,strong)SLBlackListModel *model;
@property (nonatomic , strong) NSIndexPath *indexpath;
@property (nonatomic, weak) id<SLBlackListCellDelegate>deletate;

@end
