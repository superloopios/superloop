//
//  SLLastCallViewCell.h
//  Superloop
//
//  Created by WangJiWei on 16/3/27.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLCallModel;
@class SLLastCallViewCell;
@protocol SLLastCallViewCellDelegate <NSObject>
- (void)didJudgeButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath;
@end
@interface SLLastCallViewCell : UITableViewCell
@property (nonatomic, strong) SLCallModel *callModel;
@property (nonatomic, weak) id<SLLastCallViewCellDelegate> Delegate;
@property (nonatomic , strong) NSIndexPath *indexpath;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentBtnWidth;
@end
