//
//  SLContactView.m
//  Superloop
//
//  Created by WangS on 16/8/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLContactView.h"
#import "SLLastCallViewCell.h"
#import "AppDelegate.h"
#import "SLCallModel.h"
#import "SLCallJudgeViewController.h"
@interface SLContactView ()<UITableViewDelegate,UITableViewDataSource,SLLastCallViewCellDelegate,SLLastCallViewCellDelegate>

@end

@implementation SLContactView
static NSString * const lastCall = @"callCell";

-(NSMutableArray *)contactDataSources{
    if (!_contactDataSources) {
        _contactDataSources=[NSMutableArray new];
    }
    return _contactDataSources;
}
-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        _contactTableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStylePlain];
        _contactTableView.dataSource=self;
        _contactTableView.delegate=self;
        [_contactTableView registerNib:[UINib nibWithNibName:@"SLLastCallViewCell" bundle:nil] forCellReuseIdentifier:lastCall];
        _contactTableView.backgroundColor=SLColor(244, 244, 244);
        
        UIImageView *footImageView = [[UIImageView alloc] init];
        footImageView.frame = CGRectMake(0, 0, ScreenW, 0.5);
        footImageView.backgroundColor=[UIColor lightGrayColor];
        _contactTableView.tableFooterView=footImageView;
        [self addSubview:_contactTableView];
    }
    return self;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.contactDataSources.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLLastCallViewCell *cell = [tableView dequeueReusableCellWithIdentifier:lastCall];
    if (!cell) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"SLLastCallViewCell" owner:nil options:nil] firstObject];
    }
    cell.callModel = self.contactDataSources[indexPath.row];
    cell.indexpath = indexPath;
    cell.Delegate = self;
    return cell;
}
//选择控制器
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //当返回来的时候取消单元格的选中
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SLCallModel *model = self.contactDataSources[indexPath.row];
    
    NSString *string1 = [NSString stringWithFormat:@"%@", ApplicationDelegate.userId];
    NSString *string2 = [NSString stringWithFormat:@"%@", model.from_user[@"id"]];
    NSString *stringID = [[NSString alloc] init];
    if ([string1 isEqualToString:string2]) {
        stringID = model.to_user[@"id"];
    }else{
        stringID = model.from_user[@"id"];
    }
    
    if ([self.delegate respondsToSelector:@selector(SLContactView:didSelectRowAtIndexPathWithModel:)]) {
        [self.delegate SLContactView:self didSelectRowAtIndexPathWithModel:stringID];
    }
    
}

- (void)didJudgeButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath{
    [self.contactTableView deselectRowAtIndexPath:indexpath animated:YES];
    SLCallModel *model = self.contactDataSources[indexpath.row];
    SLCallJudgeViewController *callJudgeView = [[SLCallJudgeViewController alloc] init];
    callJudgeView.hasRatedBlock=^(BOOL hasRated){
        if (hasRated) {
            model.has_rated = 1;
            if ([model.anonymous_mode isEqual:@1]) {
                [button setTitle:@"已匿名评价" forState:UIControlStateNormal];
            }else{
                [button setTitle:@"已评价" forState:UIControlStateNormal];
            }
            
            button.layer.borderColor = [UIColor whiteColor].CGColor;
            [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            button.enabled=NO;
        }
    };
    
    if ([self.delegate respondsToSelector:@selector(SLContactView:didJudgeButtonClicked:viewController:)]) {
        [self.delegate SLContactView:self didJudgeButtonClicked:model viewController:callJudgeView];
    }
    //    if ([self.delegate respondsToSelector:@selector(SLContactView:didSelectRowAtIndexPathWithModel:)]) {
    //        [self.delegate SLContactView:self didJudgeButtonClicked:button callModel:model viewController:callJudgeView];
    //    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 68;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 0.1;
    }
    return 0;
}
//其他制空页面
-(void)addNetWorkViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0 , self.frame.size.width , self.frame.size.height)];
    UILabel *tiplabel = [[UILabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    tiplabel.attributedText = AttributedStr;
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    self.haveNoDatasView = contentView;
    [self.contactTableView addSubview:self.haveNoDatasView];
    
}


@end
