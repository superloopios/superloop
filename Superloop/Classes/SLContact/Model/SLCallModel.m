//
//  SLCallModel.m
//  Superloop
//
//  Created by WangJiwei on 16/4/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCallModel.h"

@implementation SLCallModel
- (NSString *)created
{
    // 将时间戳字符串转成日期
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_created.doubleValue / 1000];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
   // fmt.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    fmt.dateFormat = @"yy/MM/dd HH:mm";

    NSString *dateStr = [fmt stringFromDate:date];
    NSDate *createdAtDate = [fmt dateFromString:dateStr];
    return [fmt stringFromDate:createdAtDate];

//    if (createdAtDate.sl_isThisYear) { // 今年
//        if (createdAtDate.sl_isYesterday) { // 昨天
//            fmt.dateFormat = @"昨天";
//            return [fmt stringFromDate:createdAtDate];
//        } else if (createdAtDate.sl_isToday) { // 今天
//            fmt.dateFormat = @"HH:mm";
//            return [fmt stringFromDate:createdAtDate];
//        } else {
//            fmt.dateFormat = @"MM-dd HH:mm";
//            return [fmt stringFromDate:createdAtDate];
//        }
//    } else { // 不是今年
//        fmt.dateFormat = @"MM-dd HH:mm";
//        return [fmt stringFromDate:createdAtDate];
//    }
}
@end
