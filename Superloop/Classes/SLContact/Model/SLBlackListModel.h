//
//  SLBlackListModel.h
//  Superloop
//
//  Created by WangS on 16/5/19.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLBlackListModel : NSObject

@property(nonatomic,copy)NSString *id;
@property(nonatomic,strong)NSDictionary *blocked;
@property(nonatomic,copy)NSString *created;

@end
