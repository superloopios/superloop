//
//  SLContactModel.h
//  Superloop
//
//  Created by WangJiWei on 16/3/14.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLContactModel : NSObject
//用户的头像
@property (nonatomic, copy) NSString *avatar;
//用户的名字
@property (nonatomic, copy) NSString *nickname;
//职位
@property (nonatomic, copy) NSString *work_position;


@end
