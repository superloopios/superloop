//
//  SLCallModel.h
//  Superloop
//
//  Created by WangJiwei on 16/4/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLCallModel : NSObject
//用户信息
@property (nonatomic, strong) NSDictionary *to_user;
@property (nonatomic, strong) NSDictionary *from_user;
//用户的头像
//@property (nonatomic, copy) NSString *avatar;
//用户的名字
//@property (nonatomic, copy) NSString *nickname;
//通话创造时间
@property (nonatomic, copy) NSString *created;
//开始时间
@property (nonatomic, copy) NSString *startTime;
//结束时间
@property (nonatomic, copy) NSString *endTime;
//是否评价0 / 1
@property (nonatomic, assign) NSInteger has_rated;
//通话id
@property (nonatomic, copy) NSString *id;

//通话类型（0:主叫，1:被叫)
@property (nonatomic, copy) NSString *type;

//挂断类型（0:正常通话，10:呼叫超时，11:主叫挂断，12:被叫拒接
@property (nonatomic, copy) NSString *bye_type;
//申诉
@property (nonatomic, copy) NSString *complain_status;
//是否匿名  1匿名0不匿名
@property (nonatomic, strong) NSNumber *anonymous_mode;


@end
