//
//  SLCallingViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/27.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCallingViewController.h"
#import <Masonry.h>
#import <AFNetworking.h>
#import "AppDelegate.h"
#import <UIImageView+WebCache.h>
#import "SLUserAccountViewController.h"
#import "SLValueUtils.h"
#import "SLAPIHelper.h"
#import "SLImageTopTitleBottomBtn.h"
#import "SLCallJudgeViewController.h"
#import "ColorUtility.h"


@interface SLCallingViewController ()<mainViewDelegate,UIAlertViewDelegate>{
    NSInteger _count;//记录重拨时间
}
@property (nonatomic,strong) UIImageView *bigImage;
@property (nonatomic,strong) UIImageView *littleImage;
@property (nonatomic,strong) UILabel *nickName;
@property (nonatomic,strong) UILabel *ensureLabel;
@property (nonatomic,strong) UIButton *ensureBtn;
@property (nonatomic,strong) UIButton *cancelBtn;
@property (nonatomic,strong) UIButton *anonymousCallBtn;
@property (nonatomic,strong) UIButton *backBtn;
@property (nonatomic,weak) NSTimer *timer;
@property (nonatomic,copy) NSString *log_token;
@property (nonatomic,assign) BOOL isDisconnected;
@property (nonatomic,assign) BOOL isConnected;
@property (nonatomic,copy) NSString *amount;//价格
@property (nonatomic,copy) NSString *duration;//时长
@property (nonatomic,strong) UILabel *amountLab;
@property (nonatomic,strong) UILabel *describeLab;
@property (nonatomic,strong) UILabel *priceLabel;
@property (nonatomic,strong) NSNumber *code;
@property (nonatomic,strong) UIActivityIndicatorView *act;
@property (nonatomic,strong) NSDictionary *dictCall;
@property (nonatomic,assign) BOOL isNoNetWork;
@property (nonatomic,strong) UILabel *attentionLab;
@property (nonatomic,strong) NSDictionary *userData;
@property (nonatomic,strong) UILabel *balanceLab;//余额
@property (nonatomic,strong) UILabel *voucherLab;//代金券
@property (nonatomic,strong) UILabel *balanceMoneyLab;//余额
@property (nonatomic,strong) UILabel *voucherMoneyLab;//代金券
@property (nonatomic,strong) UILabel *amountMoneyLab;//总额
@property (nonatomic,strong) UIView *whiteView;//白色分割线
@property (nonatomic,strong) UILabel *timeLabel;
@property (nonatomic,assign) BOOL isanonymousCall;

@end

@implementation SLCallingViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [MobClick beginLogPageView:@"SLCallingViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLCallingViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor=[UIColor whiteColor];
    self.delegate = self;
    [self setUpUI];
    self.isDisconnected=NO;
    self.isConnected=NO;
    self.isanonymousCall = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disconnected:) name:@"Disconnected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connected:) name:@"Connected" object:nil];
    //申诉成功
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(complainSuccess) name:@"complainSuccess" object:nil];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
- (void)complainSuccess{
    self.ensureBtn.userInteractionEnabled=NO;
    [self.ensureBtn setTitle:[NSString stringWithFormat:@"已申诉"] forState:UIControlStateNormal];
    
}
- (void)disconnected:(NSNotification *)notif{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIButton *destBtn;
        if (self.isanonymousCall) {
            destBtn = self.anonymousCallBtn;
        }else{
            destBtn = self.ensureBtn;
        }
        [destBtn setTitle:@"重拨" forState:UIControlStateNormal];
        
        self.timeLabel.hidden = YES;
        _count = 15;
        destBtn.userInteractionEnabled = YES;   //启动正常的按钮状态
        [destBtn setImage:[UIImage imageNamed:@"repeatCallPhone"] forState:UIControlStateNormal];
        [destBtn setTitleColor:[ColorUtility colorWithHexString:@"a1d1b3"] forState:UIControlStateNormal];
    });
    
    
    
    if (notif.object!=nil) {
        self.isDisconnected=YES;
        
        UIButton *destBtn;
        if (self.isanonymousCall) {
            destBtn = self.anonymousCallBtn;
        }else{
            destBtn = self.ensureBtn;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            destBtn.userInteractionEnabled=YES;
            //            self.ensureBtn.backgroundColor=[UIColor greenColor];
            [_timer invalidate];
        });
        //获取通话详情
        [self getCall_logsData:0];
    }
}

-(void)connected:(NSNotification *)notif{
    if (notif.object!=nil) {
        self.isConnected=YES;
        [self.anonymousCallBtn setTitle:@"去评价" forState:UIControlStateNormal];
    }
}
- (UIImageView *)bigImage{
    if (!_bigImage) {
        _bigImage = [[UIImageView alloc] init];
        _bigImage.userInteractionEnabled = YES;
        _bigImage.contentMode = UIViewContentModeScaleAspectFill;
        [self.view addSubview:_bigImage];
        [_bigImage mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view.mas_top);
            make.left.mas_equalTo(self.view.mas_left);
            make.width.mas_equalTo(ScreenW);
            make.height.mas_equalTo(ScreenH);
        }];
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *effectview = [[UIVisualEffectView alloc] initWithEffect:blur];
        
        effectview.frame = CGRectMake(0, 0, screen_W, screen_H);
        
        [_bigImage addSubview:effectview];
        
        //        UIView *coverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_W, screen_H)];
        //        coverView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.45];
        //        [_bigImage addSubview:coverView];
    }
    return _bigImage;
}
- (UIImageView *)littleImage{
    if (!_littleImage) {
        _littleImage = [[UIImageView alloc] init];
        [_littleImage sd_setImageWithURL:[NSURL URLWithString:self.user[@"avatar"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
        [self.bigImage addSubview:_littleImage];
        CGFloat littleImageHeight;
        CGFloat iconWH;
        
        if ( ScreenH==736) {
            littleImageHeight = 55;
            iconWH = 140*picWScale;
        }else if (ScreenH==667){
            littleImageHeight = 50;
            iconWH = 140;
        }else if (ScreenH==568){
            littleImageHeight = 43;
            iconWH = 140*picWScale;
        }else if (ScreenH==480){
            littleImageHeight = 36;
            iconWH = 140*picWScale;
        }
        _littleImage.layer.cornerRadius = iconWH*0.5;
        [_littleImage mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.bigImage.mas_top).offset(littleImageHeight);
            make.centerX.mas_equalTo(self.view.mas_centerX);
            make.width.height.mas_equalTo(@(iconWH));
        }];
    }
    return _littleImage;
}
- (UILabel *)nickName{
    if (!_nickName) {
        _nickName = [[UILabel alloc] init];
        NSString *strNickName = [SLValueUtils stringFromObject:self.user[@"nickname"]];
        _nickName.text = strNickName;
        _nickName.font = [UIFont boldSystemFontOfSize:23];
        _nickName.textAlignment=NSTextAlignmentCenter;
        [self.bigImage addSubview:_nickName];
        [_nickName mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.littleImage.mas_bottom).offset(15);
            make.left.mas_equalTo(self.bigImage.mas_left);
            make.width.mas_equalTo(ScreenW);
            make.height.mas_equalTo(20);
        }];
    }
    return _nickName;
}
- (UILabel *)priceLabel{
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        NSString *strPrice = [SLValueUtils stringFromObject:self.user[@"price"]];
        //富文本设置
        NSMutableAttributedString *absPrice=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ 元/分钟",strPrice]];
        [absPrice addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:25] range:NSMakeRange(0, strPrice.length)];
        [absPrice addAttribute:NSForegroundColorAttributeName value:SLColor(255, 255, 255) range:NSMakeRange(0, strPrice.length)];
        [absPrice addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(strPrice.length+1, 4)];
        [absPrice addAttribute:NSForegroundColorAttributeName value:SLColor(255, 255, 255) range:NSMakeRange(strPrice.length+1, 4)];
        
        _priceLabel.attributedText = absPrice;
        _priceLabel.textAlignment = NSTextAlignmentCenter;
        [self.bigImage addSubview:_priceLabel];
        [_priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.nickName.mas_bottom).offset(12);
            make.left.mas_equalTo(self.bigImage.mas_left);
            make.width.mas_equalTo(ScreenW);
            //            make.height.mas_equalTo(20);
        }];
    }
    return _priceLabel;
}
- (UILabel *)ensureLabel{
    if (!_ensureLabel) {
        _ensureLabel = [[UILabel alloc] init];
        _ensureLabel.text = @"";
        _ensureLabel.font = [UIFont systemFontOfSize:19];
        _ensureLabel.textAlignment=NSTextAlignmentCenter;
        [self.bigImage addSubview:_ensureLabel];
        [_ensureLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.priceLabel.mas_bottom).offset(10);
            make.left.mas_equalTo(self.bigImage.mas_left);
            make.width.mas_equalTo(ScreenW);
            make.height.mas_equalTo(15);
        }];
    }
    return _ensureLabel;
}
- (UIView *)whiteView{
    if (!_whiteView) {
        _whiteView = [[UIView alloc] init];
        _whiteView.backgroundColor = SLColor(255, 255, 255);
        [self.bigImage addSubview:_whiteView];
    }
    return _whiteView;
}
- (UILabel *)amountLab{
    if (!_amountLab) {
        _amountLab=[[UILabel alloc] init];
        _amountLab.text = @"共消费:";
        _amountLab.font = [UIFont systemFontOfSize:17];
        _amountLab.textColor = UIColorFromRGB(0xffffff);
        _amountLab.textAlignment=NSTextAlignmentRight;
        [self.bigImage addSubview:_amountLab];
    }
    return _amountLab;
}
- (UILabel *)balanceLab{
    if (!_balanceLab) {
        _balanceLab=[[UILabel alloc] init];
        _balanceLab.text = @"余额支付:";
        _balanceLab.font = [UIFont systemFontOfSize:17];
        _balanceLab.textColor = UIColorFromRGB(0xffffff);
        _balanceLab.textAlignment=NSTextAlignmentRight;
        [self.bigImage addSubview:_balanceLab];
    }
    return _balanceLab;
}
- (UILabel *)voucherLab{
    if (!_voucherLab) {
        _voucherLab=[[UILabel alloc] init];
        _voucherLab.text = @"现金券:";
        _voucherLab.font = [UIFont systemFontOfSize:17];
        _voucherLab.textColor = UIColorFromRGB(0xffffff);
        _voucherLab.textAlignment=NSTextAlignmentRight;
        [self.bigImage addSubview:_voucherLab];
    }
    return _voucherLab;
}
- (UILabel *)amountMoneyLab{
    if (!_amountMoneyLab) {
        _amountMoneyLab=[[UILabel alloc] init];
        _amountMoneyLab.textAlignment=NSTextAlignmentRight;
        [self.bigImage addSubview:_amountMoneyLab];
    }
    return _amountMoneyLab;
}
- (UILabel *)balanceMoneyLab{
    if (!_balanceMoneyLab) {
        _balanceMoneyLab=[[UILabel alloc] init];
        _balanceMoneyLab.textAlignment=NSTextAlignmentRight;
        [self.bigImage addSubview:_balanceMoneyLab];
    }
    return _balanceMoneyLab;
}
- (UILabel *)voucherMoneyLab{
    if (!_voucherMoneyLab) {
        _voucherMoneyLab=[[UILabel alloc] init];
        _voucherMoneyLab.textAlignment=NSTextAlignmentRight;
        [self.bigImage addSubview:_voucherMoneyLab];
    }
    return _voucherMoneyLab;
}
- (UILabel *)describeLab{
    if (!_describeLab) {
        _describeLab=[[UILabel alloc] init];
        _describeLab.numberOfLines=0;
        
        _describeLab.font=[UIFont systemFontOfSize:12];
        _describeLab.textAlignment=NSTextAlignmentLeft;
        [self.bigImage addSubview:_describeLab];
//        CGFloat describeLabHeight;
        
        [_describeLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.ensureLabel.mas_bottom).offset(10);
            make.left.mas_equalTo(self.bigImage.mas_left).offset(40);
            make.width.mas_equalTo(ScreenW-80);
        }];
    }
    return _describeLab;
}
- (UILabel *)attentionLab{
    if (!_attentionLab) {
        _attentionLab=[[UILabel alloc] init];
        //富文本设置
        NSMutableAttributedString *absAttention=[[NSMutableAttributedString alloc] initWithString:@"您将收到超级圈的回拨电话，为保证通话质量请您确保手机信号良好（ 特殊情况下回拨电话会显示为随机号码，请放心接听。）"];
        [absAttention addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, 30)];
        [absAttention addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, 30)];
        [absAttention addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(30, 27)];
        [absAttention addAttribute:NSForegroundColorAttributeName value:SLColor(252, 96, 82) range:NSMakeRange(30, 27)];
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 6;
        [paragraphStyle setParagraphSpacing:2];
        [absAttention addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, absAttention.length)];

        _attentionLab.attributedText=absAttention;
        
        [self.bigImage addSubview:_attentionLab];
//        [_attentionLab sizeToFit];
        [_attentionLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.priceLabel.mas_bottom).offset(36);
            make.left.mas_equalTo(self.bigImage.mas_left).offset(15);
            make.width.mas_equalTo(ScreenW-30);
        }];
        
    }
    return _attentionLab;
}

- (UIButton *)cancelBtn{
    if (!_cancelBtn) {
        _cancelBtn = [SLImageTopTitleBottomBtn buttonWithType:UIButtonTypeCustom];
        [_cancelBtn setImage:[UIImage imageNamed:@"closePhone"] forState:UIControlStateNormal];
        
        [_cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_cancelBtn setTitleColor:[ColorUtility colorWithHexString:@"9b4037"] forState:UIControlStateNormal];
        _cancelBtn.layer.masksToBounds = YES;
        [_cancelBtn addTarget:self action:@selector(hangUpCalling) forControlEvents:UIControlEventTouchUpInside];
        [self.bigImage addSubview:_cancelBtn];
        CGFloat cancelBtnHeight;
        CGFloat cancelBtnY;
        if (ScreenH==667||ScreenH==736) {
            cancelBtnY = -50;
            cancelBtnHeight = 130;
        }else if (ScreenH==568){
            cancelBtnY = -50;
            cancelBtnHeight = 110;
        }else if (ScreenH==480){
            cancelBtnY = -20;
            cancelBtnHeight = 110;
        }
        [_cancelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.bigImage.mas_bottom).offset(cancelBtnY);
            make.left.mas_equalTo(self.bigImage.mas_left).offset(40*picWScale);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(cancelBtnHeight);
        }];
        
    }
    return _cancelBtn;
}
- (UIButton *)anonymousCallBtn{
    if (!_anonymousCallBtn) {
        _anonymousCallBtn = [SLImageTopTitleBottomBtn buttonWithType:UIButtonTypeCustom];
        [_anonymousCallBtn setImage:[UIImage imageNamed:@"anonymousCall"] forState:UIControlStateNormal];
        
        [_anonymousCallBtn setTitle:@"匿名拨打" forState:UIControlStateNormal];
        _anonymousCallBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_anonymousCallBtn setTitleColor:[ColorUtility colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
        [_anonymousCallBtn addTarget:self action:@selector(anonymousCallPhone) forControlEvents:UIControlEventTouchUpInside];
        [self.bigImage addSubview:_anonymousCallBtn];
        CGFloat cancelBtnHeight;
        CGFloat cancelBtnY;
        if (ScreenH==667||ScreenH==736) {
            cancelBtnY = -50;
            cancelBtnHeight = 130;
        }else if (ScreenH==568){
            cancelBtnY = -50;
            cancelBtnHeight = 110;
        }else if (ScreenH==480){
            cancelBtnY = -20;
            cancelBtnHeight = 110;
        }
        [_anonymousCallBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.bigImage.mas_bottom).offset(cancelBtnY);
            make.centerX.mas_equalTo(self.bigImage.mas_centerX);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(cancelBtnHeight);
        }];
        
    }
    return _anonymousCallBtn;
}
- (UIButton *)ensureBtn{
    if (!_ensureBtn) {
        _ensureBtn = [SLImageTopTitleBottomBtn buttonWithType:UIButtonTypeCustom];
        [_ensureBtn setTitle:@"拨打" forState:UIControlStateNormal];
        _ensureBtn.layer.masksToBounds = YES;
        [_ensureBtn addTarget:self action:@selector(ensureCalling:) forControlEvents:UIControlEventTouchUpInside];
        [self.bigImage addSubview:_ensureBtn];
        [_ensureBtn setImage:[UIImage imageNamed:@"callPhone"] forState:UIControlStateNormal];
        
        _ensureBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_ensureBtn setTitleColor:[ColorUtility colorWithHexString:@"a1d1b3"] forState:UIControlStateNormal];
        CGFloat ensureBtnHeight;
        CGFloat ensureBtnY;
        if (ScreenH==667||ScreenH==736) {
            ensureBtnY = -50;
            ensureBtnHeight = 130;
            
        }else if (ScreenH==568){
            ensureBtnY = -50;
            ensureBtnHeight = 110;
            
        }else if (ScreenH==480){
            ensureBtnY = -20;
            ensureBtnHeight = 110;
            
        }
//        [_ensureBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.bottom.mas_equalTo(self.bigImage.mas_bottom).offset(ensureBtnY);
//            make.right.mas_equalTo(self.view.mas_right).offset(-40*picWScale);
//            make.width.mas_equalTo(80);
//            make.height.mas_equalTo(ensureBtnHeight);
//        }];
        CGFloat width = 80;
        _ensureBtn.frame = CGRectMake(screen_W-width-40*picWScale, screen_H+ensureBtnY-ensureBtnHeight, width, ensureBtnHeight);
    }
    return _ensureBtn;
}
- (UIButton *)backBtn{
    if (!_backBtn) {
        //返回
        _backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        //_backBtn.frame=CGRectMake(10, 20, 30, 30);
        [_backBtn addTarget:self action:@selector(hangUpCalling) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_backBtn];
        [_backBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.bigImage.mas_top).offset(20);
            make.left.mas_equalTo(10);
            make.width.mas_equalTo(50);
            make.height.mas_equalTo(50);
        }];
    }
    return _backBtn;
}
- (void)setUpUI{
    self.bigImage.image = [UIImage imageNamed:@"dialBg"];
    NSURL *avatarUrl;
    if ([self.user[@"avatar"] hasSuffix:@"!avatar"]) {
        NSRange range = [self.user[@"avatar"] rangeOfString:@"!avatar"];
        NSString *orignalUrlStr = [self.user[@"avatar"] substringToIndex:range.location];
        avatarUrl = [NSURL URLWithString:orignalUrlStr];
    }else{
        avatarUrl = [NSURL URLWithString:self.user[@"avatar"]];
    }
    
    
    [self.bigImage sd_setImageWithURL:avatarUrl placeholderImage:[UIImage imageNamed:@"dialBg"]];
    
    
    self.littleImage.layer.masksToBounds = YES;
    self.nickName.textColor = SLColor(255, 255, 255);
    self.priceLabel.textColor = [UIColor whiteColor];
    self.attentionLab.numberOfLines=0;
    //    self.ensureLabel.textColor = SLColor(51, 51, 51);
    //    self.describeLab.textColor=SLColor(51, 51, 51);
    //    self.attentionLab.textAlignment=NSTextAlignmentLeft;
    self.cancelBtn.backgroundColor = [UIColor clearColor];
    self.anonymousCallBtn.backgroundColor = [UIColor clearColor];
    self.ensureBtn.backgroundColor = [UIColor clearColor];
    [self.backBtn setImage:[UIImage imageNamed:@"closeBack"] forState:UIControlStateNormal];
    
}

//获取通话详情
-(void)getCall_logsData:(NSInteger)retryCount{
    UIButton *destBtn;
    
    if (self.isanonymousCall) {
        destBtn = self.anonymousCallBtn;
    }else{
        destBtn = self.ensureBtn;
    }
    if (self.isConnected&&self.isDisconnected) {
        if (self.isNoNetWork) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUDManager showWarningWithText:@"网络出错,请检查网络状况"];
                self.ensureLabel.text= @"结算失败,请重试";
            });
            destBtn.userInteractionEnabled = YES;
        }else{
            
            retryCount++;
            [SLAPIHelper getCommunicateDetail:_log_token success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                destBtn.userInteractionEnabled = YES;
                self.dictCall=data[@"result"];
                
                NSDictionary *dict=self.dictCall[@"transaction"];
                
                if (dict.count>0) {
                    //通话结束发通知刷新我的页面
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"amountChange" object:nil];
                    CGFloat amountMoney = [dict[@"amount"] doubleValue];
                    CGFloat voucherMoney = [dict[@"voucher_amount"] doubleValue];
                    CGFloat balanceMoney = amountMoney - voucherMoney;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        self.ensureLabel.text = [NSString stringWithFormat:@"本次通话:  %ld分%ld秒",[dict[@"duration"] integerValue]/60,[dict[@"duration"] integerValue]%60];
                        self.ensureLabel.textColor = [ColorUtility colorWithHexString:@"fc6052"];
                        [self.ensureLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                            make.top.mas_equalTo(self.priceLabel.mas_bottom).offset(35*picWScale);
                            make.left.mas_equalTo(self.bigImage.mas_left);
                            make.width.equalTo(@(screen_W));
                        }];
                        
                        
                        
                        [self.whiteView mas_remakeConstraints:^(MASConstraintMaker *make) {
                            make.top.mas_equalTo(self.describeLab.mas_bottom).offset(5);
                            make.width.mas_equalTo(160);
                            make.centerX.mas_equalTo(self.describeLab.centerX);
                            make.height.mas_equalTo(1);
                        }];
                        
                        [self.amountLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                            make.top.mas_equalTo(self.whiteView.mas_bottom).offset(16*picWScale);
                            make.left.mas_equalTo(self.whiteView.mas_left);
                            
                        }];
                        
                        self.amountMoneyLab.text = [NSString stringWithFormat:@"%.2f元",amountMoney];
                        //富文本设置
                        NSMutableAttributedString *amountMoneyAttention=[[NSMutableAttributedString alloc] initWithString:self.amountMoneyLab.text];
                        [amountMoneyAttention addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:NSMakeRange(0, amountMoneyAttention.length-1)];
                        [amountMoneyAttention addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, amountMoneyAttention.length-1)];
                        [amountMoneyAttention addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:NSMakeRange(amountMoneyAttention.length-1,1)];
                        [amountMoneyAttention addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(amountMoneyAttention.length-1,1)];
                        self.amountMoneyLab.attributedText=amountMoneyAttention;
                        
                        [self.amountMoneyLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                            make.top.mas_equalTo(self.amountLab.mas_top);
                            make.right.mas_equalTo(self.whiteView.mas_right);
                        }];
                        
                        [self.balanceLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                            make.top.mas_equalTo(self.amountLab.mas_bottom).offset(15*picWScale);
                            make.left.mas_equalTo(self.amountLab.mas_left);
                        }];
                        self.balanceMoneyLab.text = [NSString stringWithFormat:@"%.2f元",balanceMoney];
                        //富文本设置
                        NSMutableAttributedString *balanceMoneyAttention=[[NSMutableAttributedString alloc] initWithString:self.balanceMoneyLab.text];
                        [balanceMoneyAttention addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:NSMakeRange(0, balanceMoneyAttention.length-1)];
                        [balanceMoneyAttention addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, balanceMoneyAttention.length-1)];
                        [balanceMoneyAttention addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:NSMakeRange(balanceMoneyAttention.length-1,1)];
                        [balanceMoneyAttention addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(balanceMoneyAttention.length-1,1)];
                        self.balanceMoneyLab.attributedText=balanceMoneyAttention;
                        [self.balanceMoneyLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                            make.top.mas_equalTo(self.balanceLab.mas_top);
                            make.right.mas_equalTo(self.whiteView.mas_right);
                        }];
                        if (voucherMoney > 0) {
                            [self.voucherLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                                make.top.mas_equalTo(self.balanceLab.mas_bottom).offset(15*picWScale);
                                make.left.mas_equalTo(self.balanceLab.mas_left);
                            }];
                            self.voucherMoneyLab.text = [NSString stringWithFormat:@"%.2f元",voucherMoney];
                            //富文本设置
                            NSMutableAttributedString *voucherMoneyAttention=[[NSMutableAttributedString alloc] initWithString:self.voucherMoneyLab.text];
                            [voucherMoneyAttention addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:NSMakeRange(0, voucherMoneyAttention.length-1)];
                            [voucherMoneyAttention addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, voucherMoneyAttention.length-1)];
                            [voucherMoneyAttention addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:NSMakeRange(voucherMoneyAttention.length-1,1)];
                            [voucherMoneyAttention addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(voucherMoneyAttention.length-1,1)];
                            self.voucherMoneyLab.attributedText=voucherMoneyAttention;
                            [self.voucherMoneyLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                                make.top.mas_equalTo(self.voucherLab.mas_top);
                                make.right.mas_equalTo(self.whiteView.mas_right);
                            }];
                        }
                        self.attentionLab.hidden = YES;
                        self.cancelBtn.hidden = NO;
                        [self.cancelBtn setTitle:[NSString stringWithFormat:@"知道了"] forState:UIControlStateNormal];
                        [self.cancelBtn setImage:[UIImage imageNamed:@"notCommit"] forState:UIControlStateNormal];
                        self.ensureBtn.hidden = NO;
                        [self.ensureBtn setTitle:[NSString stringWithFormat:@"去评价"] forState:UIControlStateNormal];
                        [self.ensureBtn setTitleColor:[ColorUtility colorWithHexString:@"f2ea92"] forState:UIControlStateNormal];
                        [self.ensureBtn setImage:[UIImage imageNamed:@"goComment"] forState:UIControlStateNormal];
                        
                        CGFloat ensureBtnHeight;
                        CGFloat ensureBtnY;
                        if (ScreenH==667||ScreenH==736) {
                            ensureBtnY = -50;
                            ensureBtnHeight = 130;
                            
                        }else if (ScreenH==568){
                            ensureBtnY = -50;
                            ensureBtnHeight = 110;
                            
                        }else if (ScreenH==480){
                            ensureBtnY = -20;
                            ensureBtnHeight = 110;
                            
                        }
                        self.anonymousCallBtn.hidden = YES;
                        [self.ensureBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                            make.bottom.mas_equalTo(self.bigImage.mas_bottom).offset(ensureBtnY);
                            make.right.mas_equalTo(self.view.mas_right).offset(-75*picWScale);
                            make.width.mas_equalTo(80);
                            make.height.mas_equalTo(ensureBtnHeight);
                        }];
                        [self.cancelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                            make.bottom.mas_equalTo(self.bigImage.mas_bottom).offset(ensureBtnY);
                            make.left.mas_equalTo(self.bigImage.mas_left).offset(75*picWScale);
                            make.width.mas_equalTo(80);
                            make.height.mas_equalTo(ensureBtnHeight);
                        }];
                    });
                }else {
                    if (retryCount<4) {
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            UIWindow *win =[UIApplication sharedApplication].keyWindow;
                            [HUDManager showLoadingHUDView:win withText:@"正在加载"];
                            [self getCall_logsData:retryCount];
                            [HUDManager hideHUDView];
                        });
                        
                    }else{
                        self.ensureLabel.text = @"结算失败，请重试";
                        self.ensureLabel.textColor = [ColorUtility colorWithHexString:@"ffffff"];
                        
                        [self.ensureLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                            make.top.mas_equalTo(self.priceLabel.mas_bottom).offset(10);
                            make.left.mas_equalTo(self.bigImage.mas_left);
                            make.width.mas_equalTo(ScreenW);
                            make.height.mas_equalTo(15);
                        }];
                        
                        CGFloat ensureBtnHeight;
                        CGFloat ensureBtnY;
                        if (ScreenH==667||ScreenH==736) {
                            ensureBtnY = -50;
                            ensureBtnHeight = 130;
                            
                        }else if (ScreenH==568){
                            ensureBtnY = -50;
                            ensureBtnHeight = 110;
                            
                        }else if (ScreenH==480){
                            ensureBtnY = -20;
                            ensureBtnHeight = 110;
                        }
                        [destBtn setTitle:@"重试" forState:UIControlStateNormal];
                        [destBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                            make.bottom.mas_equalTo(self.bigImage.mas_bottom).offset(ensureBtnY);
                            make.centerX.mas_equalTo(self.view);
                            make.width.mas_equalTo(80);
                            make.height.mas_equalTo(ensureBtnHeight);
                        }];
                        self.attentionLab.hidden = YES;
                        self.timeLabel.hidden = YES;
                        [destBtn setImage:[UIImage imageNamed:@"repeatCallPhone"] forState:UIControlStateNormal];
                        [destBtn setTitleColor:[ColorUtility colorWithHexString:@"a1d1b3"] forState:UIControlStateNormal];
                    }
                    
                }
                
            } failure:^(SLHttpRequestError *error) {
                destBtn.userInteractionEnabled = YES;
                
                if (error.httpStatusCode==404) {
                    if (retryCount<4) {
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

                            
                            UIWindow *win =[UIApplication sharedApplication].keyWindow;
                            [HUDManager showLoadingHUDView:win withText:@"正在加载"];
                            [self getCall_logsData:retryCount];
                            [HUDManager hideHUDView];
                            
                        });
                        
                    }
                    else{
                        self.attentionLab.hidden = YES;
                        NSLog(@"%ld%ld",error.slAPICode,error.httpStatusCode);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            self.ensureLabel.text=@"结算失败,请重试";
                            self.ensureLabel.textColor = [ColorUtility colorWithHexString:@"ffffff"];
                            [self.ensureLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                                make.top.mas_equalTo(self.priceLabel.mas_bottom).offset(35*picWScale);
                                make.left.mas_equalTo(self.bigImage.mas_left);
                                make.width.equalTo(@(screen_W));
                            }];
                            self.attentionLab.hidden = YES;
                            
                            CGFloat ensureBtnHeight;
                            CGFloat ensureBtnY;
                            if (ScreenH==667||ScreenH==736) {
                                ensureBtnY = -50;
                                ensureBtnHeight = 130;
                                
                            }else if (ScreenH==568){
                                ensureBtnY = -50;
                                ensureBtnHeight = 110;
                                
                            }else if (ScreenH==480){
                                ensureBtnY = -20;
                                ensureBtnHeight = 110;
                                
                            }
                            
                            [destBtn setTitle:@"重试" forState:UIControlStateNormal];
                            [destBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                                make.bottom.mas_equalTo(self.bigImage.mas_bottom).offset(ensureBtnY);
                                make.centerX.mas_equalTo(self.view);
                                make.width.mas_equalTo(80);
                                make.height.mas_equalTo(ensureBtnHeight);
                            }];
                            
                            
                        });
                    }
                }
            }];
        }
    }
}

- (void)ensureCalling:(UIButton *)btn{
   
    if ([btn.titleLabel.text isEqualToString:@"拨打"] || [btn.titleLabel.text isEqualToString:@"重拨"]|| [btn.titleLabel.text isEqualToString:@"匿名拨打"]) {
        [self calling:self.ensureBtn];
    }else if ([btn.titleLabel.text isEqualToString:@"重试"]){
        [self retryBtnClick];
    }else if ([self.ensureBtn.titleLabel.text isEqualToString:@"去评价"]){
        [self evluateBtnClick];
    }
    
}
-(void)evluateBtnClick{
    SLCallJudgeViewController *vc=[[SLCallJudgeViewController alloc] init];
    vc.callId=self.dictCall[@"id"];
    vc.CallingBlock=^(BOOL isCalling){
        if (isCalling) {
            self.ensureBtn.userInteractionEnabled=NO;
            [self.ensureBtn setTitle:[NSString stringWithFormat:@"已评价"] forState:UIControlStateNormal];
        }
        
    };
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)hangUpCalling{
    
    if(self.timer.isValid){
        [self.timer invalidate];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)retryBtnClick{
    [self getCall_logsData:0];
}
- (void)calling:(id)sender{
    if(!self.user){
        return;
    }
    NSString *dnd_status=[NSString stringWithFormat:@"%@",self.user[@"dnd_status"]];
    NSString *blockedMe=[NSString stringWithFormat:@"%@",self.user[@"blockedMe"]];
    NSString *msg=@"";
    if ([dnd_status isEqualToString:@"1"]) {
        msg=@"对方已屏蔽所有来电";
    }else if ([blockedMe isEqualToString:@"1"]) {
        msg=@"对方已屏蔽你的来电";
    }
    if ([dnd_status isEqualToString:@"1"]||[blockedMe isEqualToString:@"1"]) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:msg delegate:self cancelButtonTitle:@"好" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        
        [self startCalling];
    }
    
    
}

//如果没加入黑名单或者没开启屏蔽
-(void)startCalling{
    NSNumber *phoneType;
    if (!self.isanonymousCall) {
         self.ensureBtn.userInteractionEnabled=NO;
        phoneType = @0;
    }else{
        phoneType = @1;
    }
    NSDictionary *dict = @{@"anonymous_mode":phoneType};
    [SLAPIHelper phoneCall:dict phoneNum:self.user[@"id"] success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        [MobClick event:@"call_count"];
        if (self.isanonymousCall) {
            self.cancelBtn.hidden = YES;
            self.ensureBtn.hidden = YES;
        }else{
            self.cancelBtn.hidden = YES;
            self.anonymousCallBtn.hidden = YES;
            [UIView animateWithDuration:0.5 animations:^{
                [self.ensureBtn setTitle:[NSString stringWithFormat:@"重拨"] forState:UIControlStateNormal];
                self.ensureBtn.titleLabel.numberOfLines=0;
                self.ensureBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
                self.ensureBtn.x = (screen_W-self.ensureBtn.width)/2.0;
                
            }];
        }
        _count=15;
        self.log_token=data[@"result"][@"log_token"];
        [self startTimer];
    } failure:^(SLHttpRequestError *error) {
        if (self.isanonymousCall) {
           self.anonymousCallBtn.userInteractionEnabled=YES;
        }else{
            self.ensureBtn.userInteractionEnabled=YES;
        }
        

        // 余额不足
        if (error.httpStatusCode == 400&&error.slAPICode==501) {
            //余额不足时获取信息传到下个页面
            [self getUserMessage];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"您的余额不足,请充值后再操作!" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"前往", nil];
            [alert show];
            //[self alertMessage:@"您的余额不足,请充值!"];
            
            // 拨打失败
        } else if (error.httpStatusCode == 400&&error.slAPICode==503){
           [HUDManager showWarningWithText:@"对方手机号异常\n请联系超级助理"];
        }
    }];
    
    
}
//余额不足时获取信息传到下个页面
-(void)getUserMessage{
    NSDictionary *params=@{@"id":ApplicationDelegate.userId};
    [SLAPIHelper getUsersData:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        self.userData=data[@"result"];
    } failure:^(SLHttpRequestError *failure) {
        
    }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        [self doclick];
    }
}
// 前往充值页面
-(void)doclick{
    
    SLUserAccountViewController *moneyVC = [[SLUserAccountViewController alloc]init];
    moneyVC.type = @"1";
    moneyVC.amount = self.userData[@"amount"];
    [self.navigationController pushViewController:moneyVC animated:YES];
}

-(void)startTimer{
    // 开始执行任务的定时器
    UIButton *destBtn;
    if (self.isanonymousCall) {
        destBtn = self.anonymousCallBtn;
    }else{
        destBtn = self.ensureBtn;
    }
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(redialClock:) userInfo:nil repeats:YES];
    // 修改NSTimer在NSRunLoop中的模式：NSRunLoopCommonModes
    // 主线程不管在处理什么操作，都会抽时间处理NSTimer
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];

    if (!self.timeLabel) {
        self.timeLabel = [[UILabel alloc] init];
        self.timeLabel.font = [UIFont systemFontOfSize:21];
        self.timeLabel.textColor = [UIColor whiteColor];
        [destBtn.imageView addSubview:self.timeLabel];
        self.timeLabel.textAlignment = NSTextAlignmentCenter;
        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.centerY.equalTo(destBtn.imageView);
        }];
    }
}

-(void)redialClock:(NSTimer *)myTime{
    UIButton *destBtn;
    if (self.isanonymousCall) {
        destBtn = self.anonymousCallBtn;
    }else{
        destBtn = self.ensureBtn;
    }
    [destBtn setTitle:@"重拨" forState:UIControlStateNormal];
    
    if(_count == 0){
        self.timeLabel.hidden = YES;
        _count = 15;
        if(myTime.isValid){
            [myTime invalidate];
            destBtn.userInteractionEnabled = YES;   //启动正常的按钮状态
            [destBtn setImage:[UIImage imageNamed:@"repeatCallPhone"] forState:UIControlStateNormal];
            [destBtn setTitleColor:[ColorUtility colorWithHexString:@"a1d1b3"] forState:UIControlStateNormal];
        }
    }else{
        self.timeLabel.hidden = NO;
        [destBtn setImage:[UIImage imageNamed:@"countDown"] forState:UIControlStateNormal];
        destBtn.userInteractionEnabled = NO;
        [destBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _count--;
        self.timeLabel.text = [NSString stringWithFormat:@"%ld秒",_count];
    }
}
- (void)anonymousCallPhone{
    self.isanonymousCall = YES;
    self.anonymousCallBtn.userInteractionEnabled = NO;
    [self ensureCalling:self.anonymousCallBtn];
}
- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.isNoNetWork=YES;
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            self.isNoNetWork=NO;
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.isNoNetWork=NO;
        }
    }
   
}
-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
