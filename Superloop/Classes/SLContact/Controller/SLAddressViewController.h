//
//  SLAddressViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLAddressViewController : UITableViewController


@property (nonatomic,weak)UIView *nullLoginView;

@property (nonatomic,weak)UIView *noNetWorkView;
@end
