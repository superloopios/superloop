//
//  SLAddressViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLAddressViewController.h"
#import "SLContactCell.h"
#import <AFNetworking.h>
#import "SLContactModel.h"
#import <MJExtension.h>
#import <MJRefresh/MJRefresh.h>
#import "SLRefreshHeader.h"
#import "SLMessageManger.h"
#import "AppDelegate.h"
#import "SLCallingViewController.h"
#import <UIImageView+WebCache.h>
#import "SLAPIHelper.h"
#import "SLBlackListViewController.h"
#import "ChineseString.h"
#import "SLTabBarViewController.h"
#import "HomePageViewController.h"
#import "SLNavgationViewController.h"
#import "SLRegisterUserViewController.h"
#import "SLFirstViewController.h"

@interface SLAddressViewController ()<SLContactCellDelegate>{
    UILabel *numberLabel;
}

/** 全部的联系人数据 */
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableArray *sectionArr;
@property (nonatomic, strong)UILabel *bolangLabel;
@property (nonatomic, strong)UIImageView *swanImageview;

@property (nonatomic, strong) UIView *noNetworkOrDefaultView;
@property (nonatomic, strong)UILabel *tiplabel;
@property (nonatomic, strong)UIImageView *notLoginImageView;
@property (nonatomic, strong) UIView *loginView;
@property (nonatomic, strong)UILabel *loginLabel;
@property (nonatomic, strong)UIButton *loginBtn;

@property (nonatomic, strong)UILabel *registerLabel;
@property (nonatomic, strong)UIButton *registerBtn;


@end

@implementation SLAddressViewController
static NSString * const contactCell = @"contactCell";
static NSString * const notLoginText = @"登录后查看通讯录好友！";
static NSString * const notRegisterText = @"还没有账号？去注册>>";
- (UIButton *)loginBtn{
    if (!_loginBtn) {
        _loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_loginBtn setTitle:@"登录" forState:UIControlStateNormal];
        _loginBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_loginBtn setTitleColor:UIColorFromRGB(0x00aeff) forState:UIControlStateNormal];
        [self.loginView addSubview:_loginBtn];
        [_loginBtn addTarget:self action:@selector(loginSystem) forControlEvents:UIControlEventTouchUpInside];
    }
    return _loginBtn;
}
- (UILabel *)loginLabel{
    if (!_loginLabel) {
        _loginLabel = [[UILabel alloc] init];
        _loginLabel.font = [UIFont systemFontOfSize:15];
        _loginLabel.textColor = UIColorFromRGB(0x9e9e9e);
        _loginLabel.text = @"后查看通讯录好友！";
        [self.loginView addSubview:_loginLabel];
    }
    return _loginLabel;
}

- (UIButton *)registerBtn{
    if (!_registerBtn) {
        _registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_registerBtn setTitle:@"去注册>>" forState:UIControlStateNormal];
        _registerBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_registerBtn setTitleColor:UIColorFromRGB(0x00aeff) forState:UIControlStateNormal];
        [self.loginView addSubview:_registerBtn];
        [_registerBtn addTarget:self action:@selector(registerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _registerBtn;
}
- (UILabel *)registerLabel{
    if (!_registerLabel) {
        _registerLabel = [[UILabel alloc] init];
        _registerLabel.font = [UIFont systemFontOfSize:15];
        _registerLabel.textColor = UIColorFromRGB(0x9e9e9e);
        _registerLabel.text = @"还没有账号？";
        [self.loginView addSubview:_registerLabel];
    }
    return _registerLabel;
}
- (UIView *)loginView{
    if (!_loginView) {
        _loginView = [[UIView alloc]initWithFrame:CGRectMake(0, 128.5, ScreenW , ScreenH-128.5-49-60)];
        _loginView.backgroundColor = SLColor(245, 245, 245);
        
        [self.tableView addSubview:_loginView];
    }
    return _loginView;
}
- (UILabel *)tiplabel{
    if (!_tiplabel) {
        _tiplabel = [[UILabel alloc] init];
        _tiplabel.font = [UIFont systemFontOfSize:17];
        _tiplabel.textAlignment = NSTextAlignmentCenter;
        [self.noNetworkOrDefaultView addSubview:_tiplabel];
        
    }
    return _tiplabel;
}

- (UILabel *)bolangLabel{
    if (!_bolangLabel) {
        _bolangLabel = [[UILabel alloc] init];
        _bolangLabel.font = [UIFont systemFontOfSize:12];
        _bolangLabel.textAlignment = NSTextAlignmentCenter;
        _bolangLabel.text = @"~~~";
        _bolangLabel.textColor = SLColor(213, 213, 213);
        [self.noNetworkOrDefaultView addSubview:_bolangLabel];
    }
    return _bolangLabel;
}

- (UIImageView *)swanImageview{
    if (!_swanImageview) {
        _swanImageview = [[UIImageView alloc] init];
        _swanImageview.image = [UIImage imageNamed:@"swan"];
        [self.noNetworkOrDefaultView addSubview:_swanImageview];
    }
    return _swanImageview;
}
- (UIImageView *)notLoginImageView{
    if (!_notLoginImageView) {
        _notLoginImageView = [[UIImageView alloc] init];
        _notLoginImageView.image = [UIImage imageNamed:@"swan"];
        [self.loginView addSubview:_notLoginImageView];
    }
    return _notLoginImageView;
}

- (UIView *)noNetworkOrDefaultView{
    if (!_noNetworkOrDefaultView) {
        _noNetworkOrDefaultView = [[UIView alloc]initWithFrame:CGRectMake(0, 128.5, ScreenW , ScreenH-136.5)];
        _noNetworkOrDefaultView.backgroundColor = SLColor(245, 245, 245);
        [self.tableView addSubview:_noNetworkOrDefaultView];
    }
    return _noNetworkOrDefaultView;
}

-(NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr=[NSMutableArray new];
    }
    return _dataArr;
}
-(NSMutableArray *)sectionArr{
    if (!_sectionArr) {
        _sectionArr=[NSMutableArray new];
    }
    return _sectionArr;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"SLAddressViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLAddressViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = SLColor(245, 245, 245);
    [self setUpTable];
    [self setUpRefresh];
    [self loadOldData];
    if (ApplicationDelegate.userId) {
         [self.tableView.mj_header beginRefreshing];
    }else{
        [self addDefaultLoginView];
    }
    [self addFooterView];
    self.tableView.scrollsToTop=YES;
    self.tableView.tableFooterView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
    UIImageView *footImageView = [[UIImageView alloc] init];
    footImageView.frame = CGRectMake(0, 0, self.view.width, 0.5);
//    self.tableView.tableFooterView = footImageView;
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexColor = [UIColor lightGrayColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isUnfollow) name:@"isUnfollow" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isLoginSuccessToLoadData) name:@"isLoginSuccessToLoadData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isLoginSuccessToLoadData) name:@CallAndContact_Refreshing object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearAllInfo) name:@"exitSuperLoop" object:nil];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
- (void)clearAllInfo{
    [self.sectionArr removeAllObjects];
    [self.dataArr removeAllObjects];
    [self.tableView reloadData];
    numberLabel.text = @"";
    [self addDefaultLoginView];
}
- (void)addFooterView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_W, 40)];
//    view.backgroundColor = [UIColor redColor];
    numberLabel = [[UILabel alloc] init];
    numberLabel.textColor = SLColor(104, 104, 104);
    numberLabel.font = [UIFont systemFontOfSize:16];
    numberLabel.frame = CGRectMake(0, 0, screen_W, 40);
    numberLabel.textAlignment = NSTextAlignmentCenter;
    [view addSubview:numberLabel];
    self.tableView.tableFooterView = view;
}
-(void)isLoginSuccessToLoadData{
//    [self.tableView.mj_header beginRefreshing];
    
    [self loadNewTable];
}
-(void)isUnfollow{
//    [self.tableView.mj_header beginRefreshing];
    [self loadNewTable];
}

- (void)loadOldData{
    NSData *configData = [SLUserDefault getAddressData];
    if (configData) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
//        NSLog(@"%@",dict);
        [self.sectionArr removeAllObjects];
        [self.dataArr removeAllObjects];
        NSArray *sectionArr  = dict[@"section"];
        NSMutableArray *sectionMutArr=[NSMutableArray new];
        [sectionMutArr addObjectsFromArray:sectionArr];
        self.sectionArr=sectionMutArr;
        NSArray *dataArr = dict[@"data"];
        NSMutableArray *dataMutArr=[NSMutableArray new];
        [dataMutArr addObjectsFromArray:dataArr];
        self.dataArr=dataMutArr;
        [self.tableView reloadData];
    }
}
- (void)setUpRefresh{
    self.tableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewTable)];
}
- (void)setUpTable{
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SLContactCell class]) bundle:nil] forCellReuseIdentifier:contactCell];
}

- (NSString *)getTimeNow
{
    NSString* date;
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc ] init];
    //[formatter setDateFormat:@"YYYY.MM.dd.hh.mm.ss"];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss:SSS"];
    date = [formatter stringFromDate:[NSDate date]];
    NSString * timeNow = [[NSString alloc] initWithFormat:@"%@", date];
    return timeNow;
}
- (void)loadNewTable{
    if (ApplicationDelegate.userId) {
        [_loginView removeFromSuperview];
    }else{
        [self.tableView.mj_header endRefreshing];
        return;
    }
    NSLog(@"1:%@",[self getTimeNow]);
    
    NSDictionary *parameters = @{@"id":[NSString stringWithFormat:@"%@", ApplicationDelegate.userId],@"pager":@"1000"};
    [SLAPIHelper userFollows:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        [_noNetworkOrDefaultView removeFromSuperview];
        NSArray *array = data[@"result"];
//        NSLog(@"%@",data);
        NSInteger count = array.count;
        NSLog(@"2:%@",[self getTimeNow]);
        if (array.count == 0) {
            if (!_nullLoginView) {
                self.tableView.tableFooterView.hidden = YES;
                [self addTipViews:@"你还没有联系人，活跃起来吧!"];
            }
        }else{
            numberLabel.text = [NSString stringWithFormat:@"-%ld位联系人-",count];
//            [numberLabel sizeToFit];
            self.tableView.tableFooterView.hidden = NO;
            [self.nullLoginView removeFromSuperview];
        }
        //        [self.sectionArr removeAllObjects];
        //        [self.dataArr removeAllObjects];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            self.sectionArr = [ChineseString IndexArray:array];
            self.dataArr = [ChineseString LetterSortArray:array];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"3:%@",[self getTimeNow]);
                [self.tableView.mj_header endRefreshing];
                NSDictionary *dict=@{@"section":self.sectionArr,@"data":self.dataArr};
                NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:dict];
                [SLUserDefault setAddressData:configData];
                [self.tableView reloadData];
            });
        });
        
    } failure:^(SLHttpRequestError *error) {
        //NSLog(@"请求失败");
        [self.tableView.mj_header endRefreshing];
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  self.sectionArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.dataArr.count>0) {
        return [[self.dataArr objectAtIndex:section] count];
    }else{
        return 0;
    }
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return self.sectionArr;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    [HUDManager showAlertWithText:self.sectionArr[index]];
    return index;
}
//- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return @"取消关注";
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SLContactCell *cell = [tableView dequeueReusableCellWithIdentifier:contactCell];
    cell.Delegate = self;
    cell.indexpath = indexPath;
    NSDictionary *dict=[[self.dataArr objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    
    cell.nameLabel.text = dict[@"nickname"];
    NSString *iconUrlString = dict[@"avatar"];
    [cell.iconView sd_setImageWithURL:[NSURL URLWithString:iconUrlString] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    //改为简介
    NSString *str=dict[@"bio"];
    if (str.length>0) {
        cell.positionLab.text=dict[@"bio"];
    }else{
        cell.positionLab.text=@"这个人很懒什么都没写";
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    if (section==0) {
//        return 5;
//    }
//    return 0;
//}

//选择控制器
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //当返回来的时候取消单元格的选中
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dict=[[self.dataArr objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    NSString *userId = dict[@"id"];
    HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
    otherPerson.userId = userId;
    SLTabBarViewController *tabbarVc = (SLTabBarViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    UINavigationController *selectedController = tabbarVc.selectedViewController;
    [selectedController pushViewController:otherPerson animated:YES];
}
- (void)didMessageButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath{
    NSDictionary *dict=[[self.dataArr objectAtIndex:indexpath.section]objectAtIndex:indexpath.row];
    NSString *userId = dict[@"id"];
    YWPerson *person = [[YWPerson alloc] initWithPersonId:userId.description];
    SLTabBarViewController *tabbarVc = (SLTabBarViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    UINavigationController *selectedController = tabbarVc.selectedViewController;
    ApplicationDelegate.imTitlename = dict[@"nickname"];
    [[SLMessageManger sharedInstance] openConversationViewControllerWithPerson:person fromNavigationController:selectedController];
}
//打电话按钮被点击
- (void)didCallButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath{
    NSDictionary *dict=[[self.dataArr objectAtIndex:indexpath.section]objectAtIndex:indexpath.row];
    if (dict) {
        [self loadPriceData:dict[@"id"]];
    }
}
// 获取用户信息
- (void)loadPriceData:(NSString *)uid{
    if (!uid) {
        return;
    }
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow
                          withText:@""];
    [SLAPIHelper getPriceData:uid success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        [HUDManager hideHUDView];
//        NSLog(@"%@", data[@"result"][@"price"]);
        SLCallingViewController *callView = [[SLCallingViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:callView];
        callView.user = data[@"result"];
        SLTabBarViewController *tabbarVc = (SLTabBarViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
        UINavigationController *selectedController = tabbarVc.selectedViewController;
        [selectedController presentViewController:nav animated:YES completion:nil];
        
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        if (failure.slAPICode==-1) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [HUDManager showWarningWithText:@"请检查网络状况"];
            });
        }
    }];
}

#pragma mark ----- 单元格可以编辑
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
//    return YES;
//}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if(editingStyle == UITableViewCellEditingStyleDelete){
//        //取消关注
//        NSDictionary *dict=[[self.dataArr objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
//        NSDictionary *parameters = @{@"user_id":[NSString stringWithFormat:@"%@", dict[@"id"]]};
//        //先请求接口得到数据之后再删除对应的单元格
//        [SLAPIHelper cancelFollow:parameters success:^(NSURLSessionDataTask *task, NSDictionary *data) {
//            NSMutableArray *arr = [self.dataArr objectAtIndex:indexPath.section];
//            [arr removeObjectAtIndex:indexPath.row];
//            // 刷新
//            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//        } failure:^(SLHttpRequestError *failure) {
//            [HUDManager showWarningWithText:@"取消关注失败"];
//        }];
//    }
//}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            if (!_noNetWorkView) {
                if (self.dataArr.count==0) {
                    [self addTipViews:@"世界上最遥远的距离就是－－妹有网"];
                    
                    [self.tableView.mj_header endRefreshing];
                }
                
            }
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            [_noNetWorkView  removeFromSuperview];
        }else if ([netWorkStaus isEqualToString:@"wan"] ||[netWorkStaus isEqualToString:@"Unknown"]) {
            [_noNetWorkView  removeFromSuperview];
        }
    }
    
}
- (void)addTipViews:(NSString *)str{
    [self.tableView addSubview:self.noNetworkOrDefaultView];
    CGSize tipLabelSize = [self.tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    self.tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        self.tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    self.tiplabel.attributedText = AttributedStr;
    
    self.bolangLabel.frame = CGRectMake(self.tiplabel.frame.origin.x+self.tiplabel.frame.size.width,self.tiplabel.frame.origin.y, 50, 10);
    
    self.swanImageview.frame = CGRectMake(ScreenW *0.5-96, self.tiplabel.frame.origin.y + 76, 192, 192);
    
}
- (void)addDefaultLoginView{
    [self.tableView addSubview:self.loginView];
    self.notLoginImageView.frame = CGRectMake(ScreenW *0.5-96, 33, 192*picWScale, 192*picWScale);
    
    CGFloat loginLabelWidth = [notLoginText boundingRectWithSize:CGSizeMake(screen_W, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size.width+3;
    CGFloat loginBtnWidth = [@"登录" boundingRectWithSize:CGSizeMake(screen_W, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size.width+3;
    CGFloat leftMargin = (screen_W-loginLabelWidth)/2.0;
    [self.loginBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(leftMargin);
        make.top.equalTo(self.notLoginImageView.mas_bottom).offset(39);
        make.width.equalTo(@(loginBtnWidth));
        make.height.equalTo(@20);
    }];
    [self.loginLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.loginBtn.mas_right);
        make.top.equalTo(self.loginBtn);
        make.height.equalTo(@20);
    }];
    
    CGFloat registerLabelWidth = [notRegisterText boundingRectWithSize:CGSizeMake(screen_W, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size.width+3;
    CGFloat registerBtnWidth = [@"去注册>>" boundingRectWithSize:CGSizeMake(screen_W, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size.width+3;
    CGFloat registerLeftMargin = (screen_W-registerLabelWidth)/2.0;
    [self.registerLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(registerLeftMargin);
        make.top.equalTo(self.loginLabel.mas_bottom).offset(9);
        make.height.equalTo(@20);
    }];
    [self.registerBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.registerLabel.mas_right);
        make.top.equalTo(self.registerLabel);
        make.width.equalTo(@(registerBtnWidth));
        make.height.equalTo(@20);
    }];
}

- (void)loginSystem{
    SLFirstViewController *vc=[[SLFirstViewController alloc] init];
    
    SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
    //    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}
- (void)registerBtnClick{
    SLRegisterUserViewController *vc=[[SLRegisterUserViewController alloc] init];
    SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
    //    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}
@end
