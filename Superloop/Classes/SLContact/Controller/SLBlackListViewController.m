//
//  SLBlackListViewController.m
//  Superloop
//
//  Created by WangS on 16/5/19.
//  重构  by  xiaowu  on  16/11/09
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLBlackListViewController.h"
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "AppDelegate.h"
#import "SLAPIHelper.h"
#import "SLBlackListCell.h"
#import "SLBlackListModel.h"
#import "SLMessageManger.h"
#import "HomePageViewController.h"
#import "SLDefaultView.h"

@interface SLBlackListViewController ()<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,SLBlackListCellDelegate>
@property(nonatomic,assign)NSInteger page;
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *blackListDataSources;
@property (nonatomic,strong) SLDefaultView *defaultView;
@end

@implementation SLBlackListViewController

- (SLDefaultView *)defaultView{
    if (!_defaultView) {
        _defaultView = [[SLDefaultView alloc] init];
        _defaultView.frame = CGRectMake(0, 64, screen_W, screen_H-64);
    }
    return _defaultView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLBlackListViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLBlackListViewController"];
}
#pragma mark - 懒加载
-(NSMutableArray *)blackListDataSources{
    if (!_blackListDataSources) {
        _blackListDataSources = [NSMutableArray new];
    }
    return _blackListDataSources;
}
-(UITableView *)myTableView
{
    if (!_myTableView) {
        _myTableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, ScreenW, ScreenH-64) style:UITableViewStylePlain];
        _myTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        _myTableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
        _myTableView.dataSource=self;
        _myTableView.delegate=self;
        [self.view addSubview:_myTableView];
        _myTableView.backgroundColor = SLColor(240, 240, 240);
        [_myTableView registerClass:[SLBlackListCell class] forCellReuseIdentifier:@"BlackListCell"];
    }
    return _myTableView;
}
-(NSInteger)page{
    if (!_page) {
        _page=0;
    }
    return _page;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = SLColor(240, 240, 240);
    [self setNav];
    [self setUpRefresh];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBlackList) name:@"cancelBlackList" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteBlackPerson) name:@"isUnfollow" object:nil];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
-(void)setNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    backBtn.titleLabel.font=[UIFont systemFontOfSize:15];
        [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"黑名单";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
 
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=[UIColor lightGrayColor];
    [navView addSubview:cutImgView];

}
-(void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)changeBlackList{
   [self getBlackListData];
}

- (void)deleteBlackPerson{
    [self getBlackListData];
}

- (void)setUpRefresh{
    self.myTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getBlackListData)];
    self.myTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreBlackListData)];
    [self.myTableView.mj_header beginRefreshing];
    [self.myTableView.mj_footer setHidden:YES];
}

#pragma mark - 获取黑名单数据
-(void)getBlackListData
{
    self.page=0;
    NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld", (long)self.page],@"pager":@"20"};
    [SLAPIHelper getBlackList:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        self.blackListDataSources =[SLBlackListModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
        if (self.blackListDataSources.count ==0) {
            if (!_noBlackListView) {
                [self addNotLoginViews:@"这里什么也没有"];
            }
        }else{
            [_defaultView removeFromSuperview];
        }
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        if (self.blackListDataSources.count<20) {
            [self.myTableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            [self.myTableView.mj_footer resetNoMoreData];
        }
        if (self.blackListDataSources.count==20) {
            [self.myTableView.mj_footer setHidden:NO];
        }
    } failure:^(SLHttpRequestError *error) {
        [self.myTableView.mj_header endRefreshing];
    }];
}
//加载更多的黑名单，
- (void)getMoreBlackListData
{
    [self.myTableView.mj_footer setHidden:NO];
    self.page ++;
    NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld", (long)self.page],@"pager":@"20"};
    [SLAPIHelper getBlackList:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSArray *moreLists = [SLBlackListModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
        [self.blackListDataSources addObjectsFromArray:moreLists];
        // 刷新表格
        [self.myTableView reloadData];
        if ( moreLists.count <20) {
            [self.myTableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [self.myTableView.mj_footer endRefreshing];
        }
    } failure:^(SLHttpRequestError *error) {
        // 结束刷新
        [self.myTableView.mj_footer endRefreshing];
    }];
}
#pragma mark - UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.blackListDataSources.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLBlackListCell *cell=[tableView dequeueReusableCellWithIdentifier:@"BlackListCell"];
    SLBlackListModel *model=self.blackListDataSources[indexPath.row];
    cell.deletate = self;
    cell.model=model;
    cell.indexpath = indexPath;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.myTableView deselectRowAtIndexPath:indexPath animated:YES];
    SLBlackListModel *model = self.blackListDataSources[indexPath.row];
    HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
    otherPerson.userId = model.blocked[@"id"];
    [self.navigationController pushViewController:otherPerson animated:YES];
}

// 没有搜索到内容
-(void)addNotLoginViews:(NSString *)str{
    [self.view addSubview:self.defaultView];
    self.defaultView.titleStr = str;
}

// 没有网的制空页面
-(void)addNetWorkViews:(NSString *)str{
    [self.view addSubview:self.defaultView];
    self.defaultView.titleStr = str;
}
- (void)getNetWorkStatus{
    // 需要先检测网络的链接状态，如果是链接成功在往下一部测试
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            [self addNetWorkViews:@"世界上最遥远的距离就是－－妹有网"];
            [self.myTableView.mj_header setHidden:YES];
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            [self.myTableView.mj_header setHidden:NO];
            //有网络的情况
            [_defaultView  removeFromSuperview];
        }else if ([netWorkStaus isEqualToString:@"wan"] ||[netWorkStaus isEqualToString:@"Unknown"]) {
            [self.myTableView.mj_header setHidden:NO];
            //有网络的情况
            [_defaultView  removeFromSuperview];
        }
    }
}
//点击按钮的代理方法
- (void)blackListCellRemoveButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexPath{
    SLBlackListModel *model=self.blackListDataSources[indexPath.row];
    //取消拉黑或者取消拉黑该用户
    //移除IM中的黑名单
    YWPerson *person = [[YWPerson alloc] initWithPersonId:[NSString stringWithFormat:@"%@", model.blocked[@"id"]] appKey:[YWAPI sharedInstance].appKey];
    [[[SLMessageManger sharedInstance].ywIMKit.IMCore getContactService] removePersonFromBlackList:person withResultBlock:^(NSError *error, YWPerson *person) {
        BOOL isSuccess = (error == nil ||error.code == 0);
        // 提示结果
        if(isSuccess) {
            NSDictionary *parameters = @{@"id":[NSString stringWithFormat:@"%@", model.blocked[@"id"]]};
            [SLAPIHelper cancelBlack:parameters method:Delete success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                [self.blackListDataSources removeObjectAtIndex:indexPath.row];
                [self.myTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [self updateBlackList];
            } failure:^(SLHttpRequestError *error) {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"移除黑名单失败" delegate:self cancelButtonTitle:@"确认" otherButtonTitles: nil];
                [alert show];
            }];
        }else{
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"移除黑名单失败" delegate:self cancelButtonTitle:@"确认" otherButtonTitles: nil];
            [alert show];
        }
    }];
}
- (void)updateBlackList{
    if (self.blackListDataSources.count == 0) {
        [self addNotLoginViews:@"这里什么也没有"];
    }else{
        [self.myTableView reloadData];
    }
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
