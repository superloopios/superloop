//
//  SLLatistViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLLatistViewController.h"
#import <MJExtension.h>
#import <AFNetworking.h>
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "AppDelegate.h"
#import "SLLastCallViewCell.h"
#import "SLContactCell.h"
#import "SLCallModel.h"
#import "SLCallJudgeViewController.h"
#import "HomePageViewController.h"
#import "SLAPIHelper.h"
#import "SLBlackListViewController.h"
#import "SLTabBarViewController.h"
#import "SLNavgationViewController.h"
#import "SLFirstViewController.h"
#import "SLRegisterUserViewController.h"

@interface SLLatistViewController ()<SLLastCallViewCellDelegate>

@property (nonatomic, strong) NSMutableArray         *arrs;
@property (nonatomic, strong) NSMutableDictionary    *dict;
@property (nonatomic, assign) NSInteger              page;
@property (nonatomic, strong) UIImageView            *notLoginImageView;
@property (nonatomic, strong) UIView                 *loginView;
@property (nonatomic, strong) UILabel                *loginLabel;
@property (nonatomic, strong) UIButton               *loginBtn;
@property (nonatomic, strong) UILabel                *registerLabel;
@property (nonatomic, strong) UIButton               *registerBtn;

@end

@implementation SLLatistViewController
static NSString * const lastCall = @"callCell";
- (UIButton *)loginBtn{
    if (!_loginBtn) {
        _loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_loginBtn setTitle:@"登录" forState:UIControlStateNormal];
        _loginBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_loginBtn setTitleColor:UIColorFromRGB(0x00aeff) forState:UIControlStateNormal];
        [self.loginView addSubview:_loginBtn];
        [_loginBtn addTarget:self action:@selector(loginSystem) forControlEvents:UIControlEventTouchUpInside];
    }
    return _loginBtn;
}
- (UILabel *)loginLabel{
    if (!_loginLabel) {
        _loginLabel = [[UILabel alloc] init];
        _loginLabel.font = [UIFont systemFontOfSize:15];
        _loginLabel.textColor = UIColorFromRGB(0x9e9e9e);
        _loginLabel.text = @"后查看通话记录！";
        [self.loginView addSubview:_loginLabel];
    }
    return _loginLabel;
}

- (UIButton *)registerBtn{
    if (!_registerBtn) {
        _registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_registerBtn setTitle:@"去注册>>" forState:UIControlStateNormal];
        _registerBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_registerBtn setTitleColor:UIColorFromRGB(0x00aeff) forState:UIControlStateNormal];
        [self.loginView addSubview:_registerBtn];
        [_registerBtn addTarget:self action:@selector(registerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _registerBtn;
}
- (UILabel *)registerLabel{
    if (!_registerLabel) {
        _registerLabel = [[UILabel alloc] init];
        _registerLabel.font = [UIFont systemFontOfSize:15];
        _registerLabel.textColor = UIColorFromRGB(0x9e9e9e);
        _registerLabel.text = @"还没有账号？";
        [self.loginView addSubview:_registerLabel];
    }
    return _registerLabel;
}
- (UIView *)loginView{
    if (!_loginView) {
        _loginView = [[UIView alloc]initWithFrame:CGRectMake(0, 50, ScreenW , ScreenH-128.5-49-60)];
        _loginView.backgroundColor = SLColor(245, 245, 245);
        
        [self.tableView addSubview:_loginView];
    }
    return _loginView;
}

- (UIImageView *)notLoginImageView{
    if (!_notLoginImageView) {
        _notLoginImageView = [[UIImageView alloc] init];
        _notLoginImageView.image = [UIImage imageNamed:@"swan"];
        [self.loginView addSubview:_notLoginImageView];
    }
    return _notLoginImageView;
}


-(NSInteger)page{
    if (!_page) {
        _page=0;
    }
    return _page;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"SLLatistViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLLatistViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpRefresh];
    [self setUpTable];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadNewTable) name:@"isLoginSuccessToLoadData" object:nil];
    
    self.tableView.scrollsToTop=YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIImageView *footImageView = [[UIImageView alloc] init];
    footImageView.frame = CGRectMake(0, 0, self.view.width, 0.5);
    footImageView.backgroundColor=[UIColor lightGrayColor];
    self.tableView.tableFooterView = footImageView;
    self.tableView.tableFooterView.hidden=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recordRefresh) name:@CallAndContact_Refreshing object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recordRefresh) name:@"Dialing" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearAllInfo) name:@"exitSuperLoop" object:nil];
}
- (void)clearAllInfo{
    [self.arrs removeAllObjects];
    [self.tableView reloadData];
    [self.tableView.mj_footer setHidden:YES];
    [self addDefaultLoginView];
}
- (void)recordRefresh{
    if (ApplicationDelegate.userId) {
       [self.tableView.mj_header beginRefreshing];
    }
    [self.tableView.mj_footer setHidden:YES];
}

- (void)setUpRefresh
{
    self.tableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewTable)];
    
    self.tableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopics)];
    [self.tableView.mj_footer setHidden:YES];
    if (ApplicationDelegate.userId) {
        [self.tableView.mj_header beginRefreshing];
    }else{
        [self addDefaultLoginView];
    }
    
}
- (void)setUpTable{
    self.tableView.scrollIndicatorInsets = self.tableView.contentInset;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SLLastCallViewCell class]) bundle:nil] forCellReuseIdentifier:lastCall];
}
- (void)loadNewTable{
    if (ApplicationDelegate.userId) {
        [_loginView removeFromSuperview];
    }else{
        [self.tableView.mj_header endRefreshing];
        return;
    }
    
    self.page=0;
    NSDictionary *params=@{@"page":@"0",@"pager":@"20"};
   
    [SLAPIHelper callLogs:params success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        self.arrs = [SLCallModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
//        NSLog(@"arrs-------%@",data[@"result"]);
        
        [self.tableView reloadData];
        if (self.arrs.count == 0){
            if (!_nullsLoginView) {
                [self addNotLoginViews:@"暂无数据！"];
            }
        }else{
            
            [self.nullsLoginView removeFromSuperview];
        }
        if (self.arrs.count<20) {
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer setHidden:YES];
//            [self.tableView.mj_footer endRefreshingWithNoMoreData];
            
        }else{
            [self.tableView.mj_footer setHidden:NO];
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer resetNoMoreData];
        }
    } failure:^(SLHttpRequestError *error) {
        [self.tableView.mj_header endRefreshing];
    }];
}


-(void)loadMoreTopics{
    [self.tableView.mj_footer setHidden:NO];
    self.page++;
    NSDictionary *params=@{@"page":[NSString stringWithFormat:@"%ld",(long)self.page],@"pager":@"20"};
    
    [SLAPIHelper callLogs:params success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSArray *callArr = [SLCallModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
        [self.arrs addObjectsFromArray:callArr];
        
        [self.tableView reloadData];
        
        if ( callArr.count <20) {
            
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            // 结束刷新
            [self.tableView.mj_footer endRefreshing];
        }
        
    } failure:^(SLHttpRequestError *error) {
        [self.tableView.mj_footer endRefreshing];
    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.arrs.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SLLastCallViewCell *cell = [tableView dequeueReusableCellWithIdentifier:lastCall];
    if (!cell) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"SLLastCallViewCell" owner:nil options:nil] firstObject];
    }
    cell.callModel = self.arrs[indexPath.row];
    cell.indexpath = indexPath;
    cell.Delegate = self;
    return cell;
}

//选择控制器
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //当返回来的时候取消单元格的选中
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SLCallModel *model = self.arrs[indexPath.row];
    
    NSString *string1 = [NSString stringWithFormat:@"%@", ApplicationDelegate.userId];
    NSString *string2 = [NSString stringWithFormat:@"%@", model.from_user[@"id"]];
    NSString *stringID = [[NSString alloc] init];
    if ([string1 isEqualToString:string2]) {
        stringID = model.to_user[@"id"];
    }else
    {
        stringID = model.from_user[@"id"];
        
    }
    if ([model.anonymous_mode isEqual:@1] && ![string1 isEqualToString:string2]) {
        
    }else{
        SLTabBarViewController *tabbarVc = (SLTabBarViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
        UINavigationController *selectedController = tabbarVc.selectedViewController;
        HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
        otherPerson.userId = stringID;
        
        [selectedController pushViewController:otherPerson animated:YES];
    }
    NSLog(@"%@",[UIApplication sharedApplication].keyWindow.rootViewController);
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 68;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0) {
        return 0.1;
    }
    return 0;
}


- (void)didJudgeButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath
{
    SLCallJudgeViewController *callJudgeView = [[SLCallJudgeViewController alloc] init];
    SLCallModel *model = self.arrs[indexpath.row];
    __weak typeof(self) weakSelf = self;
    callJudgeView.hasRatedBlock=^(BOOL hasRated){
        if (hasRated) {
            model.has_rated = 1;
            [weakSelf.tableView reloadData];
            if ([model.anonymous_mode isEqual:@1]) {
                [button setTitle:@"已匿名评价" forState:UIControlStateNormal];
            }else{
                [button setTitle:@"已评价" forState:UIControlStateNormal];
            }
            
            button.layer.borderColor = [UIColor whiteColor].CGColor;
            [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            button.enabled=NO;
        }
    };
    //    //当返回来的时候取消单元格的选中
    //    if (button.tag == 1000) {
    //
    //        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请勿重复评价" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
    //        [alertView show];
    //
    //    }else
    //    {
    [self.tableView deselectRowAtIndexPath:indexpath animated:YES];
    //        SLCallJudgeViewController *callJudgeView = [[SLCallJudgeViewController alloc] init];
    callJudgeView.callId = model.id;
    callJudgeView.isCallingJudge=YES;
    SLTabBarViewController *tabbarVc = (SLTabBarViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    UINavigationController *selectedController = tabbarVc.selectedViewController;
    [selectedController pushViewController:callJudgeView animated:YES];
    
    //    }
    
}


// 通讯录没有人
-(void)addNotLoginViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW , ScreenH )];
    
    UILabel *tiplabel = [[UILabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5 - 20, 142,tipLabelSize.width +20, 20);
    if (ScreenH == 200) {
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5 - 20, 62,tipLabelSize.width + 20, 20);
    }
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    tiplabel.numberOfLines = 0;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    //    [AttributedStr addAttribute:NSForegroundColorAttributeName
    //
    //                          value:SLColor(91, 133, 189)
    //
    //                          range:NSMakeRange(2, 2)];
    
    tiplabel.attributedText = AttributedStr;
    
    
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76 - 69 + 10, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    _nullsLoginView  = contentView;
    [self.view addSubview:contentView];
    
}
- (void)addDefaultLoginView{
    [self.tableView addSubview:self.loginView];
    self.notLoginImageView.frame = CGRectMake(ScreenW *0.5-96, 33, 192*picWScale, 192*picWScale);
    
    CGFloat loginLabelWidth = [@"登录后查看通话记录！" boundingRectWithSize:CGSizeMake(screen_W, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size.width+3;
    CGFloat loginBtnWidth = [@"登录" boundingRectWithSize:CGSizeMake(screen_W, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size.width+3;
    CGFloat leftMargin = (screen_W-loginLabelWidth)/2.0;
    [self.loginBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(leftMargin);
        make.top.equalTo(self.notLoginImageView.mas_bottom).offset(39);
        make.width.equalTo(@(loginBtnWidth));
        make.height.equalTo(@20);
    }];
    [self.loginLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.loginBtn.mas_right);
        make.top.equalTo(self.loginBtn);
        make.height.equalTo(@20);
    }];
    
    CGFloat registerLabelWidth = [@"还没有账号？去注册>>" boundingRectWithSize:CGSizeMake(screen_W, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size.width+3;
    CGFloat registerBtnWidth = [@"去注册>>" boundingRectWithSize:CGSizeMake(screen_W, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size.width+3;
    CGFloat registerLeftMargin = (screen_W-registerLabelWidth)/2.0;
    [self.registerLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(registerLeftMargin);
        make.top.equalTo(self.loginLabel.mas_bottom).offset(9);
        make.height.equalTo(@20);
    }];
    [self.registerBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.registerLabel.mas_right);
        make.top.equalTo(self.registerLabel);
        make.width.equalTo(@(registerBtnWidth));
        make.height.equalTo(@20);
    }];
}

- (void)loginSystem{
    SLFirstViewController *vc=[[SLFirstViewController alloc] init];
    SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
    //    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}
- (void)registerBtnClick{
    SLRegisterUserViewController *vc=[[SLRegisterUserViewController alloc] init];
    SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
    //    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}
@end
