//
//  SLContactViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLContactViewController.h"
#import "SLAddressViewController.h"
#import "SLLatistViewController.h"
#import "SLCallJudgeViewController.h"
#import "SLSegmentedControl.h"
#import "SLBlackListViewController.h"
#import "SLFirstViewController.h"
#import "SLNavgationViewController.h"
#import "UMSocial.h"
#import "AppDelegate.h"

@interface SLContactViewController ()<SLSegmentControlDelegate,UIScrollViewDelegate,UIAlertViewDelegate>
/** 用来显示所有子控制器view的scrollView */
@property (nonatomic,strong) SLAddressViewController *adressVc;
@property (nonatomic,strong) SLLatistViewController *lastVc;
@property (nonatomic,strong) UIButton *btnLeft;
@property (nonatomic,strong) UIButton *btnRight;
@property (nonatomic,strong) UIView *views;
@property (nonatomic,strong) UILabel *labLeft;
@property (nonatomic,strong) UILabel *labRight;
@property (nonatomic,strong) UIButton *btn;
@property (nonatomic,strong) UIButton *btnOther;
@property (nonatomic,strong) UIView *blackListView;
@property (nonatomic,strong) UISegmentedControl *segControl;
@property (nonatomic,strong) SLSegmentedControl *seg;
@property (nonatomic,strong) UIScrollView *scrollview;
@property (nonatomic,assign) CGFloat lastContentOffset;
@end

@implementation SLContactViewController

- (SLSegmentedControl *)seg{
    if (!_seg) {
        _seg = [[SLSegmentedControl alloc] initWithFrame:CGRectMake(60, 20, screen_W-120, 43.5)];
        _seg.delegate = self;
        _seg.titleArr = @[@"超人",@"通话记录"];
    }
    return _seg;
}
- (UIScrollView *)scrollview{
    if (!_scrollview) {
        _scrollview = [[UIScrollView alloc] init];
        _scrollview.frame = CGRectMake(0, 64, screen_W, screen_H-113);
        _scrollview.pagingEnabled = YES;
        _scrollview.delegate = self;
        [_scrollview setContentSize:CGSizeMake(screen_W*2, 0)];
        _scrollview.showsHorizontalScrollIndicator = NO;
        _scrollview.bounces = NO;
        _scrollview.delegate = self;
        _scrollview.scrollsToTop=NO;
        [self.view addSubview:_scrollview];
    }
    return _scrollview;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //[self.navigationController setNavigationBarHidden:NO animated:NO];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.view.backgroundColor = SLColor(244, 244, 244);
    [self setUpNav];

    self.adressVc = [[SLAddressViewController alloc] init];
    self.lastVc = [[SLLatistViewController alloc] init];
    [self addChildViewController:self.adressVc];
    [self addChildViewController:self.lastVc];
    [self addBlacbtn];
    self.adressVc.view.frame = CGRectMake(0, 0, screen_W, screen_H-113);
    self.lastVc.view.frame = CGRectMake(screen_W, 0, screen_W, screen_H-113);
    [self.scrollview addSubview:self.adressVc.view];
    [self.scrollview addSubview:self.lastVc.view];
    //并且在第一次的时候自动点击选中按钮
    //[self selectIndexSegControl:self.segControl];
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"post0" object:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadCallData) name:@"complainSuccess" object:nil];
    
   
}

- (void)reloadCallData{
    [self.lastVc.tableView.mj_header beginRefreshing];
    [self.lastVc.tableView.mj_footer setHidden:YES];
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    [self.view addSubview:self.seg];
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
}
- (void)addBlacbtn{
    
    UIView *headerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 128.5)];
    headerView.backgroundColor=[UIColor whiteColor];
    
    //邀请微信好友
    UIView *inviteWeChat = [[UIView alloc] initWithFrame:CGRectMake(0,0, ScreenW, 64)];
    inviteWeChat.backgroundColor=[UIColor whiteColor];
    inviteWeChat.userInteractionEnabled=YES;
    UITapGestureRecognizer *inviteWeChatTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(inviteWeChatListBtnClick)];
    [inviteWeChat addGestureRecognizer:inviteWeChatTap];
    [headerView addSubview:inviteWeChat];
    
    UIImageView *inviteWeChatImgView=[[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 44, 44)];
    inviteWeChatImgView.image=[UIImage imageNamed:@"inviteFriend"];
    [inviteWeChat addSubview:inviteWeChatImgView];
    
    UILabel *inviteWeChatListLab=[[UILabel alloc] initWithFrame:CGRectMake(70, 10, ScreenW-70, 44)];
    inviteWeChatListLab.text=@"邀请微信好友";
    inviteWeChatListLab.font=[UIFont systemFontOfSize:17];
    inviteWeChatListLab.textAlignment=NSTextAlignmentLeft;
    [inviteWeChat addSubview:inviteWeChatListLab];

    UIImageView *lineImgView=[[UIImageView alloc] initWithFrame:CGRectMake(70, 64, ScreenW-70-15, 0.5)];
    lineImgView.backgroundColor=SLColor(207, 207, 207);;
    [inviteWeChat addSubview:lineImgView];
    
    //黑名单
    _blackListView=[[UIView alloc] initWithFrame:CGRectMake(0, 64.5, ScreenW, 64)];
    _blackListView.backgroundColor=[UIColor whiteColor];
    _blackListView.userInteractionEnabled=YES;
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blackListBtnClick)];
    [_blackListView addGestureRecognizer:tap];
    [headerView addSubview:_blackListView];
    
    UIImageView *blackImgView=[[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 44, 44)];
    blackImgView.image=[UIImage imageNamed:@"blackList"];
    [_blackListView addSubview:blackImgView];
    
    UILabel *blackListLab=[[UILabel alloc] initWithFrame:CGRectMake(70, 10, ScreenW-70, 44)];
    blackListLab.text=@"黑名单";
    blackListLab.font=[UIFont systemFontOfSize:17];
    blackListLab.textAlignment=NSTextAlignmentLeft;
    [_blackListView addSubview:blackListLab];

    UIImageView *blackLineImgView=[[UIImageView alloc] initWithFrame:CGRectMake(70, 63.5, ScreenW-70-15, 0.5)];
    blackLineImgView.backgroundColor=SLColor(207, 207, 207);;
    [_blackListView addSubview:blackLineImgView];
    
     self.adressVc.tableView.tableHeaderView=headerView;
}
-(void)blackListBtnClick{
    if (!ApplicationDelegate.userId) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
        //        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [alert show];
        return;
    }
    SLBlackListViewController *vc=[[SLBlackListViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)inviteWeChatListBtnClick{
    NSString *titleStr = @"我正在玩超级圈APP，无数“超人”在等你！知识经验和人脉资源共享的超级平台，真实信息和价值体面变现的生态社交。http://dwz.cn/superquan";
    //NSString *urlStr = @"http://h5.superloop.com.cn/appdown/download.html";
    [UMSocialData defaultData].extConfig.title = titleStr;
    //[UMSocialData defaultData].extConfig.wechatSessionData.url = urlStr;
    UMSocialUrlResource *urlResource = [[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:
                                        nil];
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatSession] content:titleStr image:nil location:nil urlResource:urlResource presentedController:self completion:^(UMSocialResponseEntity *shareResponse){
        if (shareResponse.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");
        }
    }];
}

- (void)slSegmentControlClickedButton:(UIButton *)btn{
    if (btn.tag==0) {
        self.adressVc.tableView.scrollsToTop=YES;
        self.lastVc.tableView.scrollsToTop=NO;
    }else{
        self.adressVc.tableView.scrollsToTop=NO;
        self.lastVc.tableView.scrollsToTop=YES;
    }
    [self.scrollview setContentOffset:CGPointMake(screen_W*btn.tag, 0) animated:YES];
    self.lastContentOffset = self.scrollview.contentOffset.x;
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.lastContentOffset = scrollView.contentOffset.x;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat destWidth;
    CGFloat currentWidth = [self.seg.titleWidthArr[self.seg.selectedSegmentIndex] doubleValue]+4;
    
    if ((scrollView.contentOffset.x - self.lastContentOffset)>0.0) {
        NSInteger index = self.seg.selectedSegmentIndex+1;
        if(index<self.seg.titleWidthArr.count){
            destWidth = [self.seg.titleWidthArr[index] doubleValue]+4;
            self.seg.titleBottomView.width = (scrollView.contentOffset.x-self.lastContentOffset)/screen_W*(destWidth-currentWidth)+currentWidth;
        }
        
    }else{
        NSInteger index = self.seg.selectedSegmentIndex-1;
        if(index>-1){
            destWidth = [self.seg.titleWidthArr[index] doubleValue]+4;
            self.seg.titleBottomView.width = (self.lastContentOffset-scrollView.contentOffset.x)/screen_W*(destWidth-currentWidth)+currentWidth;
        }
    }
    self.seg.titleBottomView.centerX = self.seg.startLoc+scrollView.contentOffset.x/screen_W*((ScreenW-120)/2);
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int index = self.scrollview.contentOffset.x / self.view.width;
    [self.seg setSegmentControlIndex:index];
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [self scrollViewDidEndDecelerating:scrollView];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        [self loginSystem];
    }
}
- (void)loginSystem{
    SLFirstViewController *vc=[[SLFirstViewController alloc] init];
    SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
    //    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

@end
