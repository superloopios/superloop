//
//  SLCallJudgeViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/19.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCallJudgeViewController.h"
#import <Masonry.h>
#import "SLTextView.h"
#import <AFNetworking.h>
#import "AppDelegate.h"
#import <MBProgressHUD.h>
#import "SLLatistViewController.h"
#import "SLAPIHelper.h"
#import "SLComplainViewController.h"
#import "SLEvaluateTextView.h"
#import "NSString+Utils.h"

@interface SLCallJudgeViewController ()<UITextViewDelegate>
@property (nonatomic, strong) UIView *starView;
@property (nonatomic, assign) NSNumber *scrore;
@property (nonatomic, strong) SLEvaluateTextView *textView;
@property (nonatomic,assign) BOOL isTouchStar;
@property (nonatomic,strong) UIButton *uploadBtn;
@end

@implementation SLCallJudgeViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUpNav];
    [self setUpUI];
    self.isTouchStar = NO;
    //收起键盘
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到view上
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"评价";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIButton *uploadBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    uploadBtn.frame=CGRectMake(ScreenW-60, 27, 60, 30);
    [uploadBtn setTitle:@"提交" forState:UIControlStateNormal];
    uploadBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [uploadBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [uploadBtn addTarget:self action:@selector(sendJudge) forControlEvents:UIControlEventTouchUpInside];
    self.uploadBtn = uploadBtn;
    [navView addSubview:uploadBtn];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)setUpUI{
    
    UIView *firstView = [[UIView alloc] init];
    firstView.backgroundColor = UIColorFromRGB(0xf4f4f4);
    [self.view addSubview:firstView];
    [firstView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(64);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(8);
    }];
    
    UIView *topLeftLineView = [[UIView alloc] init];
    topLeftLineView.backgroundColor = UIColorFromRGB(0xb6b6b6);
    [self.view addSubview:topLeftLineView];
    [topLeftLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(64+35+8);
        make.left.mas_equalTo(self.view.mas_left);
        make.height.mas_equalTo(1);
        make.width.mas_equalTo((ScreenW - 130)/2);
    }];
    
    UIView *topRightLineView = [[UIView alloc] init];
    topRightLineView.backgroundColor = UIColorFromRGB(0xb6b6b6);
    [self.view addSubview:topRightLineView];
    [topRightLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(64+35+8);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(1);
        make.width.mas_equalTo((ScreenW - 130)/2);
    }];
    
    UILabel *starLab = [[UILabel alloc] init];
    starLab.text = @"选择评价星级";
    starLab.textColor = UIColorFromRGB(0x5d5d5d);
    starLab.font = [UIFont systemFontOfSize:14];
    starLab.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:starLab];
    [starLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(64+28+8);
        make.left.mas_equalTo(self.view.mas_left).offset((ScreenW - 130)/2+15);
        make.height.mas_equalTo(14);
        make.width.mas_equalTo(100);
    }];
    
    UIView *starView = [[UIView alloc] init];
    self.starView = starView;
    [self.view addSubview:starView];
    [starView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topLeftLineView.mas_bottom).offset(22);
        make.height.mas_equalTo(28);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.width.mas_equalTo(178);
    }];
    /*****************添加星星************************/
    for (int i = 0; i < 5; i++) {
        UIImageView *imageVeiw = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"colorCredit_level"]];
        imageVeiw.frame = CGRectMake((7 + 30) * i, 0, 30, 28);
        [starView addSubview:imageVeiw];
    }
    
    UIView *bottomLeftLineView = [[UIView alloc] init];
    bottomLeftLineView.backgroundColor = UIColorFromRGB(0xb6b6b6);
    [self.view addSubview:bottomLeftLineView];
    [bottomLeftLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(starView.mas_bottom).offset(37);
        make.left.mas_equalTo(self.view.mas_left);
        make.height.mas_equalTo(1);
        make.width.mas_equalTo((ScreenW - 130)/2);
    }];
    
    UIView *bottomRightLineView = [[UIView alloc] init];
    bottomRightLineView.backgroundColor = UIColorFromRGB(0xb6b6b6);
    [self.view addSubview:bottomRightLineView];
    [bottomRightLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(starView.mas_bottom).offset(37);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(1);
        make.width.mas_equalTo((ScreenW - 130)/2);
    }];
    
    UILabel *evaluateLab = [[UILabel alloc] init];
    evaluateLab.text = @"输入评价内容";
    evaluateLab.textColor = UIColorFromRGB(0x5d5d5d);
    evaluateLab.font = [UIFont systemFontOfSize:14];
    evaluateLab.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:evaluateLab];
    [evaluateLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(starView.mas_bottom).offset(31);
        make.left.mas_equalTo(self.view.mas_left).offset((ScreenW - 130)/2+15);
        make.height.mas_equalTo(14);
        make.width.mas_equalTo(100);
    }];
    
    
    
    SLEvaluateTextView *textView = [[SLEvaluateTextView alloc] init];
    textView.backgroundColor = UIColorFromRGB(0xf7f7f7);
    textView.alwaysBounceVertical = YES;
    textView.delegate = self;
    textView.placeholder = @"给点建议，帮助大家参考";
    textView.placeholderColor = UIColorFromRGB(0x8d8b8b);
    textView.layer.masksToBounds = YES;
    textView.layer.cornerRadius = 5;
    textView.layer.borderWidth = 0.5;
    textView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.textView = textView;
    [self.view addSubview:textView];
    [textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bottomRightLineView.mas_bottom).offset(27);
        make.left.mas_equalTo(self.view.mas_left).offset(12);
        make.right.mas_equalTo(self.view.mas_right).offset(-12);
        make.height.mas_equalTo(150);
    }];
    
    if (!self.isFromMsg) {
        UIButton * complainBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        complainBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [complainBtn setTitle:@"对本次通话不满意希望退款？去申诉>>" forState:UIControlStateNormal];
        [complainBtn setTitleColor:UIColorFromRGB(0x0079d6) forState:UIControlStateNormal];
        [complainBtn addTarget:self action:@selector(complainBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:complainBtn];
        [complainBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(textView.mas_bottom).offset(28);
            make.left.mas_equalTo(self.view.mas_left).offset(12);
            make.right.mas_equalTo(self.view.mas_right).offset(-12);
            make.height.mas_equalTo(30);
        }];
    }
}

-(void)complainBtnClick{
    SLComplainViewController *vc=[[SLComplainViewController alloc] init];
    vc.callId=self.callId;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)sendJudge{
    self.uploadBtn.enabled=NO;
    
    // 文字长度判断
    if ([NSString isEmptyStrings:self.textView.text]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"评论不能为空且不能大于1000字符" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        self.uploadBtn.enabled=YES;
        return;
    }
    if (self.textView.text.length<1||self.textView.text.length>1000) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"评论不能为空且不能大于1000字符" delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil, nil];
        [alert show];
        self.uploadBtn.enabled=YES;
        return;
    }
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow withText:@""];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    if (!self.isTouchStar) {
        [parameter setObject:@(5) forKey:@"rate"];
    }else{
        [parameter setObject:self.scrore forKey:@"rate"];
    }
    [parameter setObject:self.textView.text forKey:@"content"];
    
    if (self.isFromMsg) {//付费消息评价
        [parameter setObject:self.msgId forKey:@"messageId"];
        [SLAPIHelper msgEvaluate:parameter success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            [HUDManager hideHUDView];
            if (self.msgSuccessBlock) {
                self.msgSuccessBlock(YES);
            }
            [HUDManager showWarningWithText:@"评价成功"];
            
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^(SLHttpRequestError *failure) {
            [HUDManager hideHUDView];
            self.uploadBtn.enabled=YES;
            [HUDManager showWarningWithText:@"评价失败"];
            
        }];
    }else{//打电话评价
        [parameter setObject:@([self.callId integerValue]) forKey:@"callId"];
        [SLAPIHelper commentForCalled:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            [HUDManager hideHUDView];
            if (self.isCallingJudge) {
                //返回到通话记录页面
                self.hasRatedBlock(YES);
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                //返回到挂断电话详情页面
                if (self.CallingBlock) {
                    self.CallingBlock(YES);
                }
                [HUDManager showWarningWithText:@"评价成功"];
                [self.navigationController popToRootViewControllerAnimated:YES];
                //                    [self dismissViewControllerAnimated:YES completion:nil];
            }
        } failure:^(SLHttpRequestError *error) {
            [HUDManager hideHUDView];
            self.uploadBtn.enabled=YES;
            [HUDManager showWarningWithText:@"评价失败"];
        }];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    self.isTouchStar=YES;
    _scrore = @5;
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:_starView];
    for (UIImageView *imageView in _starView.subviews) {
        if ((touchPoint.x > 0)&&(touchPoint.x < 178+10)&&(touchPoint.y > 0)&&(touchPoint.y < 28+10)) {
            NSString *myscore = [NSString stringWithFormat:@"%i",((int)touchPoint.x)/(30+7)+1];
            NSLog(@"myscore-------%@",myscore);
            
            _scrore = @([myscore integerValue]);
            if (imageView.x < touchPoint.x) {
                imageView.image =[UIImage imageNamed:@"colorCredit_level"];
            }else{
                imageView.image =[UIImage imageNamed:@"credit_level"];
            }
        }
    }
}

-(void)keyboardHide:(UITapGestureRecognizer*)tap{
    [_textView resignFirstResponder];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [HUDManager hideHUDView];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
