//
//  SLComplainViewController.m
//  Superloop
//
//  Created by WangS on 16/5/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLComplainViewController.h"
#import "AppDelegate.h"
#import "SLTextView.h"
#import "SLAPIHelper.h"
#import "NSString+Utils.h"

@interface SLComplainViewController ()<UITextViewDelegate>
@property (nonatomic,strong)SLTextView *complainTextView;
@property (nonatomic,strong)UIButton *submitBtn;
@end

@implementation SLComplainViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //[self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    [self setUpNav];
    [self addUI];
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"申诉";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    _submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    [_submitBtn setTitle:@"提交" forState:UIControlStateNormal];
    _submitBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_submitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(submitBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:_submitBtn];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}

-(void)addUI
{
    SLTextView *complainTextView = [[SLTextView alloc] init];
    complainTextView.alwaysBounceVertical = YES;
    complainTextView.delegate = self;
    complainTextView.placeholder = @"请填写申诉内容";
    self.complainTextView = complainTextView;
    [self.view addSubview:complainTextView];
    
    
    [complainTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(64);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom);
    }];
    
}

-(void)submitBtnClick{
    _submitBtn.enabled=NO;
    // 文字长度判断
    if ([NSString isEmptyStrings:self.complainTextView.text]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"申诉内容不能为空且不能大于1000字符" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        _submitBtn.enabled=YES;
        return;
    }
    
    if (self.complainTextView.text.length<1||self.complainTextView.text.length>1000) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"申诉内容不能为空且不能大于1000字符" delegate:self cancelButtonTitle:@"确认  " otherButtonTitles:nil, nil];
        [alert show];
        _submitBtn.enabled=YES;
        return;
    }
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow withText:@""];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    [parameter setObject:self.complainTextView.text forKey:@"content"];
    if (self.isFromMsg) {
        [parameter setObject:self.msgId forKey:@"messageId"];
        [SLAPIHelper msgComplain:parameter success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            [HUDManager hideHUDView];
            if (self.msgComplainSuccessBlock) {
                self.msgComplainSuccessBlock(YES);
            }
            [HUDManager showWarningWithText:@"申诉成功"];
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^(SLHttpRequestError *failure) {
            [HUDManager hideHUDView];
            _submitBtn.enabled=YES;
            [HUDManager showWarningWithText:@"申诉失败"];
        }];
    }else{
        [parameter setObject:@([_callId integerValue]) forKey:@"callId"];
        [SLAPIHelper complain:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            [HUDManager hideHUDView];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"complainSuccess" object:nil userInfo:nil];
            [HUDManager showWarningWithText:@"申诉成功"];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } failure:^(SLHttpRequestError *error) {
            [HUDManager hideHUDView];
            _submitBtn.enabled=YES;
            [HUDManager showWarningWithText:@"申诉失败"];
            //                [HUDManager showWarningWithText:@"申诉失败,你已成功申诉过此次通话"];
        }];
    }
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [HUDManager hideHUDView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
