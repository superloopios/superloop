//
//  SLCallJudgeViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/19.
//  Copyright © 2016年 Superloop. All rights reserved.
//   打电话的评价

#import <UIKit/UIKit.h>
@interface SLCallJudgeViewController : UIViewController
@property (nonatomic, strong) NSString *callId;

@property (nonatomic,copy)void (^hasRatedBlock)(BOOL isHasRated);

@property (nonatomic,assign)BOOL isCallingJudge;

@property (nonatomic,copy)void(^CallingBlock)(BOOL isCalling);

@property (nonatomic,copy) void (^msgSuccessBlock) (BOOL isSuccess);//付费消息评价
@property (nonatomic,assign) BOOL isFromMsg;//判断是否来自付费消息
@property (nonatomic, copy) NSString *msgId;//付费消息ID

@end
