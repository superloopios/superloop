//
//  SLCallingViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/27.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface SLCallingViewController : BaseViewController

@property (nonatomic, strong) NSDictionary *user;

@end
