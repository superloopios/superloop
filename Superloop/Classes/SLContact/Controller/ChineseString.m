//
//  ChineseString.m
//  https://github.com/c6357/YUChineseSorting
//
//  Created by BruceYu on 15/4/19.
//  Copyright (c) 2015年 BruceYu. All rights reserved.
//

#import "ChineseString.h"

@implementation ChineseString
@synthesize string;
@synthesize pinYin;

#pragma mark - 返回tableview右方 indexArray
+(NSMutableArray*)IndexArray:(NSArray*)stringArr
{
    NSMutableArray *tempArray = [self ReturnSortChineseArrar:stringArr];
    NSMutableArray *A_Result = [NSMutableArray array];
    NSString *tempString ;
    
    for (NSString* object in tempArray)
    {
        NSString *pinyin = [((ChineseString*)object).pinYin substringToIndex:1];
        if ([pinyin isEqualToString:@"|"]) {
            pinyin=@"#";
        }
        //不同
        if(![tempString isEqualToString:pinyin])
        {
            [A_Result addObject:pinyin];
            tempString = pinyin;
        }
        
    }
    return A_Result;
}

#pragma mark - 返回联系人
+(NSMutableArray*)LetterSortArray:(NSArray*)stringArr
{
    NSMutableArray *tempArray = [self ReturnSortChineseArrarModel:(NSMutableArray *)stringArr];
    NSMutableArray *LetterResult = [NSMutableArray array];
    NSMutableArray *item = [NSMutableArray array];
    NSString *tempString;
    //拼音分组
    for (NSDictionary* object in tempArray) {
        NSString *charStr=[ChineseString RemoveSpecialCharacter:object[@"nickname"]];
        NSDictionary *dict=object;
        //不同
        if(![tempString isEqualToString:charStr]){
            //分组
            item = [NSMutableArray array];
            [item  addObject:dict];
            [LetterResult addObject:item];
            //遍历
            tempString = charStr;
        }else  //相同
        {
            [item  addObject:dict];
        }
    }
    return LetterResult;
}

+(char)ReturnChar:(NSDictionary *)dict{
    NSString *modelStr=[ChineseString RemoveSpecialCharacter:dict[@"nickname"]];
    return [modelStr characterAtIndex:0];
}
//返回模型
+(NSMutableArray*)ReturnSortChineseArrarModel:(NSMutableArray*)modelDictArr{
    NSMutableArray *modelArr=[NSMutableArray new];
    [modelArr addObjectsFromArray:modelDictArr];
    [ChineseString quickSort:modelArr];
//    for(int i=0;i<modelArr.count;i++){
//        for (int j=i+1; j<modelArr.count; j++) {
//            if ([ChineseString ReturnChar:modelArr[i]]>[ChineseString ReturnChar:modelArr[j]]) {
//                NSDictionary *dict=modelArr[i];
//                modelArr[i]=modelArr[j];
//                modelArr[j]=dict;
//            }
//        }
//    }
    return  modelArr;
}
+(void)quickSort:(NSMutableArray *)list{
    if (list.count <= 1) {
        return;
    }
    [self quickSort:list startIndex:0 endIndex:list.count-1];
}
+(void)quickSort:(NSMutableArray *)list startIndex:(NSInteger)start endIndex:(NSInteger)end{
    if (start >= end) { //低位大于高位，排序结束
        return;
    }
    NSInteger low = start;
    NSInteger high = end;
    NSInteger key = [ChineseString ReturnChar:list[start]];//取第一个数作为关键数据
    while (low < high) {
        
        //从后往前推，直到找到第一个比关键数据小的值
        while ([ChineseString ReturnChar:list[high]] >= key && low < high) {

            high--;
        }
        //将这个值与关键数据对调（关键数据处于low位置），对调完关键数据处于high位置
        [list exchangeObjectAtIndex:low withObjectAtIndex:high];
        
        //从前往后推，直到找到第一个比关键数据大的值
        while ([ChineseString ReturnChar:list[low]] <= key && low < high) {

            low++;
        }
        //将这个值与关键数据（关键数据已经处于high位置）对调，对调完关键数据处于low位置
        [list exchangeObjectAtIndex:high withObjectAtIndex:low];
    }
    //对关键数据前面的数据进行快速排序
    [self quickSort:list startIndex:start endIndex:low-1];
    //对关键数据后面的数据进行快速排序
    [self quickSort:list startIndex:low+1 endIndex:end];
}

//返回排序好的字符拼音
+(NSMutableArray*)ReturnSortChineseArrar:(NSArray*)stringArr
{
    //获取字符串中文字的拼音首字母并与字符串共同存放
    NSMutableArray *chineseStringsArray=[NSMutableArray array];
    for(int i=0;i<[stringArr count];i++)
    {
        ChineseString *chineseString = [[ChineseString alloc]init];
        chineseString.string = [NSString stringWithString:[stringArr objectAtIndex:i][@"nickname"]];
        if(chineseString.string == nil){
            chineseString.string = @"";
        }
        //去除两端空格和回车
        chineseString.string  = [chineseString.string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        //这里我自己写了一个递归过滤指定字符串   RemoveSpecialCharacter
        chineseString.string = [ChineseString RemoveSpecialCharacter:chineseString.string];
        
        //判断首字符是否为字母
        NSString *regex = @"[A-Za-z]+";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
        NSString *initialStr = [chineseString.string length]?[chineseString.string substringToIndex:1]:@"";
        if ([predicate evaluateWithObject:initialStr])
        {
            //首字母大写
            chineseString.pinYin = [chineseString.string capitalizedString] ;
        }else{
            //            if(![chineseString.string isEqualToString:@""]){
            //                NSString *pinYinResult = [NSString string];
            //                for(int j=0;j<chineseString.string.length;j++){
            //                    NSString *singlePinyinLetter = [[NSString stringWithFormat:@"%c",
            //
            //                                                   pinyinFirstLetter([chineseString.string characterAtIndex:j])]uppercaseString];
            //                    pinYinResult = [pinYinResult stringByAppendingString:singlePinyinLetter];
            //                }
            //                chineseString.pinYin = pinYinResult;
            //            }else{
            chineseString.pinYin = @"|";
            //            }
        }
        [chineseStringsArray addObject:chineseString];
    }
    //按照拼音首字母对这些Strings进行排序
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"pinYin" ascending:YES]];
    [chineseStringsArray sortUsingDescriptors:sortDescriptors];
    
    return chineseStringsArray;
}
+(NSString*)RemoveSpecialCharacter: (NSString *)aString {
    //转成了可变字符串
    NSMutableString *str = [NSMutableString stringWithString:aString];
    //先转换为带声调的拼音
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformMandarinLatin,NO);
    //再转换为不带声调的拼音
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformStripDiacritics,NO);
    //转化为大写拼音
    NSString *pinYin = [str capitalizedString];
    char c=[pinYin characterAtIndex:0];
    //获取并返回首字母
    if (!isalpha(c)) {
        return @"|";
    }else{
        return [pinYin substringToIndex:1];
    }
}
@end