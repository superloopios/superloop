//
//  SLComplainViewController.h
//  Superloop
//
//  Created by WangS on 16/5/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLComplainViewController : UIViewController

@property (nonatomic, strong) NSString *callId;
//@property (nonatomic, copy) void (^fromCallingBlock)(BOOL isSucess);

@property (nonatomic,copy) void (^msgComplainSuccessBlock)(BOOL isSuccess);//付费消息申诉
@property (nonatomic, copy) NSString *msgId;//付费消息ID
@property (nonatomic,assign) BOOL isFromMsg;//判断是否来自付费消息

@end
