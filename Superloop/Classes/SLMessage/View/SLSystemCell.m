//
//  SLSystemCell.m
//  Superloop
//
//  Created by WangJiwei on 16/4/22.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSystemCell.h"

@implementation SLSystemCell

- (void)awakeFromNib {
    [super awakeFromNib];
   
}
-(void)layoutSubviews{
    [super layoutSubviews];
    [self.lineView setConstant:0.5];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
