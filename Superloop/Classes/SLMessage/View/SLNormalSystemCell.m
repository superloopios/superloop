//
//  SLNormalSystemCell.m
//  Superloop
//
//  Created by xiaowu on 16/9/5.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLNormalSystemCell.h"
#import "SLSystemMessageModel.h"
#import "NSString+Utils.h"

@interface SLNormalSystemCell ()

@property(nonatomic, strong)UILabel *contentLable;
@property(nonatomic, strong)UILabel *reasonLabel;

@end

@implementation SLNormalSystemCell



- (UILabel *)contentLable{
    if (!_contentLable) {
        _contentLable = [[UILabel alloc] init];
        _contentLable.font = [UIFont systemFontOfSize:14];
        _contentLable.textColor = [UIColor blackColor];
        _contentLable.numberOfLines = 0;
        [self.myContentView addSubview:_contentLable];
        
    }
    return _contentLable;
}

- (UILabel *)reasonLabel{
    if (!_reasonLabel) {
        _reasonLabel = [[UILabel alloc] init];
        _reasonLabel.font = [UIFont systemFontOfSize:14];
        _reasonLabel.textColor = [UIColor blackColor];
        _reasonLabel.numberOfLines = 0;
        [self.myContentView addSubview:_reasonLabel];
        
        [_reasonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(15);
            make.top.equalTo(self.contentLable.mas_bottom).offset(20);
            make.width.equalTo(@(screen_W-52));
        }];
        
    }
    return _reasonLabel;
}


- (void)setModel:(SLSystemMessageModel *)model{
    [super setModel:model];
    self.contentLable.text = model.msgcontent[@"content"];
    if ([NSString isRealString:model.msgcontent[@"content"]]) {
        [self.contentLable mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.subTitleLabel.mas_bottom).offset(8);
            make.left.equalTo(self.subTitleLabel);
            make.width.equalTo(@(screen_W-52));
        }];
    }else{
        [self.contentLable mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(15);
            make.top.equalTo(self.titleLabel.mas_bottom).offset(20);
            make.width.equalTo(@(screen_W-52));
        }];
    }
//    self.topicTitleLable.text = model.topicTitle;
    [self setLineSpacing:5 label:self.contentLable];
    self.reasonLabel.text = model.msgcontent[@"memo"];
    [self setLineSpacing:5 label:self.reasonLabel];
//    self.resultLabel.text = @"如有任何疑问,请联系超级圈客服";
}

- (void)setLineSpacing:(CGFloat)spacing label:(UILabel *)label{
    if (label.text.length>0) {
        NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:label.text];
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:spacing];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [label.text length])];
        [label setAttributedText:attributedString];
        [label sizeToFit];
    }
}

@end
