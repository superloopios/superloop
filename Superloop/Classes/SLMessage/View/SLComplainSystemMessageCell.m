//
//  SLComplainSystemMessageCell.m
//  Superloop
//
//  Created by administrator on 16/7/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLComplainSystemMessageCell.h"
#import "SLSystemMessageModel.h"

@interface SLComplainSystemMessageCell ()
//显示对方用户:的lab
@property(nonatomic, strong)UILabel *nameLable;
@property(nonatomic, strong)UILabel *nickLabel;
//日期
@property(nonatomic, strong)UILabel *riqiLable;
@property(nonatomic, strong)UILabel *dateLabel;

//通话时长
@property(nonatomic, strong)UILabel *timeStrLable;
@property(nonatomic, strong)UILabel *timeLabel;
//理由
@property(nonatomic, strong)UILabel *reasonStrLable;
@property(nonatomic, strong)UILabel *reasonLabel;

@property(nonatomic, strong)UILabel *resultLabel;

@end

@implementation SLComplainSystemMessageCell

- (UILabel *)resultLabel{
    if (!_resultLabel) {
        _resultLabel = [[UILabel alloc] init];
        _resultLabel.font = [UIFont systemFontOfSize:14];
        _resultLabel.textColor = [UIColor blackColor];
        [self.myContentView addSubview:_resultLabel];
        [_resultLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.reasonLabel.mas_bottom).offset(17);
            make.left.equalTo(self.reasonStrLable);
        }];
    }
    return _resultLabel;
}

- (UILabel *)reasonStrLable{
    if (!_reasonStrLable) {
        _reasonStrLable = [[UILabel alloc] init];
        _reasonStrLable.font = [UIFont systemFontOfSize:14];
        _reasonStrLable.textColor = [UIColor blackColor];
        _reasonStrLable.text = @"理由:";
        [self.myContentView addSubview:_reasonStrLable];
        [_reasonStrLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.timeStrLable.mas_bottom).offset(8);
            make.left.equalTo(self.timeStrLable);
        }];
    }
    return _reasonStrLable;
}

- (UILabel *)reasonLabel{
    if (!_reasonLabel) {
        _reasonLabel = [[UILabel alloc] init];
        _reasonLabel.font = [UIFont systemFontOfSize:14];
        _reasonLabel.textColor = [UIColor blackColor];
        _reasonLabel.numberOfLines = 0;
        [self.myContentView addSubview:_reasonLabel];
        
        [_reasonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.reasonStrLable);
            make.left.equalTo(self.timeLabel);
            make.width.equalTo(@(screen_W-125));
        }];
    }
    return _reasonLabel;
}

- (UILabel *)timeStrLable{
    if (!_timeStrLable) {
        _timeStrLable = [[UILabel alloc] init];
        _timeStrLable.font = [UIFont systemFontOfSize:14];
        _timeStrLable.textColor = [UIColor blackColor];
        _timeStrLable.text = @"通话时长:";
        [self.myContentView addSubview:_timeStrLable];
        [_timeStrLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.riqiLable.mas_bottom).offset(8);
            make.left.equalTo(self.riqiLable);
        }];
    }
    return _timeStrLable;
}
- (UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.font = [UIFont systemFontOfSize:14];
        _timeLabel.textColor = [UIColor blackColor];
        [self.myContentView addSubview:_timeLabel];
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.timeStrLable.mas_centerY);
            make.left.equalTo(self.dateLabel);
        }];
    }
    return _timeLabel;
}

- (UILabel *)riqiLable{
    if (!_riqiLable) {
        _riqiLable = [[UILabel alloc] init];
        _riqiLable.font = [UIFont systemFontOfSize:14];
        _riqiLable.textColor = [UIColor blackColor];
        _riqiLable.text = @"日期:";
        [self.myContentView addSubview:_riqiLable];
        [_riqiLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.nameLable.mas_bottom).offset(8);
            make.left.equalTo(self.nameLable);
        }];
    }
    return _riqiLable;
}
- (UILabel *)dateLabel{
    if (!_dateLabel) {
        _dateLabel = [[UILabel alloc] init];
        _dateLabel.font = [UIFont systemFontOfSize:14];
        _dateLabel.textColor = [UIColor blackColor];
        [self.myContentView addSubview:_dateLabel];
        [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.riqiLable.mas_centerY);
            make.left.equalTo(self.nickLabel);
        }];
    }
    return _dateLabel;
}
- (UILabel *)nameLable{
    if (!_nameLable) {
        _nameLable = [[UILabel alloc] init];
        _nameLable.font = [UIFont systemFontOfSize:14];
        _nameLable.textColor = [UIColor blackColor];
        _nameLable.text = @"对方用户:";
        [self.myContentView addSubview:_nameLable];
        [_nameLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.subTitleLabel.mas_bottom).offset(17);
            make.left.equalTo(self.myContentView).offset(15);
        }];
    }
    return _nameLable;
}
- (UILabel *)nickLabel{
    if (!_nickLabel) {
        _nickLabel = [[UILabel alloc] init];
        _nickLabel.font = [UIFont systemFontOfSize:15];
        _nickLabel.textColor = SLColor(0, 83, 210);
        [self.myContentView addSubview:_nickLabel];
        [_nickLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.nameLable.mas_centerY);
            make.left.equalTo(self.nameLable.mas_right).offset(8);
            make.width.equalTo(@(screen_W - 125));
        }];
    }
    return _nickLabel;
}

- (void)setModel:(SLSystemMessageModel *)model{
    [super setModel:model];
    self.nickLabel.text = model.otherName;
    self.dateLabel.text = model.dateStr;
    self.timeLabel.text = model.timeStr;
    self.reasonLabel.text = model.reason;
    //加行高
    [self setLineSpacing:5 label:self.reasonLabel];
    if (model.isPass) {
        self.resultLabel.text = [NSString stringWithFormat:@"已通过,退款%.2f元已到达你的账户",model.drawback];
    }else{
        self.resultLabel.text = @"审核未通过";
    }
    
}

- (void)setLineSpacing:(CGFloat)spacing label:(UILabel *)label{
    if (label.text.length>0) {
        NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:label.text];
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:spacing];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [label.text length])];
        [label setAttributedText:attributedString];
        [label sizeToFit];
    }
}

@end
