//
//  SLBaseSystemMessageTableViewCell.m
//  Superloop
//
//  Created by administrator on 16/7/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLBaseSystemMessageTableViewCell.h"
#import "SLSystemMessageModel.h"

@interface SLBaseSystemMessageTableViewCell ()

@property(nonatomic, strong)UILabel *getMessageTimeLabel;

@end

@implementation SLBaseSystemMessageTableViewCell
- (UIView *)myContentView{
    if (!_myContentView) {
        _myContentView = [[UIView alloc] init];
        [self.contentView addSubview:_myContentView];
        _myContentView.layer.cornerRadius = 5;
        _myContentView.layer.borderColor = SLColor(200, 200, 200).CGColor;
        _myContentView.layer.borderWidth = 0.5;
        _myContentView.backgroundColor = [UIColor whiteColor];
        [_myContentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(33);
            make.left.equalTo(self.contentView).offset(11);
            make.right.equalTo(self.contentView).offset(-11);
            make.bottom.equalTo(self.contentView);
        }];
    }
    return _myContentView;
}
- (UILabel *)getMessageTimeLabel
{
    if (!_getMessageTimeLabel) {
        _getMessageTimeLabel = [[UILabel alloc] init];
        _getMessageTimeLabel.font = [UIFont systemFontOfSize:14];
        _getMessageTimeLabel.textColor = [UIColor whiteColor];
        _getMessageTimeLabel.textAlignment = NSTextAlignmentCenter;
        _getMessageTimeLabel.layer.cornerRadius = 3;
        _getMessageTimeLabel.clipsToBounds = YES;
        [self.contentView addSubview:_getMessageTimeLabel];
        _getMessageTimeLabel.backgroundColor = SLColor(210, 210, 210);
        
    }
    return _getMessageTimeLabel;
}
- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:16];
        [self.myContentView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.myContentView).offset(11);
        }];
    }
    return _titleLabel;
}
- (UILabel *)subTitleLabel{
    if (!_subTitleLabel) {
        _subTitleLabel = [[UILabel alloc] init];
        _subTitleLabel.font = [UIFont systemFontOfSize:14];
        [self.myContentView addSubview:_subTitleLabel];
        [_subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.myContentView).offset(15);
            make.top.equalTo(self.titleLabel.mas_bottom).offset(20);
        }];
    }
    return _subTitleLabel;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = SLColor(250, 250, 250);
}

- (void)setModel:(SLSystemMessageModel *)model{
    _model = model;
    if ([model.msgtype isEqual:@5]) {
        self.titleLabel.text = @"通知";
        self.subTitleLabel.text = @"你的话题";
    }else if ([model.msgtype isEqual:@3]){
        self.titleLabel.text = @"申诉结果";
        self.subTitleLabel.text = @"你的申诉请求";
    }else if ([model.msgtype isEqual:@4]){
        self.titleLabel.text = @"申诉结果";
        self.subTitleLabel.text = @"你的申诉请求";
    }else if([model.msgtype isEqual:@9]){
        self.titleLabel.text = model.msgcontent[@"title"];
        self.subTitleLabel.text = model.msgcontent[@"contentTitle"];
    }
    
    self.getMessageTimeLabel.text = model.updatedStr;
    CGFloat width = [model.updatedStr boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size.width+10;
    [_getMessageTimeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.contentView).offset(8);
        make.width.mas_equalTo(width);
    }];
}
- (void)setFrame:(CGRect)frame
{
//    self.contentView.layer.cornerRadius = 5;
//    self.contentView.layer.borderColor = SLColor(200, 200, 200).CGColor;
//    self.contentView.layer.borderWidth = 0.5;
//    frame.origin.x = 11;
//    frame.size.height -= 33;
//    frame.size.width -= 22;
    self.backgroundColor = SLColor(246, 246, 246);
    [super setFrame:frame];
}

@end
