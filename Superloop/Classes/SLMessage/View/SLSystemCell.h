//
//  SLSystemCell.h
//  Superloop
//
//  Created by WangJiwei on 16/4/22.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLSystemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineView;

//@property (nonatomic,strong) UIImageView *icon;
//@property (nonatomic,strong) UILabel *titleLab;
//@property (nonatomic,strong) UIImageView *redImgView;
//@property (nonatomic,strong) UILabel *numLab;
//@property (nonatomic,strong) UIImageView *cutImgView;
//
@end
