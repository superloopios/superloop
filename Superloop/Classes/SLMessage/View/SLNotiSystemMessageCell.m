//
//  SLNotiSystemMessageCell.m
//  Superloop
//
//  Created by administrator on 16/7/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLNotiSystemMessageCell.h"
#import "SLSystemMessageModel.h"


@interface SLNotiSystemMessageCell ()

//显示对方用户:的lab
@property(nonatomic, strong)UILabel *topicTitleLable;

//删除原因
@property(nonatomic, strong)UILabel *reasonStrLable;
@property(nonatomic, strong)UILabel *reasonLabel;
@property(nonatomic, strong)UILabel *resultLabel;


@end

@implementation SLNotiSystemMessageCell

- (UILabel *)resultLabel{
    if (!_resultLabel) {
        _resultLabel = [[UILabel alloc] init];
        _resultLabel.font = [UIFont systemFontOfSize:14];
        _resultLabel.textColor = [UIColor blackColor];
        [self.myContentView addSubview:_resultLabel];
        [_resultLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.reasonLabel.mas_bottom).offset(17);
            make.left.equalTo(self.reasonStrLable);
        }];
    }
    return _resultLabel;
}

- (UILabel *)reasonStrLable{
    if (!_reasonStrLable) {
        _reasonStrLable = [[UILabel alloc] init];
        _reasonStrLable.font = [UIFont systemFontOfSize:14];
        _reasonStrLable.textColor = [UIColor blackColor];
        _reasonStrLable.text = @"删除原因:";
        [self.myContentView addSubview:_reasonStrLable];
        [_reasonStrLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.topicTitleLable.mas_bottom).offset(8);
            make.left.equalTo(self.topicTitleLable);
        }];
    }
    return _reasonStrLable;
}

- (UILabel *)reasonLabel{
    if (!_reasonLabel) {
        _reasonLabel = [[UILabel alloc] init];
        _reasonLabel.font = [UIFont systemFontOfSize:14];
        _reasonLabel.textColor = [UIColor blackColor];
        _reasonLabel.numberOfLines = 0;
        [self.myContentView addSubview:_reasonLabel];
        
        [_reasonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.reasonStrLable);
            make.left.equalTo(self.reasonStrLable.mas_right).offset(8);
            make.width.equalTo(@(screen_W-125));
        }];
    }
    return _reasonLabel;
}

- (UILabel *)topicTitleLable{
    if (!_topicTitleLable) {
        _topicTitleLable = [[UILabel alloc] init];
        _topicTitleLable.font = [UIFont systemFontOfSize:14];
        _topicTitleLable.textColor = [UIColor blackColor];
        _topicTitleLable.numberOfLines = 0;
        [self.myContentView addSubview:_topicTitleLable];
        [_topicTitleLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.subTitleLabel.mas_bottom).offset(17);
            make.left.equalTo(self.myContentView).offset(15);
            make.width.equalTo(@(screen_W-52));
        }];
    }
    return _topicTitleLable;
}


- (void)setModel:(SLSystemMessageModel *)model{
    [super setModel:model];
    self.topicTitleLable.text = model.topicTitle;
    [self setLineSpacing:5 label:self.topicTitleLable];
    self.reasonLabel.text = model.reason;
    [self setLineSpacing:5 label:self.reasonLabel];
    self.resultLabel.text = @"如有任何疑问,请联系超级圈客服";
}

- (void)setLineSpacing:(CGFloat)spacing label:(UILabel *)label{
    if (label.text.length>0) {
        NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:label.text];
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:spacing];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [label.text length])];
        [label setAttributedText:attributedString];
        [label sizeToFit];
    }
}

@end
