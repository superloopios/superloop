//
//  SLBaseSystemMessageTableViewCell.h
//  Superloop
//
//  Created by administrator on 16/7/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLSystemMessageModel;

@interface SLBaseSystemMessageTableViewCell : UITableViewCell

@property(nonatomic, strong)UILabel *titleLabel;

@property(nonatomic, strong)UILabel *subTitleLabel;

@property(nonatomic, strong)SLSystemMessageModel *model;

@property(nonatomic, strong)UIView *myContentView;

@end
