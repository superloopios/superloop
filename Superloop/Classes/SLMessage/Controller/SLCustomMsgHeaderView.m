//
//  SLCustomMsgHeaderView.m
//  Superloop
//
//  Created by WangS on 16/8/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCustomMsgHeaderView.h"

@interface SLCustomMsgHeaderView ()

@end
@implementation SLCustomMsgHeaderView


-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        
        _iconImgView=[[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 44, 44)];
        [self addSubview:_iconImgView];
        
        _cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(70, 63.5, ScreenW-70, 0.5)];
        _cutImgView.backgroundColor=SLSepatorColor;
        [self addSubview:_cutImgView];
        
        _titleLab=[[UILabel alloc] initWithFrame:CGRectMake(69, 22, 100, 20)];
        _titleLab.textAlignment=NSTextAlignmentLeft;
        _titleLab.textColor=[UIColor blackColor];
        _titleLab.font=[UIFont systemFontOfSize:17];
        [self addSubview:_titleLab];
        
         _redImgView=[[UIImageView alloc] init];
        _redImgView.backgroundColor=[UIColor redColor];
        _redImgView.layer.cornerRadius = 9;
        _redImgView.layer.masksToBounds = YES;
        [self addSubview:_redImgView];
        
        _numLab=[[UILabel alloc] init];
        _numLab.font=[UIFont systemFontOfSize:11];
        _numLab.textAlignment=NSTextAlignmentCenter;
        _numLab.textColor=[UIColor whiteColor];
        [_redImgView addSubview:_numLab];
      
    }
    return self;
}



@end
