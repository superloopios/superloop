//
//  SLChargeMessageView.m
//  Superloop
//
//  Created by WangJiwei on 16/4/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//  值不值消息和消息实体

#import "SLChargeMessageView.h"
#import "SLChargeMessageViewModel.h"
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "UILabel+StringFrame.h"
#import "SLAPIHelper.h"


@interface SLChargeMessageView ()
@property (nonatomic, strong) SLChargeMessageViewModel *viewModel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *noHelpConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *standardViewConstraint;

@end
@implementation SLChargeMessageView
@dynamic viewModel;


- (void)layoutSubviews{
    [super layoutSubviews];

    [self.standardViewConstraint setConstant:self.frame.size.width/2];
    [self.constrianHeight setConstant:0.3];
    
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:self.helpBtn.bounds      byRoundingCorners:UIRectCornerTopLeft|UIRectCornerBottomLeft cornerRadii:CGSizeMake(7.5, 7.5)];
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
    maskLayer1.frame = self.helpBtn.bounds;
    maskLayer1.path = maskPath1.CGPath;
    self.helpBtn.layer.mask = maskLayer1;
    
    UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:self.noHelpBtn.bounds      byRoundingCorners:UIRectCornerTopRight|UIRectCornerBottomRight cornerRadii:CGSizeMake(7.5, 7.5)];
    CAShapeLayer *maskLayer2 = [[CAShapeLayer alloc] init];
    maskLayer2.frame = self.noHelpBtn.bounds;
    maskLayer2.path = maskPath2.CGPath;
    self.noHelpBtn.layer.mask = maskLayer2;
    
}
- (instancetype)init {
    if (self = [super init]) {
        
        UINib *nib = [UINib nibWithNibName:@"SLChargeMessageView" bundle:nil];
        [nib instantiateWithOwner:self options:NULL];
        UITapGestureRecognizer *tapGestureRecognizer =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chargeViewClicked)];
        [_hudView addGestureRecognizer:tapGestureRecognizer];
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
    }
    return self;
}


#pragma mark --------- 消息实体的点击事件
- (void)chargeViewClicked{
    if (self.viewModel.ask4showBlock){
        self.viewModel.ask4showBlock();
    }
}

- (void)setHelpfulBtnStatus:(NSString *)complainStatus evaluateStatus:(NSString *)evaluateStatus{
    
    //更新状态
    NSInteger complainIndex = [complainStatus integerValue];
    NSInteger evaluateIndex = [evaluateStatus integerValue];
    if (evaluateIndex == 1) {
        [self.helpBtn setTitle:@"申诉" forState:UIControlStateNormal];
        [self.helpBtn setTitleColor:UIColorFromRGB(0xb9b9b9) forState:UIControlStateNormal];
        self.helpBtn.enabled = NO;
        
        [self.noHelpBtn setTitle:@"已评价" forState:UIControlStateNormal];
        self.noHelpBtn.backgroundColor = UIColorFromRGB(0xb9b9b9);
        self.noHelpBtn.enabled = NO;
    }else{
        switch (complainIndex) {
            case 0://无
                [self.helpBtn setTitle:@"申诉" forState:UIControlStateNormal];
                [self.helpBtn setTitleColor:UIColorFromRGB(0xff5a5f) forState:UIControlStateNormal];
                self.helpBtn.enabled = YES;
                
                [self.noHelpBtn setTitle:@"去评价" forState:UIControlStateNormal];
                self.noHelpBtn.backgroundColor = UIColorFromRGB(0xff5a5f);
                self.noHelpBtn.enabled = YES;
                break;
            case 1://申诉中
                [self.helpBtn setTitle:@"申诉中" forState:UIControlStateNormal];
                [self.helpBtn setTitleColor:UIColorFromRGB(0xff5a5f) forState:UIControlStateNormal];
                self.helpBtn.enabled = NO;
                
                [self.noHelpBtn setTitle:@"无法评价" forState:UIControlStateNormal];
                self.noHelpBtn.backgroundColor = UIColorFromRGB(0xb9b9b9);
                self.noHelpBtn.enabled = NO;
                break;
            case 2://申诉成功
                [self.helpBtn setTitle:@"申诉成功" forState:UIControlStateNormal];
                [self.helpBtn setTitleColor:UIColorFromRGB(0xb9b9b9) forState:UIControlStateNormal];
                self.helpBtn.enabled = NO;
                
                [self.noHelpBtn setTitle:@"无法评价" forState:UIControlStateNormal];
                self.noHelpBtn.backgroundColor = UIColorFromRGB(0xb9b9b9);
                self.noHelpBtn.enabled = NO;
                break;
            case 3://申诉驳回
                [self.helpBtn setTitle:@"申诉驳回" forState:UIControlStateNormal];
                [self.helpBtn setTitleColor:UIColorFromRGB(0xb9b9b9) forState:UIControlStateNormal];
                self.helpBtn.enabled = NO;
                
                [self.noHelpBtn setTitle:@"去评价" forState:UIControlStateNormal];
                self.noHelpBtn.backgroundColor = UIColorFromRGB(0xff5a5f);
                self.noHelpBtn.enabled = YES;
                break;
            default:
                break;
        }
    }
}

// 需要刷新BubbleView时会被调用
- (void)updateBubbleView{
 
    NSString *moneyStr = [NSString stringWithFormat:@"%@",self.viewModel.moneyText];
    double strMoney = [moneyStr doubleValue];
    self.moneyLabel.text = [NSString stringWithFormat:@"%.1f",strMoney];
    self.messageLabel.text = self.viewModel.content;
    
    //如果是收费消息的实体
    if (self.hudView.hidden == YES) {
        
        self.waringLable.hidden = NO;
        self.singleLable.hidden = NO;
        self.messageLabel.hidden = NO;
        self.noHelpBtn.hidden = NO;
        self.helpBtn.hidden = NO;
        self.fengeLable.hidden = NO;
        
        CGFloat cellWidth;
        if (ScreenW-150>200) {
            cellWidth=ScreenW-150;
        }else{
            cellWidth = 200;
        }
        CGFloat height = [self.messageLabel boundingRectWithString:self.viewModel.content withSize:CGSizeMake((cellWidth-10), MAXFLOAT) withFont:15].height;
        
        _contentView.height = height + 80;
        _contentView.width = cellWidth;
        self.frame = self.contentView.frame;
    }
    else{
        self.waringLable.hidden = YES;
        self.singleLable.hidden = YES;
        self.messageLabel.hidden = YES;
        self.noHelpBtn.hidden = YES;
        self.helpBtn.hidden = YES;
        self.fengeLable.hidden = YES;
//        CGFloat width = [self.viewModel.content boundingRectWithSize:CGSizeMake(ScreenW - 100, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]} context:nil].size.width;
//        if (width >200) {
//            width = 200;
//        }
//        if (width < 110) {
//            width = 200;
//        }
//        if (width >= 110 ||width <= 200) {
//            width = 200;
//        }
        CGFloat width = 200;
        _contentView.width = width + 12;
        _contentView.height = 40;
        self.frame = self.contentView.frame;
    }
    
    [self.standardViewConstraint setConstant:self.frame.size.width/2];
    [self.constrianHeight setConstant:0.3];
    [self layoutIfNeeded];
    [self addSubview:self.contentView];
    self.forceLayout = YES;
}


// 返回所持ViewModel类名，用于类型检测
- (NSString *)viewModelClassName{
    return NSStringFromClass([SLChargeMessageViewModel class]);
}

- (IBAction)value:(UIButton *)sender {
    
    //    NSDictionary *parameters =  @{@"messageId": self.viewModel.msgId,@"type":@(sender.tag)};
    //    [SLAPIHelper postHelpOrNot:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
    NSString *uuid= [[NSUUID UUID] UUIDString];
    //减去"-"
    NSString *str = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    //变小写
    NSString *lower = [str lowercaseString];
    NSString *content = [[NSString alloc] init];
    if (sender.tag == 1) {  // 有帮助   (评价)
        // [self setHelpfulBtnStatus:@"1"];
        //sender.userInteractionEnabled = NO;
        //            content = @"true";
        content = @"true";
    }else if (sender.tag == 2) {    // 无帮助 （申诉）
        //[self setHelpfulBtnStatus:@"2"];
        //sender.userInteractionEnabled = NO;
        //            content = @"false";
        content = @"false";
    }
    NSDictionary *contentDict = @{
                                  @"customizeMsgType":@"2",
                                  @"subMessageType":@"1",
                                  @"authorId":ApplicationDelegate.userId,
                                  @"content":content,
                                  @"msgId":lower,
                                  @"money":self.viewModel.moneyText,
                                  };
    NSData *helpData = [NSJSONSerialization dataWithJSONObject:contentDict
                                                       options:0
                                                         error:NULL];
    NSString *string = [[NSString alloc] initWithData:helpData encoding:NSUTF8StringEncoding];
    if (self.viewModel.ask3showBlock) {
        self.viewModel.ask3showBlock(sender.tag, string);
    }
    
    
    //    } failure:^(SLHttpRequestError *failure) {
    //        
    //    }];
    
}

@end
