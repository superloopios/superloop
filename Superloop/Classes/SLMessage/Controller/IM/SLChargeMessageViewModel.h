//
//  SLChargeMessageViewModel.h
//  Superloop
//
//  Created by WangJiwei on 16/4/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <WXOUIModule/YWUIFMWK.h>
#import <WXOpenIMSDKFMWK/YWFMWK.h>

@interface SLChargeMessageViewModel : YWBaseBubbleViewModel
- (instancetype)initWithMessage:(id<IYWMessage>)aMessage;

@property (nonatomic, copy)NSString *content;
@property (nonatomic, copy)NSString *moneyText;
@property (nonatomic, copy) NSString *authorId;
@property (nonatomic, copy) NSString *msgId;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, copy) NSString *messageId;

//区分付款未付款消息体状态
@property (nonatomic,copy)NSString *messageType4Charge;
//类型
@property (nonatomic, strong)NSString *messageType;

typedef void (^CardBubbleAsk4ShowCard) ();

#pragma mark ------- 值不值消息体的点击之后值的回调
@property (nonatomic, copy) CardBubbleAsk4ShowCard ask4showBlock;

- (void)setAsk4showBlock:(CardBubbleAsk4ShowCard)ask4showBlock;
//值不值消息有无帮助的回调
typedef void (^ValueBubbleAsk3ShowCard) (NSInteger helpful, NSString *string);
//值不值消息的回调
@property (nonatomic, copy) ValueBubbleAsk3ShowCard ask3showBlock;
- (void)setAsk3showBlock:(ValueBubbleAsk3ShowCard)ask3showBlock;

@end
