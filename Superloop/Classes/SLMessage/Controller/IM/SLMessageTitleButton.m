//
//  SLMessageTitleButton.m
//  Superloop
//
//  Created by 朱宏伟 on 16/5/21.
//  Copyright © 2016年 Superloop. All rights reserved.
//  自定义的标题btn的显示样式

#import "SLMessageTitleButton.h"
#import "UILabel+StringFrame.h"
@implementation SLMessageTitleButton
// 不需要系统提供的高亮样式
- (void)setHighlighted:(BOOL)highlighted{
    
}
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:17];
        self.titleLabel.numberOfLines = 1;
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat w = [self.titleLabel boundingRectWithString:self.titleLabel.text withSize:CGSizeMake(MAXFLOAT, 0) withFont:17].width;
    if (w > 100) {
        w = 100;
    }
//    self.titleLabel.centerX = 75;
    self.titleLabel.x=(self.width-w-10)/2.0;
//    self.titleLabel.y = self.height * 0.4;
    self.titleLabel.y = 5;
    self.titleLabel.width = w + 10;
    self.titleLabel.height = 20;
    
//    self.imageView.y = self.height * 0.4;
    self.imageView.y = 7;
    self.imageView.width = 15;
    self.imageView.height = 20;
    self.imageView.x = self.titleLabel.x+self.titleLabel.width;
//    self.imageView.centerX = w / 2 + self.titleLabel.centerX + 10;

}

@end
