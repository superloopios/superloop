//
//  SLPayMentView.m
//  zhifudemo
//
//  Created by WangJiwei on 16/5/7.
//  Copyright © 2016年 WangJiwei. All rights reserved.
//

#import "SLPayMentView.h"
@interface SLPayMentView()<UITextFieldDelegate>

@end
@implementation SLPayMentView
- (void)awakeFromNib
{
    [super awakeFromNib];
    _passwordTextfield.keyboardType = UIKeyboardTypeNumberPad;
//    [_passwordTextfield becomeFirstResponder];
    self.layer.cornerRadius = 6;
    self.layer.masksToBounds = YES;
    _passwordView.layer.cornerRadius = 6;
    _passwordView.layer.masksToBounds = 6;
    _passwordView.layer.borderWidth = 1;
    _passwordView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self initView];
}
- (void)initView
{
    _passwordIndicatorArrary = [[NSMutableArray alloc] init];
    _passwordTextfield.delegate = self;
    _passwordView.userInteractionEnabled = NO;
    [_passwordTextfield addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    CGFloat width = (_passwordView.frame.size.width - 5) / 6;
    for (int i = 0; i < 5; i++) {
        UIImageView *lineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(width + (1 + width) * i, 3, 1, _passwordView.frame.size.height - 6)];
        lineImageView.backgroundColor = [UIColor lightGrayColor];
        [_passwordView addSubview:lineImageView];
    }
    for (int i = 0; i < 6; i++) {
        UIImageView *dotImageView = [[UIImageView alloc] initWithFrame:CGRectMake((width - 10) / 2 + (width + 1) * i, (_passwordView.frame.size.height - 10) / 2, 10, 10 )];
        dotImageView.layer.cornerRadius = 5;
        dotImageView.layer.masksToBounds = YES;
        dotImageView.hidden = YES;
        dotImageView.backgroundColor = [UIColor blackColor];
        [_passwordView addSubview:dotImageView];
        [_passwordIndicatorArrary addObject:dotImageView];
    }
}
-(void)textFieldDidChange :(UITextField *)theTextField{
    NSLog(@"%ld",theTextField.text.length);
    if (theTextField.text.length > 6) {
        return;
    }
    for (UIImageView *dotImageView in _passwordIndicatorArrary)
    {
        dotImageView.hidden = YES;
    }
    
    for (int i = 0; i< _passwordTextfield.text.length; i++)
    {
        ((UIImageView*)[_passwordIndicatorArrary objectAtIndex:i]).hidden = NO;
    }
    if (_passwordTextfield.text.length == 6) {
//        _passwordTextfield.enabled = NO;
        if ([_Delegate respondsToSelector:@selector(didInputPassword)]) {
            [_Delegate didInputPassword];
        }
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField.text.length > 6) {
        return NO;
    }
    return YES;
}
- (IBAction)cancel:(id)sender {
    if ([_Delegate respondsToSelector:@selector(didcancel)]) {
        [_Delegate didcancel];
    }
}
- (IBAction)foggetPassword:(id)sender {
    if ([_Delegate respondsToSelector:@selector(didForgetPassword)]) {
        [_Delegate didForgetPassword];
    }
}
@end
