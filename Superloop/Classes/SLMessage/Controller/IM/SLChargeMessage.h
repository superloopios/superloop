//
//  SLChargeMessage.h
//  Superloop
//
//  Created by WangJiwei on 16/4/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import <WXOUIModule/YWUIFMWK.h>
#import <WXOpenIMSDKFMWK/YWFMWK.h>
#import <Foundation/Foundation.h>

@interface SLChargeMessage : NSObject<YWInputViewPluginProtocol>
// 加载该插件的inputView
@property (nonatomic, weak) YWMessageInputView *inputViewRef;

@end
