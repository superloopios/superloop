//
//  SLMessageManger.h
//  Superloop
//
//  Created by 朱宏伟 on 16/7/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WXOpenIMSDKFMWK/YWFMWK.h>
#import <WXOUIModule/YWUIFMWK.h>
#define SPTribeSystemConversationID [NSString stringWithFormat:@"%@SPTribeSystem", kYWCustomConversationIdPrefix]
// 自定义会话的id
FOUNDATION_EXTERN NSString *const kSPCustomConversationIdForPortal;
FOUNDATION_EXTERN NSString *const kSPCustomConversationIdForFAQ;
@class YWIMKit;
@interface SLMessageManger : NSObject

// SDK中的接口分为全局接口和非全局接口，非全局接口需要通过YWIMKit或其成员YWIMCore调用，所以这里需要持有这个对象
@property (strong, nonatomic, readwrite) YWIMKit *ywIMKit;
@property (nonatomic,assign)BOOL isComeFromTopicDetailVc;
+ (instancetype)sharedInstance;

/**
 *  初始化入口函数
 *
 *  程序完成启动，在appdelegate中的 application:didFinishLaunchingWithOptions:一开始的地方调用
 */
- (void)callThisInDidFinishLaunching;

/**
 *  登录入口函数
 *
 *  用户在应用的服务器登录成功之后，向云旺服务器登录之前调用
 *  @param ywLoginId, 用来登录云旺IMSDK的id
 *  @param password, 用来登录云旺IMSDK的密码
 *  @param aSuccessBlock, 登陆成功的回调
 *  @param aFailedBlock, 登录失败的回调
 */
- (void)callThisAfterISVAccountLoginSuccessWithYWLoginId:(NSString *)ywLoginId passWord:(NSString *)passWord preloginedBlock:(void(^)())aPreloginedBlock successBlock:(void(^)())aSuccessBlock failedBlock:(void (^)(NSError *))aFailedBlock;


 //用户即将退出登录时调用

- (void)callThisBeforeISVAccountLogout;


 //初始化的示例代码

- (BOOL)openImInit;

 //登录的代码

- (void)loginWithUserID:(NSString *)aUserID password:(NSString *)aPassword successBlock:(void(^)())aSuccessBlock failedBlock:(void(^)(NSError *aError))aFailedBlock;

 //监听连接状态

- (void)listenConnectionStatus;

@property (nonatomic, assign) YWIMConnectionStatus lastConnectionStatus;


  //注销的代码
 
- (void)logout;


  //预登录：上次登录过账号A，这次app启动，可以直接预登录该帐号，进入查看本地数据。同时发起真正的登录操作，连接IM。
 
- (void)preLoginWithLoginId:(NSString *)loginId successBlock:(void(^)())aPreloginedBlock;


 //设置语音等声音的播放模式

- (void)setAudioCategory;


 //设置头像显示方式：包括圆角弧度和contentMode
 
- (void)setAvatarStyle;


  //创建会话列表页面

- (YWConversationListViewController *)makeConversationListControllerWithSelectItemBlock:(YWConversationsListDidSelectItemBlock)aSelectItemBlock;


  //打开某个会话

- (void)openConversationViewControllerWithConversation:(YWConversation *)aConversation fromNavigationController:(UINavigationController *)aNavigationController;


  //打开单聊页面

- (void)openConversationViewControllerWithPerson:(YWPerson *)aPerson fromNavigationController:(UINavigationController *)aNavigationController;


  //打开群聊页面

- (void)openConversationViewControllerWithTribe:(YWTribe *)aTribe fromNavigationController:(UINavigationController *)aNavigationController;

/**
 *  打开客服会话
 *  @param aPersonId 客服Id
 */
- (void)openEServiceConversationWithPersonId:(NSString *)aPersonId
                           fromNavigationController:(UINavigationController *)aNavigationController;


  //创建某个会话Controller

- (YWConversationViewController *)makeConversationViewControllerWithConversation:(YWConversation *)aConversation;
#pragma mark - 自定义业务


  //设置如何显示自定义消息

- (void)showCustomMessageWithConversationController:(YWConversationViewController *)aConversationController;

  //添加或者更新自定义会话
 
- (void)addOrUpdateCustomConversation;


  //自定义优先级的置顶会话（可保持长期置顶）

- (void)addHighPriorityCustomConversation;


//将会话置顶，或者取消置顶

- (void)markConversationOnTop:(YWConversation *)aConversation onTop:(BOOL)aOnTop;


//自定义会话Cell及菜单

- (void)customizeConversationCellWithConversationListController:(YWConversationListViewController *)aConversationListController;


/**
 *  插入本地消息
 *  消息不会被发送到对方，仅本地展示
 */
- (void)insertLocalMessageBody:(YWMessageBody *)aBody inConversation:(YWConversation *)aConversation;

#pragma mark - Customize


  //自定义全局导航栏

- (void)customGlobleNavigationBar;


 //自定义皮肤

- (void)customUISkin;

#pragma mark - 聊天页面自定义

  //添加输入面板插件
 
- (void)addInputViewPluginToConversationController:(YWConversationViewController *)aConversationController;

/**
 *  设置消息的长按菜单
 *  这个方法展示如何设置图片消息的长按菜单
 */
- (void)setMessageMenuToConversationController:(YWConversationViewController *)aConversationController;

/**
 *  设置气泡最大宽度
 */
- (void)setMaxBubbleWidth;


#pragma mark - events


  //监听新消息

- (void)listenNewMessage;


 //监听自己发送的消息的生命周期

- (void)listenMyMessageLife;


 //头像点击事件
 
- (void)listenOnClickAvatar;


 //链接点击事件

- (void)listenOnClickUrl;


 //预览大图事件

- (void)listenOnPreviewImage;


#pragma mark - apns

/**
 *  需要在-[AppDelegate application:didFinishLaunchingWithOptions:]中第一时间设置此回调
 *  在IMSDK截获到Push通知并需要您处理Push时，IMSDK会自动调用此回调
 */
- (void)handleAPNSPush;


#pragma mark - EService

 //获取EService对象
- (YWPerson *)fetchEServicePersonWithPersonId:(NSString *)aPersonId groupId:(NSString *)aGroupId;

@end


#pragma mark - 其他

@interface SLMessageManger ()

@property (nonatomic, readonly) id<UIApplicationDelegate> appDelegate;

@property (nonatomic, readonly) UIWindow *rootWindow;


@end
