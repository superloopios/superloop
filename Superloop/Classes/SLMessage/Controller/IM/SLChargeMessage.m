//
//  SLChargeMessage.m
//  Superloop
//
//  Created by WangJiwei on 16/4/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//  值不值消息

#import "SLChargeMessage.h"
#import "SPUtil.h"
#import "SLMessageManger.h"
#import <Masonry.h>
#import "SLTextView.h"
#import "AppDelegate.h"
#define margin (self.backView.height - 450) / 2
@interface SLChargeMessage()<UITextFieldDelegate,UITextViewDelegate>
@property (nonatomic, readonly) YWConversationViewController *conversationViewController;
/** 当前被点中的按钮 */
@property (nonatomic, weak) UIButton *clickedButton;
@property (nonatomic, strong)UIView *backView;
@property (nonatomic, strong)UITextField *otherMoneyTextfield;
@property (nonatomic, strong)SLTextView *contentView;
@property (nonatomic, strong)UIScrollView *backScrollView;
@property (nonatomic, strong)UIButton *sendBtn;
@end

@implementation SLChargeMessage

#pragma mark - properties

- (YWConversationViewController *)conversationViewController
{
    if ([self.inputViewRef.controllerRef isKindOfClass:[YWConversationViewController class]]) {
        return (YWConversationViewController *)self.inputViewRef.controllerRef;
    } else {
        return nil;
    }
}

#pragma mark - YWInputViewPluginProtocol
// 插件图标
- (UIImage *)pluginIconImage
{
    return [UIImage imageNamed:@"FeeMoney"];
}

// 插件名称
- (NSString *)pluginName
{
    return @"付费消息";
}

// 插件对应的view，会被加载到inputView上
- (UIView *)pluginContentView
{
    return nil;
}

// 插件被选中运行
- (void)pluginDidClicked
{
    //背景色
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    backView.frame = self.conversationViewController.view.bounds;
    self.backView = backView;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap)];
    [self.backView addGestureRecognizer:tap];

    
    [self.conversationViewController.view addSubview:backView];
    
    UIScrollView *backScrollView = [[UIScrollView alloc] init];
    backScrollView.backgroundColor = [UIColor clearColor];
    backScrollView.frame = backView.bounds;
    backScrollView.contentSize = CGSizeMake(ScreenW, ScreenH + 216 - margin);
    _backScrollView = backScrollView;
    [backView addSubview:backScrollView];
    
    //选择框
    UIView *selectView = [[UIView alloc] init];
    selectView.backgroundColor = SLColor(248, 156, 70);
    selectView.layer.cornerRadius = 5;
    selectView.layer.masksToBounds = YES;
    [backScrollView addSubview:selectView];

    //取消按钮
    UIButton *btnOut = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnOut setImage:[UIImage imageNamed:@"closeWindow"] forState:UIControlStateNormal];
    [selectView addSubview:btnOut];
    [btnOut addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
    
    //选择金额
    UILabel *selectLabel = [[UILabel alloc] init];
    selectLabel.text = @"选择金额";
    selectLabel.textColor = [UIColor whiteColor];
    [selectLabel setFont:[UIFont systemFontOfSize:16]];
    [selectLabel sizeToFit];
    [selectView addSubview:selectLabel];
    int cols = 3;
    NSArray *moneyArray = @[@"1元",@"5元",@"10元",@"50元",@"100元",@"200元"];
    CGFloat btnWidth = (280- 70) / cols;
    CGFloat marginW = 10;
    CGFloat marginH = 18;
    CGFloat btnHeight = 33;
    UIView *moneyView = [[UIView alloc] init];
    for (int i = 0; i < moneyArray.count; i++) {
        UIButton *moneyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        moneyBtn.layer.cornerRadius = 5;
        moneyBtn.layer.masksToBounds = YES;
        moneyBtn.layer.borderColor = SLColor(213, 173, 107).CGColor;
        moneyBtn.layer.borderWidth = 1;
        moneyBtn.backgroundColor = SLColor(255, 253, 224);
        [moneyBtn setTitleColor:SLColor(42, 35, 4) forState:UIControlStateNormal];
        [moneyBtn setTitle:moneyArray[i] forState:UIControlStateNormal];
        moneyBtn.frame = CGRectMake(33 + (marginW + btnWidth) * (i % cols), (marginH + btnHeight) * (i / cols), btnWidth, btnHeight);
        [moneyBtn addTarget:self action:@selector(selMoneyBtn:) forControlEvents:UIControlEventTouchUpInside];
        [moneyView addSubview:moneyBtn];
    }
    [selectView addSubview:moneyView];
    
    UIView *otherMoney = [[UIView alloc] init];
    otherMoney.backgroundColor = SLColor(241, 241, 241);
    otherMoney.layer.borderWidth = 1;//边框宽度
    otherMoney.layer.cornerRadius = 2;
    otherMoney.layer.borderColor = SLColor(226, 229, 233).CGColor;//边框颜色
    otherMoney.layer.masksToBounds = YES;
    [selectView addSubview:otherMoney];
    
    UILabel *waringLabel = [[UILabel alloc] init];
    waringLabel.text = @"最高200元";
    [waringLabel sizeToFit];
    waringLabel.textColor = [UIColor whiteColor];
    [selectView addSubview:waringLabel];
    
    UITextField *otherMoneyTextfield = [[UITextField alloc] init];
    otherMoneyTextfield.placeholder = @"请输入其他金额";
    otherMoneyTextfield.backgroundColor = SLColor(241, 241, 241);
    otherMoneyTextfield.delegate = self;
    otherMoneyTextfield.layer.borderWidth = 1;//边框宽度
    
    otherMoneyTextfield.layer.borderColor = SLColor(226, 229, 233).CGColor;//边框颜色
    otherMoneyTextfield.layer.masksToBounds = YES;
    otherMoneyTextfield.font = [UIFont systemFontOfSize:13];
    otherMoneyTextfield.borderStyle = UITextBorderStyleNone;
    self.otherMoneyTextfield = otherMoneyTextfield;
    self.otherMoneyTextfield.leftViewMode=UITextFieldViewModeAlways;
    
    UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 70, 40)];
    lable.textColor = SLColor(64, 64, 64);
    lable.text = @"  金额(元)";
    lable.font = [UIFont systemFontOfSize:13];
    otherMoneyTextfield.leftView = lable;
    [otherMoney addSubview:otherMoneyTextfield];
  
    
    
    SLTextView *contentView = [[SLTextView alloc] init];
    contentView.placeholder = @"请输入内容";
    contentView.delegate = self;
    contentView.returnKeyType = UIReturnKeyDefault;
    contentView.backgroundColor = [UIColor whiteColor];
    self.contentView = contentView;
    [selectView addSubview:contentView];
    
    UIButton *sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [sendBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sendBtn addTarget:self action:@selector(sendChargeMessage:) forControlEvents:UIControlEventTouchUpInside];
    sendBtn.backgroundColor = SLColor(232, 230, 230);
    [sendBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    sendBtn.layer.cornerRadius = 0;
    sendBtn.layer.masksToBounds = YES;
    self.sendBtn=sendBtn;
    [selectView addSubview:sendBtn];

    [selectView mas_makeConstraints:^(MASConstraintMaker *make) {

        make.centerX.mas_equalTo(_backScrollView.mas_centerX);
        make.centerY.mas_equalTo(_backScrollView.mas_centerY);
        make.width.mas_equalTo(296);
        make.height.mas_equalTo(400);
    }];
    
    [btnOut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(selectView.mas_left).offset(5);
        make.top.mas_equalTo(selectView.mas_top).offset(5);
        make.width.height.mas_equalTo(30);

    }];
    
    [selectLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(selectView.mas_top).offset(28);
        make.centerX.mas_equalTo(selectView.mas_centerX);
    }];
    
    [moneyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(selectLabel.mas_bottom).offset(10);
        make.left.mas_equalTo(selectView.mas_left);
        make.right.mas_equalTo(selectView.mas_right);
        make.height.mas_equalTo(100);
    }];
    
    [otherMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(selectView.mas_left).offset(20);
        make.right.mas_equalTo(selectView.mas_right).offset(-20);
        make.top.mas_equalTo(moneyView.mas_bottom).offset(10);
        make.height.mas_equalTo(40);
    }];
    
 

    [otherMoneyTextfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(otherMoney.mas_left).offset(0);
        make.centerY.mas_equalTo(otherMoney.mas_centerY);
        make.right.mas_equalTo(otherMoney.mas_right);
        make.height.mas_equalTo(40);
    }];

    
    [waringLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(otherMoneyTextfield.mas_bottom).offset(10);
        
        make.centerX.mas_equalTo(selectView.mas_centerX);
        
    }];
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(selectView.mas_left).offset(20);
        make.right.mas_equalTo(selectView.mas_right).offset(-20);
        make.height.mas_equalTo(80);
        make.top.mas_equalTo(waringLabel.mas_bottom).offset(10);
    }];
    
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(selectView.mas_left);
        make.right.mas_equalTo(selectView.mas_right);
        make.bottom.mas_equalTo(selectView.mas_bottom);
        make.height.mas_equalTo(50);
    }];
}

#pragma mark --- 处理键盘退出事件
- (void)handleTap{
    
    [self.backView endEditing:YES];
    
}

- (YWInputViewPluginType)pluginType {
    
    return YWInputViewPluginTypeDefault;
}


#pragma 付费点击事件
- (void)cancelClick
{
    [self.backView removeFromSuperview];
}

#pragma mark 金额按钮点击事件
- (void)selMoneyBtn:(UIButton *)sender
{
    //切换按钮状态
    self.clickedButton.backgroundColor = SLColor(255, 253, 224);
    [self.clickedButton setTitleColor:SLColor(64, 64, 64) forState:UIControlStateNormal];
    self.clickedButton = sender;
    [self.clickedButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.clickedButton.backgroundColor = SLColor(103, 58, 22);
    NSString *string = self.clickedButton.titleLabel.text;
    NSString *moneyStr = [string substringToIndex:string.length - 1];
    self.otherMoneyTextfield.text = moneyStr;
    [self.contentView becomeFirstResponder];
    [self.backScrollView setContentOffset:CGPointMake(0, 216 - margin) animated:YES];

}
//确定按钮被点击
- (void)sendChargeMessage:(UIButton *)sender{
    self.sendBtn.enabled=NO;
    //先将未到时间执行前的任务取消
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(sendMsgBtnClick:) object:sender];
    [self performSelector:@selector(sendMsgBtnClick:) withObject:sender afterDelay:0.2f];
  
}

-(void)sendMsgBtnClick:(UIButton *)sender{
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(sendChargeMessageBtnClick:) object:sender];
    [self performSelector:@selector(sendChargeMessageBtnClick:) withObject:sender afterDelay:0.2f];
}
-(void)sendChargeMessageBtnClick:(UIButton *)sender{

    //如果发送的消息是空，不能发送出去
    if ([self publishVerif] == NO) {
        self.sendBtn.enabled=YES;
        return;
    }
    
    NSString *uuid= [[NSUUID UUID] UUIDString];
    //减去"-"
    NSString *str = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    //变小写
    NSString *lower = [str lowercaseString];
    
    NSDictionary *contentDictionary = @{
                                        @"customizeMsgType":@"1",
                                        @"subMessageType":@"0",
                                        @"content":self.contentView.text,
                                        @"authorId":ApplicationDelegate.userId,
                                        @"msgId":lower,
                                        @"money":_otherMoneyTextfield.text,
                                        };
    NSLog(@"%@", contentDictionary);
    NSData *data = [NSJSONSerialization dataWithJSONObject:contentDictionary
                                                   options:0
                                                     error:NULL];
    NSString *content = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    YWConversationViewController *conversationController = [self conversationViewController];
    __weak __typeof(conversationController) weakController = conversationController;
    /// 构建一个自定义消息
    YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:content summary:@"[付费]"];
    /// 发送该自定义消息
    [conversationController.conversation asyncSendMessageBody:body
                                                     progress:nil
                                                   completion:^(NSError *error, NSString *messageID) {
                                                       
                                                       if (error.code != 0) {
                                                           self.sendBtn.enabled=YES;

                                                           [[SPUtil sharedInstance] showNotificationInViewController:weakController title:@"消息发送失败!" subtitle:nil type:SPMessageNotificationTypeError];
                                                           self.sendBtn.enabled=YES;
                                                           NSLog(@"发送消息失败的代码是%ld------",(long)error.code);
                                                       }else
                                                       {
                                                           //统计付费消息的发送数量
                                                           [MobClick event:@"im_send_feemsg_count"];
                                                           [self.backView removeFromSuperview];
                                                       }
                                                   }];
}
#pragma mark ----- 设置验证字符串只能输入数字（包含小数点后面的两位数字）
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHaveDian = YES;
    
    if ([textField.text rangeOfString:@"."].location == NSNotFound) {
        isHaveDian = NO;
    }
      if ([string length] > 0) {
        
        unichar single = [string characterAtIndex:0];//当前输入的字符
        if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
            //不能输入大于200的金额
            NSString *amount = [NSString stringWithFormat:@"%@%@",textField.text,string];
            float amountCount = [amount floatValue];
            if (amountCount > 200) {
                
                return NO;
            }
           
            //首字母不能为0和小数点1
            if([textField.text length] == 0){
                if(single == '.') {
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            
            //输入的字符是否是小数点
            if (single == '.') {
                if(!isHaveDian)//text中还没有小数点
                {
                    return YES;
                    
                }else{
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                if (isHaveDian) {//存在小数点
                    //判断小数点的位数
                    NSRange ran = [textField.text rangeOfString:@"."];
                    if (range.location - ran.location <= 1) {
                        
                        return YES;
                        
                    }else{
                        return NO;
                    }
                }else{
                    return YES;
                }
            }
        }else{//输入的数据格式不正确
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }
}


#pragma mark ---- 添加验证，如果有是首位是空格不能发布
- (BOOL)publishVerif{
    //空白消息
    if (self.otherMoneyTextfield.text.length == 0 ) {
        [HUDManager showWarningWithText:@"请输入金额!"];
        return NO;
    }
    
    if ([self.otherMoneyTextfield.text isEqualToString:@"0.0"] || [self.otherMoneyTextfield.text isEqualToString:@"0"]) {
        [HUDManager showWarningWithText:@"请输入有效金额!"];
        return NO;
    }
    
    NSString *strContent = [self.contentView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (strContent.length == 0) {
        [HUDManager showWarningWithText:@"请输入内容!"];
        return NO;
    }
    
    if ([self.contentView.text stringByReplacingOccurrencesOfString:@"\n" withString:@""].length == 0) {
        [HUDManager showWarningWithText:@"请输入内容!"];
        return NO;
    }
    return  YES;
}

#pragma mark ---- 需要在选择下一行的时候切换到下一行的输入框

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.otherMoneyTextfield) {
        //如果是输入其他金额的
        [self.contentView becomeFirstResponder];
    }
    
    return YES;
}


@end
