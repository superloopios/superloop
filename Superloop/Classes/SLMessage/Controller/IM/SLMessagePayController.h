//
//  SLMessagePayController.h
//  Superloop
//
//  Created by WangJiwei on 16/5/4.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLMessagePayController : UIViewController
@property (nonatomic, copy) NSString *payMoney;
@property (nonatomic, copy) NSString *userTitle;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *messageId;
@property (nonatomic, copy) NSString *to_user;

//带回来数据是哪一个支付了付费消息
@property (nonatomic ,strong) void(^valueBlcok)();

@end
