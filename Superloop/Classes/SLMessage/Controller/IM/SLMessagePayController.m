//
//  SLMessagePayController.m
//  Superloop
//
//  Created by WangJiwei on 16/5/4.
//  Copyright © 2016年 Superloop. All rights reserved.
//  支付付费消息的接口

#import "SLMessagePayController.h"
#import <Masonry.h>
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "SLTopUpViewController.h"
#import "SLPayMentView.h"
#import <MBProgressHUD.h>
#import "SLReNewPassWordViewController.h"
#import "SLAPIHelper.h"
#import "AESCrypt.h"

@interface SLMessagePayController ()<SLPayMentViewDelegate,UIAlertViewDelegate,UIScrollViewDelegate>

@property (nonatomic, copy) UILabel *moneyLabel;
@property (nonatomic, strong) UIButton *payButton;
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) SLPayMentView *payMent;
//用户是否已经设置了支付密码
@property (nonatomic,strong) NSString *deal_password_status;
@property (nonatomic,assign) BOOL hasNetWork;

@property (nonatomic,copy) NSString *vouchersMoney;//可用现金券
@property (nonatomic,copy) NSString *balanceMoney;//余额

//@property (nonatomic,strong) UIImageView *secondImgView;
//@property (nonatomic,strong) UILabel *couponLab;
//@property (nonatomic,strong) UILabel *couponLabMoneyLab;

@property (nonatomic,strong) UIView *firstView;
@property (nonatomic,strong) UILabel *payMoneyLab;
@property (nonatomic,strong) UILabel *moneyLab;
@property (nonatomic,strong) UIImageView *firstLineImgView;
@property (nonatomic,strong) UILabel *payeeLab;
@property (nonatomic,strong) UILabel *payeeNameLab;
@property (nonatomic,strong) UIImageView *payeeNameImgView;
@property (nonatomic,strong) UILabel *paymentLab;
@property (nonatomic,strong) UIView *secondView;
@property (nonatomic,strong) UILabel *balanceLab;
@property (nonatomic,strong) UILabel *balanceMoneyLab;
@property (nonatomic,strong) UILabel *couponLab;
@property (nonatomic,strong) UIImageView *secondImgView;
@property (nonatomic,strong) UILabel *couponLabMoneyLab;
@property (nonatomic,strong) UIView *thirdView;
@property (nonatomic,strong) UILabel *amountLab;
@property (nonatomic,strong) UILabel *obligationsLab;
@property (nonatomic,strong) UILabel *warnLab;
@end

@implementation SLMessagePayController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = SLColor(240, 240, 240);
    [self setUpNav];
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow withText:@""];
    [self setupNewUI];
    [self getVouchersData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chargeSuccess) name:@"chargeSuccess" object:nil];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"支付";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    [self.view addSubview:navView];
    
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 20, 60, 44);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    //添加分割线
    UIImageView *singleView = [[UIImageView alloc] init];
    singleView.backgroundColor =SLSepatorColor;
    singleView.frame = CGRectMake(0, 63.5, ScreenW, 0.5);
    [navView addSubview:singleView];
    
}

- (void)leftBtnClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setupNewUI{
    self.firstView.backgroundColor = [UIColor whiteColor];
    self.payMoneyLab.textColor = UIColorFromRGB(0x787878);
    self.moneyLab.textColor = SLMainColor;
    self.firstLineImgView.backgroundColor = SLMainColor;
    self.payeeLab.textColor = UIColorFromRGB(0x787878);
    self.payeeNameLab.textColor = UIColorFromRGB(0x1f1f1f);
    [self.payeeNameImgView sd_setImageWithURL:[NSURL URLWithString:self.avatar] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    self.paymentLab.textColor = UIColorFromRGB(0x787878);
    self.secondView.backgroundColor = [UIColor whiteColor];
    self.balanceMoneyLab.textColor = UIColorFromRGB(0x1f1f1f);
    self.payButton.backgroundColor = SLMainColor;
}
- (void)chargeSuccess{
    [self getVouchersData];
}

- (void)getVouchersData{
    
    NSDictionary *dict = [SLUserDefault getUserInfo];
    if (dict[@"userInformation"][@"password"]) {
        //获取个人信息
        [self getUserInfo];
    }else{
        self.deal_password_status = dict[@"userInformation"][@"deal_password_status"];
    }
    [SLAPIHelper getAmountInfoSuccess:^(NSURLSessionDataTask *task, NSDictionary *data) {
        self.balanceLab.textColor = UIColorFromRGB(0x787878);
        //代金券余额
        self.vouchersMoney = [NSString stringWithFormat:@"%f",[data[@"result"][@"voucher_amount"] doubleValue]];
        //账户余额
        self.balanceMoney = [NSString stringWithFormat:@"%f",[data[@"result"][@"account_amount"] doubleValue]];
        [self resetUI];
        [HUDManager hideHUDView];
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        [HUDManager showAlertWithText:@"获取支付信息失败"];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
- (void)getUserInfo{
    
    NSDictionary * pargram = @{@"id":ApplicationDelegate.userId};
    [SLAPIHelper getUsersData:pargram success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        self.deal_password_status = data[@"result"][@"deal_password_status"];
        NSDictionary *dict = [SLUserDefault getUserInfo];
        NSString *basic = dict[@"basic"];
        NSString *Password = dict[@"Password"];
        NSDictionary *newUserInfo = @{@"basic": basic, @"userInformation": data[@"result"],@"Password": Password};
        [SLUserDefault setUserInfo:newUserInfo];
    } failure:^(SLHttpRequestError *failure) {
        
    }];
}
- (void)resetUI{
    float payMoneyFloat = self.payMoney.doubleValue;//付费消息金额
    float balanceMoneyFloat = self.balanceMoney.doubleValue;//账户余额
    float vouchersMoneyFloat = self.vouchersMoney.doubleValue;//代金券金额
    self.moneyLab.text = [NSString stringWithFormat:@"%.1f元",payMoneyFloat];
    self.balanceMoneyLab.text = [NSString stringWithFormat:@"%.2f元",balanceMoneyFloat];
    self.amountLab.text = [NSString stringWithFormat:@"共  %.1f元",payMoneyFloat];
    
    if (self.balanceMoney.doubleValue<0) {//需求更改：2元改为无限制
        self.thirdView.backgroundColor = [UIColor whiteColor];
        if (balanceMoneyFloat  > payMoneyFloat || balanceMoneyFloat  == payMoneyFloat) {
            self.obligationsLab.text = [NSString stringWithFormat:@"余额支付  %.2f元",payMoneyFloat];
            [self.payButton setTitle:@"确认支付" forState:UIControlStateNormal];
        }else{
            self.obligationsLab.text = [NSString stringWithFormat:@"余额不足"];
            [self.payButton setTitle:@"充值" forState:UIControlStateNormal];
        }
        [self.secondView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.paymentLab.mas_bottom).offset(11);
            make.left.mas_equalTo(self.firstView.mas_left);
            make.right.mas_equalTo(self.firstView.mas_right);
            make.height.mas_equalTo(45);
        }];
        if ([self.vouchersMoney doubleValue]>0) {
            
            NSString *attributedStr = [NSString stringWithFormat:@"*你有现金券¥%.2f可用，当你的账户余额大于2元时可使用现金券抵扣，去充值>>",vouchersMoneyFloat];
            //富文本设置
            NSMutableAttributedString *absStr=[[NSMutableAttributedString alloc] initWithString:attributedStr];
            [absStr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xfb2a2a) range:NSMakeRange(0, attributedStr.length-5)];
            [absStr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x0058b1) range:NSMakeRange(attributedStr.length-5, 5)];
            self.warnLab.attributedText=absStr;
            [self.warnLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.thirdView.mas_bottom).offset(10);
                make.centerX.mas_equalTo(self.thirdView.mas_centerX);
                make.width.mas_equalTo(ScreenW-24);
                make.height.mas_equalTo(40);
            }];
            
            [self.payButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.warnLab.mas_bottom).offset(60);
                make.centerX.mas_equalTo(self.thirdView.mas_centerX);
                make.width.mas_equalTo(265);
                make.height.mas_equalTo(45);
            }];
        }else{
            [self.payButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.thirdView.mas_bottom).offset(60);
                make.centerX.mas_equalTo(self.thirdView.mas_centerX);
                make.width.mas_equalTo(265);
                make.height.mas_equalTo(45);
            }];
        }
        
        
        
    }else{
        self.thirdView.backgroundColor = [UIColor whiteColor];
        if ([self.vouchersMoney doubleValue]>0) {
            self.secondImgView.backgroundColor = UIColorFromRGB(0xcfcfcf);
            self.couponLab.textColor = UIColorFromRGB(0x787878);
            
            if (balanceMoneyFloat + vouchersMoneyFloat > payMoneyFloat || balanceMoneyFloat + vouchersMoneyFloat == payMoneyFloat) {
                if (vouchersMoneyFloat>=payMoneyFloat) {
                    self.obligationsLab.text = [NSString stringWithFormat:@"余额支付  0.00元"];
                    self.couponLabMoneyLab.text = [NSString stringWithFormat:@"-%.2f元",payMoneyFloat];
                    self.couponLabMoneyLab.textColor = UIColorFromRGB(0xFB2A2A);
                }else{
                    self.obligationsLab.text = [NSString stringWithFormat:@"余额支付  %.2f元",payMoneyFloat-vouchersMoneyFloat];
                    self.couponLabMoneyLab.text = [NSString stringWithFormat:@"-%.2f元",vouchersMoneyFloat];
                }
                self.obligationsLab.textColor = SLMainColor;
                [self.payButton setTitle:@"确认支付" forState:UIControlStateNormal];
                
            }else{
                self.couponLabMoneyLab.text = [NSString stringWithFormat:@"-%.2f元",vouchersMoneyFloat];
                self.couponLabMoneyLab.textColor = UIColorFromRGB(0xFB2A2A);
                self.obligationsLab.text = [NSString stringWithFormat:@"余额不足"];
                self.obligationsLab.textColor = UIColorFromRGB(0xFB2A2A);
                [self.payButton setTitle:@"充值" forState:UIControlStateNormal];
            }
            [self.secondView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.paymentLab.mas_bottom).offset(11);
                make.left.mas_equalTo(self.firstView.mas_left);
                make.right.mas_equalTo(self.firstView.mas_right);
                make.height.mas_equalTo(90.5);
            }];
            [self.secondImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.secondView.mas_bottom).offset(-45);
                make.left.mas_equalTo(self.secondView.mas_left);
                make.right.mas_equalTo(self.secondView.mas_right);
                make.height.mas_equalTo(0.5);
            }];
            [self.couponLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(self.secondView.mas_bottom).offset(-15);
                make.left.mas_equalTo(self.secondView.mas_left).offset(12);
                make.width.mas_equalTo(100);
                make.height.mas_equalTo(14);
            }];
            [self.couponLabMoneyLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(self.secondView.mas_bottom).offset(-15);
                make.right.mas_equalTo(self.secondView.mas_right).offset(-12);
                make.width.mas_equalTo(ScreenW-150);
                make.height.mas_equalTo(15);
            }];
            
        }else{
            if (balanceMoneyFloat > payMoneyFloat || balanceMoneyFloat == payMoneyFloat) {
                self.obligationsLab.text = [NSString stringWithFormat:@"余额支付  %.2f元",payMoneyFloat];
                self.obligationsLab.textColor = SLMainColor;
                [self.payButton setTitle:@"确认支付" forState:UIControlStateNormal];
            }else{
                self.obligationsLab.text = [NSString stringWithFormat:@"余额不足"];
                self.obligationsLab.textColor = UIColorFromRGB(0xFB2A2A);
                [self.payButton setTitle:@"充值" forState:UIControlStateNormal];
            }
            [self.secondView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.paymentLab.mas_bottom).offset(11);
                make.left.mas_equalTo(self.firstView.mas_left);
                make.right.mas_equalTo(self.firstView.mas_right);
                make.height.mas_equalTo(45);
            }];
        }
        
        [self.payButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.thirdView.mas_bottom).offset(60);
            make.centerX.mas_equalTo(self.thirdView.mas_centerX);
            make.width.mas_equalTo(265);
            make.height.mas_equalTo(45);
        }];
    }
}
- (void)payment{
    
}
#pragma mark --- --  支付按钮被点击事件
- (void)payOrTopUP{
    if ([self.payButton.titleLabel.text isEqualToString:@"确认支付"]) {
        NSLog(@"%@",self.deal_password_status);
        //先判断是否是已经设置过了支付密码
        NSString *str = [NSString stringWithFormat:@"%@",self.deal_password_status];
        
        if ([str isEqualToString:@"1"]) {
            [self keyInPassword];
        }else {
            SLReNewPassWordViewController *firstVc = [[SLReNewPassWordViewController alloc] init];
            firstVc.isComeFromMessageVc = YES;
            firstVc.resetSuccessBlock = ^(BOOL resetSuccess){
                if (resetSuccess) {
                    self.deal_password_status = @"1";
                }
            };
            [self.navigationController pushViewController:firstVc animated:YES];
        }
        
        //        if ([str isEqualToString:@"0"]) {
        //            //如果已经存在密码证明不是第一次登陆,直接进入到设置密码界面
        //            SLReNewPassWordViewController *firstVc = [[SLReNewPassWordViewController alloc] init];
        //            firstVc.isComeFromMessageVc = YES;
        //            [self.navigationController pushViewController:firstVc animated:YES];
        //            return;
        //        }else if ([str isEqualToString:@"1"]){
        //            [self keyInPassword];
        //        }else{
        //            kShowToast(@"请检查网络");
        //        }
    }else{
        SLTopUpViewController *topUp = [[SLTopUpViewController alloc] init];
        topUp.isComeFromMessageVc = YES;
        [self.navigationController pushViewController:topUp animated:YES];
    }
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [HUDManager hideHUDView];
}
- (void)keyInPassword{
    UIView *backView = [[UIView alloc] init];
    backView.frame = [UIScreen mainScreen].bounds;
    backView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    self.backView = backView;
    [self.view addSubview:backView];
    
    SLPayMentView *payMent = [[NSBundle mainBundle] loadNibNamed:@"SLPayMentView" owner:nil options:NULL].firstObject ;
    payMent.Delegate = self;
    NSString *playMoneyStr = [NSString stringWithFormat:@"%@",self.payMoney];
    double strBlance = [playMoneyStr doubleValue];
    payMent.amountCount.font = [UIFont systemFontOfSize:17];
    payMent.amountCount.text = [NSString stringWithFormat:@"(本次支付￥%.1f元)", strBlance];
    self.payMent = payMent;
    [self.payMent.passwordTextfield becomeFirstResponder];
    [self.backView addSubview:payMent];
    
    [payMent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.top.mas_equalTo(self.view.mas_top).offset(144);
        make.width.mas_equalTo(280);
        make.height.mas_equalTo(170);
        
    }];
}


- (void)didcancel{
    [self.view endEditing:YES];
    [self.backView removeFromSuperview];
}


#pragma mark ----------------  付费消息的支付方法
- (void)didInputPassword{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSMutableDictionary *parameters=[NSMutableDictionary new];
    
    //    NSDictionary *parameters =  @{@"messageId": _messageId,@"to_user":@([_to_user floatValue]),@"amount":@([_payMoney floatValue]),@"deal_password":@([_payMent.passwordTextfield.text floatValue])};
    
    //对应的加密密钥
    __block  NSString *security=@"";
    //获取AES加密密钥
    NSString *uuid= [[NSUUID UUID] UUIDString];
    //减去"-"
    NSString *tokenStr = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSDictionary *params=@{@"token":tokenStr};
    [SLAPIHelper getAESSecurity:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        security = data[@"result"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *encryptDeal_password = [AESCrypt encrypt:self.payMent.passwordTextfield.text password:security];
            dispatch_async(dispatch_get_main_queue(), ^{
                [parameters setObject:self.messageId forKey:@"messageId"];
                [parameters setObject:@([self.to_user floatValue]) forKey:@"to_user"];
                [parameters setObject:@([self.payMoney floatValue]) forKey:@"amount"];
                [parameters setObject:encryptDeal_password forKey:@"deal_password"];
                [parameters setObject:tokenStr forKey:@"token"];
                [self encryptSuccess:parameters hud:hud];
                
            });
        });
        
    } failure:^(SLHttpRequestError *failure) {
        [hud hide:YES];
        [HUDManager showWarningWithText:@"支付失败"];
        self.payMent.passwordTextfield.text = nil;
        [self.payMent.passwordTextfield becomeFirstResponder];
        NSLog(@"%@",self.payMent.passwordTextfield.text);
        for (UIView *iv in self.payMent.passwordIndicatorArrary) {
            iv.hidden = YES;
        }
    }];
}
//加密成功后支付
-(void)encryptSuccess:(NSDictionary *)parameters hud:(MBProgressHUD *)hud{
    
    [SLAPIHelper postPay:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSDictionary *msgDictory = [[NSDictionary alloc] init];
        //        NSLog(@"%@",data);
        //支付成功发通知刷新我的页面
        [[NSNotificationCenter defaultCenter] postNotificationName:@"amountChange" object:nil];
        [hud hide:YES];
        if ([data[@"code"] isEqual:@0]) {
            NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
            msgDictory = @{@"paid":@"1",@"valuable":@"0"};
            [settings setObject:msgDictory forKey:self.messageId];
            [settings synchronize];
            if (self.valueBlcok) {
                self.valueBlcok();
            }
            hud.mode = MBProgressHUDModeText;
            hud.labelText = NSLocalizedString(@"支付成功", @"HUD message title");
            [hud hide:YES afterDelay:0.5f];
            hud.userInteractionEnabled = NO;
            [self performSelector:@selector(popController) withObject:nil afterDelay:0.5f];
        }else if ([data[@"code"] isEqual:@603]) {
            
            NSNumber *failed_count = data[@"result"][@"failed_count"];
            NSInteger rest_count = 5-[failed_count integerValue];
            
            [self alertWithCount:rest_count];
            self.payMent.passwordTextfield.text = nil;
            //            [_payMent.passwordTextfield becomeFirstResponder];
            
            for (UIView *iv in _payMent.passwordIndicatorArrary) {
                iv.hidden = YES;
            }
        }
    } failure:^(SLHttpRequestError *failure) {
        //        NSLog(@"%d,%d",failure.slAPICode,failure.httpStatusCode);
        [hud hide:YES];
        
        NSString *failNumber = [NSString stringWithFormat:@"%ld",failure.httpStatusCode];
        
        NSString *errorCodeStr = [NSString stringWithFormat:@"%ld",failure.slAPICode];
        self.payMent.passwordTextfield.text = nil;
        [self.payMent.passwordTextfield becomeFirstResponder];
        
        
        for (UIView *iv in self.payMent.passwordIndicatorArrary) {
            iv.hidden = YES;
        }
        if (failure.slAPICode == 604) {
            [self alertWithCount:0];
            [self didcancel];
        }
        if ([failNumber isEqualToString:@"400"]) {
            
            if ([errorCodeStr isEqualToString:@"601"]) {
                [self didcancel];
                
            }else{
                
                
            }
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"支付失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
            alertView.tag = 100;
            alertView.delegate = self;
            [alertView show];
        }
        
    }];
}
- (void)alertWithCount:(NSInteger)count{
    NSString *message = @"";
    UIAlertView *alertView;
    if (count > 2) {
        message = [NSString stringWithFormat:@"输入错误,您还有%ld次机会",(long)count];
        alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    }else if(count>0){
        [self didcancel];
        NSInteger falseCount = 5-count;
        message = [NSString stringWithFormat:@"您已连续错误%ld次,是否找回密码",(long)falseCount];
        alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles: @"找回密码",nil];
    }else{
        message = [NSString stringWithFormat:@"您已达错误次数限制，请24小时后重试，如有问题请联系客服"];
        alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    }
    alertView.tag = 200;
    [alertView show];
}

- (void)popController
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 300) {
        self.payMent.passwordTextfield.text = @"";
        self.payMent.passwordTextfield.enabled = YES;
        
        for (UIImageView *dotImageView in _payMent.passwordIndicatorArrary)
        {
            dotImageView.hidden = YES;
        }
    }else{
        if (buttonIndex == 1) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                SLReNewPassWordViewController *firstVc = [[SLReNewPassWordViewController alloc] init];
                firstVc.isComeFromMessageVc = YES;
                [self.navigationController pushViewController:firstVc animated:YES];
            });
            
        }
    }
    
}


- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetWork=NO;
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            self.hasNetWork=YES;
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.hasNetWork=YES;
        }
    }
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [UIView animateWithDuration:0.25 animations:^{
        [self.backView removeFromSuperview];
    }];
    
}
#pragma mark - 懒加载
- (UIView *)firstView{
    if (!_firstView) {
        _firstView = [[UIView alloc] init];
        [self.view addSubview:_firstView];
        [_firstView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.view.mas_left);
            make.right.mas_equalTo(self.view.mas_right);
            make.top.mas_equalTo(self.view.mas_top).offset(70);
            make.height.mas_equalTo(90.5);
        }];
    }
    return _firstView;
}
- (UILabel *)payMoneyLab{
    if (!_payMoneyLab) {
        _payMoneyLab = [[UILabel alloc] init];
        _payMoneyLab.text = @"付费消息";
        _payMoneyLab.font = [UIFont systemFontOfSize:14];
        _payMoneyLab.textAlignment = NSTextAlignmentLeft;
        [self.firstView addSubview:_payMoneyLab];
        [_payMoneyLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.firstView.mas_left).offset(12);
            make.top.mas_equalTo(self.firstView.mas_top).offset(15);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(14);
        }];
    }
    return _payMoneyLab;
}
- (UILabel *)moneyLab{
    if (!_moneyLab) {
        _moneyLab = [[UILabel alloc] init];
        _moneyLab.textAlignment = NSTextAlignmentRight;
        [self.firstView addSubview:_moneyLab];
        [_moneyLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.firstView.mas_right).offset(-12);
            make.top.mas_equalTo(self.firstView.mas_top).offset(14);
            make.width.mas_equalTo(ScreenW-100);
            make.height.mas_equalTo(17);
        }];
    }
    return _moneyLab;
}
- (UIImageView *)firstLineImgView{
    if (!_firstLineImgView) {
        _firstLineImgView =[[UIImageView alloc] init];
        [self.firstView addSubview:_firstLineImgView];
        [_firstLineImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.firstView.mas_left);
            make.top.mas_equalTo(self.firstView.mas_top).offset(45);
            make.width.mas_equalTo(ScreenW);
            make.height.mas_equalTo(0.5);
        }];
    }
    return _firstLineImgView;
}
- (UILabel *)payeeLab{
    if (!_payeeLab) {
        _payeeLab =[[UILabel alloc] init];
        _payeeLab.text = @"收款方";
        _payeeLab.font = [UIFont systemFontOfSize:14];
        _payeeLab.textAlignment = NSTextAlignmentLeft;
        [self.firstView addSubview:_payeeLab];
        [_payeeLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.firstView.mas_left).offset(12);
            make.top.mas_equalTo(self.firstLineImgView.mas_top).offset(14);
            make.width.mas_equalTo(50);
            make.height.mas_equalTo(14);
        }];
    }
    return _payeeLab;
}
- (UILabel *)payeeNameLab{
    if (!_payeeNameLab) {
        _payeeNameLab = [[UILabel alloc] init];
        _payeeNameLab.textAlignment = NSTextAlignmentRight;
        _payeeNameLab.font = [UIFont systemFontOfSize:15];
        _payeeNameLab.text = self.userTitle;
        [self.firstView addSubview:_payeeNameLab];
        
        //根据计算文字的大小
        CGFloat payeeNameWidth= [self.userTitle boundingRectWithSize:CGSizeMake(ScreenW-96, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:15]} context:nil].size.width+5;
        
        if (payeeNameWidth>ScreenW - 120) {
            payeeNameWidth = ScreenW - 120;
        }
        [_payeeNameLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.firstView.mas_right).offset(-12);
            make.top.mas_equalTo(self.firstLineImgView.mas_bottom).offset(15);
            make.width.mas_equalTo(payeeNameWidth);
            make.height.mas_equalTo(15);
        }];
    }
    return _payeeNameLab;
}

- (UIImageView *)payeeNameImgView{
    if (!_payeeNameImgView) {
        _payeeNameImgView = [[UIImageView alloc] init];
        _payeeNameImgView.layer.masksToBounds = YES;
        _payeeNameImgView.layer.cornerRadius = 2.5;
        [self.firstView addSubview:_payeeNameImgView];
        [_payeeNameImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.firstLineImgView.mas_bottom).offset(14);
            make.right.mas_equalTo(self.payeeNameLab.mas_left).offset(-5);
            make.width.mas_equalTo(17);
            make.height.mas_equalTo(17);
        }];
    }
    return _payeeNameImgView;
}
- (UILabel *)paymentLab{
    if (!_paymentLab) {
        _paymentLab =[[UILabel alloc] init];
        _paymentLab.text = @"支付方式";
        _paymentLab.font = [UIFont systemFontOfSize:14];
        _paymentLab.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:_paymentLab];
        [_paymentLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.firstView.mas_bottom).offset(11);
            make.left.mas_equalTo(self.firstView.mas_left).offset(12);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(14);
        }];
    }
    return _paymentLab;
}
- (UIView *)secondView{
    if (!_secondView) {
        _secondView = [[UIView alloc] init];
        [self.view addSubview:_secondView];
    }
    return _secondView;
}
- (UILabel *)balanceLab{
    if (!_balanceLab) {
        _balanceLab =[[UILabel alloc] init];
        _balanceLab.text = @"余额";
        _balanceLab.font = [UIFont systemFontOfSize:14];
        _balanceLab.textAlignment = NSTextAlignmentLeft;
        [self.secondView addSubview:_balanceLab];
        [_balanceLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.secondView.mas_top).offset(15);
            make.left.mas_equalTo(self.secondView.mas_left).offset(12);
            make.width.mas_equalTo(50);
            make.height.mas_equalTo(14);
        }];
    }
    return _balanceLab;
}
- (UILabel *)balanceMoneyLab{
    if (!_balanceMoneyLab) {
        _balanceMoneyLab = [[UILabel alloc] init];
        _balanceMoneyLab.textAlignment = NSTextAlignmentRight;
        _balanceMoneyLab.font = [UIFont systemFontOfSize:15];
        _balanceMoneyLab.text = [NSString stringWithFormat:@"¥ %.2f",[self.balanceMoney doubleValue]];
        [self.secondView addSubview:_balanceMoneyLab];
        [_balanceMoneyLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.secondView.mas_top).offset(15);
            make.right.mas_equalTo(self.secondView.mas_right).offset(-12);
            make.width.mas_equalTo(ScreenW-100);
            make.height.mas_equalTo(14);
        }];
    }
    return _balanceMoneyLab;
}

- (UIImageView *)secondImgView{
    if (!_secondImgView) {
        _secondImgView = [[UIImageView alloc] init];
        [self.secondView addSubview:_secondImgView];
    }
    return _secondImgView;
}
- (UILabel *)couponLab{
    if (!_couponLab) {
        _couponLab =[[UILabel alloc] init];
        _couponLab.text = @"现金券抵扣";
        _couponLab.font = [UIFont systemFontOfSize:14];
        _couponLab.textAlignment = NSTextAlignmentLeft;
        [self.secondView addSubview:_couponLab];
    }
    return _couponLab;
}
- (UILabel *)couponLabMoneyLab{
    if (!_couponLabMoneyLab) {
        _couponLabMoneyLab = [[UILabel alloc] init];
        _couponLabMoneyLab.textAlignment = NSTextAlignmentRight;
        _couponLabMoneyLab.font = [UIFont systemFontOfSize:15];
        [self.secondView addSubview:_couponLabMoneyLab];
    }
    return _couponLabMoneyLab;
}
- (UIView *)thirdView{
    if (!_thirdView) {
        _thirdView = [[UIView alloc] init];
        [self.view addSubview:_thirdView];
        [_thirdView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.secondView.mas_bottom).offset(10);
            make.left.mas_equalTo(self.view.mas_left);
            make.right.mas_equalTo(self.view.mas_right);
            make.height.mas_equalTo(45);
        }];
    }
    return _thirdView;
}
- (UILabel *)amountLab{
    if (!_amountLab) {
        _amountLab =[[UILabel alloc] init];
        _amountLab.font = [UIFont systemFontOfSize:14];
        _amountLab.textAlignment = NSTextAlignmentLeft;
        [self.thirdView addSubview:_amountLab];
        [_amountLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.thirdView.mas_top).offset(15);
            make.left.mas_equalTo(self.thirdView.mas_left).offset(12);
            make.width.mas_equalTo(ScreenW/2.0);
            make.height.mas_equalTo(14);
        }];
    }
    return _amountLab;
}
- (UILabel *)obligationsLab{
    if (!_obligationsLab) {
        _obligationsLab = [[UILabel alloc] init];
        _obligationsLab.textAlignment = NSTextAlignmentRight;
        _obligationsLab.font = [UIFont systemFontOfSize:15];
        [self.thirdView addSubview:_obligationsLab];
        [_obligationsLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.thirdView.mas_top).offset(15);
            make.right.mas_equalTo(self.thirdView.mas_right).offset(-12);
            make.width.mas_equalTo(ScreenW/2.0);
            make.height.mas_equalTo(15);
        }];
    }
    return _obligationsLab;
}
- (UIButton *)payButton{
    if (!_payButton) {
        _payButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_payButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _payButton.layer.cornerRadius = 5;
        _payButton.layer.borderColor = SLColor(165, 165, 165).CGColor;
        _payButton.layer.borderWidth = 0.5;
        _payButton.layer.masksToBounds = YES;
        [_payButton addTarget:self action:@selector(payOrTopUP) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_payButton];
        
    }
    return _payButton;
}
- (UILabel *)warnLab{
    if (!_warnLab) {
        _warnLab = [[UILabel alloc] init];
        _warnLab.numberOfLines = 0;
        _warnLab.font = [UIFont systemFontOfSize:14];
        _warnLab.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:_warnLab];
        _warnLab.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recharge)];
        [_warnLab addGestureRecognizer:tap];
    }
    return _warnLab;
}
- (void)recharge{
    SLTopUpViewController *topUp = [[SLTopUpViewController alloc] init];
    topUp.isComeFromMessageVc = YES;
    [self.navigationController pushViewController:topUp animated:YES];
}

@end
