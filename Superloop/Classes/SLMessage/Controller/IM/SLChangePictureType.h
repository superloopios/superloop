//
//  SLChangePictureType.h
//  Superloop
//
//  Created by 朱宏伟 on 16/5/22.
//  Copyright © 2016年 Superloop. All rights reserved.
//  kvo  model 切换听筒状态时使用的kvo模型

#import <Foundation/Foundation.h>

@interface SLChangePictureType : NSObject

@property (nonatomic,strong)NSString *picOne;

@end
