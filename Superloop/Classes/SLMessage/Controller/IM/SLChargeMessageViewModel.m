//
//  SLChargeMessageViewModel.m
//  Superloop
//
//  Created by WangJiwei on 16/4/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLChargeMessageViewModel.h"
#import "SPUtil.h"
@implementation SLChargeMessageViewModel
- (instancetype)initWithMessage:(id<IYWMessage>)aMessage
{
    self = [super init];
    if (self) {
        YWMessageBodyCustomize *bodyCustomize = (YWMessageBodyCustomize *)[aMessage messageBody];
        
        NSData *contentData = [bodyCustomize.content dataUsingEncoding:NSUTF8StringEncoding];
        
        NSDictionary *contentDictionary = [NSJSONSerialization JSONObjectWithData:contentData
                                                                        options:0
                                                                        error:NULL];
//        NSLog(@"%@",contentDictionary);
        //创建保持消息id字典的存储
        NSUserDefaults *stands = [NSUserDefaults standardUserDefaults];
        [stands setObject:contentDictionary[@"msgId"] forKey:@"msgId"];
         [stands synchronize];
        _content = contentDictionary[@"content"];
        _moneyText = contentDictionary[@"money"];
       _authorId = contentDictionary[@"authorId"];
        _msgId = contentDictionary[@"msgId"];
        _messageId = [aMessage messageId];
        self.bubbleStyle = [aMessage outgoing] ? BubbleStyleCommonRight : BubbleStyleCommonLeft;
        self.layout = [aMessage outgoing] ? WXOBubbleLayoutRight : WXOBubbleLayoutLeft;
        _color = [aMessage outgoing] ? [UIColor whiteColor] : [UIColor blackColor];
    }
    return self;
}
@end
