//
//  SLMessageManger.m
//  Superloop
//
//  Created by 朱宏伟 on 16/7/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//  聊天管理类

#import "SLMessageManger.h"
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "HomePageViewController.h"
#import "SLAPIHelper.h"
#import "SLMessageManger.h"
#import <AVFoundation/AVFoundation.h>
#import <WXOpenIMSDKFMWK/YWFMWK.h>
#import <WXOUIModule/YWUIFMWK.h>
#import "SPUtil.h"
#import "SPBaseBubbleChatViewCustomize.h"
#import "SPBubbleViewModelCustomize.h"
#import "SPInputViewPluginGreeting.h"
#import "SPInputViewPluginCallingCard.h"
#import "SPInputViewPluginTransparent.h"
#import "SLChargeMessage.h"
#import <WXOUIModule/YWIndicator.h>
#import <objc/runtime.h>
#import <WXOpenIMSDKFMWK/YWTribeSystemConversation.h>
#import "SLTabBarViewController.h"
#import "SLMessageViewController.h"
#import "SLNavgationViewController.h"
#import "SLCallJudgeViewController.h"
#import "SLComplainViewController.h"

#if __has_include("SPContactProfileController.h")
#import "SPContactProfileController.h"
#endif

#if __has_include("SPTribeConversationViewController.h")

#import "SPTribeConversationViewController.h" // 使用了继承方式，实现群聊聊天页面。
#endif

#if __has_include("SPMessageInputView.h")

#import "SPMessageInputView.h"  // 使用了继承方式，实现群聊聊天页面。
#endif

#import "SPCallingCardBubbleViewModel.h"
#import "SPCallingCardBubbleChatView.h"
#import "SLValueMessageModel.h"
#import "SLChargeMessageViewModel.h"
#import "SLChargeMessageView.h"
#import "SLSeeMessageView.h"
#import "SLJudgeMessageView.h"
#import "SPGreetingBubbleViewModel.h"
#import "SPGreetingBubbleChatView.h"
#import "SLMessagePayController.h"
#import "SLFirstViewController.h"
#import "SLMessageTitleButton.h"
#import "SLChangePictureType.h"
#import "CHAudioPlayer.h"
#import "SLUserDefault.h"
#import "SLTopicViewController.h"
#import "SLContactViewController.h"
#import "SLMessageViewController.h"
#import "SLNavgationViewController.h"
#import <Bugly/Bugly.h>

NSString *const kSPCustomConversationIdForPortal = @"ywcustom007";
NSString *const kSPCustomConversationIdForFAQ = @"ywcustom008";

@interface SLMessageManger ()<YWMessageLifeDelegate, UIAlertViewDelegate>{
}

#define kSPAlertViewTagPhoneCall 2046
#define SLAlertViewTagLoginoutOrAgainLogin 2047

- (BOOL)isPreLogined;  //是否已经预登录进入
@property (nonatomic,strong) NSString *messageId; //会话id
@property (nonatomic,strong) SLMessageTitleButton *btn;  //会话消息标题上面的选择titleView的切换图片
@property (nonatomic,strong)SLChangePictureType *picType; //模型
@property (nonatomic,strong)NSMutableDictionary *feeMessageViewDict; //付费消息的状态
@property (nonatomic,strong)NSString *myName; //对方昵称
@property (nonatomic,strong)NSString *avatar; //对方的头像

@property (nonatomic,weak)YWConversationViewController *controller;//全局的控制器
@property (nonatomic,weak)SLChargeMessageView *chatView;
@property (nonatomic,strong)UIView *guideView;

@property (nonatomic, assign)BOOL isFirstEntry;
@property (nonatomic,strong)UIViewController *rootVC;
//需要请求的messageId数组
@property (nonatomic,strong)NSMutableArray *loadMessageArr;

//需要请求的messageId数组
@property (nonatomic,strong)NSMutableArray *currentLoadMessageArr;

@property (nonatomic, assign)NSInteger lastLoadTime;

@property (nonatomic, strong)NSOperationQueue *queue;


@property (nonatomic,assign) BOOL  aIsSuccess;
@property (nonatomic,strong) YWProfileItem *item;

@end
@implementation SLMessageManger

- (NSMutableArray *)currentLoadMessageArr{
    if (!_currentLoadMessageArr) {
        _currentLoadMessageArr = [NSMutableArray array];
    }
    return _currentLoadMessageArr;
}

- (NSMutableArray *)loadMessageArr{
    if (!_loadMessageArr) {
        _loadMessageArr = [NSMutableArray array];
    }
    return _loadMessageArr;
}

#pragma mark - lifeCycle
- (id)init {
    self = [super init];
    if (self) {
        // 初始化
        [self setLastConnectionStatus:YWIMConnectionStatusDisconnected];
        self.picType = [[SLChangePictureType alloc] init];
        [self.picType addObserver:self forKeyPath:@"picOne" options:NSKeyValueObservingOptionNew context:nil];
        _feeMessageViewDict = [[NSMutableDictionary alloc] init];
        
    }
    self.queue = [[NSOperationQueue alloc] init];
    self.queue.maxConcurrentOperationCount = 1;
    return self;
}

#pragma mark - properties
- (id<UIApplicationDelegate>)appDelegate
{
    return [UIApplication sharedApplication].delegate;
}

- (UIWindow *)rootWindow
{
    UIWindow *result = nil;
    
    do {
        if ([self.appDelegate respondsToSelector:@selector(window)]) {
            result = [self.appDelegate window];
        }
        if (result) {
            break;
        }
    } while (NO);
    
    NSAssert(result, @"检查- [SLMessageManger rootWindow]中的实现，是否符合App结构");
    return result;
    
}

- (UINavigationController *)conversationNavigationController {
    
    SLTabBarViewController *tabBarController = (SLTabBarViewController *)self.rootWindow.rootViewController;
    if (![tabBarController isKindOfClass:[SLTabBarViewController class]]) {
        return nil;
    }
    
    SLNavgationViewController *navigationController = tabBarController.viewControllers.firstObject;
    if (![navigationController isKindOfClass:[SLNavgationViewController class]]) {
        navigationController = nil;
        NSAssert(navigationController, @"如果在您的 App 中出现这个断言失败，请参考：【https://bbs.aliyun.com/read.php?spm=0.0.0.0.ia5H4C&tid=263177&displayMode=1&page=1&toread=1#tpc】");
    }
    return navigationController;
}

#pragma mark - public methods
+ (instancetype)sharedInstance
{
    static SLMessageManger *smanger = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        smanger = [[SLMessageManger alloc] init];
    });
    
    return smanger;
}
#pragma mark - SDK Life Control

//程序完成启动，在appdelegate中的 application:didFinishLaunchingWithOptions:一开始的地方调用

- (void)callThisInDidFinishLaunching{
    if ([self openImInit]) {
        // 在IMSDK截获到Push通知并需要您处理Push时，IMSDK会自动调用此回调
        [self handleAPNSPush];
        
        // 自定义全局导航栏
        //        [self customGlobleNavigationBar];
        
        
        /// 监听消息生命周期回调
        [self listenMyMessageLife];
        
    } else {
        //sdk初始化失败
    }
}

/**
 *  用户在应用的服务器登录成功之后，向云旺服务器登录之前调用
 *  @param ywLoginId, 用来登录云旺IMSDK的id
 *  @param password, 用来登录云旺IMSDK的密码
 *  @param aSuccessBlock, 登陆成功的回调
 *  @param aFailedBlock, 登录失败的回调
 */
- (void)callThisAfterISVAccountLoginSuccessWithYWLoginId:(NSString *)ywLoginId passWord:(NSString *)passWord preloginedBlock:(void(^)())aPreloginedBlock successBlock:(void(^)())aSuccessBlock failedBlock:(void (^)(NSError *))aFailedBlock
{
    // 监听连接状态
    [self listenConnectionStatus];
    
    // 设置声音播放模式
    [self setAudioCategory];
    // 设置头像和昵称
    [self setAvatarStyle];
    
    // 设置最大气泡宽度
    [self setMaxBubbleWidth];
    
    // 监听新消息
    [self listenNewMessage];
    
    // 设置提示
    [self setNotificationBlock];
    
    // 监听头像点击事件
    [self listenOnClickAvatar];
    
    
    // 监听链接点击事件
    [self listenOnClickUrl];
    
    // 监听预览大图事件
    [self listenOnPreviewImage];
    
    // 自定义皮肤
    [self customUISkin];
    
    // 开启单聊已读未读状态显示
    [self enableReadFlag];
    
    if ([ywLoginId length] > 0 && [passWord length] > 0) {
        /// 预登陆
        [self preLoginWithLoginId:ywLoginId successBlock:aPreloginedBlock];
        
        /// 真正登录
        [self loginWithUserID:ywLoginId password:passWord successBlock:aSuccessBlock failedBlock:aFailedBlock];
    } else {
        if (aFailedBlock) {
            aFailedBlock([NSError errorWithDomain:YWLoginServiceDomain code:YWLoginErrorCodePasswordError userInfo:nil]);
        }
    }
}

//用户即将退出登录时调用

- (void)callThisBeforeISVAccountLogout
{
    [self logout];
}

#pragma mark - basic
- (NSNumber *)lastEnvironment
{
    NSNumber *environment = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastEnvironment"];
    if (environment == nil) {
        return @(YWEnvironmentRelease);
    }
    return environment;
}



/**
 *  设置证书名的示例代码
 */
- (void)exampleSetCertName
{
    /// 你可以根据当前的bundleId，设置不同的证书，避免修改代码
    
    /// 这些证书是我们在百川后台添加的。
    if ([[NSBundle mainBundle].bundleIdentifier isEqualToString:@"com.superloop.chaojiquan"]) {
        [[[YWAPI sharedInstance] getGlobalPushService] setXPushCertName:@"sandbox"];
    } else {
        /// 默认的情况下，我们都设置为生产证书
        [[[YWAPI sharedInstance] getGlobalPushService] setXPushCertName:@"production"];
    }
    
}


//初始化

- (BOOL)openImInit;
{
    // 开启日志
    [[YWAPI sharedInstance] setLogEnabled:NO];
    
    NSLog(@"SDKVersion:%@", [YWAPI sharedInstance].YWSDKIdentifier);
    
    NSError *error = nil;
    // 异步初始化IM SDK
    //默认是 线上环境 YWEnvironmentRelease，OpenIM 暂时不提供测试环境，需要测试环境的，另外申请一个Appkey作为测试环境
    
#ifdef DEBUG
    
    [[YWAPI sharedInstance] syncInitWithOwnAppKey:@"23390148" getError:&error]; //线下
    
#else
    
    [[YWAPI sharedInstance] syncInitWithOwnAppKey:@"23329364" getError:&error];  //线上
    
#endif
    
    if (error.code != 0 && error.code != YWSdkInitErrorCodeAlreadyInited) {
        // 初始化失败
        return NO;
    } else {
        if (error.code == 0) {
            // 首次初始化成功
            // 获取一个IMKit并持有
            self.ywIMKit = [[YWAPI sharedInstance] fetchIMKitForOpenIM];
            [[self.ywIMKit.IMCore getContactService] setEnableContactOnlineStatus:NO];
            
        } else {
            // 已经初始化
        }
        return YES;
    }
}


//登录
- (void)loginWithUserID:(NSString *)aUserID password:(NSString *)aPassword successBlock:(void(^)())aSuccessBlock failedBlock:(void (^)(NSError *))aFailedBlock
{
    __weak typeof(self) weakSelf = self;
    aSuccessBlock = [aSuccessBlock copy];
    aFailedBlock = [aFailedBlock copy];
    
    // 登录之前，先告诉IM如何获取登录信息。
    // 当IM向服务器发起登录请求之前，会调用这个block，来获取用户名和密码信息。
    [[self.ywIMKit.IMCore getLoginService] setFetchLoginInfoBlock:^(YWFetchLoginInfoCompletionBlock aCompletionBlock) {
        aCompletionBlock(YES, aUserID, aPassword, nil, nil);
    }];
    
    // 发起登录
    [[self.ywIMKit.IMCore getLoginService] asyncLoginWithCompletionBlock:^(NSError *aError, NSDictionary *aResult) {
        if (aError.code == 0 || [[self.ywIMKit.IMCore getLoginService] isCurrentLogined]) {
            // 登录成功
            // 添加长期置顶的自定义会话
            [weakSelf addHighPriorityCustomConversation];
            
            if (aSuccessBlock) {
                aSuccessBlock();
            }
        } else {
            // 登录失败
            if (aFailedBlock) {
                aFailedBlock(aError);
                NSException *exception = [NSException exceptionWithName:@"openIMLoginError"
                                                                 reason:aError.description
                                                               userInfo:nil];
                [Bugly reportException:exception];
            }
        }
    }];
}

//预登陆
- (void)preLoginWithLoginId:(NSString *)loginId successBlock:(void(^)())aPreloginedBlock
{
    // 预登录
    if ([[self.ywIMKit.IMCore getLoginService] preLoginWithPerson:[[YWPerson alloc] initWithPersonId:loginId]]) {
        // 预登录成功，直接进入页面,这里可以打开界面
        if (aPreloginedBlock) {
            aPreloginedBlock();
        }
    }
}

//是否已经预登录进入
- (BOOL)isPreLogined
{
    //判断是否已经进入IM主页面
    return [self.rootWindow.rootViewController isKindOfClass:[UITabBarController class]];
}

//监听连接状态
- (void)listenConnectionStatus
{
    __weak typeof(self) weakSelf = self;
    [[self.ywIMKit.IMCore getLoginService] addConnectionStatusChangedBlock:^(YWIMConnectionStatus aStatus, NSError *aError) {
        [weakSelf setLastConnectionStatus:aStatus];
        if (aStatus == YWIMConnectionStatusDisconnected) {
            //失去链接
            [HUDManager showWarningWithText:@"请检查你的网络设置!"];
            return ;
        }
        if (aStatus == YWIMConnectionStatusForceLogout) {
            [SLUserDefault removeUserInfo];
            ApplicationDelegate.Basic = NULL;
            ApplicationDelegate.userInformation = NULL;
            ApplicationDelegate.userId = NULL;
            [self clearAllUserDefaultsData];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"下线通知，您的账号被迫下线或已在其他设备登录" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"重新登录", nil];
            alertView.tag=SLAlertViewTagLoginoutOrAgainLogin;
            [alertView show];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"exitSuperLoop" object:nil];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"jumptoHome" object:self];
        }
        if (aStatus == YWIMConnectionStatusConnected) {
            // 监听群系统消息
        }
        
    } forKey:[self description] ofPriority:YWBlockPriorityDeveloper];
}

//注销的示例代码
- (void)logout
{
    [[self.ywIMKit.IMCore getLoginService] asyncLogoutWithCompletionBlock:NULL];
}

//设置声音的播放状状态
- (void)setAudioCategory
{
    // 设置为扬声器模式，这样可以支持靠近耳朵时自动切换到听筒
    [self.ywIMKit setAudioSessionCategory:AVAudioSessionCategoryPlayback];
}
// 设置头像和昵称
- (void)setAvatarStyle
{
    [self.ywIMKit setAvatarImageViewCornerRadius:5];
    [self.ywIMKit setAvatarImageViewContentMode:UIViewContentModeScaleAspectFill];
    /// 在这里设置客服的显示名称
    [self.ywIMKit setFetchProfileForEServiceBlock:^(YWPerson *aPerson, YWProfileProgressBlock aProgressBlock, YWProfileCompletionBlock aCompletionBlock) {
        YWProfileItem *item = [[YWProfileItem alloc] init];
        item.person = aPerson;
        item.displayName = aPerson.personId;
        item.avatar = [UIImage imageNamed:@"customerService"];
        aCompletionBlock(YES, item);
    }];
    NSString *loginId = [NSString stringWithFormat:@"%@",ApplicationDelegate.userId];
    [self.ywIMKit removeCachedProfileForPerson:[[YWPerson alloc] initWithPersonId:loginId]];
}

//标题设置-------创建会话列表页面
- (YWConversationListViewController *)makeConversationListControllerWithSelectItemBlock:(YWConversationsListDidSelectItemBlock)aSelectItemBlock
{
//    YWConversationListViewController *result = [self.ywIMKit makeConversationListViewController];
   YWConversationListViewController *result = [YWConversationListViewController makeControllerWithIMKit:[SLMessageManger sharedInstance].ywIMKit];
    [result setDidSelectItemBlock:aSelectItemBlock];
    
    // 自定义会话Cell
    [self customizeConversationCellWithConversationListController:result];
    return result;
}

// 打开某个会话  (消息会话界面控制器)
- (void)openConversationViewControllerWithConversation:(YWConversation *)aConversation fromNavigationController:(UINavigationController *)aNavigationController
{
    
    UINavigationController *conversationNavigationController = nil;
    if (aNavigationController) {
        //存在导航控制器
        conversationNavigationController = aNavigationController;
    }
    else {
        //不存在导航控制器
        conversationNavigationController = [self conversationNavigationController];
    }
    
    //实际上是viewController
    __block YWConversationViewController *conversationViewController = nil;
    
    //如果不存在viewController
    if (!conversationViewController) {
        conversationViewController = [self makeConversationViewControllerWithConversation:aConversation];
    }
    
    NSArray *viewControllers = nil;
    //如果导航控制器存在多个子控制器,并且是conversationViewController
    if (conversationNavigationController.viewControllers.firstObject == conversationViewController) {
        viewControllers = @[conversationNavigationController.viewControllers.firstObject];
    }
    else {
        //导航控制器的第一个控制器，不是conversationViewController类型的控制器
        viewControllers = @[conversationNavigationController.viewControllers.firstObject, conversationViewController];
        NSMutableArray *array = [NSMutableArray new];
        NSArray *existControllers = conversationNavigationController.viewControllers;
        for (UIViewController* vc in existControllers) {
            [array addObject:vc];
        }
        [array addObject:conversationViewController];
        viewControllers = array;
        
    }
    //设置控制器
    __weak typeof(conversationViewController) weakConversationViewController = conversationViewController;
    [conversationNavigationController setViewControllers:viewControllers animated:YES];
    [conversationViewController setViewWillAppearBlock:^(BOOL aAnimated) {
        weakConversationViewController.tableView.y  = 64;
        weakConversationViewController.tableView.height = ScreenH - 64;
        [conversationNavigationController setNavigationBarHidden:YES animated:NO];
        //        [weakConversationViewController.tableView setContentOffset:CGPointMake(0, weakConversationViewController.tableView.contentSize.height -weakConversationViewController.tableView.height) animated:NO];
    }];
    [self addGuideView:conversationViewController];
}

//收费消息的引导界面
- (void)addGuideView:(YWConversationViewController*)conversationVc{
    //添加遮盖View
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"guideSave"]) {
        
        UIView *coverView = [[UIView alloc] init];
        self.guideView = coverView;
        coverView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        coverView.frame = conversationVc.view.bounds;
        UIButton *guideBtn = [[UIButton alloc] init];
        [guideBtn addTarget:self action:@selector(guideBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [guideBtn setImage:[UIImage imageNamed:@"yindao"] forState:UIControlStateNormal];
        [guideBtn setImage:[UIImage imageNamed:@"yindao"] forState:UIControlStateSelected];
        [coverView addSubview:guideBtn];
        UIWindow * window = [UIApplication sharedApplication].windows[0];
        [window addSubview:coverView];
        [guideBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(coverView.mas_left).offset(30);
            make.right.equalTo(coverView.mas_right).offset(-10);
            make.height.mas_equalTo(227);
            make.bottom.equalTo(coverView.mas_bottom).offset(-45);
        }];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"guideSave"];
        
    }
}

- (void)guideBtnClick{
    
    [self.guideView removeFromSuperview];
    
}

//获取EService对象
- (YWPerson *)fetchEServicePersonWithPersonId:(NSString *)aPersonId groupId:(NSString *)aGroupId
{
    YWPerson *person = [[YWPerson alloc] initWithPersonId:aPersonId EServiceGroupId:aGroupId baseContext:self.ywIMKit.IMCore];
    //控制锁定某个子账号，不分流。
    //[person setLockShunt:YES];
    return person;
}

//打开单聊界面
- (void)openConversationViewControllerWithPerson:(YWPerson *)aPerson fromNavigationController:(UINavigationController *)aNavigationController
{
    YWConversation *conversation = [YWP2PConversation fetchConversationByPerson:aPerson creatIfNotExist:YES baseContext:self.ywIMKit.IMCore];
    
    [self openConversationViewControllerWithConversation:conversation fromNavigationController:aNavigationController];
}
//打开群聊界面

- (void)openConversationViewControllerWithTribe:(YWTribe *)aTribe fromNavigationController:(UINavigationController *)aNavigationController
{
    YWConversation *conversation = [YWTribeConversation fetchConversationByTribe:aTribe createIfNotExist:YES baseContext:self.ywIMKit.IMCore];
    
    [self openConversationViewControllerWithConversation:conversation fromNavigationController:aNavigationController];
}

- (void)openEServiceConversationWithPersonId:(NSString *)aPersonId fromNavigationController:(UINavigationController *)aNavigationController
{
    YWPerson *person = [[SLMessageManger sharedInstance] fetchEServicePersonWithPersonId:aPersonId groupId:nil];
    [[SLMessageManger sharedInstance] openConversationViewControllerWithPerson:person fromNavigationController:aNavigationController];
}

-(NSString *)getConversationName:(YWConversation *)conversation {
    YWProfileItem *item = [[[SLMessageManger sharedInstance].ywIMKit.IMCore getContactService] getProfileForPerson:((YWP2PConversation *)conversation).person withTribe:nil];
    return item.displayName;
}

// 创建某个会话Controller，在这仅用于iPad SplitController中显示会话
- (YWConversationViewController *)makeConversationViewControllerWithConversation:(YWConversation *)conversation {
    YWConversationViewController *conversationController = nil;
#if __has_include("SPTribeConversationViewController.h")
    // Demo中使用了继承方式，实现群聊聊天页面。
    if ([conversation isKindOfClass:[YWTribeConversation class]]) {
        conversationController = [SPTribeConversationViewController makeControllerWithIMKit:self.ywIMKit
                                                                               conversation:conversation];
        [self.ywIMKit addDefaultInputViewPluginsToMessagesListController:conversationController];
    }
    else
#endif
    {
        //  创建聊天(单聊)控制器
        conversationController = [YWConversationViewController makeControllerWithIMKit:self.ywIMKit conversation:conversation];
        conversationController.disableTitleOnlineDisplay = YES;
        conversationController.disableTitleAutoConfig = YES;
        [self setNavView:conversationController conversation:conversation];
#pragma mark --------- 设置中间title的切换接听模式的状态
        [self.ywIMKit addDefaultInputViewPluginsToMessagesListController:conversationController];
        
    }
#if  __has_include("SPContactProfileController.h")
    if ([conversation isKindOfClass:[YWP2PConversation class]]) {
        __weak typeof(self) weakSelf = self;
        __weak YWConversationViewController *weakController = conversationController;
        conversationController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"设置" style:UIBarButtonItemStylePlain andBlock:^{
            SPContactProfileController *profileController = [[SPContactProfileController alloc] initWithContact:((YWP2PConversation *)conversation).person IMKit:weakSelf.ywIMKit];
            [weakController presentViewController:profileController animated:YES completion:nil];
        }];
    }
#endif
    // 添加自定义插件
    [self addInputViewPluginToConversationController:conversationController];
    
    // 设置显示自定义消息
    [self showCustomMessageWithConversationController:conversationController];
    
    // 设置消息长按菜单
    [self setMessageMenuToConversationController:conversationController];
    
    conversationController.hidesBottomBarWhenPushed = YES;
    
    return conversationController;
}
//判断字符串是不是整型
- (BOOL)isPureInt:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}

//设置导航条的标题文字
-(void)setNavView:(YWConversationViewController *)converStationVc conversation:(YWConversation *)conversation{
    self.controller = converStationVc;
    YWProfileItem *profileItem = [[[SLMessageManger sharedInstance].ywIMKit.IMCore getContactService] getProfileForPerson:((YWP2PConversation *)conversation).person withTribe:nil];
    NSString *title = profileItem.displayName;
    self.myName = title;
    self.avatar = profileItem.avatarUrl;
    if (ApplicationDelegate.imTitlename) {
        [self setNavNickname:converStationVc item:ApplicationDelegate.imTitlename];
    }else{
        [self setNavNickname:converStationVc item:title];
    }
//    if ([self isPureInt:profileItem.displayName]) {
//        
//        [[self.ywIMKit.IMCore getContactService] asyncGetProfileFromServerForPerson:((YWP2PConversation *)conversation).person withTribe:nil withProgress:nil andCompletionBlock:^(BOOL aIsSuccess, YWProfileItem *item) {
//            NSLog(@"111111");
//            NSLog(@"------: %@", [NSThread isMainThread] ? @"主线程" : @"子线程");
//            self.aIsSuccess =aIsSuccess;
//            self.item = item;
//            // 更新导航栏
//            if (self.aIsSuccess && self.item) {
//                NSLog(@"22222");
//                [self setNavNickname:converStationVc item:self.item];
//            }else{
//                NSLog(@"33333");
//                [self setNavNickname:converStationVc item:profileItem];
//            }
//        }];
//    }else{
//        NSLog(@"4444");
//        [self setNavNickname:converStationVc item:profileItem];
//    }
}
- (void)setNavNickname:(YWConversationViewController *)converStationVc item:(NSString *)displayName{
    converStationVc.navigationController.navigationBarHidden = YES;
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    converStationVc.customTopView = navView;
    //添加分割线
    UIImageView *singleView = [[UIImageView alloc] init];
    singleView.backgroundColor = SLSepatorColor;
    singleView.frame = CGRectMake(0, 63.5, ScreenW, 0.5);
    [navView addSubview:singleView];
    
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 20, 60, 44);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
//    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(singleMessageBack) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    
    SLMessageTitleButton *btn = [[SLMessageTitleButton alloc] init];
    self.btn = btn;
    [navView addSubview:btn];
    btn.frame = CGRectMake(60, 27, ScreenW-120, 30);
//    [converStationVc setViewDidLoadBlock:^{
//        NSLog(@"55555555555");
//    }];
   
//    [converStationVc setViewDidLoadBlock:^{
        NSLog(@"666666666");
            [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];

            if ([[SLUserDefault getVoiceType] isEqualToString:@"q"]) {
                //听筒模式
                self.picType.picOne = @"listen";
                [self.btn setImage:[UIImage imageNamed:self.picType.picOne] forState:UIControlStateNormal];
            }else{
                //外放模式
                self.picType.picOne = @"";
                [self.btn setImage:[UIImage imageNamed:self.picType.picOne] forState:UIControlStateNormal];
            }
            self.btn.userInteractionEnabled = NO;
            [self.btn setTitle:displayName forState:UIControlStateNormal];
//    }];
}
//单聊消息的返回
- (void)singleMessageBack{
    
    [self.controller.navigationController popViewControllerAnimated:YES];
}
//清除缓存
- (void)clearAllUserDefaultsData
{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    
    NSDictionary* dict = [defs dictionaryRepresentation];
    
    for(id key in dict) {
        
        if (![key isEqualToString:appIsFirst]&& ![key isEqualToString:SLNetWorkStatus] && ![key isEqualToString:strAccountName] &&![key isEqualToString:strAccout] &&![key isEqualToString:@"VoiceType"]&&![key isEqualToString:@"guideSave"]&&![key isEqualToString:ContactStatus]&&![key isEqualToString:LoginStutas]&&![key isEqualToString:AllowAddContactStatus]) {
            [defs removeObjectForKey:key];
        }
    }
    
    [defs synchronize];
}

// 自定义全局导航栏
- (void)customGlobleNavigationBar
{
    // 自定义导航栏背景
    if ( [[[UIDevice currentDevice] systemVersion] compare:@"7.0"] == NSOrderedDescending )
    {
        [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]];
        [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
        
        [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
        
        [[UITabBar appearance] setTintColor:[UIColor blackColor]];
    }
    else
    {
        UIImage *originImage = [UIImage imageNamed:@"pub_title_ico_back"];
        UIImage *backgroundImage = [originImage resizableImageWithCapInsets:UIEdgeInsetsMake(44, 7, 4, 7)];
        [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIColor whiteColor],
                                                               NSShadowAttributeName: [UIColor clearColor],
                                                               NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0]}];
        
        NSDictionary *barButtonTittleAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                    NSShadowAttributeName: [UIColor clearColor],
                                                    NSFontAttributeName: [UIFont systemFontOfSize:16.0f]};
        
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:barButtonTittleAttributes
                                                                                                forState:UIControlStateNormal];
        
        UIImage *backItemImage = [[UIImage imageNamed:@"back_black"] resizableImageWithCapInsets:UIEdgeInsetsMake(33, 24, 0, 24)
                                                                                    resizingMode:UIImageResizingModeStretch];
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setBackButtonBackgroundImage:backItemImage
                                                                                                      forState:UIControlStateNormal
                                                                                                    barMetrics:UIBarMetricsDefault];
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setBackgroundImage:[UIImage new]
                                                                                            forState:UIControlStateNormal
                                                                                          barMetrics:UIBarMetricsDefault];
        
        [[UITabBar appearance] setBackgroundImage:backgroundImage];
        [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    }
    
    
}
// 自定义皮肤
- (void)customUISkin
{
    // 使用自定义UI资源和配置
    YWIMKit *imkit = self.ywIMKit;
    NSString *bundleName = @"CustomizedUIResources.bundle";
    NSString *bundlePath = [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:bundleName];
    NSBundle *customizedUIResourcesBundle = [NSBundle bundleWithPath:bundlePath];
    [imkit setCustomizedUIResources:customizedUIResourcesBundle];
}

// 设置消息是已读和未读状态
- (void)enableReadFlag
{
    // 开启单聊已读未读显示开关
    [[self.ywIMKit.IMCore getConversationService] setEnableMessageReadFlag:NO];
}

//聊天页面添加,输入面板插件定义
- (void)addInputViewPluginToConversationController:(YWConversationViewController *)aConversationController
{
    // 添加插件
    if ([aConversationController.messageInputView isKindOfClass:[YWMessageInputView class]]) {
        YWMessageInputView *messageInputView = (YWMessageInputView *)aConversationController.messageInputView;
        
        if (!([aConversationController.conversation.conversationId rangeOfString:@"超人助理"].location !=NSNotFound)) {
            SLChargeMessage *chargeMessage = [[SLChargeMessage alloc] init];
            [messageInputView addPlugin:chargeMessage];
        }
        
    }
}


-(id)reloadView:(NSString *)paid withauth:(NSString *)authorId withIMAuthorId:(NSString *)IMAuthorId withmodel:(SLChargeMessageViewModel *)weakModel{
    
    SLSeeMessageView *chatView= [[SLSeeMessageView alloc] init];
    if ([authorId isEqualToString:IMAuthorId]) {
        return chatView;
    }
    else if ([paid isEqualToString:@"1"]) {
        SLChargeMessageView *chatView = [[SLChargeMessageView alloc] init];
        chatView.hudView.hidden = YES;
        return chatView;
    }
    else if ([paid isEqualToString:@"0"])
    {
        SLChargeMessageView *chatView = [[SLChargeMessageView alloc] init];
        chatView.tag = [weakModel.messageId integerValue];
        return chatView;
    }
    return chatView;
}

// 显示付费消息内容
-(void)showFeeMessageContent:(NSString *)msgId aConversationController:(YWConversationViewController *) aConversationController paymentStatusDict:(NSDictionary *)paymentStatusDict{
    // 付费消息view
    SLChargeMessageView  *chatView  = (SLChargeMessageView *) [self.feeMessageViewDict  objectForKey:msgId];
    
    // 强制刷新聊天气泡布局的layout
    if(chatView){
        NSString *paid = [NSString stringWithFormat:@"%@", paymentStatusDict[@"paid"]];
        NSString *complain_status = [NSString stringWithFormat:@"%@", paymentStatusDict[@"complain_status"]];
        NSString *appraisal_status = [NSString stringWithFormat:@"%@", paymentStatusDict[@"appraisal_status"]];
        //        NSString *helpful = [NSString stringWithFormat:@"%@", paymentStatusDict[@"valuable"]];
        // msgDict = @{@"paid":@"0",@"complain_status":@"0",@"appraisal_status":@"0"};
        
        if ([paid isEqualToString:@"1"]) {  // 已支付
            chatView.lblHidden.hidden = YES;
            [chatView.constrianHeight setConstant:0.5];
            chatView.hudView.hidden = YES;
            [chatView setHelpfulBtnStatus:complain_status evaluateStatus:appraisal_status];
            [chatView updateBubbleView];
        }else if([paid isEqualToString:@"0"]){  // 未支付
            chatView.lblHidden.hidden = YES;
        }
    }
}

-(void)getPaid:(NSString *)msgId aConversationController:(YWConversationViewController *) aConversationController{
    //    NSLog(@"msgID:%@",msgId);
    if([self.loadMessageArr containsObject:msgId]) return;
    [self.loadMessageArr addObject:msgId];
    // 付费消息view
    // 正在加载一条付费消息
    SLChargeMessageView  *chatView  = (SLChargeMessageView *) [self.feeMessageViewDict  objectForKey:msgId];
    if(chatView){
        chatView.lblHidden.hidden = NO;
    }
    
    [self.queue addOperationWithBlock:^(){
        [NSThread sleepForTimeInterval:0.3f];
        NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
        NSLog(@"%@",[self getTimeNow]);
        [SLAPIHelper getMessagePayStatus:msgId success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            NSLog(@"success")
            [self.loadMessageArr removeObject:msgId];
            NSDictionary *msgDict = [[NSDictionary alloc] init];
            //        msgDict = @{@"paid":data[@"result"][@"paid"],@"valuable":data[@"result"][@"valuable"]};
            msgDict = @{@"paid":[NSString stringWithFormat:@"%@",data[@"result"][@"paid"]],@"complain_status":data[@"result"][@"complain_status"],@"appraisal_status":data[@"result"][@"appraisal_status"]};
            [settings setObject:msgDict forKey:msgId];
            [settings synchronize];
            //支付成功之后需要显示消息的界面
            [self showFeeMessageContent:msgId aConversationController:aConversationController paymentStatusDict:msgDict];
        } failure:^(SLHttpRequestError *failure) {
            [self.loadMessageArr removeObject:msgId];
            if(failure.httpStatusCode == 404){
                NSDictionary *msgDict = [[NSDictionary alloc] init];
                NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
                msgDict = @{@"paid":@"0",@"complain_status":@"0",@"appraisal_status":@"0"};
                [settings setObject:msgDict forKey:msgId];
                [settings synchronize];
                [self showFeeMessageContent:msgId aConversationController:aConversationController paymentStatusDict:msgDict];
            }
        }];
    }];
}

- (NSString *)getTimeNow
{
    NSString* date;
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc ] init];
    //[formatter setDateFormat:@"YYYY.MM.dd.hh.mm.ss"];
    [formatter setDateFormat:@"mm:ss:SSS"];
    date = [formatter stringFromDate:[NSDate date]];
    NSString * timeNow = [[NSString alloc] initWithFormat:@"%@", date];
    return timeNow;
}
//设置如何显示自定义消息
- (void)showCustomMessageWithConversationController:(YWConversationViewController *)aConversationController{
    
    aConversationController.backgroundImage = [UIImage createImageWithColor:SLColor(250, 250, 250)];
    __weak __typeof(aConversationController) weakController = aConversationController;
    [aConversationController setHook4BubbleViewModel:^YWBaseBubbleViewModel *(id<IYWMessage> message) {
        if ([[message messageBody] isKindOfClass:[YWMessageBodyCustomize class]]) {
            
            YWMessageBodyCustomize *customizeMessageBody = (YWMessageBodyCustomize *)[message messageBody];
            
            NSData *contentData = [customizeMessageBody.content dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *contentDictionary = [NSJSONSerialization JSONObjectWithData:contentData
                                                                              options:0
                                                                                error:NULL];
            
            NSString *messageType = [NSString stringWithFormat:@"%@", contentDictionary[@"customizeMsgType"]];
            
            if ([messageType isEqualToString:@"1"]) {
                SLChargeMessageViewModel *viewModel = [[SLChargeMessageViewModel alloc] initWithMessage:message];
                return viewModel;
            }else if([messageType isEqualToString:@"2"]) {
                SLValueMessageModel *viewModel = [[SLValueMessageModel alloc] initWithMessage:message];
                return viewModel;
            }
            else {
                SPBubbleViewModelCustomize *viewModel = [[SPBubbleViewModelCustomize alloc] initWithMessage:message];
                return viewModel;
            }
        }
        
        return nil;
    }];
    
    // ChatView一般从ViewModel中获取已经解析的数据，用于显示
    [aConversationController setHook4BubbleView:^YWBaseBubbleChatView *(YWBaseBubbleViewModel *viewModel) {
        //设置沙盒缓存
        if ([viewModel isKindOfClass:[SLChargeMessageViewModel class]]) {
            __weak SLChargeMessageViewModel * weakModel = (SLChargeMessageViewModel *)viewModel;
            NSString *authorId = [NSString stringWithFormat:@"%@", weakModel.authorId];
            NSString *selfId = @"";
            if(ApplicationDelegate && ApplicationDelegate.userId){
                selfId =  [NSString stringWithFormat:@"%@",ApplicationDelegate.userId];
            }
            
            NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
            NSString *msgId = weakModel.msgId;
            NSDictionary *msgDict = [settings objectForKey:msgId];
            //            NSLog(@"msgDict ----- %@",msgDict);
            if (msgDict == nil) {
                [self getPaid:msgId aConversationController:weakController];
            }
            
            if ([authorId isEqualToString:selfId]) {    // 本地显示消息
                SLSeeMessageView *chatView= [[SLSeeMessageView alloc] init];
                return chatView;
            } else {    // 显示付费消息
                SLChargeMessageView *chatView = (SLChargeMessageView *) [self.feeMessageViewDict objectForKey:msgId];
                if(!chatView){
                    chatView = [[SLChargeMessageView alloc] init];
                    [self.feeMessageViewDict setObject:chatView forKey:msgId];
                    if(msgDict != nil){
                        [self showFeeMessageContent:msgId aConversationController:weakController paymentStatusDict:msgDict];
                    }
                }
                return chatView;
            }
            
        }else if ([viewModel isKindOfClass:[SLValueMessageModel class]]) {
            //如果是值不值消息的模型
            __weak SLValueMessageModel * weakModel = (SLValueMessageModel *)viewModel;
            NSString *authorId = [NSString stringWithFormat:@"%@", weakModel.authorId];
            //当前的IM登录用户ID
            NSString *selfId = @"";
            if(ApplicationDelegate && ApplicationDelegate.userId){
                selfId =  [NSString stringWithFormat:@"%@",ApplicationDelegate.userId];
            }
            
            NSString *subMsgTypeString = [NSString stringWithFormat:@"%@", weakModel.subMessageType];
            SLJudgeMessageView *chatView = [[SLJudgeMessageView alloc] init];
            NSString *content = [[NSString alloc] init];
            
            
            NSString *playMoneyStr = [NSString stringWithFormat:@"%@",weakModel.moneyText];
            double strBlance = [playMoneyStr doubleValue];
            
            NSString *conversationName = [self getConversationName:weakController.conversation];
            if ([subMsgTypeString isEqualToString:@"0"]) {
                if ([authorId isEqualToString:selfId]) {
                    //如果是对方已经支付了收费消息
                    content = [NSString stringWithFormat:@"你已支付了%@发送的￥%.1f元付费消息，评价后立即到对方账户，三天未评价将自动到账", conversationName, strBlance];
                    NSString *moneyText = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"￥%.1f", strBlance]];
                    NSRange rangeOne = [content rangeOfString:moneyText];
                    NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:content];
                    [noteStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:rangeOne];
                    [chatView.content setAttributedText:noteStr];
                } else {
                    //对方的
                    content = [NSString stringWithFormat:@"%@支付了你发送的￥%.1f元付费消息，对方评价后立即到账，三天未评价将自动到账", conversationName, strBlance];
                    NSString *moneyText = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"￥%.1f",strBlance]];
                    NSRange rangeOne = [content rangeOfString:moneyText];
                    NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:content];
                    [noteStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:rangeOne];
                    [chatView.content setAttributedText:noteStr] ;
                    
                }
            }else if ([subMsgTypeString isEqualToString:@"1"]){
                //内容为值还是不值的判断
                NSString *value = [[NSString alloc] init];
                // NSString *ploceHolder = [[NSString alloc] init];
                //设置富文本显示有颜色的文字
                if ([weakModel.content isEqualToString:@"true"]) {
                    //                    value = @"有帮助";
                    //ploceHolder = @"";
                    value = @"评价";
                    
                }else {
                    //ploceHolder = @"";
                    //                    value = @"无帮助";
                    value = @"申诉";
                }
                if ([authorId isEqualToString:selfId]) {
                    //设置有帮助无帮助消息的文字颜色
                    content = [NSString stringWithFormat:@"你%@了%@发送的￥%.1f元付费消息",value,conversationName,strBlance];
                    NSString *moneyText = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"￥%.1f",strBlance]];
                    NSRange rangeOne = [content rangeOfString:moneyText];
                    NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:content];
                    [noteStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:rangeOne];
                    //设置有无帮助的字体颜色
                    NSRange rangeTwo = [content rangeOfString:value];
                    [noteStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:rangeTwo];
                    
                    [ chatView.content setAttributedText:noteStr] ;
                    
                }else {
                    content = [NSString stringWithFormat:@"%@%@了你发送的￥%.1f元付费消息",conversationName,value,strBlance];
                    
                    NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:content];
                    NSString *moneyText = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"￥%.1f",strBlance]];
                    NSRange rangeOne = [content rangeOfString:moneyText];
                    [noteStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:rangeOne];
                    //
                    NSRange rangeTwo = [content rangeOfString:value];
                    [noteStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:rangeTwo];
                    
                    [ chatView.content setAttributedText:noteStr] ;
                    
                }
            }
            
            return chatView;
            
        }
        
        return nil;
    }];
    
    //SDk会对上面Hoo Block中返回的BubbleView做Cache，当BubbleView被首次使用或者复用时会触发Block以便刷新数据。
    [aConversationController setHook4BubbleViewPrepare4Use:^(YWBaseBubbleChatView *bubbleView) {        
//        UIImage *image;
//        if ([weakController.conversation.conversationLatestMessage outgoing]) {
//            image = [UIImage imageNamed:@"rightBubbleImg"];
//        }else{
//            image = [UIImage imageNamed:@"leftBubbleImg"];
//        }
//        NSInteger capWidth = image.size.width * 0.5;
//        NSInteger capHeight = image.size.height * 0.5;
//        UIImage *newImage = [image stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
//        bubbleView.imageViewBG.image = newImage;
    }];
    
    [aConversationController setHook4BubbleViewModelPrepare4Use:^(YWBaseBubbleViewModel *viewModel) {
        if ([viewModel isKindOfClass:[SLChargeMessageViewModel class]]) {
            __weak SLChargeMessageViewModel * weakModel = (SLChargeMessageViewModel *)viewModel;
            // 回调的支付消息的实体
            ((SLChargeMessageViewModel *)viewModel).ask4showBlock = ^(void) {
                SLMessagePayController *messagePay= [[SLMessagePayController alloc] init];
                messagePay.valueBlcok = ^(){
                    //发送支付成功消息
                    NSString *uuid= [[NSUUID UUID] UUIDString];
                    //减去"-"
                    NSString *str = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
                    //变小写
                    NSString *lower = [str lowercaseString];
                    
                    NSDictionary *contentDictionary = @{
                                                        @"customizeMsgType":@"2",
                                                        @"subMessageType":@"0",
                                                        @"authorId":ApplicationDelegate.userId,
                                                        @"msgId":lower,
                                                        @"money":weakModel.moneyText,
                                                        };
                    NSData *data = [NSJSONSerialization dataWithJSONObject:contentDictionary
                                                                   options:0
                                                                     error:NULL];
                    NSString *content = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:content summary:@"[支付]"];
                    //发送自定义消息
                    [weakController.conversation asyncSendMessageBody:body
                                                             progress:nil
                                                           completion:^(NSError *error, NSString *messageID) {
                                                               //消息发送失败
                                                               if (error.code != 0) {
                                                                   [[SPUtil sharedInstance] showNotificationInViewController:weakController title:@"消息发送失败!" subtitle:nil type:SPMessageNotificationTypeError];
                                                               }else
                                                               {//消息发送成功
                                                                   weakModel.messageType4Charge = @"1";
                                                                   
                                                               }
                                                           }];
                    NSLog(@"11111111111");
                    [self getPaid:weakModel.msgId aConversationController:weakController];
                };
                
                messagePay.payMoney = [NSString stringWithFormat:@"%@", weakModel.moneyText];
                messagePay.messageId = weakModel.msgId;
                messagePay.to_user = weakModel.authorId;
                messagePay.userTitle = self.myName;
                messagePay.avatar = self.avatar;
                [weakController.tableView reloadData];
                [weakController.navigationController pushViewController:messagePay animated:YES];
                
            };
            
            ((SLChargeMessageViewModel *)viewModel).ask3showBlock = ^(NSInteger helpful, NSString *string) {
                
                if (helpful == 2) {
                    SLComplainViewController *vc = [[SLComplainViewController alloc] init];
                    vc.isFromMsg = YES;
                    vc.msgId = weakModel.msgId;
                    vc.msgComplainSuccessBlock = ^(BOOL isSuccess){
                        if (isSuccess) {
                            [self complainOrEvaluateSuccess:string summary:@"[申诉]" viewController:weakController model:weakModel complain_status:@"1" appraisal_status:@"0"];
                        }
                    };
                    [weakController.navigationController pushViewController:vc animated:YES];
                }else{
                    SLCallJudgeViewController *vc = [[SLCallJudgeViewController alloc] init];
                    vc.isFromMsg = YES;
                    vc.msgId = weakModel.msgId;;
                    vc.msgSuccessBlock = ^(BOOL isSuccess){
                        if (isSuccess) {
                            [self complainOrEvaluateSuccess:string summary:@"[评价]" viewController:weakController model:weakModel complain_status:@"0" appraisal_status:@"1"];
                        }
                    };
                    [weakController.navigationController pushViewController:vc animated:YES];
                }
            };
        }
    }];
}
//申诉或者评价成功后显示
- (void)complainOrEvaluateSuccess:(NSString *)string summary:(NSString *)complainOrEvaluate viewController:(YWConversationViewController *)weakController model:(SLChargeMessageViewModel *)weakModel complain_status:(NSString *)complain_status appraisal_status:(NSString *)appraisal_status{
    
    SLChargeMessageView  *chatView  = (SLChargeMessageView *) [self.feeMessageViewDict  objectForKey:weakModel.msgId];
    [chatView setHelpfulBtnStatus:complain_status evaluateStatus:appraisal_status];
    
    
    YWMessageBodyCustomize *body = [[YWMessageBodyCustomize alloc] initWithMessageCustomizeContent:string summary:complainOrEvaluate];
    // 发送该自定义消息
    [weakController.conversation asyncSendMessageBody:body
                                             progress:nil
                                           completion:^(NSError *error, NSString *messageID) {
                                               if (error.code != 0) {
                                                   [[SPUtil sharedInstance] showNotificationInViewController:weakController title:@"消息发送失败!" subtitle:nil type:SPMessageNotificationTypeError];
                                               }else
                                               {
                                                   
                                               }
                                           }];
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    NSString *msgId = weakModel.msgId;
    NSDictionary *msgDict = [settings objectForKey:msgId];
    if(msgDict){
        NSString *paid = [NSString stringWithFormat:@"%@", msgDict[@"paid"]];
        msgDict = @{@"paid":paid,@"complain_status":[NSString stringWithFormat:@"%@", complain_status],@"appraisal_status":[NSString stringWithFormat:@"%@", appraisal_status]};
        [settings setObject:msgDict forKey:msgId];
        [settings synchronize];
    }
}

//添加或者更新自定义会话
- (void)addOrUpdateCustomConversation
{
    NSInteger random = arc4random()%100;
    static NSArray *contentArray = nil;
    if (contentArray == nil) {
        contentArray = @[@"欢迎使用OpenIM", @"新的开始", @"完美的APP", @"请点击我"];
    }
    YWCustomConversation *conversation = [YWCustomConversation fetchConversationByConversationId:kSPCustomConversationIdForPortal creatIfNotExist:YES baseContext:[SLMessageManger sharedInstance].ywIMKit.IMCore];
    // 每一次点击都随机的展示未读数和最后消息
    [conversation modifyUnreadCount:@(random) latestContent:contentArray[random%4] latestTime:[NSDate date]];
    
    // 将这个会话置顶
    [self markConversationOnTop:conversation onTop:YES];
}

//将会话置顶
- (void)markConversationOnTop:(YWConversation *)aConversation onTop:(BOOL)aOnTop
{
    NSError *error = nil;
    [aConversation markConversationOnTop:aOnTop getError:&error];
    if (error) {
        [[SPUtil sharedInstance] showNotificationInViewController:nil title:@"自定义消息置顶失败" subtitle:nil type:SPMessageNotificationTypeError];
    }
}
// 自定义优先级的置顶会话（可保持长期置顶）
- (void)addHighPriorityCustomConversation
{
    // 获取该自定义会话
    YWCustomConversation *conversation = [YWCustomConversation fetchConversationByConversationId:kSPCustomConversationIdForFAQ creatIfNotExist:NO baseContext:[SLMessageManger sharedInstance].ywIMKit.IMCore];
    
    if (conversation == nil) {
        // 还没有则创建
        conversation = [YWCustomConversation fetchConversationByConversationId:kSPCustomConversationIdForFAQ creatIfNotExist:YES baseContext:[SLMessageManger sharedInstance].ywIMKit.IMCore];
        // 将这个会话置顶，时间为10年后（除非10年后你置顶了其他会话，否则这个优先级最高。:-)  ）
        [conversation markConversationOnTop:YES time:[[NSDate date] timeIntervalSince1970]+3600*24*365*10 getError:NULL];
    }
}

// 自定义会话Cell
const CGFloat kSPCustomConversationCellHeight = 0;
const CGFloat kSPCustomConversationCellContentMargin =10;
- (void)customizeConversationCellWithConversationListController:(YWConversationListViewController *)aConversationListController
{
    // 自定义Cell高度
    [aConversationListController setHeightForRowBlock:^CGFloat(UITableView *aTableView, NSIndexPath *aIndexPath, YWConversation *aConversation) {
        if ([aConversation.conversationId isEqualToString:kSPCustomConversationIdForFAQ]) {
            // TODO: 如果希望自定义Cell高度，在此返回你希望的高度
            return kSPCustomConversationCellHeight;
        }else if ([aConversation.conversationId rangeOfString:@"超人助理"].location !=NSNotFound){
            return kSPCustomConversationCellHeight;
        }else {
            return YWConversationListCellDefaultHeight;
        }
    }];
    //cnhhupan超人助理
    // 自定义Cell
    [aConversationListController setCellForRowBlock:^UITableViewCell *(UITableView *aTableView, NSIndexPath *aIndexPath, YWConversation *aConversation) {
        if ([aConversation.conversationId isEqualToString:kSPCustomConversationIdForFAQ]) {
            // TODO: 如果希望自定义Cell，在此返回非空的cell
            UITableViewCell *faqCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FAQCell"];
            [faqCell setBackgroundColor:[UIColor colorWithRed:201.f/255.f green:201.f/255.f blue:206.f/255.f alpha:1.f]];
            return faqCell;
        }else if ([aConversation.conversationId rangeOfString:@"超人助理"].location !=NSNotFound){
            UITableViewCell *faqCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FAQCell"];
            [faqCell setBackgroundColor:[UIColor colorWithRed:201.f/255.f green:201.f/255.f blue:206.f/255.f alpha:1.f]];
            return faqCell;
        }
        else {
            return nil;
        }
    }];
    
    // 自定义Cell调整
    [aConversationListController setConfigureCellBlock:^(UITableViewCell *aCell, UITableView *aTableView, NSIndexPath *aIndexPath, YWConversation *aConversation) {
        if ([aConversation.conversationId isEqualToString:kSPCustomConversationIdForFAQ]) {
            return;
        } else {
            return;
        }
    }];
    
    // 自定义Cell菜单
    [aConversationListController setConversationEditActionBlock:^NSArray *(YWConversation *aConversation, NSArray *editActions) {
        if ([aConversation.conversationId isEqualToString:kSPCustomConversationIdForFAQ]) {
            // 这个会话不能取消置顶和删除
            return @[];
        } else {
            //TODO: 如果需要自定义其他会话的菜单，在此编辑
            return editActions;
        }
    }];
    
}

// 插入本地消息,消息不会被发送到对方，仅本地展示
- (void)insertLocalMessageBody:(YWMessageBody *)aBody inConversation:(YWConversation *)aConversation
{
    NSDictionary *controlParameters = @{kYWMsgCtrlKeyClientLocal:@{kYWMsgCtrlKeyClientLocalKeyOnlySave:@(YES)}}; /// 控制字段
    [aConversation asyncSendMessageBody:aBody controlParameters:controlParameters progress:NULL completion:NULL];
}
#pragma mark ---- 设置气泡最大宽度

- (void)setMaxBubbleWidth
{
    //    [YWBaseBubbleChatView setMaxWidthUsedForLayout:300.f];
    [YWBaseBubbleChatView setMaxWidthUsedForLayout:ScreenW-25];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    NSString* strError = @"保存成功，照片已经保存至相册。";
    if( error != nil )
    {
        strError = error.localizedDescription;
    }
    [[SPUtil sharedInstance] showNotificationInViewController:nil title:@"图片保存结果" subtitle:strError type:SPMessageNotificationTypeMessage];
}

// 设置消息的长按菜单,这个方法展示如何设置图片消息的长按菜单

- (void)setMessageMenuToConversationController:(YWConversationViewController *)aConversationController
{
    __weak typeof(self) weakSelf = self;
    [aConversationController setMessageCustomMenuItemsBlock:^NSArray *(id<IYWMessage> aMessage) {
        if ([[aMessage messageBody] isKindOfClass:[YWMessageBodyImage class]]) {
            YWMessageBodyImage *bodyImage = (YWMessageBodyImage *)[aMessage messageBody];
            if (bodyImage.originalImageType == YWMessageBodyImageTypeNormal) {
                /// 对于普通图片，我们增加一个保存按钮
                return @[[[YWMoreActionItem alloc] initWithActionName:@"保存" actionBlock:^(NSDictionary *aUserInfo) {
                    NSString *messageId = aUserInfo[YWConversationMessageCustomMenuItemUserInfoKeyMessageId]; /// 获取长按的MessageId
                    YWConversationViewController *conversationController = aUserInfo[YWConversationMessageCustomMenuItemUserInfoKeyController]; /// 获取会话Controller
                    id<IYWMessage> message = [conversationController.conversation fetchMessageWithMessageId:messageId];
                    message = [message conformsToProtocol:@protocol(IYWMessage)] ? message : nil;
                    if ([[message messageBody] isKindOfClass:[YWMessageBodyImage class]]) {
                        YWMessageBodyImage *bodyImage = (YWMessageBodyImage *)[message messageBody];
                        NSArray *forRetain = @[bodyImage];
                        [bodyImage asyncGetOriginalImageWithProgress:^(CGFloat progress) {
                            ;
                        } completion:^(NSData *imageData, NSError *aError) {
                            /// 下载成功后保存
                            UIImage *img = [UIImage imageWithData:imageData];
                            if (img) {
                                UIImageWriteToSavedPhotosAlbum(img, weakSelf, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
                            }
                            [forRetain count]; // 用于防止bodyImage被释放
                        }];
                    }
                }]];
            }
        }
        //设置语音消息的长按按钮切换语音模式
        
        if ([[aMessage messageBody] isKindOfClass:[YWMessageBodyVoice class]]) {
            //若果消息体是声音类型的
            YWMessageBodyVoice *bodyVoice = (YWMessageBodyVoice *)[aMessage messageBody];
            NSString *str = [SLUserDefault getVoiceType];
            if ([str isEqualToString:@"a"]) {
                return @[[[YWMoreActionItem alloc]initWithActionName:@"使用听筒模式" actionBlock:^(NSDictionary *aUserInfo) {
                    NSString *messageId = aUserInfo[YWConversationMessageCustomMenuItemUserInfoKeyMessageId]; //获取长按的userId
                    YWConversationViewController *conversationController = aUserInfo[YWConversationMessageCustomMenuItemUserInfoKeyController]; // 获取会话Controller
                    id<IYWMessage> message = [conversationController.conversation fetchMessageWithMessageId:messageId];
                    self.messageId = messageId;
                    message = [message conformsToProtocol:@protocol(IYWMessage)] ? message : nil;
                    if ([[message messageBody] isKindOfClass:[YWMessageBodyVoice class]]) {
                        YWMessageBodyVoice *bodyVoices = (YWMessageBodyVoice *)[message messageBody];
                        //设置听筒的模式
                        [self.ywIMKit setAudioSessionCategory:AVAudioSessionCategoryPlayAndRecord];
                        [SLUserDefault setVoiceType:@"q"];
                        [self.picType setValue:@"listen" forKeyPath:@"picOne"];
                        [conversationController.tableView reloadData];
                    }
                }]];
                
            }else{
                return @[[[YWMoreActionItem alloc]initWithActionName:@"使用外放模式" actionBlock:^(NSDictionary *aUserInfo) {
                    NSString *messageId = aUserInfo[YWConversationMessageCustomMenuItemUserInfoKeyMessageId]; //获取长按的userId
                    YWConversationViewController *conversationController = aUserInfo[YWConversationMessageCustomMenuItemUserInfoKeyController]; // 获取会话Controller
                    id<IYWMessage> message = [conversationController.conversation fetchMessageWithMessageId:messageId];
                    message = [message conformsToProtocol:@protocol(IYWMessage)] ? message : nil;
                    if ([[message messageBody] isKindOfClass:[YWMessageBodyVoice class]]) {
                        YWMessageBodyVoice *bodyVoices = (YWMessageBodyVoice *)[message messageBody];
                        //设置听筒的模式
                        [self.ywIMKit setAudioSessionCategory:AVAudioSessionCategoryPlayback];
                        [SLUserDefault setVoiceType:@"a"];
                        [self.picType setValue:@"" forKeyPath:@"picOne"];
                        [conversationController.tableView reloadData];
                    }
                    
                }]];
            }
            
        }
        
        return nil;
    }];
}

//KVO实现的代理
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSString *strPic = [self.picType valueForKeyPath:@"picOne"];
    [self.btn setImage:[UIImage imageNamed:strPic] forState:UIControlStateNormal];
    NSString *strVoiceType = [SLUserDefault getVoiceType];
    if (!strVoiceType) {
        [SLUserDefault setVoiceType:@"a"];
    }
}

#pragma mark - 监听新消息到来的事件
- (void)listenNewMessage
{
    //处理再来消息的时候显示声音
    [[self.ywIMKit.IMCore getConversationService] addOnNewMessageBlockV2:^(NSArray *aMessages, BOOL aIsOffline) {
        //初始化系统的声音
        CHAudioPlayer *audio = [[CHAudioPlayer alloc] initSystemSoundWithName:@"sms-received1" SoundType:@"caf"];
        //开始播放声音
        [audio play];
    } forKey:self.description ofPriority:YWBlockPriorityDeveloper];
    
}

// 监听自己发送的消息的生命周期
- (void)listenMyMessageLife
{
    [[self.ywIMKit.IMCore getConversationService] addMessageLifeDelegate:self forPriority:YWBlockPriorityDeveloper];
}


- (void)messageLifeDidSend:(NSString *)aMessageId conversationId:(NSString *)aConversationId result:(NSError *)aResult
{
    // 消息发送完成后，做一些事情，例如播放一个提示音等等
    [MobClick event:@"im_send_count"];
    
}

- (void)setNotificationBlock
{
    // 当IMSDK需要弹出提示时，会调用此回调，需要修改成你App中显示提示的样式
    [self.ywIMKit setShowNotificationBlock:^(UIViewController *aViewController, NSString *aTitle, NSString *aSubtitle, YWMessageNotificationType aType) {
        [[SPUtil sharedInstance] showNotificationInViewController:aViewController title:aTitle subtitle:aSubtitle type:(SPMessageNotificationType)aType];
    }];
}

//头像点击事件
- (void)listenOnClickAvatar
{
    __weak __typeof(self) weakSelf = self;
    [self.ywIMKit setOpenProfileBlock:^(YWPerson *aPerson, UIViewController *aParentController) {
        if (([aPerson.personId rangeOfString:@"超人助理"].location !=NSNotFound)) {
            return ;
        }
        BOOL isMe = [aPerson isEqualToPerson:[[weakSelf.ywIMKit.IMCore getLoginService] currentLoginedUser]];
        if (isMe == NO) {
            HomePageViewController *otherPersonView = [[HomePageViewController alloc] init];
            otherPersonView.userId = [aPerson.description substringFromIndex:8];
            [aParentController.navigationController pushViewController:otherPersonView animated:YES];
        }else
        {
            HomePageViewController *mineView =  [[HomePageViewController alloc] init];
            mineView.userId = ApplicationDelegate.userId;
            [aParentController.navigationController pushViewController:mineView animated:YES];
        }
    }];
}

// 链接点击事件
- (void)listenOnClickUrl
{
    __weak __typeof(self) weakSelf = self;
    [self.ywIMKit setOpenURLBlock:^(NSString *aURLString, UIViewController *aParentController) {
        // 您可以使用您的容器打开该URL
        if ([aURLString hasPrefix:@"tel:"]) {
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:aURLString]]) {
                NSString *phoneNumber = [aURLString stringByReplacingOccurrencesOfString:@"tel:" withString:@""];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"拨打电话"
                                                                    message:phoneNumber
                                                                   delegate:weakSelf
                                                          cancelButtonTitle:@"取消"
                                                          otherButtonTitles:@"呼叫", nil];
                alertView.tag = kSPAlertViewTagPhoneCall;
                [alertView show];
            }
        }
        else {
            NSRegularExpression *regularExpression = [NSRegularExpression regularExpressionWithPattern:@"^\\w+:" options:kNilOptions error:NULL];
            if ([regularExpression numberOfMatchesInString:aURLString options:NSMatchingReportCompletion range:NSMakeRange(0, aURLString.length - 1)] == 0) {
                aURLString = [NSString stringWithFormat:@"http://%@", aURLString];
            }
            YWWebViewController *controller = [YWWebViewController makeControllerWithUrlString:aURLString andImkit:[SLMessageManger sharedInstance].ywIMKit];
            [aParentController.navigationController pushViewController:controller animated:YES];
        }
    }];
}

//UIAlertViewDelegate代理事件用于被挤下线的代理事件处理
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kSPAlertViewTagPhoneCall) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            NSString *phoneNumber = alertView.message;
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phoneNumber]];
            [[UIApplication sharedApplication] openURL:url];
        }
    }
    //挤掉线
    if (alertView.tag==SLAlertViewTagLoginoutOrAgainLogin) {
        ApplicationDelegate.Basic = NULL;
        ApplicationDelegate.userInformation = NULL;
        ApplicationDelegate.userId = NULL;
        [self clearAllUserDefaultsData];
        //跳转到首页
//        if (buttonIndex==0) {
//            
//            UITabBarController *tabBar=(UITabBarController *)self.rootWindow.rootViewController;
//            SLNavgationViewController *nav= tabBar.selectedViewController;
//            [nav popToRootViewControllerAnimated:YES];
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                tabBar.selectedIndex=0;
//            });
//            //
//        }
//        //跳转到登录页
//        if (buttonIndex==1) {
        
//            SLFirstViewController *loginView=[[SLFirstViewController alloc] init];
//            SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:loginView];
//        
//            [UIApplication sharedApplication].keyWindow.rootViewController = nav;
//            [[[self conversationNavigationController].viewControllers firstObject] presentViewController:nav animated:YES completion:nil];
        
//        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            SLFirstViewController *loginView=[[SLFirstViewController alloc] init];
            SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:loginView];
            [UIApplication sharedApplication].keyWindow.rootViewController = nav;
        });
        
    }
}

#pragma mark ------  预览大图事件
- (void)listenOnPreviewImage
{
    __weak typeof(self) weakSelf = self;
    
    [self.ywIMKit setPreviewImageMessageBlockV2:^(id<IYWMessage> aMessage, YWConversation *aOfConversation, UIViewController *aFromController) {
        
        // 增加更多按钮，例如转发
        YWMoreActionItem *transferItem = [[YWMoreActionItem alloc] initWithActionName:nil actionBlock:^(NSDictionary *aUserInfo) {
            // 获取会话及消息相关信息
            NSString *convId = aUserInfo[YWImageBrowserHelperActionKeyConversationId];
            NSString *msgId = aUserInfo[YWImageBrowserHelperActionKeyMessageId];
            YWConversation *conv = [[weakSelf.ywIMKit.IMCore getConversationService] fetchConversationByConversationId:convId];
            if (conv) {
                id<IYWMessage> msg = [conv fetchMessageWithMessageId:msgId];
                if (msg) {
                    /*
                     SLMessageViewController *messageVc = [[SLMessageViewController alloc] init];
                     __block NSString *personIdStr = nil;
                     if (messageVc.chooseBlock) {
                     messageVc.chooseBlock = ^(NSString *PersonId){
                     personIdStr = PersonId;
                     };
                     }
                     [aFromController.navigationController pushViewController:messageVc animated:YES];
                     NSString *personId = [NSString stringWithFormat:@"%@",personIdStr];
                     YWPerson *person = [[YWPerson alloc] initWithPersonId:personId];
                     YWP2PConversation *targetConv = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:weakSelf.ywIMKit.IMCore];
                     [targetConv asyncForwardMessage:msg progress:NULL completion:^(NSError *error, NSString *messageID) {
                     NSLog(@"转发结果：%@", error.code == 0 ? @"成功" : @"失败");
                     [[SPUtil sharedInstance] asyncGetProfileWithPerson:person progress:nil completion:^(BOOL aIsSuccess, YWPerson *aPerson, NSString *aDisplayName, UIImage *aAvatarImage) {
                     [[SPUtil sharedInstance] showNotificationInViewController:nil title:[NSString stringWithFormat:@"已经成功转发给:%@", aDisplayName] subtitle:nil type:SPMessageNotificationTypeMessage];
                     }];
                     }];  */
                }
            }
            
        }];
        
        //打开IMSDK提供的预览大图界面
        [YWImageBrowserHelper previewImageMessage:aMessage conversation:aOfConversation inNavigationController:aFromController.navigationController additionalActions:@[transferItem]];
    }];
}


#pragma mark - apns
/**
 *  您需要在-[AppDelegate application:didFinishLaunchingWithOptions:]中第一时间设置此回调
 *  在IMSDK截获到Push通知并需要您处理Push时，IMSDK会自动调用此回调
 */
- (void)handleAPNSPush
{
    __weak typeof(self) weakSelf = self;
    
    [[[YWAPI sharedInstance] getGlobalPushService] addHandlePushBlockV4:^(NSDictionary *aResult, BOOL *aShouldStop) {
        BOOL isLaunching = [aResult[YWPushHandleResultKeyIsLaunching] boolValue];
        UIApplicationState state = [aResult[YWPushHandleResultKeyApplicationState] integerValue];
        NSString *conversationId = aResult[YWPushHandleResultKeyConversationId];
        Class conversationClass = aResult[YWPushHandleResultKeyConversationClass];
        
        if (conversationId.length <= 0) {
            return;
        }
        
        if (conversationClass == NULL) {
            return;
        }
        
        if (isLaunching) {
            // 用户划开Push导致app启动
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if ([self isPreLogined]) {
                    // 说明已经预登录成功
                    YWConversation *conversation = nil;
                    if (conversationClass == [YWP2PConversation class]) {
                        conversation = [YWP2PConversation fetchConversationByConversationId:conversationId creatIfNotExist:YES baseContext:weakSelf.ywIMKit.IMCore];
                    } else if (conversationClass == [YWTribeConversation class]) {
                        conversation = [YWTribeConversation fetchConversationByConversationId:conversationId creatIfNotExist:YES baseContext:weakSelf.ywIMKit.IMCore];
                    }
                    if (conversation) {
                        //                        [weakSelf openConversationViewControllerWithConversation:conversation fromNavigationController:[weakSelf conversationNavigationController]];
                        
                        SLTabBarViewController *tabBar=(SLTabBarViewController *)self.rootWindow.rootViewController;
                        SLNavgationViewController  *navigationController = tabBar.selectedViewController;
                        if (navigationController.viewControllers.count==1) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"PushChangeTabbarVC" object:nil];
                        }
                        
                    }
                }
            });
            
        } else {
            // app已经启动时处理Push
            
            if (state != UIApplicationStateActive) {
                if ([self isPreLogined]) {
                    // 说明已经预登录成功
                    YWConversation *conversation = nil;
                    if (conversationClass == [YWP2PConversation class]) {
                        conversation = [YWP2PConversation fetchConversationByConversationId:conversationId creatIfNotExist:YES baseContext:weakSelf.ywIMKit.IMCore];
                    } else if (conversationClass == [YWTribeConversation class]) {
                        conversation = [YWTribeConversation fetchConversationByConversationId:conversationId creatIfNotExist:YES baseContext:weakSelf.ywIMKit.IMCore];
                    }
                    if (conversation) {
                        SLTabBarViewController *tabBar=(SLTabBarViewController *)self.rootWindow.rootViewController;
                        SLNavgationViewController  *navigationController = tabBar.selectedViewController;
                        if (navigationController.viewControllers.count==1) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"PushChangeTabbarVC" object:nil];
                        }
                    }
                }
            } else {
                // 应用处于前台
                // 建议不做处理，等待IM连接建立后，收取离线消息。
            }
        }
    } forKey:self.description ofPriority:YWBlockPriorityDeveloper];
}


@end
