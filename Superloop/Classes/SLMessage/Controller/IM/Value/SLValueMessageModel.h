//
//  SLValueMessageModel.h
//  Superloop
//
//  Created by WangJiwei on 16/5/5.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <WXOUIModule/YWUIFMWK.h>
#import <WXOpenIMSDKFMWK/YWFMWK.h>

@interface SLValueMessageModel : YWBaseBubbleViewModel
@property (nonatomic, copy)NSString *moneyText;
@property (nonatomic, copy) NSString *authorId;
@property (nonatomic, copy) NSString *msgId;
@property (nonatomic, copy) NSString *subMessageType;
@property (nonatomic, copy) NSString *content;   //消息值不值
//要发送的userId
@property (nonatomic,strong) NSString *to_user;
//来自的userId
@property (nonatomic,strong) NSString *from_user;

- (instancetype)initWithMessage:(id<IYWMessage>)aMessage;

@end
