//
//  SLJudgeMessageView.m
//  Superloop
//
//  Created by WangJiwei on 16/5/5.
//  Copyright © 2016年 Superloop. All rights reserved.
//   值不值消息的view

#import "SLJudgeMessageView.h"
#import "SLValueMessageModel.h"
#import "SPUtil.h"
@interface SLJudgeMessageView()
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, strong) SLValueMessageModel *viewModel;
@end
@implementation SLJudgeMessageView
@dynamic viewModel;

- (instancetype)init {
    if (self = [super init]) {
        UINib *nib = [UINib nibWithNibName:@"SLJudgeMessageView" bundle:nil];
        [nib instantiateWithOwner:self options:NULL];
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
    }
    return self;
}
- (void)updateBubbleView
{
    CGFloat width = [_content.text boundingRectWithSize:CGSizeMake(ScreenW - 146, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13]} context:nil].size.width;
    if (width >ScreenW - 146) {
        width = ScreenW - 146;
    }
    CGFloat height = [_content.text boundingRectWithSize:CGSizeMake(ScreenW - 146, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13]} context:nil].size.height;
//    NSLog(@"%lf,  %lf", width, height);
    _contentView.width = width + 48;
    _contentView.height = height + 12;
    _contentView.layer.cornerRadius = 6;
    _contentView.layer.masksToBounds = YES;
    self.frame = self.contentView.frame;
    [self addSubview:self.contentView];
}

// 返回所持ViewModel类名，用于类型检测
- (NSString *)viewModelClassName
{
    return NSStringFromClass([SLJudgeMessageView class]);
}

@end
