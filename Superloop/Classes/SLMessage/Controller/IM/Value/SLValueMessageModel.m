//
//  SLValueMessageModel.m
//  Superloop
//
//  Created by WangJiwei on 16/5/5.
//  Copyright © 2016年 Superloop. All rights reserved.
//   值不值消息的模型

#import "SLValueMessageModel.h"
#import "SPUtil.h"
@implementation SLValueMessageModel
- (instancetype)initWithMessage:(id<IYWMessage>)aMessage
{
    self = [super init];
    if (self) {
        YWMessageBodyCustomize *bodyCustomize = (YWMessageBodyCustomize *)[aMessage messageBody];
        
        NSData *contentData = [bodyCustomize.content dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *contentDictionary = [NSJSONSerialization JSONObjectWithData:contentData
                                                                          options:0
                                                                            error:NULL];
        _moneyText = contentDictionary[@"money"];
        _authorId = contentDictionary[@"authorId"];
        _msgId = contentDictionary[@"msgId"];
        _subMessageType = contentDictionary[@"subMessageType"];
        _content = contentDictionary[@"content"];
        
        /// 设置气泡类型
        self.bubbleStyle = BubbleStyleCommonMiddle;
        self.layout = [aMessage outgoing] ? WXOBubbleLayoutRight : WXOBubbleLayoutLeft;
    }
    return self;
}

@end
