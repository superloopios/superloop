//
//  SLJudgeMessageView.h
//  Superloop
//
//  Created by WangJiwei on 16/5/5.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <WXOUIModule/YWBaseBubbleChatView.h>

@interface SLJudgeMessageView : YWBaseBubbleChatView

@property (weak, nonatomic) IBOutlet UILabel *content;

@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

@property (weak, nonatomic) IBOutlet UILabel *lblValue;

@property (weak, nonatomic) IBOutlet UILabel *leastlable;

@property (weak, nonatomic) IBOutlet UILabel *lastlable;

@end
