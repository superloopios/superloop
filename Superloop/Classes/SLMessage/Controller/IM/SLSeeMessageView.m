//
//  SLSeeMessageView.m
//  Superloop
//
//  Created by WangJiwei on 16/5/4.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSeeMessageView.h"
#import "SLChargeMessageViewModel.h"
#import "SPUtil.h"
@interface SLSeeMessageView()
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;

//@property (nonatomic,strong) UILabel *messageLabel;
//@property (nonatomic,strong) UIButton *moneyBtn;
//@property (nonatomic,strong) UILabel *moneyLab;
@property (nonatomic,strong) SLChargeMessageViewModel *viewModel;

@end
@implementation SLSeeMessageView
@dynamic viewModel;

- (instancetype)init {
    if (self = [super init]) {
        UINib *nib = [UINib nibWithNibName:@"SLSeeMessageView" bundle:nil];
        [nib instantiateWithOwner:self options:NULL];
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
    }
    return self;
}
// 需要刷新BubbleView时会被调用
- (void)updateBubbleView
{
    self.messageLabel.text = self.viewModel.content;
    self.messageLabel.textColor = self.viewModel.color;
    self.moneyLab.text = [NSString stringWithFormat:@"%@",self.viewModel.moneyText];
    
//    CGFloat width = [self configBubleViewSize].width;
//    if (width >ScreenW - 200) {
//        width = ScreenW - 200;
//    }
//    CGFloat height = [self configBubleViewSize].height;
//    _contentView.width = width + 12 ;
//    _contentView.height = height + 12;
//     self.frame = self.contentView.frame;
//    [self addSubview:self.contentView];
    CGFloat width = 200;
    CGFloat height = [self configBubleViewSize].height +25;
    _contentView.width = width ;
    _contentView.height = height ;
    _contentView.width = width + 12 ;
    _contentView.height = height + 12;
    self.frame = self.contentView.frame;
    [self addSubview:self.contentView];
}

//计算文本内容的宽高度
- (CGSize)configBubleViewSize{
   
//    return [self.viewModel.content boundingRectWithSize:CGSizeMake(ScreenW - 200, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]} context:nil].size;
        return [self.viewModel.content boundingRectWithSize:CGSizeMake(200, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]} context:nil].size;
}
// 返回所持ViewModel类名，用于类型检测
- (NSString *)viewModelClassName
{
    return NSStringFromClass([SLChargeMessageViewModel class]);
}

@end
