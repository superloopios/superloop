//
//  SLChargeMessageView.h
//  Superloop
//
//  Created by WangJiwei on 16/4/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <WXOUIModule/YWBaseBubbleChatView.h>

@interface SLChargeMessageView : YWBaseBubbleChatView
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *hudView;

@property (weak, nonatomic) IBOutlet UIButton *helpBtn;
@property (weak, nonatomic) IBOutlet UIButton *noHelpBtn;
//判读是否是来自支付过的消息
@property (nonatomic,assign)BOOL isFromOther;
//在网络状态比较差的情况下需要显示的view
@property (weak, nonatomic) IBOutlet UILabel *lblHidden;

@property (weak, nonatomic) IBOutlet UILabel *fengeLable;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@property (weak, nonatomic) IBOutlet UILabel *waringLable;
//分割线
@property (weak, nonatomic) IBOutlet UILabel *singleLable;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constrianHeight;

- (void)setHelpfulBtnStatus:(NSString *)complainStatus evaluateStatus:(NSString *)evaluateStatus;
- (void)updateBubbleView;

@end
