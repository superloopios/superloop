//
//  SLPayMentView.h
//  zhifudemo
//
//  Created by WangJiwei on 16/5/7.
//  Copyright © 2016年 WangJiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
@class v;
@protocol SLPayMentViewDelegate <NSObject>

- (void)didcancel;
- (void)didInputPassword;
@optional
- (void)didForgetPassword;

@end
@interface SLPayMentView : UIView

@property (nonatomic, strong)NSMutableArray *passwordIndicatorArrary;
@property (nonatomic, weak) id<SLPayMentViewDelegate>Delegate;
@property (weak, nonatomic) IBOutlet UILabel *amountCount;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@end
