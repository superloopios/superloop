//
//  SLOpenIMCommon.h
//  Superloop
//
//  Created by WangS on 16/8/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SLOpenIMCommon : NSObject

+ (void)login:(NSString *)uid account:(NSString *)account password:(NSString *)password successBlock:(void(^)())aSuccessBlock failedBlock:(void (^)(NSError *))aFailedBlock;


@end
