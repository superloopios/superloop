//
//  SLSystemMessageViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/22.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSystemMessageViewController.h"
#import "SLSystemMessageModel.h"
#import "SLNotiSystemMessageCell.h"
#import "SLComplainSystemMessageCell.h"
#import "SLBaseSystemMessageTableViewCell.h"
#import "SLAPIHelper.h"
#import <MJExtension.h>
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "SLNormalSystemCell.h"
#import "AppDelegate.h"

@interface SLSystemMessageViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic, strong)NSMutableArray *modelArr;

@property(nonatomic,strong)UITableView *tableView;

@property (nonatomic,assign) NSInteger page;

@end

@implementation SLSystemMessageViewController
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.frame = CGRectMake(0, 64, screen_W, screen_H - 64);
        [_tableView registerClass:[SLComplainSystemMessageCell class] forCellReuseIdentifier:@"complainCell"];
        [_tableView registerClass:[SLNormalSystemCell class] forCellReuseIdentifier:@"normalSystemCell"];
        [_tableView registerClass:[SLNotiSystemMessageCell class] forCellReuseIdentifier:@"notiCell"];
        [_tableView setContentInset:UIEdgeInsetsMake(9, 0, 0, 0)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = SLColor(246, 246, 246);
        [self setUpRefresh];
    }
    return _tableView;
}

- (NSMutableArray *)modelArr{
    if (!_modelArr) {
        _modelArr = [NSMutableArray array];
    }
    return _modelArr;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLSystemMessageViewController.m"];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLSystemMessageViewController.m"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = SLColor(250, 250, 250);
    self.page = 0;
    
    
//
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.tableView.y = 64;
//    [self.tableView  setSeparatorColor:[UIColor  lightGrayColor]];  //设置分割线浅灰色
      // self.clearsSelectionOnViewWillAppear = NO;
    
    [self setNav];
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.view addSubview:self.tableView];
    [self.tableView.mj_header beginRefreshing];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
- (void)setNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(5, 30, 30, 30);
    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    
    UILabel *titleLab=[[UILabel alloc] initWithFrame:CGRectMake(40, 32, ScreenW-80, 20)];
    titleLab.text=@"系统消息";
    titleLab.textAlignment=NSTextAlignmentCenter;
    titleLab.font=[UIFont systemFontOfSize:17];
    titleLab.textColor=[UIColor blackColor];
    [navView addSubview:titleLab];
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=[UIColor lightGrayColor];
    [navView addSubview:cutImgView];
}

- (void)getData{
    self.page = 0;
    NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.page],@"pager":@"20"};
    [SLAPIHelper getSystemNotice:parameters success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        [self.modelArr removeAllObjects];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadMessageNum" object:self];
        self.modelArr = [SLSystemMessageModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
        NSLog(@"%@",data[@"result"]);
        

        if (self.modelArr.count<20) {
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
            
        }else{
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer resetNoMoreData];
        }
        [self.tableView reloadData];
        [self.tableView.mj_footer setHidden:NO];
//        NSLog(@"%@",dict);
    } failure:^(SLHttpRequestError *failure) {
        [self.tableView.mj_header endRefreshing];
    }];
}
-(void)getMoreData{
    self.page++;
    NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.page],@"pager":@"20"};
    [SLAPIHelper getSystemNotice:parameters success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        NSArray *moreData = [SLSystemMessageModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
        
        [self.modelArr addObjectsFromArray:moreData];
        [self.tableView reloadData];
        if ( moreData.count <20) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [self.tableView.mj_footer endRefreshing];
        }
        //        NSLog(@"%@",dict);
    } failure:^(SLHttpRequestError *failure) {
        [self.tableView.mj_footer endRefreshing];
    }];

}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.modelArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLSystemMessageModel *model = self.modelArr[indexPath.row];
    return model.cellHeight + 33 ;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLSystemMessageModel *model = self.modelArr[indexPath.row];
    
    if ([model.msgtype isEqual:@5]) {
        SLNotiSystemMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"notiCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.model = model;
        return cell;
    }else if ([model.msgtype isEqual:@3]||[model.msgtype isEqual:@4]){
        SLComplainSystemMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"complainCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = model;
        return cell;
    }else {
        SLNormalSystemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"normalSystemCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.model = model;
        return cell;
    }
}

- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            [self addNetWorkViews:@"世界上最遥远的距离就是－－妹有网"];
        }else if ([netWorkStaus isEqualToString:@"wifi"]||[netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            if (_noNetWorkView) {
                [_noNetWorkView removeFromSuperview];
            }
        }
    }
    
}
-(void)addNetWorkViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, ScreenW , ScreenH-64 )];
    
    UILabel *tiplabel = [[UILabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    tiplabel.attributedText = AttributedStr;
    
    
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    _noNetWorkView = contentView;
    [self.view addSubview:contentView];
    
}

- (void)setUpRefresh {
    self.tableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
    self.tableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreData)];
    self.tableView.mj_footer.hidden = YES;
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
