//
//  SLMessageViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/22.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLMessageViewController.h"
#import "YWConversationListViewController+UIViewControllerPreviewing.h"
#import "SLMessageManger.h"
#import "SLSystemCell.h"
#import "SLTopicRepetViewController.h"
#import "SLSupportMeViewController.h"
#import "SLSystemMessageViewController.h"
#import "HomePageViewController.h"
#import "SLAPIHelper.h"
#import "SLNavgationViewController.h"
#import "AppDelegate.h"
#import "SLCustomMsgHeaderView.h"
#import "SLOpenIMCommon.h"
#import "SPUtil.h"

@interface SLMessageViewController()<UITableViewDelegate>
@property (nonatomic,strong) YWConversationListViewController *converVc;
@property (nonatomic,copy) NSString *messageNum;
@property (nonatomic,strong) UIImageView *redImgView;
@property (nonatomic,strong) UILabel *numberLab;
@property (nonatomic,strong) UIImage *image;
@property (nonatomic,strong) UIImageView *serviceRedImgView;
@property (nonatomic,strong) UILabel *serviceNumberLab;
@property (nonatomic,copy) NSString *serviceMessageNum;
@property (nonatomic,strong) UIImageView *systemRedImgView;
@property (nonatomic,strong) UILabel *systemNumberLab;
@property (nonatomic,copy) NSString *systemMessageNum;
@property (nonatomic,strong) UIView *headerView;
@property (nonatomic,strong) UILabel *nameLab;//标题

@end
@implementation SLMessageViewController

-(NSString *)messageNum{
    if (!_messageNum) {
        _messageNum=[NSString new];
    }
    return _messageNum;
}
-(NSString *)serviceMessageNum{
    if (!_serviceMessageNum) {
        _serviceMessageNum=[NSString new];
    }
    return _serviceMessageNum;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLMessageViewController.m"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserMessagesRemind) name:@"DynamicMsg" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserMessagesRemind) name:@"SystemMsg" object:nil];
    [self getUserMessagesRemind];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLMessageViewController.m"];
}
- (void)viewDidLoad{
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = SLColor(240, 240, 240);
    YWConversationListViewController *conversationListController = [[SLMessageManger sharedInstance].ywIMKit makeConversationListViewController];
    conversationListController.tableView.tableHeaderView = [self messageHeaderView];
    [self addChildViewController:conversationListController];
    UIViewController *viewcontroller = self.childViewControllers[0];
    viewcontroller.view.frame = CGRectMake(0, 64, screen_W, screen_H-64-49);
    [self.view addSubview:viewcontroller.view];
    self.converVc = conversationListController;
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = UIColorFromRGB(0xff5a5f);
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"正在加载..."];
    [refreshControl addTarget:self action:@selector(refreshBegin) forControlEvents:UIControlEventValueChanged];
    conversationListController.refreshControl = refreshControl;
    
    
    [[SLMessageManger sharedInstance] customizeConversationCellWithConversationListController:conversationListController];
    __weak __typeof(conversationListController) weakConversationListController = conversationListController;
    // __weak __typeof(self) weakSelf = self;
    conversationListController.configureCellBlock = ^(UITableViewCell *cell, UITableView *tableView, NSIndexPath *indexPath, YWConversation *conversation){
        [tableView setSeparatorInset:UIEdgeInsetsZero];
        [tableView setLayoutMargins:UIEdgeInsetsZero];
        tableView.backgroundColor = SLColor(240, 240, 240);
    };
    conversationListController.didSelectItemBlock = ^(YWConversation *aConversation) {
        
        YWProfileItem *item = [[[SLMessageManger sharedInstance].ywIMKit.IMCore getContactService] getProfileForPerson:((YWP2PConversation *)aConversation).person withTribe:nil];
        //        if (weakSelf.chooseBlock) {
        //            weakSelf.chooseBlock(item.person.personId);
        //        }
        ApplicationDelegate.imTitlename = item.displayName;
        [[SLMessageManger sharedInstance] openConversationViewControllerWithConversation:aConversation
                                                                fromNavigationController:weakConversationListController.navigationController];
    };
    
    conversationListController.didDeleteItemBlock = ^(YWConversation *aConversation){
        
        [weakConversationListController.tableView reloadData];
    };
    
    conversationListController.avatarPressedBlock = ^(YWConversation *aConversation, YWConversationListViewController *aController){
        //头像点击事件回调
        YWProfileItem *item = [[[SLMessageManger sharedInstance].ywIMKit.IMCore getContactService] getProfileForPerson:((YWP2PConversation *)aConversation).person withTribe:nil];
        HomePageViewController *otherVc = [[HomePageViewController alloc] init];
        otherVc.userId = item.person.personId;
        [self.navigationController pushViewController:otherVc animated:YES];
    };
    [conversationListController.tableView setSeparatorColor:[UIColor lightGrayColor]];
    [self setUpNav];
    [self refreshBegin];
}

- (void)refreshBegin{
    if (![[[SLMessageManger sharedInstance].ywIMKit.IMCore getLoginService] hasLoginThread]) {
        [self connectionOpenIM];
    }else{
    self.nameLab.text = @"消息";
    [self.converVc.refreshControl endRefreshing];
    }
}
- (void)connectionOpenIM{
    NSDictionary *dict = [SLUserDefault getUserInfo];
    if (dict) {
        NSString *password = @"";
        if ([dict objectForKey:@"Password"]) {
            password = [dict objectForKey:@"Password"];
            __weak typeof(self) weakSelf = self;
            [SLOpenIMCommon login:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId] account:nil password:[dict objectForKey:@"Password"] successBlock:^{
                self.nameLab.text = @"消息";
                [self.converVc.refreshControl endRefreshing];
                [[SPUtil sharedInstance] setWaitingIndicatorShown:NO withKey:weakSelf.description];
            } failedBlock:^(NSError *aError) {
                self.nameLab.text = @"消息(未连接)";
                [self.converVc.refreshControl endRefreshing];
            }];
            
        }
    }
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    //nameLab.text=@"消息";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    self.nameLab = nameLab;
    [navView addSubview:nameLab];
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}

//添加一个消息的头部View
- (UIView *)messageHeaderView{
    CGFloat height = 64;
    NSArray *cellArray =@[@{@"label":@"动态",@"icon":@"topicRepeat"},@{@"label":@"系统消息",@"icon":@"systemMessage"},@{@"label":@"超人助理",@"icon":@"customerService"}];
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, height * cellArray.count)];
    backView.backgroundColor = SLColor(240, 240, 240);
    
    _headerView = [[UIView alloc] init];
    _headerView.backgroundColor = [UIColor whiteColor];
    _headerView.frame = CGRectMake(0, 0, ScreenW, height * cellArray.count);
    [backView addSubview:_headerView];
    
    for (int i = 0; i < cellArray.count; i++) {
        SLCustomMsgHeaderView *cell=[[SLCustomMsgHeaderView alloc] init];
        cell.iconImgView.image = [UIImage imageNamed:cellArray[i][@"icon"]];
        cell.titleLab.text = cellArray[i][@"label"];
        cell.frame = CGRectMake(0, 64*i, ScreenW, height);
        cell.tag = i;
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureRecognizer:)];
        [cell addGestureRecognizer:gestureRecognizer];
        [_headerView addSubview:cell];
    }
//    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 64*cellArray.count, ScreenW, 5)];
//    lineView.backgroundColor = SLColor(240, 240, 240);
//    [backView addSubview:lineView];
    return backView;
}
- (void)gestureRecognizer:(UITapGestureRecognizer *)gestureRecognizer{
    UIView *cellView = gestureRecognizer.view;
    switch (cellView.tag) {
        case 0:{
            SLTopicRepetViewController *topicRepeat = [[SLTopicRepetViewController alloc] init];
            [self.navigationController pushViewController:topicRepeat animated:YES];
            break;
        }case 1:{
            SLSystemMessageViewController *systemMessage = [[SLSystemMessageViewController alloc] init];
            
            [self.navigationController pushViewController:systemMessage animated:YES];
            break;
        }case 2:{
            YWPerson *person=[[YWPerson alloc]initWithPersonId:@"超人助理" EServiceGroupId:0 baseContext:[SLMessageManger sharedInstance].ywIMKit.IMCore];
            YWConversation *conversation=[YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:[SLMessageManger sharedInstance].ywIMKit.IMCore];
            [self.navigationController setNavigationBarHidden:NO];
            __weak __typeof(self) weakSelf = self;
            
            [[SLMessageManger sharedInstance] openConversationViewControllerWithConversation:conversation
                                                                    fromNavigationController:weakSelf.navigationController];
            
            break;
        }
        default:{
        }
    }
}

-(void)loadUnreadMsgRemind:(NSInteger)newsCount label:(UILabel *)numberLab imgView:(UIImageView *)redImgView{
    if (newsCount>0) {
        redImgView.hidden=NO;
        if (newsCount>99) {
            self.messageNum=@"99+";
        }else{
            self.messageNum=[NSString stringWithFormat:@"%ld",(long)newsCount];
        }
        CGFloat messageWidth= [self.messageNum boundingRectWithSize:CGSizeMake(ScreenW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12]} context:nil].size.width+10;
        if (messageWidth<18) {
            messageWidth=18;
        }
        numberLab.text=self.messageNum;
        numberLab.frame=CGRectMake(0, 0, messageWidth, 18);
        redImgView.frame=CGRectMake(ScreenW-messageWidth-20, 23, messageWidth, 18);
    }else {
        redImgView.hidden=YES;
    }
}
-(void)getUserMessagesRemind{
    for (SLCustomMsgHeaderView *cell in _headerView.subviews) {
        if (cell.tag==0) {//获取动态未读数的提醒
            NSInteger newsCount = ApplicationDelegate.dynamicMsg;
            [self loadUnreadMsgRemind:newsCount label:cell.numLab imgView:cell.redImgView];
        }else if (cell.tag==1){//获取系统消息数
            NSInteger newsCount = ApplicationDelegate.systemMsg;
            [self loadUnreadMsgRemind:newsCount label:cell.numLab imgView:cell.redImgView];
        }else{//获取客服的未读消息数
            NSInteger newsCount = ApplicationDelegate.serviceMsg;
            [self loadUnreadMsgRemind:newsCount label:cell.numLab imgView:cell.redImgView];
        }
    }
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
