//
//  SLOpenIMCommon.m
//  Superloop
//
//  Created by WangS on 16/8/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLOpenIMCommon.h"
#import "SLMessageManger.h"
#import "AppDelegate.h"


@implementation SLOpenIMCommon

static NSInteger _retryCount;

+ (void)login:(NSString *)uid account:(NSString *)account password:(NSString *)password successBlock:(void(^)())aSuccessBlock failedBlock:(void (^)(NSError *))aFailedBlock{
    
    __weak typeof(self) weakSelf = self;

    [[SLMessageManger sharedInstance] callThisAfterISVAccountLoginSuccessWithYWLoginId:uid passWord:password preloginedBlock:^{
        
    } successBlock:^{
        _retryCount=0;
        aSuccessBlock();
    } failedBlock:^(NSError *aError) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_retryCount < 2) {
                _retryCount ++;
                [weakSelf login:uid account:account password:password successBlock:aSuccessBlock failedBlock:aFailedBlock];
            }else{
                _retryCount=0;
                aFailedBlock(aError);
            }
        });

    }];
}

@end
