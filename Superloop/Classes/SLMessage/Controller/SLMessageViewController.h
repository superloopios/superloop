//
//  SLMessageViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/22.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SLMessageViewController : UIViewController

@property(nonatomic,copy) void (^chooseBlock)(NSString *PersonId); //选择了某一个id的联系人
@end
