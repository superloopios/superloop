//
//  SLSystemMessageViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/22.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SLSystemMessageViewController : BaseViewController

@property (nonatomic,weak)UIView *noNetWorkView;  //没网制空页的view

@end
