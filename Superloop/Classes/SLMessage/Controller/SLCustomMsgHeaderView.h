//
//  SLCustomMsgHeaderView.h
//  Superloop
//
//  Created by WangS on 16/8/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLCustomMsgHeaderView : UIView

@property (nonatomic,strong) UIImageView *iconImgView;
@property (nonatomic,strong) UIImageView *cutImgView;
@property (nonatomic,strong) UIImageView *redImgView;
@property (nonatomic,strong) UILabel *titleLab;
@property (nonatomic,strong) UILabel *numLab;

@end
