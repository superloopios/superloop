//
//  SLWeChatView.m
//  Superloop
//
//  Created by xiaowu on 16/11/14.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLWeChatView.h"



@implementation SLWeChatView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
//        self.backgroundColor = [UIColor grayColor];
        UIView *leftLine = [[UIView alloc] init];
        leftLine.backgroundColor = SLColor(230, 230, 230);
        leftLine.frame = CGRectMake(0, 10, (screen_W-40-130)/2, 1);
        [self addSubview:leftLine];
        
        UIImageView *leftImageView = [[UIImageView alloc] init];
        leftImageView.frame = CGRectMake((screen_W-40-130)/2, 0, 10, 20);
        leftImageView.image = [UIImage imageNamed:@"scaleSquare"];
        leftImageView.contentMode = UIViewContentModeCenter;
        [self addSubview:leftImageView];
        
        UILabel *wechatLoginLabel = [[UILabel alloc] init];
        wechatLoginLabel.font = [UIFont systemFontOfSize:14];
        wechatLoginLabel.textColor = SLColor(98, 164, 58);
        wechatLoginLabel.frame = CGRectMake((screen_W-40-130)/2+10, 0, 110, 20);
        wechatLoginLabel.textAlignment = NSTextAlignmentCenter;
        wechatLoginLabel.text = @"使用微信登录";
        [self addSubview:wechatLoginLabel];
        
        UIImageView *rightImageView = [[UIImageView alloc] init];
        rightImageView.frame = CGRectMake((screen_W-40-130)/2+120, 0, 10, 20);
        rightImageView.image = [UIImage imageNamed:@"scaleSquare"];
        rightImageView.contentMode = UIViewContentModeCenter;
        [self addSubview:rightImageView];
        
        UIView *rightLine = [[UIView alloc] init];
        rightLine.backgroundColor = SLColor(230, 230, 230);
        rightLine.frame = CGRectMake((screen_W-40-130)/2+130, 10, (screen_W-40-130)/2, 1);
        [self addSubview:rightLine];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:@"wechatLogin"] forState:UIControlStateNormal];
        CGFloat WH = 75;
        btn.frame = CGRectMake((screen_W-40-WH)/2, 35, WH, WH);
        [self addSubview:btn];
        [btn addTarget:self action:@selector(wechatLoginClick) forControlEvents:UIControlEventTouchUpInside];
        [btn setAdjustsImageWhenHighlighted:NO];
        
    }
    return self;
}
- (void)wechatLoginClick{
    if (self.wechatLogin) {
        self.wechatLogin();
    }
    
}


@end
