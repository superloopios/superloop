//
//  SLGoToRegisterView.m
//  Superloop
//
//  Created by xiaowu on 16/11/18.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLGoToRegisterView.h"

@interface SLGoToRegisterView ()

@property (nonatomic, strong) UIView                        *registerView;
@property (nonatomic, strong) UILabel                       *titleLable;
@property (nonatomic, strong) UILabel                       *agreeLable;
@property (nonatomic, strong) UIButton                      *userAgreementBtn;
@property (nonatomic, strong) UIButton                      *userSecrecy;
@property (nonatomic, strong) UILabel                       *andLab;
@property (nonatomic, strong) UIView                        *sepatorView;
@property (nonatomic, strong) UIView                        *sepatorView2;
@property (nonatomic, strong) UIButton                      *registerBtn;
@property (nonatomic, strong) UIButton                      *cancelBtn;

@end

@implementation SLGoToRegisterView

- (UIButton *)registerBtn{
    if (!_registerBtn) {
        _registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_registerBtn setTitleColor:UIColorFromRGB(0xff5a5f) forState:UIControlStateNormal];
        [_registerBtn setTitle:@"注册" forState:UIControlStateNormal];
        _registerBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16];
//        _registerBtn.backgroundColor = [UIColor blueColor];
        [self.registerView addSubview:_registerBtn];
        [_registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.sepatorView2.mas_right);
            make.right.equalTo(self.registerView).offset(-7);
            make.height.mas_equalTo(46);
            make.top.equalTo(self.sepatorView);
        }];
        _registerBtn.tag = 4;
    }
    return _registerBtn;
}
- (UIButton *)cancelBtn{
    if (!_cancelBtn) {
        _cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelBtn setTitleColor:UIColorFromRGB(0xff5a5f) forState:UIControlStateNormal];
        [_cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = [UIFont systemFontOfSize:16];
//        _cancelBtn.backgroundColor = [UIColor grayColor];
        [self.registerView addSubview:_cancelBtn];
        [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.sepatorView);
            make.left.equalTo(self.registerView).offset(7);
            make.right.equalTo(self.sepatorView2.mas_left);
            make.height.mas_equalTo(46);
        }];
        _cancelBtn.tag = 3;
    }
    return _cancelBtn;
}

- (UIView *)sepatorView2{
    if (!_sepatorView2) {
        _sepatorView2 = [[UIView alloc] init];
        _sepatorView2.backgroundColor = SLColor(220, 220, 220);
        [self.registerView addSubview:_sepatorView2];
        [_sepatorView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.registerView);
            make.top.equalTo(self.sepatorView.mas_bottom);
            make.width.equalTo(@0.5);
            make.centerX.equalTo(self.registerView);
        }];
    }
    return _sepatorView2;
}

- (UIView *)sepatorView{
    if (!_sepatorView) {
        _sepatorView = [[UIView alloc] init];
        _sepatorView.backgroundColor = SLColor(220, 220, 220);
        [self.registerView addSubview:_sepatorView];
        [_sepatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.registerView);
            make.top.equalTo(self.userSecrecy.mas_bottom).offset(10);
            make.height.equalTo(@0.5);
        }];
    }
    return _sepatorView;
}

- (UIButton *)userSecrecy{
    if (!_userSecrecy) {
        _userSecrecy = [UIButton buttonWithType:UIButtonTypeCustom];
        [_userSecrecy setTitleColor:SLMainColor forState:UIControlStateNormal];
        [_userSecrecy setTitle:@"《隐私条款》" forState:UIControlStateNormal];
        _userSecrecy.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.registerView addSubview:_userSecrecy];
        [_userSecrecy mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.andLab.mas_right);
            make.centerY.equalTo(self.andLab.mas_centerY);
        }];
        _userSecrecy.tag = 2;
    }
    return _userSecrecy;
}

- (UILabel *)andLab{
    if (!_andLab) {
        _andLab = [[UILabel alloc] init];
        _andLab.font = [UIFont systemFontOfSize:13];
        _andLab.textColor =  UIColorFromRGB(0xff5a5f);
        _andLab.text = @"和";
        //        _cellPhoneLabel.backgroundColor = [UIColor redColor];
        [self.registerView addSubview:_andLab];
        [_andLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.registerView).offset(15);
            make.top.equalTo(self.userAgreementBtn.mas_bottom);
        }];
    }
    return _andLab;
}

- (UIButton *)userAgreementBtn{
    if (!_userAgreementBtn) {
        _userAgreementBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_userAgreementBtn setTitleColor:SLMainColor forState:UIControlStateNormal];
        [_userAgreementBtn setTitle:@"《超级圈用户协议》" forState:UIControlStateNormal];
        _userAgreementBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.registerView addSubview:_userAgreementBtn];
        [_userAgreementBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.agreeLable.mas_right);
            make.centerY.equalTo(self.agreeLable.mas_centerY);
        }];
        _userAgreementBtn.tag = 1;
    }
    return _userAgreementBtn;
}

- (UILabel *)agreeLable{
    if (!_agreeLable) {
        _agreeLable = [[UILabel alloc] init];
        _agreeLable.font = [UIFont systemFontOfSize:13];
        _agreeLable.textColor = UIColorFromRGB(0xff5a5f);
        [self.registerView addSubview:_agreeLable];
        [_agreeLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLable.mas_bottom).offset(5);
            make.left.equalTo(self.registerView).offset(15);
        }];
    }
    return _agreeLable;
}

- (UILabel *)titleLable{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] init];
        _titleLable.font = [UIFont systemFontOfSize:16];
        _titleLable.textColor = UIColorFromRGB(0xff5a5f);
        [self.registerView addSubview:_titleLable];
        [_titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.registerView).offset(15);
            make.centerX.equalTo(self.registerView.mas_centerX);
        }];
    }
    return _titleLable;
}

- (UIView *)registerView{
    if (!_registerView) {
        _registerView = [[UIView alloc] init];
        [self addSubview:_registerView];
        _registerView.backgroundColor = [UIColor whiteColor];
        [_registerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY);
            make.width.equalTo(@(250));
            make.height.equalTo(@(140));
        }];
    }
    return _registerView;
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.frame = CGRectMake(0, 0, screen_W, screen_H);
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        self.registerView.layer.cornerRadius = 10;
        self.registerView.clipsToBounds = YES;
        self.titleLable.text = @"确认注册?";
        self.agreeLable.text = @"注册即代表您同意";
        self.andLab.text = @"和";
        
        self.sepatorView.backgroundColor = SLColor(220, 220, 220);
        self.sepatorView2.backgroundColor = SLColor(220, 220, 220);
        [self.userAgreementBtn addTarget:self action:@selector(ubtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.userSecrecy addTarget:self action:@selector(ubtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.registerBtn addTarget:self action:@selector(ubtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.cancelBtn addTarget:self action:@selector(ubtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
- (void)ubtnClick:(UIButton *)btn{
    if ([self.delegate respondsToSelector:@selector(SLGoToRegisterView:didClickBtnAtIndex:)]) {
        [self.delegate SLGoToRegisterView:self didClickBtnAtIndex:btn.tag];
    }
}


@end
