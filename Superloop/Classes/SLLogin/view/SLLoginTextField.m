//
//  SLLoginTextField.m
//  Superloop
//
//  Created by xiaowu on 16/11/14.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLLoginTextField.h"

@interface SLLoginTextField ()
@property(nonatomic, strong)UILabel *titleLabel;
@end

@implementation SLLoginTextField

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.layer.borderColor = SLMainColor.CGColor;
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 5;
        self.font = [UIFont systemFontOfSize:14];
        self.leftViewMode = UITextFieldViewModeAlways;
        self.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, frame.size.height)];
    }
    return self;
}
- (void)setTitle:(NSString *)title{
    _title = title;
    self.titleLabel.text = title;
    self.titleLabel.font = [UIFont systemFontOfSize:16];
    [self.titleLabel sizeToFit];
}

@end
