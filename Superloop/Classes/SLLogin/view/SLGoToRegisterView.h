//
//  SLGoToRegisterView.h
//  Superloop
//
//  Created by xiaowu on 16/11/18.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLGoToRegisterView;
@protocol GoToRegisterViewDelegate <NSObject>
@optional

- (void)SLGoToRegisterView:(SLGoToRegisterView *)goToRegisterView didClickBtnAtIndex:(NSInteger)index;

@end

@interface SLGoToRegisterView : UIView
@property (nonatomic,weak)id<GoToRegisterViewDelegate> delegate;
@end
