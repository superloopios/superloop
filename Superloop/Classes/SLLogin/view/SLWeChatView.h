//
//  SLWeChatView.h
//  Superloop
//
//  Created by xiaowu on 16/11/14.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^loginWechat)(void);

@interface SLWeChatView : UIView

@property (nonatomic ,copy) loginWechat wechatLogin;
//@property (nonatomic ,strong) void(^wechatLogin)(NSString *str);
@end
