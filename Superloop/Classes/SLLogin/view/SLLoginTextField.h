//
//  SLLoginTextField.h
//  Superloop
//
//  Created by xiaowu on 16/11/14.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLLoginTextField : UITextField

@property(nonatomic, copy)NSString *title;

@end
