//
//  SLUpdateAvatarAndNicknameViewController.m
//  Superloop
//
//  Created by xiaowu on 16/11/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "AppDelegate.h"
#import "SLUpdateAvatarAndNicknameViewController.h"
#import "SLPerfectInformationViewController.h"
#import "SLImageTopTitleBottomBtn.h"
#import "SLBasebutton.h"
#import "SLLoginTextField.h"
#import "SLActionView.h"
#import <AliyunOSSiOS/OSSService.h>
#import "SLAPIHelper.h"
#import "SLMessageManger.h"
#import "SPUtil.h"
#import "NSString+Utils.h"
#import <UIButton+WebCache.h>

@interface SLUpdateAvatarAndNicknameViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,SLActionViewDelegate>

@property (nonatomic, assign) BOOL                          hasNetWork;
@property (nonatomic, strong) UILabel                       *successLabel;
@property (nonatomic, strong) UIButton                      *avatarBtn;
@property (nonatomic, strong) UIButton                      *ignoreBtn;
@property (nonatomic, strong) SLBasebutton                  *changeAvatarBtn;
@property (nonatomic, strong) SLLoginTextField              *nickNameTextfield;
@property (nonatomic, strong) UIButton                      *ensureBtn;
@property (nonatomic, strong) SLActionView                  *actionView;
@property (nonatomic, strong) UIImage                       *avatarImage;
@property (nonatomic, copy  ) NSString                      *bio;

@end

@implementation SLUpdateAvatarAndNicknameViewController

- (SLActionView *)actionView{
    if (!_actionView) {
        _actionView = [[SLActionView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _actionView.delegate = self;
        [self.view addSubview:_actionView];
    }
    return _actionView;
}

- (UIButton *)ensureBtn{
    if (!_ensureBtn) {
        _ensureBtn = [[UIButton alloc] init];
        [_ensureBtn setTitle:@"确定" forState:UIControlStateNormal];
        [_ensureBtn setBackgroundColor:SLMainColor];
        [_ensureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _ensureBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        _ensureBtn.frame = CGRectMake(20, 220, screen_W-40, 38);
        _ensureBtn.layer.cornerRadius = 5;
        [self.view addSubview:_ensureBtn];
        [_ensureBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.nickNameTextfield.mas_bottom).offset(30);
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(40);
        }];
    }
    return _ensureBtn;
}

- (SLLoginTextField *)nickNameTextfield{
    if (!_nickNameTextfield) {
        _nickNameTextfield = [[SLLoginTextField alloc] init];
        _nickNameTextfield.delegate = self;
        [self.view addSubview:_nickNameTextfield];
        [_nickNameTextfield mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.changeAvatarBtn.mas_bottom).offset(25);
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(40);
        }];
    }
    return _nickNameTextfield;
}

- (SLBasebutton *)changeAvatarBtn{
    if (!_changeAvatarBtn) {
        _changeAvatarBtn = [SLBasebutton buttonWithType:UIButtonTypeCustom];
        _changeAvatarBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_changeAvatarBtn setTitle:@"点击添加头像" forState:UIControlStateNormal];
        [_changeAvatarBtn setTitleColor:SLMainColor forState:UIControlStateNormal];
        [self.view addSubview:_changeAvatarBtn];
        [_changeAvatarBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.avatarBtn.mas_bottom).offset(5);
            make.centerX.mas_equalTo(self.view);
            make.width.mas_equalTo(90);
            make.height.mas_equalTo(20);
        }];
    }
    return _changeAvatarBtn;
}

- (UIButton *)avatarBtn{
    if (!_avatarBtn) {
        _avatarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if ([NSString isRealString:ApplicationDelegate.userInformation[@"avatar"]]) {
            NSURL *avatar = [NSURL URLWithString:ApplicationDelegate.userInformation[@"avatar"]];
            [_avatarBtn sd_setImageWithURL:avatar forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
        }else{
            [_avatarBtn setImage:[UIImage imageNamed:@"newPersonAvatar"] forState:UIControlStateNormal];
        }
        
        [_avatarBtn addTarget:self action:@selector(changeAvatar) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_avatarBtn];
        _avatarBtn.layer.cornerRadius = 10;
        _avatarBtn.clipsToBounds = YES;
        [_avatarBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.successLabel.mas_bottom).offset(20);
            make.centerX.mas_equalTo(self.view);
            make.width.mas_equalTo(90);
            make.height.mas_equalTo(90);
        }];
        [_avatarBtn setAdjustsImageWhenHighlighted:NO];
    }
    return _avatarBtn;
}

- (UILabel *)successLabel{
    if (!_successLabel) {
        _successLabel=[[UILabel alloc] init];
        _successLabel.numberOfLines = 2;
        //富文本设置
        NSString *str = @"";
        if (self.isWeChatRegister) {
            str = @"注册成功！\n是否使用微信的头像和昵称？";
        }else{
            str = @"注册成功！\n设置昵称和头像，让更多人认识你吧！";
        }
        NSRange range = NSMakeRange(0, str.length);
        NSLog(@"%@",NSStringFromRange(range));
        NSMutableAttributedString *absAttention=[[NSMutableAttributedString alloc] initWithString:str];
        [absAttention addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:range];
        [absAttention addAttribute:NSForegroundColorAttributeName value:SLMainColor range:range];
        
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3;
        [absAttention addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
        
        _successLabel.attributedText=absAttention;
        
        [self.view addSubview:_successLabel];
        //        [_attentionLab sizeToFit];
        [_successLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view).offset(67);
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
        }];
        
    }
    return _successLabel;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    [self setUpNav];
    [self setUpUI];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
-(void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetWork=NO;
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            self.hasNetWork=YES;
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.hasNetWork=YES;
        }
    }
}
- (void)setUpUI{
    [self.view addSubview:self.successLabel];
    [self.avatarBtn addTarget:self action:@selector(changeAvatar) forControlEvents:UIControlEventTouchUpInside];
    [self.changeAvatarBtn addTarget:self action:@selector(changeAvatar) forControlEvents:UIControlEventTouchUpInside];
    self.nickNameTextfield.placeholder = @"你的昵称";
    if ([NSString isRealString:ApplicationDelegate.userInformation[@"nickname"]]) {
        self.nickNameTextfield.text = ApplicationDelegate.userInformation[@"nickname"];
    }else{
        self.nickNameTextfield.text = [NSString stringWithFormat:@"super%@",ApplicationDelegate.userId];
    }
    [self.ensureBtn addTarget:self action:@selector(ensureChangeNickname) forControlEvents:UIControlEventTouchUpInside];
}
- (void)ensureChangeNickname
{
    [self.view endEditing:YES];
    if ([NSString isEmptyStrings:self.nickNameTextfield.text]) {

        [HUDManager showWarningWithText:@"昵称不能为空"];
    }else if (self.nickNameTextfield.text.length>32){

        [HUDManager showWarningWithText:@"昵称过长,暂不支持"];
    }else{
        if ([self judgeModifyNickName:self.nickNameTextfield.text]) {
            self.ensureBtn.userInteractionEnabled = NO;
            NSDictionary *parameters = @{@"nickname":self.nickNameTextfield.text};
            [HUDManager showLoadingHUDView:self.view withText:@"请稍后"];
            [SLAPIHelper resetUsers:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                NSLog(@"success");
                [HUDManager hideHUDView];
                self.ensureBtn.userInteractionEnabled = YES;
                SLPerfectInformationViewController *vc = [[SLPerfectInformationViewController alloc] init];
                __weak typeof(self) weakSelf = self;
                vc.introduceBlcok=^(NSString *str){
                    weakSelf.bio = str;
                    NSLog(@"%@",str);
                };
                [self.navigationController pushViewController:vc animated:YES];
                
            } failure:^(SLHttpRequestError *failure) {
                [HUDManager hideHUDView];
                self.ensureBtn.userInteractionEnabled = YES;
                if (failure.slAPICode==20) {
                    [HUDManager showWarningWithText:@"操作失败,昵称中包含违禁词语"];
                }
            }];
        }else{
//            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"请按照要求输入昵称" message:nil delegate:self cancelButtonTitle:@"好" otherButtonTitles: nil];
//            [alert show];
            [HUDManager showWarningWithText:@"昵称须为4-60个字符\n不能包含emoji表情"];
        }
    }
}
-(BOOL)judgeModifyNickName:(NSString *)string{
    NSString *regex = @"[\\w,_,\\-,0x4e00-0x9fa6]{1,50}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:string];
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
//    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
//    backBtn.frame=CGRectMake(0, 20, 60, 44);
//    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
//    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
//    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
//    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"注册成功";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor = SLMainColor;
    [navView addSubview:nameLab];
    
    _ignoreBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    _ignoreBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    [_ignoreBtn setTitle:@"跳过" forState:UIControlStateNormal];
    _ignoreBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_ignoreBtn setTitleColor:SLMainColor forState:UIControlStateNormal];
    [_ignoreBtn addTarget:self action:@selector(ignore) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:_ignoreBtn];
    
    [self.view addSubview:navView];
}

- (void)ignore{
    [self.view endEditing:YES];
    SLPerfectInformationViewController *vc = [[SLPerfectInformationViewController alloc] init];
    __weak typeof(self) weakSelf = self;
    vc.introduceBlcok=^(NSString *str){
        weakSelf.bio = str;
        NSLog(@"%@",str);
    };
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)backBtnClick{
    
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)changeAvatar {
    [self.view endEditing:YES];
    self.actionView.titleArr = @[@"取消",@"从相册选择",@"拍照"];
    self.actionView.tag = 1;
    [self.view addSubview:self.actionView];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    [self setSpace:textField withValue:textField.text withFont:[UIFont systemFontOfSize:16]];
    return YES;
}
//设置行间距和字间距
- (void)setSpace:(UITextField *)textField withValue:(NSString*)str withFont:(UIFont*)font {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f};
    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
    textField.attributedText = attributeStr;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
- (void)actionSheetButtonClickedAtIndex:(NSInteger)index{
    [self chooseImgae:index];
    [self.actionView dismiss];
}
- (void)chooseImgae:(NSInteger)buttonIndex{
    NSUInteger sourceType = 0;
    switch (buttonIndex) {
        case 2:
            sourceType = UIImagePickerControllerSourceTypeCamera;
            break;
        case 1:
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            break;
    }
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    
    imagePickerController.allowsEditing = YES;
    
    imagePickerController.sourceType = sourceType;
    
    [self presentViewController:imagePickerController animated:YES completion:^{}];
    
}
//验证图片是否包含二维码
- (BOOL)validateImgByQRcode:(UIImage *)image{
    //返回的UIImage
    CIImage *ciImage = [CIImage imageWithCGImage:[image CGImage]];
    CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:nil];
    NSArray *arr = [detector featuresInImage:ciImage];
    if (arr.count > 0){
        return YES;
    }else{
        return NO;
    }
}
#pragma mark - image picker delegte
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [picker dismissViewControllerAnimated:YES completion:^{}];
    
    
    self.avatarImage = [info objectForKey:UIImagePickerControllerEditedImage];
    NSData *imageData = UIImageJPEGRepresentation(self.avatarImage, 0.01);
    //验证图片是否包含二维码
    if ([self validateImgByQRcode:self.avatarImage]) {
        [HUDManager showWarningWithText:@"不能上传包含二维码的图片"];
    }else{
        // 保存图片至本地，方法见下文
        //        [self saveImage:image withName:@"currentImage.png"];
        //上传图片
        dispatch_async(dispatch_get_main_queue(), ^{
            UIWindow *win = [[UIApplication sharedApplication].windows objectAtIndex:0];
            
            [HUDManager showLoadingHUDView:win withText:@"正在上传中"];
        });
#ifdef DEBUG
        NSString *endpoint = @"http://dev.oss.superloop.com.cn";
#else
        NSString *endpoint = @"http://oss.superloop.com.cn";
#endif
        id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:@"UPhdoMR1eN1hnrdA" secretKey:@"ETuIdMVCT1mtiqs8f5KZ8dZnZLiXfG"];
        OSSClient *client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential];
        OSSPutObjectRequest *put = [OSSPutObjectRequest new];
#ifdef DEBUG
        put.bucketName = @"superloop-dev";
#else
        put.bucketName = @"superloop";
#endif
        NSString *uuid= [[NSUUID UUID] UUIDString];
        //减去"-"
        NSString *str = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
        //变小写
        NSString *lower = [str lowercaseString];
        put.objectKey = [NSString stringWithFormat:@"%@/superloop_%@.jpg",ApplicationDelegate.userId,lower];
        put.uploadingData = imageData;
        put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
        };
        OSSTask * putTask = [client putObject:put];
        [putTask continueWithBlock:^id(OSSTask *task) {
            if (!task.error) {
                NSDictionary *parameters;
                parameters = @{@"avatar":put.objectKey};
                [SLAPIHelper introduce:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [HUDManager hideHUDView];
                        [self setLoginIM];
                        [self.avatarBtn setImage:self.avatarImage forState:UIControlStateNormal];
                    });
                    
                } failure:^(SLHttpRequestError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [HUDManager hideHUDView];
                        [HUDManager showWarningWithText:@"上传头像失败"];
                    });
                }];
                
            } else {
                NSLog(@"%@",[NSThread currentThread]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUDManager hideHUDView];
                    [HUDManager showWarningWithText:@"上传头像失败"];
                });
            }
            return nil;
        }];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}


- (void)setLoginIM{
    NSDictionary *dict = [SLUserDefault getUserInfo];
    if (dict) {
        NSString *password = @"";
        if ([dict objectForKey:@"Password"]) {
            //            password = [AESCrypt decrypt:[dict objectForKey:@"Password"] password:@"chaojiquan"];
            password = [dict objectForKey:@"Password"];
            __weak typeof(self) weakSelf = self;
            [[SLMessageManger sharedInstance] callThisAfterISVAccountLoginSuccessWithYWLoginId:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]
                                                                                      passWord:[dict objectForKey:@"Password"]
                                                                               preloginedBlock:^{
                                                                                   // 显示会话列表页面
                                                                               } successBlock:^{
                                                                                   
                                                                                   [[SPUtil sharedInstance] setWaitingIndicatorShown:NO withKey:weakSelf.description];
                                                                                   
                                                                               } failedBlock:^(NSError *aError) {
                                                                                   
                                                                               }];
        }
    }
}
@end
