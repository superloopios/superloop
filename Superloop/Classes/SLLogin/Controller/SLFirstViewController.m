//
//  SLFirstViewController.m
//  Superloop
//
//  Created by WangS on 16/6/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "AppDelegate.h"
#import "SLFirstViewController.h"
#import "SLRegisterUserViewController.h"
#import "SLForgetPassWordViewController.h"
#import "SLWeChatRegisterViewController.h"
#import "SLWebViewController.h"
#import "SLUpdateAvatarAndNicknameViewController.h"
#import "SLTabBarViewController.h"



#import "SLLoginTextField.h"
#import "SLWeChatView.h"
#import "SLGoToRegisterView.h"
#import "NSString+Utils.h"
#import "AESCrypt.h"
#import "Utils.h"
#import "SLAPIHelper.h"
#import "SLIMCommon.h"
#import "UMSocial.h"



@interface SLFirstViewController ()<UITextFieldDelegate,UIScrollViewDelegate,GoToRegisterViewDelegate>



@property (nonatomic, strong) UIButton                      *userPasswordLoginBtn;
@property (nonatomic, strong) UISegmentedControl            *loginSeg;
@property (nonatomic, strong) SLLoginTextField              *phoneTextfield;
@property (nonatomic, strong) SLLoginTextField              *passwordTextfield;
@property (nonatomic, strong) SLLoginTextField              *verificationTextfield;
@property (nonatomic, strong) UIView                        *passwordView;
@property (nonatomic, strong) UIView                        *verificationView;
@property (nonatomic, strong) UIButton                      *forgetPwdBtn;
@property (nonatomic, strong) UIButton                      *getVerificationBtn;
@property (nonatomic, strong) UIButton                      *registBtn;
@property (nonatomic, strong) UIButton                      *loginBtn;
@property (nonatomic, strong) SLWeChatView                  *weChatView;
@property (nonatomic, strong) NSMutableDictionary           *parameter;
@property (nonatomic, assign) BOOL                          hasNetWork;
@property (nonatomic, strong) UIScrollView                  *baseSc;
@property (nonatomic, strong) SLGoToRegisterView            *goToRegisterView;
@property (nonatomic, strong) NSNumber                      *action;   //action为1登录，action为2注册
@property (nonatomic,   copy) NSString                      *openID;
@property (nonatomic,   copy) NSString                      *access_token;


@end


@implementation SLFirstViewController

- (SLGoToRegisterView *)goToRegisterView{
    if (!_goToRegisterView) {
        _goToRegisterView = [[SLGoToRegisterView alloc] init];
        _goToRegisterView.delegate = self;
        [self.view addSubview:_goToRegisterView];
    }
    return _goToRegisterView;
}

- (UIScrollView *)baseSc{
    if (!_baseSc) {
        _baseSc = [[UIScrollView alloc] init];
        _baseSc.frame = CGRectMake(0, 64, ScreenW, screen_H-64);
        _baseSc.delegate = self;
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEdit)];
        [_baseSc addGestureRecognizer:ges];
        [self.view addSubview:_baseSc];
    }
    return _baseSc;
}

-(NSMutableDictionary *)parameter{
    if (!_parameter) {
        _parameter=[NSMutableDictionary new];
    }
    return _parameter;
}

- (SLWeChatView *)weChatView{
    if (!_weChatView) {
        _weChatView = [[SLWeChatView alloc] init];
        __weak typeof(self) weakSelf = self;
        _weChatView.wechatLogin = ^{
            [weakSelf getUserInfoForPlatform];
        };
        _weChatView.frame = CGRectMake(20, 450-64, screen_W-40, 110);
        [self.baseSc addSubview:_weChatView];
    }
    return _weChatView;
}

- (UIButton *)loginBtn{
    if (!_loginBtn) {
        _loginBtn = [[UIButton alloc] init];
        [_loginBtn setTitle:@"登录" forState:UIControlStateNormal];
        [_loginBtn setBackgroundColor:SLMainColor];
        [_loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _loginBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _loginBtn.frame = CGRectMake(20, 305-64, screen_W-40, 40);
        _loginBtn.layer.cornerRadius = 5;
        [self.baseSc addSubview:_loginBtn];
    }
    return _loginBtn;
}
- (UIButton *)registBtn{
    if (!_registBtn) {
        _registBtn = [[UIButton alloc] init];
        [_registBtn setTitle:@"注册" forState:UIControlStateNormal];
        [_registBtn setTitleColor:SLMainColor forState:UIControlStateNormal];
        _registBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _registBtn.frame = CGRectMake(20, 385-64, screen_W-40, 40);
        _registBtn.layer.cornerRadius = 5;
        _registBtn.layer.borderWidth = 1;
        _registBtn.layer.borderColor = SLMainColor.CGColor;
        [self.baseSc addSubview:_registBtn];
    }
    return _registBtn;
}

- (UIButton *)getVerificationBtn{
    if (!_getVerificationBtn) {
        _getVerificationBtn = [[UIButton alloc] init];
        [_getVerificationBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_getVerificationBtn setBackgroundColor:SLMainColor];
        [_getVerificationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _getVerificationBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _getVerificationBtn.frame = CGRectMake(screen_W-40-80, 0, 80, 38);
        _getVerificationBtn.layer.cornerRadius = 5;
        [self.verificationView addSubview:_getVerificationBtn];
    }
    return _getVerificationBtn;
}

- (UIButton *)forgetPwdBtn{
    if (!_forgetPwdBtn) {
        _forgetPwdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_forgetPwdBtn setTitle:@"忘记密码？" forState:UIControlStateNormal];
        [_forgetPwdBtn setTitleColor:SLMainColor forState:UIControlStateNormal];
        _forgetPwdBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _forgetPwdBtn.frame = CGRectMake(screen_W-40-100, 40, 100, 25);
        _forgetPwdBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [self.passwordView addSubview:_forgetPwdBtn];
    }
    return _forgetPwdBtn;
}

- (UIView *)passwordView{
    if (!_passwordView) {
        _passwordView = [[UIView alloc] initWithFrame:CGRectMake(20, 200-64, screen_W-40, 65)];
        _passwordView.backgroundColor = [UIColor whiteColor];
        [self.baseSc addSubview:_passwordView];
    }
    return _passwordView;
}
- (UIView *)verificationView{
    if (!_verificationView) {
        _verificationView = [[UIView alloc] initWithFrame:CGRectMake(20, 200-64, screen_W-40, 65)];
        _verificationView.backgroundColor = [UIColor whiteColor];
        
        [self.baseSc addSubview:_verificationView];
    }
    return _verificationView;
}

- (SLLoginTextField *)phoneTextfield{
    if (!_phoneTextfield) {
        _phoneTextfield = [[SLLoginTextField alloc] init];
        _phoneTextfield.frame = CGRectMake(20, 150-64, screen_W-40, 40);
        _phoneTextfield.delegate = self;
        [self.baseSc addSubview:_phoneTextfield];
    }
    return _phoneTextfield;
}
- (SLLoginTextField *)passwordTextfield{
    if (!_passwordTextfield) {
        _passwordTextfield = [[SLLoginTextField alloc] init];
        _passwordTextfield.frame = CGRectMake(0, 0, screen_W-40, 40);
        _passwordTextfield.delegate = self;
        [self.passwordView addSubview:_passwordTextfield];
        _passwordTextfield.secureTextEntry = YES;
    }
    return _passwordTextfield;
}

- (SLLoginTextField *)verificationTextfield{
    if (!_verificationTextfield) {
        _verificationTextfield = [[SLLoginTextField alloc] init];
        _verificationTextfield.frame = CGRectMake(0, 0, screen_W-130, 40);
        _verificationTextfield.delegate = self;
        [self.verificationView addSubview:_verificationTextfield];
    }
    return _verificationTextfield;
}

- (UISegmentedControl *)loginSeg{
    if (!_loginSeg) {
        NSArray *segmentedArray = @[@"密码登录",@"验证码登录"];
        
        _loginSeg = [[UISegmentedControl alloc] initWithItems:segmentedArray];
        _loginSeg.frame = CGRectMake(20,85-64,screen_W-40,42);
        [self.baseSc addSubview:_loginSeg];
        _loginSeg.tintColor = SLMainColor;
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:15],NSFontAttributeName ,nil];
        [_loginSeg setTitleTextAttributes:dic forState:UIControlStateNormal];
        [_loginSeg addTarget:self action:@selector(changeLoginModel:) forControlEvents:UIControlEventValueChanged];
    }
    return _loginSeg;
}
- (void)changeLoginModel:(UISegmentedControl *)seg{
    [self.view endEditing:YES];
    self.phoneTextfield.text = nil;
    if (seg.selectedSegmentIndex==0) {
        self.passwordView.hidden = NO;
        self.verificationView.hidden = YES;
    }else{
        self.passwordView.hidden = YES;
        self.verificationView.hidden = NO;
    }
}
- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=YES;
    [super viewWillAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    [self setUpNav];
    self.loginSeg.selectedSegmentIndex = 0;
    [self setupUI];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
    
}
-(void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetWork=NO;
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            self.hasNetWork=YES;
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.hasNetWork=YES;
        }
    }
}
- (void)setupUI{
    self.phoneTextfield.placeholder = @"手机号/超级号";
    self.passwordTextfield.placeholder = @"密码";
    [self.forgetPwdBtn addTarget:self action:@selector(forgetPwd) forControlEvents:UIControlEventTouchUpInside];
    self.passwordView.hidden = NO;
    self.verificationView.hidden = YES;
    self.verificationTextfield.placeholder = @"验证码";
    [self.getVerificationBtn addTarget:self action:@selector(getVerification) forControlEvents:UIControlEventTouchUpInside];
    [self.loginBtn addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [self.registBtn addTarget:self action:@selector(registerClick) forControlEvents:UIControlEventTouchUpInside];
    [self.baseSc addSubview:self.weChatView];
    [self.baseSc setContentSize:CGSizeMake(0, 500)];
}
- (void)login{
    [self.view endEditing:YES];
    if (self.hasNetWork) {
        if (self.loginSeg.selectedSegmentIndex == 0) {
            self.loginBtn.userInteractionEnabled = NO;
            NSString *account = [self.phoneTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSString *password = [self.passwordTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if([self validateAccount:account password:password]) {
                [self loginPassWord:account password:password];
            }
        }else{
            [self loginSystemUserVerification];
        }
    }else{
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请检查网络" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        [alertView show];
        [HUDManager showWarningWithText:@"网络异常，请检查网络"];
    }
}
- (void)loginSystemUserVerification{
    NSString *account = [self.phoneTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *password = [self.verificationTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if([self validateAccount:account password:password]) {
        if ([self.action isEqual:@1]) {
            [self loginPassWord:account password:password];
        }else if ([self.action isEqual:@2]){
            [self.view addSubview:self.goToRegisterView];
        }
    }
}

- (void)loginPassWord:(NSString *)account password:(NSString *)password{
    
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow withText:@"正在登录"];
    
    //对应的加密密钥
    __block  NSString *security=@"";
    //获取AES加密密钥
    NSString *uuid= [[NSUUID UUID] UUIDString];
    //减去"-"
    NSString *tokenStr = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSDictionary *params=@{@"token":tokenStr};
    [SLAPIHelper getAESSecurity:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        security = data[@"result"];
        
        NSString *encryptCellphone = [AESCrypt encrypt:account password:security];
        NSString *encryptPassword = [AESCrypt encrypt:password password:security];
        if ([GeTuiSdk clientId]) {
            [self.parameter setObject:[GeTuiSdk clientId] forKey:@"cid"];
        }
        [self.parameter setObject:@2 forKey:@"os_platform"];
        [self.parameter setObject:tokenStr forKey:@"token"];
        if (self.loginSeg.selectedSegmentIndex == 0) {
            [self.parameter setObject:encryptCellphone forKey:@"login_name"];
            [self.parameter setObject:encryptPassword forKey:@"password"];
            [self encryptSuccess:security account:account];
        }else{
            [self.parameter setObject:encryptCellphone forKey:@"login_name"];
            [self.parameter setObject:encryptPassword forKey:@"code"];
            [self verificationEncryptSuccess:security account:account];
        }
        
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        self.loginBtn.userInteractionEnabled = YES;
        if (failure.httpStatusCode == 500) {
            [HUDManager showWarningWithText:@"未知错误"];
        }else{
            [HUDManager showWarningWithText:@"登录失败,请重试"];
        }
    }];
}
//验证码加密成功后登陆
- (void)verificationEncryptSuccess:(NSString *)security account:(NSString *)account{
    [SLAPIHelper loginUseLoginCode:self.parameter success:^(NSURLSessionDataTask *task,  NSDictionary* data) {
//        NSLog(@"data%@",data);
        ApplicationDelegate.isLogin = YES;
        
        //如果登录成功之后保存手机号码
        NSUserDefaults *phoneNumber = [NSUserDefaults standardUserDefaults];
        [phoneNumber setObject:data[@"result"][@"cellphone"] forKey:@"phoneNumber"];
        [phoneNumber synchronize];
        
        ApplicationDelegate.userId = data[@"result"][@"id"];
        NSString *nameAndPwd  = [NSString stringWithFormat:@"%@:%@",data[@"result"][@"id"], data[@"result"][@"password"]];
        ApplicationDelegate.Basic = [nameAndPwd base64EncodedString];
        ApplicationDelegate.userInformation = [data[@"result"] mutableCopy];
        
        NSString *im_password=data[@"result"][@"im_password"];
        NSString * decryptPassword = [AESCrypt decrypt:im_password password:security];
        
#warning 待测试
        [self saveBasic:ApplicationDelegate.Basic userInformation:ApplicationDelegate.userInformation password:decryptPassword];
        //应用登陆成功后，登录openIM
//        NSLog(@"%@,%@",account,decryptPassword);
        [SLIMCommon Login:account password:decryptPassword];
        [HUDManager hideHUDView];
        [HUDManager showWarningWithText:@"登录成功"];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"isLoginSuccessToLoadData" object:self];
        //        if (self.isTabbarVC) {
        //            [[NSNotificationCenter defaultCenter] postNotificationName:@"isLoginSuccessChangeTabbarVC" object:self];
        //        }
        //        if (self.isReplacePasswordVC){
        //            [[NSNotificationCenter defaultCenter] postNotificationName:@"isLoginSuccessChangeTabbarVC" object:self];
        //        }
        if (self.loginSeg.selectedSegmentIndex == 1) {
            if ([self.action isEqual:@2]) {
                SLUpdateAvatarAndNicknameViewController *vc = [[SLUpdateAvatarAndNicknameViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }else if ([self.action isEqual:@1]){
                SLTabBarViewController *indexVC = [[SLTabBarViewController alloc] init];
                [UIApplication sharedApplication].keyWindow.rootViewController = indexVC;
            }
        }else{
            SLTabBarViewController *indexVC = [[SLTabBarViewController alloc] init];
            [UIApplication sharedApplication].keyWindow.rootViewController = indexVC;
        }
        
    } failure:^(SLHttpRequestError *failure) {
        self.getVerificationBtn.userInteractionEnabled = YES;
        self.loginBtn.userInteractionEnabled = YES;
        [HUDManager hideHUDView];
        NSLog(@"%ld,%ld",failure.httpStatusCode,failure.slAPICode);
        if (failure.httpStatusCode==403) {
            if (failure.slAPICode==206||failure.slAPICode==207) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [HUDManager showWarningWithText:@"登录失败,请检查手机号和密码"];
                });
            }
        }else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [HUDManager showWarningWithText:@"登录失败,请稍后重试"];
            });
        }
    }];
}
//加密成功后登陆
-(void)encryptSuccess:(NSString *)security account:(NSString *)account{
    
    [SLAPIHelper login:self.parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSLog(@"data%@",data);
        ApplicationDelegate.isLogin = YES;
        if ([Utils isValidMobileNumber:self.phoneTextfield.text]) {
            //如果登录成功之后保存手机号码
            NSUserDefaults *phoneNumber = [NSUserDefaults standardUserDefaults];
            [phoneNumber setObject:self.phoneTextfield.text forKey:@"phoneNumber"];
            [phoneNumber synchronize];
        }
        ApplicationDelegate.userId = data[@"result"][@"id"];
        NSString *nameAndPwd  = [NSString stringWithFormat:@"%@:%@",data[@"result"][@"id"], data[@"result"][@"password"]];
        ApplicationDelegate.Basic = [nameAndPwd base64EncodedString];
        ApplicationDelegate.userInformation = [data[@"result"] mutableCopy];
        
        NSString *im_password=data[@"result"][@"im_password"];
        NSString * decryptPassword = [AESCrypt decrypt:im_password password:security];
        
        [self saveBasic:ApplicationDelegate.Basic userInformation:ApplicationDelegate.userInformation password:decryptPassword];
        //应用登陆成功后，登录openIM
        NSLog(@"%@,%@",account,decryptPassword);
        [SLIMCommon Login:account password:decryptPassword];
        [HUDManager hideHUDView];
        [HUDManager showWarningWithText:@"登录成功"];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"isLoginSuccessToLoadData" object:self];
//        if (self.isTabbarVC) {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"isLoginSuccessChangeTabbarVC" object:self];
//        }
//        if (self.isReplacePasswordVC){
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"isLoginSuccessChangeTabbarVC" object:self];
//        }
        SLTabBarViewController *indexVC = [[SLTabBarViewController alloc] init];
        [UIApplication sharedApplication].keyWindow.rootViewController = indexVC;
    } failure:^(SLHttpRequestError *error) {
        self.loginBtn.userInteractionEnabled = YES;
        [HUDManager hideHUDView];
        if (error.httpStatusCode==403) {
            if (error.slAPICode==206||error.slAPICode==207) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [HUDManager showWarningWithText:@"登录失败,请检查手机号和密码"];
                });
            }
        }else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [HUDManager showWarningWithText:@"登录失败,请稍后重试"];
            });
        }
    }];
    
}


- (void)saveBasic:(NSString *)basic userInformation:(NSMutableDictionary *)userInformation password:(NSString *)password{
    NSDictionary *dict = @{@"basic": basic, @"userInformation": userInformation,@"Password": password};
    [SLUserDefault setUserInfo:dict];
}

- (void)registerClick{
    SLRegisterUserViewController *registerView = [[SLRegisterUserViewController alloc] init];
    [self.navigationController pushViewController:registerView animated:YES];
}
- (void)forgetPwd{
    SLForgetPassWordViewController *foggetPassword = [[SLForgetPassWordViewController alloc] init];
    [self.navigationController pushViewController:foggetPassword animated:YES];
}
- (void)getVerification{
    if (!self.hasNetWork) {
        [HUDManager showWarningWithText:@"网络出错,请检查网络状况"];
    }else{
        //简单的判断手机格式
        if ([Utils isValidMobileNumber:self.phoneTextfield.text] || [self.phoneTextfield.text hasPrefix:@"super"]) {
            self.getVerificationBtn.userInteractionEnabled = NO;
            NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
            //对应的加密密钥
            __block  NSString *security=@"";
            //获取AES加密密钥
            NSString *uuid= [[NSUUID UUID] UUIDString];
            //减去"-"
            NSString *tokenStr = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
            NSDictionary *params=@{@"token":tokenStr};
            [HUDManager showLoadingHUDView:self.view withText:@""];
            [SLAPIHelper getAESSecurity:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
                
                security = data[@"result"];
                NSString *encryptCellphone = [AESCrypt encrypt:self.phoneTextfield.text password:security];
                [parameter setObject:encryptCellphone forKey:@"login_name"];
                [parameter setObject:tokenStr forKey:@"token"];
                [self encryptSuccess:parameter];
            } failure:^(SLHttpRequestError *failure) {
                [HUDManager hideHUDView];
                self.getVerificationBtn.userInteractionEnabled = YES;
                [HUDManager showWarningWithText:@"获取验证码失败"];
            }];
        }else{
            [HUDManager showWarningWithText:@"请输入正确的手机号码或超级号"];
        }
        
    }
}
//加密成功后获取验证码
-(void)encryptSuccess:(NSMutableDictionary *)parameter {
    //    __weak typeof(self) weakSelf = self;
    NSLog(@"%@",parameter);
    [SLAPIHelper getVerificationLoginCode:parameter success:^(NSURLSessionDataTask *task,  NSDictionary* data) {
        [self startTimer];
//        NSLog(@"data%@",data);
        [HUDManager hideHUDView];
        self.action = data[@"result"][@"action"];
//        self.verificationTextfield.text = data[@"result"][@"code"];
        NSLog(@"%@",NSStringFromClass([self.action class]));
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        self.getVerificationBtn.userInteractionEnabled = YES;
        if (failure.httpStatusCode==429&&failure.slAPICode==10) {
            [HUDManager showWarningWithText:@"操作频繁,请稍后重试"];
        }else{
            [HUDManager showWarningWithText:@"获取验证码失败"];
        }
    }];
}
- (void)startTimer
{
    __block int timeout = 60; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0); //每秒执行
    __weak typeof(self) weakSelf = self;
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout <= 0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [weakSelf.getVerificationBtn setTitle:@"重新获取" forState:UIControlStateNormal];
                weakSelf.getVerificationBtn.userInteractionEnabled = YES;
            });
        }else{
            NSString *strTime = [NSString stringWithFormat:@"%d秒", timeout];
            dispatch_async(dispatch_get_main_queue(), ^{
                //        设置界面的按钮显示 根据自己需求设置
                [weakSelf.getVerificationBtn setTitle:strTime forState:UIControlStateNormal];
                weakSelf.getVerificationBtn.userInteractionEnabled = NO;
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}




//- (void)registerBtnClick:(id)sender {
//    SLRegisterViewController *registerView = [[SLRegisterViewController alloc] init];
//    registerView.isTabbarVC=self.isTabbarVC;
//    [self.navigationController pushViewController:registerView animated:YES];
//}

- (void)backBtnClick{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"登录";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor = SLMainColor;
    [navView addSubview:nameLab];
    
    [self.view addSubview:navView];
}
//-(void)textViewDidChange:(UITextView *)textView{
//    [self setSpace:self.replyTextView withValue:self.replyTextView.text withFont:[UIFont systemFontOfSize:15]];
//}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//    [self setSpace:textField withValue:textField.text withFont:[UIFont systemFontOfSize:16]];
//    return YES;
//}
//设置行间距和字间距
- (void)setSpace:(UITextField *)textField withValue:(NSString*)str withFont:(UIFont*)font {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f};
    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
    textField.attributedText = attributeStr;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [HUDManager hideHUDView];
}
// 验证账号和手机
- (BOOL)validateAccount:(NSString *)account password:(NSString *)password{
    if ([account hasPrefix:@"1"]) {
        if (![Utils isValidMobileNumber:account]) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请输入正确手机号" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
//            [alertView show];
            [HUDManager showWarningWithText:@"请输入正确手机号"];
            [_phoneTextfield becomeFirstResponder];
            self.loginBtn.userInteractionEnabled=YES;
            return NO;
        }
    }else{
        if (![account hasPrefix:@"super"]) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请输入正确用户名" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
//            [alertView show];
            [HUDManager showWarningWithText:@"请输入正确用户名"];
            [_phoneTextfield becomeFirstResponder];
            self.loginBtn.userInteractionEnabled=YES;
            return NO;
        }
    }
    if (self.loginSeg.selectedSegmentIndex == 0) {
        if ([password length] < 6 || [password length] > 18) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请输入正确密码" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
//            [alertView show];
            [HUDManager showWarningWithText:@"请输入正确密码"];
            [_passwordTextfield becomeFirstResponder];
            self.loginBtn.userInteractionEnabled=YES;
            return NO;
        }
    }else{
        if (password.length!=6) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"验证码格式不正确" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
//            [alertView show];
            [HUDManager showWarningWithText:@"验证码格式不正确"];
            self.loginBtn.userInteractionEnabled=YES;
        }
    }
    return YES;
}

- (void)getUserInfoForPlatform
{
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary]valueForKey:UMShareToWechatSession];
            self.access_token = snsAccount.accessToken;
            self.openID = snsAccount.openId;
            [HUDManager showLoadingHUDView:self.view withText:@"正在登录"];
            [self getToken];

        }
    });
}
- (void)getToken{
    __block  NSString *security=@"";
    //获取AES加密密钥
    NSString *uuid= [[NSUUID UUID] UUIDString];
    //减去"-"
    NSString *tokenStr = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSDictionary *params=@{@"token":tokenStr};
    [SLAPIHelper getAESSecurity:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        security = data[@"result"];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSString *accessToken = [AESCrypt encrypt:self.access_token password:security];
        NSString *openId = [AESCrypt encrypt:self.openID password:security];
        if ([GeTuiSdk clientId]) {
            [dict setObject:[GeTuiSdk clientId] forKey:@"cid"];
        }
        [dict setObject:@2 forKey:@"os_platform"];
        [dict setObject:tokenStr forKey:@"token"];
        [dict setObject:accessToken forKey:@"access_token"];
        [dict setObject:openId forKey:@"openid"];
        [self loginUsingWechat:dict security:security];
        
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        if (failure.httpStatusCode == 500) {
            [HUDManager showWarningWithText:@"未知错误"];
        }else{
            [HUDManager showWarningWithText:@"登录失败,请重试"];
        }
    }];
}
- (void)loginUsingWechat:(NSDictionary *)dict security:(NSString *)security{
    [SLAPIHelper loginUsingWeChat:dict success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        NSLog(@"data%@",data);
        [HUDManager hideHUDView];
        NSNumber *action = data[@"result"][@"action"];
        if ([action isEqual:@3]) {
            SLWeChatRegisterViewController *vc = [[SLWeChatRegisterViewController alloc] init];
            vc.openID = self.openID;
            [self.navigationController pushViewController:vc animated:YES];
        }if ([action isEqual:@1]) {
            ApplicationDelegate.isLogin = YES;
//
//                //如果登录成功之后保存手机号码
            NSUserDefaults *phoneNumber = [NSUserDefaults standardUserDefaults];
            [phoneNumber setObject:data[@"result"][@"cellphone"] forKey:@"phoneNumber"];
            [phoneNumber synchronize];
            ApplicationDelegate.userId = data[@"result"][@"id"];
            NSString *nameAndPwd  = [NSString stringWithFormat:@"%@:%@",data[@"result"][@"id"], data[@"result"][@"password"]];
            ApplicationDelegate.Basic = [nameAndPwd base64EncodedString];
            ApplicationDelegate.userInformation = [data[@"result"] mutableCopy];
            
            NSString *im_password=data[@"result"][@"im_password"];
            NSString * decryptPassword = [AESCrypt decrypt:im_password password:security];
            
            [self saveBasic:ApplicationDelegate.Basic userInformation:ApplicationDelegate.userInformation password:decryptPassword];
            
            [SLIMCommon Login:data[@"result"][@"cellphone"] password:decryptPassword];
            [HUDManager hideHUDView];
            [HUDManager showWarningWithText:@"登录成功"];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"isLoginSuccessToLoadData" object:self];
            //        if (self.isTabbarVC) {
            //            [[NSNotificationCenter defaultCenter] postNotificationName:@"isLoginSuccessChangeTabbarVC" object:self];
            //        }
            //        if (self.isReplacePasswordVC){
            //            [[NSNotificationCenter defaultCenter] postNotificationName:@"isLoginSuccessChangeTabbarVC" object:self];
            //        }
            SLTabBarViewController *indexVC = [[SLTabBarViewController alloc] init];
            [UIApplication sharedApplication].keyWindow.rootViewController = indexVC;
        }
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        [HUDManager showWarningWithText:@"登录失败"];
    }];
}
- (void)endEdit{
    [self.view endEditing:YES];
}
- (void)SLGoToRegisterView:(SLGoToRegisterView *)goToRegisterView didClickBtnAtIndex:(NSInteger)index{
    if (index == 1) {
        [self userAgreementBtnClick];
    }else if (index == 2){
        [self userSecrecyBtnClick];
    }else if (index == 3){
        [self.goToRegisterView removeFromSuperview];
    }else if (index == 4){
        NSString *account = [self.phoneTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *password = [self.verificationTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if([self validateAccount:account password:password]) {
            if (self.hasNetWork) {
                [self loginPassWord:account password:password];
            }else{
                self.loginBtn.userInteractionEnabled = YES;
                [HUDManager showWarningWithText:@"网络异常，请检查网络"];
            }
            
        }
    }
}
- (void)userAgreementBtnClick{
    SLWebViewController *vc=[[SLWebViewController alloc] init];
    vc.url=@"http://m.superloop.com.cn/agreement.html";
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)userSecrecyBtnClick {
    SLWebViewController *vc=[[SLWebViewController alloc] init];
    vc.url=@"http://m.superloop.com.cn/privacy.html";
    [self.navigationController pushViewController:vc animated:YES];
}


@end
