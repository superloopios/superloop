//
//  SLForgetPassWordViewController.m
//  Superloop
//
//  Created by administrator on 16/7/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLForgetPassWordViewController.h"
#import <MBProgressHUD.h>
#import "SLAPIHelper.h"
#import "SLReplacePasswordViewController.h"
#import "NSString+Utils.h"
#import "Utils.h"
#import "AESCrypt.h"
#import "AppDelegate.h"
#import "SLLoginTextField.h"
#import "SLAPIHelper.h"

@interface SLForgetPassWordViewController ()<UITextFieldDelegate>{
    MBProgressHUD  *progressHUD;
}

@property (nonatomic, assign) BOOL                          hasNetWork;

@property (nonatomic, assign) BOOL                          isAlreadyGetVerifyCode;

@property (nonatomic, strong) SLLoginTextField              *phoneTextfield;
@property (nonatomic, strong) SLLoginTextField              *verificationTextfield;
@property (nonatomic, strong) UIButton                      *getVerificationBtn;
@property (nonatomic, strong) UIButton                      *verificationBtn;

@end

@implementation SLForgetPassWordViewController

- (UIButton *)verificationBtn{
    if (!_verificationBtn) {
        _verificationBtn = [[UIButton alloc] init];
        [_verificationBtn setTitle:@"确定" forState:UIControlStateNormal];
        [_verificationBtn setBackgroundColor:SLMainColor];
        [_verificationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _verificationBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        _verificationBtn.frame = CGRectMake(20, 220, screen_W-40, 38);
        _verificationBtn.layer.cornerRadius = 5;
        [self.view addSubview:_verificationBtn];
    }
    return _verificationBtn;
}

- (UIButton *)getVerificationBtn{
    if (!_getVerificationBtn) {
        _getVerificationBtn = [[UIButton alloc] init];
        [_getVerificationBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_getVerificationBtn setBackgroundColor:SLMainColor];
        [_getVerificationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _getVerificationBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _getVerificationBtn.frame = CGRectMake(screen_W-20-80, 150, 80, 38);
        _getVerificationBtn.layer.cornerRadius = 5;
        [self.view addSubview:_getVerificationBtn];
    }
    return _getVerificationBtn;
}

- (SLLoginTextField *)verificationTextfield{
    if (!_verificationTextfield) {
        _verificationTextfield = [[SLLoginTextField alloc] init];
        _verificationTextfield.frame = CGRectMake(20, 150, screen_W-130, 40);
        _verificationTextfield.delegate = self;
        [self.view addSubview:_verificationTextfield];
    }
    return _verificationTextfield;
}

- (SLLoginTextField *)phoneTextfield{
    if (!_phoneTextfield) {
        _phoneTextfield = [[SLLoginTextField alloc] init];
        _phoneTextfield.frame = CGRectMake(20, 90, screen_W-40, 40);
        _phoneTextfield.delegate = self;
        [self.view addSubview:_phoneTextfield];
    }
    return _phoneTextfield;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLForgetPassWordViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLForgetPassWordViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
    [self setUpNav];
    self.view.backgroundColor=[UIColor whiteColor];
    //判断有无网络处理
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"重置密码";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=SLMainColor;
    [navView addSubview:nameLab];
    
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setUpUI{
    self.phoneTextfield.placeholder = @"手机号";
    self.verificationTextfield.placeholder = @"验证码";
    [self.getVerificationBtn addTarget:self action:@selector(getVericationNumber) forControlEvents:UIControlEventTouchUpInside];
    [self.verificationBtn addTarget:self action:@selector(replacePasswordNext) forControlEvents:UIControlEventTouchUpInside];
}

- (void)startTimer
{
    __block int timeout = 60; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0); //每秒执行
    __weak typeof(self) weakSelf = self;
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout <= 0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [weakSelf.getVerificationBtn setTitle:@"重新获取" forState:UIControlStateNormal];
                weakSelf.getVerificationBtn.userInteractionEnabled = YES;
            });
        }else{
            NSString *strTime = [NSString stringWithFormat:@"%d秒", timeout];
            dispatch_async(dispatch_get_main_queue(), ^{
                //        设置界面的按钮显示 根据自己需求设置
                [weakSelf.getVerificationBtn setTitle:strTime forState:UIControlStateNormal];
                weakSelf.getVerificationBtn.userInteractionEnabled = NO;
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}


- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetWork=NO;
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            self.hasNetWork=YES;
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.hasNetWork=YES;
        }
    }
}

- (void)replacePasswordNext {

    if (!self.hasNetWork) {
        [HUDManager showWarningWithText:@"网络出错,请检查网络状况"];
    }else{
        if ([NSString isEmptyStrings:self.phoneTextfield.text]) {
            [HUDManager showWarningWithText:@"请输入手机号"];
            return;
        }
        if (![Utils isValidMobileNumber:self.phoneTextfield.text]) {
            [HUDManager showWarningWithText:@"请输入合法的手机号"];
            return;
        }
        if (!self.isAlreadyGetVerifyCode) {
            [HUDManager showWarningWithText:@"请获取验证码在进行下一步"];
            return;
        }
        if (!([self.verificationTextfield.text length] > 0 && [self.verificationTextfield.text length] == 6)) {
            [HUDManager showWarningWithText:@"请输入有效的验证码"];
            return;
        }else{
            self.verificationBtn.userInteractionEnabled = NO;
            
            [HUDManager showLoadingHUDView:self.view withText:@""];
            
            //对应的加密密钥
            __block  NSString *security=@"";
            //获取AES加密密钥
            NSString *uuid= [[NSUUID UUID] UUIDString];
            //减去"-"
            NSString *tokenStr = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
            NSDictionary *params=@{@"token":tokenStr};
            
            [SLAPIHelper getAESSecurity:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
                security = data[@"result"];
                NSString *account = [self.phoneTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                NSString *code = [self.verificationTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                NSString *encryptCellphone = [AESCrypt encrypt:account password:security];
                NSString *encryptCode = [AESCrypt encrypt:code password:security];
                
                NSDictionary *params = @{@"cellphone":encryptCellphone,@"code":encryptCode,@"token":tokenStr};
                [self checkSMSCode:params];
                
            } failure:^(SLHttpRequestError *failure) {
                [HUDManager hideHUDView];
                self.verificationBtn.userInteractionEnabled = YES;
                
            }];

        }
    }
}

/**
 错误：15
 过期：16
 Limit：10
 */
- (void)checkSMSCode:(NSDictionary *)dict{
    [SLAPIHelper verificationCodeIsValid:dict success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        [HUDManager hideHUDView];
        self.verificationBtn.userInteractionEnabled = YES;
        SLReplacePasswordViewController *replacePassword = [[SLReplacePasswordViewController alloc] init];
        replacePassword.cellphoneNumber = self.phoneTextfield.text;
        replacePassword.code = self.verificationTextfield.text;
        [self.navigationController pushViewController:replacePassword animated:YES];
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        
        self.verificationBtn.userInteractionEnabled = YES;
        if (failure.slAPICode == 15) {
            [HUDManager showWarningWithText:@"短信验证码错误"];
        }else if (failure.slAPICode == 16) {
            [HUDManager showWarningWithText:@"短信验证码已过期，请重新获取"];
        }else if (failure.slAPICode == 10) {
            [HUDManager showWarningWithText:@"已达到错误次数上限\n请重新获取验证码"];
        }
    }];
}

#pragma mark - 需要确定在有网络的状态下才可以发送网路请求,获取手机验证码
#pragma mark ---- 处理防止多次点击按钮的事件


- (void)getVericationNumber{
    if (!self.hasNetWork) {
        [HUDManager showWarningWithText:@"网络出错,请检查网络状况"];
    }else{
        //简单的判断手机格式
        if (![Utils isValidMobileNumber:self.phoneTextfield.text]) {
            [HUDManager showWarningWithText:@"请输入正确的手机号码"];
            return;
        }
        
        self.getVerificationBtn.userInteractionEnabled = NO;
        
        [HUDManager showLoadingHUDView:self.view withText:@""];
        NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
        //对应的加密密钥
        __block  NSString *security=@"";
        //获取AES加密密钥
        NSString *uuid= [[NSUUID UUID] UUIDString];
        //减去"-"
        NSString *tokenStr = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSDictionary *params=@{@"token":tokenStr};
        [SLAPIHelper getAESSecurity:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            security = data[@"result"];
            NSString *encryptCellphone = [AESCrypt encrypt:self.phoneTextfield.text password:security];
            [parameter setObject:encryptCellphone forKey:@"cellphone"];
            [parameter setObject:tokenStr forKey:@"token"];
            [self encryptSuccess:parameter];
        } failure:^(SLHttpRequestError *failure) {
            [HUDManager hideHUDView];
            self.getVerificationBtn.userInteractionEnabled = YES;
            [HUDManager showWarningWithText:@"获取验证码失败"];
        }];
    }
}

//加密成功后获取验证码
-(void)encryptSuccess:(NSMutableDictionary *)parameter {
    //    __weak typeof(self) weakSelf = self;
    [SLAPIHelper getSMSCode:parameter success:^(NSURLSessionDataTask *task,  NSDictionary* data) {
        [HUDManager hideHUDView];
        [self startTimer];
        NSLog(@"data%@",data);
        self.isAlreadyGetVerifyCode = YES;
    } failure:^(SLHttpRequestError *failure) {
        self.getVerificationBtn.userInteractionEnabled = YES;
        [HUDManager hideHUDView];
        self.isAlreadyGetVerifyCode = NO;
        if (failure.httpStatusCode==429&&failure.slAPICode==10) {
            [HUDManager showWarningWithText:@"操作频繁,请稍后重试"];
        }else{
            [HUDManager showWarningWithText:@"获取验证码失败"];
        }
    }];
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//    [self setSpace:textField withValue:textField.text withFont:[UIFont systemFontOfSize:16]];
//    return YES;
//}
////设置行间距和字间距
//- (void)setSpace:(UITextField *)textField withValue:(NSString*)str withFont:(UIFont*)font {
//    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
//    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
//    paraStyle.alignment = NSTextAlignmentLeft;
//    //设置字间距 NSKernAttributeName:@1.5f
//    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f};
//    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
//    textField.attributedText = attributeStr;
//}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
