//
//  SLWeChatRegisterViewController.h
//  Superloop
//
//  Created by xiaowu on 16/11/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLWeChatRegisterViewController : UIViewController

@property (nonatomic,   copy) NSString                      *openID;

@end
