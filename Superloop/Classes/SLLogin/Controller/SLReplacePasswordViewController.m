//
//  SLReplacePasswordViewController.m
//  Superloop
//
//  Created by administrator on 16/7/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLReplacePasswordViewController.h"
#import "AppDelegate.h"
#import "SLMessageManger.h"
#import "SLAPIHelper.h"
#import "AESCrypt.h"
#import "SLLoginTextField.h"
#import "NSString+Utils.h"
#import "SLFirstViewController.h"
#import "SLNavgationViewController.h"

@interface SLReplacePasswordViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) SLLoginTextField              *pwdTextfield;
@property (nonatomic, strong) SLLoginTextField              *ensurePwdTextfield;
@property (nonatomic, strong) UIButton                      *changeBtn;
@property (nonatomic, assign) BOOL                          hasNetWork;

@end

@implementation SLReplacePasswordViewController

- (UIButton *)changeBtn{
    if (!_changeBtn) {
        _changeBtn = [[UIButton alloc] init];
        [_changeBtn setTitle:@"确定" forState:UIControlStateNormal];
        [_changeBtn setBackgroundColor:SLMainColor];
        [_changeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _changeBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        _changeBtn.frame = CGRectMake(20, 220, screen_W-40, 38);
        _changeBtn.layer.cornerRadius = 5;
        [self.view addSubview:_changeBtn];
    }
    return _changeBtn;
}

- (SLLoginTextField *)pwdTextfield{
    if (!_pwdTextfield) {
        _pwdTextfield = [[SLLoginTextField alloc] init];
        _pwdTextfield.frame = CGRectMake(20, 90, screen_W-40, 40);
        _pwdTextfield.delegate = self;
        _pwdTextfield.secureTextEntry = YES;
        [self.view addSubview:_pwdTextfield];
    }
    return _pwdTextfield;
}
- (SLLoginTextField *)ensurePwdTextfield{
    if (!_ensurePwdTextfield) {
        _ensurePwdTextfield = [[SLLoginTextField alloc] init];
        _ensurePwdTextfield.frame = CGRectMake(20, 150, screen_W-40, 40);
        _ensurePwdTextfield.delegate = self;
        _ensurePwdTextfield.secureTextEntry = YES;
        [self.view addSubview:_ensurePwdTextfield];
    }
    return _ensurePwdTextfield;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUpNav];
    [self setupUI];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetWork=NO;
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            self.hasNetWork=YES;
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.hasNetWork=YES;
        }
    }
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"重置密码";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor = SLMainColor;
    [navView addSubview:nameLab];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setupUI{
   
    self.pwdTextfield.placeholder = @"输入新密码";
    self.ensurePwdTextfield.placeholder = @"确认新密码";
    [self.changeBtn addTarget:self action:@selector(todoRePassword) forControlEvents:UIControlEventTouchUpInside];
//    [self.fixView.darkToBride addTarget:self action:@selector(darkToBride:) forControlEvents:UIControlEventTouchUpInside];
//    [self.fixView.darkToBride2 addTarget:self action:@selector(darkToBride2:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark --- 需要在修改密码成功之后去直接进入登录成功之后的界面


- (void)todoRePassword{
    if ([NSString isEmptyStrings:self.pwdTextfield.text]) {
        //确认密码为空
        [self.pwdTextfield becomeFirstResponder];
        kShowToast(@"请输入重置密码");
        return;
    }
    if ([NSString isEmptyStrings:self.ensurePwdTextfield.text]) {
        //确认密码为空
        [self.ensurePwdTextfield becomeFirstResponder];
        kShowToast(@"请确认密码");
        return;
    }
    if (![self.pwdTextfield.text isEqualToString:self.ensurePwdTextfield.text]) {
        kShowToast(@"两次密码不一致");
        return;
    }
    
    if ([self.pwdTextfield.text length] < 6 || [self.pwdTextfield.text length] > 18 || [self.ensurePwdTextfield.text length] < 6 || [self.ensurePwdTextfield.text length] > 18) {
        kShowToast(@"请输入长度为6至18位密码");
        return;
    }
    if (!self.hasNetWork) {
        kShowToast(@"网络异常，请检查网络设置");
        return;
    }
    self.changeBtn.userInteractionEnabled = NO;
        //对应的加密密钥
    __block  NSString *security=@"";
    //获取AES加密密钥
    NSString *uuid= [[NSUUID UUID] UUIDString];
    //减去"-"
    NSString *tokenStr = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    [HUDManager showLoadingHUDView:self.view withText:@""];
    NSDictionary *params=@{@"token":tokenStr};
    [SLAPIHelper getAESSecurity:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        security = data[@"result"];
        
            NSString *encryptCellphone = [AESCrypt encrypt:self.cellphoneNumber password:security];
            NSString *encryptPassword = [AESCrypt encrypt:self.pwdTextfield.text password:security];
            NSString *encryptCode = [AESCrypt encrypt:self.code password:security];
            NSDictionary *parameter = @{@"code":encryptCode,@"cellphone":encryptCellphone,@"password":encryptPassword,@"token":tokenStr};
            [self encryptSuccessResetPwd:parameter];
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        self.changeBtn.userInteractionEnabled = YES;
        [HUDManager showWarningWithText:@"密码修改失败"];
    }];
    
}

//加密成功后重置密码
-(void)encryptSuccessResetPwd:(NSDictionary *)parameter{
    NSLog(@"---parameter--------%@",parameter);
    // 重置密码
    [SLAPIHelper resetPwd:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        kShowToast(@"密码修改成功");
        [HUDManager hideHUDView];

        [SLUserDefault removeUserInfo];
        ApplicationDelegate.Basic = NULL;
        ApplicationDelegate.userInformation = NULL;
        ApplicationDelegate.userId = NULL;
        ApplicationDelegate.isLogin = NO;
        ApplicationDelegate.titleStr=nil;
        ApplicationDelegate.contentStr=nil;
        ApplicationDelegate.publishPhotosArr=nil;
        [[SLMessageManger sharedInstance] logout];
        [self clearAllUserDefaultsData];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ClearMessage" object:self];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            SLFirstViewController *indexVC = [[SLFirstViewController alloc] init];
            SLNavgationViewController *nav = [[SLNavgationViewController alloc] initWithRootViewController:indexVC];
            [UIApplication sharedApplication].keyWindow.rootViewController = nav;
        });
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        self.changeBtn.userInteractionEnabled = YES;
        if (failure.slAPICode != 9) {
            [HUDManager showWarningWithText:@"密码修改失败"];
        }
        
    }];
}

-(void)viewDidDisappear:(BOOL)animated{
    [HUDManager hideHUDView];
}
- (void)clearAllUserDefaultsData{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSDictionary* dict = [defs dictionaryRepresentation];
    for(id key in dict) {
        if (![key isEqualToString:appIsFirst]&& ![key isEqualToString:SLNetWorkStatus] && ![key isEqualToString:strAccountName] &&![key isEqualToString:strAccout]&&![key isEqualToString:@"VoiceType"]&&![key isEqualToString:location]&&![key isEqualToString:@"guideSave"]&&![key isEqualToString:ContactStatus]&&![key isEqualToString:LoginStutas]&&![key isEqualToString:AllowAddContactStatus]) {
            [defs removeObjectForKey:key];
        }
    }
    [defs synchronize];
}

- (void)darkToBride:(id)sender {
//    self.fixView.darkToBride.selected = !self.fixView.darkToBride.selected;
//    self.fixView.reNewPassword.secureTextEntry = !self.fixView.darkToBride.selected;
}
- (void)darkToBride2:(id)sender {
//    self.fixView.darkToBride2.selected = !self.fixView.darkToBride2.selected;
//    self.fixView.reNewPassword2.secureTextEntry = !self.fixView.darkToBride2.selected;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    [self setSpace:textField withValue:textField.text withFont:[UIFont systemFontOfSize:16]];
    return YES;
}
//设置行间距和字间距
- (void)setSpace:(UITextField *)textField withValue:(NSString*)str withFont:(UIFont*)font {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f};
    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
    textField.attributedText = attributeStr;
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
