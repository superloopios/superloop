//
//  SLRegisterViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/3.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLRegisterViewController.h"
#import "SLLoginBtnViewController.h"
#import <AFNetworking.h>
#import "AppDelegate.h"
#import <MBProgressHUD.h>
#import "SLWebView.h"
#import "SLJavascriptBridgeEvent.h"
#import "SLMessageManger.h"
#import "SPUtil.h"
#import "AESCrypt.h"
#import "SLAPIHelper.h"
#import "SLMoreCategoryViewController.h"
#import "SLNavgationViewController.h"
#import "SLLocationViewController.h"
#import "SLOpenIMCommon.h"
#import "SLIMCommon.h"
#import "SLWebViewController.h"
#import "NSString+Utils.h"
#import <Bugly/Bugly.h>

@interface SLRegisterViewController ()<UITextFieldDelegate,UIScrollViewDelegate,UIAlertViewDelegate>
{
    NSInteger _loginIMCount; //登录IM执行的次数
    NSMutableArray *tfArr;
    
}
//手机号
@property (strong, nonatomic)UILabel *cellPhoneLabel;
@property (strong, nonatomic)UITextField *cellPhoneNumberTextField;
@property (strong, nonatomic)UIView *cellPhoneSeparatorView;
//验证码
@property (strong, nonatomic)UILabel *testNumberLabel;
@property (strong, nonatomic)UITextField *testNumberTextField;
@property (strong, nonatomic)UIView *testNumberSeparatorView;
@property (strong, nonatomic) UIButton *testNumberBtn;
//所在地
@property (strong, nonatomic) UILabel *cityLocationLab;
@property (strong, nonatomic)UILabel *cityLabel;
@property (strong, nonatomic)UIView *citySeparatorView;
@property (strong, nonatomic) UIButton *cityBtn;
//昵称
@property (strong, nonatomic)UITextField *nickNameTextField;
@property (strong, nonatomic)UILabel *nickNameLabel;
@property (strong, nonatomic)UIView *nickNameSeparatorView;
//密码
@property (strong, nonatomic)UILabel *passWordLabel;
@property (strong, nonatomic)UIView *passWordSeparatorView;
@property (strong, nonatomic)UITextField *passwordTextField;
@property (strong, nonatomic)UIButton *darkToBride;

//邀请码
@property (strong, nonatomic)UILabel *invatationLabel;
@property (strong, nonatomic)UIView *invatationSeparatorView;
@property (strong, nonatomic)UITextField *invatationTextField;

@property (strong, nonatomic)UIButton *registerBtn;

@property (strong, nonatomic) UIButton *agreeBtn;

@property (nonatomic, strong)UIViewController *agreementView;
@property (nonatomic, strong) UIView *navView;
@property (nonatomic,strong)UIImageView *cutImageView;
@property (nonatomic,strong)UILabel  *label;

@property (strong, nonatomic) UILabel *agreementLab;

@property (strong, nonatomic)UIButton *userAgreementBtn;
@property (strong, nonatomic) UIButton *userSecrecy;
@property (strong, nonatomic) UILabel *andLab;

@property (nonatomic,assign)BOOL isNetWorkStatus;   //获取网络状态
@property (nonatomic,copy)NSString *cityId;
//底部scrollview
@property (strong, nonatomic)UIScrollView *baseSc;

@end

@implementation SLRegisterViewController

- (UIScrollView *)baseSc{
    if (!_baseSc) {
        _baseSc = [[UIScrollView alloc] init];
        _baseSc.delegate = self;
        _baseSc.backgroundColor = [UIColor whiteColor];
        _baseSc.frame = CGRectMake(0, 64, screen_W, screen_H-64);
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEdit)];
        [_baseSc addGestureRecognizer:ges];
        [self.view addSubview:_baseSc];
    }
    return _baseSc;
}
- (UILabel *)cellPhoneLabel{
    if (!_cellPhoneLabel) {
        _cellPhoneLabel = [[UILabel alloc] init];
        _cellPhoneLabel.font = [UIFont systemFontOfSize:15];
        _cellPhoneLabel.textColor = [UIColor blackColor];
        _cellPhoneLabel.text = @"手机号";
//        _cellPhoneLabel.backgroundColor = [UIColor redColor];
        [self.baseSc addSubview:_cellPhoneLabel];
        [_cellPhoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(17);
            make.top.equalTo(self.baseSc).offset(30);
            make.width.equalTo(@(50));
        }];
    }
    return _cellPhoneLabel;
}
- (UITextField *)cellPhoneNumberTextField{
    if (!_cellPhoneNumberTextField) {
        _cellPhoneNumberTextField = [[UITextField alloc] init];
        _cellPhoneNumberTextField.borderStyle = UITextBorderStyleNone;
        _cellPhoneNumberTextField.font = [UIFont systemFontOfSize:15];
        _cellPhoneNumberTextField.textColor = [UIColor blackColor];
//        _cellPhoneNumberTextField.backgroundColor = [UIColor grayColor];
        _cellPhoneNumberTextField.keyboardType = UIKeyboardTypeNumberPad;
        [self.baseSc addSubview:_cellPhoneNumberTextField];
        [_cellPhoneNumberTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.cellPhoneLabel.mas_right).offset(10);
            make.right.equalTo(self.view.mas_right).offset(-10);
            make.height.equalTo(@(30));
            make.centerY.equalTo(self.cellPhoneLabel.mas_centerY);
        }];
        [tfArr addObject:_cellPhoneNumberTextField];
    }
    return _cellPhoneNumberTextField;
}
- (UIView *)cellPhoneSeparatorView{
    if (!_cellPhoneSeparatorView) {
        _cellPhoneSeparatorView = [[UIView alloc] init];
        _cellPhoneSeparatorView.backgroundColor = [UIColor lightGrayColor];
        [self.baseSc addSubview:_cellPhoneSeparatorView];
        [_cellPhoneSeparatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.cellPhoneNumberTextField);
            make.right.equalTo(self.view).offset(-17);
            make.top.equalTo(self.cellPhoneNumberTextField.mas_bottom).offset(3);
            make.height.equalTo(@0.5);
        }];
    }
    return _cellPhoneSeparatorView;
}

- (UILabel *)testNumberLabel{
    if (!_testNumberLabel) {
        _testNumberLabel = [[UILabel alloc] init];
        _testNumberLabel.font = [UIFont systemFontOfSize:15];
        _testNumberLabel.textColor = [UIColor blackColor];
        _testNumberLabel.text = @"验证码";
        //        _cellPhoneLabel.backgroundColor = [UIColor redColor];
        [self.baseSc addSubview:_testNumberLabel];
        [_testNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(17);
            make.top.equalTo(self.cellPhoneLabel.mas_bottom).offset(30);
            make.width.equalTo(@(50));
        }];
    }
    return _testNumberLabel;
}
- (UITextField *)testNumberTextField{
    if (!_testNumberTextField) {
        _testNumberTextField = [[UITextField alloc] init];
        _testNumberTextField.borderStyle = UITextBorderStyleNone;
        _testNumberTextField.font = [UIFont systemFontOfSize:15];
        _testNumberTextField.textColor = [UIColor blackColor];
//        _testNumberTextField.backgroundColor = [UIColor grayColor];
        _testNumberTextField.keyboardType = UIKeyboardTypeNumberPad;
        [self.baseSc addSubview:_testNumberTextField];
        [_testNumberTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.testNumberLabel.mas_right).offset(10);
            make.right.equalTo(self.view.mas_right).offset(-100);
            make.height.equalTo(@(30));
            make.centerY.equalTo(self.testNumberLabel.mas_centerY);
        }];
        [tfArr addObject:_testNumberTextField];
    }
    return _testNumberTextField;
}

- (UIView *)testNumberSeparatorView{
    if (!_testNumberSeparatorView) {
        _testNumberSeparatorView = [[UIView alloc] init];
        _testNumberSeparatorView.backgroundColor = [UIColor lightGrayColor];
        [self.baseSc addSubview:_testNumberSeparatorView];
        [_testNumberSeparatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.testNumberTextField);
            make.right.equalTo(self.view).offset(-17);
            make.top.equalTo(self.testNumberTextField.mas_bottom).offset(3);
            make.height.equalTo(@0.5);
        }];
    }
    return _testNumberSeparatorView;
}
- (UIButton *)testNumberBtn{
    if (!_testNumberBtn) {
        _testNumberBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_testNumberBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        _testNumberBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_testNumberBtn setTitleColor:SLColor(19, 120, 215) forState:UIControlStateNormal];
        [self.baseSc addSubview:_testNumberBtn];
        _testNumberBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
//        _testNumberBtn.backgroundColor = [UIColor redColor];
        [_testNumberBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.view).offset(-17);
            make.left.equalTo(self.testNumberTextField.mas_right).offset(0);
            make.top.equalTo(self.cellPhoneSeparatorView.mas_bottom).offset(0.5);
            make.bottom.equalTo(self.testNumberSeparatorView.mas_top).offset(-0.5);
        }];
    }
    return _testNumberBtn;
}

- (UILabel *)cityLabel{
    if (!_cityLabel) {
        _cityLabel = [[UILabel alloc] init];
        _cityLabel.font = [UIFont systemFontOfSize:15];
        _cityLabel.textColor = [UIColor blackColor];
        _cityLabel.text = @"所在地";
        //        _cellPhoneLabel.backgroundColor = [UIColor redColor];
        [self.baseSc addSubview:_cityLabel];
        [_cityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(17);
            make.top.equalTo(self.testNumberLabel.mas_bottom).offset(30);
            make.width.equalTo(@(50));
        }];
    }
    return _cityLabel;
}

- (UILabel *)cityLocationLab{
    if (!_cityLocationLab) {
        _cityLocationLab = [[UILabel alloc] init];
        _cityLocationLab.font = [UIFont systemFontOfSize:15];
        _cityLocationLab.textColor = [UIColor blackColor];
        _cityLabel.textAlignment = NSTextAlignmentRight;
        //        _testNumberTextField.backgroundColor = [UIColor grayColor];
        
        [self.baseSc addSubview:_cityLocationLab];
        [_cityLocationLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.cityLabel.mas_right).offset(10);
            make.right.equalTo(self.view.mas_right).offset(-50);
            make.height.equalTo(@(30));
            make.centerY.equalTo(self.cityLabel.mas_centerY);
        }];
    }
    return _cityLocationLab;
}

- (UIView *)citySeparatorView{
    if (!_citySeparatorView) {
        _citySeparatorView = [[UIView alloc] init];
        _citySeparatorView.backgroundColor = [UIColor lightGrayColor];
        [self.baseSc addSubview:_citySeparatorView];
        [_citySeparatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.cellPhoneNumberTextField);
            make.right.equalTo(self.view).offset(-17);
            make.top.equalTo(self.cityLocationLab.mas_bottom).offset(3);
            make.height.equalTo(@0.5);
        }];
    }
    return _citySeparatorView;
}
- (UIButton *)cityBtn{
    if (!_cityBtn) {
        _cityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cityBtn setImage:[UIImage imageNamed:@"right-Arrow-outline"] forState:UIControlStateNormal];
        [self.baseSc addSubview:_cityBtn];
        [_cityBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.view).offset(-17);
            make.left.equalTo(self.cityLocationLab.mas_right).offset(0);
            make.height.equalTo(self.cityLocationLab);
            make.centerY.equalTo(self.cityLocationLab.mas_centerY);
        }];
    }
    return _cityBtn;
}

- (UILabel *)nickNameLabel{
    if (!_nickNameLabel) {
        _nickNameLabel = [[UILabel alloc] init];
        _nickNameLabel.font = [UIFont systemFontOfSize:15];
        _nickNameLabel.textColor = [UIColor blackColor];
        _nickNameLabel.text = @"昵称";
        //        _cellPhoneLabel.backgroundColor = [UIColor redColor];
        [self.baseSc addSubview:_nickNameLabel];
        [_nickNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(17);
            make.top.equalTo(self.cityLabel.mas_bottom).offset(30);
            make.width.equalTo(@(50));
        }];
    }
    return _nickNameLabel;
}
- (UITextField *)nickNameTextField{
    if (!_nickNameTextField) {
        _nickNameTextField = [[UITextField alloc] init];
        _nickNameTextField.borderStyle = UITextBorderStyleNone;
        _nickNameTextField.font = [UIFont systemFontOfSize:15];
        _nickNameTextField.textColor = [UIColor blackColor];
        //        _cellPhoneNumberTextField.backgroundColor = [UIColor grayColor];
        
        [self.baseSc addSubview:_nickNameTextField];
        [_nickNameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nickNameLabel.mas_right).offset(10);
            make.right.equalTo(self.view.mas_right).offset(-10);
            make.height.equalTo(@(30));
            make.centerY.equalTo(self.nickNameLabel.mas_centerY);
        }];
        [tfArr addObject:_nickNameTextField];
    }
    return _nickNameTextField;
}
- (UIView *)nickNameSeparatorView{
    if (!_nickNameSeparatorView) {
        _nickNameSeparatorView = [[UIView alloc] init];
        _nickNameSeparatorView.backgroundColor = [UIColor lightGrayColor];
        [self.baseSc addSubview:_nickNameSeparatorView];
        [_nickNameSeparatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.cellPhoneNumberTextField);
            make.right.equalTo(self.view).offset(-17);
            make.top.equalTo(self.nickNameTextField.mas_bottom).offset(3);
            make.height.equalTo(@0.5);
        }];
    }
    return _nickNameSeparatorView;
}

- (UILabel *)passWordLabel{
    if (!_passWordLabel) {
        _passWordLabel = [[UILabel alloc] init];
        _passWordLabel.font = [UIFont systemFontOfSize:15];
        _passWordLabel.textColor = [UIColor blackColor];
        _passWordLabel.text = @"密码";
        //        _cellPhoneLabel.backgroundColor = [UIColor redColor];
        [self.baseSc addSubview:_passWordLabel];
        [_passWordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(17);
            make.top.equalTo(self.nickNameLabel.mas_bottom).offset(30);
            make.width.equalTo(@(50));
        }];
    }
    return _passWordLabel;
}
- (UITextField *)passwordTextField{
    if (!_passwordTextField) {
        _passwordTextField = [[UITextField alloc] init];
        _passwordTextField.borderStyle = UITextBorderStyleNone;
        _passwordTextField.font = [UIFont systemFontOfSize:15];
        _passwordTextField.textColor = [UIColor blackColor];
        //        _testNumberTextField.backgroundColor = [UIColor grayColor];
        [self.baseSc addSubview:_passwordTextField];
        _passwordTextField.secureTextEntry = YES;
        [_passwordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.passWordLabel.mas_right).offset(10);
            make.right.equalTo(self.view.mas_right).offset(-50);
            make.height.equalTo(@(30));
            make.centerY.equalTo(self.passWordLabel.mas_centerY);
        }];
        [tfArr addObject:_passwordTextField];
    }
    return _passwordTextField;
}

- (UIView *)passWordSeparatorView{
    if (!_passWordSeparatorView) {
        _passWordSeparatorView = [[UIView alloc] init];
        _passWordSeparatorView.backgroundColor = [UIColor lightGrayColor];
        [self.baseSc addSubview:_passWordSeparatorView];
        [_passWordSeparatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.passwordTextField);
            make.right.equalTo(self.view).offset(-17);
            make.top.equalTo(self.passwordTextField.mas_bottom).offset(3);
            make.height.equalTo(@0.5);
        }];
    }
    return _passWordSeparatorView;
}

- (UIButton *)darkToBride{
    if (!_darkToBride) {
        _darkToBride = [UIButton buttonWithType:UIButtonTypeCustom];
        [_darkToBride setImage:[UIImage imageNamed:@"Eyeball"] forState:UIControlStateNormal];
        [_darkToBride setImage:[UIImage imageNamed:@"Eye-blocked-symbol"] forState:UIControlStateSelected];
        [self.baseSc addSubview:_darkToBride];
        _testNumberBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        //        _testNumberBtn.backgroundColor = [UIColor redColor];
        [_darkToBride mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.view).offset(-17);
            make.left.equalTo(self.passwordTextField.mas_right).offset(0);
            make.height.equalTo(@40);
            make.bottom.equalTo(self.passWordSeparatorView.mas_top).offset(-0.5);
        }];
    }
    return _darkToBride;
}

- (UILabel *)invatationLabel{
    if (!_invatationLabel) {
        _invatationLabel = [[UILabel alloc] init];
        _invatationLabel.font = [UIFont systemFontOfSize:15];
        _invatationLabel.textColor = [UIColor blackColor];
        _invatationLabel.text = @"邀请码";
        //        _cellPhoneLabel.backgroundColor = [UIColor redColor];
        [self.baseSc addSubview:_invatationLabel];
        [_invatationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(17);
            make.top.equalTo(self.passWordLabel.mas_bottom).offset(30);
            make.width.equalTo(@(55));
        }];
    }
    return _invatationLabel;
}
- (UITextField *)invatationTextField{
    if (!_invatationTextField) {
        _invatationTextField = [[UITextField alloc] init];
        _invatationTextField.borderStyle = UITextBorderStyleNone;
        _invatationTextField.font = [UIFont systemFontOfSize:15];
        _invatationTextField.textColor = [UIColor blackColor];
        _invatationTextField.placeholder = @"选填";
        //        _testNumberTextField.backgroundColor = [UIColor grayColor];
        [self.baseSc addSubview:_invatationTextField];
        [_invatationTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.invatationLabel.mas_right).offset(10);
            make.right.equalTo(self.view.mas_right).offset(-17);
            make.height.equalTo(@(30));
            make.centerY.equalTo(self.invatationLabel.mas_centerY);
        }];
        [tfArr addObject:_invatationTextField];
    }
    return _invatationTextField;
}
- (UIView *)invatationSeparatorView{
    if (!_invatationSeparatorView) {
        _invatationSeparatorView = [[UIView alloc] init];
        _invatationSeparatorView.backgroundColor = [UIColor lightGrayColor];
        [self.baseSc addSubview:_invatationSeparatorView];
        [_invatationSeparatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.passwordTextField);
            make.right.equalTo(self.view).offset(-17);
            make.top.equalTo(self.invatationTextField.mas_bottom).offset(3);
            make.height.equalTo(@0.5);
        }];
    }
    return _invatationSeparatorView;
}

- (UIButton *)registerBtn{
    if (!_registerBtn) {
        _registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_registerBtn setTitle:@"注册" forState:UIControlStateNormal];
        _registerBtn.backgroundColor = SLColor(234, 234, 234);
        _registerBtn.titleLabel.font = [UIFont systemFontOfSize:18];
        [_registerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.baseSc addSubview:_registerBtn];
        _registerBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        //        _testNumberBtn.backgroundColor = [UIColor redColor];
        [_registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@251);
            make.centerX.equalTo(self.baseSc.mas_centerX);
            make.top.equalTo(self.invatationSeparatorView.mas_bottom).offset(40);
            make.height.equalTo(@44);
        }];
    }
    return _registerBtn;
}
- (UIButton *)agreeBtn{
    if (!_agreeBtn) {
        _agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_agreeBtn setImage:[UIImage imageNamed:@"login_checkbox_selected_"] forState:UIControlStateSelected];
        [_agreeBtn setImage:[UIImage imageNamed:@"login_checkbox_disabled_"] forState:UIControlStateNormal];
//        _agreeBtn.backgroundColor = [UIColor redColor];
        _agreeBtn.imageView.contentMode = UIViewContentModeTop;
        [self.baseSc addSubview:_agreeBtn];
        [_agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.registerBtn).offset(-8);
            make.top.equalTo(self.registerBtn.mas_bottom).offset(5);
            make.height.width.equalTo(@30);
        }];
    }
    return _agreeBtn;
}
- (UILabel *)agreementLab{
    if (!_agreementLab) {
        _agreementLab = [[UILabel alloc] init];
        _agreementLab.font = [UIFont systemFontOfSize:12];
        _agreementLab.textColor = SLColor(81, 82, 82);
        _agreementLab.text = @"我已同意";
        //        _cellPhoneLabel.backgroundColor = [UIColor redColor];
        [self.baseSc addSubview:_agreementLab];
        [_agreementLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.agreeBtn.mas_right);
            make.centerY.equalTo(self.agreeBtn.mas_centerY);
        }];
    }
    return _agreementLab;
}
- (UIButton *)userAgreementBtn{
    if (!_userAgreementBtn) {
        _userAgreementBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_userAgreementBtn setTitleColor:SLColor(16, 128, 112) forState:UIControlStateNormal];
        [_userAgreementBtn setTitle:@"《超级圈用户协议》" forState:UIControlStateNormal];
        _userAgreementBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.baseSc addSubview:_userAgreementBtn];
        [_userAgreementBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.agreementLab.mas_right);
            make.centerY.equalTo(self.agreeBtn.mas_centerY);
        }];
    }
    return _userAgreementBtn;
}
- (UILabel *)andLab{
    if (!_andLab) {
        _andLab = [[UILabel alloc] init];
        _andLab.font = [UIFont systemFontOfSize:12];
        _andLab.textColor = SLColor(81, 82, 82);
        _andLab.text = @"和";
        //        _cellPhoneLabel.backgroundColor = [UIColor redColor];
        [self.baseSc addSubview:_andLab];
        [_andLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.userAgreementBtn.mas_right);
            make.centerY.equalTo(self.agreeBtn.mas_centerY);
        }];
    }
    return _andLab;
}
- (UIButton *)userSecrecy{
    if (!_userSecrecy) {
        _userSecrecy = [UIButton buttonWithType:UIButtonTypeCustom];
        [_userSecrecy setTitleColor:SLColor(16, 128, 112) forState:UIControlStateNormal];
        [_userSecrecy setTitle:@"《隐私条款》" forState:UIControlStateNormal];
        _userSecrecy.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.baseSc addSubview:_userSecrecy];
        [_userSecrecy mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.andLab.mas_right);
            make.centerY.equalTo(self.agreeBtn.mas_centerY);
        }];
    }
    return _userSecrecy;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    //    self.navigationController.navigationBarHidden=YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
   // [self.navigationController setNavigationBarHidden:NO animated:YES];
    //    self.navigationController.navigationBarHidden=NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    tfArr = [NSMutableArray array];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUpNav];
    _loginIMCount = 0;
    [self.cellPhoneNumberTextField becomeFirstResponder];
    self.cellPhoneSeparatorView.backgroundColor = [UIColor lightGrayColor];
    self.testNumberSeparatorView.backgroundColor = [UIColor lightGrayColor];
    self.citySeparatorView.backgroundColor = [UIColor lightGrayColor];
    self.nickNameSeparatorView.backgroundColor = [UIColor lightGrayColor];
    self.passWordSeparatorView.backgroundColor = [UIColor lightGrayColor];
    self.invatationSeparatorView.backgroundColor = [UIColor lightGrayColor];
    
    [self.testNumberBtn addTarget:self action:@selector(getTestNumber:) forControlEvents:UIControlEventTouchUpInside];
    [self.cityBtn addTarget:self action:@selector(cityLocationBtn:) forControlEvents:UIControlEventTouchUpInside];
    self.cityLabel.text = @"所在地";
    self.nickNameLabel.text = @"昵称";
    [self.darkToBride addTarget:self action:@selector(darkToBride:) forControlEvents:UIControlEventTouchUpInside];
    self.invatationTextField.delegate = self;
    [self.registerBtn addTarget:self action:@selector(nextClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.agreeBtn addTarget:self action:@selector(agreeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.cellPhoneNumberTextField.delegate = self;
    self.testNumberTextField.delegate = self;
    self.nickNameTextField.delegate = self;
    self.passwordTextField.delegate = self;
    //默认进入的时候同意条款按钮是选中的状态
    
    self.agreeBtn.selected  = YES;
    self.registerBtn.layer.cornerRadius = 6;
    self.registerBtn.layer.masksToBounds =YES;
    self.registerBtn.layer.borderColor = SLColor(234, 245, 243).CGColor;
    self.registerBtn.layer.borderWidth = 0.5;
    self.agreementLab.text = @"我已同意";
    [self.userAgreementBtn addTarget:self action:@selector(userAgreementBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.andLab.text = @"和";
    [self.userSecrecy addTarget:self action:@selector(userSecrecyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    //    self.cityLocationLab.text=[userDefault objectForKey:@"location"];
    NSString *location=[userDefault objectForKey:@"location"];
    NSString *LocationCode = [SLUserDefault getLocationCode];
    
    if (LocationCode.length>0) {
        self.cityLocationLab.text=location;
        self.cityId=LocationCode;
    }else{
        self.cityLocationLab.text=@"请选择地区";
    }
    //    self.cityId=[userDefault objectForKey:@"code"];
    self.cityLocationLab.textAlignment=NSTextAlignmentRight;
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(locationBtnClick)];
    self.cityLocationLab.userInteractionEnabled=YES;
    [self.cityLocationLab addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isFromRegister) name:@"isFromRegister" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardDidChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
- (void)keyboardDidChangeFrame:(NSNotification *)noti
{
//    if ([nameTextTf isFirstResponder] || [positionTf.tf isFirstResponder]) {
//        return;
//    }
    
    UITextField *currentTf;
    int i = 0;
    for (UITextField *tf in tfArr) {
        if ([tf isFirstResponder]) {
            currentTf = tf;
            i = (int)[tfArr indexOfObject:tf];
            break;
        }
    }
    
    CGRect convertRect =  [self.baseSc convertRect:currentTf.frame toView:self.view];
    
    CGFloat duration = [noti.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    [UIView animateWithDuration:duration animations:^{
        CGFloat transformY = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].origin.y;
        
        if (transformY  < convertRect.origin.y) {
            
            [self.baseSc setContentOffset:CGPointMake(0, CGRectGetMaxY(currentTf.frame)- transformY+ 64)];
            
        }else{
            [self.baseSc setContentOffset:CGPointMake(0,0)];

        }
    }];
    
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"注册";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=[UIColor lightGrayColor];
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    if (self.isPresentVc) {
        [self.view endEditing:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
-(void)isFromRegister{
    SLLoginBtnViewController *login = [[SLLoginBtnViewController alloc] init];
    [self.navigationController pushViewController:login animated:YES];
}

-(void)locationBtnClick{
    SLLocationViewController *location = [[SLLocationViewController alloc] init];
    location.valueWithCityIdBlcok=^(NSString *cityName,NSString *cityId){
        if (cityName.length>0&&cityId.length>0) {
            self.cityLocationLab.text=cityName;
            self.cityId=cityId;
        }
    };
    
    location.isRegisterLocation=YES;
    [self.navigationController pushViewController:location animated:YES];
    
}
// 启动定时器
- (void)startTimer
{
    __block int timeout = 60; //倒计时时间
    __weak typeof(self) weakSelf = self;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout <= 0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [weakSelf.testNumberBtn setTitle:@"重新获取" forState:UIControlStateNormal];
                [weakSelf.testNumberBtn setTitle:@"重新获取" forState:UIControlStateDisabled];
                [weakSelf.testNumberBtn setEnabled:YES];
                
            });
        }else{
            NSString *strTime = [NSString stringWithFormat:@"%dS", timeout];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [weakSelf.testNumberBtn setTitle:strTime forState:UIControlStateDisabled];
                [weakSelf.testNumberBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
                [weakSelf.testNumberBtn setEnabled:NO];
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

- (IBAction)cancelRegist:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 修改没有网络的情况下不要发送网络请求
- (void)getTestNumber:(id)sender {
    
    //先将未到时间执行前的任务取消
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(todoTestNumber:) object:sender];
    [self performSelector:@selector(todoTestNumber:) withObject:sender afterDelay:0.2f];
}
- (void)todoTestNumber:(id)sender{
    NSString *phoneNumber = [NSString stringWithFormat:@"%@", self.cellPhoneNumberTextField.text];
    if (self.isNetWorkStatus == YES) {
        
        [self alert:@"网络出错,请检查网络状况"];
        
    }else{
        if ([phoneNumber length] <= 0) {
            [self.cellPhoneNumberTextField becomeFirstResponder];
            [self alert:@"请输入手机号"];
            self.testNumberBtn.enabled=YES;
            return ;
        }
        if (![self isPhoneNumberString:self.cellPhoneNumberTextField.text]) {
            [self.cellPhoneNumberTextField becomeFirstResponder];
            [self alert:@"请输入正确的手机号码"];
            self.testNumberBtn.enabled=YES;
            return ;
        }
        
        _testNumberBtn.enabled = NO;
        [_testNumberBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
        [SLAPIHelper getSMSCodeForRegister:phoneNumber success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            
            //_testNumberBtn.enabled = NO;
            [self startTimer];
        } failure:^(SLHttpRequestError *failure) {
            _testNumberBtn.enabled = YES;
            NSLog(@"--slAPICode---%ld",(long)failure.slAPICode);
            NSLog(@"---httpStatusCode----%ld",(long)failure.httpStatusCode);
            if (failure.httpStatusCode==429&&failure.slAPICode==10) {
                [HUDManager showWarningWithText:@"操作频繁,请稍后重试"];
            }
            if (failure.httpStatusCode==400&&failure.slAPICode==1) {
                [HUDManager showWarningWithText:@"手机号无效"];
            }
        }];
    }
}

- (void)nextClick:(id)sender {
    self.registerBtn.enabled=NO;
    //先将未到时间执行前的任务取消
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(todoNextClick:) object:sender];
    [self performSelector:@selector(todoNextClick:) withObject:sender afterDelay:0.2f];
}
- (void)todoNextClick:(id)sender{
    
    //拿到用户输入的参数
    [self.view endEditing:YES];
    NSString *phoneNumber = self.cellPhoneNumberTextField.text;
    NSString *testNumber = self.testNumberTextField.text;
    NSString *nickName = self.nickNameTextField.text;
    NSString *password = self.passwordTextField.text;
    NSString *locationID=self.cityId;
    //判断参数是否有值,如果没有值提醒用户重新输入并返回
    
    //判断如果没有网路情况下没必要请求网络
    
    if (self.isNetWorkStatus == YES) {
        
        //没有网络的情况下给出提示
        [self alert:@"无法连接到服务器"];
        
    }else{
        
        if ([self validateWithPhonrNumber:phoneNumber testNumber:testNumber nickName:nickName password:password locationID:locationID]) {
            [self registWithPhonrNumber:phoneNumber testNumber:testNumber nickName:nickName password:password locationID:locationID ignoreInvicationCode:NO];
        }
        
    }
    
}
// 验证账号和手机
- (BOOL)validateWithPhonrNumber:(NSString *)phoneNumber testNumber:(NSString *)testNumber nickName:(NSString *)nickNmae password:(NSString *)password locationID:(NSString *)locationID

{
    if ([phoneNumber length] <= 0) {
        [self.cellPhoneNumberTextField becomeFirstResponder];
        [self alert:@"请输入手机号"];
        self.registerBtn.enabled=YES;
        return NO;
    }
    if (![self isPhoneNumberString:self.cellPhoneNumberTextField.text]) {
        [self.cellPhoneNumberTextField becomeFirstResponder];
        [self alert:@"请输入正确的手机号码"];
        self.registerBtn.enabled=YES;
        return NO;
    }
    if (!([testNumber length] > 0)) {
        [self alert:@"请输入验证码"];
        [_testNumberBtn becomeFirstResponder];
        self.registerBtn.enabled=YES;
        return NO;
    }
    
    if (!([nickNmae length] > 0)) {
        [self.nickNameTextField becomeFirstResponder];
        [self alert:@"请输入昵称"];
        self.registerBtn.enabled=YES;
        return NO;
    }
    if (nickNmae.length < 1 || nickNmae.length >32) {
        [self alert:@"请输入昵称长度为4至42位"];
        self.registerBtn.enabled=YES;
        return NO;
    }
    if (![self judgeModifyNickName:nickNmae]) {
        [self.nickNameTextField becomeFirstResponder];
        [self alert:@"昵称只能由中文、英文、数字、字母、下划线、横线组成"];
        self.registerBtn.enabled=YES;
        return NO;
    }
    if ([password length] < 6 || [password length] > 18) {
        [self.passwordTextField becomeFirstResponder];
        [self alert:@"请输入长度为6至18位密码"];
        self.registerBtn.enabled=YES;
        return NO;
    }
    if (!_agreeBtn.selected) {
        [self alert:@"请同意超级圈用户协议"];
        self.registerBtn.enabled=YES;
        return NO;
    }
    if ([locationID length] <= 0) {
        [self alert:@"请输入所在地"];
        self.registerBtn.enabled=YES;
        return NO;
    }
    
    
    return YES;
}
-(BOOL)judgeModifyNickName:(NSString *)string{
    NSString *regex = @"[\\w,_,\\-,0x4e00-0x9fa6]{1,50}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:string];
}

#pragma mark - 修改断网情况下的闪退
- (void) registWithPhonrNumber:(NSString *)phoneNumber testNumber:(NSString *)testNumber nickName:(NSString *)nickNmae password:(NSString *)password locationID:(NSString *)locationID ignoreInvicationCode:(BOOL)isIgnore
{
    [self postRegister:phoneNumber testNumber:testNumber nickName:nickNmae password:password locationID:locationID ignoreInvicationCode:isIgnore];
    
}

#pragma mark ---- 在有网络的情况下请求网络数据

- (void)postRegister:(NSString *)phoneNumber testNumber:(NSString *)testNumber nickName:(NSString *)nickNmae password:(NSString *)password locationID:(NSString *)locationID ignoreInvicationCode:(BOOL)isIgnore{
    
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow withText:@"正在注册"];
    
    //对应的加密密钥
    __block  NSString *security=@"";
    //获取AES加密密钥
    NSString *uuid= [[NSUUID UUID] UUIDString];
    //减去"-"
    NSString *tokenStr = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSDictionary *params=@{@"token":tokenStr};
    [SLAPIHelper getAESSecurity:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        security = data[@"result"];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *encryptCellphone = [AESCrypt encrypt:phoneNumber password:security];
            NSString *encryptPassword = [AESCrypt encrypt:password password:security];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [parameter setObject:encryptCellphone forKey:@"cellphone"];
                [parameter setObject:encryptPassword forKey:@"password"];
                [parameter setObject:nickNmae forKey:@"nickname"];
                [parameter setObject:testNumber forKey:@"code"];
                [parameter setObject:tokenStr forKey:@"token"];
                [parameter setObject:locationID forKey:@"location_code"];
                if (!isIgnore) {
                    if ([NSString isRealString:self.invatationTextField.text]) {
                        [parameter setObject:self.invatationTextField.text forKey:@"invite_code"];
                    }
                }
                
                //    NSDictionary *parameter = @{@"cellphone":phoneNumber,@"password":password,@"nickname":nickNmae, @"code":testNumber,@"location_code":locationID};
                [self encryptSuccessRegister:parameter];
            });
            
        });
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        self.registerBtn.enabled=YES;
        [HUDManager showWarningWithText:@"注册失败"];
    }];
    
}
//加密成功后注册
-(void)encryptSuccessRegister:(NSMutableDictionary *)parameter {
    
    [SLAPIHelper Register:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        [HUDManager hideHUDView];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [HUDManager showWarningWithText:@"注册成功"];
        });
        [self performSelector:@selector(toTabBarController) withObject:nil afterDelay:1.0f];
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        self.registerBtn.enabled=YES;
        if (failure.slAPICode==20) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [HUDManager showWarningWithText:@"昵称包含违禁词语，注册失败"];
            });
            
        }else if(failure.slAPICode==213){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"邀请码无效，是否要继续注册？如果继续邀请双方将无法获取到现金券" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"继续", nil];
            [alert show];
            
        }else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [HUDManager showWarningWithText:@"注册失败"];
            });
        }
        
        
    }];
    
}
// 登录操作
- (void)toTabBarController
{   [self loginPassWord:_cellPhoneNumberTextField.text password:_passwordTextField.text];
    
}
#pragma mark - ---- 登陆的相关代码
- (void)loginPassWord:(NSString *)account password:(NSString *)password{
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.userInteractionEnabled = NO;
    //    hud.labelText = @"正在登录...";
    
    //    NSDictionary *parameter = @{@"cellphone":account,@"password":password};
    //    NSDictionary *parameter = @{@"cellphone":account,@"password":password,@"cid":[GeTuiSdk clientId],@"phone_system":@"1"};
    
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow withText:@"正在登录"];
    NSMutableDictionary *parameter=[NSMutableDictionary new];
    //对应的加密密钥
    __block  NSString *security=@"";
    //获取AES加密密钥
    NSString *uuid= [[NSUUID UUID] UUIDString];
    //减去"-"
    NSString *tokenStr = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSDictionary *params=@{@"token":tokenStr};
    [SLAPIHelper getAESSecurity:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        security = data[@"result"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSString *encryptCellphone = [AESCrypt encrypt:account password:security];
            NSString *encryptPassword = [AESCrypt encrypt:password password:security];
            dispatch_async(dispatch_get_main_queue(), ^{
                [parameter setObject:encryptCellphone forKey:@"cellphone"];
                [parameter setObject:encryptPassword forKey:@"password"];
                if ([GeTuiSdk clientId]) {
                    [parameter setObject:[GeTuiSdk clientId] forKey:@"cid"];
                }else{
                    NSException *exception = [NSException exceptionWithName:@"RegisterGeTui"
                                                                     reason:@"获取不到个推cid"
                                                                   userInfo:nil];
                    [Bugly reportException:exception];
                }
                [parameter setObject:@"1" forKey:@"phone_system"];
                [parameter setObject:tokenStr forKey:@"token"];
                [self encryptSuccessLogin:parameter security:security account:account];
            });
        });
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        self.registerBtn.enabled=YES;
        [HUDManager showWarningWithText:@"登录失败"];
    }];
}
//加密成功后登陆
-(void)encryptSuccessLogin:(NSDictionary *)parameter security:(NSString *)security account:(NSString *)account {
    
    [SLAPIHelper login:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        ApplicationDelegate.isLogin = YES;
        //如果登录成功之后保存手机号码
        [[NSNotificationCenter defaultCenter] postNotificationName:@"getGeTuiMessage" object:nil];
        NSUserDefaults *phoneNumber = [NSUserDefaults standardUserDefaults];
        [phoneNumber setObject:_cellPhoneNumberTextField.text forKey:@"phoneNumber"];
        [phoneNumber synchronize];
        ApplicationDelegate.userId = data[@"result"][@"id"];
        NSString *nameAndPwd  = [NSString stringWithFormat:@"%@:%@",data[@"result"][@"id"], data[@"result"][@"password"]];
        ApplicationDelegate.Basic = [nameAndPwd base64EncodedString];
        ApplicationDelegate.userInformation = [data[@"result"] mutableCopy];

        NSString *im_password=data[@"result"][@"im_password"];
        NSString * decryptPassword = [AESCrypt decrypt:im_password  password:security];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self LoginIm:account password:decryptPassword ];
        });
        
    } failure:^(SLHttpRequestError *error) {
        [HUDManager hideHUDView];
        self.registerBtn.enabled=YES;
        if (error.httpStatusCode==403) {
            if (error.slAPICode==206||error.slAPICode==207) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [HUDManager showWarningWithText:@"登录失败,请检查手机号和密码"];
                });
            }
        }else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [HUDManager showWarningWithText:@"登录失败"];
            });
        }
    }];
}
#pragma mark -- 登录IM相关
- (void)LoginIm:(NSString *)account password:(NSString *)password{
     [SLIMCommon Login:account password:password navigationViewController:self isTabbarVC:self.isTabbarVC isReplacePasswordVC:nil loginBtn:self.registerBtn isFromRegister:YES];
}

#pragma UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.cellPhoneNumberTextField)  [self.testNumberTextField becomeFirstResponder];
    if (textField == self.testNumberTextField)  [self.nickNameTextField becomeFirstResponder];
    if (textField == self.nickNameTextField)  [self.passwordTextField becomeFirstResponder];
    if (textField == self.passwordTextField)
        [self nextClick:nil];
    return YES;
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)even{
    [self.view endEditing:YES];
}

- (void)darkToBride:(id)sender {
    _darkToBride.selected = !_darkToBride.selected;
    NSString* text = self.passwordTextField.text;
    self.passwordTextField.text = @" ";
    self.passwordTextField.text = text;
    self.passwordTextField.secureTextEntry = !_darkToBride.selected;
}

- (void)agreeBtnClick:(id)sender {
    _agreeBtn.selected = !_agreeBtn.selected;
    //默认_agreeBtn是选中状态
    if (!_agreeBtn.selected) {
        _agreementLab.textColor = SLColor(201, 201, 201);
        _andLab.textColor = SLColor(201, 201, 201);
        [_registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else{
        _agreementLab.textColor = SLColor(81, 82, 82);
        _andLab.textColor = SLColor(81, 82, 82);
        [_registerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
}

- (void)userAgreementBtnClick:(id)sender {
    SLWebViewController *vc=[[SLWebViewController alloc] init];
    vc.url=@"http://m.superloop.com.cn/agreement.html";
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)userSecrecyBtnClick:(id)sender {
    SLWebViewController *vc=[[SLWebViewController alloc] init];
    vc.url=@"http://m.superloop.com.cn/privacy.html";
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)cityLocationBtn:(id)sender {
    
    SLLocationViewController *location = [[SLLocationViewController alloc] init];
    location.valueWithCityIdBlcok=^(NSString *cityName,NSString *cityId){
        self.cityLocationLab.text=cityName;
        self.cityId=cityId;
    };
    
    location.isRegisterLocation=YES;
    [self.navigationController pushViewController:location animated:YES];
    
}
#pragma mark --- 需要检测网络状态
- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.isNetWorkStatus = YES;
        }else if ([netWorkStaus isEqualToString:@"wifi"]||[netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.isNetWorkStatus = NO;
        }
    }
    
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        
        
    }else{
        [self.view endEditing:YES];
        NSString *phoneNumber = self.cellPhoneNumberTextField.text;
        NSString *testNumber = self.testNumberTextField.text;
        NSString *nickName = self.nickNameTextField.text;
        NSString *password = self.passwordTextField.text;
        NSString *locationID=self.cityId;
        //判断参数是否有值,如果没有值提醒用户重新输入并返回
        
        //判断如果没有网路情况下没必要请求网络
        
        if (self.isNetWorkStatus == YES) {
            
            //没有网络的情况下给出提示
            [self alert:@"无法连接到服务器"];
            
        }else{
            
            if ([self validateWithPhonrNumber:phoneNumber testNumber:testNumber nickName:nickName password:password locationID:locationID]) {
                [self registWithPhonrNumber:phoneNumber testNumber:testNumber nickName:nickName password:password locationID:locationID ignoreInvicationCode:YES];
            }
            
        }
        
    }
}

- (void)endEdit{
    [self.view endEditing:YES];
}
- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
