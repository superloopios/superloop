//  完善资料
//  SLPerfectInformationViewController.m
//  Superloop
//
//  Created by xiaowu on 16/11/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "AppDelegate.h"
#import "SLPerfectInformationViewController.h"
#import "SLIntroduceViewController.h"
#import "SLMoreCategoryViewController.h"
#import "SLAPIHelper.h"
#import "NSString+Utils.h"
#import "SLTabBarViewController.h"


@interface SLPerfectInformationViewController (){
    UIButton *_ignoreBtn;
    UIButton *backBtn;
}

@property (nonatomic, assign) BOOL                          hasNetWork;
@property (nonatomic, strong) UILabel                       *successLabel;
@property (nonatomic, strong) UILabel                       *introduceLabel;
@property (nonatomic, strong) UILabel                       *tagsLabel;
@property (nonatomic, strong) UIButton                      *introduceBtn;
@property (nonatomic, strong) UIButton                      *primaryTagBtn;
@property (nonatomic, strong) UIButton                      *secondTagBtn;
@property (nonatomic, strong) UIButton                      *ensureBtn;
@property (nonatomic,   copy) NSString                      *bioStr;
@property (nonatomic, strong) NSDictionary                  *userInfo;

@end

@implementation SLPerfectInformationViewController

- (UIButton *)ensureBtn{
    if (!_ensureBtn) {
        _ensureBtn = [[UIButton alloc] init];
        [_ensureBtn setTitle:@"确定" forState:UIControlStateNormal];
        [_ensureBtn setBackgroundColor:SLMainColor];
        [_ensureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _ensureBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        _ensureBtn.frame = CGRectMake(20, 434-64, screen_W-40, 40);
        _ensureBtn.layer.cornerRadius = 5;
        [self.view addSubview:_ensureBtn];
        [_ensureBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.secondTagBtn.mas_bottom).offset(30);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(40);
            make.left.mas_equalTo(self.view).offset(20);
        }];
    }
    return _ensureBtn;
}

- (UIButton *)introduceBtn{
    if (!_introduceBtn) {
        _introduceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        if (self.userInfo) {
            if ([NSString isRealString:self.userInfo[@"bio"]]) {
                self.bioStr = self.userInfo[@"bio"];
                [_introduceBtn setTitle:@"已添加" forState:UIControlStateNormal];
                [_introduceBtn setTitleColor:SLBlackTitleColor forState:UIControlStateNormal];
            }else{
                [_introduceBtn setTitle:@"添加个人简介" forState:UIControlStateNormal];
                [_introduceBtn setTitleColor:UIColorFromRGB(0x5a92ff) forState:UIControlStateNormal];
            }
        }else{
            [_introduceBtn setTitle:@"添加个人简介" forState:UIControlStateNormal];
            [_introduceBtn setTitleColor:UIColorFromRGB(0x5a92ff) forState:UIControlStateNormal];
        }
        _introduceBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [self.view addSubview:_introduceBtn];
        [_introduceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.introduceLabel.mas_right).offset(5);
            make.centerY.equalTo(self.introduceLabel);
            make.height.mas_equalTo(self.introduceLabel);
        }];
    }
    return _introduceBtn;
}

- (UIButton *)primaryTagBtn{
    if (!_primaryTagBtn) {
        _primaryTagBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        if (self.userInfo) {
            if ([[self.userInfo allKeys] containsObject:@"primary_tags"]) {
                [_primaryTagBtn setTitle:@"已添加" forState:UIControlStateNormal];
                [_primaryTagBtn setTitleColor:SLBlackTitleColor forState:UIControlStateNormal];
            }else{
                [_primaryTagBtn setTitle:@"我擅长的" forState:UIControlStateNormal];
                [_primaryTagBtn setTitleColor:UIColorFromRGB(0x5a92ff) forState:UIControlStateNormal];
            }
        }else{
            [_primaryTagBtn setTitle:@"我擅长的" forState:UIControlStateNormal];
            [_primaryTagBtn setTitleColor:UIColorFromRGB(0x5a92ff) forState:UIControlStateNormal];
        }
        _primaryTagBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [self.view addSubview:_primaryTagBtn];
        [_primaryTagBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.tagsLabel.mas_right).offset(5);
            make.centerY.equalTo(self.tagsLabel);
            make.height.mas_equalTo(self.tagsLabel);
        }];
    }
    return _primaryTagBtn;
}
- (UIButton *)secondTagBtn{
    if (!_secondTagBtn) {
        _secondTagBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        if (self.userInfo) {
            if ([[self.userInfo allKeys] containsObject:@"secondary_tags"]) {
                [_secondTagBtn setTitle:@"已添加" forState:UIControlStateNormal];
                [_secondTagBtn setTitleColor:SLBlackTitleColor forState:UIControlStateNormal];
            }else{
                [_secondTagBtn setTitle:@"我喜欢的" forState:UIControlStateNormal];
                [_secondTagBtn setTitleColor:UIColorFromRGB(0x5a92ff) forState:UIControlStateNormal];
            }
        }else{
            [_secondTagBtn setTitle:@"我喜欢的" forState:UIControlStateNormal];
            [_secondTagBtn setTitleColor:UIColorFromRGB(0x5a92ff) forState:UIControlStateNormal];
        }
        _secondTagBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [self.view addSubview:_secondTagBtn];
        [_secondTagBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.tagsLabel.mas_right).offset(5);
            make.top.equalTo(self.tagsLabel.mas_bottom).offset(10);
            make.height.mas_equalTo(self.tagsLabel);
        }];
    }
    return _secondTagBtn;
}

- (UILabel *)introduceLabel{
    if (!_introduceLabel) {
        _introduceLabel = [[UILabel alloc] init];
        _introduceLabel.font = [UIFont systemFontOfSize:16];
        _introduceLabel.text = @"个人简介 ：";
        _introduceLabel.textColor = SLMainColor;
        [self.view addSubview:_introduceLabel];
        [_introduceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.view).offset(20);
            make.top.mas_equalTo(self.successLabel.mas_bottom).offset(20);
        }];
    }
    return _introduceLabel;
}
- (UILabel *)tagsLabel{
    if (!_tagsLabel) {
        _tagsLabel = [[UILabel alloc] init];
        _tagsLabel.font = [UIFont systemFontOfSize:16];
        _tagsLabel.text = @"个人标签 ：";
        _tagsLabel.textColor = SLMainColor;
        [self.view addSubview:_tagsLabel];
        [_tagsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.view).offset(20);
            make.top.mas_equalTo(self.introduceLabel.mas_bottom).offset(15);
        }];
    }
    return _tagsLabel;
}

- (UILabel *)successLabel{
    if (!_successLabel) {
        _successLabel=[[UILabel alloc] init];
        _successLabel.numberOfLines = 2;
        //富文本设置
        NSString *str = @"超级圈汇聚了各路超人，\n添加个人标签和简介，才能脱颖而出哦！";
        
        NSRange range = NSMakeRange(0, str.length);
        NSLog(@"%@",NSStringFromRange(range));
        NSMutableAttributedString *absAttention=[[NSMutableAttributedString alloc] initWithString:str];
        [absAttention addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:range];
        [absAttention addAttribute:NSForegroundColorAttributeName value:SLMainColor range:range];
        
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3;
        [absAttention addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
        
        _successLabel.attributedText=absAttention;
        
        [self.view addSubview:_successLabel];
        //        [_attentionLab sizeToFit];
        [_successLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view).offset(64);
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
        }];
        
    }
    return _successLabel;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
     self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNav];
    self.userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"userInfo"];
    NSLog(@"%@",self.userInfo);
    [self setUpUI];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserInfo) name:@"resetSecondMeData" object:nil];
}
-(void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetWork=NO;
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            self.hasNetWork=YES;
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.hasNetWork=YES;
        }
    }
}
- (void)setUpUI{
    [self.view addSubview:self.successLabel];
    self.introduceLabel.text = @"个人简介 ：";
    self.tagsLabel.text = @"个人标签 ：";
    [self.primaryTagBtn addTarget:self action:@selector(choosePrimaryTag) forControlEvents:UIControlEventTouchUpInside];
    [self.introduceBtn addTarget:self action:@selector(updateIntroduce) forControlEvents:UIControlEventTouchUpInside];
    [self.secondTagBtn addTarget:self action:@selector(chooseSecondTag) forControlEvents:UIControlEventTouchUpInside];
    [self.ensureBtn addTarget:self action:@selector(ignore) forControlEvents:UIControlEventTouchUpInside];
}
- (void)updateIntroduce{
    
    SLIntroduceViewController *introduce = [[SLIntroduceViewController alloc] init];
    introduce.intro = [NSString isRealString:self.bioStr] ? self.bioStr : @"";
    __weak typeof(self) WeakSelf = self;
    introduce.introduceBlcok= ^(NSString *str){
        __strong typeof(WeakSelf) StrongSelf = WeakSelf;
        if ([NSString isRealString:str]) {
            [StrongSelf.introduceBtn setTitle:@"已添加" forState:UIControlStateNormal];
            [StrongSelf.introduceBtn setTitleColor:SLBlackTitleColor forState:UIControlStateNormal];
        }else{
            [StrongSelf.introduceBtn setTitle:@"添加个人简介" forState:UIControlStateNormal];
            [StrongSelf.introduceBtn setTitleColor:UIColorFromRGB(0x5a92ff) forState:UIControlStateNormal];
        }
        
        StrongSelf.bioStr = str;
        StrongSelf.introduceBlcok(str);
    };
    [self.navigationController pushViewController:introduce animated:YES];
}
- (void)choosePrimaryTag{
    self.primaryTagBtn.selected = !self.primaryTagBtn.selected;
    [self chooseTags:@1];
}
- (void)chooseSecondTag{
    self.secondTagBtn.selected = !self.secondTagBtn.selected;
    [self chooseTags:@2];
}
- (void)ensureBtnClick{
    
}

- (void)setUpNav{
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 70, 50);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setTitle:@"上一步" forState:UIControlStateNormal];
    backBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [backBtn setTitleColor:SLMainColor forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"完善资料";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor = SLMainColor;
    [navView addSubview:nameLab];
    
    _ignoreBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    _ignoreBtn.frame=CGRectMake(ScreenW-60, 20, 60, 50);
    [_ignoreBtn setTitle:@"跳过" forState:UIControlStateNormal];
    _ignoreBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_ignoreBtn setTitleColor:SLMainColor forState:UIControlStateNormal];
    [_ignoreBtn addTarget:self action:@selector(ignore) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:_ignoreBtn];
    
    [self.view addSubview:navView];
}
#pragma mark - 选择标签
- (void)chooseTags:(NSNumber *)num{
//    if ([self.tagsType isEqual:@1]) {
//        self.changeTags(self.tagsType,_primary_tagsArr);
//        [[NSUserDefaults standardUserDefaults] setObject:_primary_tagsArr forKey:@"primary_tags"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
//    if ([self.tagsType isEqual:@2]) {
//        self.changeTags(self.tagsType,_secondary_tagsArr);
//        [[NSUserDefaults standardUserDefaults] setObject:_primary_tagsArr forKey:@"secondary_tags"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
    
    __weak typeof(self) WeakSelf = self;
    SLMoreCategoryViewController *SLMoreCategoryVC = [[SLMoreCategoryViewController alloc] init];
    SLMoreCategoryVC.tagsType = num;
    SLMoreCategoryVC.changeTags = ^(NSNumber *type,NSArray *tags){
        __strong typeof(WeakSelf) StrongSelf = WeakSelf;
        if ([type isEqual:@1]) {
            if (tags.count>0) {
                [StrongSelf.primaryTagBtn setTitle:@"已添加" forState:UIControlStateNormal];
                [StrongSelf.primaryTagBtn setTitleColor:SLBlackTitleColor forState:UIControlStateNormal];
            }else{
                [StrongSelf.primaryTagBtn setTitle:@"我擅长的" forState:UIControlStateNormal];
                [StrongSelf.primaryTagBtn setTitleColor:UIColorFromRGB(0x5a92ff) forState:UIControlStateNormal];
            }
        }else if ([type isEqual:@2]){
            if (tags.count>0) {
                [StrongSelf.secondTagBtn setTitle:@"已添加" forState:UIControlStateNormal];
                [StrongSelf.secondTagBtn setTitleColor:SLBlackTitleColor forState:UIControlStateNormal];
            }else{
                [StrongSelf.secondTagBtn setTitle:@"我喜欢的" forState:UIControlStateNormal];
                [StrongSelf.secondTagBtn setTitleColor:UIColorFromRGB(0x5a92ff) forState:UIControlStateNormal];
            }
        }
    };
    
    SLMoreCategoryVC.userData = self.userInfo;
    [self.navigationController pushViewController:SLMoreCategoryVC animated:YES];
}
-(void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)ignore{
    SLTabBarViewController *indexVC = [[SLTabBarViewController alloc] init];
    [UIApplication sharedApplication].keyWindow.rootViewController = indexVC;
}
- (void)getUserInfo{
    NSDictionary *parameters =  @{@"id":ApplicationDelegate.userId};
    [SLAPIHelper getUsersData:parameters success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        self.userInfo = data[@"result"];
        [[NSUserDefaults standardUserDefaults] setObject:data[@"result"] forKey:@"userInfo"];
    } failure:nil];
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
