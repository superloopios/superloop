//
//  SLWeChatRegisterViewController.m
//  Superloop
//
//  Created by xiaowu on 16/11/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "AppDelegate.h"
#import "SLWeChatRegisterViewController.h"
#import "SLUpdateAvatarAndNicknameViewController.h"
#import "SLWebViewController.h"
#import "SLLoginTextField.h"
#import "Utils.h"
#import "SLAPIHelper.h"
#import "AESCrypt.h"
#import "SLIMCommon.h"
#import "SLTabBarViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface SLWeChatRegisterViewController ()<CLLocationManagerDelegate,UITextFieldDelegate>

@property (nonatomic, strong) UILabel                       *successLabel;
@property (nonatomic, strong) SLLoginTextField              *phoneTextfield;
@property (nonatomic, strong) SLLoginTextField              *verificationTextfield;
@property (nonatomic, strong) UIButton                      *getVerificationBtn;
@property (nonatomic, strong) UIButton                      *registerBtn;
@property (nonatomic, strong) UIButton                      *agreeBtn;
@property (nonatomic, strong) UILabel                       *agreementLab;
@property (nonatomic, strong) UIButton                      *userAgreementBtn;
@property (nonatomic, strong) UIButton                      *userSecrecy;
@property (nonatomic, strong) UILabel                       *andLab;
@property (nonatomic, assign) BOOL                          hasNetWork;
@property (nonatomic, strong) CLLocationManager             *locationM;/** 位置管理者 */


@end

@implementation SLWeChatRegisterViewController

- (CLLocationManager *)locationM{
    if(!_locationM){
        _locationM = [[CLLocationManager alloc] init];
        _locationM.delegate = self;
        [_locationM requestWhenInUseAuthorization];
    }
    return _locationM;
}

- (UIButton *)userSecrecy{
    if (!_userSecrecy) {
        _userSecrecy = [UIButton buttonWithType:UIButtonTypeCustom];
        [_userSecrecy setTitleColor:SLMainColor forState:UIControlStateNormal];
        [_userSecrecy setTitle:@"《隐私条款》" forState:UIControlStateNormal];
        _userSecrecy.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.view addSubview:_userSecrecy];
        [_userSecrecy mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.andLab.mas_right);
            make.centerY.equalTo(self.agreeBtn.mas_centerY);
        }];
    }
    return _userSecrecy;
}

- (UILabel *)andLab{
    if (!_andLab) {
        _andLab = [[UILabel alloc] init];
        _andLab.font = [UIFont systemFontOfSize:12];
        _andLab.textColor =  UIColorFromRGB(0xff9497);
        _andLab.text = @"和";
        //        _cellPhoneLabel.backgroundColor = [UIColor redColor];
        [self.view addSubview:_andLab];
        [_andLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.userAgreementBtn.mas_right);
            make.centerY.equalTo(self.agreeBtn.mas_centerY);
        }];
    }
    return _andLab;
}

- (UIButton *)userAgreementBtn{
    if (!_userAgreementBtn) {
        _userAgreementBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_userAgreementBtn setTitleColor:SLMainColor forState:UIControlStateNormal];
        [_userAgreementBtn setTitle:@"《超级圈用户协议》" forState:UIControlStateNormal];
        _userAgreementBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.view addSubview:_userAgreementBtn];
        [_userAgreementBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.agreementLab.mas_right);
            make.centerY.equalTo(self.agreeBtn.mas_centerY);
        }];
    }
    return _userAgreementBtn;
}

- (UIButton *)agreeBtn{
    if (!_agreeBtn) {
        _agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_agreeBtn setImage:[UIImage imageNamed:@"login_checkbox_selected_"] forState:UIControlStateSelected];
        [_agreeBtn setImage:[UIImage imageNamed:@"login_checkbox_disabled_"] forState:UIControlStateNormal];
        //        _agreeBtn.backgroundColor = [UIColor redColor];
        _agreeBtn.imageView.contentMode = UIViewContentModeTop;
        [self.view addSubview:_agreeBtn];
        _agreeBtn.selected = YES;
        [_agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.registerBtn).offset(-8);
            make.top.equalTo(self.registerBtn.mas_bottom).offset(5);
            make.height.width.equalTo(@30);
        }];
    }
    return _agreeBtn;
}

- (UILabel *)agreementLab{
    if (!_agreementLab) {
        _agreementLab = [[UILabel alloc] init];
        _agreementLab.font = [UIFont systemFontOfSize:12];
        _agreementLab.textColor = UIColorFromRGB(0xff9497);
        _agreementLab.text = @"我已同意";
        //        _cellPhoneLabel.backgroundColor = [UIColor redColor];
        [self.view addSubview:_agreementLab];
        [_agreementLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.agreeBtn.mas_right);
            make.centerY.equalTo(self.agreeBtn.mas_centerY);
        }];
    }
    return _agreementLab;
}

- (UIButton *)registerBtn{
    if (!_registerBtn) {
        _registerBtn = [[UIButton alloc] init];
        [_registerBtn setTitle:@"绑定" forState:UIControlStateNormal];
        [_registerBtn setBackgroundColor:SLMainColor];
        [_registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _registerBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        _registerBtn.frame = CGRectMake(20, 434-64, screen_W-40, 40);
        _registerBtn.layer.cornerRadius = 5;
        [self.view addSubview:_registerBtn];
        [_registerBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.verificationTextfield.mas_bottom).offset(30);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(40);
            make.left.mas_equalTo(self.view).offset(20);
        }];
    }
    return _registerBtn;
}

- (UIButton *)getVerificationBtn{
    if (!_getVerificationBtn) {
        _getVerificationBtn = [[UIButton alloc] init];
        [_getVerificationBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_getVerificationBtn setBackgroundColor:SLMainColor];
        [_getVerificationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _getVerificationBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _getVerificationBtn.frame = CGRectMake(screen_W-20-80, 254-64, 80, 38);
        _getVerificationBtn.layer.cornerRadius = 5;
        [self.view addSubview:_getVerificationBtn];
        [_getVerificationBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.verificationTextfield);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(80);
        }];
    }
    return _getVerificationBtn;
}


- (SLLoginTextField *)verificationTextfield{
    if (!_verificationTextfield) {
        _verificationTextfield = [[SLLoginTextField alloc] init];
        _verificationTextfield.delegate = self;
        [self.view addSubview:_verificationTextfield];
        [_verificationTextfield mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.phoneTextfield.mas_bottom).offset(20);
            make.left.mas_equalTo(self.view).offset(20);
            make.width.mas_equalTo(screen_W-130);
            make.height.mas_equalTo(40);
        }];
    }
    return _verificationTextfield;
}

- (SLLoginTextField *)phoneTextfield{
    if (!_phoneTextfield) {
        _phoneTextfield = [[SLLoginTextField alloc] init];
        _phoneTextfield.delegate = self;
        [self.view addSubview:_phoneTextfield];
        [_phoneTextfield mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.successLabel.mas_bottom).offset(25);
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(40);
        }];
    }
    return _phoneTextfield;
}

- (UILabel *)successLabel{
    if (!_successLabel) {
        _successLabel=[[UILabel alloc] init];
        _successLabel.numberOfLines = 2;
        //富文本设置
        NSString *str = @"登录成功！\n为确保您正常使用，请绑定您常用的手机号";
        
        NSRange range = NSMakeRange(0, str.length);
        NSLog(@"%@",NSStringFromRange(range));
        NSMutableAttributedString *absAttention=[[NSMutableAttributedString alloc] initWithString:str];
        [absAttention addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:range];
        [absAttention addAttribute:NSForegroundColorAttributeName value:SLMainColor range:range];
        
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3;
        [absAttention addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
        
        _successLabel.attributedText=absAttention;
        
        [self.view addSubview:_successLabel];
        //        [_attentionLab sizeToFit];
        [_successLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view).offset(65);
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
        }];
        
    }
    return _successLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUpNav];
    [self.locationM startUpdatingLocation];
    // Do any additional setup after loading the view.
    [self setUpUI];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"绑定手机号";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor = SLMainColor;
    [navView addSubview:nameLab];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetWork=NO;
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            self.hasNetWork=YES;
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.hasNetWork=YES;
        }
    }
}
- (void)setUpUI{
    [self.view addSubview:self.successLabel];
    self.phoneTextfield.placeholder = @"手机号";
    self.verificationTextfield.placeholder = @"验证码";
    [self.getVerificationBtn addTarget:self action:@selector(getVericationNumber) forControlEvents:UIControlEventTouchUpInside];
    [self.registerBtn addTarget:self action:@selector(bangDingClick) forControlEvents:UIControlEventTouchUpInside];
    self.agreeBtn.selected = YES;
    [self.agreeBtn addTarget:self action:@selector(agreeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.userAgreementBtn addTarget:self action:@selector(userAgreementBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.userSecrecy addTarget:self action:@selector(userSecrecyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
}
- (void)agreeBtnClick{
    self.agreeBtn.selected = !self.agreeBtn.selected;
}
- (void)getVericationNumber{
    [self.view endEditing:YES];
    if (!self.hasNetWork) {
        [HUDManager showWarningWithText:@"网络出错,请检查网络状况"];
    }else{
        if (![Utils isValidMobileNumber:self.phoneTextfield.text]) {
            [HUDManager showWarningWithText:@"请输入正确的手机号码"];
            return ;
        }
        self.getVerificationBtn.userInteractionEnabled = NO;
        __block  NSString *security=@"";
        //获取AES加密密钥
        NSString *uuid= [[NSUUID UUID] UUIDString];
        //减去"-"
        NSString *tokenStr = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSDictionary *params=@{@"token":tokenStr};
        [HUDManager showLoadingHUDView:self.view withText:@""];
        [SLAPIHelper getAESSecurity:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            security = data[@"result"];
            NSString *account = [self.phoneTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSString *encryptCellphone = [AESCrypt encrypt:account password:security];
            NSString *openID = [AESCrypt encrypt:self.openID password:security];
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            dict[@"token"] = tokenStr;
            dict[@"openid"] = openID;
            dict[@"cellphone"] = encryptCellphone;
            [self getCode:dict];
            
        } failure:^(SLHttpRequestError *failure) {
            [HUDManager hideHUDView];
            self.getVerificationBtn.userInteractionEnabled = YES;
            if (failure.httpStatusCode == 500) {
                [HUDManager showWarningWithText:@"未知错误"];
            }else{
                [HUDManager showWarningWithText:@"获取验证码失败"];
            }
        }];
        
    }
}
- (void)getCode:(NSDictionary *)dict{
    [SLAPIHelper getBingDingWechatCodeForRegister:dict success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        [HUDManager hideHUDView];
//        self.verificationTextfield.text = [NSString stringWithFormat:@"%@",data[@"result"][@"code"]];
//        NSLog(@"%@",data);
        //_testNumberBtn.enabled = NO;
        [self startTimer];
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        self.getVerificationBtn.userInteractionEnabled = YES;
        
        if (failure.httpStatusCode==429&&failure.slAPICode==10) {
            [HUDManager showWarningWithText:@"操作频繁,请稍后重试"];
        }
        if (failure.httpStatusCode==409&&failure.slAPICode==307) {
            [HUDManager showWarningWithText:@"该手机号已被绑定"];
        }
    }];
}

- (void)startTimer
{
    __block int timeout = 60; //倒计时时间
    __weak typeof(self) weakSelf = self;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout <= 0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [weakSelf.getVerificationBtn setTitle:@"重新获取" forState:UIControlStateNormal];
                [weakSelf.getVerificationBtn setTitle:@"重新获取" forState:UIControlStateDisabled];
                weakSelf.getVerificationBtn.userInteractionEnabled = YES;
                
            });
        }else{
            NSString *strTime = [NSString stringWithFormat:@"%dS", timeout];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [weakSelf.getVerificationBtn setTitle:strTime forState:UIControlStateNormal];
                
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

- (void)bangDingClick {
    if (self.agreeBtn.selected) {
        NSString *account = [self.phoneTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *password = [self.verificationTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if ([self validateAccount:account password:password]) {
            [self registerUserNext];
        }
    }else{
        [HUDManager showWarningWithText:@"请同意超级圈协议"];
    }
    
//    SLUpdateAvatarAndNicknameViewController *vc = [[SLUpdateAvatarAndNicknameViewController alloc] init];
//    vc.isWeChatRegister = YES;
//    [self.navigationController pushViewController:vc animated:YES];
}

- (void)userAgreementBtnClick:(id)sender {
    SLWebViewController *vc=[[SLWebViewController alloc] init];
    vc.url=@"http://m.superloop.com.cn/agreement.html";
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)userSecrecyBtnClick:(id)sender {
    SLWebViewController *vc=[[SLWebViewController alloc] init];
    vc.url=@"http://m.superloop.com.cn/privacy.html";
    [self.navigationController pushViewController:vc animated:YES];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    [self setSpace:textField withValue:textField.text withFont:[UIFont systemFontOfSize:16]];
    return YES;
}
//设置行间距和字间距
- (void)setSpace:(UITextField *)textField withValue:(NSString*)str withFont:(UIFont*)font {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f};
    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
    textField.attributedText = attributeStr;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)registerUserNext{
    self.registerBtn.userInteractionEnabled = NO;
    //对应的加密密钥
    __block  NSString *security=@"";
    //获取AES加密密钥
    NSString *uuid= [[NSUUID UUID] UUIDString];
    //减去"-"
    NSString *tokenStr = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSDictionary *params=@{@"token":tokenStr};
    NSString *account = [self.phoneTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *password = [self.verificationTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [HUDManager showLoadingHUDView:self.view withText:@""];
    [SLAPIHelper getAESSecurity:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        security = data[@"result"];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSString *encryptCellphone = [AESCrypt encrypt:account password:security];
        NSString *openID = [AESCrypt encrypt:self.openID password:security];
        NSString *encryptCode = [AESCrypt encrypt:password password:security];
        dict[@"openid"] = openID;
        dict[@"cellphone"] = encryptCellphone;
        dict[@"code"] = encryptCode;
        dict[@"token"] = tokenStr;
        dict[@"os_platform"] = @2;
        NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
        if ([userDefault objectForKey:@"code"]) {
            [dict setObject:[userDefault objectForKey:@"code"] forKey:@"location_code"];
        }

        if ([GeTuiSdk clientId]) {
            [dict setObject:[GeTuiSdk clientId] forKey:@"cid"];
        }
       
        [self registerUserSecondStep:dict security:security];
    } failure:^(SLHttpRequestError *failure) {
        //此处需要加验证码超时处理等处理
        [HUDManager hideHUDView];
        self.registerBtn.userInteractionEnabled = YES;
        [HUDManager showWarningWithText:@"绑定失败"];
    }];
}
- (void)registerUserSecondStep:(NSDictionary *)parameter security:(NSString *)security{
    [SLAPIHelper weChatRegister:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        [HUDManager hideHUDView];
        NSLog(@"%@",data);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            ApplicationDelegate.isLogin = YES;
            if ([Utils isValidMobileNumber:self.phoneTextfield.text]) {
                //如果登录成功之后保存手机号码
                NSUserDefaults *phoneNumber = [NSUserDefaults standardUserDefaults];
                [phoneNumber setObject:self.phoneTextfield.text forKey:@"phoneNumber"];
                [phoneNumber synchronize];
            }
            ApplicationDelegate.userId = data[@"result"][@"id"];
            NSString *nameAndPwd  = [NSString stringWithFormat:@"%@:%@",data[@"result"][@"id"], data[@"result"][@"password"]];
            ApplicationDelegate.Basic = [nameAndPwd base64EncodedString];
            ApplicationDelegate.userInformation = [data[@"result"] mutableCopy];
            
            NSString *im_password=data[@"result"][@"im_password"];
            NSString * decryptPassword = [AESCrypt decrypt:im_password password:security];
            
#warning 待测试
            [self saveBasic:ApplicationDelegate.Basic userInformation:ApplicationDelegate.userInformation password:decryptPassword];
            //应用登陆成功后，登录openIM
            [SLIMCommon Login:self.phoneTextfield.text password:decryptPassword];
            //我也不知道以下代码干啥用
            [[NSNotificationCenter defaultCenter] postNotificationName:@"isLoginSuccessToLoadData" object:self];
            NSDictionary *dict = data[@"result"];
            if ([[dict allKeys] containsObject:@"action"]) {
                if ([dict[@"action"] isEqual:@2]) {
                    [HUDManager showWarningWithText:@"绑定成功"];
                    SLUpdateAvatarAndNicknameViewController *vc = [[SLUpdateAvatarAndNicknameViewController alloc] init];
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }
           else {
                [HUDManager showWarningWithText:@"登录成功"];
                SLTabBarViewController *indexVC = [[SLTabBarViewController alloc] init];
                [UIApplication sharedApplication].keyWindow.rootViewController = indexVC;
            }
        });
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        self.registerBtn.userInteractionEnabled = YES;
        
    }];
}

- (BOOL)validateAccount:(NSString *)account password:(NSString *)password{
    
    if (![Utils isValidMobileNumber:account]) {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请输入正确手机号" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        [alertView show];
        [HUDManager showWarningWithText:@"请输入正确手机号"];
        return NO;
    }
    
    if ([password length] != 6 ) {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请输入正确验证码" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        [alertView show];
        [HUDManager showWarningWithText:@"请输入正确验证码"];
        return NO;
    }
    
    return YES;
}
- (void)saveBasic:(NSString *)basic userInformation:(NSMutableDictionary *)userInformation password:(NSString *)password
{
    NSDictionary *dict = @{@"basic": basic, @"userInformation": userInformation,@"Password": password};
    [SLUserDefault setUserInfo:dict];
}
#pragma mark - 找出定位城市的code
-(NSString *)findLocationCityCode:(NSString *)locationCity districts:(NSArray *)districts{
    if (districts.count == 0) return nil;
    for(NSDictionary *dist in districts){
        NSString *name = dist[@"name"];
        if([name isEqualToString:locationCity]){
            return dist[@"code"];
        }else{
            NSArray *children = dist[@"children"];
            if(children.count == 0)continue;
            return [self findLocationCityCode:locationCity districts:children];
        }
    }
    
    return nil;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:39.921 longitude:116.484];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:loc completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        CLPlacemark *pl = [placemarks firstObject];
        if(error == nil)
        {
            NSString *city = pl.locality;
            if (!city) {
                //四大直辖市的城市信息无法通过locality获得，只能通过获取省份的方法来获得（如果city为空，则可知为直辖市）
                city = pl.administrativeArea;
            }
            
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"AreaProvince" ofType:@"json"];
            NSData *data = [[NSData alloc] initWithContentsOfFile:filePath];
            NSArray *districts = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSString *code = @"";
            
            for(NSDictionary * dist in districts){
                NSArray *provinces = dist[@"provinces"];
                if(provinces.count == 0) continue;
                code = [self findLocationCityCode:city districts:provinces];
                
                if(code) break;
            }
            
            NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:city forKey:@"location"];
            [userDefault setObject:code forKey:@"code"];
            //            NSLog(@"%@",[userDefault objectForKey:@"code"]);
            [userDefault synchronize];
            
            //            self.cityStr = city;
            
        }
        else
        {
            NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:@"faiure" forKey:@"faiure"];
            //            NSLog(@"编码失败");
        }
        
    }];
    [_locationM stopUpdatingLocation];
    
}

#pragma mark --处理定位失败
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    
    if(error.code == kCLErrorLocationUnknown)
    {
        [userDefault setObject:@"faiure" forKey:@"faiure"];
    }
    else if(error.code == kCLErrorNetwork)
    {
        [userDefault setObject:@"faiure" forKey:@"faiure"];
        
    }
    else if(error.code == kCLErrorDenied)
    {
        [userDefault setObject:@"faiure" forKey:@"faiure"];
        
        [_locationM stopUpdatingLocation];
        _locationM = nil;
    }
}

#pragma mark -- 用户关闭了定位服务
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    if (status == kCLAuthorizationStatusNotDetermined) {
        
    }else if (status == kCLAuthorizationStatusAuthorizedAlways ||
              status == kCLAuthorizationStatusAuthorizedWhenInUse){
        [_locationM startUpdatingLocation];
        
    }else
    {
        [_locationM startUpdatingLocation];
        [userDefault setObject:@"closeLocation" forKey:@"closeLocation"];
    }
    
    if (status == kCLAuthorizationStatusDenied) {
        [userDefault setObject:@"closeLocation" forKey:@"closeLocation"];
    }
}


@end
