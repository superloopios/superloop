//
//  SLLoginBtnViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/3.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "SLLoginBtnViewController.h"
#import "SLTabBarViewController.h"
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "SLMessageManger.h"
#import "SPUtil.h"
#import "AESCrypt.h"
#import <MBProgressHUD.h>
#import "SLForgetPassWordViewController.h"
#import "SLAPIHelper.h"
#import "GeTuiSdk.h"
#import "AESCrypt.h"
#import "SLOpenIMCommon.h"
#import "SLIMCommon.h"
#import <Bugly/Bugly.h>

@interface SLLoginBtnViewController ()<UITextFieldDelegate>
{
    NSInteger _loginIMCount; //登录IM执行的次数
    
}
@property (weak, nonatomic) IBOutlet UITextField *userNmaeTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;
@property (weak, nonatomic) IBOutlet UIButton *darkToBride;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerContract;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneContract;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pwdContract;
//@property (nonatomic,strong)NSDictionary *parameter;
@property (nonatomic,strong)NSMutableDictionary *parameter;
@property (nonatomic,assign)BOOL hasNetWork;

@end

@implementation SLLoginBtnViewController

-(NSMutableDictionary *)parameter{
    if (!_parameter) {
        _parameter=[NSMutableDictionary new];
    }
    return _parameter;
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [self.headerContract setConstant:0.5];
    [self.phoneContract setConstant:0.5];
    [self.pwdContract setConstant:0.5];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _loginIMCount = 0;
    self.view.backgroundColor=[UIColor whiteColor];
    self.userNmaeTextField.delegate = self;
    self.userNmaeTextField.tintColor = [UIColor grayColor];
    [self.userNmaeTextField becomeFirstResponder];
    self.pwdTextField.delegate = self;
    self.pwdTextField.tintColor = [UIColor grayColor];
    
    self.loginBtn.backgroundColor = SLColor(238, 238, 238);
    self.loginBtn.layer.cornerRadius = 6;
    self.loginBtn.layer.masksToBounds = YES;
    
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
-(void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    NSLog(@"%@",netWorkStaus);
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetWork=NO;
            self.loginBtn.enabled=YES;
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            self.hasNetWork=YES;
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.hasNetWork=YES;
        }
    }
    
}
//验证是否是手机号
-(BOOL)validateCellPhone
{
    //手机号正则
    NSString *cellPhoneStr=@"^((13[0-9])|(15[0-9])|(18[0-9])|(17[0-9]))\\d{8}$";
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cellPhoneStr];
    
    if ([pred evaluateWithObject:self.userNmaeTextField.text]) {
        
        return YES;
    }else{
        return NO;
    }
}


- (IBAction)cancelLoginClick:(id)sender {
    //判断是不是从设置界面过来的
    if (self.isComeFromSettingVc) {
        [self.view endEditing:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)loginbtnClick:(id)sender {
    self.loginBtn.enabled=NO;
    //先将未到时间执行前的任务取消。
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(todoLoginBtnClick:) object:sender];
    [self performSelector:@selector(todoLoginBtnClick:) withObject:sender afterDelay:0.2f];
}

- (void)todoLoginBtnClick:(id)sender{
    // 去除空格
    [self.view endEditing:YES];
    NSString *account = [self.userNmaeTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *password = [self.pwdTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if([self validateAccount:account password:password]) {
        if (self.hasNetWork) {
            [self loginWithAccount:account password:password];
        }else{
            self.loginBtn.enabled=YES;
            [HUDManager showWarningWithText:@"请检查网络"];
        }
       
    }
    
}
// 调用后台登录接口
- (void)loginWithAccount:(NSString *)account password:(NSString *)password
{
    if (!self.hasNetWork) {
        [self alert:@"网络出错,请检查网络状况"];
        self.loginBtn.enabled = YES;
    }else{
        [self loginPassWord:account password:password];
        
    }
}

#pragma mark - ---- 登陆的相关代码
- (void)loginPassWord:(NSString *)account password:(NSString *)password{
    
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow withText:@"正在登录"];
    
    //对应的加密密钥
    __block  NSString *security=@"";
    //获取AES加密密钥
    NSString *uuid= [[NSUUID UUID] UUIDString];
    //减去"-"
    NSString *tokenStr = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSDictionary *params=@{@"token":tokenStr};
    [SLAPIHelper getAESSecurity:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        security = data[@"result"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *encryptCellphone = [AESCrypt encrypt:account password:security];
            NSString *encryptPassword = [AESCrypt encrypt:password password:security];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([self validateCellPhone]) {
                    [self.parameter setObject:encryptCellphone forKey:@"cellphone"];
                }else{
                    [self.parameter setObject:encryptCellphone forKey:@"username"];
                }
                [self.parameter setObject:encryptPassword forKey:@"password"];
                if ([GeTuiSdk clientId]) {
                    [self.parameter setObject:[GeTuiSdk clientId] forKey:@"cid"];
                }else{
                    NSException *exception = [NSException exceptionWithName:@"LoginGeTui"
                                                                     reason:@"获取不到个推cid"
                                                                   userInfo:nil];
                    [Bugly reportException:exception];
                }
                [self.parameter setObject:@"1" forKey:@"phone_system"];
                [self.parameter setObject:tokenStr forKey:@"token"];
                [self encryptSuccess:security account:account];
                
            });
        });
        
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        self.loginBtn.enabled=YES;
        if (failure.httpStatusCode == 500) {
            [HUDManager showWarningWithText:@"未知错误"];
        }else{
            [HUDManager showWarningWithText:@"登录失败,请重试"];
        }
    }];
}
//加密成功后登陆
-(void)encryptSuccess:(NSString *)security account:(NSString *)account{
    [SLAPIHelper login:self.parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        ApplicationDelegate.isLogin = YES;
        if ([self validateCellPhone]) {
            //如果登录成功之后保存手机号码
            NSUserDefaults *phoneNumber = [NSUserDefaults standardUserDefaults];
            [phoneNumber setObject:_userNmaeTextField.text forKey:@"phoneNumber"];
            [phoneNumber synchronize];
        }
        ApplicationDelegate.userId = data[@"result"][@"id"];
        NSString *nameAndPwd  = [NSString stringWithFormat:@"%@:%@",data[@"result"][@"id"], data[@"result"][@"password"]];
        ApplicationDelegate.Basic = [nameAndPwd base64EncodedString];
        ApplicationDelegate.userInformation = [data[@"result"] mutableCopy];

        NSString *im_password=data[@"result"][@"im_password"];
        NSString * decryptPassword = [AESCrypt decrypt:im_password  password:security];
        //应用登陆成功后，调用SDK
        [self LoginIm:account password:decryptPassword];
        

//        NSString *userID = data[@"result"][@"id"];
//        ApplicationDelegate.userId = userID;
//        NSString *im_password=data[@"result"][@"im_password"];
//        NSString * decryptPassword = [AESCrypt decrypt:im_password  password:security];
//        //应用登陆成功后，调用SDK
//        [self LoginIm:account password:decryptPassword userID:userID undecryptPassword:data[@"result"][@"password"]];
        
        
    } failure:^(SLHttpRequestError *error) {
        NSLog(@"%ld",error.httpStatusCode);
        self.loginBtn.enabled=YES;
        [HUDManager hideHUDView];
        if (error.httpStatusCode==403) {
            if (error.slAPICode==206||error.slAPICode==207) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [HUDManager showWarningWithText:@"登录失败,请检查手机号和密码"];
                });
            }
        }else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [HUDManager showWarningWithText:@"登录失败,请稍后重试"];
            });
        }
    }];
    
}


#pragma mark --登录IM
- (void)LoginIm:(NSString *)account password:(NSString *)password{
//- (void)LoginIm:(NSString *)account password:(NSString *)password userID:(NSString *)userID undecryptPassword:(NSString *)undecryptPassword{

    [SLIMCommon Login:account password:password navigationViewController:self isTabbarVC:self.isTabbarVC isReplacePasswordVC:self.isReplacePasswordVC loginBtn:self.loginBtn isFromRegister:NO];
    //获取个人信息
   // [self getUserInfo:userID password:(NSString *)password undecryptPassword:undecryptPassword];
}
- (void)getUserInfo:(NSString *)userID password:(NSString *)password undecryptPassword:(NSString *)undecryptPassword{
    NSDictionary * pargram = @{@"id":userID};
    [SLAPIHelper getUsersData:pargram success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        
        [SLUserDefault removeUserInfo];
        ApplicationDelegate.userId = data[@"result"][@"id"];
        NSString *nameAndPwd  = [NSString stringWithFormat:@"%@:%@",data[@"result"][@"id"], undecryptPassword];
        ApplicationDelegate.Basic = [nameAndPwd base64EncodedString];
        ApplicationDelegate.userInformation = [data[@"result"] mutableCopy];
        NSDictionary *newUserInfo = @{@"basic": ApplicationDelegate.Basic, @"userInformation": data[@"result"],@"Password": password};
        [SLUserDefault setUserInfo:newUserInfo];
    } failure:^(SLHttpRequestError *failure) {
        
    }];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [HUDManager hideHUDView];
//    if (self.pwdTextField.text !=nil) {
//        self.pwdTextField.text = @"";
//    }
}

// 验证账号和手机
- (BOOL)validateAccount:(NSString *)account password:(NSString *)password
{
    if ([account length] < 4 || [account length] > 16) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入正确用户名" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        [_userNmaeTextField becomeFirstResponder];
        self.loginBtn.enabled=YES;
        return NO;
    }
    if ([password length] < 6 || [password length] > 18) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入正确密码" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        [_pwdTextField becomeFirstResponder];
        self.loginBtn.enabled=YES;
        return NO;
    }
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.userNmaeTextField) {
        [self.pwdTextField becomeFirstResponder];
    }else
    {
        [self loginbtnClick:_loginBtn];
    }
    return YES;
}
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)even
{
    [self.view endEditing:YES];
}
//明暗文
- (IBAction)darkToBride:(id)sender {
    _darkToBride.selected = !_darkToBride.selected;
    [self.pwdTextField becomeFirstResponder];
    NSString* text = self.pwdTextField.text;
    self.pwdTextField.text = @" ";
    self.pwdTextField.text = text;
    self.pwdTextField.secureTextEntry = !_darkToBride.selected;
}
- (IBAction)foggetPassword:(id)sender {
    SLForgetPassWordViewController *foggetPassword = [[SLForgetPassWordViewController alloc] init];
    NSLog(@"%@",self.navigationController);
    [self.navigationController pushViewController:foggetPassword animated:YES];
}
-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
