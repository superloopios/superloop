//
//  SLFirstViewController.h
//  Superloop
//
//  Created by WangS on 16/6/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLFirstViewController : UIViewController
@property(nonatomic,assign)BOOL isTabbarVC;
@property(nonatomic,assign)BOOL isReplacePasswordVC;

@end
