//  进入大家玩等后选择分类的界面
//  SLChooseCateViewController.m
//  Superloop
//
//  Created by administrator on 16/7/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLChooseCateViewController.h"
#import "SLAPIHelper.h"
#import "SLCategoryModel.h"
#import "AppDelegate.h"


#define ScreenW [UIScreen mainScreen].bounds.size.width

@interface SLChooseCateViewController ()
@property (nonatomic,copy)NSString *dateStr;
@property (nonatomic, strong) NSArray *categorys;
@property (nonatomic, strong)SLCategoryModel *cateModel;
@end

@implementation SLChooseCateViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLChooseCateViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [HUDManager hideHUDView];
    [MobClick endLogPageView:@"SLChooseCateViewController"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self setNav];
//    self.title = self.navTitle;
    [self setUpNav];
    self.view.backgroundColor = SLColor(240, 240, 240);
    
    [self loadData];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=self.navTitle;
    
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)loadData
{
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow withText:@""];
    NSData *categoryData=[[NSUserDefaults standardUserDefaults] objectForKey:@"categoryKey"];
    if (categoryData) {
        _dateStr=[NSKeyedUnarchiver unarchiveObjectWithData:categoryData][@"dateStr"];
    }
    if ([SLValueUtils  periodTwoHours:_dateStr]) {
        NSDictionary *configDict = [NSKeyedUnarchiver unarchiveObjectWithData:categoryData];
        _categorys = [SLCategoryModel mj_objectArrayWithKeyValuesArray:configDict[@"configData"][@"result"]];
        [HUDManager hideHUDView];
        for (SLCategoryModel *model in _categorys) {
            if ([self.navTitle isEqualToString:model.name]) {
                self.cateModel = model;
                [self makeUI];
                break;
            }
        }
    }else{
        
        [SLAPIHelper getAllTagsValue:nil success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            // NSLog(@"%@", data[@"result"]);
            [HUDManager hideHUDView];
            _categorys = [SLCategoryModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            //缓存数据
            NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];
            NSTimeInterval timeStamp=[date timeIntervalSince1970]*1000;
            NSString *timeStampStr=[[NSString stringWithFormat:@"%f",timeStamp] componentsSeparatedByString:@"."][0];
            NSDictionary *dataWithTime=@{@"dateStr":timeStampStr,@"configData":data};
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:dataWithTime];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:@"categoryKey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            for (SLCategoryModel *model in _categorys) {
                if ([self.navTitle isEqualToString:model.name]) {
                    self.cateModel = model;
                    [self makeUI];
                    break;
                }
            }

        } failure:^(SLHttpRequestError *failure) {
            [HUDManager hideHUDView];
        }];
    }
    
}
- (void)makeUI{
    int totalloc=4;
    CGFloat btnw=(screen_W-3)/totalloc;;
    CGFloat btnh=50;
    CGFloat margin=1;
    NSInteger count = self.cateModel.children.count;
    NSDictionary *dict;
    for (int i=0; i<count; i++) {
        dict = self.cateModel.children[i];
        int row=i/totalloc;//行号
        int loc=i%totalloc;//列号
        CGFloat btnx=margin+(margin+btnw)*loc;
        CGFloat btny=margin+(margin+btnh)*row+64;
        //创建uiview控件
        UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(btnx, btny , btnw, btnh)];
        btn.backgroundColor = [UIColor whiteColor];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:12.5];
        btn.titleLabel.numberOfLines = 0;
        btn.titleLabel.textAlignment = NSTextAlignmentCenter;
        btn.tag = i;
        [btn setTitle:dict[@"name"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
}
- (void)btnClick:(UIButton *)btn{
    if (self.changeTitleAndUpdate) {
        NSDictionary *dict = self.cateModel.children[btn.tag];
        NSString* headerData=btn.titleLabel.text;
        headerData = [headerData stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
        headerData = [headerData stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        headerData = [headerData stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        self.changeTitleAndUpdate(headerData,dict[@"id"]);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            [self addNetWorkViews:@"世界上最遥远的距离就是－－妹有网"];
            [HUDManager hideHUDView];
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            if (_noNetWorkView) {
                [_noNetWorkView removeFromSuperview];
            }
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            if (_noNetWorkView) {
                [_noNetWorkView removeFromSuperview];
            }
        }
    }
}
// 没有网的制空页面
-(void)addNetWorkViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, ScreenW , ScreenH-64 )];
    
    UILabel *tiplabel = [[UILabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    //    [AttributedStr addAttribute:NSForegroundColorAttributeName
    //
    //                          value:SLColor(91, 133, 189)
    //
    //                          range:NSMakeRange(2, 2)];
    
    tiplabel.attributedText = AttributedStr;
    
    
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    _noNetWorkView = contentView;
    [self.view addSubview:contentView];
    
}


@end
