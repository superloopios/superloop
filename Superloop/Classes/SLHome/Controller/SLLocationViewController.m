//
//  SLLocationViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/2.
//  Copyright © 2016年 Superloop. All rights reserved.

#import "SLLocationViewController.h"
#import "SLProvince.h"
#import "SLCollectionCell.h"
#import <MJExtension.h>
#import "SLSelectCityViewController.h"
#import "SLCollectionItem.h"
#import "SLTabBarViewController.h"
#import "SLChildProvince.h"
#import "HUDManager.h"
#import "SLMineBasicInfoViewController.h"
#import "UILabel+StringFrame.h"
#import "SLCityBtn.h"

@interface SLLocationViewController ()<UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,SLSelectCityViewControllerDelegate,UITextFieldDelegate>
/** 所有的省份组数据 */
@property (nonatomic, strong) NSArray *modelGroups;
//每一组省份的数据
@property (nonatomic, strong) NSArray *childProvince;
//城市的数据
@property (nonatomic, strong) NSArray *cityList;
/** 所有热门城市数据 */
@property (nonatomic ,strong) NSArray *squareList;
//定义一个流水布局控件
@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSDictionary *dicts;
@property (nonatomic, strong) UIButton *btnCity;
@property (nonatomic,strong)UIButton *btnLocation;
//暂时的模糊图
@property(nonatomic,strong)UIView *buttomView;
@property (nonatomic,strong)NSString *cityName;
@property (nonatomic,strong)NSString *cityId;
@property (nonatomic,strong)NSString *cityCode; //定位城市的code
@property (nonatomic, strong)UITableView *tableView;

@end
@implementation SLLocationViewController

NSString *SLID = @"City";
static NSString *const IDD = @"collectionCell";
static NSInteger const cols = 4;
static CGFloat const margin = 10;
#define cellW (self.view.width - (cols - 1) * margin - 30) / cols
#define cellH 30
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
         [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:SLID];
//        if (self.isMineExperienceLocation||self.isMineBasicInfoLocation) {
//        _tableView.frame = CGRectMake(0, 44, screen_W, screen_H - 64);
//        }else{
//        _tableView.frame = CGRectMake(0, 0 screen_W, screen_H - 64);
        _tableView.frame=CGRectMake(0, 64, ScreenW, ScreenH-64);
//        }
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    if (self.isMineBasicInfoLocation || self.isMineExperienceLocation) {
//        [self.navigationController setNavigationBarHidden:YES animated:NO];
//    }else{
        [self.navigationController setNavigationBarHidden:YES animated:NO];
//    }
    [MobClick beginLogPageView:@"SLLocationViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLLocationViewController"];
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
//    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(30, 27, ScreenW-80, 30)];
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"选择城市";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.view.backgroundColor=[UIColor whiteColor];
    [self setUpNav];
    [self setUpHotCity];
    self.tableView.scrollIndicatorInsets = self.tableView.contentInset;
//    self.navigationItem.title = @"选择城市";
//    UIBarButtonItem *backItem=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_black"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
//    self.hidesBottomBarWhenPushed = YES;
//    self.navigationItem.leftBarButtonItem = backItem;
    //添加ui
    [self addUI];
    if (self.isHomeLocation) {
        //暂无此功能
        [self addNoCityView];
    }
    if (self.isMineBasicInfoLocation||self.isMineExperienceLocation) {
        [self setUpNav];
    }
    [self loadData];
    // 设置右边索引文字的颜色
    self.tableView.sectionIndexColor = SLColor(178, 177, 177);
    // 设置右边索引文字的背景色
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUpHotCity
{
    // 1.创建流水布局
    UICollectionViewFlowLayout *layout =  [[UICollectionViewFlowLayout alloc] init];
    // 2.设置流水布局属性
    // 2.1设置cell尺寸
    layout.itemSize = CGSizeMake(cellW, cellH);
    layout.sectionInset = UIEdgeInsetsMake(15, 15, 0, 15);
    // 2.2 设置cell之间间距
    layout.minimumInteritemSpacing = margin;
    layout.minimumLineSpacing = margin;
    // 2创建UICollectionView
        UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 100) collectionViewLayout:layout];
        // 2.设置数据源 -> 要UICollectionView展示数据
        collectionView.dataSource = self;
        collectionView.delegate = self;
        // 3. 注册cell
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SLCollectionCell class]) bundle:nil] forCellWithReuseIdentifier:IDD];
        // 4.不允许collectionView滚动
        // 取消弹簧效果
        //collectionView.bounces = NO;
        collectionView.scrollEnabled = NO;
        
        collectionView.backgroundColor = self.tableView.backgroundColor;
    
    _collectionView = collectionView;

}

- (void)loadData
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"AreaProvince" ofType:@"json"];
    NSData *data = [[NSData alloc] initWithContentsOfFile:filePath];
    NSArray *procincelArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    NSLog(@"------数组是%@",procincelArray);

    self.modelGroups = [SLProvince mj_objectArrayWithKeyValuesArray:procincelArray];
    NSString *filePath1 = [[NSBundle mainBundle] pathForResource:@"HotCity" ofType:@"json"];
    NSData *data1 = [[NSData alloc] initWithContentsOfFile:filePath1];
    NSArray *hotArray = [NSJSONSerialization JSONObjectWithData:data1 options:kNilOptions error:nil];
    self.squareList = [SLCollectionItem mj_objectArrayWithKeyValuesArray:hotArray];
    [self.tableView reloadData];
    
    NSLog(@"--%@",self.modelGroups);
    
}

- (void)addUI
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = SLColor(242, 242, 242);
    
//    //添加搜索框
//    UITextField *search = [[UITextField alloc] init];
//    search.x = 20;
//    search.y = 10;
//    search.width = self.view.width - 40;
//    search.height = 30;
//    search.backgroundColor = [UIColor whiteColor];
//    search.borderStyle = UITextBorderStyleRoundedRect;
//    search.placeholder = @"输入城市名";
//    search.textAlignment = NSTextAlignmentCenter;
//    search.delegate = self;
//    search.clearButtonMode = UITextFieldViewModeWhileEditing;
//    [view addSubview:search];
    //添加定位城市
    
    SLCityBtn *btnCity = [[SLCityBtn alloc] init];
    [btnCity setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnCity.titleLabel.textAlignment = NSTextAlignmentLeft;
    [btnCity sizeToFit];
    btnCity.backgroundColor = [UIColor whiteColor];
    [btnCity.titleLabel setFont:[UIFont systemFontOfSize:14]];
    
    CGFloat width = [btnCity.titleLabel boundingRectWithString:btnCity.titleLabel.text withSize:CGSizeMake(MAXFLOAT, 0)].width;
    
//    btnCity.frame = CGRectMake(40, 40, self.view.width + width, 40);
    btnCity.frame = CGRectMake(40, 0, self.view.width + width, 40);
    UIView *vw = [[UIView alloc] init];
    vw.frame = CGRectMake(0, 0, self.view.width, 40);
    vw.backgroundColor = [UIColor whiteColor];
    [vw addSubview:btnCity];
    [view addSubview:vw];
    [btnCity addTarget:self action:@selector(selectLocation) forControlEvents:UIControlEventTouchUpInside];
    //处理一下正在定位的城市
    self.btnCity = btnCity;
    if (self.labelCityStr.length > 0) {
        self.btnLocation.hidden = YES;
    }
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    NSString *city =  [userDefault objectForKey:@"location"];
    NSString *errorStr = [userDefault objectForKey:@"faiure"]; //失败字符串处理
    NSString *colseLocationStr = [userDefault objectForKey:@"closeLocation"]; //失败字符串处理
    NSString *strCity = nil;
    if (errorStr.length >0) {
        strCity = @"定位失败";
    }
    if (errorStr.length==0) {
        strCity = @"定位失败";
    }
    if (self.labelCityStr.length >0) {
        strCity = self.labelCityStr;
    }
    if (colseLocationStr.length >0) {
        strCity = @"请先开启定位";
    }
  
    if (city) {
        strCity = city;
    }
    [btnCity setTitle:[NSString stringWithFormat:@"%@",strCity] forState:UIControlStateNormal];
    [view addSubview:btnCity];
    
    //添加一个正在城市按钮
    UIView *ploceHoldView = [[UIView alloc] init];
    ploceHoldView.backgroundColor = [UIColor whiteColor];
    [view addSubview:ploceHoldView];
    ploceHoldView.frame = CGRectMake(0, 0, 40, 40);
    UIButton *btn = [[UIButton alloc] init];
    self.btnLocation = btn;
    [btn setImage:[UIImage imageNamed:@"Map-pointer"] forState:UIControlStateNormal];
    btn.backgroundColor =[UIColor whiteColor];
    btn.frame = CGRectMake(20, 12, 12, 15);
    [ploceHoldView addSubview:btn];
    
    
//    //添加一个label
//    UILabel *timeLabel = [[UILabel alloc] init];
//    timeLabel.textAlignment = NSTextAlignmentLeft;
//    timeLabel.backgroundColor = [UIColor clearColor];
//    timeLabel.font = [UIFont systemFontOfSize:14];
//    timeLabel.frame = CGRectMake(0, 90, self.view.width, 40);
//    timeLabel.text = @"  最近访问城市";
//    [view addSubview:timeLabel];
    //添加城市
    UIView *cityView = [[UIView alloc] init];
    cityView.backgroundColor = [UIColor whiteColor];
    cityView.frame = CGRectMake(0, 130, self.view.width, 60);
    [view addSubview:cityView];
    //添加一个label
    UILabel *hotLabel = [[UILabel alloc] init];
    hotLabel.textAlignment = NSTextAlignmentLeft;
    hotLabel.backgroundColor = [UIColor clearColor];
    hotLabel.font = [UIFont systemFontOfSize:14];
    hotLabel.frame = CGRectMake(0, 40, self.view.width, 40);
    hotLabel.text = @"  热门城市";
    [view addSubview:hotLabel];
    //添加城市
    UIView *hotView = [[UIView alloc] init];
    UICollectionView *collView = self.collectionView;
    hotView.frame = CGRectMake(0, 80, self.view.width, 100);
    collView.frame = hotView.bounds;
    [hotView addSubview:collView];
    [view addSubview:hotView];
    view.width = self.view.width;
    view.height = hotView.y + hotView.height+80;
    
    UILabel *xingLabel = [[UILabel alloc] init];
    xingLabel.text = @"*";
    xingLabel.textColor = [UIColor blackColor];
    xingLabel.font = [UIFont systemFontOfSize:20];
    xingLabel.frame = CGRectMake(15, hotView.y + hotView.height+20, 20, 20);
//    xingLabel.backgroundColor = [UIColor greenColor];
    [view addSubview:xingLabel];
    
    UIView *countryView = [[UIView alloc] init];
    countryView.backgroundColor = [UIColor whiteColor];
    countryView.frame = CGRectMake(0, 220, self.view.width, 40);
    [view addSubview:countryView];
    SLCityBtn *countryBtn = [[SLCityBtn alloc] init];
    [countryBtn setTitle:@"全国" forState:UIControlStateNormal];
    countryBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    [countryBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    countryBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    countryBtn.frame = CGRectMake(15, 0, self.view.width,40);
    [countryBtn addTarget:self action:@selector(selectBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [countryView addSubview:countryBtn];
    
    
    
    self.tableView.tableHeaderView = view;
    
}


#pragma mark -- 选择全国按钮的点击事件
- (void)selectBtnClick:(id)sender{
    NSString *city =  [NSString stringWithFormat:@"全国"];
    NSString *code = @"1";
    
    if (self.isMineBasicInfoLocation == YES ||self.isMineExperienceLocation == YES||self.isRegisterLocation == YES ||self.isMeLocation==YES) {
        return;
    }
    if (_valueWithCityIdBlcok) {
        _valueWithCityIdBlcok(city,code);
    };
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)selectLocation{
    //定位城市选择返回
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    NSString *city =  [userDefault objectForKey:@"location"];
//    NSString *code = [userDefault objectForKey:@"coded"];
    NSString *code = [userDefault objectForKey:@"code"];
    if (code.length>0) {
        if (_valueWithCityIdBlcok) {
            _valueWithCityIdBlcok(city,code);
        }
        if (_valueBlcok) {
            _valueBlcok(city);
        }
//        [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//            
//        }];
        
        [self.navigationController popViewControllerAnimated:YES];
    }  else if (code.length == 0 && city.length >0) {
//    if (code.length == 0 && city.length >0) {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"AreaProvince" ofType:@"json"];
        NSData *data = [[NSData alloc] initWithContentsOfFile:filePath];
        NSArray *districts = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSString *code = @"";
        
        for(NSDictionary * dist in districts){
            NSLog(@"%@",districts);
            NSArray *provinces = dist[@"provinces"];
            if(provinces.count == 0) continue;
            code = [self findLocationCityCode:city districts:provinces];
            
            if(code) break;
        }
        NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setObject:code forKey:@"code"];
        [userDefault synchronize];
        if (_valueWithCityIdBlcok) {
            _valueWithCityIdBlcok(city,code);
        }
        if (_valueBlcok) {
            _valueBlcok(city);
        }
//        [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//            
//        }];
        if(code.length>0){
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark - 找出定位城市的code
-(NSString *)findLocationCityCode:(NSString *)locationCity districts:(NSArray *)districts{
    if (districts.count == 0) return nil;
    NSLog(@"%@",districts);
    for(NSDictionary *dist in districts){
        NSString *name = dist[@"name"];
        if([name isEqualToString:locationCity]){
            return dist[@"code"];
        }else{
            NSLog(@"%@",dist[@"children"]);
            NSArray *children = dist[@"children"];
            if(children.count == 0)continue;
            return [self findLocationCityCode:locationCity districts:children];
        }
    }
    
    return nil;
}
/*
- (void)selectLocation{
    //定位城市选择返回
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    NSString *city =  [userDefault objectForKey:@"location"];
    NSString *code = [userDefault objectForKey:@"code"];
    if (code.length>0) {
        if (_valueWithCityIdBlcok) {
            _valueWithCityIdBlcok(city,code);
        }
        if (_valueBlcok) {
            _valueBlcok(city);
        }
        [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
            
        }];

        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if (code.length == 0 && city.length >0) {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"AreaProvince" ofType:@"json"];
        NSData *data = [[NSData alloc] initWithContentsOfFile:filePath];
        NSArray *districts = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSString *code = @"";
        
        for(NSDictionary * dist in districts){
            NSLog(@"%@",districts);
            NSArray *provinces = dist[@"provinces"];
            if(provinces.count == 0) continue;
            code = [self findLocationCityCode:city districts:provinces];
            
            if(code) break;
        }
//        NSLog(@"%@",code);
        if (_valueWithCityIdBlcok) {
            _valueWithCityIdBlcok(city,code);
        }
        if (_valueBlcok) {
            _valueBlcok(city);
        }
        [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
            
        }];
        if(code.length>0){
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark - 找出定位城市的code
-(NSString *)findLocationCityCode:(NSString *)locationCity districts:(NSArray *)districts{
    if (districts.count == 0) return nil;
    NSLog(@"%@",districts);
    for(NSDictionary *dist in districts){
        NSString *name = dist[@"name"];
        if([name isEqualToString:locationCity]){
            return dist[@"code"];
        }else{
            NSLog(@"%@",dist[@"children"]);
            NSArray *children = dist[@"children"];
            if(children.count == 0)continue;
            return [self findLocationCityCode:locationCity districts:children];
        }
    }
    
    return nil;
}

 */

-(void)addNoCityView
{
    
    _buttomView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH)];
    _buttomView.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    [[UIApplication sharedApplication].keyWindow addSubview:_buttomView];
    
    UIView *whiteView=[[UIView alloc] initWithFrame:CGRectMake((ScreenW-290)/2.0, (ScreenH-190)/2.0, 290, 190)];
    whiteView.backgroundColor=[UIColor whiteColor];
    //whiteView.layer.masksToBounds = YES;
    whiteView.layer.cornerRadius = 10.0;
    //whiteView.layer.borderWidth = 1.0;
    whiteView.layer.borderColor = [[UIColor whiteColor] CGColor];
    [_buttomView addSubview:whiteView];
    
    UILabel *lab1=[[UILabel alloc] initWithFrame:CGRectMake(0, 32, whiteView.frame.size.width, 14)];
    lab1.text=@"地区筛选功能即将上线";
    lab1.font=[UIFont systemFontOfSize:18];
    lab1.textAlignment=NSTextAlignmentCenter;
    [whiteView addSubview:lab1];
    
    UILabel *lab2=[[UILabel alloc] initWithFrame:CGRectMake(0, 56, whiteView.frame.size.width, 14)];
    lab2.text=@"敬请期待";
    lab2.font=[UIFont systemFontOfSize:18];
    lab2.textAlignment=NSTextAlignmentCenter;
    [whiteView addSubview:lab2];
    
    UIImageView *smileImg=[[UIImageView alloc] initWithFrame:CGRectMake(127, 95, 37, 26)];
    smileImg.image=[UIImage imageNamed:@"smile"];
    [whiteView addSubview:smileImg];
    
    UIView *hView=[[UIView alloc] initWithFrame:CGRectMake(0, 150, whiteView.frame.size.width, 1)];
    hView.backgroundColor=SLColor(177, 177, 177);
    [whiteView addSubview:hView];
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0, 163, whiteView.frame.size.width, 15);
    [btn setTitle:@"好" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.titleLabel.font=[UIFont systemFontOfSize:18];
    [btn addTarget:self action:@selector(sureBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [whiteView addSubview:btn];
    
   
}
-(void)sureBtnClick
{
    [_buttomView removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - <数据源>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.modelGroups.count;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CityList" ofType:@"json"];
    NSData *data = [[NSData alloc] initWithContentsOfFile:filePath];
    NSArray *dictArr = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

    
    NSDictionary *dict;
    BOOL flag = false;
    for (int i=0; i<dictArr.count; i++) {
           dict = dictArr[i];
        if ([textField.text isEqualToString:dict[@"CN"]] == true) {
            flag = true;
        }
    }
    if (!flag) {
        [HUDManager showWarningWithText:@"当前城市未开通!"];
    }
   
    return YES;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    SLProvince *province = self.modelGroups[section];
    return province.provinces.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SLID];
    SLProvince *province = self.modelGroups[indexPath.section];
    
    self.childProvince = [SLChildProvince mj_objectArrayWithKeyValuesArray:province.provinces];
    SLChildProvince *childModel = self.childProvince[indexPath.row];
    cell.textLabel.text = childModel.name;
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
//    SLCityGroup *group = self.cityGroups[section];
    SLProvince *province = self.modelGroups[section];
    return province.title;

}

/**  *  返回每一组的索引标题（数组中放的是字符串）
 */
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return [self.modelGroups valueForKeyPath:@"title"];
}

#pragma mark - <代理>
- (void)sendCityName:(NSString *)cityName
{
    if (_valueBlcok) {
        _valueBlcok(cityName);
    }
}

-(void)sendCityName:(NSString *)cityName cityId:(NSString *)cityId{
    if (_valueWithCityIdBlcok) {
    _valueWithCityIdBlcok(cityName,cityId);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SLSelectCityViewController *select = [[SLSelectCityViewController alloc] init];
    select.changeAddress = ^{
        [self.navigationController popViewControllerAnimated:YES];
    };
    select.Delegate = self;
    SLProvince *province = self.modelGroups[indexPath.section];
    self.childProvince = [SLChildProvince mj_objectArrayWithKeyValuesArray:province.provinces];
    SLChildProvince *childModel = self.childProvince[indexPath.row];
    if (childModel.children.count == 0) {
    
    NSString *name = province.provinces[indexPath.row][@"name"];
    NSString *nameId = province.provinces[indexPath.row][@"code"];
    [self sendCityName:name];
    [self sendCityName:name cityId:nameId];
    [self.navigationController popViewControllerAnimated:YES];
    return;
    }
    
    if (self.isMineBasicInfoLocation == YES) {
        select.isMineBasicInfoLocation = YES;
    }
    
    if (self.isMineExperienceLocation == YES) {
        
        select.isMineBasicInfoLocation = YES;
    }
    if (self.isRegisterLocation == YES) {
        
        select.isRegisterLocation = YES;
    }
    
    select.dataArray = childModel.children;
    select.tag = _tag;
    select.event = self.event;
//    [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//        
//    }];
    [self.navigationController pushViewController:select animated:YES];
}


#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.squareList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SLCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:IDD forIndexPath:indexPath];
    
    // 获取模型
    SLCollectionItem *item = self.squareList[indexPath.row];
//    cell.backgroundColor = [UIColor grayColor];
    cell.cellLabel.text = item.name;
    cell.layer.borderColor = [UIColor grayColor].CGColor;
    cell.layer.borderWidth = 0.5;
    cell.layer.cornerRadius = 5 ;
    return cell;
}

#pragma mark - UICollectionViewDelegate
// 监听collectionView点击
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SLCollectionItem *item = self.squareList[indexPath.row];
  
    if (_valueBlcok) {
       _valueBlcok(item.name);
       // _valueBlcok(item.CN,_tag);
    }
//    [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//        
//    }];
    if (_valueWithCityIdBlcok) {
         _valueWithCityIdBlcok(item.name,item.code);
    }
    if (_tag == 1000) {
//        [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//            
//        }];
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_tag == 2000)
    {
//        [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//            
//        }];

        [self.navigationController popViewControllerAnimated:YES];
    }else
    {
//        [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//            
//        }];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
