//
//  SLCatDetailViewController.m
//  Superloop
//
//  Created by 张梦川 on 16/5/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCatDetailViewController.h"
#import <MJRefresh/MJRefresh.h>
#import "AppDelegate.h"
#import "HomePageViewController.h"
#import "SLSearchUser.h"
#import "SLCateButton.h"
#import "SLSequenceBtn.h"
#import "SLLocationViewController.h"
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "SLAPIHelper.h"
#import "SLChooseCateViewController.h"
#import "SLUserView.h"
#import "SLUserTableViewCell.h"
#import "SLSegmentedControl.h"
#import "SLScrollView.h"
#import "SLCategoryModel.h"

@interface SLCatDetailViewController ()<UITableViewDelegate,SLUserViewDelegate,UIScrollViewDelegate,SLSegmentControlDelegate>{
    SLCateButton *_cateBtn;
    SLSegmentedControl *seg;
}
@property (nonatomic,strong) UIView *sequenceView;
@property (nonatomic,strong) UIView *sequenceHalfView;
@property (nonatomic,strong) NSArray *sequenceArray;
@property (nonatomic,assign) NSInteger btnTag;
@property (nonatomic,assign) NSInteger page;
@property (nonatomic,assign) NSInteger likePage;
@property (nonatomic,strong) UIView *selectView;
@property (nonatomic,assign) BOOL isFirstCome;
@property (nonatomic,strong) SLUserView *tagUserView;
@property (nonatomic,assign) NSInteger locationTag;
@property (nonatomic,strong) NSMutableDictionary *skilledPragrams;
@property (nonatomic,strong) NSMutableDictionary *likePragrams;
@property (nonatomic,strong) NSString *strTag;  //标记
@property (nonatomic,strong) SLScrollView *scrollview;
@property (nonatomic,strong) SLUserView *likeTagUserView;
@property (nonatomic,assign) BOOL isRefreshSkilledTag;
@property (nonatomic,assign) BOOL isRefreshLikeTag;
@property (nonatomic,assign) CGFloat lastContentOffset;
@property (nonatomic,copy) NSString *recordSkilledLocation;//标记擅长的地点
@property (nonatomic,copy) NSString *recordSkilledCategory;//标记擅长的分类
@property (nonatomic,copy) NSString *recordSkilledSequence;//标记擅长的排序
@property (nonatomic,strong) NSNumber *recordCategoryId;//标记分类id
@property (nonatomic,assign) BOOL isBtnClick;
@property (nonatomic,assign) BOOL hasCache;
@property (nonatomic, strong) NSArray *categorys;
//分类数量提醒
@property(nonatomic, strong)UILabel *tipLab;

@end

@implementation SLCatDetailViewController{
    
    UIButton *_sequenceBtn;
    UIButton *_placeBtn;
}
- (UILabel *)tipLab{
    if (!_tipLab) {
        _tipLab = [[UILabel alloc] init];
        _tipLab.font = [UIFont systemFontOfSize:12];
        _tipLab.backgroundColor = UIColorFromRGB(0xff5a5f);
        _tipLab.textColor = [UIColor whiteColor];
        _tipLab.textAlignment = NSTextAlignmentCenter;
        _tipLab.clipsToBounds = YES;
        _tipLab.layer.cornerRadius = 7;
    }
    return _tipLab;
}
-(SLScrollView *)scrollview{
    if (!_scrollview) {
        _scrollview = [[SLScrollView alloc] init];
        _scrollview.frame = CGRectMake(0, 64+40+43, ScreenW, ScreenH-64-83);
        _scrollview.pagingEnabled = YES;
        _scrollview.delegate = self;
        [_scrollview setContentSize:CGSizeMake(ScreenW*2, 0)];
        _scrollview.showsHorizontalScrollIndicator = NO;
        _scrollview.scrollsToTop=NO;
        _scrollview.bounces = NO;
        [self.view addSubview:_scrollview];
    }
    return _scrollview;
}
-(SLUserView *)tagUserView{
    if (!_tagUserView) {
        _tagUserView=[[SLUserView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH-64-80)];
        _tagUserView.delegate=self;
        [self.scrollview addSubview:_tagUserView];
    }
    return _tagUserView;
}
-(SLUserView *)likeTagUserView{
    if (!_likeTagUserView) {
        _likeTagUserView=[[SLUserView alloc] initWithFrame:CGRectMake(ScreenW, 0, ScreenW, ScreenH-64-80)];
        _likeTagUserView.delegate=self;
        [self.scrollview addSubview:_likeTagUserView];
    }
    return _likeTagUserView;
}
- (NSInteger)page{
    if (!_page) {
        _page = 0;
    }
    return _page;
}
- (NSInteger)likePage{
    if (!_likePage) {
        _likePage=0;
    }
    return _likePage;
}
-(NSMutableDictionary *)skilledPragrams{
    if (!_skilledPragrams) {
        _skilledPragrams=[NSMutableDictionary new];
    }
    return _skilledPragrams;
}
-(NSMutableDictionary *)likePragrams{
    if (!_likePragrams) {
        _likePragrams=[NSMutableDictionary new];
    }
    return _likePragrams;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [MobClick beginLogPageView:@"SLCatDetailViewController"];

}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLCatDetailViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=SLColor(240, 240, 240);
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setUpNav];
    [self setUpSegControl];
    
    _sequenceArray = @[@{@"icon":@"normal_option", @"name":@"默认排序"},@{@"icon":@"down_option", @"name":@"资费由高到低"},@{@"icon":@"up_option", @"name":@"资费由低到高"},@{@"icon":@"high_opinion", @"name":@"按好评度排序"}];
    [self addUI];
    [self addSequenceView];
    [self setUpRefresh];
    self.isRefreshLikeTag = YES;
    self.isRefreshSkilledTag = YES;
    [self getSkilledTagDatas];
    [self getNetWorkStatus];
    [self loadNumberOfCategory];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=self.navTitle;
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setUpSegControl{
    UIView *backgroupView=[[UIView alloc] initWithFrame:CGRectMake(0, 64, screen_W, 40)];
    backgroupView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:backgroupView];
    seg = [[SLSegmentedControl alloc] initWithFrame:CGRectMake(60, 0, screen_W-120, 40)];
    seg.delegate = self;
    seg.titleArr = @[@"擅长的",@"喜欢的"];
    [backgroupView addSubview:seg];
    
    UIImageView *lineImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 39.5, ScreenW, 0.5)];
    lineImgView.backgroundColor = SLSepatorColor;
    [backgroupView addSubview:lineImgView];
}
- (void)addUI{
    UIView *selectView = [[UIView alloc] init];
    selectView.frame = CGRectMake(0, 64+40, self.view.frame.size.width, 43);
    selectView.backgroundColor = [UIColor whiteColor];
    self.selectView = selectView;
    [self.view addSubview:selectView];
    
    SLCateButton *placeBtn = [SLCateButton buttonWithType:UIButtonTypeCustom];
    [placeBtn setTitle:@"地点" forState:UIControlStateNormal];
    self.recordSkilledLocation=@"地点";
    [placeBtn setImage:[UIImage imageNamed:@"Down_arrow"] forState:UIControlStateNormal];
    [placeBtn addTarget:self action:@selector(selPlace) forControlEvents:UIControlEventTouchUpInside];
    _placeBtn = placeBtn;
    [selectView addSubview:placeBtn];
    
    UIImageView *cutLeftImage = [[UIImageView alloc] init];
    cutLeftImage.backgroundColor = [UIColor lightGrayColor];
    [selectView addSubview:cutLeftImage];
    
    _cateBtn = [SLCateButton buttonWithType:UIButtonTypeCustom];
    if (self.cateTitle) {
        [_cateBtn setTitle:self.cateTitle forState:UIControlStateNormal];
        self.recordSkilledCategory=self.cateTitle;
    }else{
        [_cateBtn setTitle:@"分类" forState:UIControlStateNormal];
        self.recordSkilledCategory=@"分类";
    }
    [_cateBtn setImage:[UIImage imageNamed:@"Down_arrow"] forState:UIControlStateNormal];
    [_cateBtn addTarget:self action:@selector(selectCategray) forControlEvents:UIControlEventTouchUpInside];
    [selectView addSubview:_cateBtn];
    
    UIImageView *cutImage = [[UIImageView alloc] init];
    cutImage.backgroundColor = [UIColor lightGrayColor];
    [selectView addSubview:cutImage];
    
    SLCateButton *sequenceBtn = [SLCateButton buttonWithType:UIButtonTypeCustom];
    [sequenceBtn setTitle:@"排序" forState:UIControlStateNormal];
    self.recordSkilledSequence=@"排序";

    [sequenceBtn setImage:[UIImage imageNamed:@"Down_arrow"] forState:UIControlStateNormal];
    [sequenceBtn addTarget:self action:@selector(sequenceTableView) forControlEvents:UIControlEventTouchUpInside];
    _sequenceBtn = sequenceBtn;
    [selectView addSubview:sequenceBtn];
    
    UIImageView *HcutImage = [[UIImageView alloc] init];
    HcutImage.backgroundColor = SLSepatorColor;
    [selectView addSubview:HcutImage];
    [self.view addSubview:selectView];
    
    CGFloat btnWidth = screen_W/3.0;
    [cutImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(selectView.mas_top).offset(8);
        make.left.mas_equalTo(selectView.mas_left).offset(btnWidth);
        make.width.mas_equalTo(0.5);
        make.bottom.mas_equalTo(selectView.mas_bottom).offset(-8);
    }];
    
    [cutLeftImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(selectView.mas_top).offset(8);
        make.right.mas_equalTo(selectView.mas_right).offset(-btnWidth);
        make.width.mas_equalTo(0.5);
        make.bottom.mas_equalTo(selectView.mas_bottom).offset(-8);
    }];
    
    [placeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(selectView.mas_left);
        make.right.mas_equalTo(cutImage.mas_left);
        make.top.mas_equalTo(selectView.mas_top);
        make.bottom.mas_equalTo(selectView.mas_bottom);
    }];
    
    [sequenceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(cutLeftImage.mas_right);
        make.right.mas_equalTo(selectView.mas_right);
        make.top.mas_equalTo(selectView.mas_top);
        make.bottom.mas_equalTo(selectView.mas_bottom);
        
    }];
    
    [_cateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(cutImage.mas_right);
        make.right.mas_equalTo(cutLeftImage.mas_left);
        make.top.mas_equalTo(selectView.mas_top);
        make.bottom.mas_equalTo(selectView.mas_bottom);
    }];
    _cateBtn.titleEdgeInsets = UIEdgeInsetsMake(2, 5, 2, 5);
    
    [HcutImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(selectView.mas_left);
        make.right.mas_equalTo(selectView.mas_right);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(selectView.mas_bottom).offset(-0.5);
    }];
    
    [_cateBtn addSubview:self.tipLab];
    CGFloat cateBtnWidth = [_cateBtn.titleLabel.text boundingRectWithSize:CGSizeMake(ScreenW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.width+2;
    if (cateBtnWidth > screen_W/3 - 50) {
        cateBtnWidth = screen_W/3 -50;
    }
//
    CGFloat catex = (btnWidth-cateBtnWidth-10-10)/2.0;

    [self.tipLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_cateBtn.mas_left).offset(catex+cateBtnWidth-5);
        make.bottom.equalTo(_cateBtn.titleLabel.mas_top).offset(3);
        make.width.mas_equalTo(22);
        make.height.mas_equalTo(14);
    }];
    self.tipLab.hidden = YES;
}
-(void)addSequenceView{
    //侧滑UI
    _sequenceView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH)];
    _sequenceView.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _sequenceView.hidden=YES;
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(touch:)];
    [_sequenceView addGestureRecognizer:tapGesture];
    [[UIApplication sharedApplication].keyWindow addSubview:_sequenceView];
    
    _sequenceHalfView=[[UIView alloc] initWithFrame:CGRectMake(ScreenW, 0, ScreenW/2, ScreenH)];
    _sequenceHalfView.backgroundColor=[UIColor whiteColor];
    [_sequenceView addSubview:_sequenceHalfView];
    
    UILabel *sequenLab=[[UILabel alloc] init];
    sequenLab.text=@"用户排序";
    sequenLab.textColor=SLColor(185, 185, 185);
    sequenLab.textAlignment=NSTextAlignmentCenter;
    sequenLab.font=[UIFont systemFontOfSize:15];
    [_sequenceHalfView addSubview:sequenLab];
    
    [sequenLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_sequenceHalfView.mas_top).offset(75);
        make.centerX.mas_equalTo(_sequenceHalfView.mas_centerX);
        make.height.mas_equalTo(15);
        
    }];
    
    for (int i=0; i<_sequenceArray.count; i++) {
        
        NSDictionary *dict=_sequenceArray[i];
        SLSequenceBtn *btn=[SLSequenceBtn buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(0, 150+i*50, ScreenW/2, 50);
        btn.tag=600+i;
        [btn addTarget:self action:@selector(sequenceBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.imgIcon.image=[UIImage imageNamed:dict[@"icon"]];
        btn.textLab.text=dict[@"name"];
        btn.topLine.image=[UIImage imageNamed:@"searchHline"];
        if (i==_sequenceArray.count-1) {
            btn.bottomLine.image=[UIImage imageNamed:@"searchHline"];
        }
        [_sequenceHalfView addSubview:btn];
    }
}
- (void)setUpRefresh{
    self.tagUserView.userTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    self.tagUserView.userTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadRefreshMoreData)];
    
    self.likeTagUserView.userTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    self.likeTagUserView.userTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadRefreshMoreData)];
}
- (void)selectCategray{
    SLChooseCateViewController *catagory = [[SLChooseCateViewController alloc] init];
    catagory.navTitle = self.navTitle;
    catagory.changeTitleAndUpdate = ^(NSString *title,NSNumber *cateId){
        //self.catId = cateId;
        self.recordCategoryId = cateId;
        self.recordSkilledCategory=title;
        [_cateBtn setTitle:self.recordSkilledCategory forState:UIControlStateNormal];
        CGFloat cateBtnWidth = [_cateBtn.titleLabel.text boundingRectWithSize:CGSizeMake(ScreenW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.width+2;
        if (cateBtnWidth > screen_W/3 - 50) {
            cateBtnWidth = screen_W/3 -50;
        }
        //
        CGFloat catex = (screen_W/3.0-cateBtnWidth-10-10)/2.0;
        
        [self.tipLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_cateBtn.mas_left).offset(catex+cateBtnWidth-5);
            make.bottom.equalTo(_cateBtn.titleLabel.mas_top).offset(3);
            make.width.mas_equalTo(22);
            make.height.mas_equalTo(14);
        }];
        if (seg.selectedSegmentIndex==0) {
            self.isRefreshLikeTag = YES;
            [self.tagUserView.userTableView.mj_header beginRefreshing];
            [self.tagUserView.userTableView.mj_footer setHidden:YES];
        }else{
            self.isRefreshSkilledTag = YES;
            [self.likeTagUserView.userTableView.mj_header beginRefreshing];
            [self.likeTagUserView.userTableView.mj_footer setHidden:YES];
        }
    };
    [self.navigationController pushViewController:catagory animated:YES];
}
//定位
- (void)selPlace{
    SLLocationViewController *location = [[SLLocationViewController alloc] init];
    location.tag = 1000;
    location.valueBlcok = ^(NSString *str){
        self.recordSkilledLocation=str;
        [_placeBtn setTitle:self.recordSkilledLocation forState:UIControlStateNormal];
        
    };
    
    location.valueWithCityIdBlcok = ^(NSString *str,NSString *locationTag){
        [MobClick event:@"filter_district"];
        if (self.recordSkilledLocation) {
            self.recordSkilledLocation=str;
            [_placeBtn setTitle:self.recordSkilledLocation forState:UIControlStateNormal];
        }
        _locationTag = [locationTag integerValue];
        self.strTag = [NSString stringWithFormat:@"%@",locationTag];
        [self.skilledPragrams setObject:@(_locationTag) forKey:@"location_code"];
        [self.skilledPragrams setObject:@(_locationTag) forKey:@"location_code"];
        if (_locationTag == 1) {
            [self.skilledPragrams removeObjectForKey:@"location_code"];
            [self.skilledPragrams removeObjectForKey:@"location_code"];
        }
        [self.likePragrams setObject:@(_locationTag) forKey:@"location_code"];
        [self.likePragrams setObject:@(_locationTag) forKey:@"location_code"];
        if (_locationTag == 1) {
            [self.likePragrams removeObjectForKey:@"location_code"];
            [self.likePragrams removeObjectForKey:@"location_code"];
        }
        if (seg.selectedSegmentIndex==0) {
            self.isRefreshLikeTag = YES;
            [self.tagUserView.userTableView.mj_header beginRefreshing];
            [self.tagUserView.userTableView.mj_footer setHidden:YES];
            
        }else{
            self.isRefreshSkilledTag = YES;
            [self.likeTagUserView.userTableView.mj_header beginRefreshing];
            [self.likeTagUserView.userTableView.mj_footer setHidden:YES];
        }
        
    };
    
    [self.navigationController pushViewController:location animated:YES];
    if (seg.selectedSegmentIndex==0) {
        [self.tagUserView.userTableView reloadData];
    }else{
        [self.likeTagUserView.userTableView reloadData];
    }
}

-(void)getSkilledTagDatas{
    [self getCacheData];
    if (self.isRefreshSkilledTag) {
        [self.tagUserView.userTableView.mj_header beginRefreshing];
        [self.tagUserView.userTableView.mj_footer setHidden:YES];
        self.isRefreshSkilledTag=NO;
    }
    self.tagUserView.userTableView.scrollsToTop=YES;
    self.likeTagUserView.userTableView.scrollsToTop=NO;
    [self.scrollview setContentOffset:CGPointMake(0, 0) animated:YES];
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.lastContentOffset = scrollView.contentOffset.x;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.x > self.lastContentOffset) {
        if (self.lastContentOffset==0) {
            if (!self.hasCache) {
//                [self getCacheDatas:1];
            }
        }
    }
    CGFloat destWidth;
    CGFloat currentWidth = [seg.titleWidthArr[seg.selectedSegmentIndex] doubleValue]+4;
    
    if ((scrollView.contentOffset.x - self.lastContentOffset)>0.0) {
        NSInteger index = seg.selectedSegmentIndex+1;
        if(index<seg.titleWidthArr.count){
            destWidth = [seg.titleWidthArr[index] doubleValue]+4;
            seg.titleBottomView.width = (scrollView.contentOffset.x-self.lastContentOffset)/screen_W*(destWidth-currentWidth)+currentWidth;
        }
        
    }else{
        NSInteger index = seg.selectedSegmentIndex-1;
        if(index>-1){
            destWidth = [seg.titleWidthArr[index] doubleValue]+4;
            seg.titleBottomView.width = (self.lastContentOffset-scrollView.contentOffset.x)/screen_W*(destWidth-currentWidth)+currentWidth;
        }
    }
    seg.titleBottomView.centerX = seg.startLoc+scrollView.contentOffset.x/screen_W*((ScreenW-120)/2);
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{

    int index = self.scrollview.contentOffset.x / self.view.width;
    [seg setSegmentControlIndex:index];
    [_placeBtn setTitle:self.recordSkilledLocation forState:UIControlStateNormal];
    [_cateBtn setTitle:self.recordSkilledCategory forState:UIControlStateNormal];
    [_sequenceBtn setTitle:self.recordSkilledSequence forState:UIControlStateNormal];
    if (seg.selectedSegmentIndex==0) {
//        [self getCacheData];

        if (self.isRefreshSkilledTag) {
            
            [self.tagUserView.userTableView.mj_header beginRefreshing];
            [self.tagUserView.userTableView.mj_footer setHidden:YES];
        }
    }else{
//        [self getCacheData];

        if (self.isRefreshLikeTag) {
            
            [self.likeTagUserView.userTableView.mj_header beginRefreshing];
            [self.likeTagUserView.userTableView.mj_footer setHidden:YES];
            
        }
    }
    
    [seg setSegmentControlIndex:index];
}

-(void)slSegmentControlClickedButtonIndex:(NSInteger)index{
    if (index==0) {
        seg.selectedSegmentIndex = 0;
//        [self getCacheData];
        if (self.isRefreshSkilledTag) {
            [self.tagUserView.userTableView.mj_header beginRefreshing];
            [self.tagUserView.userTableView.mj_footer setHidden:YES];
            self.isRefreshSkilledTag=NO;
        }
        self.tagUserView.userTableView.scrollsToTop=YES;
        self.likeTagUserView.userTableView.scrollsToTop=NO;
    }else{
        seg.selectedSegmentIndex = 1;
//        [self getCacheData];
        if (self.isRefreshLikeTag) {
            [self.likeTagUserView.userTableView.mj_header beginRefreshing];
            [self.likeTagUserView.userTableView.mj_footer setHidden:YES];
            self.isRefreshLikeTag=NO;
        }
        self.likeTagUserView.userTableView.scrollsToTop=YES;
        self.tagUserView.userTableView.scrollsToTop=NO;
    }
    [self.scrollview setContentOffset:CGPointMake(ScreenW*index, 0) animated:YES];
    self.lastContentOffset = self.scrollview.contentOffset.x;
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [self scrollViewDidEndDecelerating:scrollView];
}
-(void)getCacheDatas:(NSInteger)index{
    if (index==0) {
        seg.selectedSegmentIndex=0;
    }else{
        seg.selectedSegmentIndex=1;
    }
    [self getCacheData];
}
- (void)getCacheData{
    [_placeBtn setTitle:self.recordSkilledLocation forState:UIControlStateNormal];
    [_cateBtn setTitle:self.recordSkilledCategory forState:UIControlStateNormal];
    [_sequenceBtn setTitle:self.recordSkilledSequence forState:UIControlStateNormal];
//    if (seg.selectedSegmentIndex==0) {
    
        //获取旧的数据
        NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@",self.catId]];
        
        if (configData) {
            NSDictionary *configDict = nil;
            configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
            NSArray * arrlist = [SLSearchUser mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
            NSLog(@"arrlist-------%@",configDict[@"result"]);
            self.tagUserView.userDataSouresArr = (NSMutableArray *)arrlist;
            [self.tagUserView.userTableView reloadData];
//            
//            if (arrlist.count<20) {
//                [self.tagUserView.userTableView.mj_header endRefreshing];
//                [self.tagUserView.userTableView.mj_footer endRefreshingWithNoMoreData];
//            }else{
//                [self.tagUserView.userTableView.mj_header endRefreshing];
//                [self.tagUserView.userTableView.mj_footer resetNoMoreData];
//            }
        }
//    }else{
        //获取旧的数据
        NSData *configData1 =[NSData data];
        configData1 = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@likeCategory",self.catId]];
        if (configData1) {
            self.hasCache = YES;
            NSDictionary *configDict = nil;
            configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData1];
            NSArray * arrlist = [SLSearchUser mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
            NSLog(@"likeTagUserView-------%@",configDict[@"result"]);
            self.likeTagUserView.userDataSouresArr = (NSMutableArray *)arrlist;
            [self.likeTagUserView.userTableView reloadData];
            
//            if (arrlist.count<20) {
//                [self.likeTagUserView.userTableView.mj_header endRefreshing];
//                [self.likeTagUserView.userTableView.mj_footer endRefreshingWithNoMoreData];
//            }else{
//                [self.likeTagUserView.userTableView.mj_header endRefreshing];
//                [self.likeTagUserView.userTableView.mj_footer resetNoMoreData];
//            }
        }else{
//            [self.likeTagUserView.userTableView.mj_header beginRefreshing];
//            [self.likeTagUserView.userTableView.mj_footer setHidden:YES];
        }
//    }
}

#pragma mark --- 下拉加载
-(void)loadData{
    if (seg.selectedSegmentIndex==0) {
        self.page = 0;
        NSNumber *type = @1;
        [self.skilledPragrams setObject:type forKey:@"query_type"];
        if (self.recordCategoryId) {
            [self.skilledPragrams setObject:[NSString stringWithFormat:@"%@",self.recordCategoryId] forKey:@"tag_id"];
        }else{
            [self.skilledPragrams setObject:[NSString stringWithFormat:@"%@",self.catId] forKey:@"tag_id"];
        }
        [self.skilledPragrams setObject:[NSString stringWithFormat:@"%ld",(long)self.page] forKey:@"page"];
        [self.skilledPragrams setObject:@"20" forKey:@"pager"];
        [self.skilledPragrams setObject:@"1" forKey:@"tag_type"];
        
        if (_btnTag==0) {
            [self.skilledPragrams setObject:@(1) forKey:@"sort_by"];
            [self.skilledPragrams setObject:@(1) forKey:@"order_by"];
        }
        //资费高到低
        if (_btnTag==1) {
            [self.skilledPragrams setObject:@(1) forKey:@"sort_by"];
            [self.skilledPragrams setObject:@(2) forKey:@"order_by"];
        }
        //资费低到高
        if (_btnTag==2) {
            [self.skilledPragrams setObject:@(2) forKey:@"sort_by"];
            [self.skilledPragrams setObject:@(2) forKey:@"order_by"];
        }
        //按照评论排序
        if (_btnTag==3) {
            [self.skilledPragrams setObject:@(1) forKey:@"sort_by"];
            [self.skilledPragrams setObject:@(3) forKey:@"order_by"];
        }
        [SLAPIHelper searchUsers:self.skilledPragrams success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            
            NSArray *arrlist = [SLSearchUser mj_objectArrayWithKeyValuesArray:data[@"result"]];
            if (arrlist.count == 0) {
                if (!self.tagUserView.noDatasViews) {
                    [self.tagUserView addNoDatasViews:@"该分类暂无内容"];
                }
            }else{
                [self.tagUserView.noDatasViews removeFromSuperview];
            }
            self.tagUserView.userDataSouresArr = (NSMutableArray *)arrlist;
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:[NSString stringWithFormat:@"%@",self.catId]];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if (arrlist.count<20) {
                [self.tagUserView.userTableView.mj_footer endRefreshingWithNoMoreData];
            }else {
                [self.tagUserView.userTableView.mj_footer resetNoMoreData];
            }
            [self.tagUserView.userTableView.mj_header endRefreshing];
            [self.tagUserView.userTableView reloadData];
            [self.tagUserView.userTableView.mj_footer setHidden:NO];
            self.isRefreshSkilledTag=NO;

        } failure:^(SLHttpRequestError *failure) {
            [self.tagUserView.userTableView.mj_header endRefreshing];
            [HUDManager showWarningWithText:@"加载失败"];
        }];
        
    }else{
        self.likePage = 0;
        NSNumber *type = @1;
        [self.likePragrams setObject:type forKey:@"query_type"];
        if (self.recordCategoryId) {
            [self.likePragrams setObject:[NSString stringWithFormat:@"%@",self.recordCategoryId] forKey:@"tag_id"];
        }else{
            [self.likePragrams setObject:[NSString stringWithFormat:@"%@",self.catId] forKey:@"tag_id"];
        }
        [self.likePragrams setObject:[NSString stringWithFormat:@"%ld",(long)self.likePage] forKey:@"page"];
        [self.likePragrams setObject:@"20" forKey:@"pager"];
        [self.likePragrams setObject:@"2" forKey:@"tag_type"];
        
        if (_btnTag==0) {
            [self.likePragrams setObject:@(1) forKey:@"sort_by"];
            [self.likePragrams setObject:@(1) forKey:@"order_by"];
        }
        //资费高到低
        if (_btnTag==1) {
            [self.likePragrams setObject:@(1) forKey:@"sort_by"];
            [self.likePragrams setObject:@(2) forKey:@"order_by"];
        }
        //资费低到高
        if (_btnTag==2) {
            [self.likePragrams setObject:@(2) forKey:@"sort_by"];
            [self.likePragrams setObject:@(2) forKey:@"order_by"];
        }
        //按照评论排序
        if (_btnTag==3) {
            [self.likePragrams setObject:@(1) forKey:@"sort_by"];
            [self.likePragrams setObject:@(3) forKey:@"order_by"];
        }
        [SLAPIHelper searchUsers:self.likePragrams success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            NSLog(@"%@",data);
            NSArray *arrlist = [SLSearchUser mj_objectArrayWithKeyValuesArray:data[@"result"]];
            if (arrlist.count == 0) {
                if (!self.likeTagUserView.noDatasViews) {
                    [self.likeTagUserView addNoDatasViews:@"该分类暂无内容"];
                }
            }else{
                [self.likeTagUserView.noDatasViews removeFromSuperview];
            }
            self.likeTagUserView.userDataSouresArr = (NSMutableArray *)arrlist;
            
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:[NSString stringWithFormat:@"%@likeCategory",self.catId]];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if (arrlist.count<20) {
                [self.likeTagUserView.userTableView.mj_footer endRefreshingWithNoMoreData];
            }else {
                [self.likeTagUserView.userTableView.mj_footer resetNoMoreData];
            }
            [self.likeTagUserView.userTableView.mj_header endRefreshing];
            [self.likeTagUserView.userTableView reloadData];
            [self.likeTagUserView.userTableView.mj_footer setHidden:NO];
            self.isRefreshLikeTag=NO;
            
        } failure:^(SLHttpRequestError *failure) {
            [self.likeTagUserView.userTableView.mj_header endRefreshing];
            [HUDManager showWarningWithText:@"加载失败"];
        }];
    }
}
#pragma mark ------ 上拉加载
-(void)loadRefreshMoreData{
    
    if (seg.selectedSegmentIndex==0) {
        self.page ++;
        [self.skilledPragrams setObject:[NSString stringWithFormat:@"%ld",(long)self.page] forKey:@"page"];
        //默认
        if (_btnTag==0) {
            [self.skilledPragrams setObject:@(1) forKey:@"sort_by"];
            [self.skilledPragrams setObject:@(1) forKey:@"order_by"];
        }
        //资费高到低
        if (_btnTag==1) {
            [self.skilledPragrams setObject:@(1) forKey:@"sort_by"];
            [self.skilledPragrams setObject:@(2) forKey:@"order_by"];
        }
        //资费低到高
        if (_btnTag==2) {
            [self.skilledPragrams setObject:@(2) forKey:@"sort_by"];
            [self.skilledPragrams setObject:@(2) forKey:@"order_by"];
        }
        //按照评论排序
        if (_btnTag==3) {
            [self.skilledPragrams setObject:@(1) forKey:@"sort_by"];
            [self.skilledPragrams setObject:@(3) forKey:@"order_by"];
        }
        [SLAPIHelper searchUsers:self.skilledPragrams success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            
            NSArray *arrlist = [SLSearchUser mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [self.tagUserView.userDataSouresArr addObjectsFromArray:arrlist];
            if (arrlist.count<20) {
                [self.tagUserView.userTableView.mj_footer endRefreshingWithNoMoreData];
            }else {
                [self.tagUserView.userTableView.mj_footer endRefreshing];
            }
            [self.tagUserView.userTableView reloadData];
        } failure:^(SLHttpRequestError *failure) {
            [self.tagUserView.userTableView.mj_footer endRefreshing];
            self.tagUserView.userDataSouresArr = nil;
            [self.tagUserView.userTableView reloadData];
        }];
        
    }else{
        self.likePage ++;
        [self.likePragrams setObject:[NSString stringWithFormat:@"%ld",(long)self.likePage] forKey:@"page"];
        //默认
        if (_btnTag==0) {
            [self.likePragrams setObject:@(1) forKey:@"sort_by"];
            [self.likePragrams setObject:@(1) forKey:@"order_by"];
        }
        //资费高到低
        if (_btnTag==1) {
            [self.likePragrams setObject:@(1) forKey:@"sort_by"];
            [self.likePragrams setObject:@(2) forKey:@"order_by"];
        }
        //资费低到高
        if (_btnTag==2) {
            [self.likePragrams setObject:@(2) forKey:@"sort_by"];
            [self.likePragrams setObject:@(2) forKey:@"order_by"];
        }
        //按照评论排序
        if (_btnTag==3) {
            [self.likePragrams setObject:@(1) forKey:@"sort_by"];
            [self.likePragrams setObject:@(3) forKey:@"order_by"];
        }
        [SLAPIHelper searchUsers:self.likePragrams success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            NSArray *arrlist = [SLSearchUser mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [self.likeTagUserView.userDataSouresArr addObjectsFromArray:arrlist];
            if (arrlist.count<20) {
                [self.likeTagUserView.userTableView.mj_footer endRefreshingWithNoMoreData];
            }else {
                [self.likeTagUserView.userTableView.mj_footer endRefreshing];
            }
            [self.likeTagUserView.userTableView reloadData];
        } failure:^(SLHttpRequestError *failure) {
            [self.likeTagUserView.userTableView.mj_footer endRefreshing];
            self.likeTagUserView.userDataSouresArr = nil;
            [self.likeTagUserView.userTableView reloadData];
        }];
    }
}

//排序方式点击事件
-(void)sequenceBtnClick:(UIButton *)btnClick{
    _btnTag=btnClick.tag-600;
    if (self.recordSkilledSequence) {
        self.recordSkilledSequence=_sequenceArray[_btnTag][@"name"];
        [_sequenceBtn setTitle:self.recordSkilledSequence forState:UIControlStateNormal];
    }
    
    if (seg.selectedSegmentIndex==0) {
        if (self.strTag.length != 0) {
            [self.skilledPragrams setObject:@(_locationTag) forKey:@"location_code"];
            if (_locationTag == 1) {
                [self.skilledPragrams removeObjectForKey:@"location_code"];
            }
        }
        self.isRefreshLikeTag = YES;
        [self.tagUserView.userTableView.mj_header beginRefreshing];
        [self.tagUserView.userTableView.mj_footer setHidden:YES];
    }else{
        if (self.strTag.length != 0) {
            [self.likePragrams setObject:@(_locationTag) forKey:@"location_code"];
            if (_locationTag == 1) {
                [self.likePragrams removeObjectForKey:@"location_code"];
            }
        }
        self.isRefreshSkilledTag = YES;
        [self.likeTagUserView.userTableView.mj_header beginRefreshing];
        [self.likeTagUserView.userTableView.mj_footer setHidden:YES];
    }
    [self closeMenuView];
}
//点击排序
-(void)sequenceTableView{
    [self openMenuView];
}
//打开侧滑view
- (void)openMenuView{
    _sequenceView.hidden=NO;
    [UIView animateWithDuration:0.3 animations:^{
        _sequenceHalfView.frame=CGRectMake(ScreenW/2, 0, ScreenW/2, ScreenH);
    } completion:^(BOOL finished) {
        
    }];
}
//收起侧滑view
- (void)closeMenuView{
    [UIView animateWithDuration:0.3 animations:^{
        _sequenceHalfView.frame=CGRectMake(ScreenW, 0, ScreenW/2, ScreenH);
    } completion:^(BOOL finished) {
        _sequenceView.hidden=YES;
    }];
}

//点击收起侧滑
-(void)touch:(UIGestureRecognizer *)gesture{
    [self closeMenuView];
}

#pragma mark - SLTopicViewDelegate
-(void)SLUserView:(SLUserView *)baseView didSelectRowAtIndexPathWithModel:(SLSearchUser *)model{
    if ([[NSString stringWithFormat:@"%@", model.id] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        HomePageViewController *myView = [[HomePageViewController alloc] init];
        myView.userId=ApplicationDelegate.userId;
        [self.navigationController pushViewController:myView animated:YES];
    }else{
        HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
        otherPerson.userId=model.id;
        otherPerson.didClickFollowBtn = ^(UIButton *btn){
            model.following = btn.selected?@"1":@"0";
            NSInteger follower = [model.followers integerValue];
            if (btn.selected) {
                follower++;
            }else{
                follower--;
            }
            model.followers = [NSString stringWithFormat:@"%ld",(long)follower];
            //        NSLog(@"%@",NSStringFromClass([model.followers class]))
            [baseView.userTableView reloadData];
        };
        [self.navigationController pushViewController:otherPerson animated:YES];
    }
}

// 没有搜索到内容
-(void)addNotLoginViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_selectView.frame), ScreenW , ScreenH )];
    
    UILabel *tiplabel = [[UILabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    tiplabel.attributedText = AttributedStr;
    
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    _quitLoginView  = contentView;
    [self.view addSubview:contentView];
}

// 没有网的制空页面
-(void)addNetWorkViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_selectView.frame), ScreenW , ScreenH )];
    
    UILabel *tiplabel = [[UILabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    tiplabel.attributedText = AttributedStr;
    
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    _noNetWorkView = contentView;
    [self.view addSubview:contentView];
    
}

- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            [self addNetWorkViews:@"世界上最遥远的距离就是－－妹有网"];
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            if (_noNetWorkView) {
                [_noNetWorkView removeFromSuperview];
            }
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            if (_noNetWorkView) {
                [_noNetWorkView removeFromSuperview];
            }
        }
    }
}
- (void)loadNumberOfCategory
{
    NSData *categoryData=[[NSUserDefaults standardUserDefaults] objectForKey:@"categoryKey"];
    NSString *dateStr=@"";
    if (categoryData) {
        dateStr=[NSKeyedUnarchiver unarchiveObjectWithData:categoryData][@"dateStr"];
    }
    if ([SLValueUtils  periodTwoHours:dateStr]) {
        NSDictionary *configDict = [NSKeyedUnarchiver unarchiveObjectWithData:categoryData];
        _categorys = [SLCategoryModel mj_objectArrayWithKeyValuesArray:configDict[@"configData"][@"result"]];
        [HUDManager hideHUDView];
        for (SLCategoryModel *model in _categorys) {
            if ([self.navTitle isEqualToString:model.name]) {
                self.tipLab.hidden = NO;
                self.tipLab.text = [NSString stringWithFormat:@"%ld",(unsigned long)model.children.count];
                break;
            }
        }
    }else{
        
        [SLAPIHelper getAllTagsValue:nil success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            // NSLog(@"%@", data[@"result"]);
            [HUDManager hideHUDView];
            _categorys = [SLCategoryModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            //缓存数据
            NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];
            NSTimeInterval timeStamp=[date timeIntervalSince1970]*1000;
            NSString *timeStampStr=[[NSString stringWithFormat:@"%f",timeStamp] componentsSeparatedByString:@"."][0];
            NSDictionary *dataWithTime=@{@"dateStr":timeStampStr,@"configData":data};
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:dataWithTime];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:@"categoryKey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            for (SLCategoryModel *model in _categorys) {
                if ([self.navTitle isEqualToString:model.name]) {
                    self.tipLab.hidden = NO;
                    self.tipLab.text = [NSString stringWithFormat:@"%ld",model.children.count];
                    break;
                }
            }
            
        } failure:^(SLHttpRequestError *failure) {
            [HUDManager hideHUDView];
        }];
    }
}




@end
