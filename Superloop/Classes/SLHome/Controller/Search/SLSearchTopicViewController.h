//
//  SLSearchTopicViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SLBaseTopicViewController.h"

@class SLSearchTopicViewController;
@class SLTopicModel;

@protocol SLSearchTopicViewControllerDelegate <NSObject>

- (void)SLSearchTopicViewController:(NSArray *)data;

@end

@interface SLSearchTopicViewController : SLBaseTopicViewController

@property (nonatomic, weak)id <SLSearchTopicViewControllerDelegate>Delegate;

@property(nonatomic,copy)NSString *presentKeywords;//传过来的值


@property(nonatomic,assign)NSInteger  segIndex;


@property (strong, nonatomic) SLTopicModel *model;

@property (nonatomic,strong)NSMutableArray *mutPushNew;

@property (nonatomic,copy)void (^backRefreshBlock)(BOOL isRefresh);

@property (nonatomic,weak)UIView *quitLoginView;

@property (nonatomic,weak) UIView *noNetWorkView;  //没网制空页的view
@property (nonatomic,assign) BOOL isFromQRCode;

@end
