//
//  SLSearchTopicViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "SLLocationViewController.h"
#import "SLSearchTopicViewController.h"
#import <Masonry/Masonry.h>
#import "SLCateButton.h"
#import "AppDelegate.h"
#import "SLSearchUser.h"
#import "SLAPIHelper.h"
#import "SLTopicModel.h"
#import "HomePageViewController.h"
#import "SLSequenceBtn.h"//排序
#import "SLtopicsSequenceBtn.h"
#import "DatabaseManager.h"
#import "SLTopicViewCell.h"
#import "SLTopicCollectionViewCell.h"
#import "SLTopicDetailViewController.h"
#import "UILabel+StringFrame.h"
#import  <MJRefresh.h>
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "SLFirstViewController.h"
#import "SLSupportBtn.h"
#import "SLFans.h"
#import "SLTopicView.h"
#import "SLUserView.h"
#import <Photos/Photos.h>
#import "MWPhotoBrowser.h"
#import "SLSearchViewController.h"
#import "SLNavgationViewController.h"

#define  cellWH  ((ScreenW - 70 - (cols - 1) * margin) / cols)

@interface SLSearchTopicViewController ()<UITextFieldDelegate,SLTopicViewCellDelegate,UIAlertViewDelegate,SLTopicViewDelegate,SLUserViewDelegate,MWPhotoBrowserDelegate>
@property (nonatomic,strong) UITextField *searchText;
@property (nonatomic,assign) NSInteger page;
@property (nonatomic,strong) UISegmentedControl *segControl;
@property (nonatomic,strong) SLCateButton *placeBtn;
@property (nonatomic,strong) SLCateButton *sequenceBtn;
@property (nonatomic,strong) NSArray *sequenceArray;
@property (nonatomic,strong) UIView *sequenceView;
@property (nonatomic,strong) UIView *sequenceHalfView;
@property (nonatomic,strong) UILabel *hotSearch;
@property (nonatomic,strong) UIView *selectView;
@property (nonatomic,strong) NSDictionary *topicUsers;
@property (nonatomic,assign) NSInteger btnTag;
@property (nonatomic,assign) NSInteger locationTag;
@property (nonatomic,strong) UIView *navViews;
@property (nonatomic,strong) NSString *keyWord;//搜索用到的关键词
@property (nonatomic,assign) NSInteger topicPage;
@property (nonatomic,strong) NSArray *topicsSequenceArray;
@property (nonatomic,strong) NSArray *reorderArr;
@property (nonatomic,strong) UIAlertView *alert;
@property (nonatomic,strong) NSMutableDictionary *searchParams;  //搜索用户下拉刷新的参数
@property (nonatomic,strong) NSMutableDictionary *searchUserParams;  //搜索用户上拉刷新的参数
@property (nonatomic,strong) NSString *strTag;  //选择过地点的标记
@property (nonatomic,copy) NSString *recordLocation;//记录地区
@property (nonatomic,copy) NSString *recordUserSequence;//记录用户排序
@property (nonatomic,copy) NSString *recordTopicsSequence;//记录话题排序
@property (nonatomic,strong) SLTopicView *searchBaseTopicView;
@property (nonatomic,strong) SLUserView *userView;
@property (nonatomic,assign) BOOL isUserFirstRefresh;
@property (nonatomic,assign) BOOL isTopicFirstRefresh;
@property (nonatomic,copy) NSString *topicKey;
@property (nonatomic,strong) SLTopicModel *selectedModel;
@property (nonatomic,assign) NSInteger selectedIndex;
@property (nonatomic,strong) NSMutableArray *photos;
@property (nonatomic,strong) UIButton *clearBtn;
@property (nonatomic,strong) UIView *searchBackView;
@end
//static NSInteger const cols = 3;
//static CGFloat const margin = 5;
@implementation SLSearchTopicViewController{
    UIView *_headerView;
}
#pragma mark -- setter&getter
- (UIButton *)clearBtn{
    if (!_clearBtn) {
        _clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_clearBtn setImage:[UIImage imageNamed:@"clearBtnImg"] forState:UIControlStateNormal];
        [_clearBtn addTarget:self action:@selector(clearBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.searchBackView addSubview:_clearBtn];
    }
    return _clearBtn;
}
- (NSMutableDictionary *)searchParams{
    if (!_searchParams) {
        _searchParams = [NSMutableDictionary dictionary];
    }
    return _searchParams;
}

- (NSMutableDictionary *)searchUserParams{
    if (!_searchUserParams) {
        _searchUserParams = [NSMutableDictionary dictionary];
    }
    return _searchUserParams;
}
- (NSInteger)page{
    if (!_page) {
        _page = 0;
    }
    return _page;
}
-(NSInteger)topicPage{
    if (!_topicPage) {
        _topicPage=0;
    }
    return _topicPage;
}
-(SLUserView *)userView{
    if (!_userView) {
        _userView=[[SLUserView alloc] initWithFrame:CGRectMake(0, 145, ScreenW, ScreenH-145)];
        _userView.delegate=self;
    }
    return _userView;
}

-(SLTopicView *)searchBaseTopicView{
    if (!_searchBaseTopicView) {
        _searchBaseTopicView=[[SLTopicView alloc] initWithFrame:CGRectMake(0, 145, ScreenW, ScreenH-145)];
        _searchBaseTopicView.delegate=self;
    }
    return _searchBaseTopicView;
}
-(NSString *)topicKey{
    if (!_topicKey) {
        _topicKey=[NSString new];
    }
    return _topicKey;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLSearchTopicViewController"];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
    [MobClick endLogPageView:@"SLSearchTopicViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = SLColor(244, 244, 244);
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.segControl.selectedSegmentIndex=self.segIndex;
    _sequenceArray = @[@{@"icon":@"normal_option", @"name":@"默认排序"},@{@"icon":@"down_option", @"name":@"资费由高到低"},@{@"icon":@"up_option", @"name":@"资费由低到高"},@{@"icon":@"high_opinion", @"name":@"按好评度排序"}];
    _topicsSequenceArray = @[@{@"icon":@"normal_option", @"name":@"默认排序"},@{@"icon":@"timeImg", @"name":@"按创建时间排序"},@{@"icon":@"supportCount", @"name":@"按点赞数排序"}];
    [self addUI];
    [self sequenceUI];
    [self setUpRefresh];
    
    self.isUserFirstRefresh=YES;
    self.isTopicFirstRefresh=YES;
    //跳转进来直接出搜索结果
    self.searchText.text=self.presentKeywords;
    [self selectIndexSegControl:_segControl];
    _alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
    
    NSMutableArray *mArr = [[NSMutableArray alloc]initWithArray:self.navigationController.viewControllers];
    for (UIViewController *vc in mArr) {
        if ([vc isKindOfClass:[SLSearchViewController class]]) {
            [mArr removeObject:vc];
            break;
        }
    }
    self.navigationController.viewControllers = mArr;
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            [self addNetWorkViews:@"世界上最遥远的距离就是－－妹有网"];
        }else if ([netWorkStaus isEqualToString:@"wifi"]||[netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            if (_noNetWorkView) {
                [_noNetWorkView removeFromSuperview];
            }
        }
    }
    
}
- (void)setUpRefresh{
    self.userView.userTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
    self.userView.userTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopics)];
    self.searchBaseTopicView.slTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
    self.searchBaseTopicView.slTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopics)];
}
#pragma mark - - --- 获取缓存数据
- (void)getCacheData {
    //获取旧的数据
    NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:self.topicKey];
    if (configData) {
        NSDictionary *configDict = nil;
        configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
        if (self.segControl.selectedSegmentIndex==0) {
            self.userView.userDataSouresArr = [SLSearchUser mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
            [self.userView.userTableView reloadData];
           
        }else{
            self.searchBaseTopicView.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
            [self.searchBaseTopicView.slTableView reloadData];
            
        }
    }
}

#pragma mark - 搜索用户的接口
- (void)getData{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    if (self.segControl.selectedSegmentIndex==0) {
//        [_quitLoginView removeFromSuperview];
        [self.userView removeViewFromSubviews];
        self.page = 0;
        [self.searchParams setObject:@(20) forKey:@"pager"];
        [self.searchParams setObject:@(0) forKey:@"page"];
        /*
        //排序方向 （1:DESC，2:ASC），默认为1
        [self.searchParams setObject:@(1) forKey:@"sort_by"];
        //排序依据 （1:账号创建时间，2:通话价格，3:评价），默认为1
        [self.searchParams setObject:@(2) forKey:@"order_by"];
        //关键词  （查询类型为2时此项必选，为1时无效）
        */
        if (!self.searchText) {
            NSString *keywordStr=[self.presentKeywords stringByReplacingOccurrencesOfString:@" " withString:@""];
            [self.searchParams setObject:keywordStr forKey:@"keyword"];
        } else {
            NSString *keywordStr=[self.searchText.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            [self.searchParams setObject:keywordStr forKey:@"keyword"];
        }
        //标签ID  （查询类型为1时此项必选，为2时无效）
        [self.searchParams setObject:@(1) forKey:@"tag_id"];
//        //查询类型（1:按标签查找，2:按关键词查找）
        [self.searchParams setObject:@(2) forKey:@"query_type"];
//        //默认
//        if (_btnTag==0) {
//            [self.searchParams setObject:@(1) forKey:@"order_by"];
//            [self.searchParams setObject:@(1) forKey:@"sort_by"];
//        }
        //资费高到低
        if (_btnTag==1) {
            [self.searchParams setObject:@(2) forKey:@"order_by"];
            [self.searchParams setObject:@(1) forKey:@"sort_by"];
        }
        //资费低到高
        else if (_btnTag==2) {
            [self.searchParams setObject:@(2) forKey:@"order_by"];
            [self.searchParams setObject:@(2) forKey:@"sort_by"];
        }
        //按照评论排序
       else if (_btnTag==3) {
            [self.searchParams setObject:@(3) forKey:@"order_by"];
            [self.searchParams setObject:@(1) forKey:@"sort_by"];
        }
        NSLog(@"searchParams----------%@",self.searchParams);
        [SLAPIHelper searchUsers:self.searchParams success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            self.userView.userDataSouresArr = [SLSearchUser mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [MobClick event:@"search_count_user"];
            if (self.userView.userDataSouresArr.count == 0){
//                if (!_quitLoginView){
//                    [self addNotLoginViews:@"未搜索到相关用户，请尝试优化关键词"];
//                }
                [self.userView addNetWorkViews:@"未搜索到相关用户，请尝试优化关键词"];
            }else{
                [self.userView removeViewFromSubviews];
            }
            //缓存数据
            self.topicKey=@"User";
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:self.topicKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            // 刷新表格
            [self.userView.userTableView reloadData];
            if (self.userView.userDataSouresArr.count<20) {
                [self.userView.userTableView.mj_header endRefreshing];
                [self.userView.userTableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [_quitLoginView removeFromSuperview];
                [self.userView.userTableView.mj_header endRefreshing];
                [self.userView.userTableView.mj_footer resetNoMoreData];
            }
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            if (self.userView.userDataSouresArr.count==20) {
                [self.userView.userTableView.mj_footer setHidden:NO];
            }
        } failure:^(SLHttpRequestError *error) {
            NSLog(@"错误是%ld---%ld",error.httpStatusCode,error.slAPICode);
            // 结束刷新
            [self.userView.userTableView.mj_header endRefreshing];
            self.userView.userDataSouresArr = nil;
            [self.userView addNetWorkViews:@"搜索失败"];
//            if (!_quitLoginView){
//                [self addNotLoginViews:@"搜索失败"];
//            }
            [self.userView.userTableView reloadData];
        }];
    }
    if (self.segControl.selectedSegmentIndex==1) {
//        [_quitLoginView removeFromSuperview];
        [self.searchBaseTopicView removeViewFromSubviews];
        self.topicPage = 0;
        [self.searchUserParams setObject:@(20) forKey:@"pager"];
        [self.searchUserParams setObject:@(0) forKey:@"page"];
        if (!self.searchText.text) {
            NSString *keywordUTF8 = (NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(nil,
                                                                                                         (CFStringRef)self.presentKeywords, nil,
                                                                                                         (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));

            [self.presentKeywords stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

            [self.searchUserParams setObject:keywordUTF8 forKey:@"keyword"];
            NSString *keywordStr=[self.presentKeywords stringByReplacingOccurrencesOfString:@" " withString:@""];
            [self.searchUserParams setObject:keywordStr forKey:@"keyword"];
        } else {
            NSString *keywordStr=[self.searchText.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            [self.searchUserParams setObject:keywordStr forKey:@"keyword"];
        }
//        //默认
//        if (_btnTag==0) {
//            [self.searchUserParams setObject:@(1) forKey:@"order_by"];
//            [self.searchUserParams setObject:@(1) forKey:@"sort_by"];
//        }
        //话题创建时间
         if (_btnTag==1) {
            [self.searchUserParams setObject:@(2) forKey:@"order_by"];
        }
        //点赞数
        else if (_btnTag==2) {
            [self.searchUserParams setObject:@(3) forKey:@"order_by"];
        }
        NSLog(@"%@",self.searchUserParams);
        [SLAPIHelper searchTopics:self.searchUserParams success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            self.searchBaseTopicView.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            
            [MobClick event:@"search_count_topic"];
            
            if (self.searchBaseTopicView.slDataSouresArr.count == 0){
//                if (!_quitLoginView){
//                    [self addNotLoginViews:@"未搜索到相关话题，请尝试优化关键词"];
//                }
                [self.searchBaseTopicView addNetWorkViews:@"未搜索到相关话题，请尝试优化关键词"];
            }else{
                [self.searchBaseTopicView removeViewFromSubviews];
            }
            //缓存数据
            self.topicKey=@"Topic";
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:self.topicKey];
            [[NSUserDefaults standardUserDefaults] synchronize];            [self.searchBaseTopicView.slTableView reloadData];
            if ( self.searchBaseTopicView.slDataSouresArr.count<20) {
                [self.searchBaseTopicView.slTableView.mj_header endRefreshing];
                [self.searchBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [self.searchBaseTopicView.slTableView.mj_header endRefreshing];
                [self.searchBaseTopicView.slTableView.mj_footer resetNoMoreData];
            }
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            if (self.searchBaseTopicView.slDataSouresArr.count==20) {
                [self.searchBaseTopicView.slTableView.mj_footer setHidden:NO];
            }
        } failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.searchBaseTopicView.slTableView.mj_header endRefreshing];
            NSLog(@"%ld--- %ld",(long)error.slAPICode,(long)error.httpStatusCode);
            self.searchBaseTopicView.slDataSouresArr = nil;
//            if (!_quitLoginView){
//                [self addNotLoginViews:@"搜索话题失败，请稍后重试"];
//            }
            [self.searchBaseTopicView addNetWorkViews:@"搜索话题失败，请稍后重试"];
            [self.searchBaseTopicView.slTableView reloadData];
        }];
    }
}
/**
 * 加载更多搜索出来的用户列表
 */
- (void)loadMoreTopics{
    if (self.segControl.selectedSegmentIndex==0) {
        [self.userView.userTableView.mj_footer setHidden:NO];
        self.page++;
        [self.searchParams setObject:@(20) forKey:@"pager"];
        if (!self.searchText) {
            NSString *keywordStr=[self.presentKeywords stringByReplacingOccurrencesOfString:@" " withString:@""];
            [self.searchParams setObject:keywordStr forKey:@"keyword"];
        } else {
            NSString *keywordStr=[self.searchText.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            [self.searchParams setObject:keywordStr forKey:@"keyword"];
        }
        //标签ID  （查询类型为1时此项必选，为2时无效）
        [self.searchParams setObject:@(1) forKey:@"tag_id"];
        //查询类型（1:按标签查找，2:按关键词查找）
        [self.searchParams setObject:@(2) forKey:@"query_type"];
        //地区代码
        //        [params setObject:@(_locationTag) forKey:@"location_code"];
        //默认
//        if (_btnTag==0) {
//            [self.searchParams setObject:@(1) forKey:@"order_by"];
//            [self.searchParams setObject:@(1) forKey:@"sort_by"];
//        }
        //资费高到低
        if (_btnTag==1) {
            [self.searchParams setObject:@(1) forKey:@"sort_by"];
            [self.searchParams setObject:@(2) forKey:@"order_by"];
        }
        //资费低到高
       else if (_btnTag==2) {
            [self.searchParams setObject:@(2) forKey:@"sort_by"];
            [self.searchParams setObject:@(2) forKey:@"order_by"];
        }
        //按照评论排序
       else if (_btnTag==3) {
            [self.searchParams setObject:@(1) forKey:@"sort_by"];
            [self.searchParams setObject:@(3) forKey:@"order_by"];
        }
        
        [self.searchParams setObject:[NSString stringWithFormat:@"%ld", (long)self.page] forKey:@"page"];
        
        [SLAPIHelper searchUsers:self.searchParams success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            // NSLog(@"%@",data);
            NSArray *moreTopics = [SLSearchUser mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [self.userView.userDataSouresArr addObjectsFromArray:moreTopics];
            [MobClick event:@"search_count_user"];
            //刷新表格
            [self.userView.userTableView reloadData];
            
            if ( moreTopics.count <20) {
                
                [self.userView.userTableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                // 结束刷新
                [self.userView.userTableView.mj_footer endRefreshing];
            }
        } failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.userView.userTableView.mj_footer endRefreshing];
        }];
    }
    if (self.segControl.selectedSegmentIndex==1) {
        [self.searchBaseTopicView.slTableView.mj_footer setHidden:NO];
        self.topicPage++;
        [self.searchUserParams setObject:@(20) forKey:@"pager"];
        //        //排序方向 （1:DESC，2:ASC），默认为1
        //        [self.searchUserParams setObject:@(1) forKey:@"sort_by"];
        //        //排序依据（1:默认算法，2:话题创建时间，3:点赞数），默认为1
        //        [self.searchUserParams setObject:@(2) forKey:@"order_by"];
        //关键词  （查询类型为2时此项必选，为1时无效）
        if (!self.searchText) {
            NSString *keywordStr=[self.presentKeywords stringByReplacingOccurrencesOfString:@" " withString:@""];
            [self.searchUserParams setObject:keywordStr forKey:@"keyword"];
        } else {
            NSString *keywordStr=[self.searchText.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            [self.searchUserParams setObject:keywordStr forKey:@"keyword"];
        }
//        //默认
//        if (_btnTag==0) {
//            [self.searchUserParams setObject:@(1) forKey:@"order_by"];
//            [self.searchUserParams setObject:@(1) forKey:@"sort_by"];
//        }
        //话题创建时间
        if (_btnTag==1) {
            [self.searchUserParams setObject:@(2) forKey:@"order_by"];
        }
        //点赞数
      else  if (_btnTag==2) {
            [self.searchUserParams setObject:@(3) forKey:@"order_by"];
        }
        [self.searchUserParams setObject:[NSString stringWithFormat:@"%ld", (long)self.topicPage] forKey:@"page"];
        [SLAPIHelper searchTopics:self.searchUserParams success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            NSArray *moreTopics  = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [self.searchBaseTopicView.slDataSouresArr addObjectsFromArray:moreTopics];
            
            [MobClick event:@"search_count_topic"];
            [self.searchBaseTopicView.slTableView reloadData];
            
            if ( moreTopics.count <20) {
                
                [self.searchBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                // 结束刷新
                [self.searchBaseTopicView.slTableView.mj_footer endRefreshing];
            }
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        } failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.searchBaseTopicView.slTableView.mj_footer endRefreshing];
        }];
    }
}

- (void)addUI
{
    /**** 添加控件*****/
    //1.导航view
    UIView *navView = [[UIView alloc] init];
    navView.backgroundColor = SLColor(240, 240, 240);
    navView.frame = CGRectMake(0, 0, ScreenW, 135+10);
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *stateView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 20)];
    stateView.backgroundColor=[UIColor whiteColor];
    [navView addSubview:stateView];
    
    //取消按钮
    UIButton *cancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancel setTitle:@"取消" forState:UIControlStateNormal];
    cancel.titleLabel.font = [UIFont systemFontOfSize:15];
    [cancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancel addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:cancel];
    
    //输入框
    UIView *searchBackView=[[UIView alloc] init];
    searchBackView.layer.cornerRadius=15;
    searchBackView.backgroundColor = [UIColor whiteColor];
    searchBackView.layer.borderColor = UIColorFromRGB(0x9e7f7b).CGColor;
    searchBackView.layer.borderWidth = 0.5;
    self.searchBackView = searchBackView;
    [navView addSubview:searchBackView];
    
    UITextField *searchText = [[UITextField alloc] init];
    searchText.backgroundColor = [UIColor whiteColor];
    searchText.placeholder = @"输入关键字";
    searchText.returnKeyType = UIReturnKeyGo;
    searchText.font = [UIFont systemFontOfSize:14];
    searchText.clearButtonMode = UITextFieldViewModeNever;
    searchText.delegate = self;
    self.searchText=searchText;
    [navView addSubview:searchText];
    
    //光标
//    UIImageView *imgV=[[UIImageView alloc] init];
//    imgV.image=[UIImage imageNamed:@"Magnifier"];
//    [navView addSubview:imgV];
    
    
    UIView *segBackgroundView=[[UIView alloc] init];
    segBackgroundView.backgroundColor=[UIColor whiteColor];
    [navView addSubview:segBackgroundView];
    //用户/话题
    UISegmentedControl *segControl=[[UISegmentedControl alloc] initWithItems:@[@"用户",@"话题"]];
    segControl.selectedSegmentIndex=self.segIndex;
    self.segControl=segControl;
    [_segControl setTintColor:UIColorFromRGB(0xef1c00)];
    
    NSDictionary *dict1=@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName: [UIFont systemFontOfSize:17]};
    [segControl setTitleTextAttributes:dict1 forState:UIControlStateSelected];
    
    NSDictionary *dict2=@{NSForegroundColorAttributeName:UIColorFromRGB(0x636363),NSFontAttributeName:[UIFont systemFontOfSize:17]};
    [segControl setTitleTextAttributes:dict2 forState:UIControlStateNormal];
    
    [segControl addTarget:self action:@selector(selectIndexSegControl:) forControlEvents:UIControlEventValueChanged];
    [segBackgroundView addSubview:segControl];
    
    UIView *selectView = [[UIView alloc] init];
    selectView.backgroundColor = [UIColor whiteColor];
    self.selectView = selectView;
    [navView addSubview:selectView];
    
    SLCateButton *placeBtn = [SLCateButton buttonWithType:UIButtonTypeCustom];
    [placeBtn setTitle:@"地点" forState:UIControlStateNormal];
    [placeBtn setImage:[UIImage imageNamed:@"Down_arrow"] forState:UIControlStateNormal];
    [placeBtn addTarget:self action:@selector(selPlace) forControlEvents:UIControlEventTouchUpInside];
    _placeBtn = placeBtn;
    [selectView addSubview:placeBtn];
    
    UIImageView *cutImage = [[UIImageView alloc] init];
    cutImage.backgroundColor = [UIColor lightGrayColor];
    [selectView addSubview:cutImage];
    
    SLCateButton *sequenceBtn = [SLCateButton buttonWithType:UIButtonTypeCustom];
    [sequenceBtn setTitle:@"排序" forState:UIControlStateNormal];
    self.recordUserSequence=@"排序";
    self.recordTopicsSequence=@"排序";
    [sequenceBtn setImage:[UIImage imageNamed:@"Down_arrow"] forState:UIControlStateNormal];
    [sequenceBtn addTarget:self action:@selector(sequenceTableView) forControlEvents:UIControlEventTouchUpInside];
    _sequenceBtn = sequenceBtn;
    [selectView addSubview:sequenceBtn];
    
    UIImageView *HcutImage = [[UIImageView alloc] init];
    HcutImage.backgroundColor = [UIColor lightGrayColor];
    [selectView addSubview:HcutImage];
    [self.view addSubview:navView];
    
    [searchBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(navView.mas_left).offset(10);
        make.right.mas_equalTo(navView.mas_right).offset(-60);
        make.height.mas_equalTo(30);
        make.top.mas_equalTo(navView.mas_top).offset(27);
    }];
//    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(searchBackView.mas_left).offset(10);
//        make.top.mas_equalTo(searchBackView.mas_top).offset(7);
//        make.height.mas_equalTo(15);
//        make.width.mas_equalTo(15);
//    }];
    [_searchText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(searchBackView.mas_left).offset(20);
        make.right.mas_equalTo(searchBackView.mas_right).offset(-30);
        make.height.mas_equalTo(29);
        make.top.mas_equalTo(searchBackView.mas_top).offset(0.5);
    }];
    
    [self.clearBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(searchBackView.mas_top);
        make.right.mas_equalTo(searchBackView.mas_right);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(30);
    }];
    [cancel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(navView.mas_top).offset(27);
        make.height.mas_equalTo(30);
        make.right.mas_equalTo(navView.mas_right).offset(-10);
        make.width.mas_equalTo(40);
    }];
    [segBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(searchBackView.mas_bottom).offset(7);
        make.left.mas_equalTo(navView.mas_left);
        make.right.mas_equalTo(navView.mas_right);
        make.height.mas_equalTo(40);
    }];
    [_segControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(segBackgroundView.mas_top).offset(5);
        make.left.mas_equalTo(segBackgroundView.mas_left).offset((ScreenW-262)/2.0);
        make.width.mas_equalTo(262);
        make.height.mas_equalTo(30);
    }];
    [selectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(navView.mas_left);
        make.right.mas_equalTo(navView.mas_right);
        make.top.mas_equalTo(segBackgroundView.mas_bottom).offset(1);
        make.height.mas_equalTo(40);
    }];
    [cutImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(selectView.mas_top).offset(8);
        make.left.mas_equalTo(selectView.mas_left).offset((ScreenW-0.5)/2.0);
        make.width.mas_equalTo(0.5);
        make.bottom.mas_equalTo(selectView.mas_bottom).offset(-8);
    }];
    [placeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(selectView.mas_left);
        make.right.mas_equalTo(cutImage.mas_left);
        make.top.mas_equalTo(selectView.mas_top);
        make.bottom.mas_equalTo(selectView.mas_bottom);
    }];
    [sequenceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(cutImage.mas_right);
        make.right.mas_equalTo(selectView.mas_right);
        make.top.mas_equalTo(selectView.mas_top);
        make.bottom.mas_equalTo(selectView.mas_bottom);
        
    }];
    [HcutImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(selectView.mas_left);
        make.right.mas_equalTo(selectView.mas_right);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(selectView.mas_bottom).offset(-0.5);
    }];
}

-(void)sequenceUI{
    //侧滑UI
    _sequenceView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH)];
    _sequenceView.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _sequenceView.hidden=YES;
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(touch:)];
    [_sequenceView addGestureRecognizer:tapGesture];
    [[UIApplication sharedApplication].keyWindow addSubview:_sequenceView];
    
    _sequenceHalfView=[[UIView alloc] initWithFrame:CGRectMake(ScreenW, 0, ScreenW/2, ScreenH)];
    _sequenceHalfView.backgroundColor=[UIColor whiteColor];
    [_sequenceView addSubview:_sequenceHalfView];
    
    UILabel *sequenLab=[[UILabel alloc] init];
    sequenLab.textColor=SLColor(185, 185, 185);
    sequenLab.textAlignment=NSTextAlignmentCenter;
    sequenLab.font=[UIFont systemFontOfSize:15];
    [_sequenceHalfView addSubview:sequenLab];
    
    [sequenLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_sequenceHalfView.mas_top).offset(75);
        make.centerX.mas_equalTo(_sequenceHalfView.mas_centerX);
        make.height.mas_equalTo(15);
        
    }];
    
    if (self.segControl.selectedSegmentIndex==0) {
        sequenLab.text=@"用户排序";
        _reorderArr=_sequenceArray;
        
    }else {
        sequenLab.text=@"话题排序";
        _reorderArr=_topicsSequenceArray;
    }
    if ([_sequenceHalfView.subviews count] > 0) {
        for (id tem in [_sequenceHalfView subviews]) {
            if ([tem isKindOfClass:[SLSequenceBtn class]]) {
                [tem removeFromSuperview];
            }
        }
    }
    
    for (int i=0; i<_reorderArr.count; i++) {
        NSDictionary *dict=_reorderArr[i];
        if (self.segControl.selectedSegmentIndex==0) {
            SLSequenceBtn *btn=[SLSequenceBtn buttonWithType:UIButtonTypeCustom];
            btn.frame=CGRectMake(0, 150+i*50, ScreenW/2, 50);
            btn.tag=500+i;
            [btn addTarget:self action:@selector(sequenceBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            btn.imgIcon.image=[UIImage imageNamed:dict[@"icon"]];
            
            btn.textLab.text=dict[@"name"];
            btn.topLine.image=[UIImage imageNamed:@"searchHline"];
            if (i==_reorderArr.count-1) {
                
                btn.bottomLine.image=[UIImage imageNamed:@"searchHline"];
                
            }
            
            [_sequenceHalfView addSubview:btn];
            
        }else{
            SLtopicsSequenceBtn *btn=[SLtopicsSequenceBtn buttonWithType:UIButtonTypeCustom];
            
            btn.frame=CGRectMake(0, 150+i*50, ScreenW/2, 50);
            btn.tag=500+i;
            [btn addTarget:self action:@selector(sequenceBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            btn.imgIcon.image=[UIImage imageNamed:dict[@"icon"]];
            
            btn.textLab.text=dict[@"name"];
            btn.topLine.image=[UIImage imageNamed:@"searchHline"];
            if (i==_reorderArr.count-1) {
                
                btn.bottomLine.image=[UIImage imageNamed:@"searchHline"];
                
            }
            
            [_sequenceHalfView addSubview:btn];
        }
    }
}

- (void)clearBtnClick{
    self.searchText.text = @"";
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    //开始搜索
    //_btnTag=0;
    //替换掉空格
    NSString *searchTextStr=[self.searchText.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (searchTextStr.length>0) {
        if (self.strTag.length != 0) {
            if (_locationTag == 1) {
                [self.searchParams removeObjectForKey:@"location_code"];
                [self.searchUserParams removeObjectForKey:@"location_code"];
            }
            if (self.segControl.selectedSegmentIndex==0) {
                [self.searchParams setObject:@(_locationTag) forKey:@"location_code"];
                if (self.recordLocation) {
                    [self.placeBtn setTitle:self.recordLocation forState:UIControlStateNormal];
                }
                [self.userView.userTableView.mj_header beginRefreshing];
                [self.userView.userTableView.mj_footer setHidden:YES];
            }else{
                [self.searchBaseTopicView.slTableView.mj_header beginRefreshing];
                [self.searchBaseTopicView.slTableView.mj_footer setHidden:YES];
            }
        }else{
            if (self.segControl.selectedSegmentIndex==0) {
                if (self.recordLocation) {
                    [self.placeBtn setTitle:self.recordLocation forState:UIControlStateNormal];
                }
                [self.userView.userTableView.mj_header beginRefreshing];
                [self.userView.userTableView.mj_footer setHidden:YES];
            }else{
                [self.searchBaseTopicView.slTableView.mj_header beginRefreshing];
                [self.searchBaseTopicView.slTableView.mj_footer setHidden:YES];
            }
        }
        [MobClick event:@"serach_user"];
        [MobClick event:@"search_keyword"];
        //存入数据库
        if(![[DatabaseManager sharedManger] selectData:searchTextStr]){
            [[DatabaseManager sharedManger] insertDatas:searchTextStr];
        }
        [self.searchText endEditing:YES];
        [self.searchText resignFirstResponder];
        
    }else{
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"请输入关键字" delegate:self cancelButtonTitle:@"好" otherButtonTitles: nil];
        [alertView show];
    }
    return YES;
}

//切换控制器的点击事件
-(void)selectIndexSegControl:(UISegmentedControl *)segControl{
    [_segControl setTintColor:UIColorFromRGB(0xef1c00)];
    [self sequenceUI];
    if (self.strTag.length != 0) {
        if (_locationTag == 1) {
            [self.searchParams removeObjectForKey:@"location_code"];
            [self.searchUserParams removeObjectForKey:@"location_code"];
        }
        if (self.segControl.selectedSegmentIndex==0) {
            [self.searchParams setObject:@(_locationTag) forKey:@"location_code"];
            if (self.recordLocation) {
                [self.placeBtn setTitle:self.recordLocation forState:UIControlStateNormal];
            }
            [self selectFirstSegController];
        }else{
            [self.placeBtn setTitle:@"地点" forState:UIControlStateNormal];
            [self selectSecondSegController];
        }
    }else{
        if (self.segControl.selectedSegmentIndex==0) {
            [self selectFirstSegController];
        }else{
            [self selectSecondSegController];
        }
    }
}
-(void)selectFirstSegController{
    [self.searchBaseTopicView removeFromSuperview];
    [self.userView removeFromSuperview];
    [self.view addSubview:self.userView];
    if (self.recordUserSequence) {
        [_sequenceBtn setTitle:self.recordUserSequence forState:UIControlStateNormal];
    }
    self.topicKey=@"User";
    if (self.isUserFirstRefresh) {
        [self getCacheData];
        [self.userView.userTableView.mj_header beginRefreshing];
        [self.userView.userTableView.mj_footer setHidden:YES];
        self.isUserFirstRefresh=NO;
    }

//
    
}
-(void)selectSecondSegController{
    [self.userView removeFromSuperview];
    [self.searchBaseTopicView removeFromSuperview];
    [self.view addSubview:self.searchBaseTopicView];
    if (self.recordTopicsSequence) {
        [_sequenceBtn setTitle:self.recordTopicsSequence forState:UIControlStateNormal];
    }
    self.topicKey=@"Topic";
    if (self.isTopicFirstRefresh) {
        [self getCacheData];
        [self.searchBaseTopicView.slTableView.mj_header beginRefreshing];
        [self.searchBaseTopicView.slTableView.mj_footer setHidden:YES];
        self.isTopicFirstRefresh=NO;
    }
    
}
//定位
- (void)selPlace{
    SLLocationViewController *location = [[SLLocationViewController alloc] init];
    location.tag = 1000;
    location.valueBlcok = ^(NSString *str){
        self.recordLocation=str;
        [self.placeBtn setTitle:str forState:UIControlStateNormal];
        //            if ([_Delegate respondsToSelector:@selector(becomeFirstView)]) {
        //                [_Delegate becomeFirstView];
        //            }
    };
    
    location.valueWithCityIdBlcok = ^(NSString *str,NSString *locationId){
        [MobClick event:@"filter_district"];
        _locationTag = [locationId integerValue];
        [self.searchParams setObject:@(_locationTag) forKey:@"location_code"];
        //[self.searchUserParams setObject:@(_locationTag) forKey:@"location_code"];
        self.strTag = [NSString stringWithFormat:@"%@",locationId];
        if (_locationTag == 1) {
            [self.placeBtn setTitle:@"全国" forState:UIControlStateNormal];
            self.recordLocation=@"全国";
            [self.searchParams removeObjectForKey:@"location_code"];
            [self.searchUserParams removeObjectForKey:@"location_code"];
        }
        [self.userView.userTableView.mj_header beginRefreshing];//获取定位筛选之后的用户数据
    };
    if (self.segControl.selectedSegmentIndex==1) {
        location.isHomeLocation=YES;
    }
    [self.navigationController pushViewController:location animated:YES];
}
//排序方式点击事件
-(void)sequenceBtnClick:(UIButton *)btnClick{
    _btnTag = btnClick.tag-500;
    if (self.segControl.selectedSegmentIndex==0) {
        if (self.recordUserSequence) {
            self.recordUserSequence=_sequenceArray[_btnTag][@"name"];
            [_sequenceBtn setTitle:self.recordUserSequence forState:UIControlStateNormal];
        }
        [self.userView.userTableView.mj_header beginRefreshing];
        [self.userView.userTableView.mj_footer setHidden:YES];
        
    }else{
        if (self.recordTopicsSequence) {
            self.recordTopicsSequence=_topicsSequenceArray[_btnTag][@"name"];
            [_sequenceBtn setTitle:self.recordTopicsSequence forState:UIControlStateNormal];
        }
        [self.searchBaseTopicView.slTableView.mj_header beginRefreshing];
        [self.searchBaseTopicView.slTableView.mj_footer setHidden:YES];
    }
    
    if (!self.searchText) {
        if (self.strTag.length != 0) {
            [self.searchParams setObject:@(_locationTag) forKey:@"location_code"];
            // [self.searchUserParams setObject:@(_locationTag) forKey:@"location_code"];
            if (_locationTag == 1) {
                [self.searchParams removeObjectForKey:@"location_code"];
                [self.searchUserParams removeObjectForKey:@"location_code"];
            }
        }
    }
    [self closeMenuView];
}
//点击排序
-(void)sequenceTableView{
    [self openMenuView];
}
//打开侧滑view
- (void)openMenuView{
    _sequenceView.hidden=NO;
    [UIView animateWithDuration:0.3 animations:^{
        _sequenceHalfView.frame=CGRectMake(ScreenW/2, 0, ScreenW/2, ScreenH);
    } completion:^(BOOL finished) {
        
    }];
}
//收起侧滑view
- (void)closeMenuView{
    [UIView animateWithDuration:0.3 animations:^{
        _sequenceHalfView.frame=CGRectMake(ScreenW, 0, ScreenW/2, ScreenH);
    } completion:^(BOOL finished) {
        _sequenceView.hidden=YES;
    }];
}
//点击收起侧滑
-(void)touch:(UIGestureRecognizer *)gesture{
    [self closeMenuView];
}
//取消
- (void)cancelClick{
    self.backRefreshBlock(YES);
//    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - SLUserViewDelegate
-(void)SLUserView:(SLUserView *)baseView didSelectRowAtIndexPathWithModel:(SLSearchUser *)model{
    HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
    otherPerson.userId=model.id;
    otherPerson.userId=model.id;
    otherPerson.didClickFollowBtn = ^(UIButton *btn){
        model.following = btn.selected?@"1":@"0";
        NSInteger follower = [model.followers integerValue];
        if (btn.selected) {
            follower++;
        }else{
            follower--;
        }
        model.followers = [NSString stringWithFormat:@"%ld",(long)follower];
        //        NSLog(@"%@",NSStringFromClass([model.followers class]))
        [baseView.userTableView reloadData];
    };
    self.isFromQRCode=NO;
    [self.navigationController pushViewController:otherPerson animated:YES];
}
#pragma mark - BaseTopicViewDelegate

-(void)SLTopicView:(SLTopicView *)baseView deleteTopicAtIndexPath:(NSInteger)index{
    NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:@"HotTopic"];
    if (configData) {
        NSDictionary *configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
        NSArray *arr =configDict[@"result"];
        NSMutableArray *tipcsArr=[[NSMutableArray alloc] init];
        [tipcsArr addObjectsFromArray:arr];
        [tipcsArr removeObjectAtIndex:index];
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        [dict setObject:tipcsArr forKey:@"result"];
    
        NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:dict];
        [[NSUserDefaults standardUserDefaults] setObject:configData forKey:@"HotTopic"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)SLTopicView:(SLTopicView *)baseView CellDidSelectedWithIndex:(NSInteger)index{
    SLFans *model =  self.searchBaseTopicView.suggestionArr[index];
    HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
    otherPerson.userId = model.id;
    self.isFromQRCode=NO;
    [self.navigationController pushViewController:otherPerson animated:YES];
}
- (void)SLTopicView:(SLTopicView *)baseView didSelectCollectionByMWPhotoBrowser:(id)browser WithModel:(SLTopicModel *)topicModel{
    [self presentViewController:browser animated:YES completion:nil];
}
//-(void)SLTopicView:(SLTopicView *)baseView didPraiseButtonClicked:(UIButton *)button model:(SLTopicModel *)model{
//    [self didPraiseBtnClicked:button model:model];
//}
- (void)SLTopicView:(SLTopicView *)baseView didPraiseButtonClicked:(UIButton *)button praiseLab:(UILabel *)praiseLab model:(SLTopicModel *)model{
    [self didPraiseBtnClicked:button praiseLab:praiseLab model:model];
}
-(void)SLTopicView:(SLTopicView *)baseView didUserIconClicked:(SLTopicModel *)model{
    self.isFromQRCode=NO;
    [self didUserIconClickedTransmitModel:model];
}
//-(void)SLTopicView:(SLTopicView *)baseView didSelectRowAtIndexPath:(SLTopicModel *)model{
////    [self didSelectRowAtIndexPathTransmitModel:model];
//    self.selectedModel = model;
//    [self didSelectRowAtIndexPathTransmitModel:model isFromSearch:YES];
//}
-(void)SLTopicView:(SLTopicView *)baseView didSelectRowAtIndexPath:(NSInteger)index AndModel:(SLTopicModel *)model{
    self.selectedModel = model;
    self.selectedIndex=index;
    self.isFromQRCode=NO;
    [self didSelectRowAtIndexPathTransmitModel:model isFromSearch:YES];
}
-(void)SLTopicView:(SLTopicView *)baseView pushLoginVC:(BOOL)isPushing{
    SLFirstViewController *loginView=[[SLFirstViewController alloc] init];
    SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:loginView];
//    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:loginView];
    [self presentViewController:nav animated:YES completion:nil];
}


// 没有搜索到内容
-(void)addNotLoginViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.selectView.frame), ScreenW , ScreenH )];
    
    UILabel *tiplabel = [[UILabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    tiplabel.attributedText = AttributedStr;
    
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    _quitLoginView  = contentView;
    [self.view addSubview:contentView];
    
}



// 没有网的制空页面
-(void)addNetWorkViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.selectView.frame), ScreenW , ScreenH )];
    
    UILabel *tiplabel = [[UILabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    
    tiplabel.attributedText = AttributedStr;
    
    
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    _noNetWorkView = contentView;
    [self.view addSubview:contentView];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        SLFirstViewController *vc=[[SLFirstViewController alloc] init];
        SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
//        UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:nav animated:YES completion:nil];
    }
}
#pragma mark - SLTopicDetailViewControllerDelegate
- (void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickPraise:(SLTopicModel *)model{
    self.selectedModel.has_thumbsuped =model.has_thumbsuped;
    self.selectedModel.thumbups_cnt =model.thumbups_cnt;
    [self.searchBaseTopicView.slTableView reloadData];
    
}
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickComment:(SLTopicModel *)model{
    self.selectedModel.replies_cnt=model.replies_cnt;
    [self.searchBaseTopicView.slTableView reloadData];
}
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didSelectModel:(SLTopicModel *)model{
    [self.searchBaseTopicView.slDataSouresArr removeObjectAtIndex:self.selectedIndex];
    [self.searchBaseTopicView.slTableView reloadData];
    [self SLTopicView:self.searchBaseTopicView deleteTopicAtIndexPath:self.selectedIndex];
}
- (void)slPhotoBrowser:(NSIndexPath *)indexPath withPhotos:(NSMutableArray *)photoes{
    self.photos = photoes;
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
   
    [browser setCurrentPhotoIndex:indexPath.row];
    browser.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:browser animated:YES completion:nil];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (_photos.count>9) {
        return 9;
    }
    return _photos.count;
}

#pragma 代理
- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

@end
