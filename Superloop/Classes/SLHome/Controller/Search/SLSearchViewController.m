//
//  SLSearchViewController.m
//  Superloop
//
//  Created by WangS on 16/5/14.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSearchViewController.h"

#import "SLSearchTopicViewController.h"

#import "DatabaseManager.h"

@interface SLSearchViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic, strong)UITextField *searchText;

@property(nonatomic,strong)UISegmentedControl  *segControl;

@property (nonatomic,strong)UIView *selectView;

@property(nonatomic,strong)UITableView *myTableView;

@property(nonatomic,strong)NSArray *groupSearchArr;//热门搜索和历史搜索

@property(nonatomic,strong)NSArray *hotSearchArr;

@property(nonatomic,strong)NSArray *historySearchArr;

@property(nonatomic,strong)UIButton *leftBtn;
@property(nonatomic,strong)UIButton *rightBtn;

@property (nonatomic,strong) UIButton *clearBtn;
@property (nonatomic,strong) UIView *searchBackView;
@end

@implementation SLSearchViewController
- (UIButton *)clearBtn{
    if (!_clearBtn) {
        _clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_clearBtn setImage:[UIImage imageNamed:@"clearBtnImg"] forState:UIControlStateNormal];
        [_clearBtn addTarget:self action:@selector(clearBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.searchBackView addSubview:_clearBtn];
    }
    return _clearBtn;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    [MobClick beginLogPageView:@"SLSearchViewController"];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLSearchViewController"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = SLColor(240, 240, 240);

    _groupSearchArr=@[@"历史搜索记录"];
//    _hotSearchArr=@[@"老章",@"超级圈",@"北京北京",@"程序猿的日常",@"超级圈就是屌屌屌"];
    _hotSearchArr=nil;

  
    //获取数据
    [self  getDatas];
    
    //添加UI
    [self addUI];
    
    //实例化
    [self initTableView];
    [self.searchText becomeFirstResponder];

}
//获取数据
-(void)getDatas
{
  //从数据库获取数据
    _historySearchArr=[[DatabaseManager sharedManger] getAllDatas];

    [self.myTableView reloadData];
}


-(void)addUI
{

    /**** 添加控件*****/
    //1.导航view
    UIView *navView = [[UIView alloc] init];
//    navView.backgroundColor = [UIColor clearColor];
    navView.backgroundColor=SLColor(240, 240, 240);
    navView.frame = CGRectMake(0, 0, ScreenW, 64);
    [self.view addSubview:navView];
//    [self.navigationItem.titleView addSubview: navView];
    
    UIView *stateView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 20)];
    stateView.backgroundColor=[UIColor whiteColor];
    [navView addSubview:stateView];
    
    //取消按钮
    UIButton *cancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancel setTitle:@"取消" forState:UIControlStateNormal];
    cancel.titleLabel.font = [UIFont systemFontOfSize:15];
    [cancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancel addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:cancel];
    
    //输入框
    UIView *searchBackView=[[UIView alloc] init];
    searchBackView.layer.cornerRadius=15;
    searchBackView.layer.borderColor = UIColorFromRGB(0x9e7f7b).CGColor;
    searchBackView.layer.borderWidth = 0.5;
    searchBackView.backgroundColor = [UIColor whiteColor];
    self.searchBackView = searchBackView;
    [navView addSubview:searchBackView];
    
    _searchText = [[UITextField alloc] init];
    //_searchText.delegate = self;
    _searchText.backgroundColor = [UIColor whiteColor];
    _searchText.placeholder = @"输入关键字";
    _searchText.returnKeyType = UIReturnKeyGo;
    _searchText.font = [UIFont systemFontOfSize:14];
    _searchText.clearButtonMode = UITextFieldViewModeNever;
    _searchText.delegate = self;
    [navView addSubview:_searchText];
    
    //光标
//    UIImageView *imgV=[[UIImageView alloc] init];
//    imgV.image=[UIImage imageNamed:@"Magnifier"];
//    [searchBackView addSubview:imgV];
    
    UIView *segBackgroundView=[[UIView alloc] init];
    segBackgroundView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:segBackgroundView];
    
    //用户/话题
    _segControl=[[UISegmentedControl alloc] initWithItems:@[@"用户",@"话题"]];
    _segControl.selectedSegmentIndex=0;
    //[_segControl setTintColor:SLColor(200, 200, 200)];
    [_segControl setTintColor:UIColorFromRGB(0xef1c00)];
    NSDictionary *dict1=@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:17]};
    [_segControl setTitleTextAttributes:dict1 forState:UIControlStateSelected];

    NSDictionary *dict2=@{NSForegroundColorAttributeName:UIColorFromRGB(0x636363),NSFontAttributeName:[UIFont systemFontOfSize:17]};
    [_segControl setTitleTextAttributes:dict2 forState:UIControlStateNormal];
    
    [_segControl addTarget:self action:@selector(selectIndexSegControl:) forControlEvents:UIControlEventValueChanged];
    
    [segBackgroundView addSubview:_segControl];
    
   
    
    [searchBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(navView.mas_left).offset(10);
        make.right.mas_equalTo(navView.mas_right).offset(-60);
        make.height.mas_equalTo(30);
        make.bottom.mas_equalTo(navView.mas_bottom).offset(-7);
    }];
    
//    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(searchBackView.mas_left).offset(10);
//        make.top.mas_equalTo(searchBackView.mas_top).offset(7);
//        make.height.mas_equalTo(15);
//        make.width.mas_equalTo(15);
//    }];
    
    
//    [_searchText mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(navView.mas_left).offset(30);
//        make.right.mas_equalTo(navView.mas_right).offset(-60);
//        make.height.mas_equalTo(30);
//        make.bottom.mas_equalTo(navView.mas_bottom).offset(-7);
//    }];
    [_searchText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(searchBackView.mas_left).offset(22);
        make.right.mas_equalTo(searchBackView.mas_right).offset(-30);
        make.height.mas_equalTo(29);
        make.top.mas_equalTo(searchBackView.mas_top).offset(0.5);
    }];
    
    [self.clearBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(searchBackView.mas_top);
        make.right.mas_equalTo(searchBackView.mas_right);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(30);
    }];
    
    [cancel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(_searchText.mas_centerY);
        make.top.mas_equalTo(navView.mas_top).offset(27);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(30);
    }];
    
    [segBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(navView.mas_bottom);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(40);
    }];
    
    
    [_segControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(segBackgroundView.mas_top).offset(5);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.width.mas_equalTo(262);
        make.height.mas_equalTo(30);
    }];
    

    
    
}
//实例化
-(void)initTableView
{
    _myTableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 105, ScreenW, ScreenH-105) style:UITableViewStylePlain];
    self.myTableView.dataSource=self;
    self.myTableView.delegate=self;
    [self.view addSubview:self.myTableView];
    
    UIView *footView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 0.5)];
    footView.backgroundColor=[UIColor lightGrayColor];
    self.myTableView.tableFooterView=footView;
    
    //注册
    [self.myTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"historyCell"];

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if (section==0) {
//        return self.hotSearchArr.count;
//    }
    if (section==0) {
//        if (self.historySearchArr.count>20) {
//            return 20;
//        }
        return self.historySearchArr.count;
    }
    return 0;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.groupSearchArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"historyCell"];
//    if (indexPath.section==0) {
//        cell.textLabel.text=self.hotSearchArr[indexPath.row];
//    }
    if (!cell) {
        
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"historyCell"];
    }
    if (indexPath.section==0) {
        
        cell.textLabel.text=self.historySearchArr[indexPath.row];

    }
    cell.textLabel.font=[UIFont systemFontOfSize:14];
    cell.textLabel.textColor=[UIColor lightGrayColor];
    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 40)];
    
//    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
//    btn.frame=CGRectMake(ScreenW-70, 0, 50, 40);
//    [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
//    btn.titleLabel.font=[UIFont systemFontOfSize:14];
//    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [sectionView addSubview:btn];

//    if (section==0) {
//        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
//        btn.frame=CGRectMake(ScreenW-70, 0, 50, 40);
//        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
//        btn.titleLabel.font=[UIFont systemFontOfSize:14];
//        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [sectionView addSubview:btn];
//        btn.tag=100;
//        [btn setTitle:@"换一批" forState:UIControlStateNormal];
//
//    }
    if (section==0) {
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(ScreenW-70, 0, 50, 40);
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.titleLabel.font=[UIFont systemFontOfSize:14];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [sectionView addSubview:btn];
        btn.tag=101;
        [btn setTitle:@"删除" forState:UIControlStateNormal];

    }

    UILabel *lab=[[UILabel alloc] initWithFrame:CGRectMake(10, 0, ScreenW-70, 40)];
    lab.font=[UIFont systemFontOfSize:14];
    lab.textColor=[UIColor blackColor];
    lab.text=self.groupSearchArr[section];
    [sectionView addSubview:lab];
    
    return sectionView;
}
-(void)btnClick:(UIButton *)btn
{
//    if (btn.tag==100) {
//        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"暂不支持此功能,非常抱歉" delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil];
//        [alert show];
//    }
    if (btn.tag==101) {
        BOOL ret = [[DatabaseManager sharedManger] deleteUser];
        NSLog(@"数据库删除:%@",ret ? @"成功":@"失败");
        [self getDatas];
    }
   
}

//滑动组头不停靠
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat sectionHeaderHeight = 40;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        
    }
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        self.searchText.text=self.historySearchArr[indexPath.row];
    }
    [self searchAction];
}


//切换控制器的点击事件
-(void)selectIndexSegControl:(UISegmentedControl *)segControl{
    [segControl setTintColor:UIColorFromRGB(0xef1c00)];
}

- (void)cancelClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)clearBtnClick{
    self.searchText.text = @"";
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
   
    //开始搜索
    [self searchAction];
    return YES;
}

-(void)searchAction{
    if (self.searchText.text.length>0) {
        [MobClick event:@"search_count"];
        [MobClick event:@"serach_user"];
        [MobClick event:@"search_keyword"];
        NSString *searchTextStr=[self.searchText.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        if(![[DatabaseManager sharedManger] selectData:searchTextStr]){
            
            [[DatabaseManager sharedManger] insertDatas:searchTextStr];
        }
        
        [self getDatas];
        [self.searchText endEditing:YES];
        [self.searchText resignFirstResponder];
        
        SLSearchTopicViewController *vc=[[SLSearchTopicViewController alloc] init];
        
        vc.backRefreshBlock=^(BOOL isRefresh){
            
            [self getDatas];
        };
        vc.isFromQRCode=YES;
        vc.presentKeywords=self.searchText.text;
        vc.segIndex=_segControl.selectedSegmentIndex;

        [self.navigationController pushViewController:vc animated:YES];
    }else{
        
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"请输入关键字" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
    }
}
@end
