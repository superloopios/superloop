//
//  SLSelectCityiewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^executeFinishedBlock)(void);

@protocol SLSelectCityViewControllerDelegate <NSObject>
- (void)sendCityName:(NSString *)cityName;
- (void)sendCityName:(NSString *)cityName cityId:(NSString *)cityId;
@end
@interface SLSelectCityViewController : UIViewController
@property (nonatomic, strong) NSArray *dataArray;
@property(weak, nonatomic) id<SLSelectCityViewControllerDelegate>Delegate;
@property (nonatomic, assign) NSInteger tag;
@property (nonatomic,strong)SLJavascriptBridgeEvent *event;
@property (nonatomic, copy)executeFinishedBlock(changeAddress);
@property(nonatomic,assign)BOOL isMineBasicInfoLocation; //判断是从基本信息控制器过来的
@property(nonatomic,assign)BOOL isMineExperienceLocation; //判断是从工作经历控制器过来的
@property(nonatomic,assign)BOOL isRegisterLocation; //判断是从注册控制器过来的

@end
