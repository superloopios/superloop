//
//  SLHomeViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#define isIOS(version) ([[UIDevice currentDevice].systemVersion floatValue] >= version)
//#import "SLSearchTopicViewController.h"
#import "SLHomeViewController.h"
#import "SLQuestionUsViewController.h"
#import "SLLocationViewController.h"
#import "SLMoreCategoryViewController.h"
#import "SLCatDetailViewController.h"
#import "SLSearchViewController.h"
#import "SLMoreCategoryController.h"
#import "SLWebViewController.h"
#import "SLQRCodeViewController.h"
#import "HomePageViewController.h"
#import "SLNewSuperManViewController.h"
#import "SLTabBarViewController.h"

#import "SLButtonTag.h"
#import "SLBannerModel.h"
#import "SLProvince.h"
#import "SLChildProvince.h"
#import "SLTagModel.h"
#import "SLSearchUser.h"

#import "SDCycleScrollView.h"
#import "SLUserView.h"
#import "SLQRCodeBtn.h"
#import "SLUpdateView.h"
#import "SLGiveEvaluationView.h"
#import  <MJRefresh.h>
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "SLLocationButton.h"
#import "SLClassBtn.h"
#import "SLClassesButton.h"


#import <MJExtension/MJExtension.h>
#import <UIImageView+WebCache.h>
#import <UIButton+WebCache.h>
#import "SLAPIHelper.h"
#import "UILabel+StringFrame.h"
#import "AppDelegate.h"
#import "AESCrypt.h"
#import "SLMessageManger.h"
#import <CoreLocation/CoreLocation.h>
#import <Masonry.h>

#import "SLRankingViewController.h"

#define cellWH  ((ScreenW - 70 - (cols - 1) * margin) / cols)
@interface SLHomeViewController ()
<UIAlertViewDelegate,SDCycleScrollViewDelegate,SLUserViewDelegate,SLUpdateViewDelegate,SLGiveEvaluationViewDelegate>

@property (nonatomic, strong) UIButton              *btn;
@property (nonatomic, strong) NSArray               *recommendArray;
@property (nonatomic, strong) UIView                *headView;
@property (nonatomic, strong) NSString              *cityStr;
@property (nonatomic, strong) UIView                *classesView;
@property (nonatomic, strong) NSArray               *bannerArr;
@property (nonatomic, strong) NSArray               *tagArr;//装tag的数组
@property (nonatomic,   weak) UIView                *haveNoDataView; //制空页
@property (nonatomic, assign) BOOL                  hasNetWork;
@property (nonatomic, strong) UIAlertView           *homeAlert;
@property (nonatomic, strong) NSArray               *modelGroups;
@property (nonatomic, strong) NSArray               *childProvince;
@property (nonatomic, assign) NSInteger             page;
@property (nonatomic, assign) BOOL                  isRefreshBanner;
@property (nonatomic, strong) NSArray               *tagDataArr;
@property (nonatomic, assign) BOOL                  isRefreshTag;
@property (nonatomic, strong) NSMutableArray        *photos;
@property (nonatomic, strong) SDCycleScrollView     *cycleBanner;
@property (nonatomic, strong) SLUserView            *baseUserView;
@property (nonatomic, strong) SLSearchUser          *userModel;
//是否需要提示选择主标签
@property (nonatomic, assign) BOOL                   needTipAddTags;
//是否有新版本
@property (nonatomic,assign) BOOL hasNewVersionTip;
@property (nonatomic,strong) SLGiveEvaluationView *giveEvaluationView;

@end

@implementation SLHomeViewController{
    UIImageView *_imgView;
}


static NSInteger const cols = 4;
#define imageWH  (ScreenW) / cols

- (SDCycleScrollView *)cycleBanner{
    if (!_cycleBanner) {
        _cycleBanner = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, screen_W, 124) shouldInfiniteLoop:YES imageNamesGroup:nil];
        _cycleBanner.delegate = self;
        [_cycleBanner setPlaceholderImage:[UIImage imageNamed:@"bannerDefault"]];
        _cycleBanner.pageControlStyle = SDCycleScrollViewPageContolStyleClassic;
        [_headView addSubview:_cycleBanner];
        _cycleBanner.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        //         --- 轮播时间间隔，默认1.0秒，可自定义
        _cycleBanner.autoScrollTimeInterval = 5.0;
    }
    return _cycleBanner;
}

#pragma mark -懒加载
-(SLUserView *)baseUserView{
    if (!_baseUserView) {
        _baseUserView=[[SLUserView alloc] initWithFrame:CGRectMake(0, 64, ScreenW, ScreenH-113)];
        _baseUserView.isHomeVC=YES;
        _baseUserView.delegate=self;
        _baseUserView.userTableView.scrollsToTop=YES;
        [self.view addSubview:_baseUserView];
    }
    return _baseUserView;
}
-(NSArray *)tagDataArr{
    if (!_tagDataArr) {
        _tagDataArr=[NSArray new];
    }
    return _tagDataArr;
}

-(NSInteger)page{
    if (!_page) {
        _page=0;
    }
    return _page;
}

#pragma mark - view周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLHomeViewController"];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLHomeViewController"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.view.backgroundColor = SLColor(240, 240, 240);
    self.isRefreshTag=NO;
    self.isRefreshBanner=NO;
    [self setUpUI];
    [self setUpNav];
    [self setUpRefresh];
    [self getBannerData];
    [self getCacheData];
    [self.baseUserView.userTableView.mj_header beginRefreshing];
    [self.baseUserView.userTableView.mj_footer setHidden:YES];
    [self getTagDataArr];
    self.baseUserView.userTableView.tableHeaderView = self.headView;

    [self hasNewVersion];
    
    //点击状态栏滚动到顶部
    self.baseUserView.userTableView.scrollsToTop= YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(homeRefreshing) name:@Home_Refreshing object:nil];

    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
    //登录成功刷新数据
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isLoginSuccessToLoadData) name:@"isLoginSuccessToLoadData" object:nil];
    //退出登录刷新数据
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ClearMessage) name:@"ClearMessage" object:nil];
//    [SLAPIHelper weChatDel:@{@"cellphone":@"18099485080",@"code":@"free"} success:^(NSURLSessionDataTask *task, NSDictionary *data) {
//        NSLog(@"%@",data);
//    } failure:^(SLHttpRequestError *failure) {
//        
//    }];
}
- (void)gotoEvaluate{
   
    BOOL showEvaluation= [[NSUserDefaults standardUserDefaults] boolForKey:@"GiveEvaluationView"];
    if (showEvaluation) {
        [self showGiveEvaluationView];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"GiveEvaluationView"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

}
- (void)clearAllUserDefaultsData{
    
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    
    NSDictionary* dict = [defs dictionaryRepresentation];
    
    for(id key in dict) {
        
        if (![key isEqualToString:appIsFirst]&& ![key isEqualToString:SLNetWorkStatus] && ![key isEqualToString:strAccountName] &&![key isEqualToString:strAccout] && ![key isEqualToString:@"VoiceType"]&& ![key isEqualToString:location]&&![key isEqualToString:@"guideSave"]&&![key isEqualToString:ContactStatus]&&![key isEqualToString:LoginStutas]&&![key isEqualToString:AllowAddContactStatus]) {
            
            [defs removeObjectForKey:key];
        }
    }
    
}
- (void)checkHasTag{
    if (ApplicationDelegate.getMySettingTagStatus) {
        //获取我的标签接口,判断是否已经添加标签
        if (!ApplicationDelegate.userId) {
            return;
        }
        NSDictionary *parameters =  @{@"id":ApplicationDelegate.userId};
        [SLAPIHelper getUsersData:parameters success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            NSDictionary *dict = data[@"result"];
            self.userInfo = data[@"result"];
            if(![[dict allKeys] containsObject:@"primary_tags"]){
                self.needTipAddTags = YES;
                if (!self.hasNewVersionTip) {
                    [self getMySettingTagStatus];
                }
            }else{
                //评分是否弹出去AppStore
                [self gotoEvaluate];
            }
        } failure:nil];
    }
}

- (void)hasNewVersion{
    if (!self.hasNetWork) {
         NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        NSDictionary *param = @{@"platform_type":@2,@"version_code":version};
        [SLAPIHelper getNewVersionInfo:param success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            if ([data[@"result"][@"update"] isEqual:@1]) {
                SLUpdateView *updateView = [[SLUpdateView alloc] init];
                updateView.delegate = self;
                updateView.dict = data[@"result"];
                [[UIApplication sharedApplication].keyWindow addSubview:updateView];
                self.hasNewVersionTip = YES;
            }else{
                self.hasNewVersionTip = NO;
            }
            [self checkHasTag];
        } failure:^(SLHttpRequestError *failure) {
            [self checkHasTag];
        }];
    }
}

- (void)SLUpdateView:(SLUpdateView *)updateView didClickBtnAtIndex:(NSInteger)index{
    if (index == 1) {
        NSString  *str;
        str = @"https://itunes.apple.com/cn/app/id1130659569";
        [[UIApplication  sharedApplication] openURL:[NSURL  URLWithString:str]];
    }
    [updateView removeFromSuperview];
    if (self.needTipAddTags) {
        [self getMySettingTagStatus];
    }
}

#pragma mark - 通知实现的方法
-(void)isLoginSuccessToLoadData{
    [self.baseUserView.userTableView.mj_header beginRefreshing];
    [self.baseUserView.userTableView.mj_footer setHidden:YES];
}
-(void)ClearMessage{
    [self.baseUserView.userTableView.mj_header beginRefreshing];
    [self.baseUserView.userTableView.mj_footer setHidden:YES];
}
#pragma mark --- 刷新方法
- (void)homeRefreshing{
    [self.baseUserView.userTableView.mj_header beginRefreshing];
    [self.baseUserView.userTableView.mj_footer setHidden:YES];
}
- (void)setUpRefresh {
    self.baseUserView.userTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
    self.baseUserView.userTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreData)];
}
#pragma mark -获取网络状态
-(void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetWork=NO;
            NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:@"BannerImg"];
            if (configData) {
                NSDictionary *configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
                //                    NSArray *configBannerArr = [SLBannerModel mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
                
                NSMutableArray *arr = [NSMutableArray array];
                for (NSDictionary *dict in configDict[@"result"]) {
                    [arr addObject:dict[@"image_url"]];
                }
                self.cycleBanner.imageURLStringsGroup = arr;
            }
            
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            self.hasNetWork=YES;
        }else if ([netWorkStaus isEqualToString:@"wan"] ||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.hasNetWork=YES;
        }
    }
}

#pragma mark - 获取缓存数据
- (void)getCacheData {
    //获取旧的数据
    NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:@"recommendSuperman"];
    if (configData) {
        NSDictionary *configDict = nil;
        configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
        self.baseUserView.userDataSouresArr = [SLSearchUser mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
        [self.baseUserView.collectionView reloadData];
        [self.baseUserView.userTableView reloadData];
        
    }
}
#pragma mark -获取推荐超人的数据
-(void)getData{
    if (self.isRefreshBanner) {
        [self getBannerData];
        self.isRefreshBanner=NO;
    }
//    [self getSuggestionData];
    self.page = 0;
    NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.page],@"pager":@"20",@"userType":@"0"};
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    [SLAPIHelper suggestions:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSLog(@"加载推荐超人数据1%@",data[@"result"]);

        self.baseUserView.userDataSouresArr = [SLSearchUser mj_objectArrayWithKeyValuesArray:data[@"result"]];
        //缓存
        NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
        [[NSUserDefaults standardUserDefaults] setObject:configData forKey:@"recommendSuperman"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // 刷新表格
//        [self.baseUserView.collectionView reloadData];
        [self.baseUserView.userTableView reloadData];
        
        if (self.baseUserView.userDataSouresArr.count<20) {
            [self.baseUserView.userTableView.mj_header endRefreshing];
            [self.baseUserView.userTableView.mj_footer setHidden:YES];
           
        }else{
            [self.baseUserView.userTableView.mj_footer setHidden:NO];
            [self.baseUserView.userTableView.mj_header endRefreshing];
            [self.baseUserView.userTableView.mj_footer resetNoMoreData];
        }
        //        [self.baseUserView.userTableView.mj_footer setHidden:NO];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }  failure:^(SLHttpRequestError *error) {
        // 结束刷新
        [self.baseUserView.userTableView.mj_header endRefreshing];
    }];
}
-(void)getMoreData{
    [self.baseUserView.userTableView.mj_footer setHidden:NO];
    self.page++;
    NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.page],@"pager":@"20",@"userType":@"0"};
    [SLAPIHelper suggestions:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
//        NSLog(@"加载推荐超人数据2%@",data[@"result"]);

        NSArray *moreTopics = [SLSearchUser mj_objectArrayWithKeyValuesArray:data[@"result"]];
        [self.baseUserView.userDataSouresArr addObjectsFromArray:moreTopics];
        // 刷新表格
        [self.baseUserView.userTableView reloadData];
        if ( moreTopics.count <20) {
            [self.baseUserView.userTableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [self.baseUserView.userTableView.mj_footer endRefreshing];
        }
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    } failure:^(SLHttpRequestError *error) {
        // 结束刷新
        [self.baseUserView.userTableView.mj_footer endRefreshing];
    }];
}
#pragma mark - 加载新鲜超人数据
-(void)getSuggestionData {
    NSDictionary *parameters = @{@"userType":@"1",@"pager":@"500"};
    NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:@"SuggestionData"];
    if (configData) {
        NSDictionary *configDict = nil;
        configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];

        self.baseUserView.suggestionArr = [SLSearchUser mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
    }
    [self.baseUserView.collectionView reloadData];
    [self.baseUserView.userTableView reloadData];
    
    [SLAPIHelper suggestions:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSLog(@"%@",data);
        NSArray *suggestionsData = [SLSearchUser mj_objectArrayWithKeyValuesArray:data[@"result"]];
        self.baseUserView.suggestionArr=(NSMutableArray *)suggestionsData;
        [MobClick event:@"home_recommend_user"];
        // 刷新表格
        NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
        [[NSUserDefaults standardUserDefaults] setObject:configData forKey:@"SuggestionData"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // 刷新表格
        [self.baseUserView.collectionView reloadData];
        [self.baseUserView.userTableView reloadData];
        
    } failure:^(SLHttpRequestError *failure) {
       
    }];
}
- (NSString *)getTimeNow  
{  
    NSString* date;  
     
    NSDateFormatter * formatter = [[NSDateFormatter alloc ] init];  
    //[formatter setDateFormat:@"YYYY.MM.dd.hh.mm.ss"];  
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss:SSS"];  
    date = [formatter stringFromDate:[NSDate date]];  
    NSString * timeNow = [[NSString alloc] initWithFormat:@"%@", date];
    return timeNow;  
}
#pragma mark --- 加载Banner数据
-(void)getBannerData{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:@"BannerImg"];
    NSArray *configBannerArr=[NSArray new];
    if (configData) {
        NSDictionary *configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
        configBannerArr = [SLBannerModel mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *dict in configDict[@"result"]) {
            [arr addObject:dict[@"image_url"]];
        }
        self.cycleBanner.imageURLStringsGroup = arr;
        
        [self.baseUserView.userTableView reloadData];
    }
    [SLAPIHelper getHomeBannerValue:nil success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSArray *bannerArr = [SLBannerModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
        self.bannerArr  =bannerArr;
        //缓存数据
        NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
        [[NSUserDefaults standardUserDefaults] setObject:configData forKey:@"BannerImg"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *dict in data[@"result"]) {
            [arr addObject:dict[@"image_url"]];
        }
        self.cycleBanner.imageURLStringsGroup = arr;
        [self.baseUserView.userTableView reloadData];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    } failure:^(SLHttpRequestError *failure) {
        self.isRefreshBanner=YES;
        
    }];
}
#pragma mark - 获取标签信息
-(void)getTagDataArr{
    NSArray *tagDataArr=@[@{@"name":@"大玩家",@"id":@(10000)},@{@"name":@"法律服务",@"id":@(40000)},@{@"name":@"医疗健康",@"id":@(30000)},@{@"name":@"旅游",@"id":@(110000)},@{@"name":@"体育竞技、运动健身",@"id":@(180000)},@{@"name":@"心理咨询、情感咨询",@"id":@(70000)},@{@"name":@"教育学习、留学移民",@"id":@(50000)},@{@"name":@"互联网",@"id":@(20000)},@{@"name":@"金融投资、保险理财、担保典当",@"id":@(60000)},@{@"name":@"企业管理、职场发展、商标产权",@"id":@(80000)},@{@"name":@"公共事业、政策法规",@"id":@(100000)}];
    self.tagDataArr=tagDataArr;

}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    SLBannerModel *model=self.bannerArr[index];
    if (model.navigation_url.length>0) {
        SLWebViewController *vc=[[SLWebViewController alloc] init];
        vc.url=model.navigation_url;
        vc.fromBanner=YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark - SLUserViewDelegate
-(void)SLUserView:(SLUserView *)baseView didSelectRowAtIndexPathWithModel:(SLSearchUser *)model{
    HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
    otherPerson.userId=model.id;
    otherPerson.didClickFollowBtn = ^(UIButton *btn){
        
        model.following = btn.selected?@"1":@"0";
//        [baseView.userTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        NSInteger follower = [model.followers integerValue];
        if (btn.selected) {
            follower++;
        }else{
            follower--;
        }
        model.followers = [NSString stringWithFormat:@"%ld",(long)follower];
//        NSLog(@"%@",NSStringFromClass([model.followers class]))
        [baseView.userTableView reloadData];
    };
    [self.navigationController pushViewController:otherPerson animated:YES];
}

#pragma mark ---- 获取我的标签信息
- (void)getMySettingTagStatus{
    _homeAlert =[[UIAlertView alloc] initWithTitle:@"提示" message:@"请添加个人标签" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"去添加", nil];
    _homeAlert.tag=300;
    [_homeAlert show];
}

#pragma mark - UI
- (void)setUpUI
{
    UIView *headView = [[UIView alloc] init];
    self.headView = headView;
    _headView.backgroundColor = SLColor(240, 240, 240);
    
    float proportion;
    if (ScreenW == 375) {
        proportion = 1.17;
    } else if(ScreenW == 414){
        proportion = 1.29;
    } else {
        proportion = 1.0;
    }
    
    headView.frame = CGRectMake(0, 0, ScreenW, 106*proportion + 2 * imageWH+50 + 2 * 56.4 * proportion+1);
    [self setUpClassesView:proportion];
    [self setUpActivityView:106*proportion + 2 * imageWH];
}
- (void)setUpClassesView:(CGFloat)proportion{
    UIView *classesView = [[UIView alloc] init];
    classesView.backgroundColor = SLColor(244, 244, 244);
    self.classesView = classesView;
    [self.headView addSubview:classesView];
    //    添加布局
    [classesView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(106*proportion);
        make.left.mas_equalTo(self.headView);
        make.right.mas_equalTo(self.headView);
        make.height.mas_equalTo(2 * imageWH);
    }];
    /********************选择分类的内容*************************/
    for (int i = 0; i < 8; i++) {
        UIButton *btnView=[UIButton buttonWithType:UIButtonTypeCustom];
        btnView.frame=CGRectMake( (imageWH ) * (i % cols), (imageWH) * (i / cols), imageWH, imageWH);
        btnView.tag = i;
        [btnView setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"classes_%d", i + 1]] forState:UIControlStateNormal];
        btnView.backgroundColor=[UIColor whiteColor];
        //        backImgView.image=[UIImage imageNamed:@"newPersonAvatar"];
        [btnView addTarget:self action:@selector(tapBtn:) forControlEvents:UIControlEventTouchUpInside];
        btnView.adjustsImageWhenHighlighted = NO;
        [classesView addSubview:btnView];
    }
}
- (CGFloat)getScreenHeight{
    float proportion;
    if (ScreenW == 375) {
        proportion = 1.17;
    } else if(ScreenW == 414){
        proportion = 1.29;
    } else {
        proportion = 1.0;
    }
    return 56.4 *proportion;
}
- (void)setUpActivityView:(CGFloat)y{
    CGFloat height = [self getScreenHeight];
    UIView *activeView = [[UIView alloc] init];
    activeView.backgroundColor = [UIColor whiteColor];
    activeView.frame = CGRectMake(0, y, screen_W, 50 + 2 * height+1.5);
    [self.headView addSubview:activeView];
    CGFloat width = (screen_W-0.5)/2.0;
//    CGFloat width = (screen_W-22)/2.0;
//    CGFloat height = 66;
    NSArray *imgArr = @[@"homeNewSuperman",@"homeRankingImg",@"homeCouponImg",@"homeHelp"];
    for (int i = 0; i<4; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        if (i%2==0) {
            btn.frame = CGRectMake(0, i/2*height+0.5+20, width , height);
        }else{
            btn.frame = CGRectMake(width+0.5, i/2*height+0.5+20, width , height);
        }
        [btn setBackgroundImage:[UIImage imageNamed:imgArr[i]] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:imgArr[i]] forState:UIControlStateHighlighted];
        btn.tag = i;
        [btn addTarget:self action:@selector(enterActiveDetailVC:) forControlEvents:UIControlEventTouchUpInside];
        [activeView addSubview:btn];
    }
    UIImageView *topLineImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 20, ScreenW, 0.5)];
    topLineImgView.backgroundColor = SLSepatorColor;
    [activeView addSubview:topLineImgView];
    UIImageView *topHLine = [[UIImageView alloc] initWithFrame:CGRectMake(width, 20+0.5, 0.5, height)];
    topHLine.backgroundColor = SLSepatorColor;
    [activeView addSubview:topHLine];
    UIImageView *bottomHLine = [[UIImageView alloc] initWithFrame:CGRectMake(width, 20+height+1, 0.5, height)];
    bottomHLine.backgroundColor = SLSepatorColor;
    [activeView addSubview:bottomHLine];
    UIImageView *midLineImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0.5+height+20, ScreenW, 0.5)];
    midLineImgView.backgroundColor = SLSepatorColor;
    [activeView addSubview:midLineImgView];
    UIImageView *bottomLineImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2 * height+1 + 20, ScreenW, 0.5)];
    bottomLineImgView.backgroundColor = SLSepatorColor;
    [activeView addSubview:bottomLineImgView];
}
- (void)enterActiveDetailVC:(UIButton *)btn{
    if (btn.tag == 0) {
        SLNewSuperManViewController *vc = [[SLNewSuperManViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (btn.tag == 1) {
        SLRankingViewController *vc = [[SLRankingViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (btn.tag == 2){
        SLWebViewController *vc = [[SLWebViewController alloc] init];
        vc.url = @"http://h5.superloop.com.cn/voucher/invite.html";
        vc.fromBanner = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (btn.tag == 3){
        SLWebViewController *vc = [[SLWebViewController alloc] init];
        vc.url = @"http://h5.superloop.com.cn/piazza/1.5/way.html";
        vc.fromBanner = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)setUpNav
{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    UIView *hView=[[UIView alloc] initWithFrame:CGRectMake(0, navView.frame.size.height-1, ScreenW, 0.5)];
    hView.backgroundColor=[UIColor lightGrayColor];
    [navView addSubview:hView];
    
    //添加定位按钮
//    SLLocationButton *locationBtn = [SLLocationButton buttonWithType:UIButtonTypeCustom];
//    locationBtn.frame=CGRectMake(10, 27, 42, 30);
//    [locationBtn setTitle:@"全国" forState:UIControlStateNormal];
//    locationBtn.titleLabel.font = [UIFont systemFontOfSize:14];
//    [locationBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [locationBtn setImage:[UIImage imageNamed:@"圆角矩形-2"] forState:UIControlStateNormal];
//    [locationBtn addTarget:self action:@selector(selectLocation) forControlEvents:UIControlEventTouchUpInside];
//    self.btn = locationBtn;
//    [navView addSubview:locationBtn];
    
    //搜索框
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.frame  = CGRectMake(10, 27, ScreenW-10-50, 30);
//    searchBtn.frame  = CGRectMake(10+locationBtn.frame.size.width+8, 27, ScreenW-locationBtn.frame.size.width-10-12-10-35, 30);
    searchBtn.layer.cornerRadius = 15;
    searchBtn.layer.masksToBounds =YES;
    searchBtn.layer.borderColor =  UIColorFromRGB(0x9e7f7b).CGColor;
    searchBtn.layer.borderWidth = 0.5;
    searchBtn.titleLabel.font=[UIFont systemFontOfSize:13];
    [searchBtn setTitle:@"搜索超人、话题、行业" forState:UIControlStateNormal];
    [searchBtn setTitleColor:UIColorFromRGB(0x9e7f7b) forState:UIControlStateNormal];
//    searchBtn.backgroundColor=SLColor(238, 244, 243);
    [searchBtn addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:searchBtn];
    
    UIImageView *imgVIew = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW-10-50-15, 35, 15, 15)];
    imgVIew.image = [UIImage imageNamed:@"search"];
    _imgView = imgVIew;
    [navView addSubview:imgVIew];
    
    //扫一扫
    UIButton *qrCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [qrCodeBtn setImage:[UIImage imageNamed:@"qrCode"] forState:UIControlStateNormal];
    qrCodeBtn.frame=CGRectMake(ScreenW-50, 22, 50, 40);
    qrCodeBtn.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 0);

    //SLQRCodeBtn *qrCodeBtn=[SLQRCodeBtn buttonWithType:UIButtonTypeCustom];
    //qrCodeBtn.imgView.image = [UIImage imageNamed:@"qrCode"];
    //qrCodeBtn.titleLab.text=@"扫一扫";
    //[qrCodeBtn setImage:[UIImage imageNamed:@"qrCode"] forState:UIControlStateNormal];
    [qrCodeBtn addTarget:self action:@selector(qrCodeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:qrCodeBtn];

}
-(void)qrCodeBtnClick{
    SLQRCodeViewController *qrVC=[[SLQRCodeViewController alloc] init];
    [self.navigationController pushViewController:qrVC animated:YES];
}
#pragma mark - buttn点击事情
//点击搜索框
- (void)searchClick
{
    SLSearchViewController *vc=[[SLSearchViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
//点击定位button
- (void)selectLocation{
    SLLocationViewController *location = [[SLLocationViewController alloc] init];
    location.valueBlcok = ^(NSString *str){
        CGSize size;
        UILabel *lable = [[UILabel alloc]init];
        size = [lable boundingRectWithString:str withSize:CGSizeMake(80, 30)withFont:14];
        [self.btn setTitle:str forState:UIControlStateNormal];
        self.btn.frame  = CGRectMake(0, 0, size.width +10, 30);
        _imgView.frame = CGRectMake(self.view.frame.size.width-self.btn.bounds.size.width-55, 8, 15, 15);
    };
    //将定位成功之后的城市数据传递过去
    location.labelCityStr = self.cityStr;
    location.isHomeLocation=YES;
    [self.navigationController pushViewController:location animated:YES];
}


//点击分类跳转
- (void)tapBtn:(UIButton *)tapBtn{
    SLCatDetailViewController *detailVC = [[SLCatDetailViewController alloc] init];
    if (tapBtn.tag<7) {
        detailVC.navTitle = self.tagDataArr[tapBtn.tag][@"name"];
        detailVC.catId =self.tagDataArr[tapBtn.tag][@"id"];
        [self.navigationController pushViewController:detailVC animated:YES];
    }else{
        SLMoreCategoryViewController *catagory = [[SLMoreCategoryViewController alloc] init];
        catagory.type = @"isHome";
        [self.navigationController pushViewController:catagory animated:YES];
    }
}

- (void)SLGiveEvaluationView:(SLGiveEvaluationView *)evaluationView didClickBtnAtIndex:(NSInteger)index{
    if (index == 3) {
        NSString  *str;
        str = [NSString  stringWithFormat:@"itms-apps://itunes.apple.com/app/id%d",1130659569];
        [[UIApplication  sharedApplication] openURL:[NSURL  URLWithString:str]];
    }else if(index == 2){
        SLQuestionUsViewController *questionUs = [[SLQuestionUsViewController alloc] init];
        [self.navigationController pushViewController:questionUs animated:YES];
    }
    [self.giveEvaluationView removeFromSuperview];
    self.giveEvaluationView = nil;
}
- (void)showGiveEvaluationView{
    self.giveEvaluationView = [[SLGiveEvaluationView alloc] init];
    self.giveEvaluationView.delegate = self;
    [[UIApplication sharedApplication].keyWindow addSubview:self.giveEvaluationView];
}

@end
