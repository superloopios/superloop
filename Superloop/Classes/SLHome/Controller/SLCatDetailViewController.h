//
//  SLCatDetailViewController.h
//  Superloop
//
//  Created by 张梦川 on 16/5/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface SLCatDetailViewController : BaseViewController

@property (nonatomic,  weak)UIView *quitLoginView;
@property (nonatomic,  weak)UIView *noNetWorkView;  //没网制空页的view
@property (nonatomic,strong)NSNumber *catId;
@property (nonatomic,  copy)NSString *navTitle;
@property (nonatomic,  copy)NSString *cateTitle;

@end
