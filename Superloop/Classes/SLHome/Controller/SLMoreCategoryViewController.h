//
//  SLMoreCategoryViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/28.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface SLMoreCategoryViewController : BaseViewController

@property(nonatomic,copy)NSString *type;
@property (nonatomic,strong)SLJavascriptBridgeEvent *event;
@property (nonatomic,strong)NSDictionary *userData;
@property (nonatomic,weak)UIView *noNetWorkView;  //没网制空页的view
@property(nonatomic,copy)NSNumber *tagsType;
@property (nonatomic ,copy) void(^changeTags)(NSNumber *type,NSArray *tags);

@end
