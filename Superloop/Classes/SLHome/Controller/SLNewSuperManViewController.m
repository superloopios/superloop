//
//  SLNewSuperManViewController.m
//  Superloop
//
//  Created by xiaowu on 16/11/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLNewSuperManViewController.h"
#import "HomePageViewController.h"
#import "SLUserView.h"
#import "SLUserTableViewCell.h"
#import "SLSearchUser.h"
#import "SLAPIHelper.h"
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"

@interface SLNewSuperManViewController ()<SLUserViewDelegate>

@property (nonatomic, strong) SLUserView *userView;

@property (nonatomic, assign) NSInteger page;


@end

@implementation SLNewSuperManViewController

-(SLUserView *)userView{
    if (!_userView) {
        _userView = [[SLUserView alloc] initWithFrame:CGRectMake(0, 64, ScreenW, ScreenH-64)];
        _userView.delegate = self;
        [self.view addSubview:_userView];
    }
    return _userView;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [MobClick beginLogPageView:@"SLNewSuperManViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLNewSuperManViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = SLColor(244, 244, 244);
    [self setUpNav];
    [self setUpRefresh];
}
- (void)setUpRefresh {
    self.userView.userTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getSuggestionData)];
    self.userView.userTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreData)];
    self.userView.userTableView.mj_footer.hidden = YES;
    [self.userView.userTableView.mj_header beginRefreshing];
}

- (void)setUpNav{
    UIView *navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor = [UIColor whiteColor];
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab = [[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment = NSTextAlignmentCenter;
    nameLab.text = @"新鲜超人";
    nameLab.font = [UIFont systemFontOfSize:18];
    nameLab.textColor = [UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor = SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 加载新鲜超人数据
-(void)getSuggestionData {
    self.page = 0;
    NSDictionary *parameters = @{@"userType":@"1",@"pager":@"20",@"page":[NSString stringWithFormat:@"%ld",(long)self.page]};
    NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:@"SuggestionData"];
    if (configData) {
        NSDictionary *configDict = nil;
        configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
        self.userView.userDataSouresArr = [SLSearchUser mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
        [self.userView.userTableView reloadData];
    }
    
    [SLAPIHelper suggestions:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        self.page++;
        NSLog(@"%@",data);
        NSArray *suggestionsData = [SLSearchUser mj_objectArrayWithKeyValuesArray:data[@"result"]];
        self.userView.userDataSouresArr = (NSMutableArray *)suggestionsData;
        [MobClick event:@"home_recommend_user"];
        // 刷新表格
        NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
        [[NSUserDefaults standardUserDefaults] setObject:configData forKey:@"SuggestionData"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // 刷新表格
        [self.userView.userTableView reloadData];
        if (suggestionsData.count<20) {
            [self.userView.userTableView.mj_header endRefreshing];
            [self.userView.userTableView.mj_footer setHidden:YES];
            
        }else{
            [self.userView.userTableView.mj_footer setHidden:NO];
            [self.userView.userTableView.mj_header endRefreshing];
            [self.userView.userTableView.mj_footer resetNoMoreData];
        }
        
    } failure:^(SLHttpRequestError *failure) {
        
    }];
}
-(void)getMoreData{
    
    NSDictionary *parameters = @{@"userType":@"1",@"pager":@"20",@"page":[NSString stringWithFormat:@"%ld",(long)self.page]};
    
    [SLAPIHelper suggestions:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        self.page++;
        NSArray *suggestionsData = [SLSearchUser mj_objectArrayWithKeyValuesArray:data[@"result"]];
        [self.userView.userDataSouresArr addObjectsFromArray:suggestionsData];
        // 刷新表格
        [self.userView.userTableView reloadData];
        if ( suggestionsData.count <20) {
            [self.userView.userTableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [self.userView.userTableView.mj_footer endRefreshing];
        }
        
    } failure:^(SLHttpRequestError *failure) {
        
    }];
}

#pragma mark - SLUserViewDelegate
-(void)SLUserView:(SLUserView *)baseView didSelectRowAtIndexPathWithModel:(SLSearchUser *)model{
    HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
    otherPerson.userId=model.id;
    [self.navigationController pushViewController:otherPerson animated:YES];
}
@end
