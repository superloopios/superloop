//
//  SLSelectCityiewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSelectCityViewController.h"
#import "SLCity.h"
#import <MJExtension.h>
#import "SLTabBarViewController.h"
#import "SLSearchTopicViewController.h"
#import "HomePageViewController.h"

@interface SLSelectCityViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *modelArray;
@end
@implementation SLSelectCityViewController


- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.frame = CGRectMake(0, 64, screen_W, screen_H - 64);
//        if (self.isMineExperienceLocation||self.isMineBasicInfoLocation) {
//            _tableView.frame = CGRectMake(0, 44, screen_W, screen_H - 64);
//        }else{
//            _tableView.frame = CGRectMake(0, 0, screen_W, screen_H - 64);
//        }
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLSelectCityViewController"];
//    if (self.isMineBasicInfoLocation || self.isMineExperienceLocation) {
//        [self.navigationController setNavigationBarHidden:YES animated:NO];
//    }else{
//        [self.navigationController setNavigationBarHidden:NO animated:NO];
//    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLSelectCityViewController"];
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"选择城市";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}

- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    [self.view addSubview:self.tableView];
    [self setUpNav];

//    if (self.isMineBasicInfoLocation || self.isMineExperienceLocation) {
//        [self setUpNav];
//    }
    self.modelArray = [SLCity mj_objectArrayWithKeyValuesArray:self.dataArray];
    if (self.isMineBasicInfoLocation  ==YES) {
        [self.modelArray removeObjectAtIndex:0];
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.modelArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    SLCity *model = self.modelArray[indexPath.row];
//    if (self.isMineBasicInfoLocation  ==NO) {
//        if (indexPath.row == 0){
//            cell.textLabel.font = [UIFont boldSystemFontOfSize:17];
//  
//        }
//    }
    cell.textLabel.text = model.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SLCity *model = self.modelArray[indexPath.row];
    [self.Delegate sendCityName:model.name];
    [self.Delegate sendCityName:model.name cityId:model.code];
    
    if (_tag == 1000 | _tag == 2000||self.isRegisterLocation) {
        UIViewController * viewVC = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 3];
        //修改完成刷新h5页面
//        [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//            
//        }];
        [self.navigationController popToViewController:viewVC animated:YES];
    }else
    {
        //修改完成刷新h5页面
//        [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//            
//        }];
        HomePageViewController *newHome;
        
        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isKindOfClass:[HomePageViewController class]]) {
                newHome = (HomePageViewController *)vc;
                break;
            }
        }
        [self.navigationController popToViewController:newHome animated:YES];
        
        
    }
    
}

@end
