//
//  SLMoreCategoryViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/28.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLMoreCategoryViewController.h"
#import <AFNetworking.h>
#import "SLCategoryModel.h"
#import "SLCategoryCell.h"
#import <MJExtension.h>
#import "SLChildCateCell.h"
#import "SLCatDetailViewController.h"
#import "AppDelegate.h"
#import "SLAPIHelper.h"
#import "SLValueUtils.h"
@interface SLMoreCategoryViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UIButton *ensureBtn;
}
@property (nonatomic ,strong)UITableView *tableView;
@property (nonatomic, strong) NSArray *categorys;
@property (nonatomic,strong)NSDictionary *dataWithTime;
@property (nonatomic,copy)NSString *dateStr;
@property (nonatomic,copy)NSString *selectedModelTitleStr;
@end

@implementation SLMoreCategoryViewController{
    
    NSArray *_imgList;
    NSMutableArray *_tagsArr;
    NSArray *_primary_tags;
    NSArray *_secondary_tags;
    NSMutableArray *_primary_tagsArr;
    NSMutableArray *_secondary_tagsArr;
    int _falg;
    NSArray *_arrColor1;
    NSArray *_arrColor2;
}
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenW, ScreenH-64) style:UITableViewStylePlain];
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 1, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        //        [self.tableView registerNib:[UINib nibWithNibName:@"SLChildCateCell" bundle:nil] forCellReuseIdentifier:@"childCell"];
        [self.tableView registerNib:[UINib nibWithNibName:@"SLCategoryCell" bundle:nil] forCellReuseIdentifier:@"cateCell"];
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //[self.navigationController setNavigationBarHidden:NO animated:NO];
    [HUDManager hideHUDView];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _secondary_tagsArr = [NSMutableArray array];
    _primary_tagsArr = [NSMutableArray array];
    [self setNav];
    NSLog(@"%@",self.userData);
    // 加载数据
    
    _falg = -1;
    //self.navigationItem.title = @"全部分类";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    NSArray *arrImg = @[@"superPlayer_select",@"internet_select",@"medichHealth_select",@"lawService_select",@"study_select",@"financial_select",@"friend_select",@"company_select",@"law_select",@"tour_select",@"game_select",@"publicRelation_select",@"food_select",@"car_select",@"buliding_select",@"live_select",@"puplicPerson_select",@"animal_select",@"movie_select",@"fashion_select",@"antique_select",@"science_select",@"produce_select"];
    _imgList = arrImg;
    
    NSArray *arrColor1 = @[SLColor(240, 218, 230),SLColor(212, 229, 243),SLColor(241, 228, 228),SLColor(224, 235, 249),SLColor(243, 237, 229),SLColor(236, 229, 208),SLColor(238, 217, 226),SLColor(203, 216, 239),SLColor(240, 234, 217),SLColor(223, 234, 203),SLColor(235, 231, 215),SLColor(206, 228, 229),SLColor(212, 233, 217),SLColor(207, 231, 230),SLColor(207, 215, 231),SLColor(230, 236, 203),SLColor(230, 230, 230),SLColor(232, 230, 213),SLColor(235, 209, 224),SLColor(214, 221, 232),SLColor(237, 228, 214),SLColor(239, 219, 219),SLColor(221, 237, 238),SLColor(225, 224, 241)];
    _arrColor1 = arrColor1;
    NSArray *arrColor2 = @[SLColor(253, 246, 251),SLColor(242, 250, 255),SLColor(253, 246, 246),SLColor(247, 254, 255),SLColor(255, 252, 243),SLColor(255, 254, 239),SLColor(252, 241, 247),SLColor(241, 245, 255),SLColor(255, 250, 239),SLColor(248, 253, 236),SLColor(255, 251, 241),SLColor(240, 254, 254),SLColor(243, 254, 246),SLColor(244, 254, 254),SLColor(245, 247, 255),SLColor(251, 254, 239),SLColor(248, 248, 248),SLColor(255, 252, 243),SLColor(252, 243, 249),SLColor(245, 249, 255),SLColor(254, 250, 241),SLColor(253, 245, 244),SLColor(243, 253, 255),SLColor(244, 245, 255)];
    _arrColor2 = arrColor2;
    if ([self.type isEqualToString:@"isHome"]== false) {
        
        //        UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //        rightBtn.titleLabel.font=[UIFont systemFontOfSize:15];
        //        [rightBtn setFrame:CGRectMake(0, 0, 40, 30)];
        //        rightBtn.backgroundColor=[UIColor redColor];
        //        [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
        //        [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //        [rightBtn addTarget:self
        //                       action:@selector(submitAction)
        //             forControlEvents:UIControlEventTouchUpInside];
        //        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
        //self.navigationItem.rightBarButtonItem = rightItem;
        
//        UIBarButtonItem *rightItem=[[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(submitAction)];
//        NSDictionary *dict1 = @{
//                                NSFontAttributeName : [UIFont systemFontOfSize:16],
//                                NSForegroundColorAttributeName : [UIColor blackColor]
//                                };
//        [rightItem setTitleTextAttributes:dict1 forState:UIControlStateNormal];
//        self.navigationItem.rightBarButtonItem=rightItem;
        
    }
    _tagsArr = [NSMutableArray array];
    
    [self loadData];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
    
}
-(void)setNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn  setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *titleLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    titleLab.text=@"全部分类";
    titleLab.textAlignment=NSTextAlignmentCenter;
    titleLab.textColor=[UIColor blackColor];
    titleLab.font=[UIFont systemFontOfSize:17];
    [navView addSubview:titleLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=[UIColor lightGrayColor];
    [navView addSubview:cutImgView];
    
    if ([self.type isEqualToString:@"isHome"]== false) {
    
        ensureBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        ensureBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
        [ensureBtn addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
        [ensureBtn setTitle:@"保存" forState:UIControlStateNormal];
        [ensureBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        ensureBtn.titleLabel.font=[UIFont systemFontOfSize:15];
        [navView addSubview:ensureBtn];
    }
    
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [HUDManager hideHUDView];
}
-(void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)submit{
    ensureBtn.enabled=NO;
    //先将未到时间执行前的任务取消。
    
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(submitAction) object:nil];
    [self performSelector:@selector(submitAction) withObject:nil afterDelay:0.3f];
}

-(void)submitAction{
//    if (_tagsArr !=nil) {
//        
//        NSString *tag_id = @"";
//        if (_tagsArr.count>0) {
//            tag_id = [NSString stringWithFormat:@"%@",_tagsArr[0]];
//            for (int i= 1; i<_tagsArr.count; i++) {
//                tag_id = [tag_id stringByAppendingFormat:@",%@",_tagsArr[i]];
//            }
//        }
//        
//        
//        NSLog(@"tag_id--------%@",tag_id);
//        [SLAPIHelper addTags:tag_id success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
//            ensureBtn.enabled = YES;
//            NSString *code =  data[@"code"];
//            //修改完成刷新h5页面
//            [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//                
//            }];
//            if (code.integerValue == 0) {
//                [self.navigationController popViewControllerAnimated:YES];
//            }
//        } failure:^(SLHttpRequestError *failure) {
////            [self showHUDmessage:@"添加失败！"];
//            ensureBtn.enabled = YES;
//        }];
//    }
    [HUDManager showLoadingHUDView:self.view withText:@""];
    NSString *tag_id = @"";
    if ([self.tagsType isEqual:@1]) {
        if (_primary_tagsArr.count>0) {
            for (int i = 0; i<_primary_tagsArr.count; i++) {
                tag_id = [tag_id stringByAppendingFormat:@"%@",_primary_tagsArr[i][@"id"]];
                if (i != _primary_tagsArr.count-1) {
                     tag_id = [tag_id stringByAppendingFormat:@","];
                }
            }
            
        }
    }else if([self.tagsType isEqual:@2]){
        if (_secondary_tagsArr.count>0) {
            for (int i = 0; i<_secondary_tagsArr.count; i++) {
                tag_id = [tag_id stringByAppendingFormat:@"%@",_secondary_tagsArr[i][@"id"]];
                if (i != _secondary_tagsArr.count-1) {
                    tag_id = [tag_id stringByAppendingFormat:@","];
                }
            }
            
        }
    }
  
    [SLAPIHelper addTags:tag_id tagsType:self.tagsType success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        ensureBtn.enabled = YES;
        NSString *code =  data[@"code"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeData" object:nil];

        if (code.integerValue == 0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [HUDManager hideHUDView];
                if (self.changeTags) {
                    if ([self.tagsType isEqual:@1]) {
                        self.changeTags(self.tagsType,_primary_tagsArr);
                       
                    }
                    if ([self.tagsType isEqual:@2]) {
                        self.changeTags(self.tagsType,_secondary_tagsArr);
                       
                    }
                    
                }
                [self.navigationController popViewControllerAnimated:YES];
            });
            
        }
        
    } failure:^(SLHttpRequestError *failure) {
        //
        ensureBtn.enabled = YES;
        [HUDManager hideHUDView];
        [self showHUDmessage:@"操作失败！"];
    }];
}

- (void)loadData
{
    if (self.userData) {
        NSDictionary *dict = self.userData;
        _primary_tags = dict[@"primary_tags"];
        _secondary_tags = dict[@"secondary_tags"];
        [_primary_tagsArr addObjectsFromArray:_primary_tags];
        [_secondary_tagsArr addObjectsFromArray:_secondary_tags];
    }
    
    NSData *categoryData=[[NSUserDefaults standardUserDefaults] objectForKey:@"categoryKey"];
    if (categoryData) {
        _dateStr=[NSKeyedUnarchiver unarchiveObjectWithData:categoryData][@"dateStr"];
    }
    if ([SLValueUtils  periodTwoHours:_dateStr]) {
        NSDictionary *configDict = [NSKeyedUnarchiver unarchiveObjectWithData:categoryData];
        _categorys = [SLCategoryModel mj_objectArrayWithKeyValuesArray:configDict[@"configData"][@"result"]];
        
        [self.tableView reloadData];
    }else{
        [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow
                              withText:@""];
        
        [SLAPIHelper getAllTagsValue:nil success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            
            [HUDManager hideHUDView];
            _categorys = [SLCategoryModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            //缓存数据
            NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];
            NSTimeInterval timeStamp=[date timeIntervalSince1970]*1000;
            NSString *timeStampStr=[[NSString stringWithFormat:@"%f",timeStamp] componentsSeparatedByString:@"."][0];
            NSDictionary *dataWithTime=@{@"dateStr":timeStampStr,@"configData":data};
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:dataWithTime];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:@"categoryKey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self.tableView reloadData];
            
        } failure:^(SLHttpRequestError *failure) {
            [HUDManager hideHUDView];
        }];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SLCategoryModel *model = self.categorys[indexPath.section];
    //SLCategoryModel *model1 = self.categorys[indexPath.section];
    NSInteger rows = (model.children.count - 1) / 4 +1 + 1;
    // NSInteger rows = (model.children.count - 2) / 4 +1 + 1;
    if (model.isOpen) {
        
        if (indexPath.row == 0 || indexPath.row == rows-1) {
            return 60;
        } else {
            
            return 50;
        }
        
    }
    return 50;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  _categorys.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    return 1;
    SLCategoryModel *model = self.categorys[section];
    if (model.isOpen) {
        
        if ([self.type isEqualToString:@"isHome"]== true) {
            return (model.children.count - 1) / 4 +1 + 1;
            //return (model.children.count - 2) / 4 +1 + 1;
        } else {
            return (model.children.count - 2) / 4 +1 + 1;
        }
    }else
    {
        return 1;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    SLCategoryModel *model = self.categorys[indexPath.section];
    
    //打开cell超出屏幕范围自动滑到最底部
    if (model.isOpen) {
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
    }
    
    if (indexPath.row == 0) {
        
        SLCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cateCell" forIndexPath:indexPath];
        cell.label.text = model.name;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 10)];
        
        
        if (model.isOpen) {
            cell.arrowBtn.selected = YES;
            //            cell.iconImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_light",_imgList[indexPath.section]]];
            //             cell.iconImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_select",_imgList[indexPath.section]]];
            view.backgroundColor = SLColor(238, 238, 238);
            
        }else
        {
            view.backgroundColor = [UIColor whiteColor];
            
            cell.arrowBtn.selected = NO;
            cell.iconImage.image = [UIImage imageNamed:_imgList[indexPath.section]];
        }
        [cell.contentView addSubview:view];
        
        return cell;
    }
    
    SLChildCateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"childCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SLChildCateCell" owner:self options:nil]lastObject];
    }
    //button文字换行
    cell.firstBtn.titleLabel.numberOfLines=0;
    cell.secondBtn.titleLabel.numberOfLines=0;
    cell.thirdBtn.titleLabel.numberOfLines=0;
    cell.forthBtn.titleLabel.numberOfLines=0;
    cell.firstBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    cell.secondBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    cell.thirdBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    cell.forthBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    
    cell.firstBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
    cell.secondBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
    cell.thirdBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
    cell.forthBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
    //    [cell.firstBtn setBackgroundColor:SLColor(250, 250, 250)];
    //    [cell.secondBtn setBackgroundColor:SLColor(250, 250, 250)];
    //    [cell.thirdBtn setBackgroundColor:SLColor(250, 250, 250)];
    //    [cell.forthBtn setBackgroundColor:SLColor(250, 250, 250)];
    cell.firstBtn.titleEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    cell.secondBtn.titleEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    cell.thirdBtn.titleEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    cell.forthBtn.titleEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    
    NSArray *array = model.children;
    
    NSInteger index0 = (indexPath.row - 1) * 4;
    NSInteger index1 = index0 + 1;
    NSInteger index2 = index0 + 2;
    NSInteger index3 = index0 + 3;
    if ([self.type isEqualToString:@"isHome"] == false) {
        NSMutableArray *arr = [NSMutableArray array];
        for (int i=1; i<model.children.count; i++) {
            //        for (int i=0; i<model.children.count; i++) {
            [arr addObject:model.children[i]];
        }
        
        cell.firstBtn.selected = false;
        cell.secondBtn.selected = false;
        cell.thirdBtn.selected = false;
        cell.forthBtn.selected = false;
        //        [cell.firstBtn setBackgroundImage:[UIImage imageNamed:@"login_checkbox_selected_"] forState:UIControlStateSelected];
        //        [cell.secondBtn setBackgroundImage:[UIImage imageNamed:@"login_checkbox_selected_"] forState:UIControlStateSelected];
        //        [cell.thirdBtn setBackgroundImage:[UIImage imageNamed:@"login_checkbox_selected_"] forState:UIControlStateSelected];
        //        [cell.forthBtn setBackgroundImage:[UIImage imageNamed:@"login_checkbox_selected_"] forState:UIControlStateSelected];
        [cell.firstBtn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        [cell.secondBtn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        [cell.thirdBtn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        [cell.forthBtn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        
        
        [cell.firstBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
        [cell.secondBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
        [cell.thirdBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
        [cell.forthBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
        if (arr.count == index1) {
            
            [cell.firstBtn setTitle:arr[(indexPath.row - 1) * 4 + 0][@"name"] forState:UIControlStateNormal];
            NSString *btnTag = arr[index0][@"id"];
            cell.firstBtn.tag = btnTag.intValue;
            [cell.firstBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            //            [cell.secondBtn setTitle:@"" forState:UIControlStateNormal];
            //            [cell.thirdBtn setTitle:@"" forState:UIControlStateNormal];
            //            [cell.forthBtn setTitle:@"" forState:UIControlStateNormal];
            cell.secondBtn.hidden = YES;
            cell.thirdBtn.hidden = YES;
            cell.forthBtn.hidden = YES;
        }else if (arr.count == index2)
        {
            [cell.firstBtn setTitle:arr[(indexPath.row - 1) * 4 + 0][@"name"] forState:UIControlStateNormal];
            NSString *btnTag = arr[index0][@"id"];
            cell.firstBtn.tag = btnTag.intValue;
            [cell.firstBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.secondBtn setTitle:arr[(indexPath.row - 1) * 4 + 1][@"name"] forState:UIControlStateNormal];
            NSString *btnTag1 = arr[index1][@"id"];
            cell.secondBtn.tag = btnTag1.intValue;
            [cell.secondBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            //            [cell.thirdBtn setTitle:@"" forState:UIControlStateNormal];
            //            [cell.forthBtn setTitle:@"" forState:UIControlStateNormal];
            cell.thirdBtn.hidden = YES;
            cell.forthBtn.hidden = YES;
        }else if (arr.count == index3)
        {
            [cell.firstBtn setTitle:arr[(indexPath.row - 1) * 4 + 0][@"name"] forState:UIControlStateNormal];
            NSString *btnTag = arr[index0][@"id"];
            cell.firstBtn.tag = btnTag.intValue;
            [cell.firstBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.secondBtn setTitle:arr[(indexPath.row - 1) * 4 + 1][@"name"] forState:UIControlStateNormal];
            NSString *btnTag1 = arr[index1][@"id"];
            cell.secondBtn.tag = btnTag1.intValue;
            [cell.secondBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.thirdBtn setTitle:arr[(indexPath.row - 1) * 4 + 2][@"name"] forState:UIControlStateNormal];
            NSString *btnTag2 = arr[index2][@"id"];
            cell.thirdBtn.tag = btnTag2.intValue;
            [cell.thirdBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            //            [cell.forthBtn setTitle:@"" forState:UIControlStateNormal];
            cell.forthBtn.hidden = YES;
        }else
        {
            [cell.firstBtn setTitle:arr[(indexPath.row - 1) * 4 + 0][@"name"] forState:UIControlStateNormal];
            NSString *btnTag = arr[index0][@"id"];
            cell.firstBtn.tag = btnTag.intValue;
            [cell.firstBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.secondBtn setTitle:arr[(indexPath.row - 1) * 4 + 1][@"name"] forState:UIControlStateNormal];
            NSString *btnTag1 = arr[index1][@"id"];
            cell.secondBtn.tag = btnTag1.intValue;
            [cell.secondBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.thirdBtn setTitle:arr[(indexPath.row - 1) * 4 + 2][@"name"] forState:UIControlStateNormal];
            NSString *btnTag2 = arr[index2][@"id"];
            cell.thirdBtn.tag = btnTag2.intValue;
            [cell.thirdBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.forthBtn setTitle:arr[(indexPath.row - 1) * 4 + 3][@"name"] forState:UIControlStateNormal];
            NSString *btnTag3 = arr[index3][@"id"];
            cell.forthBtn.tag = btnTag3.intValue;
            [cell.forthBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        if ([self.tagsType isEqual:@1]) {
            if (_primary_tags!=nil) {
                for (int i=0; i<_primary_tags.count; i++) {
                    if ([_primary_tags[i][@"name"] isEqualToString:cell.firstBtn.titleLabel.text] == true) {
                        cell.firstBtn.selected= true;
                    } else if ([_primary_tags[i][@"name"] isEqualToString:cell.secondBtn.titleLabel.text] == true) {
                        cell.secondBtn.selected= true;
                    } else if ([_primary_tags[i][@"name"] isEqualToString:cell.thirdBtn.titleLabel.text] == true) {
                        cell.thirdBtn.selected= true;
                    } else if([_primary_tags[i][@"name"] isEqualToString:cell.forthBtn.titleLabel.text] == true) {
                        cell.forthBtn.selected= true;
                    }
                }
            }
            if(_primary_tagsArr!=nil){
                NSNumber *btnTag;
                for (int i=0; i<_primary_tagsArr.count; i++) {
                    btnTag = _primary_tagsArr[i][@"id"];
                    if (btnTag.integerValue== cell.firstBtn.tag) {
                        cell.firstBtn.selected= true;
                    } else if (btnTag.integerValue == cell.secondBtn.tag) {
                        cell.secondBtn.selected= true;
                    } else if (btnTag.integerValue == cell.thirdBtn.tag) {
                        cell.thirdBtn.selected= true;
                    } else if(btnTag.integerValue == cell.forthBtn.tag) {
                        cell.forthBtn.selected= true;
                    }
                }
                
            }
            if (_secondary_tags!=nil) {
                for (int i=0; i<_secondary_tags.count; i++) {
                    if ([_secondary_tags[i][@"name"] isEqualToString:cell.firstBtn.titleLabel.text] == true) {
                        cell.firstBtn.enabled= NO;
                        cell.firstBtn.backgroundColor =SLColor(230, 230, 230);
                    } else if ([_secondary_tags[i][@"name"] isEqualToString:cell.secondBtn.titleLabel.text] == true) {
                        cell.secondBtn.enabled= NO;
                        cell.secondBtn.backgroundColor = SLColor(230, 230, 230);
                    } else if ([_secondary_tags[i][@"name"] isEqualToString:cell.thirdBtn.titleLabel.text] == true) {
                        cell.thirdBtn.enabled= NO;
                        cell.thirdBtn.backgroundColor =SLColor(230, 230, 230);
                    } else if([_secondary_tags[i][@"name"] isEqualToString:cell.forthBtn.titleLabel.text] == true) {
                        cell.forthBtn.enabled= NO;
                        cell.forthBtn.backgroundColor = SLColor(230, 230, 230);
                    }
                }
            }
            if(_secondary_tagsArr!=nil){
                NSNumber *btnTag;
                for (int i=0; i<_secondary_tagsArr.count; i++) {
                    btnTag = _secondary_tagsArr[i][@"id"];
                    if (btnTag.integerValue== cell.firstBtn.tag) {
                        cell.firstBtn.enabled= NO;
                        cell.firstBtn.backgroundColor = SLColor(230, 230, 230);
                    } else if (btnTag.integerValue == cell.secondBtn.tag) {
                        cell.secondBtn.enabled= NO;
                        cell.secondBtn.backgroundColor = SLColor(230, 230, 230);
                    } else if (btnTag.integerValue == cell.thirdBtn.tag) {
                        cell.thirdBtn.enabled= NO;
                        cell.thirdBtn.backgroundColor = SLColor(230, 230, 230);
                    } else if(btnTag.integerValue == cell.forthBtn.tag) {
                        cell.forthBtn.enabled= NO;
                        cell.forthBtn.backgroundColor = SLColor(230, 230, 230);
                    }
                }
                
            }
        }else if([self.tagsType isEqual:@2]){
            if (_secondary_tags!=nil) {
                for (int i=0; i<_secondary_tags.count; i++) {
                    if ([_secondary_tags[i][@"name"] isEqualToString:cell.firstBtn.titleLabel.text] == true) {
                        cell.firstBtn.selected= true;
                    } else if ([_secondary_tags[i][@"name"] isEqualToString:cell.secondBtn.titleLabel.text] == true) {
                        cell.secondBtn.selected= true;
                    } else if ([_secondary_tags[i][@"name"] isEqualToString:cell.thirdBtn.titleLabel.text] == true) {
                        cell.thirdBtn.selected= true;
                    } else if([_secondary_tags[i][@"name"] isEqualToString:cell.forthBtn.titleLabel.text] == true) {
                        cell.forthBtn.selected= true;
                    }
                }
            }
            if(_secondary_tagsArr!=nil){
                NSNumber *btnTag;
                for (int i=0; i<_secondary_tagsArr.count; i++) {
                    btnTag = _secondary_tagsArr[i][@"id"];
                    if (btnTag.integerValue== cell.firstBtn.tag) {
                        cell.firstBtn.selected= true;
                    } else if (btnTag.integerValue == cell.secondBtn.tag) {
                        cell.secondBtn.selected= true;
                    } else if (btnTag.integerValue == cell.thirdBtn.tag) {
                        cell.thirdBtn.selected= true;
                    } else if(btnTag.integerValue == cell.forthBtn.tag) {
                        cell.forthBtn.selected= true;
                    }
                }
                
            }
            if (_primary_tags!=nil) {
                for (int i=0; i<_primary_tags.count; i++) {
                    if ([_primary_tags[i][@"name"] isEqualToString:cell.firstBtn.titleLabel.text] == true) {
                        cell.firstBtn.enabled= NO;
                        cell.firstBtn.backgroundColor = SLColor(230, 230, 230);
                    } else if ([_primary_tags[i][@"name"] isEqualToString:cell.secondBtn.titleLabel.text] == true) {
                        cell.secondBtn.enabled= NO;
                        cell.secondBtn.backgroundColor = SLColor(230, 230, 230);
                    } else if ([_primary_tags[i][@"name"] isEqualToString:cell.thirdBtn.titleLabel.text] == true) {
                        cell.thirdBtn.enabled= NO;
                        cell.thirdBtn.backgroundColor = SLColor(230, 230, 230);
                    } else if([_primary_tags[i][@"name"] isEqualToString:cell.forthBtn.titleLabel.text] == true) {
                        cell.forthBtn.enabled= NO;
                        cell.forthBtn.backgroundColor = SLColor(230, 230, 230);
                    }
                }
            }
            if(_primary_tagsArr!=nil){
                NSNumber *btnTag;
                for (int i=0; i<_primary_tagsArr.count; i++) {
                    btnTag = _primary_tagsArr[i][@"id"];
                    if (btnTag.integerValue== cell.firstBtn.tag) {
                        cell.firstBtn.enabled= NO;
                        cell.firstBtn.backgroundColor =SLColor(230, 230, 230);
                    } else if (btnTag.integerValue == cell.secondBtn.tag) {
                        cell.secondBtn.enabled= NO;
                        cell.secondBtn.backgroundColor = SLColor(230, 230, 230);
                    } else if (btnTag.integerValue == cell.thirdBtn.tag) {
                        cell.thirdBtn.enabled= NO;
                        cell.thirdBtn.backgroundColor = SLColor(230, 230, 230);
                    } else if(btnTag.integerValue == cell.forthBtn.tag) {
                        cell.forthBtn.enabled= NO;
                        cell.forthBtn.backgroundColor = SLColor(230, 230, 230);
                    }
                }
                
            }
        }
        
    } else {
        
        if (array.count == index1) {
            
            [cell.firstBtn setTitle:model.children[(indexPath.row - 1) * 4 + 0][@"name"] forState:UIControlStateNormal];
            NSString *btnTag = model.children[index0][@"id"];
            cell.firstBtn.tag = btnTag.intValue;
            [cell.firstBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            //            [cell.secondBtn setTitle:@"" forState:UIControlStateNormal];
            //            [cell.thirdBtn setTitle:@"" forState:UIControlStateNormal];
            //            [cell.forthBtn setTitle:@"" forState:UIControlStateNormal];
            cell.secondBtn.hidden = YES;
            cell.thirdBtn.hidden = YES;
            cell.forthBtn.hidden = YES;
        }else if (array.count == index2)
        {
            [cell.firstBtn setTitle:model.children[(indexPath.row - 1) * 4 + 0][@"name"] forState:UIControlStateNormal];
            NSString *btnTag = model.children[index0][@"id"];
            cell.firstBtn.tag = btnTag.intValue;
            [cell.firstBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.secondBtn setTitle:model.children[(indexPath.row - 1) * 4 + 1][@"name"] forState:UIControlStateNormal];
            NSString *btnTag1 = model.children[index1][@"id"];
            cell.secondBtn.tag = btnTag1.intValue;
            [cell.secondBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            //            [cell.thirdBtn setTitle:@"" forState:UIControlStateNormal];
            //            [cell.forthBtn setTitle:@"" forState:UIControlStateNormal];
            cell.thirdBtn.hidden = YES;
            cell.forthBtn.hidden = YES;
        }else if (array.count == index3)
        {
            [cell.firstBtn setTitle:model.children[(indexPath.row - 1) * 4 + 0][@"name"] forState:UIControlStateNormal];
            NSString *btnTag = model.children[index0][@"id"];
            cell.firstBtn.tag = btnTag.intValue;
            [cell.firstBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.secondBtn setTitle:model.children[(indexPath.row - 1) * 4 + 1][@"name"] forState:UIControlStateNormal];
            NSString *btnTag1 = model.children[index1][@"id"];
            cell.secondBtn.tag = btnTag1.intValue;
            [cell.secondBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.thirdBtn setTitle:model.children[(indexPath.row - 1) * 4 + 2][@"name"] forState:UIControlStateNormal];
            NSString *btnTag2 = model.children[index2][@"id"];
            cell.thirdBtn.tag = btnTag2.intValue;
            [cell.thirdBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            //            [cell.forthBtn setTitle:@"" forState:UIControlStateNormal];
            cell.forthBtn.hidden = YES;
        }else
        {
            [cell.firstBtn setTitle:model.children[(indexPath.row - 1) * 4 + 0][@"name"] forState:UIControlStateNormal];
            NSString *btnTag = model.children[index0][@"id"];
            cell.firstBtn.tag = btnTag.intValue;
            [cell.firstBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.secondBtn setTitle:model.children[(indexPath.row - 1) * 4 + 1][@"name"] forState:UIControlStateNormal];
            NSString *btnTag1 = model.children[index1][@"id"];
            cell.secondBtn.tag = btnTag1.intValue;
            [cell.secondBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.thirdBtn setTitle:model.children[(indexPath.row - 1) * 4 + 2][@"name"] forState:UIControlStateNormal];
            NSString *btnTag2 = model.children[index2][@"id"];
            cell.thirdBtn.tag = btnTag2.intValue;
            [cell.thirdBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.forthBtn setTitle:model.children[(indexPath.row - 1) * 4 + 3][@"name"] forState:UIControlStateNormal];
            NSString *btnTag3 = model.children[index3][@"id"];
            cell.forthBtn.tag = btnTag3.intValue;
            [cell.forthBtn addTarget:self action:@selector(cellForBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    NSInteger rows = (model.children.count - 1) / 4 +1 + 1;
    if (indexPath.row == rows-1) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 10)];
        if (model.isOpen) {
            view.backgroundColor = SLColor(238, 238, 238);
            cell.firstBtn.titleEdgeInsets = UIEdgeInsetsMake(-5, 5, 5, 5);
            cell.secondBtn.titleEdgeInsets = UIEdgeInsetsMake(-5, 5, 5, 5);
            cell.thirdBtn.titleEdgeInsets = UIEdgeInsetsMake(-5, 5, 5, 5);
            cell.forthBtn.titleEdgeInsets = UIEdgeInsetsMake(-5, 5, 5, 5);
        } else {
            view.backgroundColor = [UIColor whiteColor];
            view.hidden = YES;
            view.frame = CGRectMake(0, 0, 0, 0);
        }
        
        
        [cell.contentView addSubview:view];
    }
    cell.backgroundColor = SLColor(238, 238, 238);
    return cell;
}

// cell内部按钮的事件
-(void)cellForBtnAction:(UIButton *)btn{
    
    if ([self.type isEqualToString:@"isHome"] == false) {
        if (!btn.selected) {
            if ([self.tagsType isEqual:@1]) {
                if (_primary_tagsArr.count == 5) {
                    [self alert:@"主标签最多可添加5个!"];
                    return;
                }else{
                    btn.selected = !btn.selected;
                    NSDictionary *dict = @{@"name":btn.titleLabel.text,@"id":@(btn.tag)};
                    [_primary_tagsArr addObject:dict];
                }
            }else if([self.tagsType isEqual:@2]){
                if (_secondary_tagsArr.count == 15) {
                    [self alert:@"副标签最多可添加15个!"];
                    return;
                }else{
                    btn.selected = !btn.selected;
                    NSDictionary *dict = @{@"name":btn.titleLabel.text,@"id":@(btn.tag)};
                    [_secondary_tagsArr addObject:dict];
                }
            }
           
        }else{
            if ([self.tagsType isEqual:@1]) {
                btn.selected = !btn.selected;
                NSDictionary *dict;
                for (NSDictionary *d in _primary_tagsArr) {
                    if ([d[@"id"] isEqual:@(btn.tag)]) {
                        dict = d;
                        break;
                    }
                }
                [_primary_tagsArr removeObject:dict];
            }else if([self.tagsType isEqual:@2]){
                btn.selected = !btn.selected;
                NSDictionary *dict;
                for (NSDictionary *d in _secondary_tagsArr) {
                    if ([d[@"id"] isEqual:@(btn.tag)]) {
                        dict = d;
                        break;
                    }
                }
                [_secondary_tagsArr removeObject:dict];
            }
            
        }
        
        
        
    } else {
        SLCatDetailViewController *detailVC = [[SLCatDetailViewController alloc]init];
        
        detailVC.navTitle = self.selectedModelTitleStr;
        NSString *cateTitle=btn.titleLabel.text;
        cateTitle = [cateTitle stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        cateTitle = [cateTitle stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        detailVC.cateTitle = cateTitle;
        detailVC.catId =@(btn.tag);
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}

//通过颜色来生成一个纯色图片
- (UIImage *)buttonImageFromColor:(UIColor *)color{
    
    CGRect rect = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext(); return img;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //首页导航
    if (_falg!=-1 &&_falg != indexPath.section) {
        SLCategoryModel *model = self.categorys[_falg];
        model.isOpen = false;
        [MobClick event:@"home_navigation"];
        
    }
    
    //首页分类导航
    SLCategoryModel *model = self.categorys[indexPath.section];
    self.selectedModelTitleStr = model.name;
    model.isOpen = !model.isOpen;
    //    NSIndexSet *set = [NSIndexSet indexSetWithIndex:indexPath.section];
    //    [self.tableView reloadSections:set withRowAnimation:UITableViewRowAnimationFade];
    [MobClick event:@"home_category"];
    _falg = (int)indexPath.section;
    [self.tableView reloadData];
}

// 没有网的制空页面
-(void)addNetWorkViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, ScreenW , ScreenH-64 )];
    
    UILabel *tiplabel = [[UILabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    //    [AttributedStr addAttribute:NSForegroundColorAttributeName
    //
    //                          value:SLColor(91, 133, 189)
    //
    //                          range:NSMakeRange(2, 2)];
    
    tiplabel.attributedText = AttributedStr;
    
    
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    _noNetWorkView = contentView;
    [self.view addSubview:contentView];
    
}

- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            [self addNetWorkViews:@"世界上最遥远的距离就是－－妹有网"];
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            [_noNetWorkView removeFromSuperview];
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            [_noNetWorkView removeFromSuperview];
        }
    }
}


@end
