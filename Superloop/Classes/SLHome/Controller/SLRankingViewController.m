//
//  SLRankingViewController.m
//  Superloop
//
//  Created by WangS on 16/11/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLRankingViewController.h"
#import "SLRankingModel.h"
#import "SLAPIHelper.h"
#import "SLScrollView.h"
#import "SLSegmentedControl.h"
#import "SLRefreshHeader.h"
#import "SLRankingAskView.h"
#import "AppDelegate.h"
#import "SLFirstViewController.h"
#import "SLNavgationViewController.h"
#import "HomePageViewController.h"
#import "SLRankingView.h"

@interface SLRankingViewController ()<SLSegmentControlDelegate,UIAlertViewDelegate,UIScrollViewDelegate,SLRankingViewDelegate>

@property (nonatomic,assign) BOOL isFirstLoadMoneyRanking;
@property (nonatomic,assign) BOOL isFirstLoadServiceRanking;
@property (nonatomic,strong) UIAlertView *alert;
@property (nonatomic,strong) SLSegmentedControl *rankingSegControl;
@property (nonatomic,strong) SLScrollView *rankingScrollView;
@property (nonatomic,strong) SLRankingView *moneyRankingView;
@property (nonatomic,strong) SLRankingView *serviceRankingView;
@property (nonatomic,strong) UIView *nav;
@property (nonatomic,assign) NSInteger currentIndex;
@property (nonatomic,assign) CGFloat lastContentOffset;
@property (nonatomic,assign) BOOL hasCache;
@end

@implementation SLRankingViewController
- (SLScrollView *)rankingScrollView{
    if (!_rankingScrollView) {
        _rankingScrollView = [[SLScrollView alloc] initWithFrame:CGRectMake(0, 64+40, ScreenW, ScreenH-64-40)];
        _rankingScrollView.contentSize = CGSizeMake(ScreenW * 2, 0);
        _rankingScrollView.bounces = NO;
        _rankingScrollView.pagingEnabled = YES;
        _rankingScrollView.delegate = self;
        _rankingScrollView.showsHorizontalScrollIndicator = NO;
        _rankingScrollView.backgroundColor = [UIColor whiteColor];
        _rankingScrollView.scrollsToTop = NO;
        [self.view addSubview:_rankingScrollView];
    }
    return _rankingScrollView;
}
- (SLRankingView *)moneyRankingView{
    if (!_moneyRankingView) {
        _moneyRankingView = [[SLRankingView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH-64-40)];
        _moneyRankingView.delegate = self;
        [self.rankingScrollView addSubview:_moneyRankingView];
    }
    return _moneyRankingView;
}
- (SLRankingView *)serviceRankingView{
    if (!_serviceRankingView) {
        _serviceRankingView = [[SLRankingView alloc] initWithFrame:CGRectMake(ScreenW, 0, ScreenW, ScreenH-64-40)];
        _serviceRankingView.delegate = self;
        [self.rankingScrollView addSubview:_serviceRankingView];
    }
    return _serviceRankingView;
}

- (UIAlertView *)alert{
    if (!_alert) {
        _alert = [[UIAlertView alloc] initWithTitle:@"请先登录再进行操作" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
    }
    return _alert;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    NSLog(@"-------- %@",ApplicationDelegate.rankingFollow);
    self.isFirstLoadMoneyRanking = YES;
    self.isFirstLoadServiceRanking = YES;
    [self setUpRefresh];
    [self getMoneyRankingDatas];
    [self initNav];
    [self initUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [MobClick beginLogPageView:@"SLRankingViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLRankingViewController"];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    ApplicationDelegate.rankingFollow = nil;
}
- (void)initNav{
    UIView *nav = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    nav.backgroundColor = [UIColor whiteColor];
    self.nav = nav;
    [self.view addSubview:nav];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    backBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [nav addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(100, 27, ScreenW-200, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"超人排行榜";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [nav addSubview:nameLab];
    
    UIImageView *lineImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    lineImgView.backgroundColor = [UIColor lightGrayColor];
    [nav addSubview:lineImgView];
}
- (void)initUI{
    UIView *segView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, ScreenW,40)];
    segView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:segView];
    
    self.rankingSegControl = [[SLSegmentedControl alloc] initWithFrame:CGRectMake(60, 0, ScreenW-120, 40)];
    self.rankingSegControl.titleArr = @[@"赚钱榜",@"服务榜"];
    self.rankingSegControl.delegate = self;
    [segView addSubview:self.rankingSegControl];
    
    UIImageView *lineImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 39.5, ScreenW, 0.5)];
    lineImgView.backgroundColor = [UIColor lightGrayColor];
    [segView addSubview:lineImgView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setUpRefresh{
    self.moneyRankingView.rankingTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getRankingDatas)];
    self.serviceRankingView.rankingTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getRankingDatas)];
}
-(void)getMoneyRankingDatas{
    [self getCacheData];
    if (self.isFirstLoadMoneyRanking) {
        [self.moneyRankingView.rankingTableView.mj_header beginRefreshing];
        self.isFirstLoadMoneyRanking=NO;
        self.moneyRankingView.rankingTableView.scrollsToTop = YES;
        self.serviceRankingView.rankingTableView.scrollsToTop = NO;
    }
    [self.rankingScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}
- (void)getRankingDatas{
    NSMutableDictionary *pagrams = [NSMutableDictionary new];
    if (self.rankingSegControl.selectedSegmentIndex == 0) {
        [pagrams setValue:@"1" forKey:@"type"];
        [SLAPIHelper getRanking:pagrams success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            NSMutableArray *dataSources = [SLRankingModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            self.moneyRankingView.rankingDataSources = dataSources;
            [self.moneyRankingView.rankingTableView reloadData];
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:@"MoneyCache"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.moneyRankingView.rankingTableView.mj_header endRefreshing];
        } failure:^(SLHttpRequestError *failure) {
            [self.moneyRankingView.rankingTableView.mj_header endRefreshing];
            [HUDManager showAlertWithText:@"加载失败"];
        }];
    }else{
        [pagrams setValue:@"2" forKey:@"type"];
        [SLAPIHelper getRanking:pagrams success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            NSMutableArray *dataSources = [SLRankingModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            self.serviceRankingView.rankingDataSources = dataSources;
            [self.serviceRankingView.rankingTableView reloadData];
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:@"ServiceCache"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.serviceRankingView.rankingTableView.mj_header endRefreshing];
        } failure:^(SLHttpRequestError *failure) {
            [self.serviceRankingView.rankingTableView.mj_header endRefreshing];
            [HUDManager showAlertWithText:@"加载失败"];
        }];
    }
}

#pragma  mark -  SLRankingViewDelegate
//- (void)SLRankingView:(SLRankingView *)rankingView didSelectCellRankingModel:(SLRankingModel *)rankingModel{
- (void)SLRankingView:(SLRankingView *)rankingView didSelectCellRankingModel:(SLRankingModel *)rankingModel indexPath:(NSIndexPath *)indexPath{
    HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
    otherPerson.userId = [NSString stringWithFormat:@"%ld",(long)rankingModel.id];
    otherPerson.rankingFollowBlock = ^(BOOL isFollow){
        NSString *isFollowStr;
        if (self.rankingSegControl.selectedSegmentIndex == 0) {
            if (isFollow) {
                rankingModel.following = YES;
                isFollowStr = @"1";
            }else{
                rankingModel.following = NO;
                isFollowStr = @"0";
            }
            [ApplicationDelegate.rankingFollow setValue:isFollowStr forKey:[NSString stringWithFormat:@"%ld",(long)rankingModel.id]];
            [self.moneyRankingView.rankingDataSources replaceObjectAtIndex:indexPath.row withObject:rankingModel];
        }else{
            if (isFollow) {
                rankingModel.following = YES;
                isFollowStr = @"1";
            }else{
                rankingModel.following = NO;
                isFollowStr = @"0";
            }
            [ApplicationDelegate.rankingFollow setValue:isFollowStr forKey:[NSString stringWithFormat:@"%ld",(long)rankingModel.id]];
            [self.serviceRankingView.rankingDataSources replaceObjectAtIndex:indexPath.row withObject:rankingModel];
        }
    };
    [self.navigationController pushViewController:otherPerson animated:YES];
}
- (void)SLRankingView:(SLRankingView *)rankingView didAskBtnClickIndexPath:(NSIndexPath *)indexPath rankingModel:(SLRankingModel *)rankingModel{
    if (!ApplicationDelegate.Basic) {
        //判断是否是登录成功的状态
        [self.alert show];
        return;
    }
    SLRankingAskView *askView = [[SLRankingAskView alloc] init];
    askView.frame = CGRectMake(0, 0, ScreenW, ScreenH + ScreenH);
    [askView showViewWithModel:rankingModel indexPath:indexPath viewController:self navigationController:self.navigationController];
    [[UIApplication sharedApplication].keyWindow addSubview:askView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.lastContentOffset = scrollView.contentOffset.x;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.x > self.lastContentOffset) {
        if (self.lastContentOffset==0) {
            if (!self.hasCache) {
                [self getCacheDatas:1];
            }
        }
    }
    CGFloat destWidth;
    CGFloat currentWidth = [self.rankingSegControl.titleWidthArr[self.rankingSegControl.selectedSegmentIndex] doubleValue]+4;
    
    if ((scrollView.contentOffset.x - self.lastContentOffset)>0.0) {
        NSInteger index = self.rankingSegControl.selectedSegmentIndex+1;
        if(index<self.rankingSegControl.titleWidthArr.count){
            destWidth = [self.rankingSegControl.titleWidthArr[index] doubleValue]+4;
            self.rankingSegControl.titleBottomView.width = (scrollView.contentOffset.x-self.lastContentOffset)/screen_W*(destWidth-currentWidth)+currentWidth;
        }
        
    }else{
        NSInteger index = self.rankingSegControl.selectedSegmentIndex-1;
        if(index>-1){
            destWidth = [self.rankingSegControl.titleWidthArr[index] doubleValue]+4;
            self.rankingSegControl.titleBottomView.width = (self.lastContentOffset-scrollView.contentOffset.x)/screen_W*(destWidth-currentWidth)+currentWidth;
        }
    }
    self.rankingSegControl.titleBottomView.centerX = self.rankingSegControl.startLoc+scrollView.contentOffset.x/screen_W*((ScreenW-120)/2);
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    int index = self.rankingScrollView.contentOffset.x / self.view.width;
    [self.rankingSegControl setSegmentControlIndex:index];
    if (self.rankingSegControl.selectedSegmentIndex==0) {
        [self getCacheData];
        if (self.isFirstLoadMoneyRanking) {
            [self.moneyRankingView.rankingTableView.mj_header beginRefreshing];
            self.isFirstLoadMoneyRanking = NO;
        }
        self.moneyRankingView.rankingTableView.scrollsToTop = YES;
        self.serviceRankingView.rankingTableView.scrollsToTop = NO;
    }else{
        [self getCacheData];
        if (self.isFirstLoadServiceRanking) {
            [self.serviceRankingView.rankingTableView.mj_header beginRefreshing];
            self.isFirstLoadServiceRanking = NO;
        }
        self.moneyRankingView.rankingTableView.scrollsToTop = NO;
        self.serviceRankingView.rankingTableView.scrollsToTop = YES;
    }
    [self.rankingSegControl setSegmentControlIndex:index];
}

-(void)slSegmentControlClickedButtonIndex:(NSInteger)index{
    if (index==0) {
        self.rankingSegControl.selectedSegmentIndex = 0;
        [self getCacheData];
        if (self.isFirstLoadMoneyRanking) {
            [self.moneyRankingView.rankingTableView.mj_header beginRefreshing];
            self.isFirstLoadMoneyRanking=NO;
        }
        self.moneyRankingView.rankingTableView.scrollsToTop = YES;
        self.serviceRankingView.rankingTableView.scrollsToTop = NO;
    }else{
        self.rankingSegControl.selectedSegmentIndex = 1;
        [self getCacheData];
        if (self.isFirstLoadServiceRanking) {
            [self.serviceRankingView.rankingTableView.mj_header beginRefreshing];
            self.isFirstLoadServiceRanking=NO;
        }
        self.moneyRankingView.rankingTableView.scrollsToTop = NO;
        self.serviceRankingView.rankingTableView.scrollsToTop = YES;
    }
    [self.rankingScrollView setContentOffset:CGPointMake(ScreenW*index, 0) animated:YES];
    self.lastContentOffset = self.rankingScrollView.contentOffset.x;
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [self scrollViewDidEndDecelerating:scrollView];
}
-(void)getCacheDatas:(NSInteger)index{
    if (index==0) {
        self.rankingSegControl.selectedSegmentIndex=0;
    }else{
        self.rankingSegControl.selectedSegmentIndex=1;
    }
    [self getCacheData];
}
- (void)getCacheData{
    if (self.rankingSegControl.selectedSegmentIndex==0) {
        //获取旧的数据
        NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:@"MoneyCache"];
        if (configData) {
            NSDictionary *configDict = nil;
            configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
            NSArray * arrlist = [SLRankingModel mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
            self.moneyRankingView.rankingDataSources = (NSMutableArray *)arrlist;
            [self.moneyRankingView.rankingTableView reloadData];
        }
    }else{
        //获取旧的数据
        NSData *configData1 =[NSData data];
        configData1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"ServiceCache"];
        if (configData1) {
            self.hasCache = YES;
            NSDictionary *configDict = nil;
            configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData1];
            NSArray * arrlist = [SLRankingModel mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
            self.serviceRankingView.rankingDataSources = (NSMutableArray *)arrlist;
            [self.serviceRankingView.rankingTableView reloadData];
            
        }
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        SLFirstViewController *vc = [[SLFirstViewController alloc] init];
        SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
        [self presentViewController:nav animated:YES completion:nil];
    }
}

@end

