//
//  SLMoreCategoryController.m
//  Superloop
//
//  Created by administrator on 16/7/25.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLMoreCategoryController.h"
#import "SLCategoryModel.h"
#import "SLCategoryTableViewCell.h"

@interface SLMoreCategoryController ()<UITableViewDelegate,UITableViewDataSource,SLCategoryTableViewCellDelegate>
@property (nonatomic ,strong)UITableView *tableView;
@property (nonatomic, strong) NSArray *categorys;
@property (nonatomic,strong)NSDictionary *dataWithTime;
@property (nonatomic,copy)NSString *dateStr;
@property (nonatomic,copy)NSString *selectedModelTitleStr;
@end



@implementation SLMoreCategoryController{
    
    NSArray *_imgList;
    NSMutableArray *_tagsArr;
    NSArray *_tags;
    int _falg;
    NSArray *_arrColor1;
    NSArray *_arrColor2;
}
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenW, ScreenH-64) style:UITableViewStylePlain];
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 1, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self.tableView registerClass:[SLCategoryTableViewCell class] forCellReuseIdentifier:@"categorycell"];
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLMoreCategoryController"];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLMoreCategoryController"];
   // [self.navigationController setNavigationBarHidden:NO animated:NO];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    // 加载数据
    
    _falg = -1;
    //self.navigationItem.title = @"全部分类";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    NSArray *arrImg = @[@"superPlayer_select",@"internet_select",@"medichHealth_select",@"lawService_select",@"study_select",@"financial_select",@"friend_select",@"company_select",@"law_select",@"tour_select",@"game_select",@"publicRelation_select",@"food_select",@"car_select",@"buliding_select",@"live_select",@"puplicPerson_select",@"animal_select",@"movie_select",@"fashion_select",@"antique_select",@"science_select",@"produce_select"];
    _imgList = arrImg;
    
    NSArray *arrColor1 = @[SLColor(240, 218, 230),SLColor(212, 229, 243),SLColor(241, 228, 228),SLColor(224, 235, 249),SLColor(243, 237, 229),SLColor(236, 229, 208),SLColor(238, 217, 226),SLColor(203, 216, 239),SLColor(240, 234, 217),SLColor(223, 234, 203),SLColor(235, 231, 215),SLColor(206, 228, 229),SLColor(212, 233, 217),SLColor(207, 231, 230),SLColor(207, 215, 231),SLColor(230, 236, 203),SLColor(230, 230, 230),SLColor(232, 230, 213),SLColor(235, 209, 224),SLColor(214, 221, 232),SLColor(237, 228, 214),SLColor(239, 219, 219),SLColor(221, 237, 238),SLColor(225, 224, 241)];
    _arrColor1 = arrColor1;
   
    
    _tagsArr = [NSMutableArray array];
    _tags =[[NSArray alloc]init];
    [self loadData];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
-(void)setNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(5, 27, 30, 30);
    [backBtn  setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *titleLab=[[UILabel alloc] initWithFrame:CGRectMake(40, 32, ScreenW-80, 20)];
    titleLab.text=@"全部分类";
    titleLab.textAlignment=NSTextAlignmentCenter;
    titleLab.textColor=[UIColor blackColor];
    titleLab.font=[UIFont systemFontOfSize:17];
    [navView addSubview:titleLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=[UIColor lightGrayColor];
    [navView addSubview:cutImgView];
    
    if ([self.type isEqualToString:@"isHome"]== false) {
        
        UIButton *ensureBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        ensureBtn.frame=CGRectMake(ScreenW-40, 27, 30, 30);
        [ensureBtn addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
        [ensureBtn setTitle:@"保存" forState:UIControlStateNormal];
        [ensureBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        ensureBtn.titleLabel.font=[UIFont systemFontOfSize:15];
        [navView addSubview:ensureBtn];
    }
    
}
-(void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)submitAction{
//    if (_tagsArr !=nil) {
//        
//        NSString *tag_id = @"";
//        if (_tagsArr.count > 20) {
//            [self alert:@"标签最多可添加20个!"];
//            return;
//        }
//        if (_tagsArr.count>0) {
//            tag_id = [NSString stringWithFormat:@"%@",_tagsArr[0]];
//            for (int i= 1; i<_tagsArr.count; i++) {
//                tag_id = [tag_id stringByAppendingFormat:@",%@",_tagsArr[i]];
//            }
//        }
//        
//        NSLog(@"tag_id--------%@",tag_id);
//        [SLAPIHelper addTags:tag_id tagsType:self. success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
//            NSString *code =  data[@"code"];
//            //修改完成刷新h5页面
//            [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//                
//            }];
//            if (code.integerValue == 0) {
//                [self.navigationController popViewControllerAnimated:YES];
//            }
//        } failure:^(SLHttpRequestError *failure) {
//            [self showHUDmessage:@"添加失败！"];
//        }];
//    }
}

- (void)loadData
{
    if (self.userData) {
        NSDictionary *dict = self.userData;
        NSArray *tagsArr = dict[@"tags"];
        _tags = tagsArr;
        
        for (int i=0; i<_tags.count; i++) {
            [_tagsArr addObject:_tags[i][@"id"]];
        }
    }
    
    NSData *categoryData=[[NSUserDefaults standardUserDefaults] objectForKey:@"categoryKey"];
    if (categoryData) {
        _dateStr=[NSKeyedUnarchiver unarchiveObjectWithData:categoryData][@"dateStr"];
    }
    if ([SLValueUtils  periodTwoHours:_dateStr]) {
        NSDictionary *configDict = [NSKeyedUnarchiver unarchiveObjectWithData:categoryData];
        
        _categorys = [SLCategoryModel mj_objectArrayWithKeyValuesArray:configDict[@"configData"][@"result"]];
        
        [self.tableView reloadData];
    }else{
        
        [SLAPIHelper getAllTagsValue:nil success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            // NSLog(@"%@", data[@"result"]);
            
            _categorys = [SLCategoryModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            //缓存数据
            NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];
            NSTimeInterval timeStamp=[date timeIntervalSince1970]*1000;
            NSString *timeStampStr=[[NSString stringWithFormat:@"%f",timeStamp] componentsSeparatedByString:@"."][0];
            NSDictionary *dataWithTime=@{@"dateStr":timeStampStr,@"configData":data};
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:dataWithTime];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:@"categoryKey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.tableView reloadData];
            
        } failure:nil];
    }
    
    NSLog(@"地址%@",NSHomeDirectory());
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SLCategoryModel *model = self.categorys[indexPath.row];
    NSInteger rows = (model.children.count + 3) / 4 ;
    if (model.isOpen) {
        return 50.5 + 50*rows+20;
    }
    return 50.5;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _categorys.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLCategoryModel *model = _categorys[indexPath.row];
    SLCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"categorycell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (model.isOpen) {
        cell.btnView.hidden = NO;
    }else{
        cell.btnView.hidden = YES;
    }
    cell.collectionView.backgroundColor = _arrColor1[indexPath.row];
    cell.delegate = self;
    cell.model = model;
    cell.iconIV.image = [UIImage imageNamed:_imgList[indexPath.row]];
    return cell;
}
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    SLCategoryModel *model = 
//    static NSString *headerViewId = @"headerViewId";
//    UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerViewId];
//    if (!headerView) {
//        headerView = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:headerViewId];
//        
//        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10,15, 200, 20)];
//        label.backgroundColor = [UIColor clearColor];
//        label.text =[_categorys objectAtIndex:section];
//        [headerView addSubview:label];
//    }
//    return headerView;
//}


// cell内部按钮的事件
-(void)cellForBtnAction:(UIButton *)btn{
    
    if ([self.type isEqualToString:@"isHome"] == false) {
        
        btn.selected = !btn.selected;
        if (btn.selected == true) {
            [_tagsArr addObject:@(btn.tag)];
            
        } else {
            if (_tagsArr !=nil) {
                [_tagsArr removeObject:@(btn.tag)];
                //                btn.backgroundColor=[UIColor whiteColor];
            }
        }
        
    } else {
        SLCatDetailViewController *detailVC = [[SLCatDetailViewController alloc]init];
        
        detailVC.navTitle = self.selectedModelTitleStr;
        detailVC.cateTitle = btn.titleLabel.text;
        detailVC.catId =@(btn.tag);
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}

// 没有网的制空页面
-(void)addNetWorkViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, ScreenW , ScreenH-64 )];
    
    UILabel *tiplabel = [[UILabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    //    [AttributedStr addAttribute:NSForegroundColorAttributeName
    //
    //                          value:SLColor(91, 133, 189)
    //
    //                          range:NSMakeRange(2, 2)];
    
    tiplabel.attributedText = AttributedStr;
    
    
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    _noNetWorkView = contentView;
    [self.view addSubview:contentView];
    
}

- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            [self addNetWorkViews:@"世界上最遥远的距离就是－－妹有网"];
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            [_noNetWorkView removeFromSuperview];
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            [_noNetWorkView removeFromSuperview];
        }
    }
    
}
#pragma 代理
- (void)categoryTableViewCell:(SLCategoryTableViewCell *)categoryCell clickIsOpenBtnAtIndex:(NSIndexPath *)indexpath{
    [self.tableView reloadData];
}

@end
