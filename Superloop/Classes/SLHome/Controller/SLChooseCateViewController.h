//
//  SLChooseCateViewController.h
//  Superloop
//
//  Created by administrator on 16/7/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "BaseViewController.h"

@interface SLChooseCateViewController : BaseViewController

@property(nonatomic, assign)NSNumber *catId;
@property(nonatomic, copy)NSString *navTitle;
@property (nonatomic, copy) void(^changeTitleAndUpdate)(NSString *,NSNumber *);
@property (nonatomic,weak)UIView *noNetWorkView;  //没网制空页的view
@end
