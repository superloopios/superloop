//
//  SLLocationViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLLocationViewController;
@interface SLLocationViewController : UIViewController
@property (nonatomic ,strong) void(^valueBlcok)(NSString *str);
@property (nonatomic ,strong) void(^valueWithCityIdBlcok)(NSString *str,NSString *locationId);

@property (nonatomic, copy) NSString *string;
@property (nonatomic, assign) NSInteger tag;
@property (nonatomic,strong) NSString *labelCityStr;

@property (nonatomic,strong) SLJavascriptBridgeEvent *event;

//临时，判断是否是个人页面传过来的
@property(nonatomic,assign)BOOL isHomeLocation;

@property(nonatomic,assign)BOOL isMineBasicInfoLocation; //判断是从基本信息控制器过来的
@property(nonatomic,assign)BOOL isMineExperienceLocation; //判断是从工作经历控制器过来的
@property(nonatomic,assign)BOOL isRegisterLocation; //判断是从注册控制器过来的
@property(nonatomic,assign)BOOL isMeLocation; //判断是从个人页控制器过来的


@end
