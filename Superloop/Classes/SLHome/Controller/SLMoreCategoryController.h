//
//  SLMoreCategoryController.h
//  Superloop
//
//  Created by administrator on 16/7/25.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "BaseViewController.h"
#import <AFNetworking.h>
#import "SLCategoryModel.h"
#import "SLCategoryCell.h"
#import <MJExtension.h>
#import "SLChildCateCell.h"
#import "SLCatDetailViewController.h"
#import "AppDelegate.h"
#import "SLAPIHelper.h"
#import "SLValueUtils.h"

@interface SLMoreCategoryController : BaseViewController

@property(nonatomic,copy)NSString *type;
@property (nonatomic,strong)SLJavascriptBridgeEvent *event;
@property (nonatomic,strong)NSDictionary *userData;
@property (nonatomic,weak)UIView *noNetWorkView;  //没网制空页的view


@end
