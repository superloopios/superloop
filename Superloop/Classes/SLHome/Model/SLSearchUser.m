//
//  SLSearchUser.m
//  Superloop
//
//  Created by 朱宏伟 on 16/5/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSearchUser.h"
#import "NSString+Utils.h"

@implementation SLSearchUser

- (NSMutableArray *)tags{
    if (!_tags) {
         _tags = [NSMutableArray array];
        [_tags addObjectsFromArray:_primary_tags];
        [_tags addObjectsFromArray:_secondary_tags];
    }
    return _tags;
}
- (CGFloat)cellHeight{
    if (!_cellHeight) {
        if (self.bio.length>0) {
            //根据计算文字的大小
            CGFloat bioHeight= [self.bio boundingRectWithSize:CGSizeMake(ScreenW-24, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]} context:nil].size.height;
            if (bioHeight>40) {
                bioHeight=40;
            }
            if (self.tags.count>0) {
                _cellHeight = 149+bioHeight+15;
            }else{
                _cellHeight = 117+bioHeight+15;
            }
            
        }else{
            if (self.tags.count>0) {
                _cellHeight = 137+15;
            }else{
                _cellHeight = 105+15;
            }
        }
    }
    return _cellHeight;
}
- (CGRect)nicknameFrame{
    
    if (CGRectEqualToRect(_nicknameFrame,CGRectZero)) {
        NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
        paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
        paraStyle.lineSpacing = 4; //设置行间距
        paraStyle.hyphenationFactor = 1.0;
        paraStyle.paragraphSpacing = 2;
        CGSize nickNameSize = [self.nickname boundingRectWithSize:CGSizeMake(ScreenW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:18]} context:nil].size;
        if (nickNameSize.width>screen_W-137.5-30-70) {
            nickNameSize.width = screen_W-137.5-30-70;
        }
        _nicknameFrame = CGRectMake(137.5, 25, nickNameSize.width, 26);
    }
    return _nicknameFrame;
}
- (CGRect)bioLabelFrame{
    if (CGRectEqualToRect(_bioLabelFrame,CGRectZero)) {
        NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
        paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
        paraStyle.lineSpacing = 4; //设置行间距
        paraStyle.hyphenationFactor = 1.0;
        paraStyle.paragraphSpacing = 2;
        if (![NSString isRealString:self.bio]) {
            self.bio = @"这个人的简介一片空白";
        }
        CGSize bioSize = [self.bio boundingRectWithSize:CGSizeMake(screen_W - 137.5 - 10, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14],NSParagraphStyleAttributeName:paraStyle,NSKernAttributeName:@1.5f} context:nil].size;
        if (bioSize.height>60) {
            bioSize.height = 60;
        }
        _bioLabelFrame = CGRectMake(137.5, 83, screen_W - 137.5 - 10, bioSize.height);
    }
    return _bioLabelFrame;
}
- (NSString *)removeStringSpace:(NSString *)str{
    NSString *headerData = str;
    headerData = [headerData stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return headerData;
}

- (CGRect)crownImgViewFrame{
    if (CGRectEqualToRect(_crownImgViewFrame,CGRectZero)) {
        _crownImgViewFrame = CGRectMake(self.nicknameFrame.origin.x+self.nicknameFrame.size.width, self.nicknameFrame.origin.y, 25, 25);
    }
    return _crownImgViewFrame;
}

- (NSString *)bio{
    return [self removeStringSpace:_bio];
}

@end
