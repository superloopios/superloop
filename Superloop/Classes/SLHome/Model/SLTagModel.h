//
//  SLTagModel.h
//  Superloop
//
//  Created by WangS on 16/7/20.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLTagModel : NSObject
@property (nonatomic,copy) NSString *id;
@property (nonatomic,copy) NSString *icon;
@property (nonatomic,copy) NSString *name;
@end
