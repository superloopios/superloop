//
//  SLRankingModel.m
//  Superloop
//
//  Created by WangS on 16/11/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLRankingModel.h"
#import "SLCalculateSpace.h"
@implementation SLRankingModel

- (CGFloat)bioHeight{
    CGFloat standartSize = [SLCalculateSpace getSpaceHeight:@"高度，高度" withFont:[UIFont systemFontOfSize:14] withWidth:ScreenW-180 lineSpace:5 wordSpace:@0.0f];
    CGFloat contentSize = [SLCalculateSpace getSpaceHeight:self.bio withFont:[UIFont systemFontOfSize:14] withWidth:ScreenW-180 lineSpace:5 wordSpace:@0.0f];
    NSInteger count = contentSize/standartSize;
    if (contentSize/standartSize > count) {
        count += 1;
    }
    if (count > 2) {
        count = 2;
    }
    _bioHeight = standartSize * count;
    return _bioHeight;
}

@end
