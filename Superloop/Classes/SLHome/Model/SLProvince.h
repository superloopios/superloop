//
//  SLCity.h
//  Superloop
//
//  Created by WangJiWei on 16/3/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLProvince : NSObject
//组头
@property (nonatomic, copy) NSString *title;
//省数组
@property (nonatomic, strong) NSArray *provinces;


@end