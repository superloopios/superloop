//
//  SLChildProvince.h
//  Superloop
//
//  Created by WangJiWei on 16/3/14.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLChildProvince : NSObject
//省id
@property (nonatomic, copy) NSString *id;
//城市数组
@property (nonatomic, strong) NSArray *children;
//省名字
@property (nonatomic, copy) NSString *name;
@end
