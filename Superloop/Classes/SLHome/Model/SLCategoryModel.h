//
//  SLCategoryModel.h
//  Superloop
//
//  Created by WangJiwei on 16/4/28.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLCategoryModel : NSObject
@property (nonatomic, strong) NSArray *children;  //子数组
@property (nonatomic, copy) NSString *name;


/** 描述抽屉的开关闭合状态*/
@property(nonatomic ,assign)BOOL isOpen;
@end
