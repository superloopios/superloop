//
//  SLCellModel.h
//  Superloop
//
//  Created by WangJiWei on 16/3/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLCellModel : NSObject
//cell的label
@property(nonatomic, copy) NSString *cellLabel;
//celld的textfield
@property(nonatomic, strong)UITextField *cellTextField;
@end
