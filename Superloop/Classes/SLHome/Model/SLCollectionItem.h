//
//  SLCollectionItem.h
//  Superloop
//
//  Created by WangJiWei on 16/3/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLCollectionItem : NSObject

//城市名
@property(nonatomic, strong) NSString *code;
//城市ID
@property(nonatomic, strong) NSString *name;
@end
