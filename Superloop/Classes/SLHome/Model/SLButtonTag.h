//
//  SLButtonTag.h
//  Superloop
//
//  Created by WangS on 16/5/17.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLButtonTag : NSObject


@property (nonatomic, strong) NSNumber *id;


@property (nonatomic, copy) NSString *name;


@end
