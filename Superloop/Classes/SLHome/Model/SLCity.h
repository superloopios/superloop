//
//  SLSecondCity.h
//  Superloop
//
//  Created by WangJiWei on 16/3/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLCity : NSObject
//城市id
@property (nonatomic, copy) NSString *id;
//城市名字
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *code;
@end
