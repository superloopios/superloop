//
//  SLBannerModel.h
//  Superloop
//
//  Created by WangS on 16/5/21.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLBannerModel : NSObject

@property (nonatomic,copy)NSString *id;

@property (nonatomic,copy)NSString *position;

@property (nonatomic,copy)NSString *created;

@property (nonatomic,copy)NSString *image_url;

@property (nonatomic,copy)NSString *navigation_type;

@property (nonatomic,copy)NSString *navigation_url;


@end
