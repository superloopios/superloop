//
//  SLSuggestion.h
//  Superloop
//
//  Created by 朱宏伟 on 16/5/6.
//  Copyright © 2016年 Superloop. All rights reserved.
//  获取关注人的模型数据

#import <Foundation/Foundation.h>

@interface SLSuggestion : NSObject

@property (nonatomic,copy)NSString *id;
@property (nonatomic,copy)NSString *gender;
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *location;
@property (nonatomic,copy)NSString *nickname;

@end
