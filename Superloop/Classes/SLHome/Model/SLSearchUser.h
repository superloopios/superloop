//
//  SLSearchUser.h
//  Superloop
//
//  Created by 朱宏伟 on 16/5/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLSearchUser : NSObject

@property (nonatomic,copy) NSString *location;  // 用户所在城市
@property (nonatomic,copy) NSString *id;   // 用户ID
@property (nonatomic,copy) NSString *nickname;   // 个人姓名
@property (nonatomic,copy) NSString *avatar;      //用户头像
@property (nonatomic,copy) NSString *bio;        // 个人简介
@property (nonatomic,copy) NSString *price;     // 通话价格
@property (nonatomic,copy) NSString *followers;   // 粉丝数量
@property (nonatomic,copy) NSString *credit_level;  // 评价值
@property (nonatomic,strong) NSMutableArray *tags;    // 用户标签
@property (nonatomic,strong) NSArray *primary_tags;    // 用户标签
@property (nonatomic,strong) NSArray *secondary_tags;    // 用户标签
@property (nonatomic,copy) NSString *work_position;  //职位
@property (nonatomic,copy) NSString *role_type;  //特权
@property (nonatomic,copy) NSString *verification_status; //认证
@property (nonatomic,copy) NSString *following;  // 用户所在城市

                                                // 用户标签下的内容
                                                // "id": 1,
                                                //"name": "大玩家"
@property (nonatomic,assign) CGFloat cellHeight;
@property (nonatomic,assign) CGRect nicknameFrame;
@property (nonatomic,assign) CGRect crownImgViewFrame;
@property (nonatomic,assign) CGRect bioLabelFrame;

@end
