//
//  SLRankingModel.h
//  Superloop
//
//  Created by WangS on 16/11/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLRankingModel : NSObject
@property (nonatomic,strong) NSNumber *amount;//赚取金额
@property (nonatomic,copy) NSString *avatar;//头像URL
@property (nonatomic,copy) NSString *bio;//个人简介
@property (nonatomic,assign) BOOL following;//是否正在关注
@property (nonatomic,assign) NSInteger id;//用户ID
@property (nonatomic,copy) NSString *nickname;//用户昵称
@property (nonatomic,assign) NSInteger service_count;//服务次数
@property (nonatomic,assign) NSInteger rankingSort;//排序

@property (nonatomic,assign) CGFloat bioHeight;
@end
