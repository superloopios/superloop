//
//  SLCategoryCollectionViewCell.h
//  Superloop
//
//  Created by administrator on 16/7/28.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLCategoryCollectionViewCell : UICollectionViewCell

@property(nonatomic, copy)NSString *cTitle;

@end
