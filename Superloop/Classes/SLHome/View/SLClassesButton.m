//
//  SLClassesButton.m
//  Superloop
//
//  Created by WangJiwei on 16/4/29.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLClassesButton.h"

@implementation SLClassesButton

- (void)setHighlighted:(BOOL)highlighted{
    
}
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        self.titleLabel.numberOfLines = 0;
//        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        
        self.imageView.backgroundColor = [UIColor redColor];
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    
    self.titleLabel.width = self.width;
    //    self.titleLabel.y = CGRectGetMaxY(self.imageView.frame);
//    self.titleLabel.x = 10;
    self.titleLabel.x = 0;
    CGFloat textMaxW = self.width;
    self.titleLabel.height = [self.titleLabel.text boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
    self.titleLabel.y = self.height - self.titleLabel.height;
    //    self.titleLabel.height = self.height - self.titleLabel.y;
    
    
    self.imageView.y = 0;
    self.imageView.centerX = self.titleLabel.centerX;
    self.imageView.width = self.height - self.titleLabel.height;
    self.imageView.height = self.height - self.titleLabel.height;
//    self.imageView.centerX = self.centerX;
    
}


@end
