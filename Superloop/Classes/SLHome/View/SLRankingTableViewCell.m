//
//  SLRankingTableViewCell.m
//  Superloop
//
//  Created by WangS on 16/11/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLRankingTableViewCell.h"
#import "SLRankingModel.h"
#import "SLCalculateSpace.h"

@interface SLRankingTableViewCell ()
@property (nonatomic,strong) UIImageView *iconImgView;
@property (nonatomic,strong) UIImageView *rankImgView;
@property (nonatomic,strong) UILabel *nicknameLab;
@property (nonatomic,strong) UILabel *bioLab;
@property (nonatomic,strong) UIButton *askBtn;
@end
@implementation SLRankingTableViewCell

- (UIImageView *)iconImgView{
    if (!_iconImgView) {
        _iconImgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 25, 78, 78)];
        [self addSubview:_iconImgView];
    }
    return _iconImgView;
}
- (UIImageView *)rankImgView{
    if (!_rankImgView) {
        _rankImgView = [[UIImageView alloc] initWithFrame:CGRectMake(6, 27, 33, 18)];
        [self addSubview:_rankImgView];
    }
    return _rankImgView;
}
- (UILabel *)nicknameLab{
    if (!_nicknameLab) {
        _nicknameLab = [[UILabel alloc] initWithFrame:CGRectMake(20+78+12, 35, ScreenW-180, 16)];
        _nicknameLab.font = [UIFont boldSystemFontOfSize:16];
        _nicknameLab.textColor = UIColorFromRGB(0x2b2124);
        _nicknameLab.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_nicknameLab];
    }
    return _nicknameLab;
}
- (UILabel *)bioLab{
    if (!_bioLab) {
        _bioLab = [[UILabel alloc] init];
        _bioLab.textColor = UIColorFromRGB(0x2b2124);
        _bioLab.textAlignment = NSTextAlignmentLeft;
        _bioLab.numberOfLines = 0;
        [self addSubview:_bioLab];
    }
    return _bioLab;
}
- (UIButton *)askBtn{
    if (!_askBtn) {
        _askBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _askBtn.frame = CGRectMake(ScreenW - 70, 20, 50, 50);
        _askBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_askBtn setTitleColor:UIColorFromRGB(0xff5a5f) forState:UIControlStateNormal];
        [_askBtn addTarget:self action:@selector(askBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_askBtn];
    }
    return _askBtn;
}
- (void)setRankingModel:(SLRankingModel *)rankingModel{
    _rankingModel = rankingModel;
     //图像
     [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:rankingModel.avatar] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    //昵称
    self.nicknameLab.text = rankingModel.nickname;
    //简介
    NSString *bioStr;
    if (rankingModel.bio) {
        bioStr = rankingModel.bio;
    }else{
    bioStr = @"这个人的简介一片空白";
    }
    [SLCalculateSpace setLabelSpace:self.bioLab withValue:bioStr withFont:[UIFont systemFontOfSize:14] lineSpace:5 wordSpace:@0.0f];
    [self.bioLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nicknameLab.mas_bottom).offset(10);
        make.left.mas_equalTo(self.iconImgView.mas_right).offset(12);
        make.right.mas_equalTo(self.mas_right).offset(-70);
        make.height.mas_equalTo(rankingModel.bioHeight);
    }];
    //问Ta
    [self.askBtn setTitle:@"问Ta" forState:UIControlStateNormal];
    if (rankingModel.rankingSort == 0 ) {
        [self addSubview:self.rankImgView];
        self.rankImgView.image =[UIImage imageNamed:@"rankingSun"];
        [self addSubview:self.rankImgView];
    }else if(rankingModel.rankingSort == 1){
        self.rankImgView.image =[UIImage imageNamed:@"rankingMoon"];
        [self addSubview:self.rankImgView];
    }else if (rankingModel.rankingSort == 2){
        self.rankImgView.image =[UIImage imageNamed:@"rankingStar"];
        [self addSubview:self.rankImgView];
    }else{
        [self.rankImgView removeFromSuperview];
    }
}

- (void)askBtnClick{
    if ([self.delegate respondsToSelector:@selector(SLRankingTableViewCell:askBtnClickIndexPath:rankingModel:)]) {
        [self.delegate SLRankingTableViewCell:self askBtnClickIndexPath:self.indexPath rankingModel:self.rankingModel];
    }
}

@end
