//
//  SLCategoryTableViewCell.m
//  Superloop
//
//  Created by administrator on 16/7/26.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCategoryTableViewCell.h"
#import "SLCategoryModel.h"
#import "SLCategoryCollectionViewCell.h"

@interface SLCategoryTableViewCell ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource>

//标题
@property(nonatomic, strong)UILabel *titleLabel;
//小箭头
@property(nonatomic, strong)UIImageView *arrowIV;
//每个cell顶部覆盖一个button
@property(nonatomic, strong)UIButton *topBtn;

//小箭头
@property(nonatomic, strong)UIImageView *separatorIV;

/** collectionViewLayout */
@property (nonatomic, strong)UICollectionViewFlowLayout *layout;



@end

@implementation SLCategoryTableViewCell

- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        self.layout = layout;
        layout.minimumLineSpacing = 1;
        layout.itemSize = CGSizeMake(screen_W/4.0, 50);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        
        [self.contentView addSubview:_collectionView];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.separatorIV.mas_bottom).offset(0);
            make.left.right.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView).offset(5);
        }];
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SLCategoryCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"categoryBtnCell"];
        
    }
    return _collectionView;
}

- (UIButton *)topBtn{
    if (!_topBtn) {
        _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:_topBtn];
        [_topBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.left.right.equalTo(self.contentView);
            make.height.equalTo(@50);
        }];
        [_topBtn addTarget:self action:@selector(isOpen) forControlEvents:UIControlEventTouchUpInside];
    }
    return _topBtn;
}
- (void)isOpen{
    self.model.isOpen = !self.model.isOpen;
    self.collectionView.hidden = !self.model.isOpen;
    if ([_delegate respondsToSelector:@selector(categoryTableViewCell:clickIsOpenBtnAtIndex:)]) {
        [_delegate categoryTableViewCell:self clickIsOpenBtnAtIndex:self.indexPath];
    }
    
}

- (UIImageView *)iconIV{
    if (!_iconIV) {
        _iconIV = [[UIImageView alloc] init];
        [self.contentView addSubview:_iconIV];
        [_iconIV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(10);
            make.width.height.equalTo(@50);
        }];
    }
    return _iconIV;
}
- (UIImageView *)separatorIV{
    if (!_separatorIV) {
        _separatorIV = [[UIImageView alloc] init];
        [self.contentView addSubview:_separatorIV];
        _separatorIV.backgroundColor = SLColor(220, 220, 220);
        [_separatorIV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.iconIV.mas_bottom);
            make.left.right.equalTo(self.contentView);
            make.height.equalTo(@0.5);
        }];
    }
    return _separatorIV;
}
- (UIImageView *)arrowIV{
    if (!_arrowIV) {
        _arrowIV = [[UIImageView alloc] init];
        _arrowIV.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:_arrowIV];
        [_arrowIV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.right.equalTo(self.contentView).offset(-10);
            make.width.height.equalTo(@50);
        }];
        
    }
    return _arrowIV;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_titleLabel];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(self.contentView);
            make.left.equalTo(self.iconIV.mas_right).offset(10);
            make.centerY.equalTo(self.iconIV);
        }];
    }
    return _titleLabel;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

    }
    return self;
}
- (void)setModel:(SLCategoryModel *)model{
    _model = model;
    self.titleLabel.text = model.name;
    self.arrowIV.image = [UIImage imageNamed:@"down_arrow"];
    [self.contentView addSubview:self.topBtn];
    [self.contentView addSubview:self.separatorIV];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.collectionView reloadData];
    self.collectionView.hidden = !model.isOpen;
}
- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource,UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath andModel:(SLCategoryModel *)model
{

}




-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.model.children.count;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((screen_W-3)/4, 50);
}
//每个UICollectionView展s示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SLCategoryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"categoryBtnCell" forIndexPath:indexPath];
    NSLog(@"%ld,,,,,%ld",self.model.children.count,indexPath.row);
    cell.cTitle = self.model.children[indexPath.row][@"name"];
    return cell;
    
}
//每个UICollectionView的点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 0, 5, 0);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1;
}
@end
