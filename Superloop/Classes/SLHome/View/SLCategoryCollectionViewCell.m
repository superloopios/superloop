//
//  SLCategoryCollectionViewCell.m
//  Superloop
//
//  Created by administrator on 16/7/28.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCategoryCollectionViewCell.h"

@interface SLCategoryCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIButton *cBtn;

@end

@implementation SLCategoryCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setCTitle:(NSString *)cTitle{
    _cTitle = cTitle;
    [self.cBtn setTitle:cTitle forState:UIControlStateNormal];
    [self.cBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.cBtn.titleLabel.numberOfLines = 0;
    self.cBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.cBtn.titleLabel.font = [UIFont systemFontOfSize:12.5];
    self.cBtn.backgroundColor = [UIColor whiteColor];
}
@end
