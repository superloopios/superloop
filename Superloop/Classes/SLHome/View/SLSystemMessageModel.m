//
//  SLSystemMessageModel.m
//  Superloop
//
//  Created by administrator on 16/7/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSystemMessageModel.h"
#import "NSString+Utils.h"

@implementation SLSystemMessageModel

- (CGFloat)cellHeight{
    //78为顶部标题跟副标题高度包括副标题底部间距
    _cellHeight = 0;
    if ([_msgtype isEqual:@5]) {
        //62为间距和最低下文字高度
        _cellHeight = 78;
        _cellHeight += 62;
        //渠道每行的高度
        CGFloat textMaxW = ScreenW - 52;
        NSString *titleContentextString = @"高度";
        CGFloat titleFontSize = [titleContentextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} context:nil].size.height;
        
        //标题高度
        CGFloat titleHeight = [self.topicTitle boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} context:nil].size.height;
//        NSLog(@"%.2f,%.2f",titleFontSize,titleHeight);
        NSInteger titleLineNum = titleHeight / titleFontSize;
        if (titleLineNum == 1) {
            _cellHeight += 10;
        }
        _cellHeight += titleLineNum * titleFontSize+(titleLineNum -1 ) * 5;
        
        
        CGFloat reasonMaxW = screen_W-125;
        NSString *titleContentextString1 = @"高度高度";
        CGFloat reasonFontSize = [titleContentextString1 boundingRectWithSize:CGSizeMake(reasonMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} context:nil].size.height;
        
        //标题高度
        CGFloat reasonHeight = [self.reason boundingRectWithSize:CGSizeMake(reasonMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} context:nil].size.height;
        NSInteger reasonLineNum = reasonHeight / reasonFontSize;
        _cellHeight += reasonLineNum * reasonFontSize+(reasonLineNum -1) * 5;
    }else if ([_msgtype isEqual:@9]) {
        //62为间距和最低下文字高度
        _cellHeight += 56;
        if ([NSString isRealString:self.msgcontent[@"contentTitle"]]) {
            _cellHeight += 22;
        }
        //渠道每行的高度
        CGFloat textMaxW = ScreenW - 52;
        if ([NSString isRealString:self.msgcontent[@"content"]]) {
            NSString *titleContentextString = @"高度";
            CGFloat titleFontSize = [titleContentextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} context:nil].size.height;
            
            CGFloat titleHeight = [self.msgcontent[@"content"] boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} context:nil].size.height;
            //        NSLog(@"%.2f,%.2f",titleFontSize,titleHeight);
            NSInteger titleLineNum = titleHeight / titleFontSize;
            if (titleLineNum == 1) {
                _cellHeight += 10;
            }
            _cellHeight += titleLineNum * titleFontSize+(titleLineNum -1 ) * 5;
        }
        if ([NSString isRealString:self.msgcontent[@"memo"]]) {
            CGFloat reasonMaxW = screen_W-52;
            NSString *titleContentextString1 = @"高度高度";
            CGFloat reasonFontSize = [titleContentextString1 boundingRectWithSize:CGSizeMake(reasonMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} context:nil].size.height;
            
            //标题高度
            CGFloat reasonHeight = [self.msgcontent[@"memo"] boundingRectWithSize:CGSizeMake(reasonMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} context:nil].size.height;
            NSInteger reasonLineNum = reasonHeight / reasonFontSize;
            _cellHeight += reasonLineNum * reasonFontSize+(reasonLineNum -1) * 5;
            _cellHeight += 20;
        }
        _cellHeight += 20;
    }
    else{
        _cellHeight = 78;
        _cellHeight += 129;
        CGFloat textMaxW = screen_W-125;
        NSString *titleContentextString = @"高度";
        CGFloat titleFontSize = [titleContentextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} context:nil].size.height;
        
        //标题高度
        CGFloat titleHeight = [self.reason boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} context:nil].size.height;
        NSInteger titleLineNum = titleHeight / titleFontSize;
        if (titleLineNum == 1) {
            _cellHeight += 10;
        }
        _cellHeight += titleLineNum * titleFontSize+(titleLineNum -1 ) * 5;
    }
    return _cellHeight;
}
- (NSString *)dateStr{
    // 将时间戳字符串转成日期
    //    NSString *time = [NSString stringWithFormat:@]
    NSInteger startTime = [_msgcontent[@"startTime"] doubleValue];
    return [self getTimeStr:startTime format:@"yyyy-MM-dd HH:mm"];
}
- (NSString *)getTimeStr:(NSInteger)startTime format:(NSString *)format{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:startTime/1000.0];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
    fmt.dateFormat = format;
    NSString *dateStr = [fmt stringFromDate:date];
    NSDate *createdAtDate = [fmt dateFromString:dateStr];
    
    return [fmt stringFromDate:createdAtDate];
}
- (NSString *)timeStr{
    NSInteger time = (NSInteger)_msgcontent[@"callDuration"];
    int seconds = time % 60;
    int minutes = (time / 60) % 60;
    int hours = (int)time / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}
- (NSString *)topicTitle{
    return _msgcontent[@"title"];
}

-(NSString *)reason{
    if ([_msgtype  isEqual: @5]) {
        return _msgcontent[@"reason"];
    }else{
        return _msgcontent[@"appeal"];
    }
}
- (NSString *)otherName{
    return _msgcontent[@"callUser"];
}

- (CGFloat)drawback{
    return [_msgcontent[@"amount"] doubleValue];
}
- (BOOL)isPass{
    return [_msgtype isEqual:@4] ? YES:NO;
}
- (NSString *)updatedStr{
    NSInteger startTime = self.updated;
    return [self getTimeStr:startTime format:@"yy-MM-dd HH:mm"];
}

@end
