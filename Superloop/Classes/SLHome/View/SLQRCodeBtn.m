//
//  SLQRCodeBtn.m
//  Superloop
//
//  Created by WangS on 16/8/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLQRCodeBtn.h"

@implementation SLQRCodeBtn


-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        _imgView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 20, 20)];
        [self addSubview:_imgView];
        
        _titleLab=[[UILabel alloc] initWithFrame:CGRectMake(0, 25, 40, 12)];
        _titleLab.font=[UIFont systemFontOfSize:10];
        _titleLab.textAlignment=NSTextAlignmentCenter;
        _titleLab.textColor=[UIColor blackColor];
        [self addSubview:_titleLab];
    }
    return self;
}



//- (void)layoutSubviews{
//    [super layoutSubviews];
//    self.imageView.height=self.height-15;
//    self.imageView.width=self.width-25;
//}
@end
