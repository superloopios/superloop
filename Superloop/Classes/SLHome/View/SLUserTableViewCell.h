//
//  SLUserTableViewCell.h
//  Superloop
//
//  Created by WangS on 16/7/29.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLSearchUser;
@interface SLUserTableViewCell : UITableViewCell
@property (nonatomic,strong)SLSearchUser *user;
@property (nonatomic, copy ) void(^didClickFollowBtn)(UIButton *btn);

@end
