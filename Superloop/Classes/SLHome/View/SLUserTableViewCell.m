//
//  SLUserTableViewCell.m
//  Superloop
//
//  Created by WangS on 16/7/29.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLUserTableViewCell.h"
#import "SLSearchUser.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"
#import "SLPersonTagsCollectionViewCell.h"
#import "NSString+Utils.h"
#import <Masonry.h>
#import "AppDelegate.h"
#import "SLAPIHelper.h"



@interface SLUserTableViewCell ()<UICollectionViewDelegate,UICollectionViewDataSource,UIAlertViewDelegate>
@property (nonatomic,strong) UIImageView    * iconImgView;             //用户图像
@property (nonatomic,strong) UIImageView    * identificationImgView;   //认证标志
@property (nonatomic,strong) SLLabel        * nickNameLab;             //昵称
@property (nonatomic,strong) UIImageView    * crownImgView;            //皇冠
@property (nonatomic,strong) SLLabel        * positionLab;             //职位
@property (nonatomic,strong) UIImageView    * starImgView;             //星星
@property (nonatomic,strong) SLLabel        * chargeLab;               //价格
@property (nonatomic,strong) SLLabel        * bioLab;                  //简介
@property (nonatomic,strong) UIScrollView   * tagsScrollView;          //标签
@property (nonatomic,strong) UIImageView    * locationImgView;         //位置标识
@property (nonatomic,strong) SLLabel        * locationLab;             //位置
@property (nonatomic,strong) SLLabel        * fansLab;                 //粉丝
@property (nonatomic,strong) UIImageView    * fansStarImgView;         //粉丝前面的星
@property (nonatomic,strong) UIView         * xingView;
@property (nonatomic,strong) UIView         * sepatorView;
@property (nonatomic,strong) UIView         * coverView;
@property (nonatomic,strong) UIImageView    * yellowImgView;
@property (nonatomic,strong) UIImageView    * GrayImgView;
@property (nonatomic,strong) UILabel        * tagDefaultLabel;
@property (nonatomic,strong) UIButton       * followBtn;                 //关注btn
//主标签
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic, strong)UICollectionViewFlowLayout *layout;

@property (nonatomic,strong) NSMutableArray *userTags;

@end
@implementation SLUserTableViewCell

- (UIButton *)followBtn{
    if (!_followBtn) {
        _followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _followBtn.frame = CGRectMake(screen_W-70, 23, 55, 30);
        [_followBtn setImage:[UIImage imageNamed:@"followTa"] forState:UIControlStateNormal];
        [_followBtn setImage:[UIImage imageNamed:@"cancleFollow"] forState:UIControlStateSelected];
        [self.contentView addSubview:_followBtn];
    }
    return _followBtn;
}

- (NSMutableArray *)userTags{
    if (!_userTags) {
        _userTags = [NSMutableArray array];
    }
    return _userTags;
}
- (UIView *)sepatorView{
    if (!_sepatorView) {
        _sepatorView = [[UIView alloc] init];
        _sepatorView.frame = CGRectMake(0, 239, screen_W, 0.5);
        _sepatorView.backgroundColor = SLMainColor;
        [self.contentView addSubview:_sepatorView];
    }
    return _sepatorView;
}


- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        self.layout = layout;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.scrollsToTop=NO;
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SLPersonTagsCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"personFutagLabelCell"];
        [self.contentView addSubview:_collectionView];
        _collectionView.frame = CGRectMake(10, 157.5, screen_W-20, 27.5);
        _collectionView.showsHorizontalScrollIndicator = NO;
    }
    return _collectionView;
}

- (UILabel *)tagDefaultLabel{
    if (!_tagDefaultLabel) {
        _tagDefaultLabel = [[UILabel alloc] init];
        _tagDefaultLabel.font = [UIFont systemFontOfSize:13];
        _tagDefaultLabel.textColor = UIColorFromRGB(0x888888);
        _tagDefaultLabel.textAlignment = NSTextAlignmentCenter;
        _tagDefaultLabel.text = @"该用户尚未选择标签";
        _tagDefaultLabel.frame = CGRectMake(10, 157.5, screen_W-20, 27.5);
        [self.contentView addSubview:_tagDefaultLabel];
    }
    return _tagDefaultLabel;
}
    
- (UIImageView *)iconImgView{
    if (!_iconImgView) {
        _iconImgView=[[UIImageView alloc] init];
        _iconImgView.frame = CGRectMake(11, 25, 117.5, 117.5);
        [self.contentView addSubview:_iconImgView];
    }
    return _iconImgView;
}
- (UIView *)coverView{
    if (!_coverView) {
        _coverView = [[UIView alloc] init];
        _coverView.frame = CGRectMake(0, 100, 117.5, 17.5);
        _coverView.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.7];
        [self.iconImgView addSubview:_coverView];
    }
    return _coverView;
}
- (UIImageView *)GrayImgView{
    if (!_GrayImgView) {
        _GrayImgView=[[UIImageView alloc] init];
        _GrayImgView.image = [UIImage imageNamed:@"starFiveGray"];
        _GrayImgView.frame = CGRectMake(8 , 0, 70, 17.5);
        _GrayImgView.contentMode = UIViewContentModeLeft;
        [self.coverView addSubview:_GrayImgView];
    }
    return _GrayImgView;
}
- (UIImageView *)yellowImgView{
    if (!_yellowImgView) {
        _yellowImgView=[[UIImageView alloc] init];
        _yellowImgView.image = [UIImage imageNamed:@"starFiveYellow"];
        
        _yellowImgView.contentMode = UIViewContentModeLeft;
        _yellowImgView.clipsToBounds = YES;
        [self.coverView addSubview:_yellowImgView];
    }
    return _yellowImgView;
}

- (UIImageView *)identificationImgView{
    if (!_identificationImgView) {
        _identificationImgView=[[UIImageView alloc] init];
        _identificationImgView.image=[UIImage imageNamed:@"identification"];
        _identificationImgView.contentMode = UIViewContentModeCenter;
        _identificationImgView.frame = CGRectMake(90, 0, 20, 17.5);
        [self.coverView addSubview:_identificationImgView];
    }
    return _identificationImgView;
}

- (SLLabel *)nickNameLab{
    if (!_nickNameLab) {
        _nickNameLab=[[SLLabel alloc] init];
        _nickNameLab.textColor= SLBlackTitleColor;
        _nickNameLab.font=[UIFont boldSystemFontOfSize:18];
        [self.contentView addSubview:_nickNameLab];
        
//        _nickNameLab.backgroundColor = [UIColor redColor];
    }
    return _nickNameLab;
}

- (UIImageView *)crownImgView{
    if (!_crownImgView) {
        _crownImgView=[[UIImageView alloc] init];
        _crownImgView.image=[UIImage imageNamed:@"crown"];
        _crownImgView.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:_crownImgView];
        
    }
    return _crownImgView;
}

- (SLLabel *)positionLab{
    if (!_positionLab) {
        _positionLab=[[SLLabel alloc] init];
        _positionLab.textColor=UIColorFromRGB(0x888888);
        _positionLab.font=[UIFont systemFontOfSize:12];
//        _positionLab.frame = CGRectMake(<#CGFloat x#>, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
        [self.contentView addSubview:_positionLab];
//        _positionLab.backgroundColor = [UIColor redColor];
        [_positionLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.nickNameLab.mas_bottom).offset(8.5);
            make.left.mas_equalTo(self.iconImgView.mas_right).offset(10);
            make.right.mas_equalTo(self.mas_right).offset(-10);
            make.height.mas_equalTo(12);
        }];
    }
    return _positionLab;
}
- (UIView *)xingView{
    if (!_xingView) {
        _xingView = [[UIView alloc] init];
        _xingView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_xingView];
        
        
        for (int i=0; i<5; i++) {
            UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"commentStar"]];
            iv.frame = CGRectMake(i*15, 1.5, 12, 12);
            [_xingView addSubview:iv];
        }
    }
    return _xingView;
}

- (SLLabel *)chargeLab{
    if (!_chargeLab) {
        _chargeLab = [[SLLabel alloc] init];
        _chargeLab.font=[UIFont systemFontOfSize:13];
        _chargeLab.textColor=SLColor(80, 80, 80);
        [self.contentView addSubview:_chargeLab];
        
        [_chargeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.xingView.mas_right).offset(10);
            make.centerY.equalTo(self.xingView.mas_centerY);
        }];
    }
    return _chargeLab;
}

- (SLLabel *)bioLab{
    if (!_bioLab) {
        _bioLab=[[SLLabel alloc] init];
        _bioLab.numberOfLines=0;
        _bioLab.font=[UIFont systemFontOfSize:14];
        _bioLab.textColor=SLBlackTitleColor;
//        _bioLab.frame = CGRectMake(137.5, 90, screen_W - 137.5 - 10, 60);
        _bioLab.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_bioLab];
        
//        [_bioLab mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(self.positionLab.mas_bottom).offset(12.5);
//            make.left.mas_equalTo(self.iconImgView.mas_right).offset(10);
//            make.right.mas_equalTo(self.mas_right).offset(-10);
//        }];
    }
    return _bioLab;
}

- (UIImageView *)locationImgView{
    if (!_locationImgView) {
        _locationImgView=[[UIImageView alloc] init];
        _locationImgView.image=[UIImage imageNamed:@"local_icon"];
        _locationImgView.contentMode = UIViewContentModeCenter;
        _locationImgView.frame = CGRectMake(10, 202, 10, 14);
        [self.contentView addSubview:_locationImgView];
        
    }
    return _locationImgView;
}
- (SLLabel *)locationLab{
    if (!_locationLab) {
        //定位信息
        _locationLab=[[SLLabel alloc] init];
        _locationLab.font=[UIFont systemFontOfSize:13];
        _locationLab.textColor=SLBlackTitleColor;
        _locationLab.frame = CGRectMake(22, 202, 100, 14);
        [self.contentView addSubview:self.locationLab];
    }
    return _locationLab;
}

- (SLLabel *)fansLab{
    if (!_fansLab) {
        _fansLab=[[SLLabel alloc] init];
        _fansLab.font=[UIFont systemFontOfSize:13];
        _fansLab.frame = CGRectMake(screen_W-60, 202, 100, 14);
        _fansLab.textColor=SLBlackTitleColor;
        [self.contentView addSubview:_fansLab];
    }
    return _fansLab;
}

- (UIImageView *)fansStarImgView{
    if (!_fansStarImgView) {
        _fansStarImgView=[[UIImageView alloc] init];
        _fansStarImgView.frame = CGRectMake(screen_W-80, 202, 15, 14);
        [self.contentView addSubview:_fansStarImgView];
    }
    return _fansStarImgView;
}

- (void)setUser:(SLSearchUser *)user{
    _user = user;
    //用户图像
    [self.userTags removeAllObjects];
    [self.userTags addObjectsFromArray:self.user.primary_tags];
    [self.userTags addObjectsFromArray:self.user.secondary_tags];
    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
//

    //昵称
    self.nickNameLab.frame = user.nicknameFrame;
    self.nickNameLab.text=user.nickname;
    //皇冠
    self.crownImgView.frame = user.crownImgViewFrame;
  
    if ([user.role_type isEqualToString:@"1"]) {
        self.crownImgView.hidden=NO;
    }else{
        self.crownImgView.hidden=YES;
    }
    
    
    if ([NSString isRealString:user.work_position]) {
        self.positionLab.text=user.work_position;
    }else{
        self.positionLab.text=@"无职位信息";
    }
    self.bioLab.frame = user.bioLabelFrame;
    
    if ([NSString isRealString:user.bio]) {
        self.bioLab.text=user.bio;
    }else{
        self.bioLab.text=@"这个人的简介一片空白";
    }
    [self setLabelSpace:self.bioLab withValue:self.bioLab.text withFont:[UIFont systemFontOfSize:14]];
    if (self.userTags.count>0) {
        self.collectionView.hidden = NO;
        self.tagDefaultLabel.hidden = YES;
    }else{
        self.collectionView.hidden = YES;
        self.tagDefaultLabel.hidden = NO;
    }
    [self.collectionView reloadData];
    self.locationImgView.image=[UIImage imageNamed:@"local_icon"];
    if ([NSString isRealString:user.location]) {
        self.locationLab.hidden = NO;
        self.locationLab.text=user.location;
    }else{
        self.locationLab.hidden = YES;
    }
    if (![user.followers isEqualToString:@"0"]) {
        self.fansStarImgView.hidden=NO;
        self.fansLab.hidden = NO;
        self.fansStarImgView.image=[UIImage imageNamed:@"fans_icon"];
        self.fansLab.text=[NSString stringWithFormat:@"粉丝%@",user.followers];
    }else{
        self.fansStarImgView.hidden=YES;
        self.fansLab.hidden = YES;
    }
    if ([user.verification_status isEqualToString:@"2"]) {
        self.identificationImgView.hidden=NO;
    }else{
        self.identificationImgView.hidden=YES;
    }
    
    
    [self.coverView addSubview:self.GrayImgView];
    [self.coverView addSubview:self.yellowImgView];
    NSInteger numberOfCredit_level=[user.credit_level integerValue];
    if (numberOfCredit_level != 0) {
        self.yellowImgView.hidden = NO;
        self.GrayImgView.hidden = NO;
        self.yellowImgView.frame = CGRectMake(8 , 0, [self starWidth:numberOfCredit_level], 17.5);
    }else{
        self.yellowImgView.hidden = YES;
        self.GrayImgView.hidden = YES;
    }
    if (numberOfCredit_level==0&&![user.verification_status isEqualToString:@"2"]) {
        self.coverView.hidden = YES;
    }else{
        self.coverView.hidden = NO;
    }
    self.sepatorView.backgroundColor = UIColorFromRGB(0xebddd5);
    
    [self.followBtn addTarget:self action:@selector(followBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    if (ApplicationDelegate.userId) {
        if ([@"1" isEqualToString:[NSString stringWithFormat:@"%@",user.following]]) {
            self.followBtn.selected = YES;
        }else{
            self.followBtn.selected = NO;
        }
    }else{
        self.followBtn.selected = NO;
    }
}
- (void)followBtnClick:(UIButton *)button{
//    btn.selected = !btn.selected;
//    if (btn.selected) {
//        _user.following = @"1";
//    }else{
//        _user.following = @"0";
//    }
    if (!ApplicationDelegate.Basic) {
        //判断是否是登录成功的状态
//        [_alert show];
        return;
    }
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSString *para = [NSString stringWithFormat:@"%@",_user.id];
    [parameter setObject:para forKey:@"user_id"];
    if ([_user.id integerValue]==[ApplicationDelegate.userId integerValue]) {
        [HUDManager showWarningWithText:@"不能关注自己"];
        return;
    }
    button.userInteractionEnabled = NO;
    if (button.selected) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"确认要取消关注吗？" message:nil delegate:self cancelButtonTitle:@"不了" otherButtonTitles:@"好", nil];
        [alert show];
        
    }else
    {
        [SLAPIHelper getfollow:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeUserData" object:nil];
            _user.following = @"1";
            [[NSNotificationCenter defaultCenter] postNotificationName:@"isUnfollow" object:self];
            button.userInteractionEnabled = YES;
            button.selected = !button.selected;
            [HUDManager showWarningWithText:@"关注成功"];
            if (self.didClickFollowBtn) {
                self.didClickFollowBtn(button);
            }
        } failure:^(SLHttpRequestError *error) {
            
            if (error.slAPICode == 35 || error.slAPICode == 34 || error.slAPICode == 11) {
                
            }else{
                [HUDManager showWarningWithText:@"关注失败"];
            }
            button.userInteractionEnabled = YES;
        }];
    }
}

- (CGFloat)starWidth:(NSInteger)rank{
    return rank*10.5+(rank-1)*3.5;
}


#pragma mark - collectionView代理
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.userTags.count;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *title = [self removeStringSpace:self.userTags[indexPath.row][@"name"]];
    //15.3为一个字宽度,这样做目的减少计算字体宽度,
    return CGSizeMake(15.3*title.length+20, 27.5);
}
//每个UICollectionView展s示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SLPersonTagsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"personFutagLabelCell" forIndexPath:indexPath];
//    NSLog(@"%p",cell);
    if (indexPath.row<self.user.primary_tags.count) {
        cell.type = @"primary";
    }else{
        cell.type = @"second";
    }
    cell.cTitle = [self removeStringSpace:self.userTags[indexPath.row][@"name"]];
    return cell;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

#pragma 去掉空格换行
- (NSString *)removeStringSpace:(NSString *)str{
    NSString *headerData = str;
    headerData = [headerData stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return headerData;
}
- (void)setLabelSpace:(UILabel*)label withValue:(NSString*)str withFont:(UIFont*)font {
    
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paraStyle.lineSpacing = 4; //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.paragraphSpacing = 2;

    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f
                          };
    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
    label.attributedText = attributeStr;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    NSLog(@"%d",buttonIndex);
    if (buttonIndex == 1) {
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        NSString *para = [NSString stringWithFormat:@"%@",_user.id];
        [parameter setObject:para forKey:@"user_id"];
        
        [SLAPIHelper cancelFollow:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeUserData" object:nil];
            _user.following = @"0";
            [[NSNotificationCenter defaultCenter] postNotificationName:@"isUnfollow" object:self];
            self.followBtn.userInteractionEnabled = YES;
            self.followBtn.selected = !self.followBtn.selected;
            [HUDManager showWarningWithText:@"取消关注"];
            if (self.didClickFollowBtn) {
                self.didClickFollowBtn(_followBtn);
            }
        } failure:^(SLHttpRequestError *error) {
            [HUDManager showWarningWithText:@"关注取消失败"];
            _followBtn.userInteractionEnabled = YES;
        }];
    }else{
        self.followBtn.userInteractionEnabled = YES;
    }
}
@end
