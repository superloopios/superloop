//
//  SLSequenceBtn.h
//  Superloop
//
//  Created by WangS on 16/5/14.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLSequenceBtn : UIButton


@property(nonatomic,strong)UIImageView *imgIcon;
@property(nonatomic,strong)UILabel *textLab;

@property(nonatomic,strong)UIImageView *topLine;
@property(nonatomic,strong)UIImageView *bottomLine;

@end
