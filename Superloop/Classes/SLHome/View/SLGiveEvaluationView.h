//
//  SLGiveEvaluationView.h
//  Superloop
//
//  Created by xiaowu on 16/10/21.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLGiveEvaluationView;

@protocol SLGiveEvaluationViewDelegate <NSObject>
@optional

- (void)SLGiveEvaluationView:(SLGiveEvaluationView *)evaluationView didClickBtnAtIndex:(NSInteger)index;

@end

@interface SLGiveEvaluationView : UIView
@property (nonatomic,weak)id<SLGiveEvaluationViewDelegate> delegate;
@end
