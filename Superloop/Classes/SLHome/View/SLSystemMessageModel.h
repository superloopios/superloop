//
//  SLSystemMessageModel.h
//  Superloop
//
//  Created by administrator on 16/7/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLSystemMessageModel : NSObject

@property(nonatomic, assign)CGFloat cellHeight;

//以下四项为后端完全控制的系统消息,客户端不对该消息进行重新组合编辑
//有的字段已经废弃,待修改
@property(nonatomic, copy)NSString *notiTitle;
@property(nonatomic, copy)NSString *subTitle;
@property(nonatomic, copy)NSString *content;
@property(nonatomic, copy)NSString *myreason;

@property(nonatomic, copy)NSString *topicTitle;
//推送的type,比如是通知还是申诉结果
@property(nonatomic, copy)NSNumber *msgtype;
//通话日期
@property(nonatomic, assign)NSUInteger date;
//通话日期
@property(nonatomic, assign)NSString *dateStr;
//通话时长
@property(nonatomic, assign)NSInteger time;
//创建时间
@property(nonatomic, assign)NSInteger created;
//通话时长
@property(nonatomic, assign)NSString *timeStr;
//理由
@property(nonatomic, copy)NSString *reason;
//对方的昵称
@property(nonatomic, copy)NSString *otherName;

@property(nonatomic, strong)NSDictionary *msgcontent;
//审核是否通过
@property(nonatomic,assign)BOOL isPass;
//退款
@property(nonatomic, assign)CGFloat drawback;

@property(nonatomic, assign)NSInteger updated;

@property(nonatomic, copy)NSString *updatedStr;
@end
