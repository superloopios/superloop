//
//  SLTagsCollectionViewCell.m
//  Superloop
//
//  Created by xiaowu on 16/8/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTagsCollectionViewCell.h"

@interface SLTagsCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *tagLab;

@end

@implementation SLTagsCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setCTitle:(NSString *)cTitle{
    _cTitle = cTitle;
    self.tagLab.text = cTitle;
}


@end
