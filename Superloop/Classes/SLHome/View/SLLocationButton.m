//
//  SLLocationButton.m
//  Superloop
//
//  Created by 朱宏伟 on 16/5/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLLocationButton.h"

@implementation SLLocationButton

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.titleLabel.textAlignment = NSTextAlignmentRight;
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        [self setTitleColor:SLColor(80, 80, 80) forState:UIControlStateNormal];
    }
    return self;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //self.titleLabel.width = 30;
    //self.titleLabel.y = 30;
//    self.titleLabel.x = -8;
    self.titleLabel.x = 0;

    //self.titleLabel.height = 30;
    
     self.imageView.centerY = self.height * 0.5;
    
    
//    self.imageView.x =self.titleLabel.x +self.titleLabel.bounds.size.width+5 ;
//    self.imageView.x =self.titleLabel.bounds.size.width+11 ;
    self.imageView.x =self.titleLabel.width+11 ;    
}

-(void)setFrame:(CGRect)frame
{

    frame.size.width += 11;
    [super setFrame:frame];
}





@end
