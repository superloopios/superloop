//
//  SLGiveEvaluationView.m
//  Superloop
//
//  Created by xiaowu on 16/10/21.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLGiveEvaluationView.h"

@interface SLGiveEvaluationView ()

@property(nonatomic, strong)UIView *evaluationView;
@property(nonatomic, strong)UILabel *titleLable;
@property(nonatomic, strong)UILabel *logsLabel;

//更新日志说明
@property(nonatomic, strong)NSArray *btnTitleArr;


@end

@implementation SLGiveEvaluationView



- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.frame = CGRectMake(0, 0, screen_W, screen_H);
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        //        self.updateView;
        self.evaluationView.layer.cornerRadius = 10;
        [self addCloseBtn];
        self.titleLable.text = @"赏我们一个好评吧";
        self.logsLabel.textColor = [UIColor blackColor];
        self.btnTitleArr = @[@"暂不评分",@"我要吐槽和建议",@"去给好评"];
        [self addBottomBtns];
    }
    return self;
}
- (void)addCloseBtn{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.evaluationView addSubview:btn];
//    btn.backgroundColor = [UIColor redColor];
    
    [btn setImage:[UIImage imageNamed:@"closeGiveEvalationView"] forState:UIControlStateNormal];
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@25);
        make.left.equalTo(self.evaluationView).offset(5);
        make.top.equalTo(self.evaluationView).offset(5);
    }];
    [btn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
}
- (void)addBottomBtns{
    for (int i=3; i > 0; i--) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.evaluationView addSubview:btn];
        [btn setTitleColor:SLMainColor forState:UIControlStateNormal];
        [btn setTitle:self.btnTitleArr[i-1] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@50);
            make.left.right.equalTo(self.evaluationView);
            make.top.equalTo(self.evaluationView.mas_bottom).offset(-i*50);
        }];
        UIView *btnBorder = [[UIView alloc] init];
        [btn addSubview:btnBorder];
        btnBorder.backgroundColor = SLColor(200, 200, 200);
        [btnBorder mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(btn);
            make.height.mas_equalTo(0.5);
        }];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = i;
    }
}
- (void)close{
    if ([self.delegate respondsToSelector:@selector(SLGiveEvaluationView:didClickBtnAtIndex:)]) {
        [self.delegate SLGiveEvaluationView:self didClickBtnAtIndex:1];
    }
}
- (void)btnClick:(UIButton *)btn{
    if ([self.delegate respondsToSelector:@selector(SLGiveEvaluationView:didClickBtnAtIndex:)]) {
        [self.delegate SLGiveEvaluationView:self didClickBtnAtIndex:btn.tag];
    }
}
#pragma mark - lazy
- (UIView *)evaluationView{
    if (!_evaluationView) {
        _evaluationView = [[UIView alloc] init];
        [self addSubview:_evaluationView];
        
        _evaluationView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.95];
        
        [_evaluationView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.centerY.equalTo(self);
            make.width.equalTo(@(250));
            make.height.equalTo(@(265));
        }];
    }
    return _evaluationView;
}

- (UILabel *)titleLable{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] init];
        _titleLable.font = [UIFont boldSystemFontOfSize:15];
        _titleLable.textColor = SLColor(0, 0, 0);
        [self.evaluationView addSubview:_titleLable];
        [_titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.evaluationView).offset(15);
            make.centerX.equalTo(self.evaluationView.mas_centerX);
        }];
    }
    return _titleLable;
}

- (UILabel *)logsLabel{
    if (!_logsLabel) {
        _logsLabel = [[UILabel alloc] init];
        _logsLabel.font = [UIFont systemFontOfSize:14];
        NSMutableAttributedString *absAttention=[[NSMutableAttributedString alloc] initWithString:@"如果用的开心，您的一个好评将激励我们提供更好的服务。您若用的不爽，欢迎吐槽我们(^_^)"];
        
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3;
        
        [absAttention addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, absAttention.length)];
        
        _logsLabel.attributedText=absAttention;
        _logsLabel.numberOfLines = 0;
        _logsLabel.textAlignment = NSTextAlignmentCenter;
        
        [self.evaluationView addSubview:_logsLabel];
        [_logsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.evaluationView).offset(15);
            make.right.equalTo(self.evaluationView).offset(-15);
            make.top.equalTo(self.titleLable.mas_bottom).offset(10);
        }];
    }
    return _logsLabel;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.evaluationView];
    if (point.x<0||point.y<0||point.x>250||point.y>265) {
        
        if ([self.delegate respondsToSelector:@selector(SLGiveEvaluationView:didClickBtnAtIndex:)]) {
            [self.delegate SLGiveEvaluationView:self didClickBtnAtIndex:1];
        }
    }
}

@end
