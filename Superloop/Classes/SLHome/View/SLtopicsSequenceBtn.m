//
//  SLtopicsSequenceBtn.m
//  Superloop
//
//  Created by WangS on 16/6/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLtopicsSequenceBtn.h"

@implementation SLtopicsSequenceBtn

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        
        _imgIcon=[[UIImageView alloc] init];
        [self addSubview:_imgIcon];
        
        [_imgIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(20);
            make.left.mas_equalTo(self.mas_left).offset(20);
            make.width.mas_equalTo(15);
            make.height.mas_equalTo(15);
            
        }];
        
        
        _textLab=[[UILabel alloc] init];
        _textLab.font=[UIFont systemFontOfSize:15];
        _textLab.textColor=[UIColor grayColor];
        _textLab.textAlignment=NSTextAlignmentLeft;
        [self addSubview:_textLab];
        
        [_textLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(18);
            make.left.mas_equalTo(_imgIcon.mas_right).offset(11);
            make.right.mas_equalTo(self.mas_right);
            make.height.mas_equalTo(15);
            
        }];
        
        _topLine=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ScreenW/2, 1)];
        [self addSubview:_topLine];
        
        
        _bottomLine=[[UIImageView alloc] initWithFrame:CGRectMake(0, 49, ScreenW/2, 1)];
        [self addSubview:_bottomLine];
        
    }
    return self;
}


@end
