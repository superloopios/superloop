//
//  SLCollectionCell.h
//  Superloop
//
//  Created by WangJiWei on 16/3/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLCollectionItem;
@interface SLCollectionCell : UICollectionViewCell
@property (nonatomic, strong) SLCollectionItem *collectionItem;
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
@end
