//
//  SLRankingBtn.m
//  Superloop
//
//  Created by WangS on 16/11/19.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLRankingBtn.h"

@implementation SLRankingBtn
-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        
        _imgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 68, 68)];
        [self addSubview:_imgView];
        
        _titleLab=[[UILabel alloc] initWithFrame:CGRectMake(0, 98, 68, 13)];
        _titleLab.textAlignment=NSTextAlignmentCenter;
        _titleLab.textColor=UIColorFromRGB(0x2b2124);
        _titleLab.font=[UIFont systemFontOfSize:13];
        [self addSubview:_titleLab];
    }
    return self;
}

@end
