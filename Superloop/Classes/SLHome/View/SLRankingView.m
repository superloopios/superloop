//
//  SLRankingView.m
//  Superloop
//
//  Created by WangS on 16/11/25.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLRankingView.h"
#import "SLRankingTableViewCell.h"
#import "SLRankingModel.h"

@interface SLRankingView ()<UITableViewDelegate,UITableViewDataSource,SLRankingTableViewCellDelegate>

@end
@implementation SLRankingView
- (UITableView *)rankingTableView{
    if (!_rankingTableView) {
        _rankingTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH-64-40) style:UITableViewStylePlain];
        _rankingTableView.delegate = self;
        _rankingTableView.dataSource = self;
        _rankingTableView.contentInset = UIEdgeInsetsMake(0, 0, 114, 0);
        _rankingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:_rankingTableView];
    }
    return _rankingTableView;
}
- (NSMutableArray *)rankingDataSources{
    if (!_rankingDataSources) {
        _rankingDataSources = [NSMutableArray new];
    }
    return _rankingDataSources;
}
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        UIView *footView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 0.1)];
        footView.backgroundColor=SLColor(244, 244, 244);
        self.rankingTableView.tableFooterView=footView;
        [self.rankingTableView registerClass:[SLRankingTableViewCell class] forCellReuseIdentifier:@"RankingCell"];
    }
    return self;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.rankingDataSources.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLRankingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RankingCell"];
    SLRankingModel *rankingModel = self.rankingDataSources[indexPath.row];
    rankingModel.rankingSort = indexPath.row;
    [self.rankingDataSources replaceObjectAtIndex:indexPath.row withObject:rankingModel];
    cell.rankingModel = rankingModel;
    cell.indexPath = indexPath;
    cell.delegate = self;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 114;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SLRankingModel *rankingModel = self.rankingDataSources[indexPath.row];
//    if ([self.delegate respondsToSelector:@selector(SLRankingView:didSelectCellRankingModel:)]) {
//        [self.delegate SLRankingView:self didSelectCellRankingModel:rankingModel];
//    }
    if ([self.delegate respondsToSelector:@selector(SLRankingView:didSelectCellRankingModel:indexPath:)]) {
        [self.delegate SLRankingView:self didSelectCellRankingModel:rankingModel indexPath:indexPath];
    }
}
- (void)SLRankingTableViewCell:(SLRankingTableViewCell *)rankingTableViewCell askBtnClickIndexPath:(NSIndexPath *)indexPath rankingModel:(SLRankingModel *)rankingModel{
    if ([self.delegate respondsToSelector:@selector(SLRankingView:didAskBtnClickIndexPath:rankingModel:)]) {
        [self.delegate SLRankingView:self didAskBtnClickIndexPath:indexPath rankingModel:rankingModel];
    }
}
@end
