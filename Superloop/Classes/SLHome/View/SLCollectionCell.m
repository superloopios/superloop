//
//  SLCollectionCell.m
//  Superloop
//
//  Created by WangJiWei on 16/3/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "SLCollectionItem.h"
#import "SLCollectionCell.h"
@interface SLCollectionCell()

@end
@implementation SLCollectionCell



- (void)setCollectionItem:(SLCollectionItem *)collectionItem
{
    _collectionItem = collectionItem;

    _cellLabel.text = collectionItem.name;

}

@end
