//
//  SLCateButton.m
//  Superloop
//
//  Created by WangJiwei on 16/4/6.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCateButton.h"

@implementation SLCateButton

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.titleLabel.textAlignment = NSTextAlignmentRight;
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        [self setTitleColor:SLMainColor forState:UIControlStateNormal];
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //根据计算文字的大小
    CGFloat btnWidth= [self.titleLabel.text boundingRectWithSize:CGSizeMake(ScreenW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.width+2;
    if (btnWidth > screen_W/3 - 50) {
        btnWidth = screen_W/3 -50;
    }
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.titleLabel.width = btnWidth;
    self.titleLabel.x = (self.width-btnWidth-self.imageView.width-10)/2.0;
    self.titleLabel.y = self.height * 0.5/2.0;
    self.titleLabel.height = self.height * 0.5;
    self.imageView.centerY = self.height * 0.5;
    self.imageView.x = self.titleLabel.x+btnWidth + 10;
    
    
}

@end
