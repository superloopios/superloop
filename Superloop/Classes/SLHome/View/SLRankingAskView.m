//
//  SLRankingAskView.m
//  Superloop
//
//  Created by WangS on 16/11/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLRankingAskView.h"
#import "SLRankingBtn.h"
#import "SLRankingModel.h"
#import "SLMessageManger.h"
#import "AppDelegate.h"
#import "SLAPIHelper.h"
#import "SLCallingViewController.h"

@interface SLRankingAskView ()<UIAlertViewDelegate>
@property (nonatomic,strong) UIView *rankingAskView;
@property (nonatomic,strong) SLRankingModel *rankingModel;
@property (nonatomic,strong) UIViewController *currentViewController;
@property (nonatomic,strong) SLRankingBtn *followBtn;
@property (nonatomic,strong) UINavigationController *navigationController;
@property (nonatomic,strong) NSIndexPath *indexPath;
@property (nonatomic,strong) UIImageView *iconImgView;
@property (nonatomic,strong) UILabel *nicknameLab;
@property (nonatomic,strong) NSMutableDictionary *parameter;
@end

@implementation SLRankingAskView
- (NSMutableDictionary *)parameter{
    if (!_parameter) {
        _parameter = [NSMutableDictionary new];
    }
    return _parameter;
}
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initRankingAskView];
    }
    return self;
}
- (void)initRankingAskView{
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] init];
    [singleTap addTarget:self action:@selector(cancelBtnClick)];
    [self addGestureRecognizer:singleTap];
    
    UIView *rankingAskView=[[UIView alloc] initWithFrame:CGRectMake(0, ScreenH, ScreenW, ScreenH)];
    rankingAskView.backgroundColor=[UIColor colorWithRed:232/255.0 green:232/255.0 blue:232/255.0 alpha:0.9];
    self.rankingAskView = rankingAskView;
    [self addSubview:rankingAskView];

    UIImageView *iconImgView = [[UIImageView alloc] initWithFrame:CGRectMake((ScreenW-78)/2, ScreenH-450, 78, 78)];
    self.iconImgView = iconImgView;
    [rankingAskView addSubview:iconImgView];
    
    UILabel *nicknameLab = [[UILabel alloc] initWithFrame:CGRectMake(50, ScreenH-350, ScreenW-100, 16)];
    nicknameLab.font = [UIFont boldSystemFontOfSize:16];
    nicknameLab.textColor = [UIColor blackColor];
    nicknameLab.textAlignment = NSTextAlignmentCenter;
    self.nicknameLab = nicknameLab;
    [rankingAskView addSubview:nicknameLab];
    
    SLRankingBtn *msgBtn = [[SLRankingBtn alloc] initWithFrame:CGRectMake((ScreenW-68*3)/4, ScreenH-280, 68, 101)];
    msgBtn.tag = 100;
    msgBtn.imgView.image = [UIImage imageNamed:@"rankingMsg"];
    msgBtn.titleLab.text = @"先聊两句";
    [msgBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [rankingAskView addSubview:msgBtn];
    
    SLRankingBtn *phoneBtn = [[SLRankingBtn alloc] initWithFrame:CGRectMake((ScreenW-68*3)/4*2+68, ScreenH-280, 68, 101)];
    phoneBtn.tag = 101;
    phoneBtn.imgView.image = [UIImage imageNamed:@"rankingPhone"];
    phoneBtn.titleLab.text = @"立即通话";
    [phoneBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [rankingAskView addSubview:phoneBtn];
    
    
    SLRankingBtn *followBtn = [[SLRankingBtn alloc] initWithFrame:CGRectMake((ScreenW-68*3)/4*3+68*2, ScreenH-280, 68, 101)];
    followBtn.tag = 102;
    [followBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.followBtn = followBtn;
    [rankingAskView addSubview:followBtn];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake((ScreenW-50)/2, ScreenH - 110, 50, 50);
    [cancelBtn setImage:[UIImage imageNamed:@"rankingCancel"] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [rankingAskView addSubview:cancelBtn];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.rankingAskView.frame = CGRectMake(0, 0, ScreenW, ScreenH);
    }];
}
-(void)cancelBtnClick{
    [UIView animateWithDuration:0.3 animations:^{
        self.rankingAskView.frame = CGRectMake(0, ScreenH, ScreenW, ScreenH);
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self removeFromSuperview];
    });
}
-(void)btnClick:(SLRankingBtn *)btn{
    NSInteger index=btn.tag-100;
    switch (index) {
        case 0:
            [self msgBtnClick];
            break;
        case 1:
            [self phoneBtnClick];
            break;
        case 2:
            [self followBtnClick];
            break;
        default:
            [self cancelBtnClick];
            break;
    }
}
- (void)msgBtnClick{
    if ([[NSString stringWithFormat:@"%@",ApplicationDelegate.userId] isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.rankingModel.id]]) {
        [HUDManager showAlertWithText:@"不能给自己发消息"];
        return;
    }
    [self cancelBtnClick];
    NSString *chatName = [NSString stringWithFormat:@"%ld",(long)self.rankingModel.id];
    YWPerson *person = [[YWPerson alloc] initWithPersonId:chatName.description];
    ApplicationDelegate.imTitlename = self.rankingModel.nickname;
    [[SLMessageManger sharedInstance] openConversationViewControllerWithPerson:person fromNavigationController:self.navigationController];
}
- (void)phoneBtnClick{
    if ([[NSString stringWithFormat:@"%@",ApplicationDelegate.userId] isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.rankingModel.id]]) {
        [HUDManager showAlertWithText:@"不能给自己打电话"];
        return;
    }
    [self cancelBtnClick];
    [self loadPriceData];
}
- (void)followBtnClick{
    if ([[NSString stringWithFormat:@"%@",ApplicationDelegate.userId] isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.rankingModel.id]]) {
        [HUDManager showAlertWithText:@"不能关注自己"];
        [self cancelBtnClick];
        return;
    }
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSString *para = [NSString stringWithFormat:@"%ld",(long)self.rankingModel.id];
    [parameter setObject:para forKey:@"user_id"];
    self.parameter = parameter;
    self.followBtn.enabled = NO;
    if (!self.followBtn.selected) {
        [SLAPIHelper getfollow:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            self.followBtn.enabled = YES;
            self.followBtn.selected = !self.followBtn.selected;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"isUnfollow" object:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeUserData" object:self];
            self.followBtn.imgView.image = [UIImage imageNamed:@"rankingMsg"];
            self.followBtn.titleLab.text = @"已关注";
            [ApplicationDelegate.rankingFollow setValue:@"1" forKey:[NSString stringWithFormat:@"%ld",(long)self.rankingModel.id]];
            [HUDManager showWarningWithText:@"关注成功"];
            self.rankingModel.following = YES;
            if ([self.delegate respondsToSelector:@selector(SLRankingAskView:didSelectModel:indexPath:)]) {
                [self.delegate SLRankingAskView:self didSelectModel:self.rankingModel indexPath:self.indexPath];
            }
            [self cancelBtnClick];
        } failure:^(SLHttpRequestError *error) {
            if (error.slAPICode == 35 || error.slAPICode == 34) {
                
            }else{
                [HUDManager showWarningWithText:@"关注失败"];
            }
            self.followBtn.enabled = YES;
        }];
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"确认取消关注吗？" message:nil delegate:self cancelButtonTitle:@"不了" otherButtonTitles:@"好", nil];
        alert.tag = 100;
        [alert show];
//        [SLAPIHelper cancelFollow:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
//            self.followBtn.selected = !self.followBtn.selected;
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"isUnfollow" object:self];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeUserData" object:self];
//            self.followBtn.imgView.image = [UIImage imageNamed:@"rankingFollow"];
//            self.followBtn.titleLab.text = @"关注";
//            [ApplicationDelegate.rankingFollow setValue:@"0" forKey:[NSString stringWithFormat:@"%ld",(long)self.rankingModel.id]];
//            [HUDManager showWarningWithText:@"关注取消"];
//            self.rankingModel.following = NO;
//            if ([self.delegate respondsToSelector:@selector(SLRankingAskView:didSelectModel:indexPath:)]) {
//                [self.delegate SLRankingAskView:self didSelectModel:self.rankingModel indexPath:self.indexPath];
//            }
//            self.followBtn.enabled = YES;
//        } failure:^(SLHttpRequestError *error) {
//            [HUDManager showWarningWithText:@"关注取消失败"];
//            self.followBtn.enabled = YES;
//        }];
    }
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [SLAPIHelper cancelFollow:self.parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            self.followBtn.selected = !self.followBtn.selected;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"isUnfollow" object:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeUserData" object:self];
            self.followBtn.imgView.image = [UIImage imageNamed:@"rankingFollow"];
            self.followBtn.titleLab.text = @"关注";
            [ApplicationDelegate.rankingFollow setValue:@"0" forKey:[NSString stringWithFormat:@"%ld",(long)self.rankingModel.id]];
            [HUDManager showWarningWithText:@"关注取消"];
            self.rankingModel.following = NO;
            if ([self.delegate respondsToSelector:@selector(SLRankingAskView:didSelectModel:indexPath:)]) {
                [self.delegate SLRankingAskView:self didSelectModel:self.rankingModel indexPath:self.indexPath];
            }
            self.followBtn.enabled = YES;
            [self cancelBtnClick];
        } failure:^(SLHttpRequestError *error) {
            [HUDManager showWarningWithText:@"关注取消失败"];
            self.followBtn.enabled = YES;
        }];
    }
}
// 获取通话资费价格
- (void)loadPriceData{
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow
                          withText:@""];
    [SLAPIHelper getPriceData:[NSString stringWithFormat:@"%ld",(long)self.rankingModel.id] success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        [HUDManager hideHUDView];
        SLCallingViewController *callView = [[SLCallingViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:callView];
        callView.user=data[@"result"];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        if (failure.slAPICode==-1) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [HUDManager showWarningWithText:@"请检查网络状况"];
            });
        }
    }];
}

-(void)showViewWithModel:(SLRankingModel *)rankingModel indexPath:(NSIndexPath *)indexPath viewController:(UIViewController *)currentViewController navigationController:(UINavigationController *)navigationController{
    self.rankingModel = rankingModel;
    self.indexPath = indexPath;
    self.currentViewController = currentViewController;
    self.navigationController = navigationController;
    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:rankingModel.avatar] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    self.nicknameLab.text = rankingModel.nickname;
    if (self.rankingModel.following) {
        self.followBtn.selected = YES;
        self.followBtn.imgView.image = [UIImage imageNamed:@"rankingFollowed"];
        self.followBtn.titleLab.text = @"已关注";
    }else{
        self.followBtn.selected = NO;
        self.followBtn.imgView.image = [UIImage imageNamed:@"rankingFollow"];
        self.followBtn.titleLab.text = @"关注";
    }
    for (NSString *useId in ApplicationDelegate.rankingFollow) {
        if ([useId isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.rankingModel.id]]) {
            NSString *isFollow = [ApplicationDelegate.rankingFollow valueForKey:useId];
            if ([isFollow isEqualToString:@"1"]) {
                self.followBtn.selected = YES;
                self.followBtn.imgView.image = [UIImage imageNamed:@"rankingFollowed"];
                self.followBtn.titleLab.text = @"已关注";
            }else{
                self.followBtn.selected = NO;
                self.followBtn.imgView.image = [UIImage imageNamed:@"rankingFollow"];
                self.followBtn.titleLab.text = @"关注";
            }
            return;
        }
    }
}
@end
