//
//  SLUpdateView.m
//  Superloop
//
//  Created by xiaowu on 16/8/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLUpdateView.h"
#import "ColorUtility.h"
#import "UILabel+StringFrame.h"

@interface SLUpdateView ()

@property(nonatomic, strong)UIView *updateView;
@property(nonatomic, strong)UILabel *titleLable;
@property(nonatomic, strong)UILabel *versionLabel;
@property(nonatomic, strong)UILabel *logLabel;
@property(nonatomic, strong)UIScrollView *scrollView;
@property(nonatomic, strong)UIButton *ignoreButton;
@property(nonatomic, strong)UIButton *goStoreButton;
@property(nonatomic, strong)UIView *sepatorView;
//更新日志说明
@property(nonatomic, strong)UILabel *logsLabel;


@end

@implementation SLUpdateView

- (UILabel *)logsLabel{
    if (!_logsLabel) {
        _logsLabel = [[UILabel alloc] init];
        _logsLabel.font = [UIFont systemFontOfSize:14];
        _logsLabel.textColor = [UIColor blackColor];
        _logsLabel.numberOfLines = 0;
        [self.scrollView addSubview:_logsLabel];
        [_logsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.scrollView);
            make.top.equalTo(self.scrollView);
            make.width.equalTo(@(280*picWScale-20));
        }];
    }
    return _logsLabel;
}

- (UIView *)sepatorView{
    if (!_sepatorView) {
        _sepatorView = [[UIView alloc] init];
        _sepatorView.backgroundColor = SLColor(230, 230, 230);
        [self addSubview:_sepatorView];
        [_sepatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.scrollView.mas_bottom).offset(0.5);
            make.left.right.equalTo(self.updateView);
            make.height.equalTo(@1);
        }];
    }
    return _sepatorView;
}
- (UIButton *)ignoreButton{
    if (!_ignoreButton) {
        _ignoreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _ignoreButton.tag = 0;
        _ignoreButton.backgroundColor = [UIColor whiteColor];
        _ignoreButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [_ignoreButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.updateView addSubview:_ignoreButton];
        [_ignoreButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.updateView);
            make.top.equalTo(self.sepatorView.mas_bottom);
            make.right.equalTo(self.updateView.mas_centerX);
            make.height.equalTo(@40);
        }];
        [_ignoreButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _ignoreButton;
}
- (UIButton *)goStoreButton{
    if (!_goStoreButton) {
        _goStoreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _goStoreButton.tag = 1;
        _goStoreButton.backgroundColor = SLMainColor;
        _goStoreButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [_goStoreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.updateView addSubview:_goStoreButton];
        
        [_goStoreButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _goStoreButton;
}

- (UILabel *)titleLable{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] init];
        _titleLable.font = [UIFont systemFontOfSize:15];
        _titleLable.textColor = SLColor(80, 80, 80);
        [self.updateView addSubview:_titleLable];
        
        [_titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.updateView).offset(10);
            make.height.equalTo(@15);
            make.centerX.equalTo(self.updateView.mas_centerX);
        }];
    }
    return _titleLable;
}

- (UILabel *)versionLabel{
    if (!_versionLabel) {
        _versionLabel = [[UILabel alloc] init];
        _versionLabel.font = [UIFont boldSystemFontOfSize:15];
        _versionLabel.textColor = [UIColor blackColor];
        [self.updateView addSubview:_versionLabel];
        [_versionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLable.mas_bottom).offset(4);
            make.left.equalTo(self.updateView.mas_left).offset(10);
            make.height.equalTo(@15);
        }];
    }
    return _versionLabel;
}
- (UILabel *)logLabel{
    if (!_logLabel) {
        _logLabel = [[UILabel alloc] init];
        _logLabel.font = [UIFont boldSystemFontOfSize:15];
        _logLabel.textColor = [UIColor blackColor];
        [self.updateView addSubview:_logLabel];
        [_logLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.versionLabel.mas_bottom).offset(4);
            make.left.equalTo(self.versionLabel.mas_left);
            make.height.equalTo(@15);
        }];
    }
    return _logLabel;
}
- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        [self.updateView addSubview:_scrollView];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.scrollEnabled = YES;
        [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.logLabel.mas_bottom).offset(4);
            make.left.equalTo(self.logLabel.mas_left);
            make.right.equalTo(self.updateView.mas_right).offset(-10);
            make.bottom.equalTo(self.updateView).offset(-41);
        }];
    }
    return _scrollView;
}

- (UIView *)updateView{
    if (!_updateView) {
        _updateView = [[UIView alloc] init];
        [self addSubview:_updateView];
        _updateView.backgroundColor = [UIColor whiteColor];
        
    }
    return _updateView;
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.frame = CGRectMake(0, 0, screen_W, screen_H);
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        self.updateView.layer.cornerRadius = 5;
        self.updateView.clipsToBounds = YES;
    }
    return self;
}

- (void)setDict:(NSDictionary *)dict{
    _dict = dict;
//    107为出去更新日志以外的高度
    NSString *logStr = dict[@"version_log"];
    CGSize titleSize = [logStr boundingRectWithSize:CGSizeMake(280*picWScale-20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size;
    if (titleSize.height+107>screen_H*0.6) {
        [self.updateView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY);
            make.width.equalTo(@(280*picWScale));
            make.height.equalTo(@(screen_H*0.6));
        }];
    }else if (titleSize.height+127<280*picWScale){
        [self.updateView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY);
            make.width.height.equalTo(@(280*picWScale));
        }];
    }else{
        [self.updateView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY);
            make.width.equalTo(@(280*picWScale));
            make.height.equalTo(@(titleSize.height+127));
        }];
    }
    
    self.titleLable.text = @"新版本更新";
    self.versionLabel.text = [NSString stringWithFormat:@"V %@",dict[@"version_code"]];
    self.logLabel.text = @"更新日志:";
    
    self.logsLabel.text = logStr;
    
    CGSize labelSize = [self.logsLabel boundingRectWithString:self.logsLabel.text withSize:CGSizeMake(280*picWScale-20, MAXFLOAT) withFont:14];
    
    labelSize.height += 10;
    [self.scrollView setContentSize:labelSize];
    
    self.sepatorView.backgroundColor = SLColor(230, 230, 230);
    [self.ignoreButton setTitle:@"暂不更新" forState:UIControlStateNormal];
    [self.goStoreButton setTitle:@"立即更新" forState:UIControlStateNormal];
    if ([dict[@"force_upgrade"] isEqual:@1]) {
        [_goStoreButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.updateView);
            make.top.equalTo(self.sepatorView.mas_bottom);
            make.left.equalTo(self.updateView);
            make.height.equalTo(@40);
        }];
    }else{
        [_goStoreButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.updateView);
            make.top.equalTo(self.sepatorView.mas_bottom);
            make.left.equalTo(self.updateView.mas_centerX);
            make.height.equalTo(@40);
        }];
    }
    
}
- (void)btnClick:(UIButton *)btn{
    if ([self.delegate respondsToSelector:@selector(SLUpdateView:didClickBtnAtIndex:)]) {
        [self.delegate SLUpdateView:self didClickBtnAtIndex:btn.tag];
    }
}
@end
