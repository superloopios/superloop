//
//  SLTagsCollectionViewCell.h
//  Superloop
//
//  Created by xiaowu on 16/8/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLTagsCollectionViewCell : UICollectionViewCell

@property(nonatomic, copy)NSString *cTitle;

@end
