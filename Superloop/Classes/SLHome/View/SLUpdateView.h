//
//  SLUpdateView.h
//  Superloop
//
//  Created by xiaowu on 16/8/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLUpdateView;

@protocol SLUpdateViewDelegate <NSObject>
@optional

- (void)SLUpdateView:(SLUpdateView *)updateView didClickBtnAtIndex:(NSInteger)index;

@end

@interface SLUpdateView : UIView

@property(nonatomic,strong)NSDictionary *dict;
@property (nonatomic,weak)id<SLUpdateViewDelegate> delegate;

@end
