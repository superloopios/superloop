//
//  SLCategoryTableViewCell.h
//  Superloop
//
//  Created by administrator on 16/7/26.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLCategoryModel;
@class SLCategoryTableViewCell;

@protocol SLCategoryTableViewCellDelegate <NSObject>
@optional
- (void)categoryTableViewCell:(SLCategoryTableViewCell *)categoryCell clickIsOpenBtnAtIndex:(NSIndexPath *)indexpath;  
@end

@interface SLCategoryTableViewCell : UITableViewCell

//头部的image
@property(nonatomic, strong)UIImageView *iconIV;

@property(nonatomic, strong)SLCategoryModel *model;

@property(nonatomic, strong)NSIndexPath *indexPath;

@property (nonatomic, strong)UIView *btnView;

@property (nonatomic,strong) UICollectionView *collectionView;

@property (nonatomic, weak) id<SLCategoryTableViewCellDelegate>delegate;



@end
