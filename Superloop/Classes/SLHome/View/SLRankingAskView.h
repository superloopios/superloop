//
//  SLRankingAskView.h
//  Superloop
//
//  Created by WangS on 16/11/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLRankingModel;
@class SLRankingAskView;
@protocol SLRankingAskViewDelegate <NSObject>
- (void)SLRankingAskView:(SLRankingAskView *)rankingAskView didSelectModel:(SLRankingModel *)rankingModel indexPath:(NSIndexPath *)indexPath;
@end
@interface SLRankingAskView : UIView
@property (nonatomic,weak) id<SLRankingAskViewDelegate>delegate;
@property (nonatomic,assign) BOOL following;
-(void)showViewWithModel:(SLRankingModel *)rankingModel indexPath:(NSIndexPath *)indexPath viewController:(UIViewController *)currentViewController navigationController:(UINavigationController *)navigationController;
@end
