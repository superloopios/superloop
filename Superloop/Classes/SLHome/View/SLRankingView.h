//
//  SLRankingView.h
//  Superloop
//
//  Created by WangS on 16/11/25.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLRankingModel;
@class SLRankingView;
@protocol SLRankingViewDelegate <NSObject>
- (void)SLRankingView:(SLRankingView *)rankingView didAskBtnClickIndexPath:(NSIndexPath *)indexPath  rankingModel:(SLRankingModel *)rankingModel;
- (void)SLRankingView:(SLRankingView *)rankingView didSelectCellRankingModel:(SLRankingModel *)rankingModel indexPath:(NSIndexPath *)indexPath;
@end
@interface SLRankingView : UIView
@property (nonatomic,strong) UITableView *rankingTableView;
@property (nonatomic,strong) NSMutableArray *rankingDataSources;
@property (nonatomic,weak) id<SLRankingViewDelegate>delegate;
@end
