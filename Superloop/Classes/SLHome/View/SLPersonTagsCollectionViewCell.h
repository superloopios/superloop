//
//  SLPersonTagsCollectionViewCell.h
//  Superloop
//
//  Created by xiaowu on 16/8/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLPersonTagsCollectionViewCell : UICollectionViewCell

@property(nonatomic, copy)NSString *cTitle;

@property (nonatomic, copy)NSString *type;

@end
