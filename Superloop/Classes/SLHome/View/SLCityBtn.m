//
//  SLCityBtn.m
//  Superloop
//
//  Created by 朱宏伟 on 16/6/25.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCityBtn.h"

@implementation SLCityBtn

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.titleLabel.textAlignment = NSTextAlignmentRight;
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        [self setTitleColor:SLColor(80, 80, 80) forState:UIControlStateNormal];
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.titleLabel.width = self.width * 0.5;
    self.titleLabel.y = 0;
    self.titleLabel.x = 0;
    self.titleLabel.height = self.height;
    
    self.imageView.centerY = self.height * 0.5;
    self.imageView.x = self.width * 0.5 + 10;
    
}

@end
