//
//  SLPersonTagsCollectionViewCell.m
//  Superloop
//
//  Created by xiaowu on 16/8/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLPersonTagsCollectionViewCell.h"

@interface SLPersonTagsCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *tagLabel;



@end

@implementation SLPersonTagsCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 13.75;
    self.layer.masksToBounds = NO;
}
- (void)setCTitle:(NSString *)cTitle{
    _cTitle = cTitle;
    self.tagLabel.text = cTitle;
}
- (void)setType:(NSString *)type{
    _type = type;
    if ([type isEqualToString:@"primary"]) {
//        self.backgroundColor = UIColorFromRGB(0xf7e3df);
        self.layer.borderWidth = 0.6;
        self.layer.borderColor = UIColorFromRGB(0xf4604c).CGColor;
        self.tagLabel.textColor = UIColorFromRGB(0xcf524e);
    }else{
//        self.backgroundColor = SLColor(235, 235, 235);
        self.layer.borderWidth = 0.6;
        self.layer.borderColor = UIColorFromRGB(0xb3907d).CGColor;
        self.tagLabel.textColor = UIColorFromRGB(0x9e7f7b);
    }
}
@end
