//
//  SLRankingTableViewCell.h
//  Superloop
//
//  Created by WangS on 16/11/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLRankingModel;
@class SLRankingTableViewCell;
@protocol SLRankingTableViewCellDelegate <NSObject>
- (void)SLRankingTableViewCell:(SLRankingTableViewCell *)rankingTableViewCell askBtnClickIndexPath:(NSIndexPath *)indexPath rankingModel:(SLRankingModel *)rankingModel;
@end
@interface SLRankingTableViewCell : UITableViewCell
@property (nonatomic,weak) id<SLRankingTableViewCellDelegate> delegate;
@property (nonatomic,strong) SLRankingModel *rankingModel;
@property (nonatomic,strong) NSIndexPath *indexPath;
@end
