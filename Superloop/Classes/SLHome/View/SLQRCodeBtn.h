//
//  SLQRCodeBtn.h
//  Superloop
//
//  Created by WangS on 16/8/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLQRCodeBtn : UIButton

@property (nonatomic,strong) UIImageView *imgView;
@property (nonatomic,strong) UILabel *titleLab;

@end
