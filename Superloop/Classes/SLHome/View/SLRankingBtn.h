//
//  SLRankingBtn.h
//  Superloop
//
//  Created by WangS on 16/11/19.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLRankingBtn : UIButton
@property (nonatomic,strong) UIImageView *imgView;
@property (nonatomic,strong) UILabel *titleLab;
@end
