//
//  SLJavascriptBridgeEvent.m
//  Superloop
//
//  Created by WangJiWei on 16/3/21.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "SLIntroduceViewController.h"
#import "SLAddressViewController.h"
#import "SLMineBasicInfoViewController.h"
#import "SLCheckPhotoViewController.h"
#import "SLUserAccountViewController.h"
#import "SLPriceViewController.h"
#import "SLJavascriptBridgeEvent.h"
#import "SLWebViewController.h"
#import "AppDelegate.h"
#import <AFNetworking.h>
#import "SLMyTopicViewController.h"
#import "SLCollectionTopicViewController.h"
#import <Masonry.h>
#import "SLMessageManger.h"
#import "SLCallingViewController.h"
#import <MBProgressHUD.h>
#import "SLMyCommentViewController.h"
#import "SLFansTableViewController.h"
#import "SLHarassmentViewController.h"
#import "SLAPIHelper.h"
#import "SLHttpRequest.h"
#import "AppDelegate.h"
#import "SLFirstViewController.h"
#import "UMSocial.h"
#import <UIImageView+WebCache.h>
#import "SLValueUtils.h"
#import "SLNavgationViewController.h"

@interface SLJavascriptBridgeEvent()<UIWebViewDelegate,UMSocialUIDelegate,UIAlertViewDelegate>
@property (nonatomic, strong)SLWebView *webView;
@property (nonatomic, weak)UIViewController* parentController;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIView *loadingView;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic,strong)UIImageView *imageVw;  //分享的头像的view
@property (nonatomic,strong)UIAlertView *alert;
@property (nonatomic,strong)UIView *backView;

@property (nonatomic,strong)NSString *urlStr; //分享的url
@end
@implementation SLJavascriptBridgeEvent
-(instancetype)init:(SLWebView*)webView ParentController:(UIViewController*)parentController{
    self.webView = webView;
//    webView.delegate = self;
    self.parentController = parentController;
    
    _alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
    if (self = [super init]){
        [self initWVJBridge];
    }
    return self;
}

-(void) initWVJBridge{
    if (_bridge) { return; }
    _bridge = [WKWebViewJavascriptBridge bridgeForWebView:self.webView];
    [self initBridgeEvent];
}
- (void)registerHandler:(NSString *)handlerName handler:(WVJBHandler)handler {
    [_bridge registerHandler:handlerName handler:handler];
}
- (void)callHandler:(NSString *)handlerName data:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    [_bridge callHandler:handlerName data:data responseCallback:responseCallback];
}

-(void) initBridgeEvent{
    [self httpRequest];
    [self getUser];
    [self setUpActivityIndicator];
    [self setUpAlert];
    [self openIM];
    [self openCall];
}

#pragma mark ---- 打开聊天界面
- (void)openIM{
    [self registerHandler:@"IM" handler:^(id data, WVJBResponseCallback responseCallback) {
        if (!ApplicationDelegate.Basic) {
            //判断是否是登录成功的状态
            [_alert show];
            return;
        }
        NSString *otherUserID = [NSString stringWithFormat:@"%@",data[@"id"]];
        YWPerson *person = [[YWPerson alloc] initWithPersonId:otherUserID.description];
        [_parentController.navigationController setNavigationBarHidden:YES];
        ApplicationDelegate.imTitlename = data[@"nickname"];
        [[SLMessageManger sharedInstance] openConversationViewControllerWithPerson:person fromNavigationController:_parentController.navigationController];
    }];
}

#pragma mark --- 打开通话

- (void)openCall{
    [self registerHandler:@"call" handler:^(id data, WVJBResponseCallback responseCallback) {
        if (!ApplicationDelegate.Basic) {
            //判断是否是登录成功的状态
            [_alert show];
            return;
        }
        NSString *uid = [NSString stringWithFormat:@"%@",data[@"id"]];
        [self loadPriceData:uid];
    }];
}

// 获取用户信息
- (void)loadPriceData:(NSString *)uid{
    if (!uid) {
        return;
    }
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow
                          withText:@""];
    [SLAPIHelper getPriceData:uid success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        [HUDManager hideHUDView];
        NSLog(@"%@", data[@"result"][@"price"]);
        SLCallingViewController *callView = [[SLCallingViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:callView];
        callView.user = data[@"result"];
        
        [_parentController.navigationController presentViewController:nav animated:YES completion:nil];
        
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        if (failure.slAPICode==-1) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [HUDManager showWarningWithText:@"请检查网络状况"];
            });
        }
    }];
}
//设置loading
- (void)setUpActivityIndicator
{
    [self registerHandler:@"loading" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSString *show = data[@"show"];
        
        if ([show.description isEqualToString:@"1"]) {
            _hud = [MBProgressHUD showHUDAddedTo:_parentController.view animated:YES];
            [_hud show:YES];
        }else if ([show.description isEqualToString:@"0"]){
            
        }
        [_hud hide:YES];
    }];
}
//设置弹窗
- (void)setUpAlert{
    [self registerHandler:@"alert" handler:^(id data, WVJBResponseCallback responseCallback) {
//        if (!ApplicationDelegate.Basic) {
//            //判断是否是登录成功的状态
//            [_alert show];
//            return;
//        }
        NSLog(@"data--------%@",data);
        NSString *alertContent=[[NSString alloc] init];
        NSInteger alertContentInt=[data[@"content"] integerValue];
        switch (alertContentInt) {
            case 0:
                alertContent=@"关注成功";
                break;
            case 1:
                alertContent=@"已取消关注";
                break;
            case 2:
                alertContent=@"对方已在您的黑名单中";
                break;
            case 3:
                alertContent=@"对方将您加入了黑名单";
                break;
            default:
                alertContent=@"未知错误";
                break;
        }
        NSString *alertDelay = data[@"delay"];
        if (!alertDelay) {
            alertDelay = @"3";
        }
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_parentController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(alertContent, @"HUD message title");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"isUnfollow" object:self];
        
        float floatString = [alertDelay floatValue];
        hud.userInteractionEnabled = NO;
        [hud hide:YES afterDelay:floatString];
    }];
}
- (void)timerFireMethod:(NSTimer*)theTimer//弹出框
{
    UIAlertView *promptAlert = (UIAlertView*)[theTimer userInfo];
    [promptAlert dismissWithClickedButtonIndex:0 animated:NO];
    promptAlert =NULL;
}

-(void) httpRequest{
    
    [self registerHandler:@"httpRequest" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSString *type = data[@"type"];
        if(type == nil)
        {
            type = @"GET";
            return;
        }
        NSString *url = data[@"url"];
        if(url == nil)return;
        NSDictionary *params = data[@"params"];
        NSDictionary *dataParams = data[@"data"];
        NSLog(@"%@", dataParams);
        SLHttpMethod method = Get;
        if ([type isEqualToString:@"GET"]) {
            method = Get;
        }else if([type isEqualToString:@"POST"]) {
            method = Post;
            params = dataParams;
        }else if([type isEqualToString:@"PUT"]) {
            method = Put;
            params = dataParams;
        }else if([type isEqualToString:@"DELETE"]) {
            method = Delete;
            params = dataParams;
        }
       
        [SLAPIHelper requestHandleErrorWithUrl:url headers:nil params:params method:method success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            if(data == nil){
                return;
            }
            
            NSError * err;
            NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:data options:0 error:&err];
            if(err){
                return;
            }
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSDictionary *response = @{@"code":@"601",@"message":@"", @"responseText":jsonString};
           
            NSString *selfUrl = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@",ApplicationDelegate.userId]];
            NSString *otherUrl=[NSString new];
            if (ApplicationDelegate.otherId) {
                otherUrl = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@",ApplicationDelegate.otherId]];
            }
            NSDictionary *userData = data[@"result"];
            NSLog(@"selfUrl%@",selfUrl);
            NSLog(@"url%@",url);
            if ([url isEqualToString:selfUrl]) {
                ApplicationDelegate.SelfInfo=userData;//保存我的个人数据
                NSLog(@"SelfInfo%@",ApplicationDelegate.SelfInfo);
                NSLog(@"userData%@",userData);

            }else if([url isEqualToString:otherUrl]){
//                ApplicationDelegate.OtherInfo=userData;//保存他人数据
                //保存他人数据
                NSString *idStr=[NSString stringWithFormat:@"%@",userData[@"id"]];
                  [ApplicationDelegate.OtherInfo  setObject:userData forKey:idStr];
               
            }
            responseCallback(response);
        } failure:^(SLHttpRequestError *failure) {
            NSDictionary *response = @{@"code":@"404", @"message":@"未知错误"};
            switch (failure.httpStatusCode) {
                case 403:
                    switch (failure.slAPICode) {
                        case 4: // 未登录
                            response = @{@"code":@"403"};
                            break;
                            
                        default:
                            break;
                    }
                    break;
                    
                default:
                    break;
            }
            responseCallback(response);
        }];
        
    }];
    
}

-(NSDictionary*) httpRequestFailure:(NSInteger) code{
    NSDictionary *response = @{@"code":@"404", @"message":@"未知错误"};
    switch (code) {
        case -1009:
            response = @{@"code":@"106", @"message":@"没有网络，请检测网络连接！"};
            break;
        case -1005:
            response = @{@"code":@"107", @"message":@"网络连接超时，请稍候重试！"};
            break;
    }
    return response;
}

#pragma mark --------- 获取自己的用户信息
-(void)getUser{
    [self registerHandler:@"getUser" handler:^(id data, WVJBResponseCallback responseCallback) {
        
        NSString *uid=[[NSString alloc] init];
        if ([ApplicationDelegate.userId isKindOfClass:[NSNull class]]) {
            uid=@"";
        }else{
            uid=ApplicationDelegate.userId;
        }
        
        NSLog(@"==+++++%@",data);
        NSDictionary* user = @{@"id": uid};
        responseCallback(user);
    }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
        if (buttonIndex==1) {
            SLFirstViewController *vc = [[SLFirstViewController alloc] init];
            SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
//            UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:vc];
            [_parentController presentViewController:nav animated:YES completion:nil];
        }
}
-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
