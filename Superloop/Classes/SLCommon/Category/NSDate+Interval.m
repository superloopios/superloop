//
//  NSDate+Interval.m
//  Superloop
//
//  Created by WangJiWei on 16/3/31.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "NSDate+Interval.h"
#import "NSCalendar+Init.h"

NSString * const SLTimeIntervalDayKey = @"day";
NSString * const SLTimeIntervalHourKey = @"hour";
NSString * const SLTimeIntervalMinuteKey = @"minute";
NSString * const SLTimeIntervalSecondKey = @"second";

@implementation NSDate (Interval)
- (void)sl_timeIntervalSinceDate:(NSDate *)date day:(NSInteger *)day hour:(NSInteger *)hour minute:(NSInteger *)minute second:(NSInteger *)second
{
    // 求出self和date相差多少秒
    NSInteger interval = [self timeIntervalSinceDate:date];
    
    // 1天有多少秒
    NSInteger secondsPerDay = 24 * 60 * 60;
    // 1小时有多少秒
    NSInteger secondsPerHour = 60 * 60;
    // 1分钟有多少秒
    NSInteger secondsPerMinute = 60;
    
    *day = interval / secondsPerDay;
    *hour = (interval % secondsPerDay) / secondsPerHour;
    *minute = ((interval % secondsPerDay) % secondsPerHour) / secondsPerMinute;
    *second = ((interval % secondsPerDay) % secondsPerHour) % secondsPerMinute;
}


- (SLTimeInterval *)sl_timeIntervalSinceDate:(NSDate *)date
{
    // 求出self和date相差多少秒
    NSInteger interval = [self timeIntervalSinceDate:date];
    
    // 1天有多少秒
    NSInteger secondsPerDay = 24 * 60 * 60;
    // 1小时有多少秒
    NSInteger secondsPerHour = 60 * 60;
    // 1分钟有多少秒
    NSInteger secondsPerMinute = 60;
    
    SLTimeInterval *intervalObject = [[SLTimeInterval alloc] init];
    intervalObject.day = interval / secondsPerDay;
    intervalObject.hour = (interval % secondsPerDay) / secondsPerHour;
    intervalObject.minute = ((interval % secondsPerDay) % secondsPerHour) / secondsPerMinute;
    intervalObject.second = ((interval % secondsPerDay) % secondsPerHour) % secondsPerMinute;
    return intervalObject;
}

- (BOOL)sl_isToday
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyyMMdd";
    
    NSString *nowString = [fmt stringFromDate:[NSDate date]]; // 20151226
    NSString *selfString = [fmt stringFromDate:self]; // 20151226
    
    return [nowString isEqualToString:selfString];
}

/**
 * 判断self是否为今年
 */
- (BOOL)sl_isThisYear
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy";
    
    NSString *nowYear = [fmt stringFromDate:[NSDate date]];
    NSString *selfYear = [fmt stringFromDate:self];
    
    return [nowYear isEqualToString:selfYear];
    //    return [nowYear isEqualTo:selfYear];
    //    return [nowYear isEqual:selfYear];
}

- (BOOL)sl_isYesterday
{
    // now : 2015-12-01 12:14:57 ->  2015-12-01 00:00:00
    // self : 2015-11-30 11:11:20 -> 2015-11-30 00:00:00
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyyMMdd";
    
    // 生成只有年月日的字符串
    NSString *nowString = [fmt stringFromDate:[NSDate date]]; // 20151201
    NSString *selfString = [fmt stringFromDate:self]; // 20151130
    
    // 生成只有年月日的日期
    NSDate *nowDate = [fmt dateFromString:nowString];
    NSDate *selfDate = [fmt dateFromString:selfString];
    
    // 比较nowDate和selfDate的差值
    NSCalendar *calendar = [NSCalendar sl_calendar];
    NSCalendarUnit unit = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents *cmps = [calendar components:unit fromDate:selfDate toDate:nowDate options:0];
    
    return cmps.year == 0
    && cmps.month == 0
    && cmps.day == 1;
}

- (BOOL)sl_isTomorrow
{    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyyMMdd";
    
    // 生成只有年月日的字符串
    NSString *nowString = [fmt stringFromDate:[NSDate date]]; // 20151201
    NSString *selfString = [fmt stringFromDate:self]; // 20151130
    
    // 生成只有年月日的日期
    NSDate *nowDate = [fmt dateFromString:nowString];
    NSDate *selfDate = [fmt dateFromString:selfString];
    
    // 比较nowDate和selfDate的差值
    NSCalendar *calendar = [NSCalendar sl_calendar];
    NSCalendarUnit unit = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents *cmps = [calendar components:unit fromDate:selfDate toDate:nowDate options:0];
    
    return cmps.year == 0
    && cmps.month == 0
    && cmps.day == -1;
}

@end

@implementation SLTimeInterval
@end
