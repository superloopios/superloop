//
//  NSCalendar+Init.h
//  Superloop
//
//  Created by WangJiWei on 16/3/31.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCalendar (Init)
+ (instancetype)sl_calendar;
@end
