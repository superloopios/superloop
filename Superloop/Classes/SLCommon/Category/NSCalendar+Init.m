//
//  NSCalendar+Init.m
//  Superloop
//
//  Created by WangJiWei on 16/3/31.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "NSCalendar+Init.h"

@implementation NSCalendar (Init)
+ (instancetype)sl_calendar
{
    if ([NSCalendar respondsToSelector:@selector(calendarWithIdentifier:)]) {
        return [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    } else {
        return [NSCalendar currentCalendar];
    }
}
@end
