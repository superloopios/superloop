//
//  UIView+Frame.h
//
//  Created by Wang on 16/1/24.
//  Copyright © 2016年 Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)

// width
@property CGFloat width;
// height
@property CGFloat height;

@property CGFloat x;

@property CGFloat y;

@property CGFloat centerX;

@property CGFloat centerY;

@end
