//
//  UIImage+Image.h
//
//  Created by Wang on 16/1/24.
//  Copyright © 2016年 Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Image)
+(UIImage*) createImageWithColor:(UIColor*) color;

// 提供一个加载原始图片方法
+ (instancetype)imageNamedWithOriganlMode:(NSString *)imageName;

@end
