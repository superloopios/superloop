//
//  NSDate+Interval.h
//  Superloop
//
//  Created by WangJiWei on 16/3/31.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface SLTimeInterval : NSObject
/** 相隔多少天 */
@property (nonatomic, assign) NSInteger day;
/** 相隔多少小时 */
@property (nonatomic, assign) NSInteger hour;
/** 相隔多少分钟 */
@property (nonatomic, assign) NSInteger minute;
/** 相隔多少秒 */
@property (nonatomic, assign) NSInteger second;
@end

extern NSString * const SLTimeIntervalDayKey;
extern NSString * const SLTimeIntervalHourKey;
extern NSString * const SLTimeIntervalMinuteKey;
extern NSString * const SLTimeIntervalSecondKey;

typedef enum {
    SLTimeIntervalIndexDay = 0,
    SLTimeIntervalIndexHour = 1,
    SLTimeIntervalIndexMinute = 2,
    SLTimeIntervalIndexSecond = 3
} SLTimeIntervalIndex;

@interface NSDate (Interval)
- (void)sl_timeIntervalSinceDate:(NSDate *)date day:(NSInteger *)day hour:(NSInteger *)hour minute:(NSInteger *)minute second:(NSInteger *)second;


- (SLTimeInterval *)sl_timeIntervalSinceDate:(NSDate *)date;

- (BOOL)sl_isYesterday;
- (BOOL)sl_isToday;
- (BOOL)sl_isTomorrow;
- (BOOL)sl_isThisYear;@end
