//
//  SLJavascriptBridgeEvent.h
//  Superloop
//
//  Created by WangJiWei on 16/3/21.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SLWebView.h"
#import "WebViewJavascriptBridge.h"
#import "WKWebViewJavascriptBridge.h"

@interface SLJavascriptBridgeEvent : NSObject
-(instancetype)init:(SLWebView*)webView ParentController:(UIViewController*)parentController;
@property WKWebViewJavascriptBridge* bridge;
-(void) initBridgeEvent;
- (void)registerHandler:(NSString *)handlerName handler:(WVJBHandler)handler;
- (void)callHandler:(NSString *)handlerName data:(id)data responseCallback:(WVJBResponseCallback)responseCallback;
-(void) httpRequest;
@property(nonatomic,strong)NSDictionary *userData;
@end
