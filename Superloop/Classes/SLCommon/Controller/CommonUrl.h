//
//  CommonUrl.h
//  calculation
//
//  Created by fangyp on 15-01-17.
//  Copyright 2015年 fangyp. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kAbs(__MAINURL__, __CHILDURL__) [NSString stringWithFormat:@"%@%@", __MAINURL__, __CHILDURL__]
//#define URL_ROOT_PATIENT @"https://api.superloop.com.cn"

// 搜索用户
#define SLUserSearchUrl kAbs(URL_ROOT_PATIENT,@"/search/users")

// 添加标签
#define SLUserTagsUrl kAbs(URL_ROOT_PATIENT,@"/users/tags")

// 删除标签
#define SLUserTagsDeleteUrl kAbs(URL_ROOT_PATIENT,@"/users/tags/:id")