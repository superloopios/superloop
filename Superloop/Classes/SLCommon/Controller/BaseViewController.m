//
//  BaseViewController.m
//  playboy
//
//  Created by 张梦川 on 15/11/5.
//  Copyright © 2015年 yaoyu. All rights reserved.
//

#import "BaseViewController.h"
#import "MBProgressHUD/MBProgressHUD.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import <sys/socket.h>
#import <sys/sockio.h>
#import <sys/ioctl.h>
#import <net/if.h>
#import <arpa/inet.h>
@interface BaseViewController ()<MBProgressHUDDelegate>

@end

@implementation BaseViewController{
     MBProgressHUD   *progressHUD;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];

    [leftButton setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    leftButton.imageEdgeInsets = UIEdgeInsetsMake(0, -50, 0, 0);
    [leftButton setFrame:CGRectMake(0, 0, 50, 30)];
    [leftButton addTarget:self
                   action:@selector(goback)
         forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    self.navigationItem.leftBarButtonItem = leftItem;
}

-(NSString *)dateForMat :(NSNumber *)dateNumber{
    // 将时间戳字符串转成日期
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:dateNumber.doubleValue / 1000];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *dateStr = [fmt stringFromDate:date];
    NSDate *createdAtDate = [fmt dateFromString:dateStr];
    
//    if (createdAtDate.sl_isThisYear) { // 今年
//        if (createdAtDate.sl_isYesterday) { // 昨天
//            fmt.dateFormat = @"昨天 HH:mm";
//            return [fmt stringFromDate:createdAtDate];
//        } else if (createdAtDate.sl_isToday) { // 今天
//            NSCalendar *calendar = [NSCalendar sl_calendar];
//            NSCalendarUnit unit = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
//            NSDateComponents *cmps = [calendar components:unit fromDate:createdAtDate toDate:[NSDate date] options:0];
//            
//            if (cmps.hour >= 1) { // 时间间隔 >= 1小时
//                return [NSString stringWithFormat:@"%zd小时前", cmps.hour];
//            } else if (cmps.minute >= 1) { // 1小时 > 时间间隔 >= 1分钟
//                return [NSString stringWithFormat:@"%zd分钟前", cmps.minute];
//            } else { // 时间间隔 < 1分钟
//                return @"刚刚";
//            }
//        } else {
            fmt.dateFormat = @"yyyy-MM-dd HH:mm";
            return [fmt stringFromDate:createdAtDate];
//        }
//    } else{
//        return dateStr;
//    }
    
}



// 返回按钮
-(void)goback{
    
    [self.navigationController popViewControllerAnimated:YES];
}

//////////////////////////////netWork//////////////////////////////////////////////
//判断网路类型
- (NetworkType) getNetworkTypeFromStatusBar {
    
    UIApplication *app = [UIApplication sharedApplication];
    
    NSArray *subviews = [[[app valueForKey:@"statusBar"] valueForKey:@"foregroundView"] subviews];
    
    NSNumber *dataNetworkItemView = nil;
    for (id subview in subviews) {
        if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]])     {
            dataNetworkItemView = subview;
            break;
        }
    }
    NetworkType nettype = Network_None;
    NSNumber * num = [dataNetworkItemView valueForKey:@"dataNetworkType"];
    nettype = [num intValue];
    return nettype;
}

//////////////////////////////message//////////////////////////////////////////////

- (void)showHUDmessage:(NSString *) message {
    
    [self showHUDmessage:message withImage:nil afterDelay:0];
}

- (void) showHUDmessage: (NSString *) message withImage:(NSString *)imagePath {
    
    [self showHUDmessage:message withImage:imagePath afterDelay:0];
}

- (void) showHUDmessage: (NSString *) message afterDelay:(NSTimeInterval) delay {
    
    [self showHUDmessage:message withImage:nil afterDelay:delay];
}

- (void) showHUDmessage:(NSString *)message withImage:(NSString *)imagePath afterDelay:(NSTimeInterval) delay {
    
    progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:progressHUD];
    
    if (imagePath != nil) {
        
        progressHUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imagePath]];
    }
    else {
        
        progressHUD.customView = [[UIImageView alloc] init];
    }
    progressHUD.delegate = self;
    progressHUD.animationType = MBProgressHUDAnimationZoom;
    progressHUD.mode = MBProgressHUDModeCustomView;
    progressHUD.labelText = message;
    [progressHUD show:YES];
    if (delay == 0) {
        
        [progressHUD hide:YES afterDelay:1.40];
    }
    else {
        
        [progressHUD hide:YES afterDelay:delay];
    }
}

//网络请求开始使用的等待消息
- (void) startHUDmessage:(NSString *) message {
    
    progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:progressHUD];
    progressHUD.animationType = MBProgressHUDAnimationZoom;
    progressHUD.delegate = self;
    progressHUD.labelText = message;
    [progressHUD show:YES];
}

- (void) stopHUDmessage {
    
    [progressHUD hide:YES];
}

- (void) stopHUDmessageByAfterDelay:(NSTimeInterval) delay {
    
    [progressHUD hide:YES afterDelay:delay];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    
    [progressHUD removeFromSuperview];
    progressHUD = nil;
}


//电话格式检查
- (BOOL)isPhoneNumberString:(NSString *)number {
    NSRegularExpression *regularexpression = [[NSRegularExpression alloc]
                                              initWithPattern:@"^1[3|4|5|7|8]\\d{9}$"
                                              options:NSRegularExpressionCaseInsensitive
                                              error:nil];
    NSUInteger numberofMatch = [regularexpression numberOfMatchesInString:number
                                                                  options:NSMatchingReportProgress
                                                                    range:NSMakeRange(0,number.length)];
    
    if(numberofMatch > 0) {
        
        return YES;
    }
    return NO;
    
}

//mail地址格式检查
- (BOOL)isEmailString:(NSString *) email {
    
    NSRegularExpression *regularexpression = [[NSRegularExpression alloc]
                                              initWithPattern:@"\\b([a-zA-Z0-9%_.+\\-]+)@([a-zA-Z0-9.\\-]+?\\.[a-zA-Z]{2,6})\\b"
                                              options:NSRegularExpressionCaseInsensitive
                                              error:nil];
    NSUInteger numberofMatch = [regularexpression numberOfMatchesInString:email
                                                                  options:NSMatchingReportProgress
                                                                    range:NSMakeRange(0,email.length)];
    
    if(numberofMatch > 0) {
        
        return YES;
    }
    return NO;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
    
    const int movementDistance = 50; // tweak as needed
    
    const float movementDuration = 0.3f; // tweak as needed
    
    
    int movement = (up ? -movementDistance : movementDistance);
    
    
    [UIView beginAnimations: @"anim" context: nil];
    
    [UIView setAnimationBeginsFromCurrentState: YES];
    
    [UIView setAnimationDuration: movementDuration];
    
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    
    [UIView commitAnimations];
    
}
- (void) animateTextView: (UITextView*) textField up: (BOOL) up

{
    
    const int movementDistance = 120+30; // tweak as needed
    
    const float movementDuration = 0.3f; // tweak as needed
    
    
    int movement = (up ? -movementDistance : movementDistance);
    
    
    [UIView beginAnimations: @"anim" context: nil];
    
    [UIView setAnimationBeginsFromCurrentState: YES];
    
    [UIView setAnimationDuration: movementDuration];
    
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    
    [UIView commitAnimations];
    
}

- (NSString *) md5:(NSString *)string {
    
    if (string == nil) {
        return nil;
    }
    const char *cstr = [string UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cstr, strlen(cstr), result);
    
    return [NSString stringWithFormat: @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1],
            result[2], result[3],
            result[4], result[5],
            result[6], result[7],
            result[8], result[9],
            result[10], result[11],
            result[12], result[13],
            result[14], result[15]];
}
- (void) alertMessage:(NSString *)str{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示消息" message:str preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"忽略" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"前往" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [_delegate doclick];
    }];
    
    [alert addAction:cancel];
    [alert addAction:confirm];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) alert:(NSString *)str{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示消息" message:str preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (BOOL)isEmpty:(NSString *)str{
    if (str == nil) {
        
        return YES;
    }
    
    if ([str isKindOfClass:[NSString class]] || ([str isKindOfClass:[NSNumber class]])) {
        
        NSString *temp = [NSString stringWithFormat:@"%@",str];
        
        if ([temp length] == 0 || [[temp stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
            
            return YES;
        }
    }
    return NO;

}

#pragma mark ----- 获取设备的IP地址
- (NSString *)getDeviceIPIpAddresses{
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    
    if (sockfd < 0) return nil;
    
    NSMutableArray *ips = [NSMutableArray array];
    
    int BUFFERSIZE = 4096;
    
    struct ifconf ifc;
    
    char buffer[BUFFERSIZE], *ptr, lastname[IFNAMSIZ], *cptr;
    
    struct ifreq *ifr, ifrcopy;
    
    ifc.ifc_len = BUFFERSIZE;
    
    ifc.ifc_buf = buffer;
    
    if (ioctl(sockfd, SIOCGIFCONF, &ifc) >= 0){
        
        for (ptr = buffer; ptr < buffer + ifc.ifc_len; ){
            
            ifr = (struct ifreq *)ptr;
            
            int len = sizeof(struct sockaddr);
            
            if (ifr->ifr_addr.sa_len > len) {
                
                len = ifr->ifr_addr.sa_len;
                
            }
            
            ptr += sizeof(ifr->ifr_name) + len;
            
            if (ifr->ifr_addr.sa_family != AF_INET) continue;
            
            if ((cptr = (char *)strchr(ifr->ifr_name, ':')) != NULL) *cptr = 0;
            
            if (strncmp(lastname, ifr->ifr_name, IFNAMSIZ) == 0) continue;
            
            memcpy(lastname, ifr->ifr_name, IFNAMSIZ);
            
            ifrcopy = *ifr;
            
            ioctl(sockfd, SIOCGIFFLAGS, &ifrcopy);
            
            if ((ifrcopy.ifr_flags & IFF_UP) == 0) continue;
            
            
            
            NSString *ip = [NSString  stringWithFormat:@"%s", inet_ntoa(((struct sockaddr_in *)&ifr->ifr_addr)->sin_addr)];
            
            [ips addObject:ip];
            
        }
        
    }
    
    close(sockfd);
    
    
    NSString *deviceIP = @"";
    
    for (int i=0; i < ips.count; i++)
        
    {
        
        if (ips.count > 0)
            
        {
            
            deviceIP = [NSString stringWithFormat:@"%@",ips.lastObject];
            
            
            
        }
        
    }
    
    return deviceIP;
    

}



@end
