//
//  SLShareModel.h
//  Superloop
//
//  Created by WangS on 16/8/3.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SLShareModel : NSObject

@property (nonatomic,strong) UIImage *shareImage;//分享图片
@property (nonatomic,copy) NSString *shareTitle;//分享标题
@property (nonatomic,copy) NSString *shareContent;//分享内容
@property (nonatomic,copy) NSString *shareUrl;//分享链接
//@property (nonatomic,copy) NSString *shareImageUrl;//分享图片url

@end
