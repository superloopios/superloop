//
//  SLShareView.h
//  Superloop
//
//  Created by WangS on 16/8/3.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLShareView;
@class SLShareModel;
@interface SLShareView : UIView
-(void)showViewWithModel:(SLShareModel *)shareModel viewController:(UIViewController *)currentViewController;
@end
