//
//  SLActionView.m
//  Superloop
//
//  Created by administrator on 16/7/19.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLActionView.h"

@interface SLActionView ()

@property(nonatomic, weak)UIView *titleView;

@end

@implementation SLActionView


- (UIView *)titleView{
    if (!_titleView) {
        UIView *titleView = [[UIView alloc] init];
        [self addSubview:titleView];
        _titleView = titleView;
    }
    return _titleView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
        
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
        [self addGestureRecognizer:ges];
    }
    
    return self;
}
- (void)setTitleArr:(NSArray *)titleArr{
    NSArray *views = [_titleView subviews];
    for (UIView *v in views) {
        [v removeFromSuperview];
    }
    _titleArr = titleArr;
    
    self.titleView.frame = CGRectMake(0, screen_H, screen_W, (50*titleArr.count + 6));
    self.titleView.backgroundColor = SLColor(224, 224, 224);
    
    for (int i = 0; i<titleArr.count; i++) {
        UIButton *btn = [[UIButton alloc] init];
        if (i == 0) {
            btn.frame = CGRectMake(0, self.titleView.height - (i+1)*50, screen_W, 50);
        }else{
            btn.frame = CGRectMake(0, self.titleView.height - (i+1)*50.5-5, screen_W, 50);
        }
        btn.tag = i;
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBackgroundColor:[UIColor whiteColor]];
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:15];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.titleView addSubview:btn];
    }
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        self.titleView.frame = CGRectMake(0, screen_H - (50*titleArr.count + 6), screen_W, (50*titleArr.count + 6));
    }];
}
- (void)dismiss{
    [UIView animateWithDuration:0.25 animations:^{
        self.titleView.frame = CGRectMake(0, screen_H , screen_W, (50*self.titleArr.count + 6));
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
}
- (void)btnClick:(UIButton *)btn{
    if ([_delegate respondsToSelector:@selector(actionSheetButtonClickedAtIndex:)]) {
        [_delegate actionSheetButtonClickedAtIndex:btn.tag];
    }
}

@end
