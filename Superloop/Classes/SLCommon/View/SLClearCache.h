//
//  SLClearCache.h
//  Superloop
//
//  Created by WangS on 16/8/18.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLClearCache : NSObject
+(void)clearCache;
@end
