//
//  SLCalculateSpace.m
//  Superloop
//
//  Created by WangS on 16/11/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCalculateSpace.h"

@implementation SLCalculateSpace

//设置行间距和字间距
+ (void)setTextViewSpace:(UITextView *)textView withValue:(NSString*)str withFont:(UIFont*)font lineSpace:(CGFloat)lineSpace wordSpace:(NSNumber *)wordSpace {
    if (str) {
        NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
        paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
        paraStyle.alignment = NSTextAlignmentLeft;
        paraStyle.lineSpacing = lineSpace; //设置行间距
        paraStyle.hyphenationFactor = 1.0;
        paraStyle.firstLineHeadIndent = 0.0;
        paraStyle.paragraphSpacingBefore = 0.0;
        paraStyle.headIndent = 0;
        paraStyle.tailIndent = 0;
        //设置字间距 NSKernAttributeName:@1.5f
        NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:wordSpace
                              };
        NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
        textView.attributedText = attributeStr;
    }
}
+ (void)setLabelSpace:(UILabel *)label withValue:(NSString*)str withFont:(UIFont*)font lineSpace:(CGFloat)lineSpace wordSpace:(NSNumber *)wordSpace{
    if (str) {
        NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
        paraStyle.lineBreakMode = NSLineBreakByTruncatingTail;
        paraStyle.alignment = NSTextAlignmentLeft;
        paraStyle.lineSpacing = lineSpace; //设置行间距
        paraStyle.hyphenationFactor = 1.0;
        paraStyle.firstLineHeadIndent = 3.0;
        paraStyle.paragraphSpacingBefore = 0.0;
        paraStyle.headIndent = 3;
        paraStyle.tailIndent = 0;
        //设置字间距 NSKernAttributeName:@1.5f
        NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:wordSpace
                              };
        NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
        label.attributedText = attributeStr;
    }
}
//计算高度(带有行间距的情况)
+ (CGFloat)getSpaceHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width lineSpace:(CGFloat)lineSpace wordSpace:(NSNumber *)wordSpace {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = lineSpace;
    
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:wordSpace
                          };
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return size.height;
}



@end
