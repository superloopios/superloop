//
//  SLLabel.m
//  Superloop
//
//  Created by xiaowu on 16/10/31.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLLabel.h"

@implementation SLLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)setText:(NSString *)text{
    NSString *utfStr = [text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *rmIllegitUtfStr = [utfStr stringByReplacingOccurrencesOfString:@"%E2%80%8D" withString:@""];
    super.text = [rmIllegitUtfStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}




@end
