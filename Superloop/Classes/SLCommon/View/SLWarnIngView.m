//
//  SLWarnIngView.m
//  Superloop
//
//  Created by xiaowu on 16/11/20.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLWarnIngView.h"

@interface SLWarnIngView ()

@property(nonatomic, strong)UIImageView *imgV;

@end

@implementation SLWarnIngView

- (UIImageView *)imgV{
    if (!_imgV) {
        _imgV = [[UIImageView alloc] init];
        _imgV.contentMode = UIViewContentModeCenter;
        [self addSubview:_imgV];
        [_imgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self);
            make.width.height.equalTo(@55);
        }];
    }
    return _imgV;
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.width = 125;
        self.height = 110;
        self.backgroundColor = [UIColor colorWithRed:239.0/255 green:28.0/255 blue:0 alpha:0.7];
//        self.alpha = 0.7;
        self.layer.cornerRadius = 10;
        self.imgV.image = [UIImage imageNamed:@"warnIng"];
    }
    return self;
}
- (void)setWarningStr:(NSString *)warningStr{
    _warningStr = warningStr;
    CGSize selfSize = [warningStr boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]} context:nil].size;
    self.height = selfSize.height*3;
    self.width = selfSize.width;
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self removeFromSuperview];
//    });
}

@end
