//
//  SLContactDatas.h
//  Superloop
//
//  Created by WangS on 16/8/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLContactDatas : NSObject

+ (instancetype)instancetypeSLContactDatas;
- (void)getContact;

@end
