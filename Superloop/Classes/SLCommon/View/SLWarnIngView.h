//
//  SLWarnIngView.h
//  Superloop
//
//  Created by xiaowu on 16/11/20.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLWarnIngView : UIView

@property (nonatomic, copy)NSString *warningStr;

@end
