//
//  SLIMCommon.m
//  Superloop
//
//  Created by WangS on 16/8/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLIMCommon.h"
#import "SLOpenIMCommon.h"
#import "AppDelegate.h"
#import "SLMessageManger.h"
#import "SPUtil.h"
#import "SLAPIHelper.h"
#import <Bugly/Bugly.h>

@implementation SLIMCommon

+ (void)Login:(NSString *)account password:(NSString *)password{
    __weak typeof(self) weakSelf = self;
    [SLOpenIMCommon login:[NSString stringWithFormat:@"%@",ApplicationDelegate.userId] account:account password:password successBlock:^(){
        [[SPUtil sharedInstance] setWaitingIndicatorShown:NO withKey:weakSelf.description];
    } failedBlock:^(NSError *aError){

    }];
}
@end
