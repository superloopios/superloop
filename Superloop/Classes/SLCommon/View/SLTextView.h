//
//  SLTextView.h
//  Superloop
//
//  Created by WangJiwei on 16/4/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLTextView : UITextView
@property (nonatomic , copy) NSString *placeholder;
@property (nonatomic , strong) UIColor *placeholderColor;


@end
