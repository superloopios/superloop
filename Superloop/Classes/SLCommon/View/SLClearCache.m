//
//  SLClearCache.m
//  Superloop
//
//  Created by WangS on 16/8/18.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLClearCache.h"

@implementation SLClearCache
+(void)clearCache{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSDictionary* dict = [defs dictionaryRepresentation];
    for(id key in dict) {
        if (![key isEqualToString:appIsFirst] && ![key isEqualToString:strAccountName] &&![key isEqualToString:strAccout] && ![key isEqualToString:@"VoiceType"]&& ![key isEqualToString:location]&&![key isEqualToString:@"guideSave"]) {
            [defs removeObjectForKey:key];
        }
    }
}
@end
