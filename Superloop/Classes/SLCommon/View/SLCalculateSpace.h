//
//  SLCalculateSpace.h
//  Superloop
//
//  Created by WangS on 16/11/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SLCalculateSpace : NSObject
//设置textView行间距和字间距
+ (void)setTextViewSpace:(UITextView *)textView withValue:(NSString*)str withFont:(UIFont*)font lineSpace:(CGFloat)lineSpace wordSpace:(NSNumber *)wordSpace;
//设置label行间距和字间距
+ (void)setLabelSpace:(UILabel *)label withValue:(NSString*)str withFont:(UIFont*)font lineSpace:(CGFloat)lineSpace wordSpace:(NSNumber *)wordSpace;
//计算高度(带有行间距的情况)
+ (CGFloat)getSpaceHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width lineSpace:(CGFloat)lineSpace wordSpace:(NSNumber *)wordSpace;
@end
