//
//  SLWebView.h
//  Superloop
//
//  Created by Daniel Zhao on 16/3/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface SLWebView : WKWebView

-(instancetype)initWithFrame:(CGRect)frame;
- (void)loadPage:(NSString *) page;


@end
