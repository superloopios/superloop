//
//  SLWebViewController.m
//  Superloop
//
//  Created by Daniel Zhao on 16/3/20.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLWebViewController.h"
#import "WebViewJavascriptBridge.h"
#import "SLWebView.h"
#import "SLJavascriptBridgeEvent.h"
#import <Masonry.h>
#import "AppDelegate.h"
#import "SLValueUtils.h"
#import "UMSocial.h"
#import <UIImageView+WebCache.h>
#import "SLFirstViewController.h"
#import "SLShareView.h"
#import "SLShareModel.h"
#import "SLNavgationViewController.h"
#import "SLHttpRequest.h"
#import "SLAPIHelper.h"
#import "SLTabBarViewController.h"
#import "AppDelegate.h"
#import "SLNavgationViewController.h"

@interface SLWebViewController()<WKNavigationDelegate,UMSocialUIDelegate>
@property (nonatomic, strong) SLWebView *webView;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) SLJavascriptBridgeEvent* bridgeEvent;
@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UIImageView *avatarImageVw;
@property (nonatomic, strong) NSString *urlStr;
@property (nonatomic, strong) UIButton *shareBtn;
@property (nonatomic, strong) NSDictionary *shareData;//分享数据
@property (nonatomic, strong) UIButton *popBackBtn;
@end
@implementation SLWebViewController
-(NSDictionary *)shareData{
    if (!_shareData) {
        _shareData=[NSDictionary new];
    }
    return _shareData;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLWebViewController"];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLWebViewController"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    SLWebView *webView = [[SLWebView alloc] init];
    webView.frame=CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-64);
    webView.navigationDelegate=self;
    self.webView=webView;
    _bridgeEvent = [[SLJavascriptBridgeEvent alloc] init:webView ParentController:self];
    [self.view addSubview:webView];
    [self setNav];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    
    // 进度条
    [webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:NULL];
    // title
    [webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];
    
//    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow withText:@""];

    [self initBridgeEvent];
}

- (void)setNav{
    
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    UIView *hView=[[UIView alloc] initWithFrame:CGRectMake(0, navView.frame.size.height-0.5, ScreenW, 0.5)];
    hView.backgroundColor=[UIColor lightGrayColor];
    [navView addSubview:hView];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 27,40, 30);
    [backBtn setTitle:@" " forState:UIControlStateNormal];
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backClickBtn) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];

    UIButton *popBackBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    popBackBtn.frame=CGRectMake(40, 27, 30, 30);
    popBackBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [popBackBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [popBackBtn setImage:[UIImage imageNamed:@"login_close"] forState:UIControlStateNormal];
    [popBackBtn setTitle:@" " forState:UIControlStateNormal];
    [popBackBtn addTarget:self action:@selector(popBackBtnClick) forControlEvents:UIControlEventTouchUpInside];
    popBackBtn.hidden=YES;
    self.popBackBtn=popBackBtn;
    [navView addSubview:popBackBtn];
    
    UILabel *titleLab=[[UILabel alloc] initWithFrame:CGRectMake(70, 32, ScreenW-140, 20)];
    titleLab.text=@"正在加载...";
    self.titleLab=titleLab;
    titleLab.font=[UIFont systemFontOfSize:17];
    titleLab.textAlignment=NSTextAlignmentCenter;
    titleLab.textColor=[UIColor blackColor];
    [navView addSubview:titleLab];
    
    self.progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, navView.height - 2, ScreenW, 0)];
    self.progressView.progressTintColor = UIColorFromRGB(0xff5a5f);
    [navView addSubview:self.progressView];
//    NSLog(@"navViewHeight:%f,progressY:%f,progressHeight:%f", navView.height, self.progressView.frame.origin.y,self.progressView.height);
    
    if (self.fromBanner) {
        UIButton *shareBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        shareBtn.frame=CGRectMake(ScreenW-40, 27, 30, 30);
        shareBtn.hidden=YES;
        self.shareBtn=shareBtn;
        [shareBtn setImage:[UIImage imageNamed:@"Share"] forState:UIControlStateNormal];
        [shareBtn addTarget:self action:@selector(shareWebView) forControlEvents:UIControlEventTouchUpInside];
        [navView addSubview:shareBtn];
    }
}

- (void) initBridgeEvent{
    [self httpRequest];
    [self sharePage];
    [self triggerShare];//触发分享
    [self setUpAlert];//弹框
}
- (void)triggerShare{
    __weak typeof(self) weakSelf = self;
    [_bridgeEvent registerHandler:@"triggerShare" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf sharePage];
        [weakSelf shareWebView];
    }];
}
#pragma mark - httpRequest
- (void)httpRequest{
    
    [_bridgeEvent registerHandler:@"httpRequest" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSString *type = data[@"type"];
        if(type == nil)
        {
            type = @"GET";
            return;
        }
        NSString *url = data[@"url"];
        if(url == nil)return;
        NSDictionary *params = data[@"params"];
        NSDictionary *dataParams = data[@"data"];
        NSLog(@"%@", dataParams);
        SLHttpMethod method = Get;
        if ([type isEqualToString:@"GET"]) {
            method = Get;
        }else if([type isEqualToString:@"POST"]) {
            method = Post;
            params = dataParams;
        }else if([type isEqualToString:@"PUT"]) {
            method = Put;
            params = dataParams;
        }else if([type isEqualToString:@"DELETE"]) {
            method = Delete;
            params = dataParams;
        }
        
        [SLAPIHelper requestWithUrl:url headers:nil params:params method:method success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
           
            if(data == nil){
                return;
            }
            
            NSError * err;
            NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:data options:0 error:&err];
            if(err){
                return;
            }
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSDictionary *response = @{@"code":@"601",@"message":@"", @"responseText":jsonString};
            /*
            NSString *selfUrl = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@",ApplicationDelegate.userId]];
            NSString *otherUrl=[NSString new];
            if (ApplicationDelegate.otherId) {
                otherUrl = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@",ApplicationDelegate.otherId]];
            }
             
            NSDictionary *userData = data[@"result"];
            NSLog(@"selfUrl%@",selfUrl);
            NSLog(@"url%@",url);
            if ([url isEqualToString:selfUrl]) {
                NSLog(@"SelfInfo%@",ApplicationDelegate.SelfInfo);
                NSLog(@"userData%@",userData);
                
            }else if([url isEqualToString:otherUrl]){
                //                ApplicationDelegate.OtherInfo=userData;//保存他人数据
                //保存他人数据
                
                
            }
             */
            responseCallback(response);
        } failure:^(SLHttpRequestError *failure) {
            NSDictionary *response = @{@"code":[NSString stringWithFormat:@"%ld",failure.slAPICode]};
            responseCallback(response);
        }];
        
    }];
    
}
//设置弹窗
- (void)setUpAlert{
    __weak typeof(self) weakSelf = self;
    [_bridgeEvent registerHandler:@"alert" handler:^(id data, WVJBResponseCallback responseCallback) {
        
        NSString *alertContent=data[@"content"];
        NSString *alertDelay = data[@"delay"];
        if (!alertDelay) {
            alertDelay = @"3";
        }
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:weakSelf.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(alertContent, @"HUD message title");
        
        float floatString = [alertDelay floatValue];
        hud.userInteractionEnabled = NO;
        [hud hide:YES afterDelay:floatString];
    }];
}

#pragma mark - banner分享
- (void)sharePage{
    __weak typeof(self) weakSelf=self;
    [_bridgeEvent registerHandler:@"SharePage" handler:^(id data, WVJBResponseCallback responseCallback) {
        weakSelf.shareData=data;
        if (weakSelf.shareData.count>0) {
            if (weakSelf.fromBanner) {
                weakSelf.shareBtn.hidden=NO;
            }
            NSString *imageUrl = weakSelf.shareData[@"image"];
            NSURL *url = [NSURL URLWithString:imageUrl];
            UIImageView *imgVw= [[UIImageView alloc] init];
            imgVw.frame = CGRectMake(-200, -200, 50, 50);
            [weakSelf.view addSubview:imgVw];
            [imgVw sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
            weakSelf.avatarImageVw = imgVw;
        }
    }];
}
- (void)shareWebView{
    SLShareModel *shareModel=[[SLShareModel alloc] init];
    shareModel.shareTitle=self.shareData[@"title"];
    shareModel.shareContent=[NSString stringWithFormat:@"%@",[SLValueUtils stringFromObject:self.shareData[@"content"]]];
    shareModel.shareUrl=[NSString stringWithFormat:@"%@",[NSURL URLWithString:self.shareData[@"url"]]];
    shareModel.shareImage=self.avatarImageVw.image;
    
    SLShareView *shareView = [[SLShareView alloc] init];
    shareView.frame = CGRectMake(0, 0, ScreenW, ScreenH+190);
    [shareView showViewWithModel:shareModel viewController:self];
    [[UIApplication sharedApplication].keyWindow addSubview:shareView];

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        SLFirstViewController *vc = [[SLFirstViewController alloc] init];
        SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
        [self presentViewController:nav animated:YES completion:nil];
    }
}
-(void)backClickBtn{
    if (self.isFromSplash) {//启动图
        if (ApplicationDelegate.userId) {
            SLTabBarViewController *indexVC = [[SLTabBarViewController alloc]init];
            [UIApplication sharedApplication].keyWindow.rootViewController = indexVC;
        }else{
            SLFirstViewController *loginVc = [[SLFirstViewController alloc] init];
            SLNavgationViewController *nav = [[SLNavgationViewController alloc] initWithRootViewController:loginVc];
            [UIApplication sharedApplication].keyWindow.rootViewController = nav;
        }
        
        return;
    }
    if ([self.webView canGoBack]) {
        self.popBackBtn.hidden=NO;
        [self.webView goBack];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)popBackBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if(object == self.webView){
        if ([keyPath isEqualToString:@"estimatedProgress"]) {
            //不要让进度条倒着走...有时候goback会出现这种情况
            if ([change[@"new"] floatValue] < [change[@"old"] floatValue]) {
                return;
            }
            
            [self.progressView setAlpha:1.0f];
            
            [self.progressView setProgress:self.webView.estimatedProgress animated:YES];
            
            if (self.webView.estimatedProgress == 1.0f) {
                [UIView animateWithDuration:0.4 delay:0.4 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    [self.progressView setAlpha:0.0f];
                } completion:^(BOOL finished) {
                    [self.progressView setProgress:0.0f animated:NO];
                }];
            }
        }
        else if ([keyPath isEqualToString:@"title"]) {
            self.titleLab.text = self.webView.title;
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
//    [HUDManager hideHUDView];

    
}
- (void)dealloc{
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
    [self.webView removeObserver:self forKeyPath:@"title"];
    //    NSLog(@"dealloc");
}

@end
