//
//  SLShareView.m
//  Superloop
//
//  Created by WangS on 16/8/3.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLShareView.h"
#import "SLShareBtn.h"
#import "UMSocial.h"
#import "SLShareModel.h"
@interface SLShareView ()
@property (nonatomic,strong) SLShareModel *shareModel;
@property (nonatomic,strong) UIViewController *currentViewController;
@property (nonatomic,strong) UIView *shareView;
@end
@implementation SLShareView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        [self initShareView];
    }
    return self;
}
-(void)initShareView{

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] init];
    [singleTap addTarget:self action:@selector(cancelBtnClick)];
    [self addGestureRecognizer:singleTap];

    UIView *shareView=[[UIView alloc] initWithFrame:CGRectMake(0, ScreenH, ScreenW, 190)];
    shareView.backgroundColor=[UIColor colorWithRed:232/255.0 green:232/255.0 blue:232/255.0 alpha:1];
    self.shareView = shareView;
    [self addSubview:shareView];
    
    UILabel *topLab=[[UILabel alloc] initWithFrame:CGRectMake(0, 5 , ScreenW, 20)];
    topLab.text=@"分享到";
    topLab.font=[UIFont systemFontOfSize:13];
    topLab.textColor=[UIColor blackColor];
    topLab.textAlignment=NSTextAlignmentCenter;
    [shareView addSubview:topLab];
    
    NSArray *titleArray=@[@"朋友圈",@"微信好友",@"微博"];
    NSArray *imgArray=@[@"timeline",@"session",@"sina"];
    for (int i=0; i<titleArray.count; i++) {
        SLShareBtn *btn=[SLShareBtn buttonWithType:UIButtonTypeCustom];
        btn.userInteractionEnabled=YES;
        btn.frame=CGRectMake((ScreenW-65*3)/4*(i+1)+65*i, 45, 65, 65);
        btn.tag=100+i;
        btn.imgView.image=[UIImage imageNamed:imgArray[i]];
        btn.titleLab.text=titleArray[i];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [shareView addSubview:btn];
    }
  
    UIButton *bottomBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    bottomBtn.frame=CGRectMake(0, 140, ScreenW, 50);
    bottomBtn.backgroundColor=[UIColor whiteColor];
    bottomBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [bottomBtn setTitle:@"取消分享" forState:UIControlStateNormal];
    [bottomBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [bottomBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:bottomBtn];
 
    
    [UIView animateWithDuration:0.3 animations:^{
        self.shareView.frame = CGRectMake(0, ScreenH-190, ScreenW, 190);
    }];
  
}
-(void)btnClick:(SLShareBtn *)btn{
    NSInteger index=btn.tag-100;
        switch (index) {
        case 0:
            [self shareWXTimeline];
            break;
        case 1:
            [self shareWXSession];
            break;
        case 2:
            [self shareSina];
            break;
        default:
            [self cancelBtnClick];
            break;
    }
}
-(void)showViewWithModel:(SLShareModel *)shareModel viewController:(UIViewController *)currentViewController{
    self.shareModel=shareModel;
    self.currentViewController=currentViewController;
}
-(void)cancelBtnClick{

    [UIView animateWithDuration:0.3 animations:^{
        self.shareView.frame = CGRectMake(0, ScreenH, ScreenW, 190);
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self removeFromSuperview];
    });
}
//处理分享内容过长的问题
-(NSString *)changeShareContent{
    NSString *strContent = [NSString stringWithFormat:@"%@",self.shareModel.shareContent];
    if (strContent.length >64) {
        strContent = [strContent substringToIndex:64];
    }
    NSString *str = [NSString stringWithFormat:@"%@%@",strContent,self.shareModel.shareUrl];
    return str;
}

-(void)shareWXTimeline{
    [self cancelBtnClick];
    [UMSocialData defaultData].extConfig.title = self.shareModel.shareTitle;
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = self.shareModel.shareUrl;
    UMSocialUrlResource *urlResource = [[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:
                                        nil];
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatTimeline] content:[self changeShareContent] image:self.shareModel.shareImage location:nil urlResource:urlResource presentedController:self.currentViewController completion:^(UMSocialResponseEntity *shareResponse){
        if (shareResponse.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");
        }
    }];
}
-(void)shareWXSession{
    [self cancelBtnClick];
    [UMSocialData defaultData].extConfig.title = self.shareModel.shareTitle;
    [UMSocialData defaultData].extConfig.wechatSessionData.url = self.shareModel.shareUrl;
    UMSocialUrlResource *urlResource = [[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:
                                        nil];
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatSession] content:[self changeShareContent] image:self.shareModel.shareImage location:nil urlResource:urlResource presentedController:self.currentViewController completion:^(UMSocialResponseEntity *shareResponse){
        if (shareResponse.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");

        }
    }];
}
-(void)shareSina{
    [self cancelBtnClick];
    [UMSocialData defaultData].extConfig.title = self.shareModel.shareTitle;
    UMSocialUrlResource *urlResource = [[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:
                                        nil];
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToSina] content:[self changeShareContent] image:self.shareModel.shareImage location:nil urlResource:urlResource presentedController:self.currentViewController completion:^(UMSocialResponseEntity *shareResponse){
        if (shareResponse.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");

        }
    }];

}

@end
