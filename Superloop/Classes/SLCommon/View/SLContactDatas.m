//
//  SLContactDatas.m
//  Superloop
//
//  Created by WangS on 16/8/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLContactDatas.h"
#import "AppDelegate.h"
#import "SLAPIHelper.h"
#import "ChineseString.h"

@implementation SLContactDatas

+ (instancetype)instancetypeSLContactDatas{
    static SLContactDatas *instance=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[SLContactDatas alloc] init];
    });
    return instance;
}
-(void)getContact{
    //异步加载通讯录数据
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (ApplicationDelegate.Basic) {
            [self getContactDatas];
        }
    });
}
-(void)getContactDatas{
    NSDictionary *parameters = @{@"id":[NSString stringWithFormat:@"%@", ApplicationDelegate.userId],@"pager":@"1000"};
    [SLAPIHelper userFollows:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSArray *array = data[@"result"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSMutableArray *sectionArr = [ChineseString IndexArray:array];
            NSMutableArray *dataArr = [ChineseString LetterSortArray:array];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *dict=@{@"section":sectionArr,@"data":dataArr};
                NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:dict];
                [SLUserDefault setAddressData:configData];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"getContactSuccess" object:nil];
            });
        });
        
    } failure:^(SLHttpRequestError *error) {
        
    }];
}

@end
