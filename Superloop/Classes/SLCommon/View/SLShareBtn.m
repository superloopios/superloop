//
//  SLShareBtn.m
//  Superloop
//
//  Created by WangS on 16/8/3.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLShareBtn.h"

@implementation SLShareBtn

-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        
        _imgView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 45, 45)];
        [self addSubview:_imgView];
        
        _titleLab=[[UILabel alloc] initWithFrame:CGRectMake(0, 45, 65, 20)];
        _titleLab.textAlignment=NSTextAlignmentCenter;
        _titleLab.textColor=[UIColor blackColor];
        _titleLab.font=[UIFont systemFontOfSize:13];
        [self addSubview:_titleLab];
    }
    return self;
}

@end
