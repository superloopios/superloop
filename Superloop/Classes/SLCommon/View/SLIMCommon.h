//
//  SLIMCommon.h
//  Superloop
//
//  Created by WangS on 16/8/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SLIMCommon : NSObject
+ (void)Login:(NSString *)account password:(NSString *)password;
@end
