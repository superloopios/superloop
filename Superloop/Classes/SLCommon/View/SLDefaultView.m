//
//  SLDefaultView.m
//  Superloop
//
//  Created by xiaowu on 16/11/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLDefaultView.h"

@implementation SLDefaultView{
    UILabel *tiplabel;
    UILabel *bolangLabel;
    UIImageView *imgView;
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        tiplabel = [[UILabel alloc]init];
        self.backgroundColor = SLColor(245, 245, 245);
        tiplabel.font = [UIFont systemFontOfSize:17];
        tiplabel.textAlignment = NSTextAlignmentCenter;
        tiplabel.frame = CGRectMake(0, 150*picWScale, screen_W, 17);
        [self addSubview:tiplabel];
        imgView = [[UIImageView alloc] init];
        imgView.image = [UIImage imageNamed:@"swan"];
        imgView.frame = CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 30, 192, 192);
        [self addSubview:imgView];
    }
    return self;
}
- (void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
    tiplabel.text = titleStr;
}

@end
