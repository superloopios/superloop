//
//  SLWebViewController.h
//  Superloop
//
//  Created by Daniel Zhao on 16/3/20.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLWebViewController : UIViewController
@property (nonatomic, strong) NSString *url;
@property (nonatomic, assign) BOOL fromBanner;
@property (nonatomic, assign) BOOL isFromSplash;
@end
