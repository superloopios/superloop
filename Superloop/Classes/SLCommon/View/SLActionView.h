//
//  SLActionView.h
//  Superloop
//
//  Created by administrator on 16/7/19.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SLActionViewDelegate <NSObject>

@optional
- (void)actionSheetButtonClickedAtIndex:(NSInteger)index;
@end

@interface SLActionView : UIView

@property (nonatomic , strong) NSArray *titleArr;
@property (nonatomic, weak) id<SLActionViewDelegate>delegate;
- (void)dismiss;

@end
