//
//  SLWebView.m
//  Superloop
//
//  Created by Daniel Zhao on 16/3/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLWebView.h"
#import "SLWebViewController.h"


@interface SLWebView ()<WKNavigationDelegate>

@end

@implementation SLWebView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self.layer.backgroundColor = [UIColor whiteColor].CGColor;
   
    return self;
}
- (void)loadPage:(NSString *) page {
    // mainBundle path
    NSString *mainBundlePath = [[NSBundle mainBundle] bundlePath];
    
    // html base目录
    NSString *basePath = [NSString stringWithFormat:@"%@/h5", mainBundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:basePath isDirectory:YES];
    //    NSLog(@"baseURL:%@", baseURL);
    NSString *htmlPath = [NSString stringWithFormat:@"%@/%@", basePath, page];
    //    NSLog(@"htmlPath:%@", htmlPath);
    NSString *htmlString = [NSString stringWithContentsOfFile:htmlPath encoding:NSUTF8StringEncoding error:nil];
    [self loadHTMLString:htmlString baseURL:baseURL];
    
    
}



@end
