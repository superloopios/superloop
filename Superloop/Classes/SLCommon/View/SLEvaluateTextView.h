//
//  SLEvaluateTextView.h
//  Superloop
//
//  Created by WangS on 16/10/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLEvaluateTextView : UITextView
@property (nonatomic , copy) NSString *placeholder;
@property (nonatomic , strong) UIColor *placeholderColor;
@end
