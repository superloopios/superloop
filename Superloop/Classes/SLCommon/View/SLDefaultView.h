//
//  SLDefaultView.h
//  Superloop
//
//  Created by xiaowu on 16/11/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLDefaultView : UIView

@property(nonatomic, copy)NSString *titleStr;

@end
