//
//  Utils.h
//  PayFeeSystem
//
//  Created by Mac on 15/7/16.
//  Copyright (c) 2015年 Cp.Li. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIAlertView;


@interface Utils : NSObject

+(instancetype)sharedInstance;

+(BOOL)isValidMobileNumber:(NSString *)mobileNum;
+(BOOL)isValidEmail:(NSString *)email;
+(BOOL)isBlankString:(NSString *)string;
+(BOOL)isValidQQ:(NSString *)qq;
+ (BOOL)checkUserIdCard: (NSString *)value;
+ (BOOL) validateChinese:(NSString *)name;


@end
