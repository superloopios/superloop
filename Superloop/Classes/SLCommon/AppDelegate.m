//
//  AppDelegate.m
//  Superloop
//
//  Created by WangJiWei on 16/3/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "SPUtil.h"
#import "AESCrypt.h"
#import "AppDelegate.h"
#import "SLNavgationViewController.h"
#import "SLTabBarViewController.h"
#import "SLMessageManger.h"
#import "UMSocial.h"
#import "UMSocialWechatHandler.h"
#import "UMSocialSinaSSOHandler.h"
#import <AFNetworking.h>
#import "WXApi.h"
#import "WXApiObject.h"
#import <AlipaySDK/AlipaySDK.h>
#import "NVMContactManager.h"
#import "FirstLeadViewController.h"
#import <CoreTelephony/CTCall.h>
#import <CoreTelephony/CTCallCenter.h>
#import "SLStartTimerViewController.h"
#import "SLAPIHelper.h"
#import "HomePageViewController.h"
#import "SLTopicDetailViewController.h"
#import "SLSystemMessageViewController.h"
#import "SLTopicRepetViewController.h"
#import "SLOpenIMCommon.h"
#import "SLContactDatas.h"
#import "SLCreateQRCodeViewController.h"
#import <UserNotifications/UserNotifications.h>
#import "SLQRCodeViewController.h"
#import "SLContactViewController.h"
#import "SLAddSubjectViewController.h"
#import "SLSearchViewController.h"
#import "SLFansTableViewController.h"
#import <Bugly/Bugly.h>
#import "SLFirstViewController.h"

@interface AppDelegate ()<WXApiDelegate,UIAlertViewDelegate,UNUserNotificationCenterDelegate>

@property(nonatomic,strong)CTCallCenter *callCenter;
@property(nonatomic,strong)NSDictionary *dataWithTime;
@property(nonatomic, assign)BOOL isFirstEntry;
@property(nonatomic,strong)UIViewController *rootVC;
@property(nonatomic,assign)BOOL isBackMode;
@property (nonatomic, assign) NSInteger launchingtime;//客服消息
@property (nonatomic,strong) NSDateFormatter *formatter;
@end
static NSString *const weixinPay = @"weixinPay";
@implementation AppDelegate{
    UIAlertView *alert;
}
- (NSMutableDictionary *)rankingFollow{
    if (!_rankingFollow) {
        _rankingFollow = [NSMutableDictionary new];
    }
    return _rankingFollow;
}
- (NSDateFormatter *)formatter{
    if (!_formatter) {
        _formatter = [[NSDateFormatter alloc] init];
    }
    return _formatter;
}
-(NSMutableDictionary *)OtherInfo{
    if (!_OtherInfo) {
        _OtherInfo=[NSMutableDictionary new];
    }
    return _OtherInfo;
}

-(void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler{
    if (ApplicationDelegate.Basic) {
        SLTabBarViewController *mainTab = [[SLTabBarViewController alloc] init];
        self.window.rootViewController = mainTab;
        [self.window makeKeyAndVisible];
        
        
        if ([shortcutItem.type isEqualToString:@"SCAN"]) {
            SLNavgationViewController * nav = mainTab.childViewControllers[0];
            SLQRCodeViewController * vc = [[SLQRCodeViewController alloc] init];
            [nav pushViewController:vc animated:YES];
        }else if ([shortcutItem.type isEqualToString:@"TOPICS"]) {
            SLNavgationViewController * nav = mainTab.childViewControllers[1];
            SLAddSubjectViewController * vc = [[SLAddSubjectViewController alloc] init];
            [nav presentViewController:vc animated:YES completion:nil];
        }else if ([shortcutItem.type isEqualToString:@"CONTACT"]) {
            mainTab.selectedIndex = 2;
        }else if ([shortcutItem.type isEqualToString:@"SEARCH"]) {
            SLNavgationViewController * nav = mainTab.childViewControllers[0];
            SLSearchViewController * vc = [[SLSearchViewController alloc] init];
            [nav pushViewController:vc animated:YES];
        }
    }else{
        SLNavgationViewController *nav = [[SLNavgationViewController alloc] initWithRootViewController:[[SLFirstViewController alloc] init]];
        self.window.rootViewController = nav;
    }
    
}
- (void)init3DTouch{
    
    UIApplicationShortcutIcon *icon1 = [UIApplicationShortcutIcon iconWithTemplateImageName:@"touchScan"];
    UIApplicationShortcutItem *item1 = [[UIApplicationShortcutItem alloc] initWithType:@"SCAN" localizedTitle:@"扫一扫" localizedSubtitle:nil icon:icon1 userInfo:nil];
    
    UIApplicationShortcutIcon *icon2 = [UIApplicationShortcutIcon iconWithTemplateImageName:@"touchTopics"];
    UIApplicationShortcutItem *item2 = [[UIApplicationShortcutItem alloc] initWithType:@"TOPICS" localizedTitle:@"发话题" localizedSubtitle:nil icon:icon2 userInfo:nil];
    
    UIApplicationShortcutIcon *icon3 = [UIApplicationShortcutIcon iconWithTemplateImageName:@"touchContact"];
    UIApplicationShortcutItem *item3 = [[UIApplicationShortcutItem alloc] initWithType:@"CONTACT" localizedTitle:@"通讯录" localizedSubtitle:nil icon:icon3 userInfo:nil];
    
    UIApplicationShortcutIcon *icon4 = [UIApplicationShortcutIcon iconWithTemplateImageName:@"touchSearch"];
    UIApplicationShortcutItem *item4 = [[UIApplicationShortcutItem alloc] initWithType:@"SEARCH" localizedTitle:@"搜索" localizedSubtitle:nil icon:icon4 userInfo:nil];
    [UIApplication sharedApplication].shortcutItems = @[item1,item2,item3,item4];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    BuglyConfig *config = [[BuglyConfig alloc] init];
    config.channel = @"AppStore";
    [Bugly startWithAppId:@"8ddcc88a53" config:config];
    
    //3D-touch
    if ([[UIDevice currentDevice].systemVersion doubleValue] > 9.0) {
        [self init3DTouch];
    }
    //友盟
#ifdef DEBUG
    [MobClick setLogEnabled:YES];
#else
    [MobClick setLogEnabled:NO];
#endif
    UMConfigInstance.appKey = @"56ee3c49e0f55aff3300039c";
    UMConfigInstance.secret = @"Super2709@$";
    [MobClick startWithConfigure:UMConfigInstance];
    //每次进入app,将提示消息个数清零
    application.applicationIconBadgeNumber = 0;
    //注册个推
    [GeTuiSdk startSdkWithAppId:kGtAppId appKey:kGtAppKey appSecret:kGtAppSecret delegate:self];
    
    //生成地图管理者
    //注册openIM
    [[SLMessageManger sharedInstance] callThisInDidFinishLaunching];
    //设置友盟社会化组件appkey
    [UMSocialData setAppKey:UmengAppkey];
    //设置微信AppId，设置分享url，默认使用友盟的网址
    [UMSocialWechatHandler setWXAppId:@"wx9d74d7fa95aeff85" appSecret:@"a67c3030d850bd58f902ab3823e164df" url:SuperUrl];
    
    // 打开新浪微博的SSO开关
    // 将在新浪微博注册的应用appkey、redirectURL替换下面参数，并在info.plist的URL Scheme中相应添加wb+appkey，如"wb3921700954"，详情请参考官方文档。
    [UMSocialSinaSSOHandler openNewSinaSSOWithAppKey:@"380893160"
                                              secret:@"9c3762cd55e9a26ac6a21b9996bb311e"
                                         RedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    //设置分享到QQ空间的应用Id，和分享url 链接
    //    [UMSocialQQHandler setQQWithAppId:@"1105274216" appKey:@"u658zj4K2GOdVsAs" url:SuperUrl];
    //设置支持没有客户端情况下使用SSO授权
    //    [UMSocialQQHandler setSupportWebView:YES];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10) {
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate=self;
        UNAuthorizationOptions types10=UNAuthorizationOptionBadge|UNAuthorizationOptionAlert|UNAuthorizationOptionSound;
        [center requestAuthorizationWithOptions:types10 completionHandler:^(BOOL granted, NSError * _Nullable error) {
            
        }];
        
    } else { // iOS8.0 以前远程推送设置方式
        
    }
    //向微信注册wx84b2b09b83324c2b
    [WXApi registerApp:@"wx9d74d7fa95aeff85" withDescription:@"demo 2.0"];
    //由于苹果审核政策需求，对未安装客户端平台进行隐藏，在设置QQ、微信AppID之后调用下面的方法
    [UMSocialConfig hiddenNotInstallPlatforms:@[UMShareToWechatSession, UMShareToWechatTimeline]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpAction) name:@"jumptoHome" object:nil];
    
    //从本地取出登录信息
    [self getLoginInformation];
    
    //启动
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    if (![SLUserDefault getAppIsFirst]){
        
        FirstLeadViewController *firstLeadVC = [[FirstLeadViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:firstLeadVC];
        self.window.rootViewController = nav;
        
    } else {
        SLStartTimerViewController *firstLeadVC = [[SLStartTimerViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:firstLeadVC];
        self.window.rootViewController = nav;
        
    }
    [self.window makeKeyAndVisible];
    self.getMySettingTagStatus = YES;  //设置为提示用户设置标签
    
    // 注册APNS
    //[[SLMessageManger sharedInstance] handleAPNSPush];
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    [self registerUserNotification];
    //OPENIM通知
    
    //处理通话状态问题
    _callCenter = [[CTCallCenter alloc] init];
    
    _callCenter.callEventHandler=^(CTCall* call){
        
        if ([call.callState isEqualToString:CTCallStateDisconnected]){
            NSLog(@"Call has been disconnected");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Disconnected" object:@"1"];
        }
        else if ([call.callState isEqualToString:CTCallStateConnected]){
            NSLog(@"Call has just been connected");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Connected" object:@"1"];
        }
        else if([call.callState isEqualToString:CTCallStateIncoming]){
            NSLog(@"Call is incoming");
        }
        else if ([call.callState isEqualToString:CTCallStateDialing]){
            NSLog(@"call is dialing");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Dialing" object:nil];
        }
        else{
            NSLog(@"Nothing is done");
        }
    };
    
    NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]) {
        //跳转到一个controller
        NSLog(@"通知");
        NSString *category = [userInfo valueForKey:@"category"];
        NSDictionary *dic = [self dictionaryWithJsonString:category];
        [self jumpToDestVc:dic];
    }
    // 添加超级圈到通讯录
    [[NVMContactManager manager] addSuperloop2AddressBook];
    self.netWorkStatus = @"Unknown";
    [self getNetWorkStatus];
    [[UITextField appearance] setTintColor:SLMainColor];
    [[UITextView appearance] setTintColor:SLMainColor];
    return YES;
}
//是否去评分
- (void)gotoEavluate{
    NSData *categoryData=[[NSUserDefaults standardUserDefaults] objectForKey:@"timeAndCount"];
    NSString * dateTime = nil;//时间
    NSInteger count;//次数
    NSString * dateCount = nil;//3天内是否达到次数
    if (categoryData) {
        dateTime=[NSKeyedUnarchiver unarchiveObjectWithData:categoryData][@"dateTime"];
        count=[[NSKeyedUnarchiver unarchiveObjectWithData:categoryData][@"count"] integerValue];
        dateCount=[NSKeyedUnarchiver unarchiveObjectWithData:categoryData][@"3DaysBeyond"];
        if ([self judge3DaysApart:dateTime]) {//3天内
            count ++;
            if (count < 10) {//没超过10次
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"GiveEvaluationView"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                dateCount = @"0";
            }else{//超过10次
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"GiveEvaluationView"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                count = 0;
                //NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [self.formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *createdAtDate = [self.formatter dateFromString:dateTime];
                NSTimeInterval secondsPerDay = 3*24*60*60;
                NSDate *nextDay = [createdAtDate dateByAddingTimeInterval:secondsPerDay];
                dateTime = [self.formatter stringFromDate:nextDay];//3天后的时间
                dateCount = @"1";
            }
            [self cacheDate:dateTime count:count dateCount:dateCount];
        }else{//清空
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"GiveEvaluationView"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if ([dateCount isEqualToString:@"0"]) {//3天内没超过10次，下次打开超过了3天
                [self recordTimeAndCount:0];
            }else{//3天内超过10次，下次打开超过了3天
                [self cacheDate:dateTime count:count dateCount:dateCount];
            }
        }
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"GiveEvaluationView"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self recordTimeAndCount:0];
    }
}
//沙盒存时间、次数
- (void)cacheDate:(NSString *)dateTime count:(NSInteger )count dateCount:(NSString *)dateCount{
    NSDictionary *timeAndCount=@{@"dateTime":dateTime,@"count":[NSString stringWithFormat:@"%ld",(long)count],@"3DaysBeyond":dateCount};
    NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:timeAndCount];
    [[NSUserDefaults standardUserDefaults] setObject:configData forKey:@"timeAndCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
//记录登录时间和次数
- (void)recordTimeAndCount:(NSInteger)count{
    //    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [self.formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime = [self.formatter stringFromDate:[NSDate date]];
    NSString *dateCount = @"0";
    [self cacheDate:dateTime count:count dateCount:dateCount];
}
//判断是否相差3天
- (BOOL)judge3DaysApart:(NSString *)dateTime{
    //    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [self.formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date1 = [self.formatter dateFromString:dateTime];
    NSDate *date2 = [self getCurrentDate];
    NSTimeInterval aTimer = [date2 timeIntervalSinceDate:date1];
    if (aTimer && (aTimer < 3 * 24 * 3600) &&( aTimer > 0 || aTimer == 0)) {
        return YES;
    }
    return NO;
}
//获取当前时间
- (NSDate *)getCurrentDate{
    //    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [self.formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime = [self.formatter stringFromDate:[NSDate date]];
    NSDate *currentDate = [self.formatter dateFromString:dateTime];
    return currentDate;
}
-(void)jumpAction{
    //    SLTabBarViewController *tabBar = [[SLTabBarViewController alloc] init];
    //    tabBar.selectedIndex = 0;
}
- (void)getLoginInformation{
    NSDictionary *dict = [SLUserDefault getUserInfo];
    _Basic = [dict objectForKey:@"basic"];
    _userInformation = [dict objectForKey:@"userInformation"];
    _userId = _userInformation[@"id"];
    if (_Basic) {
        
        [self autoLogin];
    }
}
-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options{
    NSLog(@"%@",url);
    if ([url.host isEqualToString:@"scheme.superloop.com"]) {
        UINavigationController *selectedController;
        if([[UIApplication sharedApplication].keyWindow.rootViewController isKindOfClass:[SLTabBarViewController class]]){
            self.rootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
            selectedController = ((SLTabBarViewController *)self.rootVC).selectedViewController;
        }else{
            SLTabBarViewController *indexVC = [[SLTabBarViewController alloc]init];
            UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 49)];
            backView.backgroundColor = [UIColor whiteColor];
            [indexVC.tabBar insertSubview:backView atIndex:0];
            indexVC.tabBar.opaque = YES;
            //设置tabBar的默认选中为第一个
            indexVC.selectedIndex = 0;
            self.window.rootViewController = indexVC;
            self.isFirstEntry = YES;
            selectedController = indexVC.selectedViewController;
        }
        if ([url.path isEqualToString:@"/user"]) {
            NSLog(@"user%@",url.query);
            NSArray *idArr = [url.query componentsSeparatedByString:@"="];
            if ([[NSString stringWithFormat:@"%@", idArr[1]] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
                HomePageViewController *myView = [[HomePageViewController alloc] init];
                myView.userId = ApplicationDelegate.userId;
                [selectedController pushViewController:myView animated:YES];
            }else{
                HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
                
                otherPerson.userId = idArr[1];
                [selectedController pushViewController:otherPerson animated:YES];
            }
        }
        if ([url.path isEqualToString:@"/topic"]) {
            //            kShowToast(@"topic")
            NSArray *idArr = [url.query componentsSeparatedByString:@"="];
            SLTopicDetailViewController *topicDetail = [[SLTopicDetailViewController alloc] init];
            topicDetail.topicId = idArr[1];
            [selectedController pushViewController:topicDetail animated:YES];
        }
    }
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDict) {
            if(!resultDict) return;
            NSString *resultStatus = [NSString stringWithFormat:@"%@",resultDict[@"resultStatus"]];
            
            if([resultStatus isEqualToString:@"9000"]){
                NSString *result=resultDict[@"result"];
                if(!result) return;
                NSArray *subArray = [result componentsSeparatedByString:@"&"];
                
                NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
                
                for (int i = 0 ; i < subArray.count; i++) {
                    // 通过=号拆分键和值
                    NSArray *dictArray = [subArray[i] componentsSeparatedByString:@"="];
                    if(dictArray.count == 0) continue;
                    // 给字典加入元素
                    [tempDict setObject:dictArray[1] forKey:dictArray[0]];
                }
                
                NSString *successStr = tempDict[@"success"];
                if(!successStr) return;
                
                if([successStr isEqualToString:@"\"true\""]){
                    NSString *out_trade_no = tempDict[@"out_trade_no"];
                    
                    if(!out_trade_no) return;
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"aliPaySuccessed" object:self];
                }
            }
            
            if([resultStatus isEqualToString:@"6001"]){
                //充值取消
                [HUDManager showWarningWithText:@"充值取消"];
            }
            
            if([resultStatus isEqualToString:@"4000"]){
                //充值订单支付失败
                [HUDManager showWarningWithText:@"订单支付失败"];
            }
            
            if([resultStatus isEqualToString:@"6002"]){
                //充值网络连接出错
                [HUDManager showWarningWithText:@"网络连接出错"];
            }
            
        }];
        return YES;
    }
    
    BOOL result = [UMSocialSnsService handleOpenURL:url];
    
    
    if ([url.host isEqualToString:@"pay"]) {
        
        return [WXApi handleOpenURL:url delegate:self];
        
    }
    
    if (result == FALSE) {
        //调用其他SDK，例如支付宝SDK等
        return [WXApi handleOpenURL:url delegate:self];
    }else{
        
        return result;
    }
    
    return YES;
}
/**
 这里处理新浪微博SSO授权之后跳转回来，和微信分享完成之后跳转回来
 */
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
        }];
        return YES;
    }
    
    BOOL result = [UMSocialSnsService handleOpenURL:url];
    
    
    if ([url.host isEqualToString:@"pay"]) {
        
        return [WXApi handleOpenURL:url delegate:self];
        
    }
    
    if (result == FALSE) {
        //调用其他SDK，例如支付宝SDK等
        return [WXApi handleOpenURL:url delegate:self];
    }else{
        
        return result;
    }
    
    
    return YES;
}




#pragma 支付的回调方法
//收到一个来自微信的处理结果。调用一次sendReq后会收到onResp。
#pragma mark - 第三方的跳转
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    NSLog(@"%@",url);
    
    if(url != nil && [[url host] isEqualToString:@"pay"]){
        //微信支付
        return [WXApi handleOpenURL:url delegate:self];
    }
    else{
        //其他
        return YES;
    }
    
}

//** 注册用户通知 */
- (void)registerUserNotification {
    
    /*
     注册通知(推送)
     申请App需要接受来自服务商提供推送消息
     */
    
    // 判读系统版本是否是“iOS 8.0”以上
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 ||
        [UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]) {
        
        // 定义用户通知类型(Remote.远程 - Badge.标记 Alert.提示 Sound.声音)
        UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound;
        
        // 定义用户通知设置
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        
        // 注册用户通知 - 根据用户通知设置
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else { // iOS8.0 以前远程推送设置方式
        // 定义远程通知类型(Remote.远程 - Badge.标记 Alert.提示 Sound.声音)
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        
        // 注册远程通知 -根据远程通知类型
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    }
}
- (void)jumpToDestVc:(NSDictionary *)dic{
    if (!self.Basic) {
        return;
    }
    NSLog(@"%@",dic)
    if (!_isBackMode) {
        UINavigationController *selectedController;
        if([[UIApplication sharedApplication].keyWindow.rootViewController isKindOfClass:[SLTabBarViewController class]]){
            self.rootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
            selectedController = ((SLTabBarViewController *)self.rootVC).selectedViewController;
        }else{
            SLTabBarViewController *indexVC = [[SLTabBarViewController alloc]init];
            UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 49)];
            backView.backgroundColor = [UIColor whiteColor];
            [indexVC.tabBar insertSubview:backView atIndex:0];
            indexVC.tabBar.opaque = YES;
            //设置tabBar的默认选中为第一个
            indexVC.selectedIndex = 0;
            self.window.rootViewController = indexVC;
            self.isFirstEntry = YES;
            selectedController = indexVC.selectedViewController;
        }
        if ([[dic[@"messageType"] stringValue] isEqualToString:@"0"]) {
            SLTopicRepetViewController *vc = [[SLTopicRepetViewController alloc] init];
            if ([NSStringFromClass([[selectedController.viewControllers lastObject] class]) isEqualToString:NSStringFromClass([SLTopicRepetViewController class])]) {
                return;
            }
            [selectedController pushViewController:vc animated:YES];
            
        }else if ([[dic[@"messageType"] stringValue] isEqualToString:@"1"]){
            NSDictionary *messageDic = dic[@"message"];
            if ([[messageDic[@"messageType"] stringValue] isEqualToString:@"8"] ) {
                SLTabBarViewController *tabBarVc = (SLTabBarViewController *)self.rootVC;
                [tabBarVc setSelectedIndex:4];
            }else{
                SLSystemMessageViewController *vc = [[SLSystemMessageViewController alloc] init];
                if ([NSStringFromClass([[selectedController.viewControllers lastObject] class]) isEqualToString:NSStringFromClass([SLSystemMessageViewController class])]) {
                    return;
                }
                [selectedController pushViewController:vc animated:YES];
            }
            
        }
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler {
    
    NSDictionary *aps = [userInfo valueForKey:@"aps"];
    NSString *category = [aps valueForKey:@"category"];
    NSDictionary *dic = [self dictionaryWithJsonString:category];
    [self jumpToDestVc:dic];
}

//iOS10新增：处理后台点击通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler{
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //应用处于后台时的远程推送接受
        //必须加这句代码
        NSDictionary *aps = [userInfo valueForKey:@"aps"];
        NSString *category = [aps valueForKey:@"category"];
        NSDictionary *dic = [self dictionaryWithJsonString:category];
        //    NSLog(@"%@",aps);
        [self jumpToDestVc:dic];
        
    }
    
}
/** 自定义：APP被“推送”启动时处理推送消息处理（APP 未启动--》启动）*/
- (void)receiveNotificationByLaunchingOptions:(NSDictionary *)launchOptions {
    if (!launchOptions)
        return;
    /*
     通过“远程推送”启动APP
     UIApplicationLaunchOptionsRemoteNotificationKey 远程推送Key
     */
}
/** 远程通知注册成功委托 */
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *myToken = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    myToken = [myToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    [GeTuiSdk registerDeviceToken:myToken];    /// 向个推服务器注册deviceToken
}
/** SDK启动成功返回cid */
- (void)GeTuiSdkDidRegisterClient:(NSString *)clientId {
    //个推SDK已注册，返回clientId
    NSLog(@"\n>>>[GeTuiSdk RegisterClient]:%@\n\n", clientId);
}
/** SDK遇到错误回调 */
- (void)GeTuiSdkDidOccurError:(NSError *)error {
    if (error) {
        NSException *exception = [NSException exceptionWithName:@"GeTuiError"
                                                         reason:[error localizedDescription]
                                                       userInfo:nil];
        [Bugly reportException:exception];
    }
}
//如果获取DeviceToken获取失败，也需要通知个推服务器
/** 远程通知注册失败委托 */
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [GeTuiSdk registerDeviceToken:@""];     /// 如果APNS注册失败，通知个推服务器
}
/** SDK收到透传消息回调 */
- (void)GeTuiSdkDidReceivePayloadData:(NSData *)payloadData andTaskId:(NSString *)taskId andMsgId:(NSString *)msgId andOffLine:(BOOL)offLine fromGtAppId:(NSString *)appId
{
    NSData *payload = payloadData;
    NSString *payloadMsg = nil;
    if (payload) {
        payloadMsg = [[NSString alloc] initWithBytes:payload.bytes length:payload.length encoding:NSUTF8StringEncoding];
    }
    NSString *msg = [NSString stringWithFormat:@"%@",payloadMsg];
    NSDictionary *dic = [self dictionaryWithJsonString:msg];
    
    if ([[dic allKeys] containsObject:@"message"]) {
        NSDictionary *fansMessage = dic[@"message"];
        if ([[fansMessage allKeys] containsObject:@"messageType"]) {
            if ([fansMessage[@"messageType"] integerValue] == 8) {
                if (self.userId) {
                    NSString *newNum = [NSString stringWithFormat:@"%@",fansMessage[@"content"][@"news"]];
                    [[NSUserDefaults standardUserDefaults] setObject:newNum forKey:@"newFansNumber"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"hasNewFans" object:nil];
                    SLTabBarViewController *vc;
                    SLNavgationViewController *navVc;
                    if([[UIApplication sharedApplication].keyWindow.rootViewController isKindOfClass:[SLTabBarViewController class]]){
                        vc = (SLTabBarViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
                        navVc = vc.viewControllers[4];
                        navVc.tabBarItem.badgeValue = newNum;
                    }
                }
                return;
            }
        }
    }
    if (!offLine) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"getGeTuiMessage" object:nil userInfo:nil];
    }
    
}

/*!
 * @brief 把格式化的JSON格式的字符串转换成字典
 * @param jsonString JSON格式的字符串
 * @return 返回字典
 */
- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        return nil;
    }
    return dic;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    _isBackMode = YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    //application.applicationIconBadgeNumber = 0;
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    _isBackMode = NO;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"getGeTuiMessage" object:nil userInfo:nil];
    [UMSocialSnsService  applicationDidBecomeActive];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application{
    // 停止下载图片
    [[SDWebImageManager sharedManager] cancelAll];
    // 清除内存缓存图片
    [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
    [[SDImageCache sharedImageCache] clearMemory];
}

- (void)saveBasic:(NSString *)basic userInformation:(NSMutableDictionary *)userInformation password:(NSString *)password
{
    NSDictionary *dict = @{@"basic": basic, @"userInformation": userInformation,@"Password": password};
    [SLUserDefault setUserInfo:dict];
}
- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
}
- (void)autoLogin{
    NSDictionary *dict = [SLUserDefault getUserInfo];
    if (dict) {
        NSString *password = @"";
        if ([dict objectForKey:@"Password"]) {
            password = [dict objectForKey:@"Password"];
            
            __weak typeof(self) weakSelf = self;
            [SLOpenIMCommon login:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId] account:nil password:[dict objectForKey:@"Password"] successBlock:^{
                [[SPUtil sharedInstance] setWaitingIndicatorShown:NO withKey:weakSelf.description];
            } failedBlock:^(NSError *aError) {
                
            }];
            
        }
    }
}

#pragma mark --- 支付宝的结果处理
-(BOOL)Alipay:(NSURL *)url{
    /*
     9000 订单支付成功
     8000 正在处理中
     4000 订单支付失败
     6001 用户中途取消
     6002 网络连接出错
     */
    if ([url.host isEqualToString:@"safepay"]) {
        //这个是进程KILL掉之后也会调用，这个只是第一次授权回调，同时也会返回支付信息
        [[AlipaySDK defaultService]processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
            [self AlipayWithResutl:resultDic];
        }];
        //跳转支付宝钱包进行支付，处理支付结果，这个只是辅佐订单支付结果回调
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            [self AlipayWithResutl:resultDic];
        }];
        
    }else if ([url.host isEqualToString:@"platformapi"]){
        //授权返回码
        [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
            [self AlipayWithResutl:resultDic];
        }];
    }
    
    return YES;
    
}

-(void)AlipayWithResutl:(NSDictionary *)resultDic{
    NSString  *str = [resultDic objectForKey:@"resultStatus"];
    if (str.intValue == 9000)
    {
        // 支付成功
        //通过通知中心发送通知
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PayResult" object:@"ali_success" userInfo:nil];
    }
    else
    {
        //通过通知中心发送通知
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PayResult" object:@"fail" userInfo:nil];
    }
}

//收到一个来自微信的处理结果。调用一次sendReq后会收到onResp。
- (void)onResp:(BaseResp *)resp {
    if ([resp isKindOfClass:[PayResp class]]) {
        PayResp *response = (PayResp *)resp;
        switch (response.errCode) {
            case WXSuccess: {
                
                NSLog(@"支付成功");
                [[NSNotificationCenter defaultCenter] postNotificationName:@"weixinPaySuccess" object:self];
                
                //...支付成功相应的处理，跳转界面等
                
                break;
            }
            case WXErrCodeUserCancel: {
                
                NSLog(@"用户取消支付");
                
                break;
            }
            default: {
                
                NSLog(@"支付失败");
                
                //...做相应的处理，重新支付或删除支付
                
                break;
            }
        }
    }
}
-(void)getNetWorkStatus{
    //进入页面之后会有一个提示网络状态的情况
    //判断如果没有网路情况下没必要请求网络
    __weak typeof(self) weakSelf = self;
    
    AFNetworkReachabilityManager *manger = [AFNetworkReachabilityManager sharedManager];
    [manger setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        __strong typeof(self) strongSelf = weakSelf;
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
                if([[UIApplication sharedApplication].keyWindow.rootViewController isKindOfClass:[SLTabBarViewController class]]){
                    [HUDManager showWarningWithText:@"网络出错,请检查网络状况"];
                }
                strongSelf.netWorkStatus = @"noNetWork";
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                strongSelf.netWorkStatus = @"wifi";
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                strongSelf.netWorkStatus = @"wan";
                break;
            default:
                strongSelf.netWorkStatus = @"Unknown";
                break;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"netWorkChange" object:nil];
    }];
    
    [manger startMonitoring];
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
