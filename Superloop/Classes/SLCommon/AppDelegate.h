//
//  AppDelegate.h
//  Superloop
//
//  Created by WangJiWei on 16/3/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GeTuiSdk.h"
#ifdef DEBUG

#define kGtAppId           @"0Ch8tCoBfIAjZivVZ0aQgA"
#define kGtAppKey          @"DyPJsioMNo6mKUu50l3vL7"
#define kGtAppSecret       @"aaR3JTQuUp7bpEg01lLK68"

#else

#define kGtAppId           @"8HsHGG4vNs6yhPzEF2SG17"
#define kGtAppKey          @"8avcqXVQOo8lbdGqjitwl"
#define kGtAppSecret       @"4CXwnJDSkr5OJetPrVMwd"

#endif


#define UmengAppkey @"56ee3c49e0f55aff3300039c"
#define SuperUrl @"http://www.superloop.com.cn"
@interface AppDelegate : UIResponder <UIApplicationDelegate,GeTuiSdkDelegate>
@property (strong, nonatomic) UIWindow *window;
//判断用户是否为登录状态
@property (nonatomic, assign) BOOL isLogin;
//@property (nonatomic, copy) NSString *account;  //登录名
@property (nonatomic, copy) NSString *userId; // 用户id
@property (nonatomic, copy) NSString *otherId; // 他人id
//@property (nonatomic, copy) NSString *resultPassword;  // 返回的密码
//@property (nonatomic, copy) NSString *userName; //用户名
@property (nonatomic, copy) NSString *Basic; // 保存basic信息
@property (nonatomic, strong) NSMutableDictionary *userInformation; //用户信息
@property (nonatomic, strong) NSDictionary *SelfInfo;//我的个人数据
//@property (nonatomic, strong) NSDictionary *OtherInfo;//他人数据
@property (nonatomic, strong) NSMutableDictionary *OtherInfo;//他人数据
@property (nonatomic, assign) BOOL getMySettingTagStatus;//我的标签属性
@property (nonatomic, strong) NSMutableArray *publishPhotosArr;//发布话题图片保存
@property (nonatomic, copy) NSString *titleStr;//发布话题标题
@property (nonatomic, copy) NSString *contentStr;//发布话题内容
@property (nonatomic, assign) NSInteger dynamicMsg;//消息动态
@property (nonatomic, assign) NSInteger systemMsg;//系统动态
@property (nonatomic, assign) NSInteger serviceMsg;//客服消息
@property (nonatomic, copy) NSString *netWorkStatus;//发布话题内容
@property (nonatomic, copy) NSString *imTitlename;//IM聊天title
@property (nonatomic,strong) NSMutableDictionary *rankingFollow;//排行榜和服务榜之间关注
@end

