//
//  SLUserdefault.h
//  Superloop
//
//  Created by administrator on 16/7/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SLUserDefault : NSObject
//get基础方法
+ (id)getData:(id)value;
//set数据基本的方法
+ (void)setData:(id)value forKey:(NSString *)key;



+ (instancetype)sharedInstance;
+ (BOOL)getAppIsFirst;
+ (void)setAppIsFirst:(BOOL)value;
+ (id)getUserInfo;
+ (void)removeUserInfo;
+ (void)setUserInfo:(id)value;
+ (id)getAddressData;
+ (void)setAddressData:(id)value;
+ (NSString *)getStrAccout;
+ (void)setStrAccount:(id)value;
+ (void)removeStrAccount;

+ (NSString *)getStrAccountName;
+ (void)setStrAccountName:(id)value;
+ (void)removeStrAccountName;

+ (NSInteger)getswithIsOn;
+ (void)setswithIsOn:(NSInteger)isOn;

+ (id)getBannerImgData;
+ (void)setBannerImgData:(id)value;

+ (NSString *)getLocation;
+ (void)setLocation:(id)value;

+ (NSString *)getLocationCode;
+ (void)setLocationCode:(id)value;

+ (NSString *)getLocationFailure;
+ (void)setLocationFailure:(id)value;

+ (NSString *)getCloseLocation;
+ (void)setCloseLocation:(id)value;

+ (NSString *)getPhoneNumber;
+ (void)setPhoneNumber:(id)value;

+ (NSString *)getNewsCount;
+ (void)setNewsCount:(id)isNew;

+ (id)getCategoryKey;
+ (void)setCategoryKey:(id)value;

+ (NSString *)getPrice;
+ (void)setPrice:(id)value;
//应该是验证密码
+ (NSString *)getSure;
+ (void)setSure:(id)value;

//不知道在哪里用
+ (NSString *)getName;
+ (void)setName:(id)value;


+ (id)getSuggestionData;
+ (void)setSuggestionData:(id)value;

+ (NSString *)getVoiceType; //获取耳机的类型
+ (void)setVoiceType:(id)value; //设置耳机的类型

- (NSDictionary *)getMessageInfo:(NSString *)key; //获取值不值消息
- (void)setMessageInfo:(NSDictionary *)value withKey:(NSString *)key; //设置值不值消息

//获取是否允许添加通讯录状态
+ (BOOL)getAllowAddContactStatus;
//设置是否允许添加通讯录状态
+ (void)setAllowAddContactStatus:(BOOL)value;
//获取通讯录状态
+ (BOOL)getContactStatus;
//设置通讯录状态
+ (void)setContactStatus:(BOOL)value;

//临时数据
+ (BOOL)getLoginStutas;
//临时数据
+ (void)setLoginStutas:(BOOL)value;

//获取个人信息缓存
+ (id)getPersonInfo:(NSString *)personId;
//设置personcache
+ (void)setPersonInfo:(NSString *)personId andData:(NSData *)data;

//获取个人话题缓存信息
+ (id)getPersonTopic:(NSString *)personId;
//设置个人话题缓存信息
+ (void)setPersonTopic:(NSString *)personId andData:(NSData *)data;

//获取个人收到的评价缓存信息
+ (id)getPersonReceive:(NSString *)personId;
//设置个人收到的评价缓存信息
+ (void)setPersonReceive:(NSString *)personId andData:(NSData *)data;

@end
