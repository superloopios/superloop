//
//  SLStartTimerViewController.m
//  Superloop
//
//  Created by 朱宏伟 on 16/6/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLStartTimerViewController.h"
#import "SLTabBarViewController.h"
#import  <ImageIO/ImageIO.h>
#import "SLAPIHelper.h"
#import <UIImageView+WebCache.h>
#import "SLWebViewController.h"
#import "AppDelegate.h"
#import "SLFirstViewController.h"
#import "SLNavgationViewController.h"
#import "SLUserDefault.h"

@interface SLStartTimerViewController ()

@property (nonatomic,strong) dispatch_source_t _timer;
@property (nonatomic,strong) UIImageView *lightBubleImageView;
@property (nonatomic,assign) NSInteger timeCount;//倒计时长
@property (nonatomic,strong) UIButton *jumpBtn;
@property (nonatomic,strong) NSDictionary *splashDatas;
@property (nonatomic,assign) BOOL isTimeOut;
@property (nonatomic,assign) BOOL isLoadSuccess;
@property (nonatomic,strong) UIView *imgJointView;
@property (nonatomic,strong) UIImageView *topImgView;
@property (nonatomic,strong) UIImageView *bottomImgView;

@end

@implementation SLStartTimerViewController
- (NSDictionary *)splashDatas{
    if (!_splashDatas) {
        _splashDatas = [NSDictionary dictionary];
    }
    return _splashDatas;
}
- (UIView *)imgJointView{
    if (!_imgJointView) {
        _imgJointView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _imgJointView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_imgJointView];
    }
    return _imgJointView;
}
- (UIImageView *)topImgView{
    if (!_topImgView) {
        _topImgView = [[UIImageView alloc] init];
        _topImgView.backgroundColor = [UIColor whiteColor];
        CGFloat heightProportion = [self getScreenProportion];
        _topImgView.frame = CGRectMake(0, 0, ScreenW, ScreenH - heightProportion);
        _topImgView.contentMode = UIViewContentModeScaleAspectFill;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgTap)];
        _topImgView.userInteractionEnabled = YES;
        [_topImgView addGestureRecognizer:tap];
    }
    return _topImgView;
}
- (UIImageView *)bottomImgView{
    if (!_bottomImgView) {
        _bottomImgView = [[UIImageView alloc] init];
        CGFloat heightProportion = [self getScreenProportion];
        _bottomImgView.frame = CGRectMake(0, ScreenH - heightProportion, ScreenW, heightProportion);
        _bottomImgView.image = [UIImage imageNamed:@"ios-2"];
        _bottomImgView.contentMode = UIViewContentModeScaleAspectFill;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgTap)];
        _bottomImgView.userInteractionEnabled = YES;
        [_bottomImgView addGestureRecognizer:tap];
    }
    return _bottomImgView;
}
- (UIButton *)jumpBtn{
    if (!_jumpBtn) {
        _jumpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _jumpBtn.frame = CGRectMake(ScreenW -80, 20, 80, 50);
        _jumpBtn.adjustsImageWhenHighlighted = NO;
//        _jumpBtn.layer.cornerRadius = 10.0;
//        _jumpBtn.layer.masksToBounds = YES;
//        [_jumpBtn.layer setBorderWidth:1.0f];
//        _jumpBtn.layer.borderColor = [UIColor colorWithRed:255.0 / 255 green:255.0 /255 blue:255.0 / 255 alpha:0.5].CGColor;
        [_jumpBtn setImage:[UIImage imageNamed:@"skip"] forState:UIControlStateNormal];
//        [_jumpBtn.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
//        [_jumpBtn setTitleColor:SLColor(165,165, 165) forState:UIControlStateNormal];
//        _jumpBtn.backgroundColor = [UIColor c];
        _jumpBtn.alpha = 0.5;
        [_jumpBtn addTarget:self action:@selector(jumpAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _jumpBtn;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.imgJointView addSubview:self.topImgView];
    [self.imgJointView addSubview:self.bottomImgView];
    self.isTimeOut = NO;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self getDatas];
        [self operationTimeCount];
    });
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (CGFloat)getScreenProportion{//获取屏幕比例

    if (ScreenW == 375) {
        return 142;
    } else if(ScreenW == 414){
        return 157;
    } else {
        return 122;
    }
}
- (void)getDatas{
    [SLAPIHelper getSplashSuccess:^(NSURLSessionDataTask *task, NSDictionary *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.isTimeOut) {
                return ;
            }
            self.isLoadSuccess = YES;
            self.splashDatas = data[@"result"];
            [self.topImgView sd_setImageWithURL:[NSURL URLWithString:self.splashDatas[@"image_url"]]];
            [self.view addSubview:self.jumpBtn];
            [self startTimer:[self.splashDatas[@"count_down"] integerValue]];
        });
    } failure:^(SLHttpRequestError *failure) {
        if (self.isTimeOut) {
            return ;
        }
        self.isLoadSuccess = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self jumpAction];
        });
    }];
}
- (void)operationTimeCount{
    __block int timeout = 2.0; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t operationTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(operationTimer,dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(operationTimer, ^{
        if (self.isLoadSuccess) {
            dispatch_source_cancel(operationTimer);
            return ;
        }
        if (timeout ==0) {
            dispatch_source_cancel(operationTimer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                self.isTimeOut = YES;
                [self jumpAction];
                return;
            });
        }
        timeout--;
    });
    dispatch_resume(operationTimer);
}

- (void)imgTap{
    if (self.splashDatas[@"target_url"]) {
        if (self._timer) {
            dispatch_cancel(self._timer);
        }
        SLWebViewController *vc = [[SLWebViewController alloc] init];
        vc.url = self.splashDatas[@"target_url"];
        vc.isFromSplash = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)startTimer:(NSInteger)timeCount{
    __block NSInteger timeout = timeCount; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    self._timer = _timer;
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if (timeout ==0) {
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [self jumpAction];
                return;
            });
        }
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [_jumpBtn setTitle:@"跳过" forState:UIControlStateNormal];
//        });
        timeout--;
    });
    dispatch_resume(_timer);
}


-(void)jumpAction{
    self.isTimeOut = YES;
    self.isLoadSuccess = YES;
    if (self._timer) {
        dispatch_cancel(self._timer);
    }
    if ([SLUserDefault getUserInfo]) {
        SLTabBarViewController *tabVc = [[SLTabBarViewController alloc] init];
        [UIApplication sharedApplication].keyWindow.rootViewController = tabVc;
    }else{
        SLFirstViewController *indexVC = [[SLFirstViewController alloc] init];
        SLNavgationViewController *nav = [[SLNavgationViewController alloc] initWithRootViewController:indexVC];
        [UIApplication sharedApplication].keyWindow.rootViewController = nav;
    }
  
}
@end
