//
//  SLUserdefault.m
//  Superloop
//
//  Created by administrator on 16/7/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLUserDefault.h"

#define userDefault [NSUserDefaults standardUserDefaults]

@implementation SLUserDefault

+ (instancetype)sharedInstance{
    static SLUserDefault *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    return manager;
}
//获取数据的基础方法
+ (id)getData:(id)value{
    return [userDefault objectForKey:value];
}
//set数据基本的方法
+ (void)setData:(id)value forKey:(NSString *)key{
    [userDefault setObject:value forKey:key];
    [userDefault synchronize];
}
//删除数据的基础方法
+ (void)removeDataForKey:(NSString *)key{
    [userDefault removeObjectForKey:key];
    [userDefault synchronize];
}
//是否第一次登陆
+ (BOOL)getAppIsFirst{
    return [userDefault boolForKey:appIsFirst];
}
//设置是否第一次登陆
+ (void)setAppIsFirst:(BOOL)value{
    [userDefault setBool:value forKey:appIsFirst];
    [userDefault synchronize];
}
//获取用户的基本信息
+ (id)getUserInfo{
    return [self getData:SLUserLogin];
}
//删除用户的信息
+ (void)removeUserInfo{
    [self removeDataForKey:SLUserLogin];
    
}
//设置用户的信息
+ (void)setUserInfo:(id)value{
    [self setData:value forKey:SLUserLogin];
}
//获取地理信息
+ (id)getAddressData{
    return [self getData:addressData];
}
//设置地理信息
+ (void)setAddressData:(id)value{
    [self setData:value forKey:addressData];
}
//获取账号str
+ (NSString *)getStrAccout{
    return [self getData:strAccout];
}
//设置账号
+ (void)setStrAccount:(id)value{
    [self setData:value forKey:strAccout];
}
//删除账号
+ (void)removeStrAccount{
    [self removeDataForKey:strAccout];
}
//获取账户名称
+ (NSString *)getStrAccountName{
    return [self getData:strAccountName];
}
//设置账号名
+ (void)setStrAccountName:(id)value{
    [self setData:value forKey:strAccountName];
}
//删除用户名称
+ (void)removeStrAccountName{
    [self removeDataForKey:strAccountName];
}

+ (NSInteger)getswithIsOn{
    return [userDefault integerForKey:swithIsOn];
}
+ (void)setswithIsOn:(NSInteger)isOn{
    [userDefault setInteger:isOn forKey:swithIsOn];
    [userDefault synchronize];
}

+ (NSString *)getVoiceType{
    return [userDefault objectForKey:VoiceType]; //获取消息耳机的类型
}
+ (void)setVoiceType:(id)value{
    [userDefault setObject:value forKey:VoiceType]; //设置消息耳机的类型
    [userDefault synchronize];
}

- (NSString *)getMessageInfo:(NSString *)key{
  return [userDefault objectForKey:key];  //获取缓存付费消息
}
- (void)setMessageInfo:(NSDictionary *)value withKey:(NSString *)key{
    [userDefault setObject:value forKey:key]; //设置付费消息
    [userDefault synchronize];

}
//轮播图
+ (id)getBannerImgData{
    return [self getData:BannerImg];
}
+ (void)setBannerImgData:(id)value{
    [self setData:value forKey:BannerImg];
}

+ (NSString *)getLocation{
    return [self getData:location];
}
+ (void)setLocation:(id)value{
    [self setData:value forKey:location];
}

+ (NSString *)getLocationCode{
    return [self getData:code];
}
+ (void)setLocationCode:(id)value{
    [self setData:value forKey:code];
}
+ (NSString *)getLocationFailure{
    return [self getData:faiure];
}
+ (void)setLocationFailure:(id)value{
    [self setData:value forKey:faiure];
}

+ (NSString *)getCloseLocation{
    return [self getData:closeLocation];
}
+ (void)setCloseLocation:(id)value{
    [self setData:value forKey:closeLocation];
}

+ (NSString *)getPhoneNumber{
    return [self getData:phoneNumber];
}
+ (void)setPhoneNumber:(id)value{
    [self setData:value forKey:phoneNumber];
}

+ (NSString *)getNewsCount{
    return [self getData:NewsCount];
}
+ (void)setNewsCount:(id)isNew{
    [self setData:isNew forKey:NewsCount];
}

+ (id)getCategoryKey{
    return [self getData:categoryKey];
}
+ (void)setCategoryKey:(id)value{
    [self setData:value forKey:categoryKey];
}


+ (NSString *)getPrice{
    return [self getData:price];
}
+ (void)setPrice:(id)value{
    [self setData:value forKey:price];
}

+ (NSString *)getSure{
    return [self getData:Sure];
}
+ (void)setSure:(id)value{
    [self setData:value forKey:Sure];
}

+ (NSString *)getName{
    return [self getData:name];
}
+ (void)setName:(id)value{
    [self setData:value forKey:name];
}


+ (id)getSuggestionData{
    return [self getData:SuggestionData];
}
+ (void)setSuggestionData:(id)value{
    [self setData:value forKey:SuggestionData];
}
//获取是否允许添加通讯录状态
+ (BOOL)getAllowAddContactStatus{
    return [userDefault boolForKey:AllowAddContactStatus];
}
//设置是否允许添加通讯录状态
+ (void)setAllowAddContactStatus:(BOOL)value{
    [userDefault setBool:value forKey:AllowAddContactStatus];
    [userDefault synchronize];
}
//获取通讯录状态
+ (BOOL)getContactStatus{
    return [userDefault boolForKey:ContactStatus];
}
//设置通讯录状态
+ (void)setContactStatus:(BOOL)value{
    [userDefault setBool:value forKey:ContactStatus];
    [userDefault synchronize];
}

//临时数据
+ (BOOL)getLoginStutas{
    return [userDefault boolForKey:LoginStutas];
}
//临时数据
+ (void)setLoginStutas:(BOOL)value{
    [userDefault setBool:value forKey:LoginStutas];
    [userDefault synchronize];
}

//获取通讯录状态
+ (BOOL)getContactStutas{
    return [userDefault boolForKey:ContactStatus];
}
//设置通讯录状态
+ (void)setContactStutas:(BOOL)value{
    [userDefault setBool:value forKey:ContactStatus];
    [userDefault synchronize];
}


+ (id)getPersonInfo:(NSString *)personId{
    NSString *personDataStr = [NSString stringWithFormat:@"personId%@",personId];
    return [self getData:personDataStr];
}

+ (void)setPersonInfo:(NSString *)personId andData:(NSData *)data{
    NSString *personDataStr = [NSString stringWithFormat:@"personId%@",personId];
    [userDefault setObject:data forKey:personDataStr];
    [userDefault synchronize];
}

//获取个人话题缓存信息
+ (id)getPersonTopic:(NSString *)personId{
    NSString *personDataStr = [NSString stringWithFormat:@"personTopic%@",personId];
    return [self getData:personDataStr];
}
//设置个人话题缓存信息
+ (void)setPersonTopic:(NSString *)personId andData:(NSData *)data{
    NSString *personDataStr = [NSString stringWithFormat:@"personTopic%@",personId];
    NSString *needCacheStr = [NSString stringWithFormat:@"%@,",personDataStr];
    NSString *oldCacheStr = [userDefault objectForKey:@"cachepersonTopicIDs"];
    NSString *newCacheStr;
    if (oldCacheStr) {
        newCacheStr = [NSString stringWithFormat:@"%@%@",oldCacheStr,needCacheStr];
    }else{
        newCacheStr = needCacheStr;
    }
    [userDefault setObject:newCacheStr forKey:@"cachepersonTopicIDs"];
    [userDefault setObject:data forKey:personDataStr];
    [userDefault synchronize];
}

//获取个人收到的评价缓存信息
+ (id)getPersonReceive:(NSString *)personId{
    NSString *personDataStr = [NSString stringWithFormat:@"personReceive%@",personId];
    return [self getData:personDataStr];
}
//设置个人收到的评价缓存信息
+ (void)setPersonReceive:(NSString *)personId andData:(NSData *)data{
    NSString *personDataStr = [NSString stringWithFormat:@"personReceive%@",personId];
    NSString *needCacheStr = [NSString stringWithFormat:@"%@,",personDataStr];
    NSString *oldCacheStr = [userDefault objectForKey:@"cachepersonReceiveCommentIDs"];
    NSString *newCacheStr;
    if (oldCacheStr) {
        newCacheStr = [NSString stringWithFormat:@"%@%@",oldCacheStr,needCacheStr];
    }else{
        newCacheStr = needCacheStr;
    }
    [userDefault setObject:newCacheStr forKey:@"cachepersonReceiveCommentIDs"];
    [userDefault setObject:data forKey:personDataStr];
    [userDefault synchronize];
}


@end
