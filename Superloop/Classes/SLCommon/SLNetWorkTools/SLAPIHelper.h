//
//  SLAPIHelper.h
//  Superloop
//
//  Created by Daniel Zhao on 16/5/20.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SLHttpRequest.h"
#import "SLHttpRequestErrorHandler.h"
#import "SLHttpRequestError.h"

@interface SLAPIHelper : NSObject

@property(nonatomic,assign)NSInteger methodType;
+ (void)requestWithUrl:(NSString *)url
                headers:(NSDictionary *)headers
                params:(NSDictionary *)params
                method:(SLHttpMethod)method
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure;

+ (void)requestHandleErrorWithUrl:(NSString *)url
               headers:(NSDictionary *)headers
                params:(NSDictionary *)params
                method:(SLHttpMethod)method
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure;

// 登录
+(void)login:(NSDictionary *)params
             success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure;
// 获取验证码
+(void)getSMSCode:(NSMutableDictionary *)parameters
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure;

// 获取注册时的验证码
+(void)getSMSCodeForRegister:(NSDictionary *)dict
                     success:(SLHttpRequestSuccessBlock)success
                     failure:(SLHttpRequestFailureCodeBlock)failure;
// 注册
+(void)Register:(NSDictionary *)params
                     success:(SLHttpRequestSuccessBlock)success
                     failure:(SLHttpRequestFailureCodeBlock)failure;

// 重置密码
+(void)resetPwd:(NSDictionary *)params
        success:(SLHttpRequestSuccessBlock)success
        failure:(SLHttpRequestFailureCodeBlock)failure;

//获取搜索的tag值
+(void)getTagValue:(NSDictionary *)params
        success:(SLHttpRequestSuccessBlock)success
        failure:(SLHttpRequestFailureCodeBlock)failure;

//获取首页轮播图数据
+(void)getHomeBannerValue:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure;
// 添加标签
+(void)addTags:(NSString *)tags
                 tagsType:(NSNumber *)tagsType
                  success:(SLHttpRequestSuccessBlock)success
                  failure:(SLHttpRequestFailureCodeBlock)failure;

// 获取标签数据
+(void)getTagsValue:(NSDictionary *)params
       success:(SLHttpRequestSuccessBlock)success
       failure:(SLHttpRequestFailureCodeBlock)failure;

//获取所有标签数据
+(void)getAllTagsValue:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;

// 搜索用户
+(void)searchUsers:(NSDictionary *)params
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure;
// 搜索话题
+(void)searchTopics:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure;
//点赞
+(void)dGood:(NSDictionary *)params
             method:(SLHttpMethod)method
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;
// 获取黑名单数据
+(void)getBlackList:(NSDictionary *)params
     success:(SLHttpRequestSuccessBlock)success
     failure:(SLHttpRequestFailureCodeBlock)failure;

 //取消拉黑或者取消拉黑该用户
+(void)cancelBlack:(NSDictionary *)params
             method:(SLHttpMethod)method
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;
// 获取用户粉丝
+(void)userFollows:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure;
//获取通话详情
+(void)getCommunicateDetail:(NSString *)logToken
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure;
// 打电话功能
+(void)phoneCall:(NSDictionary *)params
       phoneNum :(NSString *)phoneNum
         success:(SLHttpRequestSuccessBlock)success
         failure:(SLHttpRequestFailureCodeBlock)failure;

// 评论打电话功能
+(void)commentForCalled:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure;

// 通话记录
+(void)callLogs:(NSDictionary *)params
                success:(SLHttpRequestSuccessBlock)success
                failure:(SLHttpRequestFailureCodeBlock)failure;

// 申诉
+(void)complain:(NSDictionary *)params
        success:(SLHttpRequestSuccessBlock)success
        failure:(SLHttpRequestFailureCodeBlock)failure;
// 查看话题详情
+(void)topicsVisit:(NSDictionary *)params
        success:(SLHttpRequestSuccessBlock)success
        failure:(SLHttpRequestFailureCodeBlock)failure;
// suggestions
+(void)suggestions:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure;

// fans
+(void)getFollowers:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure;
// 话题相关Base型接口
+(void)getTopicsData:(NSDictionary *)params
                 url:(NSString *)url
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;

// 评论
+(void)topicsReplies:(NSDictionary *)params
              method:(SLHttpMethod)method
             success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure;
// 话题详情
+(void)getTopics:(NSString *)topicId
             success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure;

//删除话题
+ (void)deleteTopics:(NSString *)TopicsId success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure;
//收藏按钮被点击的代理事件
+(void)collectTopics:(NSString *)topicId
          method:(SLHttpMethod)method
         success:(SLHttpRequestSuccessBlock)success
         failure:(SLHttpRequestFailureCodeBlock)failure;
// 获取通话资费价格
+(void)getPriceData:(NSString *)topicId
             success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure;
//cell中的删除评论按钮被点击
+(void)deleteComment:(NSString *)commentId
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;
// 修改用户资料
+(void)resetUsers:(NSDictionary *)params
             success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure;

// 获取用户资料
+(void)getUsersData:(NSDictionary *)params
          success:(SLHttpRequestSuccessBlock)success
          failure:(SLHttpRequestFailureCodeBlock)failure;

// 微信支付
+(void)wxPay:(NSDictionary *)params
             method:(SLHttpMethod)method
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;


//验证微信的支付状态
+(void)wxPayVerify:(NSString *)orderId
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure;
// 支付宝获取支付参数列表
+(void)aliPay:(NSDictionary *)params
     success:(SLHttpRequestSuccessBlock)success
     failure:(SLHttpRequestFailureCodeBlock)failure;
// 验证支付宝支付状态
+(void)aliPayVerify:(NSString *)orderId
      success:(SLHttpRequestSuccessBlock)success
      failure:(SLHttpRequestFailureCodeBlock)failure;

// 设置支付密码
+(void)setDealPwd:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;

//重新设置支付密码
+(void)reSetDealPwd:(NSDictionary *)params
          success:(SLHttpRequestSuccessBlock)success
          failure:(SLHttpRequestFailureCodeBlock)failure;
// 交易记录
+(void)getUsersTransactions:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;

// 获取通话评价记录
+(void)getUsersAppraisals:(NSDictionary *)params
                    success:(SLHttpRequestSuccessBlock)success
                    failure:(SLHttpRequestFailureCodeBlock)failure;
// 删除教育信息
+(void)deleteEducation:(NSDictionary *)params
                  success:(SLHttpRequestSuccessBlock)success
                  failure:(SLHttpRequestFailureCodeBlock)failure;
// 删除工作经历
+(void)deleteWorkHistory:(NSDictionary *)params
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure;
// 个人简介
+(void)introduce:(NSDictionary *)params
                 success:(SLHttpRequestSuccessBlock)success
                 failure:(SLHttpRequestFailureCodeBlock)failure;

// 关注
+(void)getfollow:(NSDictionary *)params
         success:(SLHttpRequestSuccessBlock)success
         failure:(SLHttpRequestFailureCodeBlock)failure;
// 取消关注
+(void)cancelFollow:(NSDictionary *)params
         success:(SLHttpRequestSuccessBlock)success
         failure:(SLHttpRequestFailureCodeBlock)failure;
// 添加工作经历
+(void)addWorkHistory:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;
// 修改工作经历
+(void)reviseWorkHistory:(NSDictionary *)params
              success:(SLHttpRequestSuccessBlock)success
              failure:(SLHttpRequestFailureCodeBlock)failure;
// 添加教育经历
+(void)addEducation:(NSDictionary *)params
              success:(SLHttpRequestSuccessBlock)success
              failure:(SLHttpRequestFailureCodeBlock)failure;

// 修改教育经历
+(void)reviseEducation:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;

// 获取话题评论
+(void)getTopicComment:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;
//获取话题内容
+(void)getTopicContent:(NSDictionary *)params
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure;
// 删除话题评论
+(void)deleteTopicComment:(NSDictionary *)params
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure;
// 获取话题回复列表
+(void)getTopicsReplies:(NSDictionary *)params
                  success:(SLHttpRequestSuccessBlock)success
                  failure:(SLHttpRequestFailureCodeBlock)failure;


//设置支付宝的支付账号
+(void)setAliayNum:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure;
//他人页评价
+(void)getOtherEvaluate:(NSDictionary *)params
                success:(SLHttpRequestSuccessBlock)success
                failure:(SLHttpRequestFailureCodeBlock)failure;

//发起提现申请
+ (void)requestWithdraw:(NSDictionary *)params
                success:(SLHttpRequestSuccessBlock)success
                failure:(SLHttpRequestFailureCodeBlock)failure;

//获取提现的历史记录
+(void)getWithdrawHistorys:(NSDictionary *)params
                success:(SLHttpRequestSuccessBlock)success
                failure:(SLHttpRequestFailureCodeBlock)failure;

//获取提现的账号接口
+(void)getWithdrawAccountHistorys:(NSDictionary *)params
                           method:(SLHttpMethod)method
                          success:(SLHttpRequestSuccessBlock)success
                          failure:(SLHttpRequestFailureCodeBlock)failure;
//获取提现的详情接口
+(void)getWithdrawDetail:(NSDictionary *)params
                  method:(SLHttpMethod)method
                 success:(SLHttpRequestSuccessBlock)success
                 failure:(SLHttpRequestFailureCodeBlock)failure;
//获取超人说接口
+(void)getSuperTalk:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;
//获取最新说接口
+(void)getLastTalk:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure;

//获取代金券接口
+(void)getCashCouponsSuccess:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;
//意见反馈
+(void)suggestionFeedback:(NSDictionary *)params
                  success:(SLHttpRequestSuccessBlock)success
                  failure:(SLHttpRequestFailureCodeBlock)failure;
//认证审核
+(void)certification:(NSDictionary *)params
             success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure;

//获取消息有无帮助的状态
+(void)getHelpOrNot:(NSDictionary *)params
         success:(SLHttpRequestSuccessBlock)success
         failure:(SLHttpRequestFailureCodeBlock)failure;
//上传消息有无帮助
+(void)postHelpOrNot:(NSDictionary *)params
             success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure;
//支付
+(void)postPay:(NSDictionary *)params
       success:(SLHttpRequestSuccessBlock)success
       failure:(SLHttpRequestFailureCodeBlock)failure;
//消息中获取支付状态
+(void)getMessagePayStatus:(NSString *)str
                   success:(SLHttpRequestSuccessBlock)success
                   failure:(SLHttpRequestFailureCodeBlock)failure;

//发布话题
+(void)publishTopic:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure;

//获取我的新消息提醒
+(void)getMessageRemindsuccess:(SLHttpRequestSuccessBlock)success
                       failure:(SLHttpRequestFailureCodeBlock)failure;
//获取系统通知
+(void)getSystemNotice:(NSDictionary *)params
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure;
//获取获取AES加密密钥
+(void)getAESSecurity:(NSDictionary *)params
              success:(SLHttpRequestSuccessBlock)success
              failure:(SLHttpRequestFailureCodeBlock)failure;
//获取是否有更新
+(void)getNewVersionInfo:(NSDictionary *)params
                 success:(SLHttpRequestSuccessBlock)success
                 failure:(SLHttpRequestFailureCodeBlock)failure;
//通讯录接口
+(void)getContactInfo:(NSDictionary *)params
              success:(SLHttpRequestSuccessBlock)success
              failure:(SLHttpRequestFailureCodeBlock)failure;

//获取余额信息
+(void)getAmountInfoSuccess:(SLHttpRequestSuccessBlock)success
                    failure:(SLHttpRequestFailureCodeBlock)failure;
//回复通话评价
+(void)replyCallsEvaluate:(NSDictionary *)params
                  success:(SLHttpRequestSuccessBlock)success
                  failure:(SLHttpRequestFailureCodeBlock)failure;
//付费消息评价
+(void)msgEvaluate:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure;
//付费消息申诉
+(void)msgComplain:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure;
//回复付费消息评价
+(void)replyMsgEvaluate:(NSDictionary *)params
                success:(SLHttpRequestSuccessBlock)success
                failure:(SLHttpRequestFailureCodeBlock)failure;
//启动图
+(void)getSplashSuccess:(SLHttpRequestSuccessBlock)success
                failure:(SLHttpRequestFailureCodeBlock)failure;

// 获取验证码登录的验证码
+(void)getVerificationLoginCode:(NSMutableDictionary *)parameters
                        success:(SLHttpRequestSuccessBlock)success
                        failure:(SLHttpRequestFailureCodeBlock)failure;
//使用验证码登录
+(void)loginUseLoginCode:(NSMutableDictionary *)parameters
                 success:(SLHttpRequestSuccessBlock)success
                 failure:(SLHttpRequestFailureCodeBlock)failure;

//获取超人排行榜信息
+(void)getRanking:(NSDictionary *)params
          success:(SLHttpRequestSuccessBlock)success
          failure:(SLHttpRequestFailureCodeBlock)failure;
//微信登陆
+(void)loginUsingWeChat:(NSDictionary *)params
                success:(SLHttpRequestSuccessBlock)success
                failure:(SLHttpRequestFailureCodeBlock)failure;
// 获取微信绑定时的验证码
+(void)getBingDingWechatCodeForRegister:(NSDictionary *)dict
                                success:(SLHttpRequestSuccessBlock)success
                                failure:(SLHttpRequestFailureCodeBlock)failure;
+(void)weChatRegister:(NSDictionary *)dict
              success:(SLHttpRequestSuccessBlock)success
              failure:(SLHttpRequestFailureCodeBlock)failure;

+(void)weChatDel:(NSDictionary *)dict
         success:(SLHttpRequestSuccessBlock)success
         failure:(SLHttpRequestFailureCodeBlock)failure;
//修改密码时进行验证码验证
+(void)verificationCodeIsValid:(NSDictionary *)params
                       success:(SLHttpRequestSuccessBlock)success
                       failure:(SLHttpRequestFailureCodeBlock)failure;
@end
