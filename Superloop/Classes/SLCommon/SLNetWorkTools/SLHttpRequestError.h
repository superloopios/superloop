//
//  SLHttpRequestFailure.h
//  Superloop
//
//  Created by WangS on 16/6/6.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLHttpRequestError : NSObject

@property (nonatomic,assign)NSInteger httpStatusCode;

@property (nonatomic,assign)NSInteger slAPICode;

@end
