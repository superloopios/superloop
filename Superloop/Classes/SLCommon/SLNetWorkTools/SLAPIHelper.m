//
//  SLAPIHelper.m
//  Superloop
//
//  Created by Daniel Zhao on 16/5/20.
//  Copyright © 2016年 Superloop. All rights reserved.
//  网络请求工具类

#import "SLAPIHelper.h"
#import "AppDelegate.h"
//#import "SLHttpRequestFailure.h"

@implementation SLAPIHelper

+ (void)requestWithUrl:(NSString *)url
               headers:(NSMutableDictionary *)headers
                params:(NSDictionary *)params
                method:(SLHttpMethod)method
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure{
    
  
    
    // 设置头信息
    if (ApplicationDelegate && ApplicationDelegate.Basic) {
        if (!headers) {
            headers = [[NSMutableDictionary alloc]init];
        }
        [headers setValue:[NSString stringWithFormat:@"Basic %@",ApplicationDelegate.Basic] forKey:@"Authorization"];
//        NSLog(@"%@",[NSString stringWithFormat:@"Basic %@",ApplicationDelegate.Basic]);
    }
    
    
    [SLHttpRequest requestWhitUrl:url
                          headers:headers
                           params:params
                           method:method success:^(NSURLSessionDataTask *task,  NSDictionary *data){
        
                               if (success){
                                   success(task, data);
                               }                           }
                          failure:^(NSDictionary *data, NSInteger statusCode) {
                              // 业务层状态码
                              NSLog(@"%@",data);
                              NSInteger code = -1;
                              if (data) {
                                  code =[data[@"code"] integerValue];
                              }
                             
                              if (failure) {
                                  SLHttpRequestError *errorResult=[[SLHttpRequestError alloc] init];
                                  errorResult.httpStatusCode=statusCode;
                                  errorResult.slAPICode=code;
                                  failure(errorResult);
                              }
                      
                          }];

}


+ (void)requestHandleErrorWithUrl:(NSString *)url
               headers:(NSMutableDictionary *)headers
                params:(NSDictionary *)params
                method:(SLHttpMethod)method
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure {

    [self requestWithUrl:url headers:headers params:params method:method success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
           if (success){
               success(task, data);
           }
        
    } failure:^(SLHttpRequestError *error) {
        
              switch (error.httpStatusCode) {
                  case 403:
                      [SLHttpRequestErrorHandler Error403:error.slAPICode];
                      break;
                  case 404:
                      [SLHttpRequestErrorHandler Error404:error.slAPICode];
                      break;
                  case 400:
                      [SLHttpRequestErrorHandler Error400:error.slAPICode];
                       break;
                  case 409:
                      [SLHttpRequestErrorHandler Error409:error.slAPICode];
                       break;
                  case 500:
                      [SLHttpRequestErrorHandler Error500:error.slAPICode];
                       break;
                  default:
                      break;
              }
              if (failure){
                  failure(error);
            }
     
    }];
}

+(void)login:(NSDictionary *)params
     success:(SLHttpRequestSuccessBlock)success
     failure:(SLHttpRequestFailureCodeBlock) failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/auth/classic"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}

// 获取验证码登录的验证码
+(void)getVerificationLoginCode:(NSMutableDictionary *)parameters
                        success:(SLHttpRequestSuccessBlock)success
                        failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/auth/verification-code"];
    [self requestHandleErrorWithUrl:url headers:nil params:parameters method:Get success:success failure:failure];
}
+(void)loginUseLoginCode:(NSMutableDictionary *)parameters
                        success:(SLHttpRequestSuccessBlock)success
                        failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/auth/verification-code"];
    [self requestHandleErrorWithUrl:url headers:nil params:parameters method:Post success:success failure:failure];
}
+(void)getSMSCode:(NSMutableDictionary *)parameters
          success:(SLHttpRequestSuccessBlock)success
          failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/reset_password"];
    
    //NSDictionary *parameters =  @{@"cellphone":[NSString stringWithFormat:@"%@", phoneNum]};
    // NSString *URL=[url stringByAppendingString:[NSString stringWithFormat:@"?cellphone=%@",phoneNum]];
    [self requestHandleErrorWithUrl:url headers:nil params:parameters method:Get success:success failure:failure];
}

// 获取注册时的验证码
+(void)getSMSCodeForRegister:(NSDictionary *)dict
                     success:(SLHttpRequestSuccessBlock)success
                     failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/register/verification-code"];
    [self requestHandleErrorWithUrl:url headers:nil params:dict method:Get success:success failure:failure];
}
// 获取微信绑定时的验证码
+(void)getBingDingWechatCodeForRegister:(NSDictionary *)dict
                     success:(SLHttpRequestSuccessBlock)success
                     failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/wechat/verification-code"];
    [self requestHandleErrorWithUrl:url headers:nil params:dict method:Get success:success failure:failure];
}
// 微信绑定
+(void)weChatRegister:(NSDictionary *)dict
                                success:(SLHttpRequestSuccessBlock)success
                                failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/auth/wechat/binding"];
    [self requestHandleErrorWithUrl:url headers:nil params:dict method:Post success:success failure:failure];
}

// 注册
+(void)Register:(NSDictionary *)params
        success:(SLHttpRequestSuccessBlock)success
        failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/register/verification-code"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
    
}
// 重置密码
+(void)resetPwd:(NSDictionary *)params
        success:(SLHttpRequestSuccessBlock)success
        failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/reset_password"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
    
}
//获取搜索的tag值
+(void)getTagValue:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/misc/tags/navigations"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
    
}
//获取首页轮播图数据
+(void)getHomeBannerValue:(NSDictionary *)params
                  success:(SLHttpRequestSuccessBlock)success
                  failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/misc/index/navigations"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}
// 添加标签
+(void)addTags:(NSString *)tags
      tagsType:(NSNumber *)tagsType
       success:(SLHttpRequestSuccessBlock)success
       failure:(SLHttpRequestFailureCodeBlock)failure{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"ids"] = tags;
    parameters[@"type"] = tagsType;
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/tags"];
    [self requestHandleErrorWithUrl:url headers:nil params:parameters method:Post success:success failure:failure];
    
}


// 获取标签数据
+(void)getTagsValue:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *userId =[NSString stringWithFormat:@"%@",ApplicationDelegate.userId];
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/tags"];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",url,userId];
    [self requestHandleErrorWithUrl:urlString headers:nil params:params method:Get success:success failure:failure];
}

//获取所有标签数据
+(void)getAllTagsValue:(NSDictionary *)params
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/misc/tags"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
    
}

//获取系统通知
+(void)getSystemNotice:(NSDictionary *)params
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/notices"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
    
}


// 搜索用户
+(void)searchUsers:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/search/users"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

// 搜索话题
+(void)searchTopics:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/search/topics"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}
// 点赞
+(void)dGood:(NSDictionary *)params
      method:(SLHttpMethod)method
     success:(SLHttpRequestSuccessBlock)success
     failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/topics/%@/thumbups", params[@"id"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:method success:success failure:failure];
}

// 获取黑名单数据
+(void)getBlackList:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/block_list"];

    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
    
}

//取消拉黑或者取消拉黑该用户
+(void)cancelBlack:(NSDictionary *)params
            method:(SLHttpMethod)method
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@/block", params[@"id"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:method success:success failure:failure];
}

// 获取粉丝
+(void)userFollows:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure{
   
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat: @"/users/%@/follows", params[@"id"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

//获取通话详情
+(void)getCommunicateDetail:(NSString *)logToken
                    success:(SLHttpRequestSuccessBlock)success
                    failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat: @"/users/calls/logs/%@", logToken]];
    [self requestWithUrl:url headers:nil params:nil method:Get success:success failure:failure];

}

// 打电话功能
+(void)phoneCall:(NSDictionary *)params
           phoneNum :(NSString *)phoneNum
         success:(SLHttpRequestSuccessBlock)success
         failure:(SLHttpRequestFailureCodeBlock)failure{
    
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat: @"/users/calls/call/%@", phoneNum]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}

// 评论打电话功能
+(void)commentForCalled:(NSDictionary *)params
                success:(SLHttpRequestSuccessBlock)success
                failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/calls/%@/appraisal", params[@"callId"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}

// 通话记录
+(void)callLogs:(NSDictionary *)params
        success:(SLHttpRequestSuccessBlock)success
        failure:(SLHttpRequestFailureCodeBlock)failure{
    
      NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/calls/logs"];
     [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

// 申诉
+(void)complain:(NSDictionary *)params
        success:(SLHttpRequestSuccessBlock)success
        failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/calls/%@/complain",params[@"callId"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}
// 查看话题详情
+(void)topicsVisit:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/topics/%@/visit", params[@"topicId"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}
// suggestions
+(void)suggestions:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/popular"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

// 粉丝
+(void)getFollowers:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure{
    
     NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@/followers", params[@"id"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

// 话题相关Base型接口
+(void)getTopicsData:(NSDictionary *)params
                 url:(NSString *)url
             success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure{
     [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

// 评论
+(void)topicsReplies:(NSDictionary *)params
              method:(SLHttpMethod)method
             success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/topics/%@/replies", params[@"id"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:method success:success failure:failure];
}
// 话题详情
+(void)getTopics:(NSString *)topicId
         success:(SLHttpRequestSuccessBlock)success
         failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/topics/%@", topicId]];
    [self requestHandleErrorWithUrl:url headers:nil params:nil method:Get success:success failure:failure];
}
//删除话题
+ (void)deleteTopics:(NSString *)TopicsId success:(SLHttpRequestSuccessBlock)success failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/topics/%@", TopicsId]];
    [self requestHandleErrorWithUrl:url headers:nil params:nil method:Delete success:success failure:failure];
}

//收藏按钮被点击的代理事件
+(void)collectTopics:(NSString *)topicId
              method:(SLHttpMethod)method
             success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/topics/%@/collect", topicId]];
    [self requestHandleErrorWithUrl:url headers:nil params:nil method:method success:success failure:failure];
}
// 获取通话资费价格
+(void)getPriceData:(NSString *)topicId
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@", topicId]];
    [self requestHandleErrorWithUrl:url headers:nil params:nil method:Get success:success failure:failure];
}
//cell中的删除评论按钮被点击
+(void)deleteComment:(NSString *)commentId
             success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/replies/%@", commentId]];
    [self requestHandleErrorWithUrl:url headers:nil params:nil method:Delete success:success failure:failure];
}
// 修改用户资料
+(void)resetUsers:(NSDictionary *)params
          success:(SLHttpRequestSuccessBlock)success
          failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Put success:success failure:failure];
}

// 获取用户资料
+(void)getUsersData:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@", params[@"id"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];

}

//获取代金券接口
+(void)getCashCouponsSuccess:(SLHttpRequestSuccessBlock)success
                     failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/vouchers"];
    [self requestHandleErrorWithUrl:url headers:nil params:nil method:Get success:success failure:failure];
}

// 微信支付
+(void)wxPay:(NSDictionary *)params
      method:(SLHttpMethod)method
     success:(SLHttpRequestSuccessBlock)success
     failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/wepay/app/request"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:method success:success failure:failure];
    
}
//验证微信的支付状态
+(void)wxPayVerify:(NSString *)orderId
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/wepay/request/verify/%@", orderId]];
    [self requestHandleErrorWithUrl:url headers:nil params:nil method:Get success:success failure:failure];
}

// 支付宝获取支付参数列表
+(void)aliPay:(NSDictionary *)params
      success:(SLHttpRequestSuccessBlock)success
      failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/alipay/app/request"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}

// 验证支付状态
+(void)aliPayVerify:(NSString *)orderId
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/alipay/request/verify/%@", orderId]];
    [self requestHandleErrorWithUrl:url headers:nil params:nil method:Get success:success failure:failure];
}


// 设置支付密码
+(void)setDealPwd:(NSDictionary *)params
          success:(SLHttpRequestSuccessBlock)success
          failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/deal_password"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}
//重新设置支付密码
+(void)reSetDealPwd:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/reset_deal_password"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

// 交易记录
+(void)getUsersTransactions:(NSDictionary *)params
                    success:(SLHttpRequestSuccessBlock)success
                    failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/transactions"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

// 获取通话评价记录
+(void)getUsersAppraisals:(NSDictionary *)params
                  success:(SLHttpRequestSuccessBlock)success
                  failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@/appraisals", params[@"userId"]]];
    
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}
// 删除教育信息
+(void)deleteEducation:(NSDictionary *)params
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/educations/%@", params[@"id"]]];
    
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Delete success:success failure:failure];

}
// 删除工作经历
+(void)deleteWorkHistory:(NSDictionary *)params
                 success:(SLHttpRequestSuccessBlock)success
                 failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/works/%@", params[@"id"]]];
    
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Delete success:success failure:failure];
}
// 个人简介
+(void)introduce:(NSDictionary *)params
         success:(SLHttpRequestSuccessBlock)success
         failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Put success:success failure:failure];
}

// 关注
+(void)getfollow:(NSDictionary *)params
         success:(SLHttpRequestSuccessBlock)success
         failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@/follow", params[@"user_id"]]];
    
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}
// 取消关注
+(void)cancelFollow:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@/unfollow", params[@"user_id"]]];
    
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}
// 添加工作经历
+(void)addWorkHistory:(NSDictionary *)params
              success:(SLHttpRequestSuccessBlock)success
              failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/works"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
    
}

// 修改工作经历
+(void)reviseWorkHistory:(NSDictionary *)params
              success:(SLHttpRequestSuccessBlock)success
              failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/works/%@",params[@"id"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Put success:success failure:failure];
    
}


// 添加教育经历
+(void)addEducation:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/educations"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}
// 修改教育经历
+(void)reviseEducation:(NSDictionary *)params
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/educations/%@",params[@"id"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Put success:success failure:failure];
}

// 获取话题评论
+(void)getTopicComment:(NSDictionary *)params
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/topics/replies"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

//获取话题内容
+(void)getTopicContent:(NSDictionary *)params
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/topics/%@", params[@"id"]]];
    
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}
// 删除话题评论
+(void)deleteTopicComment:(NSDictionary *)params
                   success:(SLHttpRequestSuccessBlock)success
                   failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/replies/%@", params[@"id"]]];
    
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Delete success:success failure:failure];
}
// 获取话题回复列表
+(void)getTopicsReplies:(NSDictionary *)params
                success:(SLHttpRequestSuccessBlock)success
                failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/messages"];
    
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

//设置支付宝的支付账号
+(void)setAliayNum:(NSDictionary *)params
                success:(SLHttpRequestSuccessBlock)success
                failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/withdraw/account"];
    
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}



//他人页评价
+(void)getOtherEvaluate:(NSDictionary *)params
           success:(SLHttpRequestSuccessBlock)success
           failure:(SLHttpRequestFailureCodeBlock)failure{
    
//    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat: @"/users/%@/call/appraisals?type=1&page=1", params[@"id"]]];
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat: @"/users/%@/call/appraisals", params[@"userId"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

//发起提现申请
+(void)requestWithdraw:(NSDictionary *)params
                success:(SLHttpRequestSuccessBlock)success
                failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat: @"/withdraw/request"]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}

//获取提现的历史记录
+(void)getWithdrawHistorys:(NSDictionary *)params
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat: @"/users/withdraw/request/history"]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}


//获取提现的账号接口
+(void)getWithdrawAccountHistorys:(NSDictionary *)params
                    method:(SLHttpMethod)method
                   success:(SLHttpRequestSuccessBlock)success
                   failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat: @"/withdraw/account"]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:method success:success failure:failure];
}

//获取提现的详情接口
+(void)getWithdrawDetail:(NSDictionary *)params
                           method:(SLHttpMethod)method
                          success:(SLHttpRequestSuccessBlock)success
                          failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat: @"/withdraw/request/%@",params[@"tradeNo"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:method success:success failure:failure];
}

//获取超人说接口
+(void)getSuperTalk:(NSDictionary *)params
                 success:(SLHttpRequestSuccessBlock)success
                 failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/follows/topics"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

//获取最新说接口
+(void)getLastTalk:(NSDictionary *)params
                   success:(SLHttpRequestSuccessBlock)success
                   failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/topics"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}
//意见反馈
+(void)suggestionFeedback:(NSDictionary *)params
     success:(SLHttpRequestSuccessBlock)success
     failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/feedback"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}
//认证审核
+(void)certification:(NSDictionary *)params
                  success:(SLHttpRequestSuccessBlock)success
                  failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/verification"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}

//获取消息有无帮助的状态
+(void)getHelpOrNot:(NSDictionary *)params
             success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url =[URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/messages/%@/payment/status", params[@"messageId"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

//上传消息有无帮助
+(void)postHelpOrNot:(NSDictionary *)params
            success:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url =[URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/messages/%@/worth", params[@"messageId"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}
//支付
+(void)postPay:(NSDictionary *)params
             success:(SLHttpRequestSuccessBlock)success
             failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url =[URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/messages/%@/payment", params[@"messageId"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}

//消息中获取支付状态
+(void)getMessagePayStatus:(NSString *)str
       success:(SLHttpRequestSuccessBlock)success
       failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url =[URL_ROOT_PATIENT
                    stringByAppendingString:[NSString stringWithFormat:@"/messages/%@/payment/status", str]];
    [self requestHandleErrorWithUrl:url headers:nil params:nil method:Get success:success failure:failure];
}

//发布话题
+(void)publishTopic:(NSDictionary *)params
                   success:(SLHttpRequestSuccessBlock)success
                   failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url =[URL_ROOT_PATIENT
                    stringByAppendingString:[NSString stringWithFormat:@"/topics"]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}
//获取我的新消息提醒
+(void)getMessageRemindsuccess:(SLHttpRequestSuccessBlock)success
            failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url =[URL_ROOT_PATIENT
                    stringByAppendingString:[NSString stringWithFormat:@"/users/messages/remind"]];
    [self requestHandleErrorWithUrl:url headers:nil params:nil method:Get success:success failure:failure];
}

//获取获取AES加密密钥
+(void)getAESSecurity:(NSDictionary *)params
              success:(SLHttpRequestSuccessBlock)success
                       failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url =[URL_ROOT_PATIENT
                    stringByAppendingString:[NSString stringWithFormat:@"/misc/security/token"]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}

//获取更新信息
+(void)getNewVersionInfo:(NSDictionary *)params
               success:(SLHttpRequestSuccessBlock)success
               failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/misc/clients/updates"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
    
}
//通讯录接口
+(void)getContactInfo:(NSDictionary *)params
                 success:(SLHttpRequestSuccessBlock)success
                 failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/misc/permission/contact/status"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

//获取余额信息
+(void)getAmountInfoSuccess:(SLHttpRequestSuccessBlock)success
              failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/amount"];
    [self requestHandleErrorWithUrl:url headers:nil params:nil method:Get success:success failure:failure];
}
//回复通话评价
+(void)replyCallsEvaluate:(NSDictionary *)params
                    success:(SLHttpRequestSuccessBlock)success
                    failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/calls/appraisal/%@/reply",params[@"appraisal_id"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}
//付费消息评价
+(void)msgEvaluate:(NSDictionary *)params
                  success:(SLHttpRequestSuccessBlock)success
                  failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/messages/%@/appraisal",params[@"messageId"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}
//付费消息申诉
+(void)msgComplain:(NSDictionary *)params
        success:(SLHttpRequestSuccessBlock)success
        failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/messages/%@/complain",params[@"messageId"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}
//回复付费消息评价
+(void)replyMsgEvaluate:(NSDictionary *)params
                  success:(SLHttpRequestSuccessBlock)success
                  failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/messages/appraisal/%@/reply",params[@"appraisal_id"]]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}
//启动图
+(void)getSplashSuccess:(SLHttpRequestSuccessBlock)success
                failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/misc/splash"];
    [self requestHandleErrorWithUrl:url headers:nil params:nil method:Get success:success failure:failure];
}
//获取超人排行榜信息
+(void)getRanking:(NSDictionary *)params
                success:(SLHttpRequestSuccessBlock)success
                failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/leader-boards"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}


/**
 微信登录

 {
	action = 3  //进入绑定手机
	social_account = {
	type = 1
	openid = oUiPGvuvyjlPhkTOvKOhqLgWFNC0
    }
 }
 */
+(void)loginUsingWeChat:(NSDictionary *)params
          success:(SLHttpRequestSuccessBlock)success
          failure:(SLHttpRequestFailureCodeBlock)failure{
    
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/auth/wechat"];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Post success:success failure:failure];
}

// 微信绑定解除
+(void)weChatDel:(NSDictionary *)dict
              success:(SLHttpRequestSuccessBlock)success
              failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url = [URL_ROOT_PATIENT stringByAppendingString:@"/users/wechat"];
    [self requestHandleErrorWithUrl:url headers:nil params:dict method:Delete success:success failure:failure];
}
//修改密码时进行验证码验证
+(void)verificationCodeIsValid:(NSDictionary *)params
              success:(SLHttpRequestSuccessBlock)success
              failure:(SLHttpRequestFailureCodeBlock)failure{
    NSString *url =[URL_ROOT_PATIENT
                    stringByAppendingString:[NSString stringWithFormat:@"/users/checkSMSCode"]];
    [self requestHandleErrorWithUrl:url headers:nil params:params method:Get success:success failure:failure];
}

@end
