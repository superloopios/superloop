//
//  SLHttpReqeustError.m
//  Superloop
//
//  Created by Daniel Zhao on 16/5/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//   网络请求错误处理

#import "SLHttpRequestErrorHandler.h"
#import "SLFirstViewController.h"
#import "SLNavgationViewController.h"
#import "AppDelegate.h"
#import "SLMessageManger.h"
#import "SLUserDefault.h"
@interface SLHttpRequestErrorHandler ()<UIAlertViewDelegate>
@end
@implementation SLHttpRequestErrorHandler

+(void)Error500:(NSInteger)code{
    switch (code) {
        case 14:
            [HUDManager showWarningWithText:@"服务器内部错误"];

            break;
           
        default:
           // [HUDManager showWarningWithText:@"网络出错,请检查网络状况"];

            break;
    }
 
}

#pragma mark --- 清理状态
+ (void)clearAllUserDefaultsData
{
    
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSDictionary* dict = [defs dictionaryRepresentation];
    for(id key in dict) {
        if (![key isEqualToString:appIsFirst] && ![key isEqualToString:strAccountName]&& ![key isEqualToString:SLNetWorkStatus] &&![key isEqualToString:strAccout]&&![key isEqualToString:@"VoiceType"]&&![key isEqualToString:location]&&![key isEqualToString:@"guideSave"]&&![key isEqualToString:ContactStatus]&&![key isEqualToString:LoginStutas]&&![key isEqualToString:AllowAddContactStatus]) {
            
            [defs removeObjectForKey:key];
        }
    }
    [defs synchronize];
}

// HTTP错误代码403
+(void)Error403:(NSInteger)code{
    switch (code) {
        case 4:
        {
            /// 手动登出、被踢、自动连接失败，都退出到登录页面
            NSDictionary *loginMsg=[SLUserDefault getUserInfo];
            NSString *titleMsg=[[NSString alloc] init];
            NSString *msg=[[NSString alloc] init];
            if (loginMsg.count>0) {
                [SLUserDefault removeUserInfo];
                ApplicationDelegate.Basic = NULL;
                ApplicationDelegate.userInformation = NULL;
                ApplicationDelegate.userId = NULL;
                titleMsg=@"下线通知，您的账号被迫下线或已在其他设备登录";
                [self clearAllUserDefaultsData];  //清除沙盒路径下面的东西
                [[NSNotificationCenter defaultCenter] postNotificationName:@"exitSuperLoop" object:nil];
                [[SLMessageManger sharedInstance] callThisBeforeISVAccountLogout]; // 退出IM
                SLFirstViewController *loginVc = [[SLFirstViewController alloc] init];
                SLNavgationViewController *nav = [[SLNavgationViewController alloc] initWithRootViewController:loginVc];
                [UIApplication sharedApplication].keyWindow.rootViewController = nav;
            }else{
                [SLUserDefault removeUserInfo];
                ApplicationDelegate.Basic = NULL;
                ApplicationDelegate.userInformation = NULL;
                ApplicationDelegate.userId = NULL;
                titleMsg=@"登录通知，您还未登录，请登录后再操作";
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:titleMsg message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"好", nil];
                [alertView show];
            });
            break;
        }
        case 5:
            [self alert:@"用户还没有激活!"];

            break;
        case 206: // 登录，手机号密码错误
            
//            [self alert:@"手机号或密码错误!"];
            break;
        case 207: // 登录，手机号密码错误
            
//            [self alert:@"手机号或密码错误!"];
            break;
    }
}




// HTTP错误代码404
+(void)Error404:(NSInteger)code{
    switch (code) {
        case 3:   // 请求id不存在
            break;
        case 12: // 未找到
            break;
    }
}

+(void)Error400:(NSInteger)code{
   
    switch (code) {
        case 1:   //
            
//            [self alert:@"参数无效!"];
            break;
        case 2:   //
            
//            [self alert:@"缺失必要文件!"];
            break;
        case 9:   //
            
            [self alert:@"短信验证码错误或已过期！"];
            break;
        case 101:   //
            
            [self alert:@"手机号未注册"];
            break;
//        case 501:   //
//            
//           [self alert:@"余额不足"];
//            break;
        case 601:   //
            
            [self alert:@"该消息已支付"];
            break;
        case 602:   //
            
            [self alert:@"尚未设置支付密码"];
            break;
//        case 603:   //
//            
//            [self alert:@"交易密码不正确"];
//            break;
        case 611:   //
            
            [self alert:@"新密码与旧密码一致"];
            break;
        case 612:   //
            
            [self alert:@"旧密码错误"];
            break;
        }
    
}

+(void)Error409:(NSInteger)code{
    
    switch (code) {
        case 7:   //
            
            [self alert:@"昵称已经存在！"];
            break;
        
        case 8:   //
            
            [self alert:@"手机号已经存在！"];
            break;
    
        case 11:   //
            
            [self alert:@"操作失败！"];
            break;
        case 35:   //
            
            [self alert:@"操作失败\n对方在你的黑名单中"];
            break;
        case 34:   //
           
            [self alert:@"操作失败\n你在对方的黑名单中"];
            break;
        case 423:
            
            [self alert:@"你已设置过账号！"];
            break;
            
     }
}


+(void)alert:(NSString *)message{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:nil cancelButtonTitle:@"好" otherButtonTitles: nil];
    [alertView show];
}


@end
