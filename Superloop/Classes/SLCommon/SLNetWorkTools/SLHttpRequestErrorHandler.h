//
//  SLHttpReqeustError.h
//  Superloop
//
//  Created by Daniel Zhao on 16/5/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface SLHttpRequestErrorHandler : NSObject

+(void)Error403:(NSInteger)code;

+(void)Error404:(NSInteger)code;

+(void)Error400:(NSInteger)code;
+(void)Error409:(NSInteger)code;
+(void)Error500:(NSInteger)code;
+(void)alert:(NSString *)message;
@end
