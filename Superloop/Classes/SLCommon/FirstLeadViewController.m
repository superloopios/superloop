//
//  FirstLeadViewController.m
//  playboy
//
//  Created by 张梦川 on 16/5/3.
//  Copyright © 2016年 yaoyu. All rights reserved.
//

#import "FirstLeadViewController.h"
#include <CoreGraphics/CoreGraphics.h>
#include <ImageIO/ImageIOBase.h>
#import  <ImageIO/ImageIO.h>
#import "SLUserDefault.h"
#import "SLFirstViewController.h"
#import "SLNavgationViewController.h"
#import "SLTabBarViewController.h"

@interface FirstLeadViewController ()<UIScrollViewDelegate>
@property(nonatomic,strong)UIButton *jumpBtn;
@property(nonatomic,strong)UIScrollView * jumpView;
@property(nonatomic,strong)UIImageView *imgView;
@property(nonatomic,strong)dispatch_source_t _timer;
@property (nonatomic,weak)UIImageView *lightBubleImageView;
@property (nonatomic,strong)UIPageControl *page;  //启动页的pageControl
@property (nonatomic,strong) UIButton *btn; //跳转第一次的3张图片
@end

@implementation FirstLeadViewController{
    UIScrollView *_scrollView;
    UIPageControl *_page;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    _scrollView = [[UIScrollView alloc]init];
    _scrollView.frame = [UIScreen mainScreen].bounds;
    _scrollView.delegate = self;
    _scrollView.contentSize =  CGSizeMake(WIDTH * 5, HEIGHT);
    _scrollView.pagingEnabled = true;
    _scrollView.showsHorizontalScrollIndicator = false;
    _scrollView.showsVerticalScrollIndicator = false;
    _scrollView.scrollsToTop = false;
    _scrollView.contentOffset = CGPointZero;
    
    UIPageControl *page = [[UIPageControl alloc]init];
    for (int i = 0; i< 5; i++) {
        NSString *imgName = [NSString stringWithFormat:@"Guide%d",i+1];
        UIImage *img = [UIImage imageNamed:imgName];
        UIImageView *imgView = [[UIImageView alloc]initWithImage:img];
        imgView.frame = CGRectMake(WIDTH *i, 0, WIDTH, HEIGHT);
        [_scrollView addSubview:imgView];
    }
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(WIDTH *4, 0, WIDTH, HEIGHT);
    [btn addTarget:self action:@selector(touchInAction) forControlEvents:UIControlEventTouchUpInside];
    self.btn = btn;
    [_scrollView addSubview:btn];
    [self.view addSubview:_scrollView];
    page.numberOfPages = 5;
    page.currentPage = 0;
    page.pageIndicatorTintColor= [UIColor lightGrayColor];
    page.currentPageIndicatorTintColor = UIColorFromRGB(0xff5a5f);
    _page = page;
    [self.view addSubview:_page];
    [self.view addSubview: _jumpView];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [_page mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.top.mas_equalTo(self.btn.mas_bottom).mas_offset(-20);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(10);
        
    }];
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //更新UIPageControl的当前页
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.frame;
    [_page setCurrentPage:offset.x / bounds.size.width];
   
}
//-(BOOL)prefersStatusBarHidden{
//    return true;
//}
// 点击
-(void)touchInAction{
    if ([SLUserDefault getUserInfo]) {
        SLTabBarViewController *tabVc = [[SLTabBarViewController alloc] init];
        [UIApplication sharedApplication].keyWindow.rootViewController = tabVc;
    }else{
        SLFirstViewController *indexVC = [[SLFirstViewController alloc]init];
        SLNavgationViewController *nav = [[SLNavgationViewController alloc] initWithRootViewController:indexVC];
        [UIApplication sharedApplication].keyWindow.rootViewController = nav;
        if (![SLUserDefault getAppIsFirst]){
            [SLUserDefault setAppIsFirst:true];
        }
    }
    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.x<= 0 || scrollView.contentOffset.x >= WIDTH *3) {
        scrollView.bounces = false;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = true;
}
/*
#pragma mark -- 添加gif播放的方法
- (void)addGifImage:(UIImageView *)imageView{
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"lightBuble.gif" withExtension:nil];
    CGImageSourceRef csf = CGImageSourceCreateWithURL((__bridge CFTypeRef) url, NULL);
    size_t const count = CGImageSourceGetCount(csf);
    UIImage *frames[count];
    CGImageRef images[count];
    for (size_t i = 0; i < count; ++i) {
        images[i] = CGImageSourceCreateImageAtIndex(csf, i, NULL);
        UIImage *image =[[UIImage alloc] initWithCGImage:images[i]];
        frames[i] = image;
        CFRelease(images[i]);
    }
    UIImage *const animation = [UIImage animatedImageWithImages:[NSArray arrayWithObjects:frames count:count] duration:4];
    [imageView setImage:animation];
    CFRelease(csf);
}
*/

@end
