//
//  SLConst.h
//  Superloop
//
//  Created by administrator on 16/7/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLUserDefaultsConsts : NSObject

/**
 *  类型（属性类型）
 */
extern NSString *const appIsFirst;
extern NSString *const SLUserLogin;
extern NSString *const addressData;
extern NSString *const strAccout;
extern NSString *const strAccountName;
extern NSString *const SuggestionData;
extern NSString *const swithIsOn;
extern NSString *const BannerImg;
extern NSString *const location;
extern NSString *const code;
extern NSString *const faiure;
extern NSString *const closeLocation;
extern NSString *const phoneNumber;
extern NSString *const NewsCount;
extern NSString *const categoryKey;
extern NSString *const price;
extern NSString *const Sure;
extern NSString *const name;
extern NSString *const VoiceType;
extern NSString *const ContactStatus;
extern NSString *const AllowAddContactStatus;
//临时数据
extern NSString *const LoginStutas;
extern NSString *const SLNetWorkStatus;
@end
