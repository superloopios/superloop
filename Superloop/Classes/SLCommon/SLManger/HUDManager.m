//
//  HUDManager.m
//  yilingdoctorCRM
//
//  Created by 朱宏伟的MAC on 14/10/28.
//  Copyright (c) 2014年 yuntai. All rights reserved.
//

#import "HUDManager.h"

static MBProgressHUD *HUDView;
@implementation HUDManager

+ (void)showWarningWithText:(NSString *)text {
//    [HUDManager hideHUDView];
//    UIWindow *window = [[UIApplication sharedApplication].windows objectAtIndex:0];
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
//    hud.alpha = 0.8;
//    hud.labelText = text;
//    hud.mode = MBProgressHUDModeText;
//    hud.dimBackground = NO;
//    hud.margin = 12.f;
//    [hud hide:YES afterDelay:2.0];
    kShowToast(text)
}

+ (void)showLoadingHUDView:(UIView*)view
{
    
    HUDView = [MBProgressHUD showHUDAddedTo:view animated:YES];
    HUDView.mode = MBProgressHUDModeIndeterminate;
    HUDView.margin = 18.f;
    HUDView.opacity = 0.8;
//    HUDView.dimBackground = YES;
    HUDView.dimBackground = NO;

    HUDView.labelText = @"正在加载";
    HUDView.minShowTime = 0.3;
    HUDView.labelFont = [UIFont boldSystemFontOfSize:14];
}

+ (void)showLoadingHUDView:(UIView*)view withText:(NSString *)text {
    HUDView = [MBProgressHUD showHUDAddedTo:view animated:YES];
    HUDView.mode = MBProgressHUDModeIndeterminate;
//    HUDView.margin = 18.f;
    HUDView.margin = 24.f;
//    HUDView.dimBackground = YES;
    HUDView.dimBackground = NO;
    HUDView.mode = MBProgressHUDModeIndeterminate;
    
    HUDView.labelText = text;
    HUDView.minShowTime = 0.3;
    HUDView.labelFont = [UIFont boldSystemFontOfSize:14];
}

+ (void)hideHUDView
{
    [HUDView hide:YES];
}

+ (void)showAlertWithText:(NSString *)text {
        [HUDManager hideHUDView];
        UIWindow *window = [[UIApplication sharedApplication].windows objectAtIndex:0];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
        hud.alpha = 0.8;
        hud.labelText = text;
        hud.mode = MBProgressHUDModeText;
        hud.dimBackground = NO;
        hud.margin = 24.f;
        [hud hide:YES afterDelay:2.0];
}

@end
