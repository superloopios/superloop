//
//  SLLoginViewControllerManger.m
//  Superloop
//
//  Created by 朱宏伟 on 16/5/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLLoginViewControllerManger.h"
#import "SLFirstViewController.h"

@implementation SLLoginViewControllerManger


//在其他的界面展示本控制器
+ (void)show:(id)viewController{
    
    SLFirstViewController *Vc = [[SLFirstViewController alloc] init];
//    Vc.isComeFromSettingVc = YES;
    [viewController presentViewController:Vc animated:YES completion:nil];
    
}

//在需要用到的控制器隐藏控制器
+ (void)hidden:(id)viewController{
    
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

@end
