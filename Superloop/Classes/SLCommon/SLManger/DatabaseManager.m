//
//  DatabaseManager.m
//  Superloop
//
//  Created by WangS on 16/5/14.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "DatabaseManager.h"

#import "FMDatabase.h"


@implementation DatabaseManager

{
    FMDatabase *db;
}

//单例
+(DatabaseManager *)sharedManger
{
    static DatabaseManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DatabaseManager alloc] init];
    });
    return instance;
}

-(instancetype)init
{
    if (self = [super init]) {
        
        //1, 创建数据库操作对象
        db = [FMDatabase databaseWithPath:[self dbPath]];
        
        //打开数据库
        BOOL ret = [db open];
        NSLog(@"数据库打开:%@", ret ? @"成功" : @"失败");
        
        //2, 创建表
        BOOL ret2 = [self createTable];
        NSLog(@"表创建:%@", ret2 ? @"成功" : @"失败");
    }
    return self;
}

//创建表
-(BOOL)createTable
{
    NSString *sql = @"create table if not exists HistorySearch(id integer primary key autoincrement, historyInput text)";
    
    return [db executeUpdate:sql];
}

//插入数据
-(BOOL)insertDatas:(NSString *)historyInput
{
    NSString *sql = @"insert into HistorySearch(historyInput) values(?)";
    
    return [db executeUpdate:sql, historyInput];
}
-(BOOL)selectData:(NSString *)historyInput
{
    NSString *existsSql = [NSString stringWithFormat:@"select * from  HistorySearch where historyInput = '%@'",historyInput];
    FMResultSet *set = [db executeQuery:existsSql];

  // NSString *existsSql = @"select * from  HistorySearch where historyInput = ？";
//   FMResultSet *set = [db executeQuery:existsSql,historyInput] ;
    if ([set next]) {

         return YES;
        
    } else {
        return NO;
    }
   
}

//获取数据
-(NSArray *)getAllDatas
{
    NSMutableArray *mArr = [NSMutableArray new];
    
    NSString *sql = @"select * from HistorySearch";
    
    FMResultSet *set = [db executeQuery:sql];
    
    while ([set next]) {

        NSString *input = [set stringForColumn:@"historyInput"];
        
       // [mArr addObject:input];
        [mArr insertObject:input atIndex:0];
    }
   return mArr;
}

//删除数据
- (BOOL)deleteUser
{
    //sql
    NSString *sql = @"delete from HistorySearch ";
    
    //执行删除
    return [db executeUpdate:sql];
   
}



//数据库文件的存储路径
-(NSString *)dbPath
{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSLog(@"path:%@", path);
    
    return [path stringByAppendingPathComponent:@"historySearch.db"];
}


@end
