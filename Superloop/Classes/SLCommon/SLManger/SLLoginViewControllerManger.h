//
//  SLLoginViewControllerManger.h
//  Superloop
//
//  Created by 朱宏伟 on 16/5/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//   登录页面的显示帮助类

#import <Foundation/Foundation.h>

@interface SLLoginViewControllerManger : NSObject

+ (void)show:(id)viewController;

+ (void)hidden:(id)viewController;
@end
