//
//  DatabaseManager.h
//  Superloop
//
//  Created by WangS on 16/5/14.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatabaseManager : NSObject

//单例
+(DatabaseManager *)sharedManger;

//插入数据
-(BOOL)insertDatas:(NSString *)historyInput;

//获取数据
-(NSArray *)getAllDatas;
-(BOOL)selectData:(NSString *)historyInput;

//删除数据
- (BOOL)deleteUser;

@end
