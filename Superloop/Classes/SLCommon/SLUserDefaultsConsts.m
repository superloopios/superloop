//
//  SLConst.m
//  Superloop
//
//  Created by administrator on 16/7/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLUserDefaultsConsts.h"

@implementation SLUserDefaultsConsts

NSString *const appIsFirst = @"appIsFirst";
NSString *const SLUserLogin = @"SLUserLogin";
NSString *const addressData = @"addressData";
NSString *const strAccout = @"strAccout";
NSString *const strAccountName = @"strAccountName";
NSString *const SuggestionData = @"SuggestionData";
NSString *const swithIsOn = @"swithIsOn";
NSString *const BannerImg = @"BannerImg";
NSString *const location = @"location";
NSString *const code = @"code";
NSString *const faiure = @"faiure";
NSString *const closeLocation = @"closeLocation";
NSString *const phoneNumber = @"phoneNumber";
NSString *const NewsCount = @"NewsCount";
NSString *const categoryKey = @"categoryKey";
NSString *const price = @"price";
NSString *const Sure = @"Sure";
NSString *const name = @"name";
NSString *const VoiceType = @"VoiceType";

NSString *const ContactStatus = @"ContactStatusV1";
NSString *const AllowAddContactStatus = @"AllowAddContactStatusV1";
//临时数据
NSString *const LoginStutas = @"LoginStutas";
NSString *const SLNetWorkStatus = @"SLNetWorkStatus";

@end
