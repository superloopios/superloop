//
//  NSString+Base64.h
//  Superloop
//
//  Created by Wang on 16/3/6.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Base64)
- (NSString *)base64EncodedString;
+ (NSString *)base64StringFromData:(NSData *)data length:(NSUInteger)length;
@end
