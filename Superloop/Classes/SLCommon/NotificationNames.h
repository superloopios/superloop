//
//  NotificationNames.h
//  Superloop
//
//  Created by 朱宏伟 on 16/7/3.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import <Foundation/Foundation.h>

#define Home_Refreshing "homeRefreshing"

#define Topic_Refreshing "topicRefreshing"

#define Mine_Refreshing "mineRefreshing"

#define Message_AccountLogin "messageAccountLogin"

#define CallAndContact_Refreshing "CallAndContactRefreshing"