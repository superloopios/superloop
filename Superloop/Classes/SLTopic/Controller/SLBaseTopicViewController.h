//
//  SLBaseTopicViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLTopicModel;
@class SLTopicDetailViewController;
@interface SLBaseTopicViewController : UIViewController

//登录用户的数据信息
@property(nonatomic, strong)NSDictionary *userInfo;

-(void)didUserIconClickedTransmitModel:(SLTopicModel *)model;
-(void)didSelectRowAtIndexPathTransmitModel:(SLTopicModel *)model isFromSearch:(BOOL) isFromSearch;
-(void)didPraiseBtnClicked:(UIButton *)btn praiseLab:(UILabel *)praiseLab model:(SLTopicModel *)model;
- (void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickPraise:(SLTopicModel *)model;

@end
     
