//
//  SLAddSubjectViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLAddSubjectViewController : UIViewController
//存放所有照片
@property (nonatomic,strong) NSMutableArray *images;
@property (nonatomic,copy) void(^publishSuccessBlock)(BOOL isSuccess);
@end
