//
//  SLAwardViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/5.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLAwardViewController.h"
#import <Masonry.h>
@interface SLAwardViewController ()
@property (nonatomic, strong)NSArray *moneyArray;
/** 当前被点中的按钮 */
@property (nonatomic, weak) UIButton *clickedButton;
@end
/**这里我们取内容的宽度为218,间距为13 */
static const CGFloat btnWidth = (218 - 26 ) / 3;
static const CGFloat marginW = 13;
static const CGFloat marginH = 9;
static const CGFloat btnHeight = (70 - 9) / 2;
@implementation SLAwardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor grayColor];
    self.moneyArray = @[@"10元",@"20元",@"30元",@"40元",@"50元",@"60元"];
    [self setUpUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)setUpUI
{
    UIView *awardView = [[UIView alloc] init];
    awardView.backgroundColor = [UIColor whiteColor];
    awardView.layer.cornerRadius = 5;
    awardView.layer.masksToBounds = YES;
    [self.view addSubview:awardView];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setBackgroundImage:[UIImage imageNamed:@"cancel001"] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [awardView addSubview:cancelBtn];
    
    UILabel *awardLabel = [[UILabel alloc] init];
    awardLabel.text = @"打赏作者";
    awardLabel.font = [UIFont systemFontOfSize:15];
    awardLabel.textColor = SLColor(80, 80, 80);
    [awardView addSubview:awardLabel];
    
    UIImageView *authorIcon = [[UIImageView alloc] init];
    authorIcon.image = [UIImage imageNamed:@"Simple-avatar"];
    [awardView addSubview:authorIcon];
    
    UILabel *authorName = [[UILabel alloc] init];
    authorName.text = @"JOHN.DEO";
    authorName.textColor = SLColor(80, 80, 80);
    authorName.font = [UIFont systemFontOfSize:14];
    [awardView addSubview:authorName];
    
    UILabel *topictitle = [[UILabel alloc] init];
    topictitle.numberOfLines = 2;
    topictitle.textAlignment = NSTextAlignmentCenter;
    topictitle.text = @"快速瘦身的有效方法?朝阳区哪家健身馆比较好,有没有推荐的教练?";
    topictitle.font = [UIFont systemFontOfSize:13];
    topictitle.textColor = SLColor(80, 80, 80);
    [awardView addSubview:topictitle];

    UIButton *footBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footBtn.backgroundColor = SLColor(20, 110, 93);
    [footBtn setTitle:@"打赏" forState:UIControlStateNormal];
    footBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [awardView addSubview:footBtn];
    
    UITextField *moneyField = [[UITextField alloc] init];
    moneyField.borderStyle = UITextBorderStyleRoundedRect;
    moneyField.placeholder = @"1-200元";
    moneyField.textColor = SLColor(208, 208, 208);
    moneyField.height = 35;
    moneyField.textAlignment = NSTextAlignmentCenter;
    [awardView addSubview:moneyField];
    
    UILabel *otherMoney = [[UILabel alloc] init];
    otherMoney.text = @"其他金额:";
    otherMoney.textColor = SLColor(80, 80, 80);
    otherMoney.font = [UIFont systemFontOfSize:14];
    [awardView addSubview:otherMoney];
    
    UIView *moneyView = [[UIView alloc] init];
    for (int i = 0; i < 6; i++) {
        UIButton *moneyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        moneyBtn.layer.cornerRadius = 5;
        moneyBtn.layer.masksToBounds = YES;
        moneyBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        moneyBtn.layer.borderWidth = 1;
        [moneyBtn setTitle:self.moneyArray[i] forState:UIControlStateNormal];
        [moneyBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        moneyBtn.frame = CGRectMake((marginW + btnWidth) * (i % 3), (marginH + btnHeight) * (i / 3), btnWidth, btnHeight);
        [moneyBtn addTarget:self action:@selector(selMoneyBtn:) forControlEvents:UIControlEventTouchUpInside];
        [moneyView addSubview:moneyBtn];
    }
    [awardView addSubview:moneyView];
    
    UIImageView *cutImage =[[UIImageView alloc] init];
    cutImage.backgroundColor = [UIColor lightGrayColor];
    [awardView addSubview:cutImage];
    
    
    //添加约束
    [awardView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(294);
        make.height.mas_equalTo(490);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.centerY.mas_equalTo(self.view.mas_centerY);
    }];
    
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(awardView.mas_top).offset(20);
        make.right.mas_equalTo(awardView.mas_right).offset(-20);
        make.width.height.mas_equalTo(20);
    }];
    [awardLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(awardView.mas_centerX);
        make.top.mas_equalTo(awardView.mas_top).offset(25);
    }];
    
    [authorIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(awardLabel.mas_bottom).offset(30);
        make.centerX.mas_equalTo(awardView.mas_centerX);
        make.width.height.mas_equalTo(65);
    }];
    
    [authorName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(awardView.mas_centerX);
        make.top.mas_equalTo(authorIcon.mas_bottom).offset(18);
    }];
    
    [topictitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(awardView.mas_centerX);
        make.top.mas_equalTo(authorName.mas_bottom).offset(26);
        make.width.mas_equalTo(218);
        
    }];
    
    [footBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(awardView.mas_bottom);
        make.left.mas_equalTo(awardView.mas_left);
        make.right.mas_equalTo(awardView.mas_right);
        make.height.mas_equalTo(60);
    }];
    
    [moneyField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(footBtn.mas_top).offset(-23);
        make.left.mas_equalTo(awardView.mas_centerX).offset(-30);
        make.height.mas_equalTo(30);
    }];
    
    [otherMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(moneyField.mas_left).offset(-20);
        make.centerY.mas_equalTo(moneyField.mas_centerY);
    }];
    
    [moneyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(moneyField.mas_top).offset(-25);
        make.centerX.mas_equalTo(awardView.mas_centerX);
        make.width.mas_equalTo(topictitle.mas_width);
        make.height.mas_equalTo(70);
    }];
    
    [cutImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(moneyView.mas_top).offset(-25);
        make.centerX.mas_equalTo(awardView.mas_centerX);
        make.width.mas_equalTo(topictitle.mas_width);
        make.height.mas_equalTo(1);
        
    }];
    
}

- (void)cancelBtnClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)selMoneyBtn:(UIButton *)sender
{
     //切换按钮状态
    [self.clickedButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    self.clickedButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.clickedButton = sender;
    [self.clickedButton setTitleColor:SLColor(20, 110, 93) forState:UIControlStateNormal];
    self.clickedButton.layer.borderColor = SLColor(20, 110, 93).CGColor;
}
@end
