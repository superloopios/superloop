//
//  SLAttenTopicViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLBaseTopicViewController.h"

@interface SLCollectionTopicViewController : SLBaseTopicViewController
@property (nonatomic,assign)BOOL isFromMeController;
@property (nonatomic,weak)UIView *noViewCoutView;

@end
