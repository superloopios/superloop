//
//  SLCommendViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/5.
//  Copyright © 2016年 Superloop. All rights reserved.
//   发表评论控制器

#import "SLCommendViewController.h"
#import <Masonry.h>
#import "SLTextView.h"
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "SLAPIHelper.h"
#import <MBProgressHUD.h>
@interface SLCommendViewController ()<UITextViewDelegate>
@property (nonatomic, strong) SLTextView *commendTextField;

@property (nonatomic,strong) UIButton *rightBtn;

@property (nonatomic,weak) UIScrollView *baseSC;

@end

@implementation SLCommendViewController


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLCommendViewController"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNav];
    [self setUpUI];
    [self.commendTextField becomeFirstResponder];
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor=SLColor(240, 240, 240);
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backBtn setTitle:@"取消" forState:UIControlStateNormal];
    backBtn.titleLabel.font=[UIFont systemFontOfSize:15];
//    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"添加评论";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    _rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    _rightBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    [_rightBtn setTitle:@"发布" forState:UIControlStateNormal];
    _rightBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_rightBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_rightBtn addTarget:self action:@selector(sendCommend:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:_rightBtn];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.commendTextField resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
}
- (void)setUpUI{
   
    SLTextView *commendTextField = [[SLTextView alloc] init];
    commendTextField.placeholder = self.placerHolder;
    if (self.placerHolder) {
        commendTextField.placeholder = self.placerHolder;
    }else{
        commendTextField.placeholder = @"请在这里输入评论内容";
    }
    self.commendTextField = commendTextField;
    commendTextField.delegate = self;
    [self.view addSubview:commendTextField];
    
    
    
    //处理键盘被挡住的情况
    
    
//    [navView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.view.mas_top);
//        make.left.mas_equalTo(self.view.mas_left);
//        make.right.mas_equalTo(self.view.mas_right);
//        make.height.mas_equalTo(63);
//    }];
//    
//    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.view.mas_left);
//        make.right.mas_equalTo(self.view.mas_right);
//        make.top.mas_equalTo(navView.mas_bottom);
//        make.height.mas_equalTo(1);
//    }];
//    
//    [commendLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.mas_equalTo(navView.mas_bottom).offset(-5);
//        make.centerX.mas_equalTo(navView.mas_centerX);
//    }];
//    
//    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(navView.mas_left).offset(5);
//        make.bottom.mas_equalTo(navView.mas_bottom);
//        
//    }];
//    
//    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(navView.mas_right).offset(-5);
//        make.bottom.mas_equalTo(navView.mas_bottom);
//        
//    }];
    
    [commendTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.top.mas_equalTo(self.view.mas_top).offset(64);
        make.height.mas_equalTo(300);
    }];
}

- (void)sendCommend:(UIButton *)sender {
    _rightBtn.enabled=NO;
    //先将未到时间执行前的任务取消。
    
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(postComment:) object:sender];
    [self performSelector:@selector(postComment:) withObject:sender afterDelay:0.3f];
}


- (void)postComment:(UIButton *)sender{
 
    if ([self publishVerif]) {
        [HUDManager showLoadingHUDView:self.view withText:nil];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"id"] = _topicID;
        parameter[@"content"] = self.commendTextField.text;
//        NSDictionary *parameter = @{@"id":_topicID,@"content":self.commendTextField.text};
        if (self.selectModel) {
            parameter[@"reply_to"] = self.selectModel.id;
        }
        [SLAPIHelper topicsReplies:parameter method:Post success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            //  NSLog(@"success : %@",data);
            //这里需要发送通知告知上一个控制器刷新控件
            //        [[NSNotificationCenter defaultCenter] postNotificationName:@"aa" object:self];
            [HUDManager hideHUDView];
            self.commendSuccessBlock(YES);
            [HUDManager showWarningWithText:@"评论成功!"];

            
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.commendTextField resignFirstResponder];
            [self dismissViewControllerAnimated:YES completion:nil];
            //            });
        } failure:^(SLHttpRequestError *failure) {
            _rightBtn.enabled=YES;
            [HUDManager hideHUDView];
            if (failure.httpStatusCode==400&&failure.slAPICode==10) {
                [HUDManager showWarningWithText:@"您的操作过于频繁，请稍候重试"];
            }else if(failure.httpStatusCode==409){
                if (failure.slAPICode==30) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [HUDManager showWarningWithText:[NSString stringWithFormat:@"操作失败\n发布话题者已将您屏蔽"]];
                    });
                    
                }else if (failure.slAPICode==31){
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                       [HUDManager showWarningWithText:[NSString stringWithFormat:@"操作失败\n发布话题者在你的黑名单中"]];
                    });
                    
                }else if (failure.slAPICode==32){
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                       [HUDManager showWarningWithText:[NSString stringWithFormat:@"操作失败\n对方已经将你屏蔽"]];
                    });
                    
                }else if (failure.slAPICode==33){
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                         [HUDManager showWarningWithText:[NSString stringWithFormat:@"操作失败\n对方在你的黑名单中"]];
                    });
                   
                }
                
            }else{
                [HUDManager showWarningWithText:@"网络异常"];
            }
        }];
        
    }
    
}

//处理输入框对于键盘的显示是不完全的
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLCommendViewController"];

    //注册通知,监听键盘出现
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidShow:)
                                                name:UIKeyboardDidShowNotification
                                              object:nil];
    //注册通知，监听键盘消失事件
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidHidden)
                                                name:UIKeyboardDidHideNotification
                                              object:nil];
}

//监听事件
- (void)handleKeyboardDidShow:(NSNotification*)paramNotification
{
    //获取键盘高度
    NSValue *keyboardRectAsObject=[[paramNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    self.commendTextField.contentInset=UIEdgeInsetsMake(0, 0,keyboardRect.size.height - 200, 0);
}

- (void)handleKeyboardDidHidden
{
    self.commendTextField.contentInset=UIEdgeInsetsZero;
}


#pragma mark ---- 需要限制评论的内容的有效性(不要空格，不能为空)
- (BOOL)publishVerif{
    //空白消息
    NSString *strContent = [self.commendTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (strContent.length == 0) {
        [HUDManager showWarningWithText:@"请输入内容!"];
        _rightBtn.enabled=YES;
        return NO;
    }
    
    if (self.commendTextField.text.length<1 ||self.commendTextField.text.length > 4095) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入评论内容长度为1至4095位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        _rightBtn.enabled=YES;
        return NO;
        
    }
    if ([self.commendTextField.text isEqualToString:self.replayContent]) {
        
        kShowToast(@"回复内容不可与对方相同");
        _rightBtn.enabled=YES;
        return NO;
        
    }
    return  YES;
}


-(void)textViewDidChange:(UITextView *)textView
{
    if (_commendTextField.text.length>0) {
        [_rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }else{
        [_rightBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
}
- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
