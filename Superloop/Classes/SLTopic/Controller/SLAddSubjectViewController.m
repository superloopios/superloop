//
//  SLAddSubjectViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/7.
//  重构  by  xiaowu  on  16/11/10
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <AFNetworking.h>
#import "AppDelegate.h"
#import <AliyunOSSiOS/OSSService.h>
#import "UIView+Layout.h"
#import "TZImagePickerController.h"
#import "SLAddSubjectViewController.h"
#import <Masonry.h>
#import "SLTextView.h"
#import "NSString+Extension.h" //处理字符串处理
#import <MBProgressHUD.h>
#import "SLAPIHelper.h"
#import "SLFirstViewController.h"
#import "SLNavgationViewController.h"

#include <CommonCrypto/CommonDigest.h>
#import "SLActionView.h"
#import "NSString+Utils.h"
#import "SLPhotoView.h"

#define FileHashDefaultChunkSizeForReadingData 1024*8 // 8K

#define FileHashDefaultChunkSizeForReadingData 1024*8


@interface SLAddSubjectViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,TZImagePickerControllerDelegate,UITextFieldDelegate,UITextViewDelegate,UIAlertViewDelegate,SLActionViewDelegate,SLPhotoViewDelegate>
{
    NSMutableArray      *_selectedAssets;
    CGFloat             _itemWH;
    NSInteger           _num;
}
@property (nonatomic, strong) UITextField           *titleField;
@property (nonatomic, strong) SLTextView            *textView;
@property (nonatomic, strong) NSData                *data;
@property (nonatomic, strong) OSSClient             *client;
@property (nonatomic,   copy) NSString              *imagName;
@property (nonatomic, strong) UIButton              *addImageBtn;
@property (nonatomic, strong) UIScrollView          *baseSc;
@property (nonatomic, strong) UIButton              *btnRegistBtn;
@property (nonatomic, strong) UIButton              *submitBtn;
@property (nonatomic,   copy) NSString              *titleStr;
@property (nonatomic,   copy) NSString              *contentStr;
@property (nonatomic, strong) SLActionView          *actionView;
@property (nonatomic, strong) NSMutableArray        *photoArrays;
@property (nonatomic, strong) NSMutableArray        *photoNameArrays;
@property (nonatomic, strong) NSMutableArray        *photoViews;
@property (nonatomic, strong) NSMutableArray        *photoModels;
@property (nonatomic, assign) NSInteger              tag_ids;
@property (nonatomic, assign) BOOL                   isPublishSuccess;

@end
@implementation SLAddSubjectViewController

- (UIButton *)addImageBtn{
    if (!_addImageBtn) {
        _addImageBtn = [[UIButton alloc] init];
        [_addImageBtn setImage:[UIImage imageNamed:@"addImage"] forState:UIControlStateNormal];
        [_addImageBtn addTarget:self action:@selector(addPictures) forControlEvents:UIControlEventTouchUpInside];
        [self.baseSc addSubview:_addImageBtn];
    }
    return _addImageBtn;
}

- (UIScrollView *)baseSc{
    if (!_baseSc) {
        _baseSc = [[UIScrollView alloc] init];
        _baseSc.frame = CGRectMake(0, 64, screen_W, screen_H-64);
        [self.view addSubview:_baseSc];
    }
    return _baseSc;
}

- (SLActionView *)actionView{
    if (!_actionView) {
        _actionView = [[SLActionView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _actionView.delegate = self;
        [self.view addSubview:_actionView];
    }
    return _actionView;
}
#pragma mark - 懒加载
-(NSString *)titleStr{
    if (!_titleStr) {
        _titleStr=[NSString new];
    }
    return _titleStr;
}
-(NSString *)contentStr{
    if (!_contentStr) {
        _contentStr=[NSString new];
    }
    return _contentStr;
}
-(NSMutableArray *)photoArrays{
    if (!_photoArrays) {
        _photoArrays=[NSMutableArray new];
    }
    return _photoArrays;
}

-(NSMutableArray *)photoViews{
    if (!_photoViews) {
        _photoViews=[NSMutableArray new];
    }
    return _photoViews;
}
-(NSMutableArray *)photoNameArrays{
    if (!_photoNameArrays) {
        _photoNameArrays=[NSMutableArray new];
    }
    return _photoNameArrays;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLAddSubjectViewController"];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.btnRegistBtn.hidden = YES;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [MobClick endLogPageView:@"SLAddSubjectViewController"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUpNav];
    [self getUserDefaults];
    _itemWH = (self.view.width- 30) / 3;
    [self setUpUI];
//
    self.textView.delegate=self;
    self.titleField.delegate=self;
    self.isPublishSuccess=NO;
    
    //收起键盘
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到view上
    [self.textView addGestureRecognizer:tapGestureRecognizer];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    [self calculationImageFrame];
    
}


- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backBtn setTitle:@"取消" forState:UIControlStateNormal];
    backBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"添加话题";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    _submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    [_submitBtn setTitle:@"发布" forState:UIControlStateNormal];
    _submitBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_submitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(publickClick:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:_submitBtn];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)setUpUI{
    UITextField *titleField = [[UITextField alloc] init];
    titleField.textColor = SLBlackTitleColor;
    titleField.font=[UIFont systemFontOfSize:20];
    titleField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"添加标题" attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0x888888)}];
    self.titleField = titleField;
    if (self.titleStr.length>0) {
        self.titleField.text=self.titleStr;
    }
    [self.baseSc addSubview:titleField];
    [titleField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(17);
        make.right.mas_equalTo(self.view.mas_right).offset(-17);
        make.top.mas_equalTo(self.baseSc);
        make.height.mas_equalTo(55);
    }];
    
    UIImageView *imgView=[[UIImageView alloc] init];
    imgView.backgroundColor=SLColor(220, 220, 220);
    [self.baseSc addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(17);
        make.right.mas_equalTo(self.view.mas_right).offset(-12);
        make.top.mas_equalTo(self.titleField.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
    
    SLTextView *textView = [[SLTextView alloc] init];
    textView.placeholder = @"请输入正文";
    textView.textColor = SLBlackTitleColor;
    self.textView = textView;
    if (self.contentStr.length>0) {
        self.textView.text=self.contentStr;
    }
    [self.baseSc addSubview:textView];
    [textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(13);
        make.right.mas_equalTo(self.view.mas_right).offset(-17);
        make.top.mas_equalTo(imgView.mas_bottom).offset(7);
        make.height.mas_equalTo(215);
    }];
    
    if (iPhone4 || iPhone5 || iPhone4s) {
        UIButton *btnRegistBtn = [[UIButton alloc] initWithFrame:CGRectMake(ScreenW - 60, ScreenH - 286, 60, 30)];
        btnRegistBtn.hidden = YES;
        self.btnRegistBtn = btnRegistBtn;
        [btnRegistBtn setImage:[UIImage imageNamed:@"down_option"] forState:UIControlStateNormal];
        [btnRegistBtn addTarget:self action:@selector(btnRegistKeyBoard) forControlEvents:UIControlEventTouchUpInside];
        UIWindow *window = [[UIApplication sharedApplication].windows objectAtIndex:0];
        [window addSubview:btnRegistBtn];
    }
    
}
-(void)getUserDefaults{
    if (ApplicationDelegate.titleStr.length>0) {
        self.titleStr=ApplicationDelegate.titleStr;
    }
    if (ApplicationDelegate.contentStr.length>0) {
        self.contentStr=ApplicationDelegate.contentStr;
    }
    if (ApplicationDelegate.publishPhotosArr.count>0) {
        [self.photoArrays removeAllObjects];
        for (NSData *data in ApplicationDelegate.publishPhotosArr) {
            [self.photoArrays addObject:data];
            [self.photoNameArrays addObject:[NSString getMD5WithData:data]];
        }
    }
}

#pragma mark -- 注销键盘事件
- (void)btnRegistKeyBoard{
    
    [self.view endEditing:YES];
    self.btnRegistBtn.hidden = YES;
}

-(void)backBtnClick{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [HUDManager hideHUDView];
    if (!self.isPublishSuccess) {
        if (self.titleField.text.length>0) {
            ApplicationDelegate.titleStr=self.titleField.text;
        }else{
            ApplicationDelegate.titleStr=nil;
        }
        if (self.textView.text.length>0) {
            ApplicationDelegate.contentStr=self.textView.text;
        }else{
            ApplicationDelegate.contentStr=nil;
        }
        if (self.photoArrays.count>0) {
            ApplicationDelegate.publishPhotosArr=self.photoArrays;
        }else{
            ApplicationDelegate.publishPhotosArr=nil;
        }
    }else{
        ApplicationDelegate.titleStr=nil;
        ApplicationDelegate.contentStr=nil;
        ApplicationDelegate.publishPhotosArr=nil;
    }
    
}

- (void)addPictures{
    [self.textView resignFirstResponder];
    self.actionView.titleArr = @[@"取消",@"从相册选择",@"拍照"];
    [self.view addSubview:self.actionView];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (string.length > 1 || string.length < 100) {
        return YES;
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入标题长度为1至100位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        return NO;
    }
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    self.btnRegistBtn.hidden = YES;
}

//发布按钮
- (void)publickClick:(UIButton *)sender{
    _submitBtn.enabled=NO;
    //先将未到时间执行前的任务取消。
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(publishTodo:) object:sender];
    [self performSelector:@selector(publishTodo:) withObject:sender afterDelay:0.3f];
    
}

//上传图片到阿里云服务器
- (void)publishTodo:(UIButton *)sender{
    
    if (!ApplicationDelegate.Basic) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
        alert.tag=100;
        [alert show];
        _submitBtn.enabled=YES;
        return;
    }
    // 文字长度判断
    if ([NSString isEmptyStrings:self.titleField.text]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入标题长度为1至100位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        _submitBtn.enabled=YES;
        return;
    }
    if(self.titleField.text.length < 1 || self.titleField.text.length > 100){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入标题长度为1至100位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        _submitBtn.enabled=YES;
        return;
    }
    if ([NSString isEmptyStrings:self.textView.text]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入内容长度为1至4095位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        _submitBtn.enabled=YES;
        return;
    }
    
    if (self.textView.text.length<1 ||self.textView.text.length > 4095) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入内容长度为1至4095位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        _submitBtn.enabled=YES;
        return;
    }
    
    if (self.photoArrays.count>0) {
        self.imagName = @"";
#ifdef DEBUG
        NSString *endpoint = @"http://dev.oss.superloop.com.cn";
#else
        NSString *endpoint = @"http://oss.superloop.com.cn";
#endif
        id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:@"UPhdoMR1eN1hnrdA" secretKey:@"ETuIdMVCT1mtiqs8f5KZ8dZnZLiXfG"];
        OSSClient *client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIWindow *win = [[UIApplication sharedApplication].windows objectAtIndex:0];
            [HUDManager showLoadingHUDView:win withText:@"正在上传中"];
        });
        for (int i=0;i<self.photoArrays.count;i++) {
            
            OSSPutObjectRequest *put = [OSSPutObjectRequest new];
#ifdef DEBUG
            put.bucketName = @"superloop-dev";
#else
            put.bucketName = @"superloop";
#endif
            put.objectKey = [NSString stringWithFormat:@"%@/%@.jpg",ApplicationDelegate.userId,self.photoNameArrays[i]];
            self.imagName = [self.imagName stringByAppendingString:[@"&imgs=" stringByAppendingString:put.objectKey]];
            put.uploadingData = self.photoArrays[i];
            put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
            };
            OSSTask * putTask = [client putObject:put];
            [putTask continueWithBlock:^id(OSSTask *task) {
                if (!task.error) {
                    NSLog(@"upload object success!");
                    _submitBtn.enabled=NO;
                    _num++;
                    if (_num == self.photoArrays.count) {
                        _num = 0;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [HUDManager hideHUDView];
                        });
                        [self postDatas];//图片上传成功，请求发布话题接口
                    }
                } else {
                    NSLog(@"发布话题图片上传失败" );
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _submitBtn.enabled=YES;
                        [HUDManager hideHUDView];
                        [HUDManager showWarningWithText:@"上传图片失败，请稍后重试"];
                    });
                }
                return nil;
            }];
        }
    }else{
        [self postDatas];
    }
    
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    self.btnRegistBtn.hidden = NO;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==100) {
        if (buttonIndex==1) {
            SLFirstViewController *vc=[[SLFirstViewController alloc] init];
            SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
            [self presentViewController:nav animated:YES completion:nil];
        }
    }
}

//发布话题
-(void)postDatas{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIWindow *win = [[UIApplication sharedApplication].windows objectAtIndex:0];
        [HUDManager showLoadingHUDView:win withText:@"正在发布"];
    });
    //1.确定url
    // NSURL *url = [NSURL URLWithString:@"http://dev.superloop.com.cn/topics"];
    NSURL *url = [NSURL URLWithString:[URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/topics"]]];
    //2.创建可变请求对象
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    //3.修改请求方法,POST是需要大写的
    request.HTTPMethod = @"POST";
    //设置请求头信息
    [request setValue:[NSString stringWithFormat:@"Basic %@",ApplicationDelegate.Basic] forHTTPHeaderField:@"Authorization"];
    //4.设置请求体
    NSString *titleFieldUTF8 = (NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(nil,
                                                                                                    (CFStringRef)self.titleField.text, nil,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
    
    NSString *textViewUTF8 = (NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(nil,
                                                                                                  (CFStringRef)self.textView.text, nil,
                                                                                                  (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
    if (!(self.imagName.length>0)) {
        self.imagName=@"";
    }
    NSString *httpBodyStr = [NSString stringWithFormat:@"title=%@&content=%@%@", titleFieldUTF8,textViewUTF8,self.imagName];
    request.HTTPBody =[httpBodyStr dataUsingEncoding:NSUTF8StringEncoding];
    //    //发送请求之前先判断一下
    if ([self publishVerif]){
        //    //5.发送请求
        NSURLSession *session=[NSURLSession sharedSession];
        NSURLSessionTask *task=[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            //发布成功刷新页面
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PublishSuccess" object:self];
            if (error==nil) {
                NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                NSInteger responseStatusCode = [httpResponse statusCode];
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                NSInteger code=[dict[@"code"] integerValue];
                if (responseStatusCode==200) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self.publishSuccessBlock) {
                            self.publishSuccessBlock(YES);
                        }
                        self.isPublishSuccess=YES;
                        [HUDManager hideHUDView];
                        [MobClick event:@"topic_count"];
                        [self performSelector:@selector(popToController) withObject:nil afterDelay:1.0f];
                        [HUDManager showWarningWithText:@"发布成功"];
                    });
                }else if (responseStatusCode==429&&code==10){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _submitBtn.enabled=YES;
                        [HUDManager hideHUDView];
                        [HUDManager showWarningWithText:@"操作频繁,发布失败"];
                    });
                    
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _submitBtn.enabled=YES;
                        [HUDManager hideHUDView];
                        [HUDManager showWarningWithText:@"发布失败"];
                    });
                }
                
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    _submitBtn.enabled=YES;
                    [HUDManager hideHUDView];
                    [HUDManager showWarningWithText:@"发布失败"];
                });
                
            }
            
        } ];
        [task resume];
    }
}
#pragma mark ---- 添加验证，如果没有内容就不允许发布
- (BOOL)publishVerif{
    //空白消息
    NSString *strtitle = [self.titleField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"内容是%@",self.textView.text);
    if (strtitle.length == 0) {
        [HUDManager showWarningWithText:@"请输入标题!"];
        _submitBtn.enabled=YES;
        return NO;
    }
    NSString *strContent = [self.textView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (strContent.length == 0) {
        [HUDManager showWarningWithText:@"请输入内容!"];
        _submitBtn.enabled=YES;
        return NO;
    }
    
    if ([self.textView.text stringByReplacingOccurrencesOfString:@"\n" withString:@""].length == 0) {
        [HUDManager showWarningWithText:@"请输入内容!"];
        _submitBtn.enabled=YES;
        return NO;
    }
    return  YES;
}


- (void) popToController{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)actionSheetButtonClickedAtIndex:(NSInteger)index{
    NSUInteger sourceType = 0;
    switch (index) {
            case 2:
            // 相机
            sourceType = UIImagePickerControllerSourceTypeCamera;
            break;
            case 1:
            // 相册
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            break;
            
            case 0:
            [self.actionView dismiss];
            return;
    }
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        
        imagePickerController.delegate = self;
        imagePickerController.automaticallyAdjustsScrollViewInsets = NO;
        imagePickerController.allowsEditing = NO;
        imagePickerController.sourceType = sourceType;
        
        [self presentViewController:imagePickerController animated:YES completion:^{}];
    }else
    {
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 :self.photoArrays.count delegate:self];
        [self presentViewController:imagePickerVc animated:YES completion:nil];
    }
    [self.actionView dismiss];
}
-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSUInteger sourceType = 0;
    switch (buttonIndex) {
            case 0:
            // 相机
            sourceType = UIImagePickerControllerSourceTypeCamera;
            break;
            case 1:
            // 相册
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            break;
            
            case 2:
            
            return;
    }
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        
        imagePickerController.delegate = self;
        imagePickerController.automaticallyAdjustsScrollViewInsets = NO;
        imagePickerController.allowsEditing = NO;
        imagePickerController.sourceType = sourceType;
        
        [self presentViewController:imagePickerController animated:YES completion:^{}];
    }else
    {
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 :self.photoArrays.count delegate:self];
        [self presentViewController:imagePickerVc animated:YES completion:nil];
    }
    
}
- (void)imagePickerControllerDidCancel:(TZImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:^{}];
}



- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets infos:(NSArray<NSDictionary *> *)infos{
    
    for (UIImage *img in photos) {
        NSData *data = UIImageJPEGRepresentation(img,0.01);
        [self.photoArrays addObject:data];
        [self.photoNameArrays addObject:[NSString getMD5WithData:data]];
    }
    [self calculationImageFrame];
}
#pragma mark - image picker delegte
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    [picker dismissViewControllerAnimated:YES completion:^{}];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    NSData *data = UIImageJPEGRepresentation(image,0.01);
    [self.photoArrays addObject:data];
    [self.photoNameArrays addObject:[NSString getMD5WithData:data]];
    [self calculationImageFrame];
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_titleField resignFirstResponder];
    return YES;
}

-(void)keyboardHide:(UITapGestureRecognizer*)tap{
    [_textView resignFirstResponder];
    [_titleField resignFirstResponder];
}

- (void)calculationImageFrame{
    int totalCol = 3;
    int col = 0;
    int row = 0;
    for (int i = 0; i< self.photoArrays.count+1;i++) {
        col = i % totalCol;
        row = i / totalCol;
        if (self.photoArrays.count == 0) {
            self.addImageBtn.hidden=NO;
            self.addImageBtn.frame = CGRectMake(15+col*_itemWH, 300+row*_itemWH, _itemWH, _itemWH);
        }else{
            if (i < self.photoArrays.count) {
                SLPhotoView *view;
                if (i < self.photoViews.count) {
                    view = self.photoViews[i];
                }else{
                    view = [[SLPhotoView alloc] init];
                    [self.photoViews addObject:view];
                    view.photoImageView.image = [UIImage imageWithData:self.photoArrays[i]];
                }
                view.removeBtn.tag = i;
                view.frame = CGRectMake(15+col*_itemWH, 300+row*_itemWH, _itemWH, _itemWH);
                view.delegate = self;
                [self.baseSc addSubview:view];
                if (i == self.photoArrays.count-1) {
                    [self.baseSc setContentSize:CGSizeMake(0, 300+row*_itemWH+_itemWH)];
                    if (300+row*_itemWH+_itemWH > screen_H-64) {
                        [self.baseSc setContentOffset:CGPointMake(0, 300+row*_itemWH+_itemWH-screen_H+64)];
                    }
                }
            }else{
                if (self.photoArrays.count == 9) {
                    self.addImageBtn.hidden=YES;
                }else{
                    self.addImageBtn.hidden=NO;
                    self.addImageBtn.frame = CGRectMake(15+col*_itemWH, 300+row*_itemWH, _itemWH, _itemWH);
                    [self.baseSc setContentSize:CGSizeMake(0, 300+row*_itemWH+_itemWH)];
                    if (300+row*_itemWH+_itemWH > screen_H-64) {
                        [self.baseSc setContentOffset:CGPointMake(0, 300+row*_itemWH+_itemWH-screen_H+64)];
                    }
                }
            }
        }
    }
}
- (void)photoView:(SLPhotoView *)photoView DidClickRemoveButton:(UIButton *)removeBtn{
    [self.photoArrays removeObjectAtIndex:removeBtn.tag];
    [self.photoNameArrays removeObjectAtIndex:removeBtn.tag];
    SLPhotoView *view = self.photoViews[removeBtn.tag];
    [view removeFromSuperview];
    [self.photoViews removeObjectAtIndex:removeBtn.tag];
    [self calculationImageFrame];
}
- (void)dealloc{
    NSLog(@"dea")
}
@end
