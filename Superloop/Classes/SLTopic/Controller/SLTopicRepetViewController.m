//
//  SLTopicRepetViewController.m
//  Superloop
//
//  Created by 张梦川 on 16/5/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTopicRepetViewController.h"
#import "SLtTopicRepetTableViewCell.h"
#import "AppDelegate.h"
#import <UIImageView+WebCache.h>
#import "UILabel+StringFrame.h"
#import "SLTopicDetailViewController.h"
#import "SLTopicModel.h"
#import "SLAPIHelper.h"
#import  <MJRefresh.h>
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "HomePageViewController.h"
#define  cellWH  ((ScreenW - 70 - (cols - 1) * margin) / cols)
@interface SLTopicRepetViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,assign)NSInteger page;
@property (nonatomic,strong)NSMutableArray *arrList;
@end
static NSInteger const cols = 3;
static CGFloat const margin = 5;
@implementation SLTopicRepetViewController{
    
    UITableView *_tableView;
    CGFloat _cellHeight;
}
-(NSInteger)page{
    if (!_page) {
        _page=0;
    }
    return _page;
}
-(NSMutableArray *)arrList{
    if (!_arrList) {
        _arrList=[[NSMutableArray alloc] init];
    }
    return _arrList;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLTopicRepetViewController"];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLTopicRepetViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self  setNav];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,WIDTH, HEIGHT-64) style:UITableViewStylePlain];
    [_tableView   setSeparatorColor:[UIColor   lightGrayColor]];  //设置分割线浅灰色
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    self.view.backgroundColor = SLColor(250, 250, 250);
    _tableView.backgroundColor = SLColor(250, 250, 250);
    [self setUpRefresh];
    [_tableView.mj_header beginRefreshing];
    [_tableView.mj_footer setHidden:YES];
}
-(void)setNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(5, 30, 30, 30);
    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    
    UILabel *titleLab=[[UILabel alloc] initWithFrame:CGRectMake(40, 32, ScreenW-80, 20)];
    titleLab.text=@"动态";
    titleLab.textAlignment=NSTextAlignmentCenter;
    titleLab.font=[UIFont systemFontOfSize:17];
    titleLab.textColor=[UIColor blackColor];
    [navView addSubview:titleLab];
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=[UIColor lightGrayColor];
    [navView addSubview:cutImgView];

}
-(void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setUpRefresh
{
    _tableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    _tableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
}

-(void)loadData{
    self.page=0;
    NSDictionary *params=@{@"page":[NSString stringWithFormat:@"%ld",(long)self.page],@"pager":@"20"};
    
   [SLAPIHelper getTopicsReplies:params success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
      self.arrList = data[@"result"];
       NSLog(@"%@",data[@"result"]);
       if (self.arrList.count== 0) {
           if (!_hasNoView) {
               [self addNetWorkViews:@"暂无动态"];
           }
       }else{
           [_hasNoView removeFromSuperview];
       }
       [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadMessageNum" object:self];
       [_tableView reloadData];
       if (self.arrList.count<20) {
           [_tableView.mj_header endRefreshing];
           [_tableView.mj_footer endRefreshingWithNoMoreData];
           
       }else{
           [_tableView.mj_header endRefreshing];
           [_tableView.mj_footer resetNoMoreData];
       }
       [_tableView.mj_footer setHidden:NO];
   } failure:^(SLHttpRequestError *error) {
       [_tableView.mj_header endRefreshing];
        [self showHUDmessage:@"加载错误"];
   }];
}
-(void)loadMoreData{
    [_tableView.mj_footer setHidden:NO];
    self.page++;
    NSDictionary *params=@{@"page":[NSString stringWithFormat:@"%ld",(long)self.page],@"pager":@"20"};
    [SLAPIHelper getTopicsReplies:params success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSArray *arrList = data[@"result"];
        NSMutableArray *mutableArr=[[NSMutableArray alloc] init];
        [mutableArr addObjectsFromArray:self.arrList];
        [mutableArr addObjectsFromArray:arrList];
        self.arrList=mutableArr;

        [_tableView reloadData];
        if ( arrList.count <20) {
            
            [_tableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            // 结束刷新
            [_tableView.mj_footer endRefreshing];
        }
    } failure:^(SLHttpRequestError *error) {
        [_tableView.mj_footer endRefreshing];
        [self showHUDmessage:@"加载错误"];
    }];

}

#pragma ---代理方法
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.arrList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    _cellHeight =0;
    static NSString *identif = @"tradeCell";
    SLtTopicRepetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identif];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SLtTopicRepetTableViewCell" owner:self options:nil]lastObject];
    }
    NSDictionary *dict = self.arrList[indexPath.row];
    NSDictionary *user = dict[@"user"];
    _cellHeight +=20;
    [cell.headImg sd_setImageWithURL:[NSURL URLWithString:user[@"avatar"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    //图像的点击事件
    cell.headImg.tag=indexPath.row+100;
    cell.headImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [cell.headImg addGestureRecognizer:tapView];
    
    
    NSString *time = [self dateForMat:dict[@"created"]];
    cell.timeLabel.text = time;
    _cellHeight +=cell.headImg.frame.size.height +10;
    // 昵称
    UILabel *nameLable = [[UILabel alloc]init];
    CGSize size = [nameLable boundingRectWithString:user[@"nickname"] withSize:CGSizeMake(WIDTH-110, 20) withFont:16];
    if (size.width+cell.headImg.frame.origin.x + cell.headImg.frame.size.width +10+7+1+7+80>ScreenW) {
        size.width=ScreenW-cell.headImg.frame.origin.x - cell.headImg.frame.size.width -10-7-1-7-80;
    }
   
    if (screen_W < 375) {
        if (size.width >120) {
            size.width = 120;
        }
    }else{
        if (size.width>150) {
            size.width = 150;
        }
    }
    nameLable.frame = CGRectMake(cell.headImg.frame.origin.x + cell.headImg.frame.size.width +10, 25, size.width +10, 20);
    nameLable.text = user[@"nickname"];
    nameLable.font = [UIFont boldSystemFontOfSize:16];
    [cell.contentView addSubview:nameLable];
    // 线
    UIImageView *lineImg= [[UIImageView alloc]initWithFrame:CGRectMake(nameLable.frame.origin.x +nameLable.frame.size.width +7, nameLable.frame.origin.y +2, 1, 16)];
    lineImg.backgroundColor = [UIColor lightGrayColor];
    [cell.contentView addSubview:lineImg];

    // 评论and赞
    UILabel *tipLabel = [[UILabel alloc]init];
    tipLabel.frame = CGRectMake(lineImg.frame.origin.x + 11, nameLable.frame.origin.y, 100, 20);
    NSString *type = dict[@"type"];
    UIView *topicView = [[UIView alloc]init];
    if (type.integerValue == 1 || type.integerValue == 0) {
        tipLabel.text = @"赞了话题";
        topicView.frame = CGRectMake(0, cell.headImg.frame.origin.y+ 41, WIDTH, 50);
    } else if(type.integerValue == 2){
       tipLabel.text = @"评论了话题";
        UILabel *commentLabel = [[UILabel alloc]init];
        CGSize sizeForcomment = [commentLabel boundingRectWithString:dict[@"content"] withSize:CGSizeMake(WIDTH -20, MAXFLOAT)];
        if (sizeForcomment.height>90) {
            sizeForcomment.height=90;
        }
        commentLabel.frame = CGRectMake(10, cell.headImg.frame.origin.y + 41, WIDTH-20, sizeForcomment.height);
        commentLabel.text = dict[@"content"];
        commentLabel.numberOfLines=5;
        commentLabel.font=[UIFont systemFontOfSize:14];
        [cell.contentView addSubview:commentLabel];
        topicView.frame = CGRectMake(0, commentLabel.frame.origin.y+ commentLabel.frame.size.height +10, WIDTH, 50);
        _cellHeight +=commentLabel.frame.size.height +10;
    }else{
        tipLabel.text = @"回复了我的评论";
        UILabel *commentLabel = [[UILabel alloc]init];
        CGSize sizeForcomment = [commentLabel boundingRectWithString:dict[@"content"] withSize:CGSizeMake(WIDTH -20, MAXFLOAT)];
        if (sizeForcomment.height>90) {
            sizeForcomment.height=90;
        }
        commentLabel.frame = CGRectMake(10, cell.headImg.frame.origin.y + 41, WIDTH-20, sizeForcomment.height);
        commentLabel.text = dict[@"content"];
        commentLabel.numberOfLines=5;
        commentLabel.font=[UIFont systemFontOfSize:14];
        [cell.contentView addSubview:commentLabel];
        topicView.frame = CGRectMake(0, commentLabel.frame.origin.y+ commentLabel.frame.size.height +10, WIDTH, 30);
        _cellHeight +=commentLabel.frame.size.height +10;
        UIView *sepView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(commentLabel.frame)+8, screen_W, 0.5)];
        sepView.backgroundColor = SLColor(233, 233, 233);
        [cell.contentView addSubview:sepView];
    }
    
    [cell.contentView addSubview:tipLabel];
    // 话题title
    CGFloat topicLabelHeight =0;
    if (type.integerValue == 3) {
        topicLabelHeight = 20;
        
    }else{
        topicLabelHeight = 40;
    }
    UILabel *topicLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, WIDTH-20, topicLabelHeight)];
    topicLabel.textColor = SLColor(110, 110, 110);
    topicLabel.numberOfLines = 0;
    topicLabel.font = [UIFont systemFontOfSize:14];
    NSDictionary *topic = dict[@"topic"];
    topicLabel.text =topic[@"digest"];
    if (type.integerValue == 0 || type.integerValue == 2) {
        topicLabel.text = dict[@"topic"][@"title"];
    }
    if([[dict allKeys] containsObject:@"reply"])
    {
        NSDictionary *reply = dict[@"reply"];
        topicLabel.text = reply[@"digest"];
    }
    if (topic[@"imgs"]) {
        // 话题图片
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(WIDTH -50, 2, 45, 45)];
        topicLabel.frame = CGRectMake(10, 5, WIDTH-60, 40);
        NSArray *arrUrl = [[NSArray alloc]init];
        arrUrl = topic[@"imgs"];
        NSDictionary *dict =arrUrl[0];
        [imgView sd_setImageWithURL:[NSURL URLWithString:dict[@"url"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
        [topicView addSubview:imgView];
    }
    [topicView addSubview:topicLabel];
    
    
   
    [cell.contentView addSubview:topicView];
    tipLabel.font = [UIFont systemFontOfSize:13];
    tipLabel.textColor = SLColor(182, 182, 182);
    
    
    if (type.integerValue == 3) {
        topicView.backgroundColor = [UIColor whiteColor];
        _cellHeight +=47;
    }else{
        topicView.backgroundColor = SLColor(247, 247, 247);
        _cellHeight +=67;
    }
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, _cellHeight - 17, screen_W, 10)];
    [cell addSubview:separatorView];
    separatorView.backgroundColor = [UIColor whiteColor];
    UIView *separatorView2 = [[UIView alloc] initWithFrame:CGRectMake(0, _cellHeight - 0.5, screen_W, 0.5)];
    [cell addSubview:separatorView2];
    separatorView2.backgroundColor = SLSepatorColor;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(_cellHeight){
        return _cellHeight;
    } else {
        return 50;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.userInteractionEnabled = NO;
    
    NSDictionary *dict = self.arrList[indexPath.row];
     NSLog(@"afsadfads%@",dict);
    NSString *topic_id;
    NSString *type = dict[@"type"];
    if (type.integerValue == 3) {
        NSString *status = dict[@"reply"][@"status"];
        if (status.integerValue == 0) {
            kShowToast(@"该评论已被删除");
            return;
        }
    }
    if([[dict allKeys] containsObject:@"topic"])
    {
        topic_id = dict[@"topic"][@"id"];
        NSString *status = dict[@"topic"][@"status"];
        if (status.integerValue == 0) {
            kShowToast(@"该话题已被删除");
            return;
        }
    }else{
        topic_id = dict[@"reply"][@"topic_id"];
        NSString *status = dict[@"reply"][@"status"];
        if (status.integerValue == 0) {
            kShowToast(@"该话题已被删除");
            return;
        }
    }
    
    [SLAPIHelper getTopics:topic_id success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSLog(@"afsadfads%@",topic_id);
        NSDictionary *dict = data[@"result"];
        SLTopicModel *newmodel = [SLTopicModel mj_objectWithKeyValues:dict];
        cell.userInteractionEnabled = YES;
        SLTopicDetailViewController *topicDetail = [[SLTopicDetailViewController alloc] init];
        topicDetail.model = newmodel;
        //点击时候计算好cell的内容frame
        //头像高度
        topicDetail.cellHeight = 45;
        // 标题高度
        CGFloat textMaxW = ScreenW - 50;
        
        if (newmodel.content.length == 0) {
            topicDetail.cellHeight = 45;
        }
        //渠道每行的高度
        NSString *titleContentextString = @"Text";
        CGFloat titleFontSize = [titleContentextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
        CGFloat titleHeight = [self.title boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
        NSInteger newLineNum = titleHeight / titleFontSize;
        if (newLineNum > 2) {
            newLineNum = 2;
        }
        topicDetail.cellHeight += newLineNum * titleFontSize + 11;
        //内容高度
        NSString *contentTextString = @"Text";
        CGFloat contemtFontSize = [contentTextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
        CGFloat contentHeight= [newmodel.content boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
        NSInteger newContentLineNum = contentHeight / contemtFontSize;
        topicDetail.cellHeight += newContentLineNum * contemtFontSize  +11 +(newContentLineNum - 1) * 5;
        //图片高度
        if (newmodel.imgs.count > 0) {
            topicDetail.cellHeight += ((newmodel.imgs.count - 1) / 3 + 1) * cellWH + (((newmodel.imgs.count - 1) / 3 + 1) - 1) * margin + 8;
        }
        // 底部工具条
        topicDetail.cellHeight += 89;
        [self.navigationController pushViewController:topicDetail animated:YES];
    } failure:^(SLHttpRequestError *failure){
        cell.userInteractionEnabled = YES;
        if (failure.slAPICode==12&&failure.httpStatusCode==404) {
            [HUDManager showWarningWithText:@"该话题已删除"];
        }
    }];
}

-(void)tap:(UIGestureRecognizer *)tap
{
    UIImageView *imgView=(UIImageView *)tap.view;
    NSInteger index=imgView.tag-100;
    NSString *userID=self.arrList[index][@"user"][@"id"];
    
    if ([[NSString stringWithFormat:@"%@", userID] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        HomePageViewController *myView = [[HomePageViewController alloc] init];
        myView.userId=ApplicationDelegate.userId;
        [self.navigationController pushViewController:myView animated:YES];
    }else
    {
        HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
        otherPerson.userId = userID;
        [self.navigationController pushViewController:otherPerson animated:YES];
    }
    
}

// 没有网的制空页面
-(void)addNetWorkViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, ScreenW , ScreenH-64 )];
    
    UILabel *tiplabel = [[UILabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    tiplabel.attributedText = AttributedStr;
    
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    _hasNoView = contentView;
    [self.view addSubview:contentView];
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
