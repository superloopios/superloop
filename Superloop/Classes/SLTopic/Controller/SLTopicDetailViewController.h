//
//  SLTopicDetailViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLBaseTopicViewController.h"
#import "SLTopicDetalModel.h"
#import "SLMyComment.h"
@class SLTopicModel;
@class SLTopicDetailViewController;
@protocol SLTopicDetailViewControllerDelegate <NSObject>
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didSelectModel:(SLTopicModel *)model;
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickPraise:(SLTopicModel *)model;
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickComment:(SLTopicModel *)model;
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickCollection:(SLTopicModel *)model;
@end
@interface SLTopicDetailViewController : SLBaseTopicViewController
@property (nonatomic,weak) id<SLTopicDetailViewControllerDelegate>delegate;
@property (nonatomic, strong) SLTopicModel *model;
@property (nonatomic, strong) NSIndexPath *indexpath;
@property (nonatomic,strong) NSString *has_thumbsuped;
@property (nonatomic, assign) CGFloat cellHeight;//行高
@property (nonatomic,assign) BOOL isComeFromMyComment;
@property (nonatomic,strong) SLMyComment *myComment;
@property (nonatomic,assign) CGFloat iconWidth;  //话题详情按钮的宽度
@property (nonatomic,assign) BOOL isFromSearch;//来自搜索页
@property (nonatomic, copy) NSString *topicId;
@end
