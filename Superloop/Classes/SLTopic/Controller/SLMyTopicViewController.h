//
//  SLMytopicViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "SLBaseTopicViewController.h"

@interface SLMyTopicViewController : SLBaseTopicViewController

@property (nonatomic,copy) NSString *userId;
@property (nonatomic,copy) NSString *isFromMeController;
@property (nonatomic,copy) NSString *BaseUserId;

@end
