//
//  SLCommendViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/5.
//  Copyright © 2016年 Superloop. All rights reserved.
//  发表评论控制器

#import <UIKit/UIKit.h>
#import "SLBaseTableViewController.h"
#import "SLCommentModel.h"

@interface SLCommendViewController : UIViewController
@property (nonatomic, copy) NSString *topicID;
@property (nonatomic, copy) NSString *placerHolder;
@property (nonatomic, copy) NSString *replayContent;
@property (nonatomic,strong)SLCommentModel *selectModel;
@property (nonatomic,copy)void (^commendSuccessBlock)(BOOL isCommendSuccess);
@end
