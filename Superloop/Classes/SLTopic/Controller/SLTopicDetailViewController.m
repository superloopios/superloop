//
//  SLTopicDetailViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/24.
//  Copyright © 2016年 Superloop. All rights reserved.
#import "SLTopicViewCell.h"
#import "SLTopicDetailViewController.h"
#import <Masonry.h>
#import "SLIndexPathCollection.h"
#import "SLTopicCollectionViewCell.h"
#import "SLTopicModel.h"
#import "SLCommentCell.h"
#import "SLSawBtn.h"
#import "UMSocial.h"
#import "AppDelegate.h"
//#import "SLTopicFootView.h"
#import "SLAwardViewController.h"
#import "SLCallingViewController.h"
#import "SLCommendViewController.h"
#import <AFNetworking.h>
#import <MJExtension.h>
#import "SLCommentModel.h"
#import "HomePageViewController.h"
#import "SLTopicDetalModel.h"

#import "SLLoginViewControllerManger.h"
#import "SLAPIHelper.h"
#import "SLCommentObserve.h"
#import "SLMessageManger.h"
#import  <MJRefresh.h>
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "SLFirstViewController.h"
#import <UIImageView+WebCache.h>
#import "UMSocialData.h"
#import "SLDetailView.h"

#import "SLActionView.h"
#import <Photos/Photos.h>
#import "MWPhotoBrowser.h"
#import "SLShareView.h"
#import "SLShareModel.h"
#import "SLNavgationViewController.h"
#import "SLTopicDetailView.h"
#import "SLTopicDetailTopView.h"
#import "SLTopicDetailFooterView.h"

#import "SLTopicDetailCommentCell.h"

@interface SLTopicDetailViewController ()<UITableViewDelegate,UITableViewDataSource, SLTopicViewCellDelegate,UIActionSheetDelegate,UIAlertViewDelegate,UMSocialUIDelegate,SLDetailViewDelegate,SLActionViewDelegate,SLTopicDetailViewDelegate,SLTopicDetailFooterViewDelegate,SLTopicDetailTopViewDelegate,SLTopicDetailCommentCellDelegate>
@property (nonatomic,strong) NSMutableArray *comments; /**评论列表数组*/
@property (nonatomic,assign) CGFloat imageWH;
@property (nonatomic,assign) BOOL textIsNull;
@property (nonatomic,assign) CGFloat heght;
@property (nonatomic,assign) NSInteger errorCode;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) SLCommentObserve *comObs;
@property (nonatomic,copy)   NSString *numOfCommentStr;
//@property (nonatomic,strong) SLTopicFootView *footView;
@property (nonatomic,strong) UIAlertView *alert;
@property (nonatomic,strong) NSDictionary *topicDict;
@property (nonatomic,strong) SLTopicModel *newmodel;
@property (nonatomic,assign) NSInteger page;
@property (nonatomic,strong) NSDictionary *topicDataDict;
@property (nonatomic,strong) UIImageView *imgVw;
@property (nonatomic,strong) NSString *urlStr;  //分享的地址
@property (nonatomic,strong) UIImage *image;
@property (nonatomic,strong) UITableView *topicDetailTableView;
@property (nonatomic,strong) SLTopicCollectionViewCell * cellCollectionViewCell;
@property (nonatomic,assign) NSIndexPath *index;
@property (nonatomic,strong) SLActionView *actionView;
@property (nonatomic,strong) SLCommentModel *selectModel;
@property (nonatomic,strong) NSIndexPath *selectIndexPath;
@property (nonatomic,strong) NSMutableArray *photos; //图片数组
@property (nonatomic,strong) UIButton *deleteButton;
@property (nonatomic,strong) NSIndexPath *deleteIndexPath;
@property (nonatomic,strong) UIView *navView;
@property (nonatomic,strong) SLTopicDetailFooterView *topicDetailFooterView;
@property (nonatomic,strong) SLTopicDetailTopView *topicDetailTopView;
@end
@implementation SLTopicDetailViewController{
    CGFloat originalLocation;
}

- (SLActionView *)actionView{
    if (!_actionView) {
        _actionView = [[SLActionView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _actionView.titleArr = @[@"取消",@"复制",@"回复",];
        _actionView.delegate = self;
        [self.view addSubview:_actionView];
    }
    return _actionView;
}

-(NSInteger)page{
    if (!_page) {
        _page=0;
    }
    return _page;
}
-(NSMutableArray *)photos{
    if (!_photos) {
        _photos=[NSMutableArray new];
    }
    return _photos;
}
-(NSDictionary *)topicDataDict{
    if (!_topicDataDict) {
        _topicDataDict=[[NSDictionary alloc] init];
    }
    return _topicDataDict;
}
-(UITableView *)topicDetailTableView{
    if (!_topicDetailTableView) {
        _topicDetailTableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 20, ScreenW, ScreenH-20) style:UITableViewStylePlain];
        _topicDetailTableView.contentInset = UIEdgeInsetsMake(44, 0, 49, 0);
        [_topicDetailTableView registerClass:[SLTopicDetailCommentCell class] forCellReuseIdentifier:@"CommentCell"];
        _topicDetailTableView.delegate = self;
        _topicDetailTableView.dataSource = self;
        self.topicDetailTableView.separatorStyle = UITableViewCellSelectionStyleNone;
        [self.view addSubview:_topicDetailTableView];
    }
    return _topicDetailTableView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLTopicDetailViewController"];
    if (self.isFromSearch) {
        [self loadData];
    }
    if (ApplicationDelegate.userId) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }else{
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLTopicDetailViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.topicDetailTableView.scrollsToTop=YES;
    self.view.backgroundColor = [UIColor whiteColor];
    self.topicDetailTableView.backgroundColor=SLColor(244, 244, 244);
    
    if (!self.model) {
        [self loadData];
    }
    [self setUpRefresh];
    if (!self.isFromSearch) {
        [self setUpTableView];
    }
    [self.topicDetailTableView.mj_header beginRefreshing];
    [self.topicDetailTableView.mj_footer setHidden:YES];
    
    //监听评论变化
    _comObs=[[SLCommentObserve alloc] init];
    [_comObs addObserver:self forKeyPath:@"numOfComment" options:NSKeyValueObservingOptionNew context:nil];
    _alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
    
    NSString *imageUrl = self.model.user[@"avatar"];
    NSURL *url = [NSURL URLWithString:imageUrl];
    UIImageView *imgVw= [[UIImageView alloc] init];
    imgVw.frame = CGRectMake(-200, -200, 50, 50);
    [self.view addSubview:imgVw];
    [imgVw sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    self.imgVw = imgVw;
    
}
- (void)setUpUI{
    if (self.model) {
        if (!self.topicDetailTopView) {
            SLTopicDetailTopView *topicDetailTopView = [[SLTopicDetailTopView alloc] initWithFrame:CGRectMake(0, 20, ScreenW, 44)];
            topicDetailTopView.topicModel = self.model;
            topicDetailTopView.delegate = self;
            self.topicDetailTopView = topicDetailTopView;
            [self.view addSubview:topicDetailTopView];
            [self.view bringSubviewToFront:self.topicDetailTopView];
        }
        if (!self.topicDetailFooterView) {
            SLTopicDetailFooterView *topicDetailFooterView = [[SLTopicDetailFooterView alloc] initWithFrame:CGRectMake(0, ScreenH-49, ScreenW, 49)];
            topicDetailFooterView.delegate = self;
            topicDetailFooterView.topicModel = self.model;
            self.topicDetailFooterView = topicDetailFooterView;
            [self.view addSubview:topicDetailFooterView];
            if ([self.model.has_thumbsuped isEqualToString:@"1"]) {
                topicDetailFooterView.praiseBtn.selected = YES;
            }else{
                topicDetailFooterView.praiseBtn.selected = NO;
            }
            if ([self.model.has_collected isEqualToString:@"1"]) {
                topicDetailFooterView.collectionBtn.selected = YES;
            }else{
                topicDetailFooterView.collectionBtn.selected = NO;
            }
            
            if (self.model.replies_cnt) {
                NSInteger repliesCount = [self.model.replies_cnt integerValue];
                if (repliesCount > 9999) {
                    topicDetailFooterView.commentLab.text = [NSString stringWithFormat:@"%ld万",repliesCount/10000];
                }else{
                    topicDetailFooterView.commentLab.text = [NSString stringWithFormat:@"%ld",repliesCount];
                }
            }else{
                topicDetailFooterView.commentLab.text = [NSString stringWithFormat:@"%d",0];
            }
            if (self.model.thumbups_cnt) {
                NSInteger repliesCount = [self.model.thumbups_cnt integerValue];
                if (repliesCount > 9999) {
                    topicDetailFooterView.praiseLab.text = [NSString stringWithFormat:@"%ld万",repliesCount/10000];
                }else{
                    topicDetailFooterView.praiseLab.text = [NSString stringWithFormat:@"%ld",repliesCount];
                }
            }else{
                topicDetailFooterView.praiseLab.text = [NSString stringWithFormat:@"%d",0];
            }
        }
    }
}
- (void)showFootViewByAnimations{
    [UIView animateWithDuration:1.0 animations:^{
        self.topicDetailFooterView.frame = CGRectMake(0, ScreenH - 49, ScreenW, 49);
    }];
}
- (void)hideFootViewByAnimations{
    [UIView animateWithDuration:1.0 animations:^{
        self.topicDetailFooterView.frame = CGRectMake(0, ScreenH, ScreenW, 49);
    }];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y > originalLocation && scrollView.contentOffset.y > 0) {//向上滑动
        [self hideFootViewByAnimations];
    }else if (scrollView.contentOffset.y < originalLocation && scrollView.contentOffset.y >= -44){//向下滑动
        [self showFootViewByAnimations];
    }
    originalLocation = scrollView.contentOffset.y;//将当前位移变成缓存位移
}
- (void)setUpTableView{
    [self setUpUI];
    if (self.model) {
        SLTopicDetailView *topicDetailView = [[SLTopicDetailView alloc] init];
        topicDetailView.topicModel = self.model;
        topicDetailView.delegate = self;
        topicDetailView.frame = CGRectMake(0, 0, ScreenW, topicDetailView.topicHeight);
        self.topicDetailTableView.tableHeaderView = topicDetailView;
    }
}
#pragma mark - SLTopicDetailTopViewDelegate
- (void)SLTopicDetailTopView:(SLTopicDetailTopView *)topicDetailTopView didSelectNicknameWithModel:(SLTopicModel *)topicModel{
    HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
    otherPerson.userId = topicModel.user[@"id"];
    [self.navigationController pushViewController:otherPerson animated:YES];
}
- (void)SLTopicDetailTopView:(SLTopicDetailTopView *)topicDetailTopView didSelectWithButton:(UIButton *)shareBtn{
    [self share];
}
#pragma mark - SLTopicDetailViewDelegate
- (void)SLTopicDetailView:(SLTopicDetailView *)topicDetailView didSelectCollectionByMWPhotoBrowser:(MWPhotoBrowser *)browser WithModel:(SLTopicModel *)topicModel{
    [self presentViewController:browser animated:YES completion:nil];
}
#pragma mark - SLTopicDetailFooterViewDelegate
- (void)SLTopicDetailFooterView:(SLTopicDetailFooterView *)topicDetailFooterView didSelectWithBackButton:(UIButton *)selectBtn{
    if (ApplicationDelegate.userId) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        SLFirstViewController *vc = [[SLFirstViewController alloc] init];
        SLNavgationViewController *nav = [[SLNavgationViewController alloc] initWithRootViewController:vc];
        [UIApplication sharedApplication].keyWindow.rootViewController = nav;
    }
}
- (void)SLTopicDetailFooterView:(SLTopicDetailFooterView *)topicDetailFooterView didSelectWithCommentButton:(UIButton *)selectBtn label:(UILabel *)selectLab{
    if (!ApplicationDelegate.Basic) {
        //判断是否是登录成功的状态
        [_alert show];
        return;
    }
    SLCommendViewController *commendView = [[SLCommendViewController alloc] init];
    commendView.topicID = self.model.id;
    __weak typeof(self) weakSelf = self;
    commendView.commendSuccessBlock=^(BOOL isCOmmendSuccess){
        if (isCOmmendSuccess) {
            [weakSelf.topicDetailTableView.mj_header beginRefreshing];
        }
    };
    [self presentViewController:commendView animated:YES completion:nil];
}
- (void)SLTopicDetailFooterView:(SLTopicDetailFooterView *)topicDetailFooterView didSelectWithPraiseButton:(UIButton *)selectBtn label:(UILabel *)selectLab topicModel:(SLTopicModel *)topicModel{
    if (!ApplicationDelegate.Basic) {
        [_alert show];
        return;
    }
    __block  NSInteger count = [self.model.thumbups_cnt integerValue];
    NSDictionary *parameter = @{@"id":self.model.id};
    if ([topicModel.has_thumbsuped isEqualToString:@"0"]) {
        selectBtn.enabled=NO;
        [SLAPIHelper dGood:parameter method:Post success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            selectBtn.enabled=YES;
            selectBtn.selected = YES;
            count ++ ;
            if (count>9999) {
                self.topicDetailFooterView.praiseLab.text = [NSString stringWithFormat:@"%ld万", (long)count/10000];
            }else{
                self.topicDetailFooterView.praiseLab.text = [NSString stringWithFormat:@"%ld", (long)count];
            }
            self.model.thumbups_cnt=[NSString stringWithFormat:@"%ld",(long)count];
            self.model.has_thumbsuped=[NSString stringWithFormat:@"%d",1];
            [HUDManager showWarningWithText:@"点赞成功!"];
            if ([self.delegate respondsToSelector:@selector(SLTopicDetailViewController:didClickPraise:)]) {
                [self.delegate SLTopicDetailViewController:self didClickPraise:self.model];
            }
        } failure:^(SLHttpRequestError *error) {
            selectBtn.enabled=YES;
            if (error.httpStatusCode==404&&error.slAPICode==12) {
                [HUDManager showWarningWithText:@"点赞失败，话题已被删除!"];
            }else if (error.httpStatusCode==409&&error.slAPICode==1) {
                [HUDManager showWarningWithText:@"你已赞过该话题，请勿重复点赞!"];
                selectBtn.selected = YES;
                count ++ ;
                if (count>9999) {
                    self.topicDetailFooterView.praiseLab.text = [NSString stringWithFormat:@"%ld万", (long)count/10000];
                }else{
                    self.topicDetailFooterView.praiseLab.text = [NSString stringWithFormat:@"%ld", (long)count];
                }
                topicModel.thumbups_cnt=[NSString stringWithFormat:@"%ld",count];
                topicModel.has_thumbsuped=[NSString stringWithFormat:@"%d",1];
            }
            else{
                [HUDManager showWarningWithText:@"点赞失败!"];
            }
        }];
    }
    
    if([topicModel.has_thumbsuped isEqualToString:@"1"]){
        
        selectBtn.enabled=NO;
        //取消单赞
        [SLAPIHelper dGood:parameter method:Delete success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            selectBtn.enabled=YES;
            selectBtn.selected = NO;
            count --;
            if (count>9999) {
                self.topicDetailFooterView.praiseLab.text = [NSString stringWithFormat:@"%ld万", (long)count/10000];
            }else{
                self.topicDetailFooterView.praiseLab.text = [NSString stringWithFormat:@"%ld", (long)count];
            }
            self.model.thumbups_cnt=[NSString stringWithFormat:@"%ld",(long)count];
            self.model.has_thumbsuped=[NSString stringWithFormat:@"%d",0];
            [HUDManager showWarningWithText:@"点赞取消!"];
            if ([self.delegate respondsToSelector:@selector(SLTopicDetailViewController:didClickPraise:)]) {
                [self.delegate SLTopicDetailViewController:self didClickPraise:self.model];
            }
        } failure:^(SLHttpRequestError *error) {
            selectBtn.enabled=YES;
            if (error.httpStatusCode==404&&error.slAPICode==12) {
                [HUDManager showWarningWithText:@"点赞取消失败，话题已被删除!"];
            }else if (error.httpStatusCode==409&&error.slAPICode==1) {
                selectBtn.selected = NO;
                [HUDManager showWarningWithText:@"你已取消点赞，请勿重复操作!"];
                count --;
                if (count>9999) {
                    self.topicDetailFooterView.praiseLab.text = [NSString stringWithFormat:@"%ld万", (long)count/10000];
                }else{
                    self.topicDetailFooterView.praiseLab.text = [NSString stringWithFormat:@"%ld", (long)count];
                }
                topicModel.thumbups_cnt=[NSString stringWithFormat:@"%ld",(long)count];
                topicModel.has_thumbsuped=[NSString stringWithFormat:@"%d",0];
            }else{
                [HUDManager showWarningWithText:@"点赞取消失败!"];
            }
        }];
    }
}
- (void)SLTopicDetailFooterView:(SLTopicDetailFooterView *)topicDetailFooterView didSelectWithCallButton:(UIButton *)selectBtn{
    if (!ApplicationDelegate.Basic) {
        //判断是否是登录成功的状态
        [_alert show];
        return;
    }
    if ([[NSString stringWithFormat:@"%@", self.model.user[@"id"]] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        [HUDManager showWarningWithText:@"你不能对自己打电话！"];
        return;
    }
    self.actionView.titleArr = @[@"取消",@"即时消息",@"电话联系",];
    self.actionView.tag = 100;
    [self.view addSubview:self.actionView];
}
- (void)SLTopicDetailFooterView:(SLTopicDetailFooterView *)topicDetailFooterView didSelectWithCollectionButton:(UIButton *)selectBtn{
    if (!ApplicationDelegate.Basic) {
        //判断是否是登录成功的状态
        [_alert show];
        return;
    }
    
    if ([self.model.has_collected isEqualToString:@"0"]) {
        selectBtn.enabled=NO;
        [SLAPIHelper collectTopics:self.model.id method:Post success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            selectBtn.enabled=YES;
            self.comments = [SLCommentModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            selectBtn.selected=YES;
            [HUDManager showWarningWithText:@"收藏成功!"];
            self.model.has_collected=[NSString stringWithFormat:@"%d",1];
            //收藏成功之后发送通知告知收藏控制器刷新数据
            if ([self.delegate respondsToSelector:@selector(SLTopicDetailViewController:didClickCollection:)]) {
                [self.delegate SLTopicDetailViewController:self didClickCollection:self.model];
            }
        } failure:^(SLHttpRequestError *error) {
            selectBtn.enabled=YES;
            [HUDManager showWarningWithText:@"收藏失败!"];
            if (error.httpStatusCode == 403) {
                self.errorCode = 403;
            }
        }];
    }else{
        //取消收藏
        selectBtn.enabled=NO;
        [SLAPIHelper collectTopics:self.model.id method:Delete success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            selectBtn.enabled=YES;
            selectBtn.selected=NO;
            [HUDManager showWarningWithText:@"收藏已取消!"];
            self.model.has_collected=[NSString stringWithFormat:@"%d",0];
            //收藏已取消成功之后发送通知告知收藏控制器刷新数据
            if ([self.delegate respondsToSelector:@selector(SLTopicDetailViewController:didClickCollection:)]) {
                [self.delegate SLTopicDetailViewController:self didClickCollection:self.model];
            }
        } failure:^(SLHttpRequestError *error) {
            selectBtn.enabled=YES;
            [HUDManager showWarningWithText:@"收藏取消失败！"];
            if (error.httpStatusCode == 403) {
                self.errorCode = 403;
            }
        }];
    }
    
}

- (void)setUpRefresh{
    self.topicDetailTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadCommentData)];
    self.topicDetailTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreCommentData)];
}
// 加载数据
-(void)loadData{
    NSString *topicId;
    if (self.model) {
        topicId = self.model.id;
    }else{
        topicId = self.topicId;
    }
    [SLAPIHelper getTopics:topicId success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSDictionary *dict = data[@"result"];
        self.topicDataDict=dict;
        SLTopicModel *newmodel = [SLTopicModel mj_objectWithKeyValues:dict];
        self.model=newmodel;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.isFromSearch) {
                [self setUpTableView];
            }
        });
        if (self.topicId) {
            [self setUpTableView];
        }
        //值变化
        [_comObs setValue:[NSString stringWithFormat:@"%@",self.model.replies_cnt] forKey:@"numOfComment"];
        
        [self.topicDetailTableView reloadData];
    } failure:nil];
}

/**获取评论列表*/
- (void)loadCommentData{
    if (self.isFromSearch) {
        self.isFromSearch=NO;
    }else{
        [self loadData];
    }
    self.page=0;
    NSString *topicId;
    if (self.model) {
        topicId = self.model.id;
    }else{
        topicId = self.topicId;
    }
    NSDictionary *parameters =  @{@"id":topicId,@"page":[NSString stringWithFormat:@"%ld",(long)self.page ],@"pager":@"20"};
    [SLAPIHelper topicsReplies:parameters method:Get success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        self.comments = [SLCommentModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
        if (self.comments.count==0) {
            UILabel *noCommentLab=[[UILabel alloc] initWithFrame:CGRectMake(0, 20, ScreenW, 40)];
            noCommentLab.text=@"暂无评论内容";
            noCommentLab.font=[UIFont systemFontOfSize:14];
            noCommentLab.textAlignment=NSTextAlignmentCenter;
            noCommentLab.textColor=[UIColor grayColor];
            self.topicDetailTableView.tableFooterView=noCommentLab;
            [self.topicDetailTableView.mj_footer setHidden:YES];
        }else{
            UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 0.5)];
            imgView.backgroundColor=SLColor(240, 240, 240);
            self.topicDetailTableView.tableFooterView=imgView;
            [self.topicDetailTableView.mj_footer setHidden:YES];
        }
        [self.topicDetailTableView reloadData];
        if (self.comments.count<20) {
            [self.topicDetailTableView.mj_header endRefreshing];
            [self.topicDetailTableView.mj_footer setHidden:YES];
        }else{
            [self.topicDetailTableView.mj_footer setHidden:NO];
            [self.topicDetailTableView.mj_header endRefreshing];
            [self.topicDetailTableView.mj_footer resetNoMoreData];
        }
        
    } failure:^(SLHttpRequestError *failure) {
        if (failure.slAPICode==12&&failure.httpStatusCode==404) {
            if ([self.delegate respondsToSelector:@selector(SLTopicDetailViewController:didSelectModel:)]) {
                [self.delegate SLTopicDetailViewController:self didSelectModel:self.model];
            }
            [HUDManager showWarningWithText:@"该话题已删除"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedTopics" object:self.model]; //删除话题
            [self.navigationController popViewControllerAnimated:YES];
        }
        [self.topicDetailTableView.mj_header endRefreshing];
    }];
}
-(void)loadMoreCommentData{
    [self.topicDetailTableView.mj_footer setHidden:NO];
    self.page++;
    NSDictionary *parameters =  @{@"id":self.model.id,@"page":[NSString stringWithFormat:@"%ld",(long)self.page ],@"pager":@"20"};
    [SLAPIHelper topicsReplies:parameters method:Get success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSArray *comments = [SLCommentModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
        [self.comments addObjectsFromArray:comments];
        [self.topicDetailTableView reloadData];
        if ( comments.count <20) {
            [self.topicDetailTableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            // 结束刷新
            [self.topicDetailTableView.mj_footer endRefreshing];
        }
    } failure:^(SLHttpRequestError *failure) {
        [self.topicDetailTableView.mj_footer endRefreshing];
    }];
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    _numOfCommentStr=[_comObs valueForKey:@"numOfComment"];
    NSInteger commentCount = [_numOfCommentStr integerValue];
    if (commentCount > 9999) {
        self.topicDetailFooterView.commentLab.text = [NSString stringWithFormat:@"%ld万",commentCount/10000];
    }else{
        self.topicDetailFooterView.commentLab.text = [NSString stringWithFormat:@"%ld",(long)commentCount];
    }
    if ([self.delegate respondsToSelector:@selector(SLTopicDetailViewController:didClickComment:)]) {
        [self.delegate SLTopicDetailViewController:self didClickComment:self.model];
    }
}
#pragma tableViewDelegate;
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.comments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLTopicDetailCommentCell *commentCell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
    commentCell.selectionStyle = UITableViewCellSelectionStyleNone;
    commentCell.commentModel = self.comments[indexPath.row];
    commentCell.indexPath = indexPath;
    commentCell.delegate = self;
    return commentCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!ApplicationDelegate.Basic) {
        //判断是否是登录成功的状态
        [_alert show];
        return;
    }
    self.selectModel = self.comments[indexPath.row];
    self.selectIndexPath = indexPath;
    if ([[NSString stringWithFormat:@"%@", self.selectModel.user_id] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        self.actionView.titleArr = @[@"取消",@"删除",@"复制",];
    }else{
        self.actionView.titleArr = @[@"取消",@"复制",@"回复",];
    }
    self.actionView.tag = 200;
    [self.view addSubview:self.actionView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLCommentModel *commentModel = self.comments[indexPath.row];
    return commentModel.commentCellHeight ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 23;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *sectionHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 23)];
    sectionHeader.backgroundColor = SLColor(240, 240, 240);
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 5, 13, 13)];
    imgView.image = [UIImage imageNamed:@"topicDetailCommentArea"];
    [sectionHeader addSubview:imgView];
    UILabel * commentAreaLab = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 100, 23)];
    commentAreaLab.font = [UIFont systemFontOfSize:13];
    commentAreaLab.textColor = UIColorFromRGB(0xff5a5f);
    commentAreaLab.text = @"评论区";
    [sectionHeader addSubview:commentAreaLab];
    return sectionHeader;
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
//分享按钮
- (void)share{
    SLShareModel *shareModel=[[SLShareModel alloc] init];
    shareModel.shareUrl=[KEY_Share_Topic_url stringByAppendingString:[NSString stringWithFormat:@"id=%@",self.model.id]];
    shareModel.shareImage=self.imgVw.image;
    shareModel.shareTitle=self.model.title;
    shareModel.shareContent=[NSString stringWithFormat:@"%@",self.model.content];
    
    SLShareView *shareView = [[SLShareView alloc] init];
    shareView.frame = CGRectMake(0, 0, ScreenW, ScreenH+190);
    [shareView showViewWithModel:shareModel viewController:self];
    [[UIApplication sharedApplication].keyWindow addSubview:shareView];
}
#pragma mark -UIActionSheet
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        //先加载价格资费数据
        [self loadPriceData];
    }if (buttonIndex==1) {
        NSString *chatName = self.model.user[@"id"];
        YWPerson *person = [[YWPerson alloc] initWithPersonId:chatName.description];
        [SLMessageManger sharedInstance].isComeFromTopicDetailVc = YES;
        ApplicationDelegate.imTitlename = self.model.user[@"nickname"];
        [[SLMessageManger sharedInstance] openConversationViewControllerWithPerson:person fromNavigationController:self.navigationController];
    }
}
// 获取通话资费价格
- (void)loadPriceData{
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow
                          withText:@""];
    [SLAPIHelper getPriceData:self.model.user[@"id"] success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        [HUDManager hideHUDView];
        [self.topicDetailTableView reloadData];
        self.price =  [NSString stringWithFormat:@"%@",data[@"result"][@"price"]];
        SLCallingViewController *callView = [[SLCallingViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:callView];
        callView.user=data[@"result"];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        if (failure.slAPICode==-1) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [HUDManager showWarningWithText:@"请检查网络状况"];
            });
        }
    }];
}
#pragma mark - SLTopicDetailCommentCellDelegate
- (void)SLTopicDetailCommentCell:(SLTopicDetailCommentCell *)topicDetailCommentCell didSelectDeleteBtn:(UIButton *)deleteBtn indexPath:(NSIndexPath *)indexPath commentModel:(SLCommentModel *)commentModel{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"删除评论?" delegate:self cancelButtonTitle:@"不了" otherButtonTitles:@"好", nil];
    alert.tag = 100;
    self.deleteButton = deleteBtn;
    self.deleteIndexPath = indexPath;
    alert.delegate = self;
    [alert show];
}
- (void)SLTopicDetailCommentCell:(SLTopicDetailCommentCell *)topicDetailCommentCell didSelectIconIndexPath:(NSIndexPath *)indexPath commentModel:(SLCommentModel *)commentModel{
    SLCommentModel *model = self.comments[indexPath.row];
    if ([[NSString stringWithFormat:@"%@", model.user[@"id"]] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        HomePageViewController *myView = [[HomePageViewController alloc] init];
        myView.userId=ApplicationDelegate.userId;
        [self.navigationController pushViewController:myView animated:YES];
    }else{
        HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
        otherPerson.userId = model.user[@"id"];
        [self.navigationController pushViewController:otherPerson animated:YES];
    }
}
- (void)SLTopicDetailCommentCell:(SLTopicDetailCommentCell *)topicDetailCommentCell attributedLabel:(NSIndexPath *)indexPath commentModel:(SLCommentModel *)commentModel{
    SLCommentModel *model = self.comments[indexPath.row];
    if ([[NSString stringWithFormat:@"%@", model.reply_to_user[@"id"]] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        HomePageViewController *myView = [[HomePageViewController alloc] init];
        myView.userId=ApplicationDelegate.userId;
        [self.navigationController pushViewController:myView animated:YES];
    }else{
        HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
        otherPerson.userId = model.reply_to_user[@"id"];
        [self.navigationController pushViewController:otherPerson animated:YES];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 100) {
        if (buttonIndex==1) {
            self.deleteButton.selected = !self.deleteButton.selected;
            SLCommentModel *model = self.comments[self.deleteIndexPath.row];
            //请求删除评论操作
            [SLAPIHelper deleteComment:model.id success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                //删除评论成功
                [self.comments removeObjectAtIndex:self.deleteIndexPath.row];
                //        [self.topicDetailTableView deleteSections:[NSIndexSet indexSetWithIndex:indexpath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
                [self loadCommentData];
                self.deleteButton.enabled=YES;
                
            } failure:^(SLHttpRequestError *error) {
                self.deleteButton.enabled=YES;
                
                [HUDManager showWarningWithText:@"删除失败!"];
            }];
        }else{
            self.deleteButton.enabled=YES;
        }
    }else if(alertView.tag == 200){
        if (buttonIndex==1) {
            [SLAPIHelper deleteComment:self.selectModel.id success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                //删除评论成功
                [self.actionView dismiss];
                [self.comments removeObjectAtIndex:self.indexpath.row];
                [self loadCommentData];
                [self.topicDetailTableView reloadData];
                
            } failure:^(SLHttpRequestError *error) {
                [HUDManager showWarningWithText:@"删除失败!"];
            }];
        }
    }else{
        if (buttonIndex==1) {
            SLFirstViewController *vc=[[SLFirstViewController alloc] init];
            SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
            [self presentViewController:nav animated:YES completion:nil];
        }
    }
    
}
- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_comObs removeObserver:self forKeyPath:@"numOfComment"];  //移除
}
- (void)actionSheetButtonClickedAtIndex:(NSInteger)index{
    if (self.actionView.tag == 100) {
        if (index==2) {
            //先加载价格资费数据
            [self loadPriceData];
        }if (index==1) {
            NSString *chatName = self.model.user[@"id"];
            YWPerson *person = [[YWPerson alloc] initWithPersonId:chatName.description];
            ApplicationDelegate.imTitlename = self.model.user[@"nickname"];
            [SLMessageManger sharedInstance].isComeFromTopicDetailVc = YES;
            [[SLMessageManger sharedInstance] openConversationViewControllerWithPerson:person fromNavigationController:self.navigationController];
        }
        [self.actionView dismiss];
        
    }else if (self.actionView.tag == 200) {
        if ([[NSString stringWithFormat:@"%@", self.selectModel.user_id] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
            if (index == 0) {
                
            }else if (index == 1){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"删除评论?" delegate:self cancelButtonTitle:@"不了" otherButtonTitles:@"好", nil];
                alert.tag = 200;
                alert.delegate = self;
                [alert show];
                
            }else if (index == 2){
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = self.selectModel.content;
                
                kShowToast(@"复制成功");
            }
        }else{
            if (index == 0) {
                
            }else if (index == 1){
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = self.selectModel.content;
                
                kShowToast(@"复制成功");
            }else if (index == 2){
                [self didCommendClicked:[NSString stringWithFormat:@"回复%@",self.selectModel.user[@"nickname"]]];
            }
        }
        
        [self.actionView dismiss];
    }
    
}
- (void)didCommendClicked:(NSString *)placerHolder{
    if (!ApplicationDelegate.Basic) {
        //判断是否是登录成功的状态
        [_alert show];
        return;
    }
    SLCommendViewController *commendView = [[SLCommendViewController alloc] init];
    commendView.topicID = self.model.id;
    commendView.placerHolder = placerHolder;
    commendView.selectModel = self.selectModel;
    commendView.replayContent = self.selectModel.content;
    __weak typeof(self) weakSelf = self;
    commendView.commendSuccessBlock=^(BOOL isCOmmendSuccess){
        if (isCOmmendSuccess) {
            [weakSelf.topicDetailTableView.mj_header beginRefreshing];
        }
    };
    [self presentViewController:commendView animated:YES completion:nil];
}


@end
