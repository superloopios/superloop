//
//  SLAttenTopicViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCollectionTopicViewController.h"
#import "SLTopicDetailViewController.h"
#import "AppDelegate.h"
#import  <MJRefresh.h>
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "SLAPIHelper.h"
#import "SLTopicView.h"
#import "SLTopicModel.h"
#import <Photos/Photos.h>
#import "MWPhotoBrowser.h"
@interface SLCollectionTopicViewController ()<SLTopicViewDelegate,MWPhotoBrowserDelegate>
@property (nonatomic,copy) NSString *topicKey;
@property (nonatomic,assign) NSInteger collectionPage;
@property (nonatomic,strong) UIView *notFansView;
@property (nonatomic,assign) BOOL hasNetwork;
@property (nonatomic,strong) SLTopicView *collectionBaseTopicView;
@property (nonatomic,strong) SLTopicModel *selectedModel;
@property (nonatomic,assign)NSInteger selectedIndex;
@property (nonatomic,strong)NSMutableArray *photos;
@end
@implementation SLCollectionTopicViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLCollectionTopicViewController"];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLCollectionTopicViewController"];
}
#pragma mark - 懒加载
-(NSString *)topicKey{
    if (!_topicKey) {
        _topicKey=[NSString new];
    }
    return _topicKey;
}
-(NSInteger)collectionPage{
    if (!_collectionPage) {
        _collectionPage=0;
    }
    return _collectionPage;
}

-(SLTopicView *)collectionBaseTopicView{
    if (!_collectionBaseTopicView) {
        _collectionBaseTopicView=[[SLTopicView alloc] initWithFrame:CGRectMake(0, 64, ScreenW, ScreenH-64)];
        _collectionBaseTopicView.delegate=self;
        [self.view addSubview:_collectionBaseTopicView];
    }
    return _collectionBaseTopicView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.view.backgroundColor = SLColor(244, 244, 244);

    [self setUpRefresh];
    
    [self setUpNav];
    if (self.isFromMeController) {
        self.navigationItem.title = @"收藏话题";
        self.topicKey = [URL_ROOT_PATIENT stringByAppendingString:@"/users/collections/topics"];
        [self getCacheData];
        [self.collectionBaseTopicView.slTableView.mj_header beginRefreshing];
        [self.collectionBaseTopicView.slTableView.mj_footer setHidden:YES];
    }
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"收藏话题";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 上下拉刷新
- (void)setUpRefresh {
    self.collectionBaseTopicView.slTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
    self.collectionBaseTopicView.slTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreData)];
}

#pragma mark - 获取缓存数据
- (void)getCacheData {
    //获取旧的数据
    NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:self.topicKey];
    if (configData) {
        NSDictionary *configDict = nil;
        configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
        self.collectionBaseTopicView.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
        [self.collectionBaseTopicView.slTableView reloadData];
        
        if (self.collectionBaseTopicView.slDataSouresArr.count<20) {
            [self.collectionBaseTopicView.slTableView.mj_header endRefreshing];
            [self.collectionBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            [self.collectionBaseTopicView.slTableView.mj_header endRefreshing];
            [self.collectionBaseTopicView.slTableView.mj_footer resetNoMoreData];
        }
    } else {
        [self getData];
    }
}
#pragma mark -获取热门话题的数据
-(void)getData{
    self.collectionPage=0;
    if (self.isFromMeController) {
         NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:@"/users/collections/topics"];
        NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.collectionPage],@"pager":@"20"};
        self.topicKey=topicUrl;
        [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            self.collectionBaseTopicView.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            NSLog(@"-------%@",data);
            //缓存
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:self.topicKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if (self.collectionBaseTopicView.slDataSouresArr.count==0) {
                [self.collectionBaseTopicView.slTableView.mj_footer setHidden:YES];
                [self addNotFansView:@"这里什么也没有！"];
            }else{
                [self.notFansView removeFromSuperview];
            }
            // 刷新表格
            [self.collectionBaseTopicView.slTableView reloadData];
            if (self.collectionBaseTopicView.slDataSouresArr.count<20) {
                [self.collectionBaseTopicView.slTableView.mj_header endRefreshing];
                
                [self.collectionBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
                
            }else{
                [self.collectionBaseTopicView.slTableView.mj_header endRefreshing];
                [self.collectionBaseTopicView.slTableView.mj_footer resetNoMoreData];
            }
            if (self.collectionBaseTopicView.slDataSouresArr.count==20) {
                [self.collectionBaseTopicView.slTableView.mj_footer setHidden:NO];
            }
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        }  failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.collectionBaseTopicView.slTableView.mj_header endRefreshing];
            
        }];
    }
}
-(void)getMoreData{
    
    if (self.collectionPage) {
        self.collectionPage++;
        NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:@"/users/collections/topics"];
        NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.collectionPage],@"pager":@"20"};
        [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            
            NSArray *moreTopics = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [self.collectionBaseTopicView.slDataSouresArr addObjectsFromArray:moreTopics];
            // 刷新表格
            [self.collectionBaseTopicView.slTableView reloadData];
            if ( moreTopics.count <20) {
                
                [self.collectionBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                // 结束刷新
                [self.collectionBaseTopicView.slTableView.mj_footer endRefreshing];
            }
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        } failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.collectionBaseTopicView.slTableView.mj_footer endRefreshing];
        }];
    }
}
#pragma mark - SLBaseTopicViewDelegate

-(void)SLTopicView:(SLTopicView *)baseView deleteTopicAtIndexPath:(NSInteger)index{

    NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:self.topicKey];
    if (configData) {
        NSDictionary *configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
        
        NSArray *arr =configDict[@"result"];
        NSMutableArray *tipcsArr=[[NSMutableArray alloc] init];
        [tipcsArr addObjectsFromArray:arr];
        [tipcsArr removeObjectAtIndex:index];
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        [dict setObject:tipcsArr forKey:@"result"];
        
        NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:dict];
        [[NSUserDefaults standardUserDefaults] setObject:configData forKey:[URL_ROOT_PATIENT stringByAppendingString:@"/users/collections/topics"]];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)SLTopicView:(SLTopicView *)baseView didUserIconClicked:(SLTopicModel *)model{
    [self didUserIconClickedTransmitModel:model];
}
//-(void)SLTopicView:(SLTopicView *)baseView didSelectRowAtIndexPath:(SLTopicModel *)model{
////    [self didSelectRowAtIndexPathTransmitModel:model];
//    self.selectedModel = model;
//    [self didSelectRowAtIndexPathTransmitModel:model isFromSearch:NO];
//}
-(void)SLTopicView:(SLTopicView *)baseView didSelectRowAtIndexPath:(NSInteger)index AndModel:(SLTopicModel *)model{
    self.selectedModel = model;
    self.selectedIndex=index;
    [self didSelectRowAtIndexPathTransmitModel:model isFromSearch:NO];
}
//-(void)SLTopicView:(SLTopicView *)baseView didPraiseButtonClicked:(UIButton *)button model:(SLTopicModel *)model{
//    [self didPraiseBtnClicked:button model:model];
//}
- (void)SLTopicView:(SLTopicView *)baseView didPraiseButtonClicked:(UIButton *)button praiseLab:(UILabel *)praiseLab model:(SLTopicModel *)model{
    [self didPraiseBtnClicked:button praiseLab:praiseLab model:model];
}
- (void)slPhotoBrowser:(NSIndexPath *)indexPath withPhotos:(NSMutableArray *)photoes{
    self.photos = photoes;
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];

    [browser setCurrentPhotoIndex:indexPath.row];
    browser.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:browser animated:YES completion:nil];
}


- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (_photos.count>9) {
        return 9;
    }
    return _photos.count;
}

#pragma 代理
- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}
#pragma mark - SLTopicDetailViewControllerDelegate
- (void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickPraise:(SLTopicModel *)model{
    self.selectedModel.has_thumbsuped = model.has_thumbsuped;
    self.selectedModel.thumbups_cnt = model.thumbups_cnt;
    [self.collectionBaseTopicView.slTableView reloadData];
}
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickComment:(SLTopicModel *)model{
    self.selectedModel.replies_cnt=model.replies_cnt;
    [self.collectionBaseTopicView.slTableView reloadData];
}
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didSelectModel:(SLTopicModel *)model{
    [self.collectionBaseTopicView.slDataSouresArr removeObjectAtIndex:self.selectedIndex];
    [self.collectionBaseTopicView.slTableView reloadData];
    [self SLTopicView:self.collectionBaseTopicView deleteTopicAtIndexPath:self.selectedIndex];
}

-(void)addNotFansView:(NSString *)title {
    if(!_notFansView){
        UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, ScreenW, ScreenH-49)];
        
        UILabel *tiplabel = [[UILabel alloc]init];
        CGSize tipLabelSize = [tiplabel boundingRectWithString:title withSize:CGSizeMake(300,20) withFont:17];
        
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
        if (ScreenH == 480) {
            tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
        }
        contentView.backgroundColor = SLColor(245, 245, 245);
        tiplabel.font = [UIFont systemFontOfSize:17];
        tiplabel.textAlignment = NSTextAlignmentCenter;
        tiplabel.textColor=[UIColor blackColor];
        tiplabel.text=title;
        [contentView addSubview:tiplabel];
        UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
        bolangLabel.text = @"~~~";
        bolangLabel.font = [UIFont systemFontOfSize:12];
        bolangLabel.textColor = SLColor(213, 213, 213);
        [contentView addSubview:bolangLabel];
        
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
        imgView.image = [UIImage imageNamed:@"swan"];
        [contentView addSubview:imgView];
        _notFansView  = contentView;
    }
    [self.view addSubview:_notFansView];
    
}
- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetwork = NO;
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            self.hasNetwork = YES;
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.hasNetwork = YES;
        }
    }
}


@end
