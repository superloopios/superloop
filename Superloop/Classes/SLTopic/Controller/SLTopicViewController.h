//
//  SLTopicViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLBaseTopicViewController.h"
@class SLTopicViewController;
@protocol slTopicViewControllerDelegate <NSObject>
- (void)pushViewController:(SLTopicViewController *)slTopicViewController;
@end
@interface SLTopicViewController : SLBaseTopicViewController
@property (nonatomic, weak) id<slTopicViewControllerDelegate>delegate;
@end
