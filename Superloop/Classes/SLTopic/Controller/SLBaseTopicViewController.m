//
//  SLBaseTopicViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//  最基本的话题控制器

#import "SLBaseTopicViewController.h"
#import "SLTopicDetailViewController.h"
#import "SLTopicModel.h"
#import "AppDelegate.h"
#import "SLFirstViewController.h"
#import "SLMoreCategoryViewController.h"
#import "SLTopicViewCell.h"
#import "SLAPIHelper.h"
#import "HomePageViewController.h"
#import "SLNavgationViewController.h"

#define  cellWH  ((ScreenW - 70 - (cols - 1) * margin) / cols)
@interface SLBaseTopicViewController ()<UIAlertViewDelegate,SLTopicDetailViewControllerDelegate>{
    BOOL hasNotWork;
}
@end

@implementation SLBaseTopicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
-(void)didUserIconClickedTransmitModel:(SLTopicModel *)model{
    if ([[NSString stringWithFormat:@"%@", model.user[@"id"]] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        HomePageViewController *myView = [[HomePageViewController alloc] init];
        myView.userId=ApplicationDelegate.userId;
        [self.navigationController pushViewController:myView animated:YES];
    }else{
        HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
        otherPerson.userId = model.user[@"id"];
        [self.navigationController pushViewController:otherPerson animated:YES];
    }
    
}
-(void)didSelectRowAtIndexPathTransmitModel:(SLTopicModel *)model isFromSearch:(BOOL) isFromSearch{
    SLTopicDetailViewController *topicDetail = [[SLTopicDetailViewController alloc] init];
    topicDetail.model = model;
    topicDetail.delegate = self;
    if (isFromSearch) {
//        topicDetail.isFromSearch=YES;
        topicDetail.isFromSearch=NO;
    }
    [self.navigationController pushViewController:topicDetail animated:YES];
}
//点赞的按钮被点击的代理
//-(void)didPraiseBtnClicked:(UIButton *)btn model:(SLTopicModel *)model{
- (void)didPraiseBtnClicked:(UIButton *)btn praiseLab:(UILabel *)praiseLab model:(SLTopicModel *)model{
    __block  NSInteger count = [model.thumbups_cnt integerValue];
    NSDictionary *parameter = @{@"id":model.id};
    if ([model.has_thumbsuped isEqualToString:@"0"]) {
        btn.enabled=NO;
        [SLAPIHelper dGood:parameter method:Post success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            btn.selected = YES;
            count ++ ;
            if (count>9999) {
                praiseLab.text = [NSString stringWithFormat:@"%ld万", (long)count/10000];
//            [btn setTitle:[NSString stringWithFormat:@"%ld万", (long)count/10000] forState:UIControlStateNormal];
            }else{
                praiseLab.text = [NSString stringWithFormat:@"%ld", (long)count];

//             [btn setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
            }
            model.thumbups_cnt=[NSString stringWithFormat:@"%ld",count];
            model.has_thumbsuped=[NSString stringWithFormat:@"%d",1];
            [HUDManager showWarningWithText:@"点赞成功!"];
            btn.enabled=YES;
        } failure:^(SLHttpRequestError *error) {
            btn.enabled=YES;
            if (error.httpStatusCode==404&&error.slAPICode==12) {
                [HUDManager showWarningWithText:@"点赞失败，话题已被删除!"];
            }else if (error.httpStatusCode==409&&error.slAPICode==1) {
                [HUDManager showWarningWithText:@"你已赞过该话题，请勿重复点赞!"];
                btn.selected = YES;
                count ++ ;
                if (count>9999) {
                    praiseLab.text = [NSString stringWithFormat:@"%ld万", (long)count/10000];
                    //            [btn setTitle:[NSString stringWithFormat:@"%ld万", (long)count/10000] forState:UIControlStateNormal];
                }else{
                    praiseLab.text = [NSString stringWithFormat:@"%ld", (long)count];
                    
                    //             [btn setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
                }
//                if (count>9999) {
//                    [btn setTitle:[NSString stringWithFormat:@"%ld万", (long)count/10000] forState:UIControlStateNormal];
//                }else{
//                    [btn setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
//                }
                model.thumbups_cnt=[NSString stringWithFormat:@"%ld",count];
                model.has_thumbsuped=[NSString stringWithFormat:@"%d",1];
            }
            else{
                [HUDManager showWarningWithText:@"点赞失败!"];
            }
        }];
    }
   
    if([model.has_thumbsuped isEqualToString:@"1"]){
        //取消单赞
        btn.enabled=NO;
        [SLAPIHelper dGood:parameter method:Delete success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            btn.selected = NO;
            count --;
//            if (count>9999) {
//                [btn setTitle:[NSString stringWithFormat:@"%ld万", (long)count/10000] forState:UIControlStateNormal];
//            }else{
//                [btn setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
//            }
            if (count>9999) {
                praiseLab.text = [NSString stringWithFormat:@"%ld万", (long)count/10000];
                //            [btn setTitle:[NSString stringWithFormat:@"%ld万", (long)count/10000] forState:UIControlStateNormal];
            }else{
                praiseLab.text = [NSString stringWithFormat:@"%ld", (long)count];
                
                //             [btn setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
            }
            model.thumbups_cnt=[NSString stringWithFormat:@"%ld",(long)count];
            model.has_thumbsuped=[NSString stringWithFormat:@"%d",0];
            [HUDManager showWarningWithText:@"点赞取消!"];
            btn.enabled=YES;
        } failure:^(SLHttpRequestError *error) {
            btn.enabled=YES;
            if (error.httpStatusCode==404&&error.slAPICode==12) {
                [HUDManager showWarningWithText:@"点赞取消失败，话题已被删除!"];
            }else if (error.httpStatusCode==409&&error.slAPICode==1) {
                btn.selected = NO;
                [HUDManager showWarningWithText:@"你已取消点赞，请勿重复操作!"];
                count --;
//                if (count>9999) {
//                    [btn setTitle:[NSString stringWithFormat:@"%ld万", (long)count/10000] forState:UIControlStateNormal];
//                }else{
//                    [btn setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
//                }
                if (count>9999) {
                    praiseLab.text = [NSString stringWithFormat:@"%ld万", (long)count/10000];
                    //            [btn setTitle:[NSString stringWithFormat:@"%ld万", (long)count/10000] forState:UIControlStateNormal];
                }else{
                    praiseLab.text = [NSString stringWithFormat:@"%ld", (long)count];
                    
                    //             [btn setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
                }
                model.thumbups_cnt=[NSString stringWithFormat:@"%ld",(long)count];
                model.has_thumbsuped=[NSString stringWithFormat:@"%d",0];
            }else{
                [HUDManager showWarningWithText:@"点赞取消失败!"];
            }
        }];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==300) {
        if (buttonIndex==1) {
            SLMoreCategoryViewController *vc=[[SLMoreCategoryViewController alloc] init];
            vc.tagsType = @1;
            vc.userData = self.userInfo;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [alertView dismissWithClickedButtonIndex:1 animated:YES];
        }
    }
    
    if (alertView.tag==400) {
        if (buttonIndex==1) {
            SLFirstViewController *vc=[[SLFirstViewController alloc] init];
            SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
            [self presentViewController:nav animated:YES completion:nil];
        }
    }
    if (alertView.tag==500) {
        if (buttonIndex==1) {
            SLFirstViewController *vc=[[SLFirstViewController alloc] init];
            SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
            [self presentViewController:nav animated:YES completion:nil];
        }
    }
}



@end
