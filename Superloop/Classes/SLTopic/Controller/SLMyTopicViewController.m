//
//  SLMytopicViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLMyTopicViewController.h"
#import "SLTopicDetailViewController.h"
#import "AppDelegate.h"
#import  <MJRefresh.h>
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "SLAPIHelper.h"
#import "SLTopicView.h"
#import "SLTopicModel.h"
#import "SLFirstViewController.h"
#import <Photos/Photos.h>
#import "MWPhotoBrowser.h"
#import "SLNavgationViewController.h"

#define  cellWH  ((ScreenW - 70 - (cols - 1) * margin) / cols)
@interface SLMyTopicViewController ()<SLTopicViewDelegate,MWPhotoBrowserDelegate>
@property (nonatomic,copy)NSString *topicKey;
@property (nonatomic,assign)NSInteger mePage;
@property (nonatomic,assign)NSInteger otherPage;
@property (nonatomic,strong)UIView *notFansView;
@property (nonatomic,assign)BOOL hasNetwork;
@property (nonatomic,strong)SLTopicView *mineBaseTopicView;
@property (nonatomic,strong)SLTopicView *otherBaseTopicView;
@property (nonatomic,strong)SLTopicModel *selectedModel;
@property (nonatomic,assign)NSInteger selectedIndex;
@property (nonatomic,strong)NSMutableArray *photos;
@end
@implementation SLMyTopicViewController
//static NSInteger const cols = 3;
//static CGFloat const margin = 5;

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
   // [self.navigationController setNavigationBarHidden:YES animated:NO];
}
#pragma mark - 懒加载
-(NSString *)topicKey{
    if (!_topicKey) {
        _topicKey=[NSString new];
    }
    return _topicKey;
}
-(NSInteger)mePage{
    if (!_mePage) {
        _mePage=0;
    }
    return _mePage;
}
-(NSInteger)otherPage{
    if (!_otherPage) {
        _otherPage=0;
    }
    return _otherPage;
}

-(SLTopicView *)mineBaseTopicView{
    if (!_mineBaseTopicView) {
        _mineBaseTopicView=[[SLTopicView alloc] initWithFrame:CGRectMake(0, 64, ScreenW, ScreenH-64)];
        _mineBaseTopicView.delegate=self;
    }
    return _mineBaseTopicView;
}

-(SLTopicView *)otherBaseTopicView{
    if (!_otherBaseTopicView) {
        _otherBaseTopicView=[[SLTopicView alloc] initWithFrame:CGRectMake(0, 64, ScreenW, ScreenH-64)];
        _otherBaseTopicView.delegate=self;
    }
    return _otherBaseTopicView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.view.backgroundColor = SLColor(244, 244, 244);
    [self setUpNav];
    [self setUpRefresh];
    
    if (self.BaseUserId) {
        [self.mineBaseTopicView removeFromSuperview];
        [self.view addSubview:self.otherBaseTopicView];
        self.topicKey=[NSString stringWithFormat:@"%@",self.BaseUserId];
//        [self getCacheData];
        [self.otherBaseTopicView.slTableView.mj_header beginRefreshing];
        [self.otherBaseTopicView.slTableView.mj_footer setHidden:YES];
    } else {
        [self.otherBaseTopicView removeFromSuperview];
        [self.view addSubview:self.mineBaseTopicView];
        self.topicKey=[NSString stringWithFormat:@"%@",ApplicationDelegate.userId];
        [self getCacheData];
        [self.mineBaseTopicView.slTableView.mj_header beginRefreshing];
        [self.mineBaseTopicView.slTableView.mj_footer setHidden:YES];
    }
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    
    if (self.BaseUserId) {
        nameLab.text = @"话题";
    } else {
        nameLab.text = @"我的话题";
    }
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 上下拉刷新
- (void)setUpRefresh {
    self.mineBaseTopicView.slTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
    self.mineBaseTopicView.slTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreData)];
    self.otherBaseTopicView.slTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
    self.otherBaseTopicView.slTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreData)];
}

#pragma mark - 获取缓存数据
- (void)getCacheData {
    //获取旧的数据
    NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:self.topicKey];
    if (configData) {
        NSDictionary *configDict = nil;
        configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
        if (!self.BaseUserId) {
            //            self.otherBaseTopicView.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
            //            [self.otherBaseTopicView.slTableView reloadData];
            //            if (self.otherBaseTopicView.slDataSouresArr.count<20) {
            //                [self.otherBaseTopicView.slTableView.mj_header endRefreshing];
            //                [self.otherBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
            //            }else{
            //                [self.otherBaseTopicView.slTableView.mj_header endRefreshing];
            //                [self.otherBaseTopicView.slTableView.mj_footer resetNoMoreData];
            //            }
            //        }else{
            self.mineBaseTopicView.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
            [self.mineBaseTopicView.slTableView reloadData];
            if (self.mineBaseTopicView.slDataSouresArr.count<20) {
                [self.mineBaseTopicView.slTableView.mj_header endRefreshing];
                [self.mineBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [self.mineBaseTopicView.slTableView.mj_header endRefreshing];
                [self.mineBaseTopicView.slTableView.mj_footer resetNoMoreData];
            }
        }
    } else {
        [self getData];
    }
}
#pragma mark -获取话题的数据
-(void)getData{
    
    self.mePage=0;
    self.otherPage=0;
    if (self.BaseUserId) {
        NSString *user = self.BaseUserId;
        NSLog(@"%@",self.BaseUserId);
        NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@/topics",user]];
        NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.otherPage],@"pager":@"20"};
        self.topicKey=[NSString stringWithFormat:@"%@",user];
        [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            self.otherBaseTopicView.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
//            NSLog(@"%@",data[@"result"]);
            //            //缓存
            //            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
            //            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:self.topicKey];
            //            [[NSUserDefaults standardUserDefaults] synchronize];
//            for (SLTopicModel *model in self.otherBaseTopicView.slDataSouresArr) {
//                NSLog(@"%@,%@",model.has_thumbsuped,model.thumbups_cnt);
//            }
            if (self.otherBaseTopicView.slDataSouresArr.count==0) {
                [self.otherBaseTopicView addNetWorkViews:@"这里什么也没有！"];
            }else{
                [self.otherBaseTopicView.noNetWorkViews removeFromSuperview];
            }
            // 刷新表格
            [self.otherBaseTopicView.slTableView reloadData];
            if (self.otherBaseTopicView.slDataSouresArr.count<20) {
                [self.otherBaseTopicView.slTableView.mj_header endRefreshing];
                
                [self.otherBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
                
            }else{
                [self.otherBaseTopicView.slTableView.mj_header endRefreshing];
                [self.otherBaseTopicView.slTableView.mj_footer resetNoMoreData];
            }
            if (self.otherBaseTopicView.slDataSouresArr.count==20) {
                [self.otherBaseTopicView.slTableView.mj_footer setHidden:NO];
            }
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        }  failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.otherBaseTopicView.slTableView.mj_header endRefreshing];
            
        }];
    }else{
        NSString *user = ApplicationDelegate.userId;
        NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@/topics",user]];
        NSDictionary *parameters = @{@"id":[NSString stringWithFormat:@"%@", ApplicationDelegate.userId],@"page":[NSString stringWithFormat:@"%ld",(long)self.mePage],@"pager":@"20"};
        self.topicKey=[NSString stringWithFormat:@"%@",user];
        [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            self.mineBaseTopicView.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            //缓存
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:self.topicKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if (self.mineBaseTopicView.slDataSouresArr.count==0) {
                [self.mineBaseTopicView addNetWorkViews:@"这里什么也没有！"];
            }else{
                [self.mineBaseTopicView.noNetWorkViews removeFromSuperview];
            }
            
            // 刷新表格
            [self.mineBaseTopicView.slTableView reloadData];
            if (self.mineBaseTopicView.slDataSouresArr.count<20) {
                [self.mineBaseTopicView.slTableView.mj_header endRefreshing];
                
                [self.mineBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
                
            }else{
                [self.mineBaseTopicView.slTableView.mj_header endRefreshing];
                [self.mineBaseTopicView.slTableView.mj_footer resetNoMoreData];
            }
            if (self.mineBaseTopicView.slDataSouresArr.count==20) {
                [self.mineBaseTopicView.slTableView.mj_footer setHidden:NO];
            }
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        }  failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.mineBaseTopicView.slTableView.mj_header endRefreshing];
            
        }];
    }
}
-(void)getMoreData{
    
    if (self.BaseUserId) {
        self.otherPage++;
        NSString *user = self.BaseUserId;
        NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@/topics",user]];
        NSDictionary *parameters = @{@"id":[NSString stringWithFormat:@"%@", ApplicationDelegate.userId],@"page":[NSString stringWithFormat:@"%ld",(long)self.otherPage],@"pager":@"20"};
        [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            
            NSArray *moreTopics = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [self.otherBaseTopicView.slDataSouresArr addObjectsFromArray:moreTopics];
            // 刷新表格
            [self.otherBaseTopicView.slTableView reloadData];
            
            if ( moreTopics.count <20) {
                
                [self.otherBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                // 结束刷新
                [self.otherBaseTopicView.slTableView.mj_footer endRefreshing];
            }
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        } failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.otherBaseTopicView.slTableView.mj_footer endRefreshing];
        }];
    }else{
        self.mePage++;
        NSString *user = ApplicationDelegate.userId;
        NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@/topics",user]];
        NSDictionary *parameters = @{@"id":[NSString stringWithFormat:@"%@", ApplicationDelegate.userId],@"page":[NSString stringWithFormat:@"%ld",(long)self.mePage],@"pager":@"20"};
        [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            
            NSArray *moreTopics = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [self.mineBaseTopicView.slDataSouresArr addObjectsFromArray:moreTopics];
            // 刷新表格
            [self.mineBaseTopicView.slTableView reloadData];
            if ( moreTopics.count <20) {
                
                [self.mineBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                // 结束刷新
                [self.mineBaseTopicView.slTableView.mj_footer endRefreshing];
            }
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        } failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.mineBaseTopicView.slTableView.mj_footer endRefreshing];
        }];
        
    }
}

#pragma mark - SLBaseTopicViewDelegate

-(void)SLTopicView:(SLTopicView *)baseView deleteTopicAtIndexPath:(NSInteger)index{
    
    NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:self.topicKey];
    if (configData) {
        NSDictionary *configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
        
        NSArray *arr =configDict[@"result"];
        NSMutableArray *tipcsArr=[[NSMutableArray alloc] init];
        [tipcsArr addObjectsFromArray:arr];
        [tipcsArr removeObjectAtIndex:index];
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        [dict setObject:tipcsArr forKey:@"result"];
        
        NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:dict];
        if (self.BaseUserId) {
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:[NSString stringWithFormat:@"%@",self.BaseUserId]];
        }else{
        [[NSUserDefaults standardUserDefaults] setObject:configData forKey:[NSString stringWithFormat:@"%@",ApplicationDelegate.userId]];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)SLTopicView:(SLTopicView *)baseView pushLoginVC:(BOOL)isPushing{
    SLFirstViewController *loginView=[[SLFirstViewController alloc] init];
    SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:loginView];
//    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:loginView];
    [self presentViewController:nav animated:YES completion:nil];
}
//-(void)SLTopicView:(SLTopicView *)baseView didPraiseButtonClicked:(UIButton *)button model:(SLTopicModel *)model{
//    [self didPraiseBtnClicked:button model:model];
//}
- (void)SLTopicView:(SLTopicView *)baseView didPraiseButtonClicked:(UIButton *)button praiseLab:(UILabel *)praiseLab model:(SLTopicModel *)model{
    [self didPraiseBtnClicked:button praiseLab:praiseLab model:model];
}
-(void)SLTopicView:(SLTopicView *)baseView didUserIconClicked:(SLTopicModel *)model{
    [self didUserIconClickedTransmitModel:model];
}
//-(void)SLTopicView:(SLTopicView *)baseView didSelectRowAtIndexPath:(SLTopicModel *)model{
//    //    [self didSelectRowAtIndexPathTransmitModel:model];
//    self.selectedModel = model;
//    [self didSelectRowAtIndexPathTransmitModel:model isFromSearch:NO];
//}
-(void)SLTopicView:(SLTopicView *)baseView didSelectRowAtIndexPath:(NSInteger)index AndModel:(SLTopicModel *)model{
    self.selectedModel = model;
    self.selectedIndex=index;
    [self didSelectRowAtIndexPathTransmitModel:model isFromSearch:NO];
}
- (void)slPhotoBrowser:(NSIndexPath *)indexPath withPhotos:(NSMutableArray *)photoes{
    self.photos = photoes;
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
  
    [browser setCurrentPhotoIndex:indexPath.row];
    browser.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:browser animated:YES completion:nil];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (_photos.count>9) {
        return 9;
    }
    return _photos.count;
}

#pragma 代理
- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}
#pragma mark - SLTopicDetailViewControllerDelegate
- (void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickPraise:(SLTopicModel *)model{
    NSLog(@"%@,%@,%@",model,model.has_thumbsuped,model.thumbups_cnt);
    self.selectedModel.has_thumbsuped = model.has_thumbsuped;
    self.selectedModel.thumbups_cnt = model.thumbups_cnt;
    if (self.BaseUserId) {
        [self.otherBaseTopicView.slTableView reloadData];
    }else{
        [self.mineBaseTopicView.slTableView reloadData];
    }
}
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickComment:(SLTopicModel *)model{
    self.selectedModel.replies_cnt=model.replies_cnt;
    if (self.BaseUserId) {
        [self.otherBaseTopicView.slTableView reloadData];
    }else{
        [self.mineBaseTopicView.slTableView reloadData];
    }
}
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didSelectModel:(SLTopicModel *)model{
    if (self.BaseUserId) {
        [self.otherBaseTopicView.slDataSouresArr removeObjectAtIndex:self.selectedIndex];
        [self.otherBaseTopicView.slTableView reloadData];
        [self SLTopicView:self.otherBaseTopicView deleteTopicAtIndexPath:self.selectedIndex];
    }else{
        [self.mineBaseTopicView.slDataSouresArr removeObjectAtIndex:self.selectedIndex];
        [self.mineBaseTopicView.slTableView reloadData];
        [self SLTopicView:self.mineBaseTopicView deleteTopicAtIndexPath:self.selectedIndex];
    }
}

- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetwork = NO;
            //没有网络的情况下给出提示
            NSData * mineConfigData=[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@",ApplicationDelegate.userId]];
            NSDictionary *mineConfigDict=[NSKeyedUnarchiver unarchiveObjectWithData:mineConfigData];
            NSArray *mineArr = mineConfigDict[@"result"];
            if (mineArr.count==0) {
                [self.mineBaseTopicView addNetWorkViews:@"世界上最遥远的距离就是－－妹有网"];
            }
            NSData * otherConfigData=[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@",self.BaseUserId]];
            NSDictionary *otherConfigDict=[NSKeyedUnarchiver unarchiveObjectWithData:otherConfigData];
            NSArray *otherArr=otherConfigDict[@"result"];
            if (otherArr==0) {
                [self.otherBaseTopicView addNetWorkViews:@"世界上最遥远的距离就是－－妹有网"];
            }
        }else if ([netWorkStaus isEqualToString:@"wifi"] || [netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.hasNetwork = YES;
            
            //有网络的情况
            if (self.mineBaseTopicView.noNetWorkViews) {
                [self.mineBaseTopicView.noNetWorkViews removeFromSuperview];
                [self.mineBaseTopicView.slTableView reloadData];
            }
            if (self.otherBaseTopicView.noNetWorkViews) {
                [self.otherBaseTopicView.noNetWorkViews removeFromSuperview];
                [self.otherBaseTopicView.slTableView reloadData];
            }
        }
    }
    
}

@end
