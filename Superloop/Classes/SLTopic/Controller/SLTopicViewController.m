//
//  SLTopicViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTopicViewController.h"
#import "SLAddSubjectViewController.h"
#import "SLCollectionTopicViewController.h"
#import "SLTopicDetailViewController.h"
#import "AppDelegate.h"
#import "SLFirstViewController.h"
#import "SLAPIHelper.h"
#import  <MJRefresh.h>
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "SLTopicViewCell.h"
#import "SLTopicView.h"
#import <Photos/Photos.h>
#import "MWPhotoBrowser.h"
#import "SLSegmentedControl.h"
#import "SLNavgationViewController.h"

@interface SLTopicViewController ()< SLSegmentControlDelegate,SLTopicViewCellDelegate,UIAlertViewDelegate,SLTopicViewDelegate,MWPhotoBrowserDelegate,UIScrollViewDelegate>{
    SLSegmentedControl *seg;
}
@property (nonatomic,assign)NSInteger followTopicsPage;
@property (nonatomic,assign)NSInteger hotTopicsPage;
@property (nonatomic,assign)NSInteger lastTopicsPage;
@property (nonatomic,copy)NSString *topicKey;
@property (nonatomic,copy)NSString *topicKey0;
@property (nonatomic,copy)NSString *topicKey1;
@property (nonatomic,copy)NSString *topicKey2;
@property (nonatomic,assign)BOOL hasNetwork;
//是否需要刷新hot
@property (nonatomic,assign)BOOL isRefreshHotsTopics;
//是否需要刷新follow
@property (nonatomic,assign)BOOL isRefreshFollowsTopics;
//是否需要刷新last
@property (nonatomic,assign)BOOL isRefreshLastTopics;
@property (nonatomic,strong)SLTopicView *hotBaseTopicView;
@property (nonatomic,strong)SLTopicView *followBaseTopicView;
@property (nonatomic,strong)SLTopicView *lastBaseTopicView;
@property (nonatomic,assign)BOOL isDelete;
@property (nonatomic,assign)NSInteger deleteIndex;
@property (nonatomic,strong)SLTopicModel *selectedModel;
@property (nonatomic,assign)NSInteger selectedIndex;
@property (nonatomic,strong)NSMutableArray *photos;
@property (nonatomic,strong)UIScrollView *scrollview;
@property (nonatomic,assign)CGFloat lastContentOffset;
//是否有缓存
//是否已经加载热门缓存数据
@property (nonatomic, assign)BOOL hasLoadPopularCache;
//是否已经加载最新缓存数据
@property (nonatomic, assign)BOOL hasLoadnewTopicHasCache;

@end
@implementation SLTopicViewController



- (UIScrollView *)scrollview{
    if (!_scrollview) {
        _scrollview = [[UIScrollView alloc] init];
        _scrollview.frame = CGRectMake(0, 64, screen_W, screen_H-113);
        _scrollview.pagingEnabled = YES;
        _scrollview.delegate = self;
        [_scrollview setContentSize:CGSizeMake(screen_W*3, 0)];
        _scrollview.showsHorizontalScrollIndicator = NO;
        _scrollview.scrollsToTop=NO;
        _scrollview.bounces = NO;
        _scrollview.delegate = self;
        [self.view addSubview:_scrollview];
    }
    return _scrollview;
}
#pragma mark - 懒加载
-(NSInteger)followTopicsPage{
    if (!_followTopicsPage) {
        _followTopicsPage=0;
    }
    return _followTopicsPage;
}
-(NSInteger)hotTopicsPage{
    if (!_hotTopicsPage) {
        _hotTopicsPage=0;
    }
    return _hotTopicsPage;
}
-(NSInteger)lastTopicsPage{
    if (!_lastTopicsPage) {
        _lastTopicsPage=0;
    }
    return _lastTopicsPage;
}
-(NSString *)topicKey{
    if (!_topicKey) {
        _topicKey=[NSString new];
    }
    return _topicKey;
}
-(NSString *)topicKey1{
    if (!_topicKey1) {
        _topicKey1=[NSString new];
    }
    return _topicKey1;
}
-(NSString *)topicKey2{
    if (!_topicKey) {
        _topicKey=[NSString new];
    }
    return _topicKey;
}
-(NSString *)topicKey3{
    if (!_topicKey) {
        _topicKey=[NSString new];
    }
    return _topicKey;
}
-(SLTopicView *)hotBaseTopicView{
    if (!_hotBaseTopicView) {
        _hotBaseTopicView=[[SLTopicView alloc] initWithFrame:CGRectMake(screen_W, 0, ScreenW, ScreenH-113)];
        _hotBaseTopicView.tag=102;
        _hotBaseTopicView.delegate=self;
        [self.scrollview addSubview:_hotBaseTopicView];
    }
    return _hotBaseTopicView;
}
-(SLTopicView *)followBaseTopicView{
    if (!_followBaseTopicView) {
        _followBaseTopicView=[[SLTopicView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH-113)];
        _followBaseTopicView.tag=100;
        _followBaseTopicView.delegate=self;
        _followBaseTopicView.isFollowVC=YES;
        [self.scrollview addSubview:_followBaseTopicView];
    }
    return _followBaseTopicView;
}
-(SLTopicView *)lastBaseTopicView{
    if (!_lastBaseTopicView) {
        _lastBaseTopicView=[[SLTopicView alloc] initWithFrame:CGRectMake(screen_W*2, 0, ScreenW, ScreenH-113)];
        _lastBaseTopicView.tag=101;
        _lastBaseTopicView.delegate=self;
        [self.scrollview addSubview:_lastBaseTopicView];
    }
    return _lastBaseTopicView;
}
#pragma mark - view周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLTopicViewController"];
    if (!ApplicationDelegate.Basic && seg.selectedSegmentIndex==0) {
        if (!self.followBaseTopicView.notLoginView) {
            [self.followBaseTopicView addNotLoginView:@"没有登录什么也看不了"];
        }
    } else if (ApplicationDelegate.Basic){
        if (self.followBaseTopicView.notLoginView) {
            [self.followBaseTopicView.notLoginView removeFromSuperview];
        }
    }
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLTopicViewController"];
    //[self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.view.backgroundColor = SLColor(244, 244, 244);
    [self setUpNav];
    [self setUpSegControl];
    [self setUpRefresh];
    
    self.isRefreshFollowsTopics=YES;
    self.isRefreshLastTopics=YES;
    self.isRefreshHotsTopics=YES;
    //并且在第一次的时候自动点击选中按钮
    
    [self selectIndexSegControl:seg];
    //登录成功刷新页面
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isLoginSuccessToLoadData) name:@"isLoginSuccessToLoadData" object:nil];
    //关注或者取消关注，加入黑名单或取消黑名单
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getfollowData) name:@"isUnfollow" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isRecivedSuccessToLoadData) name:@Topic_Refreshing object:nil];
    //退出登录刷新数据
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isLoginSuccessToLoadData) name:@"ClearMessage" object:nil];
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
#pragma mark - 通知实现的方法
-(void)isLoginSuccessToLoadData{
    self.isRefreshLastTopics=YES;
    self.isRefreshFollowsTopics=YES;
    self.isRefreshHotsTopics=YES;
    [self selectIndexSegControl:seg];
}

//刷新话题
- (void)isRecivedSuccessToLoadData{
    if (seg.selectedSegmentIndex == 1) {
        self.isRefreshHotsTopics = YES;
    }else if (seg.selectedSegmentIndex == 0) {
        self.isRefreshFollowsTopics=YES;
    }else{
        self.isRefreshLastTopics=YES;
    }
    [self selectIndexSegControl:seg];
}
#pragma mark - UI
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    
    
    UIButton *addBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    if (ScreenW==320) {
        addBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    }else{
        addBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    }
    [addBtn setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
    [addBtn addTarget:self action:@selector(addClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:addBtn];
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
}
- (void)setUpSegControl{
    seg = [[SLSegmentedControl alloc] initWithFrame:CGRectMake(60, 20, screen_W-120, 43.5)];
    seg.delegate = self;
    seg.titleArr = @[@"关注",@"热门",@"最新"];
    [self.view addSubview:seg];
    
}
#pragma mark - button点击事件
- (void)addClick{
    if (ApplicationDelegate.userId==nil) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
        alert.tag=500;
        [alert show];
    } else {
        SLAddSubjectViewController *addSubject = [[SLAddSubjectViewController alloc] init];
        addSubject.publishSuccessBlock=^(BOOL isSuccess){
            if (isSuccess) {
                self.isRefreshLastTopics=YES;
                self.isRefreshHotsTopics=YES;
                [self selectIndexSegControl:seg];
            }
        };
        //[self.navigationController pushViewController:addSubject animated:YES];
        [self presentViewController:addSubject animated:YES completion:nil];
    }
}
-(void)selectIndexSegControl:(SLSegmentedControl *)segment{
    if (segment.selectedSegmentIndex==1) {
        //        [self.lastBaseTopicView removeFromSuperview];
        //        [self.followBaseTopicView removeFromSuperview];
        //        [self.scrollview addSubview:self.hotBaseTopicView];
        self.hotBaseTopicView.slTableView.scrollsToTop=YES;
        self.followBaseTopicView.slTableView.scrollsToTop=NO;
        self.lastBaseTopicView.slTableView.scrollsToTop=NO;
        self.topicKey = [URL_ROOT_PATIENT stringByAppendingString:@"/topics/popular"];
        [self getCacheData:1];
        if (self.isRefreshHotsTopics) {
            [self.hotBaseTopicView.slTableView.mj_header beginRefreshing];
            [self.hotBaseTopicView.slTableView.mj_footer setHidden:YES];
        }
    }else if (segment.selectedSegmentIndex==0) {
        //        [self.hotBaseTopicView removeFromSuperview];
        //        [self.lastBaseTopicView removeFromSuperview];
        //        [self.scrollview addSubview:self.followBaseTopicView];
        self.hotBaseTopicView.slTableView.scrollsToTop=NO;
        self.followBaseTopicView.slTableView.scrollsToTop=YES;
        self.lastBaseTopicView.slTableView.scrollsToTop=NO;

        if (ApplicationDelegate.Basic) {
            if (self.followBaseTopicView.notLoginView) {
                [self.followBaseTopicView.notLoginView removeFromSuperview];
            }
            self.topicKey=[URL_ROOT_PATIENT stringByAppendingString:@"/users/follows/topics"];
            [self getCacheData:0];
            if (self.isRefreshFollowsTopics) {
                [self.followBaseTopicView.slTableView.mj_header beginRefreshing];
                [self.followBaseTopicView.slTableView.mj_footer setHidden:YES];
            }
        }else{
            [self.followBaseTopicView addNotLoginView:@"没有登录什么也看不了"];
        }
    }else{
        //        [self.hotBaseTopicView removeFromSuperview];
        //        [self.followBaseTopicView removeFromSuperview];
        //        [self.scrollview addSubview:self.lastBaseTopicView];
        self.hotBaseTopicView.slTableView.scrollsToTop=NO;
        self.followBaseTopicView.slTableView.scrollsToTop=NO;
        self.lastBaseTopicView.slTableView.scrollsToTop=YES;
        self.topicKey = [URL_ROOT_PATIENT stringByAppendingString:@"/topics"];
        [self getCacheData:2];
        if (self.isRefreshLastTopics) {
            [self.lastBaseTopicView.slTableView.mj_header beginRefreshing];
            [self.lastBaseTopicView.slTableView.mj_footer setHidden:YES];
        }
    }
}
#pragma mark - 上下拉刷新
- (void)setUpRefresh {
    self.hotBaseTopicView.slTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
    self.hotBaseTopicView.slTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreData)];
    self.followBaseTopicView.slTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
    self.followBaseTopicView.slTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreData)];
    self.lastBaseTopicView.slTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
    self.lastBaseTopicView.slTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreData)];
    
    [self.hotBaseTopicView.slTableView.mj_footer setHidden:YES];
    [self.followBaseTopicView.slTableView.mj_footer setHidden:YES];
    [self.lastBaseTopicView.slTableView.mj_footer setHidden:YES];
    
}

#pragma mark - 获取缓存数据
- (void)getCacheData:(NSInteger)index {
    //获取旧的数据
    NSString *topicKey = @"";
    if (index == 0) {
        topicKey = [URL_ROOT_PATIENT stringByAppendingString:@"/users/follows/topics"];
    }else if (index == 1) {
        topicKey = [URL_ROOT_PATIENT stringByAppendingString:@"/topics/popular"];
    }else if (index == 2) {
        topicKey = [URL_ROOT_PATIENT stringByAppendingString:@"/topics"];
    }
    
    NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:topicKey];
    if (configData) {
        NSDictionary *configDict = nil;
        configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
        if (index == 1) {
            self.hasLoadPopularCache = YES;
            self.hotBaseTopicView.slTableView.scrollsToTop=YES;
            self.followBaseTopicView.slTableView.scrollsToTop=NO;
            self.lastBaseTopicView.slTableView.scrollsToTop=NO;

            self.hotBaseTopicView.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
            [self.hotBaseTopicView.slTableView reloadData];
            
        }else if (index==0) {  //超人说
            self.hotBaseTopicView.slTableView.scrollsToTop=NO;
            self.followBaseTopicView.slTableView.scrollsToTop=YES;
            self.lastBaseTopicView.slTableView.scrollsToTop=NO;
            
            self.followBaseTopicView.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
            [self.followBaseTopicView.slTableView reloadData];
            
        }else{  //最新说
            self.hasLoadnewTopicHasCache = YES;
            self.hotBaseTopicView.slTableView.scrollsToTop=NO;
            self.followBaseTopicView.slTableView.scrollsToTop=NO;
            self.lastBaseTopicView.slTableView.scrollsToTop=YES;

            self.lastBaseTopicView.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:configDict[@"result"]];
            [self.lastBaseTopicView.slTableView reloadData];
            
        }
    }
}
#pragma mark -获取话题的数据
-(void)getData{
    self.hotTopicsPage=0;
    self.followTopicsPage = 0;
    self.lastTopicsPage=0;
    if (seg.selectedSegmentIndex == 1) {
        
        self.hotBaseTopicView.slTableView.scrollsToTop=YES;
        self.followBaseTopicView.slTableView.scrollsToTop=NO;
        self.lastBaseTopicView.slTableView.scrollsToTop=NO;

        NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:@"/topics/popular"];
        
        NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.hotTopicsPage],@"pager":@"20"};
        self.topicKey=topicUrl;
        
        [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            
            if (!self.isRefreshHotsTopics) {
                for (SLTopicView *view in [self.view subviews]) {
                    if (view.tag==102) {
                        [self.hotBaseTopicView.slTableView.mj_header endRefreshing];
                        return;
                    }
                }
            }
            self.hotBaseTopicView.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            //缓存
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:self.topicKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            // 刷新表格
            [self.hotBaseTopicView.slTableView reloadData];
            if (self.hotBaseTopicView.slDataSouresArr.count<20) {
                [self.hotBaseTopicView.slTableView.mj_header endRefreshing];
                [self.hotBaseTopicView.slTableView.mj_footer setHidden:YES];
                //[self.lastBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
                
            }else{
                [self.hotBaseTopicView.slTableView.mj_footer setHidden:NO];
                [self.hotBaseTopicView.slTableView.mj_header endRefreshing];
                [self.hotBaseTopicView.slTableView.mj_footer resetNoMoreData];
            }
            self.isRefreshHotsTopics=NO;
            // [self.hotBaseTopicView.slTableView.mj_footer setHidden:NO];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        }  failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.hotBaseTopicView.slTableView.mj_header endRefreshing];
            
        }];
    }else if (seg.selectedSegmentIndex==0) {
        self.hotBaseTopicView.slTableView.scrollsToTop=NO;
        self.followBaseTopicView.slTableView.scrollsToTop=YES;
        self.lastBaseTopicView.slTableView.scrollsToTop=NO;

        if (!ApplicationDelegate.Basic && seg.selectedSegmentIndex==0) {
            if (!self.followBaseTopicView.notLoginView) {
                [self.followBaseTopicView addNotLoginView:@"没有登录什么也看不了"];
            }
            return;
        }
        
        NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:@"/users/follows/topics"];
        NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.followTopicsPage],@"pager":@"20"};
        self.topicKey=topicUrl;
        [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            NSArray *arr=[SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            if (arr.count==0) {
                [self.followBaseTopicView addNetWorkViews:@"这里什么也没有！"];
            }else{
                [self.followBaseTopicView.noNetworkOrDefaultView removeFromSuperview];
            }
            if (!self.isRefreshFollowsTopics) {
                for (SLTopicView *view in [self.view subviews]) {
                    if (view.tag==101) {
                        [self.followBaseTopicView .slTableView.mj_header endRefreshing];
                        return ;
                    }
                }
            }
            self.followBaseTopicView.slDataSouresArr =(NSMutableArray *)arr;
            //缓存
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:self.topicKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            // 刷新表格
            [self.followBaseTopicView.slTableView reloadData];
            if (self.followBaseTopicView.slDataSouresArr.count<20) {
                [self.followBaseTopicView.slTableView.mj_header endRefreshing];
                [self.followBaseTopicView.slTableView.mj_footer setHidden:YES];
                //[self.followBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
                
            }else{
                [self.followBaseTopicView.slTableView.mj_footer setHidden:NO];
                [self.followBaseTopicView.slTableView.mj_header endRefreshing];
                [self.followBaseTopicView.slTableView.mj_footer resetNoMoreData];
            }
            self.isRefreshFollowsTopics=NO;
            //[self.followBaseTopicView.slTableView.mj_footer setHidden:NO];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            
        }  failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.followBaseTopicView.slTableView.mj_header endRefreshing];
            
        }];
        
    }else{
        self.hotBaseTopicView.slTableView.scrollsToTop=NO;
        self.followBaseTopicView.slTableView.scrollsToTop=NO;
        self.lastBaseTopicView.slTableView.scrollsToTop=YES;

        NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:@"/topics"];
        NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.lastTopicsPage],@"pager":@"20"};
        self.topicKey=topicUrl;
        
        [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            
            if (!self.isRefreshLastTopics) {
                for (SLTopicView *view in [self.view subviews]) {
                    if (view.tag==100) {
                        [self.lastBaseTopicView.slTableView.mj_header endRefreshing];
                        return;
                    }
                }
            }
            self.lastBaseTopicView.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            //缓存
            NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:self.topicKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            // 刷新表格
            [self.lastBaseTopicView.slTableView reloadData];
            if (self.lastBaseTopicView.slDataSouresArr.count<20) {
                [self.lastBaseTopicView.slTableView.mj_header endRefreshing];
                [self.lastBaseTopicView.slTableView.mj_footer setHidden:YES];
                //[self.lastBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
                
            }else{
                [self.lastBaseTopicView.slTableView.mj_footer setHidden:NO];
                [self.lastBaseTopicView.slTableView.mj_header endRefreshing];
                [self.lastBaseTopicView.slTableView.mj_footer resetNoMoreData];
            }
            self.isRefreshLastTopics=NO;
            // [self.lastBaseTopicView.slTableView.mj_footer setHidden:NO];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        }  failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.lastBaseTopicView.slTableView.mj_header endRefreshing];
            
        }];
    }
}
- (void)getfollowData{
    self.hotBaseTopicView.slTableView.scrollsToTop=NO;
    self.followBaseTopicView.slTableView.scrollsToTop=YES;
    self.lastBaseTopicView.slTableView.scrollsToTop=NO;
    if (!ApplicationDelegate.Basic && seg.selectedSegmentIndex==0) {
        if (!self.followBaseTopicView.notLoginView) {
            [self.followBaseTopicView addNotLoginView:@"没有登录什么也看不了"];
        }
        return;
    }
    NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:@"/users/follows/topics"];
    NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.followTopicsPage],@"pager":@"20"};
    self.topicKey=topicUrl;
    [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSArray *arr=[SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
        if (arr.count==0) {
            [self.followBaseTopicView addNetWorkViews:@"这里什么也没有！"];
        }else{
            [self.followBaseTopicView.noNetworkOrDefaultView removeFromSuperview];
        }
        if (!self.isRefreshFollowsTopics) {
            for (SLTopicView *view in [self.view subviews]) {
                if (view.tag==101) {
                    [self.followBaseTopicView .slTableView.mj_header endRefreshing];
                    return ;
                }
            }
        }
        self.followBaseTopicView.slDataSouresArr =(NSMutableArray *)arr;
        //缓存
        NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data];
        [[NSUserDefaults standardUserDefaults] setObject:configData forKey:self.topicKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // 刷新表格
        [self.followBaseTopicView.slTableView reloadData];
        if (self.followBaseTopicView.slDataSouresArr.count<20) {
            [self.followBaseTopicView.slTableView.mj_header endRefreshing];
            [self.followBaseTopicView.slTableView.mj_footer setHidden:YES];
            //[self.followBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
            
        }else{
            [self.followBaseTopicView.slTableView.mj_footer setHidden:NO];
            [self.followBaseTopicView.slTableView.mj_header endRefreshing];
            [self.followBaseTopicView.slTableView.mj_footer resetNoMoreData];
        }
        self.isRefreshFollowsTopics=NO;
        //[self.followBaseTopicView.slTableView.mj_footer setHidden:NO];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }  failure:^(SLHttpRequestError *error) {
        // 结束刷新
        [self.followBaseTopicView.slTableView.mj_header endRefreshing];
        
    }];
}
-(void)getMoreData{
    if (seg.selectedSegmentIndex==1) {
        self.hotTopicsPage++;
        SLTopicModel *model = self.hotBaseTopicView.slDataSouresArr.lastObject;
        NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:@"/topics/popular"];
        NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.hotTopicsPage],@"pager":@"20",@"last_id":model.id};
        [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            NSArray *moreTopics = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [self.hotBaseTopicView.slDataSouresArr addObjectsFromArray:moreTopics];
            // 刷新表格
            [self.hotBaseTopicView.slTableView reloadData];
            if ( moreTopics.count <20) {
                [self.hotBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                // 结束刷新
                [self.hotBaseTopicView.slTableView.mj_footer endRefreshing];
            }
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        } failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.hotBaseTopicView.slTableView.mj_footer endRefreshing];
        }];
    }else if (seg.selectedSegmentIndex==0) {
        self.followTopicsPage++;
        SLTopicModel *model = self.followBaseTopicView.slDataSouresArr.lastObject;
        NSLog(@"%@",model.id);
        NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:@"/users/follows/topics"];
        NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.followTopicsPage],@"pager":@"20",@"last_id":model.id};
        [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            NSArray *moreTopics = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [self.followBaseTopicView.slDataSouresArr addObjectsFromArray:moreTopics];
            // 刷新表格
            [self.followBaseTopicView.slTableView reloadData];
            if ( moreTopics.count <20) {
                [self.followBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                // 结束刷新
                [self.followBaseTopicView.slTableView.mj_footer endRefreshing];
            }
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        } failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.followBaseTopicView.slTableView.mj_footer endRefreshing];
        }];
    }else{
        self.lastTopicsPage++;
        SLTopicModel *model = self.lastBaseTopicView.slDataSouresArr.lastObject;
        NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:@"/topics"];
        NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.lastTopicsPage],@"pager":@"20",@"last_id":model.id};
        [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            NSArray *moreTopics = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [self.lastBaseTopicView.slDataSouresArr addObjectsFromArray:moreTopics];
            // 刷新表格
            [self.lastBaseTopicView.slTableView reloadData];
            if ( moreTopics.count <20) {
                [self.lastBaseTopicView.slTableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                // 结束刷新
                [self.lastBaseTopicView.slTableView.mj_footer endRefreshing];
            }
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        } failure:^(SLHttpRequestError *error) {
            // 结束刷新
            [self.lastBaseTopicView.slTableView.mj_footer endRefreshing];
        }];
    }
}

#pragma mark - SLBaseTopicViewDelegate

-(void)SLTopicView:(SLTopicView *)baseView deleteTopicAtIndexPath:(NSInteger)index{
    
    NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:self.topicKey];
    if (configData) {
        NSDictionary *configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
        
        NSArray *arr =configDict[@"result"];
        NSMutableArray *tipcsArr=[[NSMutableArray alloc] init];
        [tipcsArr addObjectsFromArray:arr];
        [tipcsArr removeObjectAtIndex:index];
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        [dict setObject:tipcsArr forKey:@"result"];
        
        NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:dict];
        if (seg.selectedSegmentIndex == 0) {
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:[URL_ROOT_PATIENT stringByAppendingString:@"/topics/popular"]];
        }else if (seg.selectedSegmentIndex==1) {
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:[URL_ROOT_PATIENT stringByAppendingString:@"/users/follows/topics"]];
        }else{
            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:[URL_ROOT_PATIENT stringByAppendingString:@"/topics"]];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
- (void)SLTopicView:(SLTopicView *)baseView didSelectCollectionByMWPhotoBrowser:(id)browser WithModel:(SLTopicModel *)topicModel{
    [self presentViewController:browser animated:YES completion:nil];
}
//-(void)SLTopicView:(SLTopicView *)baseView didPraiseButtonClicked:(UIButton *)button model:(SLTopicModel *)model{
//    [self didPraiseBtnClicked:button model:model];
//}
- (void)SLTopicView:(SLTopicView *)baseView didPraiseButtonClicked:(UIButton *)button praiseLab:(UILabel *)praiseLab model:(SLTopicModel *)model{
    [self didPraiseBtnClicked:button praiseLab:praiseLab model:model];
}
-(void)SLTopicView:(SLTopicView *)baseView didUserIconClicked:(SLTopicModel *)model{
    [self didUserIconClickedTransmitModel:model];
}
//-(void)SLTopicView:(SLTopicView *)baseView didSelectRowAtIndexPath:(SLTopicModel *)model{
//    self.selectedModel = model;
//    [self didSelectRowAtIndexPathTransmitModel:model isFromSearch:NO];
//}
-(void)SLTopicView:(SLTopicView *)baseView didSelectRowAtIndexPath:(NSInteger)index AndModel:(SLTopicModel *)model{
    self.selectedModel = model;
    self.selectedIndex=index;
    [self didSelectRowAtIndexPathTransmitModel:model isFromSearch:NO];
}
-(void)SLTopicView:(SLTopicView *)baseView pushLoginVC:(BOOL)isPushing{
    SLFirstViewController *loginView=[[SLFirstViewController alloc] init];
    SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:loginView];
    //    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:loginView];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)slPhotoBrowser:(NSIndexPath *)indexPath withPhotos:(NSMutableArray *)photoes{
    self.photos = photoes;
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    [browser setCurrentPhotoIndex:indexPath.row];
    browser.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:browser animated:YES completion:nil];
}


//#pragma 代理
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (_photos.count>9) {
        return 9;
    }
    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}
#pragma mark - SLTopicDetailViewControllerDelegate
- (void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickPraise:(SLTopicModel *)model{
    self.selectedModel.has_thumbsuped = model.has_thumbsuped;
    self.selectedModel.thumbups_cnt = model.thumbups_cnt;
    NSLog(@"self.selectedModel.has_thumbsuped ----- %@",self.selectedModel.has_thumbsuped);

    if (seg.selectedSegmentIndex == 1) {
        [self.hotBaseTopicView.slTableView reloadData];
    }else if (seg.selectedSegmentIndex == 0) {
        [self.followBaseTopicView.slTableView reloadData];
    }else{
        [self.lastBaseTopicView.slTableView reloadData];
    }
}
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickComment:(SLTopicModel *)model{
    self.selectedModel.replies_cnt=model.replies_cnt;
    if (seg.selectedSegmentIndex == 1) {
        [self.hotBaseTopicView.slTableView reloadData];
    }else if (seg.selectedSegmentIndex == 0) {
        [self.followBaseTopicView.slTableView reloadData];
    }else{
        [self.lastBaseTopicView.slTableView reloadData];
    }
}
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickCollection:(SLTopicModel *)model{
    self.selectedModel.has_collected = model.has_collected;
    NSLog(@"self.selectedModel.has_collected ----- %@",self.selectedModel.has_collected);
    if (seg.selectedSegmentIndex == 1) {
        [self.hotBaseTopicView.slTableView reloadData];
    }else if (seg.selectedSegmentIndex == 0) {
        [self.followBaseTopicView.slTableView reloadData];
    }else{
        [self.lastBaseTopicView.slTableView reloadData];
    }
}
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didSelectModel:(SLTopicModel *)model{
    if (seg.selectedSegmentIndex==1) {
        [self.hotBaseTopicView.slDataSouresArr removeObjectAtIndex:self.selectedIndex];
        [self.hotBaseTopicView.slTableView reloadData];
        [self SLTopicView:self.hotBaseTopicView deleteTopicAtIndexPath:self.selectedIndex];
    }else if (seg.selectedSegmentIndex==0) {
        [self.followBaseTopicView.slDataSouresArr removeObjectAtIndex:self.selectedIndex];
        [self.followBaseTopicView.slTableView reloadData];
        [self SLTopicView:self.followBaseTopicView deleteTopicAtIndexPath:self.selectedIndex];
    }else{
        [self.lastBaseTopicView.slDataSouresArr removeObjectAtIndex:self.selectedIndex];
        [self.lastBaseTopicView.slTableView reloadData];
        [self SLTopicView:self.lastBaseTopicView deleteTopicAtIndexPath:self.selectedIndex];
    }
}
- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetwork = NO;
            //没有网络的情况下给出提示
            NSData * followsConfigData=[[NSUserDefaults standardUserDefaults] objectForKey:[URL_ROOT_PATIENT stringByAppendingString:@"/users/follows/topics"]];
            NSDictionary *followsConfigDict=[NSKeyedUnarchiver unarchiveObjectWithData:followsConfigData];
            NSArray *followsArr=followsConfigDict[@"result"];
            if (followsArr.count==0) {
                [self.followBaseTopicView addNetWorkViews:@"世界上最遥远的距离就是－－妹有网"];
            }
            NSData * lastConfigData=[[NSUserDefaults standardUserDefaults] objectForKey:[URL_ROOT_PATIENT stringByAppendingString:@"/topics"]];
            NSDictionary *lastConfigDict=[NSKeyedUnarchiver unarchiveObjectWithData:lastConfigData];
            NSArray *lastArr=lastConfigDict[@"result"];
            if (lastArr==0) {
                [self.lastBaseTopicView addNetWorkViews:@"世界上最遥远的距离就是－－妹有网"];
            }
            NSData * hotConfigData=[[NSUserDefaults standardUserDefaults] objectForKey:[URL_ROOT_PATIENT stringByAppendingString:@"/topics/popular"]];
            NSDictionary *hotConfigDict=[NSKeyedUnarchiver unarchiveObjectWithData:hotConfigData];
            NSArray *hotArr=hotConfigDict[@"result"];
            if (hotArr==0) {
                [self.hotBaseTopicView addNetWorkViews:@"世界上最遥远的距离就是－－妹有网"];
            }
        }else if ([netWorkStaus isEqualToString:@"wifi"]||[netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            if (!self.hasNetwork) {
                [self.followBaseTopicView.noNetworkOrDefaultView removeFromSuperview];
                if (seg.selectedSegmentIndex == 0) {
                    
                    if (self.followBaseTopicView.slDataSouresArr.count==0) {
                        //                        [self.followBaseTopicView.slTableView.mj_header beginRefreshing];
                    }
                    
                }
                
                [self.lastBaseTopicView.noNetworkOrDefaultView removeFromSuperview];
                if (seg.selectedSegmentIndex == 2) {
                    if (self.lastBaseTopicView.slDataSouresArr.count==0) {
                        [self.lastBaseTopicView.slTableView.mj_header beginRefreshing];
                    }
                }
                
                [self.hotBaseTopicView.noNetworkOrDefaultView removeFromSuperview];
                if (seg.selectedSegmentIndex == 1) {
                    if (self.hotBaseTopicView.slDataSouresArr.count==0) {
                        [self.hotBaseTopicView.slTableView.mj_header beginRefreshing];
                    }
                }
            }
            
            self.hasNetwork = YES;
        }
    }
    
}
- (void)slSegmentControlClickedButton:(UIButton *)btn{
    
    if (btn.tag*screen_W == self.scrollview.contentOffset.x) {
        if (btn.tag == 0) {
            if ([self.followBaseTopicView.slTableView.mj_header isRefreshing]) {
                return;
            }
            [self.followBaseTopicView.slTableView.mj_header beginRefreshing];
            [self.followBaseTopicView.slTableView.mj_footer setHidden:YES];
        }else if(btn.tag == 1){
            if ([self.hotBaseTopicView.slTableView.mj_header isRefreshing]) {
                return;
            }
            [self.hotBaseTopicView.slTableView.mj_header beginRefreshing];
            [self.hotBaseTopicView.slTableView.mj_footer setHidden:YES];
        }else if(btn.tag == 2){
            if ([self.lastBaseTopicView.slTableView.mj_header isRefreshing]) {
                return;
            }
            [self.lastBaseTopicView.slTableView.mj_header beginRefreshing];
            [self.lastBaseTopicView.slTableView.mj_footer setHidden:YES];
        }
    }
    [self.scrollview setContentOffset:CGPointMake(screen_W*btn.tag, 0) animated:YES];
    
    self.lastContentOffset = self.scrollview.contentOffset.x;
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.lastContentOffset = scrollView.contentOffset.x;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat destWidth;
    CGFloat currentWidth = [seg.titleWidthArr[seg.selectedSegmentIndex] doubleValue]+4;
    
    if ((scrollView.contentOffset.x - self.lastContentOffset)>0.0) {
        NSInteger index = seg.selectedSegmentIndex+1;
        if(index<seg.titleWidthArr.count){
            destWidth = [seg.titleWidthArr[index] doubleValue]+4;
            seg.titleBottomView.width = (scrollView.contentOffset.x-self.lastContentOffset)/screen_W*(destWidth-currentWidth)+currentWidth;
            if (index == 1) {
                if (!self.hasLoadPopularCache) {
                    [self getCacheData:index];
                }
            }
            if (index == 2) {
                if (!self.hasLoadnewTopicHasCache) {
                    [self getCacheData:index];
                }
            }
        }
        
        
    }else{
        NSInteger index = seg.selectedSegmentIndex-1;
        if(index>-1){
            destWidth = [seg.titleWidthArr[index] doubleValue]+4;
            seg.titleBottomView.width = (self.lastContentOffset-scrollView.contentOffset.x)/screen_W*(destWidth-currentWidth)+currentWidth;
        }
    }
    seg.titleBottomView.centerX = seg.startLoc+scrollView.contentOffset.x/screen_W*((ScreenW-120)/3);
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int index = self.scrollview.contentOffset.x / self.view.width;
    if (index==1) {
        
        self.topicKey = [URL_ROOT_PATIENT stringByAppendingString:@"/topics/popular"];
        if (self.isRefreshHotsTopics) {
            [self.hotBaseTopicView.slTableView.mj_header beginRefreshing];
            [self.hotBaseTopicView.slTableView.mj_footer setHidden:YES];
        }
        
        self.hotBaseTopicView.slTableView.scrollsToTop = YES;
        self.followBaseTopicView.slTableView.scrollsToTop = NO;
        self.lastBaseTopicView.slTableView.scrollsToTop = NO;
        
    }else if (index==0) {
        
        if (ApplicationDelegate.Basic) {
            if (self.followBaseTopicView.notLoginView) {
                [self.followBaseTopicView.notLoginView removeFromSuperview];
            }
            self.topicKey=[URL_ROOT_PATIENT stringByAppendingString:@"/users/follows/topics"];
            if (self.isRefreshFollowsTopics) {
                [self.followBaseTopicView.slTableView.mj_header beginRefreshing];
                [self.followBaseTopicView.slTableView.mj_footer setHidden:YES];
            }
        }else{
            [self.followBaseTopicView addNotLoginView:@"没有登录什么也看不了"];
        }
        self.hotBaseTopicView.slTableView.scrollsToTop=NO;
        self.followBaseTopicView.slTableView.scrollsToTop=YES;
        self.lastBaseTopicView.slTableView.scrollsToTop=NO;
        
    }else{
        
        self.topicKey = [URL_ROOT_PATIENT stringByAppendingString:@"/topics"];
        
        if (self.isRefreshLastTopics) {
            [self.lastBaseTopicView.slTableView.mj_header beginRefreshing];
            [self.lastBaseTopicView.slTableView.mj_footer setHidden:YES];
        }
        self.hotBaseTopicView.slTableView.scrollsToTop=NO;
        self.followBaseTopicView.slTableView.scrollsToTop=NO;
        self.lastBaseTopicView.slTableView.scrollsToTop=YES;
    }
    
    [seg setSegmentControlIndex:index];
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [self scrollViewDidEndDecelerating:scrollView];
}
// 没有内容
-(void)addNotDatas:(NSString *)str tableView:(UITableView *)tableView index:(NSInteger)index{
    for (int i = 0; i<3; i++) {
        UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW , ScreenH)];
        UILabel *tiplabel = [[UILabel alloc] init];
        CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
        
        contentView.backgroundColor = SLColor(245, 245, 245);
        tiplabel.font = [UIFont systemFontOfSize:17];
        tiplabel.textAlignment = NSTextAlignmentCenter;
        NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
        [AttributedStr addAttribute:NSFontAttributeName
         
                              value:[UIFont systemFontOfSize:17.0]
         
                              range:NSMakeRange(2, 2)];
        
        tiplabel.attributedText = AttributedStr;
        [contentView addSubview:tiplabel];
        UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
        bolangLabel.text = @"~~~";
        bolangLabel.font = [UIFont systemFontOfSize:12];
        bolangLabel.textColor = SLColor(213, 213, 213);
        [contentView addSubview:bolangLabel];
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
        imgView.image = [UIImage imageNamed:@"swan"];
        //        if (index==1) {
        //            self.hasNoTopicsDatasView = contentView;
        //        }else if (index==2){
        //            self.hasNoReciveDatasView = contentView;
        //        }
        [contentView addSubview:imgView];
        [tableView addSubview:contentView];
    }
    
}

@end
