//
//  SLAttentionViewController.m
//  Superloop
//
//  Created by 张梦川 on 16/6/6.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLAttentionViewController.h"
#import "SLTopicDetailViewController.h"
#import "SLOtherPersonViewController.h"
#import "SLMeTableViewController.h"
#import "AppDelegate.h"
@interface SLAttentionViewController ()

@end
static NSInteger const cols = 3;
static CGFloat const margin = 5;
#define  cellWH  ((ScreenW - 70 - (cols - 1) * margin) / cols)

@implementation SLAttentionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //当返回来的时候取消单元格的选中
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    // SLTopicModel *model = self.topics[indexPath.row];
    SLTopicModel *model = self.topics[indexPath.section];
    
    SLTopicDetailViewController *topicDetail = [[SLTopicDetailViewController alloc] init];
    topicDetail.model = model;
    //点击时候计算好cell的内容frame
    //头像高度
    topicDetail.cellHeight = 45;
    // 标题高度
    CGFloat textMaxW = ScreenW - 50;
    
    if (model.digest.length == 0) {
        topicDetail.cellHeight = 45;
    }
    
    //渠道每行的高度
    NSString *titleContentextString = @"Text";
    CGFloat titleFontSize = [titleContentextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
    CGFloat titleHeight = [self.title boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
    NSInteger newLineNum = titleHeight / titleFontSize;
    if (newLineNum > 2) {
        newLineNum = 2;
    }
    topicDetail.cellHeight += newLineNum * titleFontSize + 11;
    //内容高度
    NSString *contentTextString = @"Text";
    CGFloat contemtFontSize = [contentTextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
    CGFloat contentHeight= [model.digest boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
    NSInteger newContentLineNum = contentHeight / contemtFontSize;
    
    //    if (newContentLineNum > 8) {
    //        newContentLineNum = 8;
    //    }
    topicDetail.cellHeight += newContentLineNum * contemtFontSize  +11;
    //图片高度
    if (model.imgs.count > 0) {
        
        topicDetail.cellHeight += ((model.imgs.count - 1) / 3 + 1) * cellWH + (((model.imgs.count - 1) / 3 + 1) - 1) * margin + 8;
    }
    else
    {
        topicDetail.cellHeight += 50;
    }
    // 底部工具条
    topicDetail.cellHeight += 89;
    
}

//子类实现头像被点击的代理方法
- (void)didUserIconClicked:(NSIndexPath *)indexpath
{
    SLTopicModel *model = self.topics[indexpath.row];
    if ([[NSString stringWithFormat:@"%@", model.user[@"id"]] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        SLMeTableViewController *myView = [[SLMeTableViewController alloc] init];
        //myView.tag = 1000;
        myView.isPassValue=YES;
        myView.nickName=model.user[@"nickname"];
        
        
    }else
    {
        
        SLOtherPersonViewController *otherPerson = [[SLOtherPersonViewController alloc] init];
        otherPerson.othersID = model.user[@"id"];
        otherPerson.nickName=model.user[@"nickname"];
        
        [self.navigationController pushViewController:otherPerson animated:YES];
   
        
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
