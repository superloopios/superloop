//
//  SLIndexPathCollection.h
//  Superloop
//
//  Created by WangJiWei on 16/3/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLIndexPathCollection : UICollectionView
/**
 *  当前的索引
 */
@property (nonatomic,strong) NSIndexPath *indexPath;

/**
 *  tag 当多个CollectionView 时 给CollectionView 添加tag
 */
@property (nonatomic, assign) NSInteger collectionViewTag;
@end
