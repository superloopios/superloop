//
//  SLTopicDetalModel.m
//  Superloop
//
//  Created by 朱宏伟 on 16/5/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTopicDetalModel.h"

static NSInteger const cols = 3;
static CGFloat const margin = 5;
#define  cellWH  ((ScreenW - 70 - (cols - 1) * margin) / cols)
#define rows ((self.imgs.count - 1) / 3 + 1)

@implementation SLTopicDetalModel


- (CGFloat)cellHeight
{
    if (_cellHeight) return _cellHeight;
    //头像高度
    _cellHeight = 45;
    // 标题高度
    CGFloat textMaxW = ScreenW - 50;
    //渠道每行的高度
    NSString *titleContentextString = @"Text";
    CGFloat titleFontSize = [titleContentextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]} context:nil].size.height;
    CGFloat titleHeight = [self.title boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]} context:nil].size.height;
    NSInteger newLineNum = titleHeight / titleFontSize;
    if (newLineNum > 2) {
        newLineNum = 2;
    }
    _cellHeight += newLineNum * titleFontSize + 11;
    //内容高度
    NSString *contentTextString = @"Text";
    CGFloat contemtFontSize = [contentTextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
    CGFloat contentHeight= [self.digest boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
    NSInteger newContentLineNum = contentHeight / contemtFontSize;
    
    if (newContentLineNum > 8) {
        newContentLineNum = 8;
    }
    _cellHeight += newContentLineNum * contemtFontSize  + 11 +(newContentLineNum - 1) * 5;
    //图片高度
    if (self.imgs.count > 0) {
        
        _cellHeight += rows * cellWH + (rows - 1) * margin + 8;
    }
    else
    {
        _cellHeight += 50;
    }
    //计算时间
    
    // 底部工具条
    _cellHeight += 39 + 80;
    
    return _cellHeight;
}


- (NSString *)created
{
    // 将时间戳字符串转成日期
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_created.doubleValue / 1000];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
    
    fmt.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *dateStr = [fmt stringFromDate:date];
    NSDate *createdAtDate = [fmt dateFromString:dateStr];
    
    if (createdAtDate.sl_isThisYear) { // 今年
        if (createdAtDate.sl_isYesterday) { // 昨天
            fmt.dateFormat = @"昨天 HH:mm";
            return [fmt stringFromDate:createdAtDate];
        } else if (createdAtDate.sl_isToday) { // 今天
            NSCalendar *calendar = [NSCalendar sl_calendar];
            NSCalendarUnit unit = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
            NSDateComponents *cmps = [calendar components:unit fromDate:createdAtDate toDate:[NSDate date] options:0];
            
            if (cmps.hour >= 1) { // 时间间隔 >= 1小时
                return [NSString stringWithFormat:@"%zd小时前", cmps.hour];
            } else if (cmps.minute >= 1) { // 1小时 > 时间间隔 >= 1分钟
                return [NSString stringWithFormat:@"%zd分钟前", cmps.minute];
            } else { // 时间间隔 < 1分钟
                return @"刚刚";
            }
        } else {
            fmt.dateFormat = @"MM-dd HH:mm";
            return [fmt stringFromDate:createdAtDate];
        }
    } else { // 不是今年
        return _created;
    }
    

}

@end
