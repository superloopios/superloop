//
//  SLTopicDetalModel.h
//  Superloop
//
//  Created by 朱宏伟 on 16/5/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLTopicDetalModel : NSObject

@property (nonatomic, copy) NSString *title;  //标题
@property (nonatomic, copy) NSString *digest; //内容
@property (nonatomic, strong) NSDictionary *user; //用户信息
@property (nonatomic, strong) NSArray *imgs; // 图片集
@property (nonatomic, copy) NSString *created; //创建时间
@property (nonatomic, copy) NSString *id;  //帖子id;
@property (nonatomic, copy) NSString *views; //浏览数
@property (nonatomic, copy) NSString *thumbups_cnt; //点赞数
@property (nonatomic, copy) NSString *has_thumbsuped;  //是否点赞
@property (nonatomic, assign) CGFloat cellHeight;




@end
