//
//  SLTopicModel.h
//  Superloop
//
//  Created by WangJiWei on 16/3/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLTopicModel : NSObject
@property (nonatomic, copy) NSString *title;  //标题
@property (nonatomic, copy) NSString *digest; //内容
@property (nonatomic, copy) NSString *content; //内容

@property (nonatomic, strong) NSDictionary *user; //用户信息
@property (nonatomic, strong) NSArray *imgs; // 图片集
@property (nonatomic, copy) NSString *created; //创建时间
@property (nonatomic, copy) NSString *id;  //帖子id;
@property (nonatomic, copy) NSString *views; //浏览数
@property (nonatomic, copy) NSString *thumbups_cnt; //点赞数
@property (nonatomic, copy) NSString *has_thumbsuped;  //是否点赞
@property (nonatomic, copy) NSString *replies_cnt; //评论数量
@property (nonatomic, copy) NSString *price;   //价格
@property (nonatomic, copy) NSString *has_collected;   //收藏
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, assign) CGFloat titleHeight;
@property (nonatomic, assign) CGFloat contentHeight;
@property (nonatomic, assign) CGFloat imgsHeight;

@end
