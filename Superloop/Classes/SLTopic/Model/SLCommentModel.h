//
//  SLCommentModel.h
//  Superloop
//
//  Created by WangJiwei on 16/4/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLCommentModel : NSObject
@property (nonatomic, copy) NSString *digest; //内容
@property (nonatomic, copy) NSString *content; //内容
@property (nonatomic, strong) NSDictionary *user; //用户信息
@property (nonatomic, strong) NSDictionary *reply_to_user;//回复用户信息
@property (nonatomic, copy) NSString *created; //创建时间

@property (nonatomic, assign) CGFloat commentCellHeight;
@property (nonatomic, copy) NSString  *user_id;
@property (nonatomic, copy) NSString  *id;

@property (nonatomic, assign) CGFloat contentHeight;//内容高度

@end
