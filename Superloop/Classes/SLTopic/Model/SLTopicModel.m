//
//  SLTopicModel.m
//  Superloop
//
//  Created by WangJiWei on 16/3/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTopicModel.h"
#import "SLCalculateSpace.h"
static NSInteger const cols = 3;
static CGFloat const margin = 5;
#define  cellWH  ((ScreenW - 50 - (cols - 1) * margin) / cols)
#define rows ((self.imgs.count - 1) / 3 + 1)
@implementation SLTopicModel
- (CGFloat)cellHeight{
    if (_cellHeight) return _cellHeight;
    //头像高度
    _cellHeight = 50;
    //图像距标题的高度
    _cellHeight += 5;
    //标题每行的高度
    _titleHeight = [self calculateHeight:self.title withFont:[UIFont boldSystemFontOfSize:20] withWidth:ScreenW-40 lineSpace:13 wordSpace:@0.0f line:2];
    _cellHeight += _titleHeight;
    //标题和内容
    _cellHeight += 10;
    //话题内容
    _contentHeight = [self calculateHeight:self.digest withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-40 lineSpace:13 wordSpace:@0.0f line:8];
    _cellHeight += _contentHeight;
    //话题内容距图片
    _cellHeight += 18;
    //图片高度
    if (self.imgs.count > 0) {
       // NSInteger rows=(self.imgs.count - 1) / 3 + 1;
        CGFloat imgHeight;
        if (self.imgs.count == 1 || self.imgs.count==2) {
            imgHeight = 1.5 * cellWH + rows*5;
        }else if (self.imgs.count == 4) {
            imgHeight = 3 * cellWH + rows*5;
        }else if (self.imgs.count > 9) {
            imgHeight = 3 * cellWH + 2*5;
        }else{
            imgHeight = rows * cellWH + rows*5;
        }
        _imgsHeight = imgHeight;
        _cellHeight += imgHeight + 18;
    }
    //点赞
    _cellHeight += 36;
    return _cellHeight;
}
//计算高度
- (CGFloat)calculateHeight:(NSString *)str withFont:(UIFont *)font withWidth:(CGFloat)width lineSpace:(CGFloat)lineSpace wordSpace:(NSNumber *)wordSpace line:(NSInteger)line{
    CGFloat standardHeight = [SLCalculateSpace getSpaceHeight:@"高度，高度" withFont:font withWidth:width lineSpace:lineSpace wordSpace:wordSpace];
    CGFloat titleHeight = [SLCalculateSpace getSpaceHeight:str withFont:font withWidth:width lineSpace:lineSpace wordSpace:wordSpace];
    CGFloat fCount = titleHeight/standardHeight;
    NSInteger count = titleHeight/standardHeight;
    if (fCount > count) {
        count += 1;
    }
    if (count > line) {
        count = line;
    }
    return count * standardHeight;
}
- (NSString *)created
{
    // 将时间戳字符串转成日期
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_created.doubleValue / 1000];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *dateStr = [fmt stringFromDate:date];
    NSDate *createdAtDate = [fmt dateFromString:dateStr];
    
    if (createdAtDate.sl_isThisYear) { // 今年
        if (createdAtDate.sl_isYesterday) { // 昨天
            fmt.dateFormat = @"昨天 HH:mm";
            return [fmt stringFromDate:createdAtDate];
        } else if (createdAtDate.sl_isToday) { // 今天
            NSCalendar *calendar = [NSCalendar sl_calendar];
            NSCalendarUnit unit = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
            NSDateComponents *cmps = [calendar components:unit fromDate:createdAtDate toDate:[NSDate date] options:0];
            
            if (cmps.hour >= 1) { // 时间间隔 >= 1小时
                return [NSString stringWithFormat:@"%zd小时前", cmps.hour];
            } else if (cmps.minute >= 1) { // 1小时 > 时间间隔 >= 1分钟
                return [NSString stringWithFormat:@"%zd分钟前", cmps.minute];
            } else { // 时间间隔 < 1分钟
                return @"刚刚";
            }
        } else {
            fmt.dateFormat = @"MM-dd HH:mm";
            return [fmt stringFromDate:createdAtDate];
        }
    } else { // 不是今年
        return _created;
    }
    
}




@end
