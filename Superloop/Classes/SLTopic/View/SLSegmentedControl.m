//
//  SLSegmentedControl.m
//  Superloop
//
//  Created by administrator on 16/8/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSegmentedControl.h"

@interface SLSegmentedControl ()
@property (nonatomic, strong)NSMutableArray *btnArr;
@end

@implementation SLSegmentedControl

- (NSMutableArray *)btnArr{
    if (!_btnArr) {
        _btnArr = [[NSMutableArray alloc] init];
    }
    return _btnArr;
}
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
    }
    return self;
}

- (void)setTitleArr:(NSArray *)titleArr{
    
    _titleArr = titleArr;
    NSInteger count = titleArr.count;
    
    for (int i = 0; i<titleArr.count; i++) {
        CGFloat titleWidth = 0;
        UIButton *btn = [[UIButton alloc] init];
        [self.btnArr addObject:btn];
        
        btn.frame = CGRectMake(i*(self.width/count), 0, self.width/count, 44);
        btn.tag = i;
        btn.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [btn setTitleColor:SLColor(124, 124, 124) forState:UIControlStateNormal];
        [btn setTitleColor:SLColor(0, 0, 0) forState:UIControlStateSelected];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        NSString *title = titleArr[i];
        titleWidth = [title boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:16]} context:nil].size.width;
        [self.titleWidthArr addObject:@(titleWidth)];
        [self addSubview:btn];
        
        if (i == 0) {
            btn.selected = YES;
//            NSLog(@"%f,%f",btn.titleLabel.width,btn.titleLabel.x);
            self.selectBtn = btn;
            self.selectedSegmentIndex = 0;
            
        }
    }
    CGFloat titleWidth = [self.titleWidthArr[0] doubleValue];
    UIView *titleBottomView = [[UIView alloc] init];
    titleBottomView.backgroundColor = SLMainColor;
    titleBottomView.frame = CGRectMake((self.selectBtn.width-titleWidth-4)/2, self.frame.size.height-2, titleWidth+4, 2);
//    titleBottomView.frame = CGRectMake((self.selectBtn.width-titleWidth-4)/2, 40, titleWidth+4, 2);
    self.startLoc = titleBottomView.centerX;
    
//    titleBottomView.centerX = self.selectBtn.centerX;
    
    [self addSubview:titleBottomView];
    self.titleBottomView = titleBottomView;
}
- (void)btnClick:(UIButton *)btn{
//    self.selectBtn.selected = NO;
//    btn.selected = YES;
//    self.selectBtn = btn;
//    self.titleBottomView.width = btn.titleLabel.width+4;
//    self.startLoc = btn.titleLabel.x-2;
//    [UIView animateWithDuration:0.15 animations:^{
//        self.titleBottomView.centerX = btn.centerX;
////        self.titleBottomView.width = btn.titleLabel.width+4;
//    }completion:^(BOOL finished) {
//        
//    }];
//    self.selectedSegmentIndex = btn.tag;
    if ([self.delegate respondsToSelector:@selector(slSegmentControlClickedButton:)]) {
        [self.delegate performSelector:@selector(slSegmentControlClickedButton:) withObject:btn];
    }
    if ([self.delegate respondsToSelector:@selector(slSegmentControlClickedButtonIndex:)]) {
        [self.delegate slSegmentControlClickedButtonIndex:btn.tag];
    }
//    [self setSegmentControlIndex:btn.tag];
    
}
- (void)setSegmentControlIndex:(NSInteger)index{
    UIButton *btn = self.btnArr[index];
    self.selectBtn.selected = NO;
    btn.selected = YES;
    self.selectBtn = btn;
    self.titleBottomView.width = btn.titleLabel.width+4;
    self.titleBottomView.centerX = btn.centerX;
    self.selectedSegmentIndex = btn.tag;
}


- (NSMutableArray *)titleWidthArr{
    if (!_titleWidthArr) {
        _titleWidthArr = [NSMutableArray array];
    }
    return _titleWidthArr;
}

@end
