//
//  SLTopicDetailView.m
//  Superloop
//
//  Created by WangS on 16/11/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTopicDetailView.h"
#import "SLTopicModel.h"
#import "SLTopicCollectionViewCell.h"
#import <Photos/Photos.h>
#import "MWPhotoBrowser.h"
#import "SLCalculateSpace.h"
#import "SLTopicTextView.h"

#define cellWH  ((ScreenW - 50 - (cols - 1) * margin) / cols)
static NSInteger const cols = 3;
static CGFloat const margin = 5;

@interface SLTopicDetailView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,MWPhotoBrowserDelegate>

@property (nonatomic,strong) SLTopicTextView *titleTextView;//标题
@property (nonatomic,strong) SLTopicTextView *contentTextView;//内容
@property (nonatomic,strong) UICollectionView *imgCollectionView;//图片
@property (nonatomic,strong) UILabel *timeLab;//时间
@property (nonatomic,strong) NSMutableArray *photos; //图片数组

@end

@implementation SLTopicDetailView

- (SLTopicTextView *)titleTextView{
    if (!_titleTextView) {
        _titleTextView = [[SLTopicTextView alloc] init];
        _titleTextView.editable = NO;
        _titleTextView.scrollEnabled = NO;
        [self addSubview:_titleTextView];
    }
    return _titleTextView;
}
- (SLTopicTextView *)contentTextView{
    if (!_contentTextView) {
        _contentTextView = [[SLTopicTextView alloc] init];
        _contentTextView.editable = NO;
        _contentTextView.scrollEnabled = NO;
        [self addSubview:_contentTextView];
    }
    return _contentTextView;
}
- (UICollectionView *)imgCollectionView{
    if (!_imgCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(50, 50);
        _imgCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _imgCollectionView.dataSource = self;
        _imgCollectionView.delegate = self;
        _imgCollectionView.scrollEnabled = NO;
        _imgCollectionView.showsVerticalScrollIndicator = NO;
        _imgCollectionView.showsHorizontalScrollIndicator = NO;
        _imgCollectionView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_imgCollectionView];
        [_imgCollectionView registerNib:[UINib nibWithNibName:@"SLTopicCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionViewCells"];
    }
    return _imgCollectionView;
}
- (UILabel *)timeLab{
    if (!_timeLab) {
        _timeLab = [[UILabel alloc] init];
        _timeLab.textColor = UIColorFromRGB(0x888888);
        _timeLab.font = [UIFont systemFontOfSize:13];
        _timeLab.textAlignment = NSTextAlignmentRight;
        [self addSubview:_timeLab];
    }
    return _timeLab;
}
- (NSMutableArray *)photos{
    if (!_photos) {
        _photos = [NSMutableArray new];
    }
    return _photos;
}
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
- (void)setTopicModel:(SLTopicModel *)topicModel{
    _topicModel = topicModel;
    self.topicHeight = 25;
    //标题
    [SLCalculateSpace setTextViewSpace:self.titleTextView withValue:topicModel.title withFont:[UIFont boldSystemFontOfSize:20] lineSpace:13.0 wordSpace:@0.0f];
    CGFloat titleHeight = [self calculateHeight:topicModel.title withFont:[UIFont boldSystemFontOfSize:20] withWidth:ScreenW - 36 lineSpace:13.0 wordSpace:@0.0f oneLine:YES];
    [self.titleTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset(25);
        make.left.mas_equalTo(self.mas_left).offset(18);
        make.right.mas_equalTo(self.mas_right).offset(-18);
        make.height.mas_equalTo(titleHeight+5);
    }];
    self.topicHeight += titleHeight + 25;
    
    //内容
    NSString *chooseContent = topicModel.content ? topicModel.content : topicModel.digest;
    [SLCalculateSpace setTextViewSpace:self.contentTextView withValue:chooseContent withFont:[UIFont systemFontOfSize:15] lineSpace:13.0 wordSpace:@0.0f];
    CGFloat contentHeight;
    if (ScreenW == 320) {
        contentHeight = [self calculateHeight:chooseContent withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW - 47 lineSpace:13.0 wordSpace:@0.0f oneLine:NO];
    }else if (ScreenW == 375) {
        if ([[UIDevice currentDevice].systemVersion doubleValue] > 10.0) {
            contentHeight = [self calculateHeight:chooseContent withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW - 42 lineSpace:13.0 wordSpace:@0.0f oneLine:NO];
        }else{
            contentHeight = [self calculateHeight:chooseContent withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW - 47 lineSpace:13.0 wordSpace:@0.0f oneLine:NO];
        }
    }else{
        contentHeight = [self calculateHeight:chooseContent withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW - 42 lineSpace:13.0 wordSpace:@0.0f oneLine:NO];
    }
    
    [self.contentTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleTextView.mas_bottom).offset(15);
        make.left.mas_equalTo(self.mas_left).offset(18);
        make.right.mas_equalTo(self.mas_right).offset(-18);
        make.height.mas_equalTo(contentHeight+5);
    }];
    self.topicHeight += contentHeight ;
    
    //图片
    if (topicModel.imgs.count >0) {
        [self addSubview:self.imgCollectionView];
        NSInteger rows=(topicModel.imgs.count - 1) / 3 + 1;
        CGFloat imgHeight;
        if (topicModel.imgs.count == 1 || topicModel.imgs.count==2) {
            imgHeight = 1.5 * cellWH + rows*5;
        }else if (topicModel.imgs.count == 4) {
            imgHeight = 3 * cellWH + rows*5;
        }else if (topicModel.imgs.count > 9) {
            imgHeight = 3 * cellWH + 2*5;
        }else{
            imgHeight = rows * cellWH + rows*5;
        }
        [self.imgCollectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentTextView.mas_bottom).offset(15);
            make.left.mas_equalTo(self.mas_left).offset(20);
            make.right.mas_equalTo(self.mas_right).offset(-20);
            make.height.mas_equalTo(imgHeight);
            
        }];
        [self.timeLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.imgCollectionView.mas_bottom).offset(25);
            make.left.mas_equalTo(self.mas_left).offset(20);
            make.right.mas_equalTo(self.mas_right).offset(-20);
            make.height.mas_equalTo(13);
        }];
        self.topicHeight +=  imgHeight + 40;
    }else{
        [self.imgCollectionView removeFromSuperview];
        [self.timeLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentTextView.mas_bottom).offset(25);
            make.left.mas_equalTo(self.mas_left).offset(20);
            make.right.mas_equalTo(self.mas_right).offset(-20);
            make.height.mas_equalTo(13);
        }];
        self.topicHeight += 25;
    }
    //时间
    self.timeLab.text = topicModel.created;
    self.topicHeight += 38;
}
//计算高度
- (CGFloat)calculateHeight:(NSString *)str withFont:(UIFont *)font withWidth:(CGFloat)width lineSpace:(CGFloat)lineSpace wordSpace:(NSNumber *)wordSpace oneLine:(BOOL)isTitleOrContent{
    CGFloat standardHeight = [SLCalculateSpace getSpaceHeight:@"高度，高度" withFont:font withWidth:width lineSpace:lineSpace wordSpace:wordSpace];
    CGFloat titleHeight = [SLCalculateSpace getSpaceHeight:str withFont:font withWidth:width lineSpace:lineSpace wordSpace:wordSpace];
    CGFloat fCount = titleHeight/standardHeight;
    NSInteger count = titleHeight/standardHeight;
    if (fCount > count) {
        count += 1;
    }
    return count * standardHeight;
}
#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.topicModel.imgs.count>9) {
        return 9;
    }
    return self.topicModel.imgs.count;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.topicModel.imgs.count == 1) {
        return CGSizeMake(ScreenW - 50, 1.5 * cellWH);
    }else if (self.topicModel.imgs.count == 2 || self.topicModel.imgs.count == 4){
        return CGSizeMake((ScreenW - 50-5)/2.0, 1.5 * cellWH);
    }
    return CGSizeMake(cellWH, cellWH);
}
//每个UICollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SLTopicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCells" forIndexPath:indexPath];
    if (self.topicModel.imgs.count > 0) {
        NSString *imageUrl = [self.topicModel.imgs[indexPath.row][@"url"] stringByAppendingString:@"@!topic"];
        [cell setupCellWithImageURl:imageUrl];
    }
    return cell;
}
//每个UICollectionView的点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.photos removeAllObjects];
    for (NSDictionary *dict in self.topicModel.imgs) {
        [self.photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:dict[@"url"]]]];
    }
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    [browser setCurrentPhotoIndex:indexPath.row];
    browser.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    if ([self.delegate respondsToSelector:@selector(SLTopicDetailView:didSelectCollectionByMWPhotoBrowser:WithModel:)]) {
        [self.delegate SLTopicDetailView:self didSelectCollectionByMWPhotoBrowser:browser WithModel:self.topicModel];
    }
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (self.photos.count>9) {
        return 9;
    }
    return _photos.count;
}
#pragma 代理
- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count)
        return [self.photos objectAtIndex:index];
    return nil;
}

@end
