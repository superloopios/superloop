//
//  SLtTopicRepetTableViewCell.h
//  Superloop
//
//  Created by 张梦川 on 16/5/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLtTopicRepetTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
