//
//  SLSawBtn.m
//  Superloop
//
//  Created by 朱宏伟 on 16/6/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSawBtn.h"

@implementation SLSawBtn

// 不需要系统提供的高亮样式
- (void)setHighlighted:(BOOL)highlighted{
    
}
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        if (screen_W == 320) {
            self.titleLabel.font = [UIFont systemFontOfSize:14];
        }else{
            self.titleLabel.font = [UIFont systemFontOfSize:15];
        }
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    self.titleLabel.x = (self.width-self.titleLabel.width)/2+10;
    
    self.imageView.x = self.titleLabel.x - 25;
    self.imageView.y = self.titleLabel.y+(self.titleLabel.height-self.imageView.height)/2;
    
    
}
- (void)setTextColor:(UIColor *)textColor{
    [self setTitleColor:textColor forState:UIControlStateNormal];
}
@end
