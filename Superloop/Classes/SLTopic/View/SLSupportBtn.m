//
//  SLSupportBtn.m
//  Superloop
//
//  Created by 朱宏伟 on 16/6/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSupportBtn.h"

@implementation SLSupportBtn

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.selected = !self.selected;
        _imgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 15, 15)];
        [self addSubview:_imgView];
        
        _titleLab=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgView.frame)+15, 8, self.width-CGRectGetMaxX(_imgView.frame), 15)];
        
        [self addSubview:_titleLab];
    }
    return self;
}


@end
