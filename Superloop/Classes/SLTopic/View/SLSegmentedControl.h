//
//  SLSegmentedControl.h
//  Superloop
//
//  Created by administrator on 16/8/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SLSegmentControlDelegate <NSObject>
@optional
- (void)slSegmentControlClickedButton:(UIButton *)btn;
- (void)slSegmentControlClickedButtonIndex:(NSInteger)index;
@end

@interface SLSegmentedControl : UIView
@property(nonatomic, strong)UIButton *selectBtn;

@property (nonatomic, assign)NSInteger selectedSegmentIndex;
@property (nonatomic , strong) NSArray *titleArr;
@property (nonatomic, strong)NSMutableArray *titleWidthArr;
@property (nonatomic, weak) id<SLSegmentControlDelegate>delegate;
@property (nonatomic, assign)CGFloat startLoc;
@property (nonatomic, weak) UIView *titleBottomView;

-(void)setSegmentControlIndex:(NSInteger)index;
@end
