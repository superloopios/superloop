//
//  SLCommentCell.h
//  Superloop
//
//  Created by WangJiWei on 16/3/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLCommentModel;
@class SLCommentCell;
@protocol SLCommentCellDelegate <NSObject>
- (void)didCommentUserIconClicked:(NSIndexPath *)indexpath;
//点赞按钮被点击
- (void)didZanClicked:(UIButton *)btn:(NSIndexPath *)indexpath;

//- (void)zanTextStrHeght:(CGFloat)heght:(NSIndexPath *)indexpath;

- (void)didClickUserNickName:(NSIndexPath *)indexpath;

@end
@interface SLCommentCell : UITableViewCell
@property (nonatomic, strong)SLCommentModel *commentModel;
@property (nonatomic, weak) id<SLCommentCellDelegate>Delegate;
@property (nonatomic , strong) NSIndexPath *indexpath;
@end
