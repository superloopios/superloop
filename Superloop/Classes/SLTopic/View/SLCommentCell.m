//
//  SLCommentCell.m
//  Superloop
//
//  Created by WangJiWei on 16/3/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "SLCommentModel.h"
#import "SLCommentCell.h"
#import <UIImageView+WebCache.h>
#import "AppDelegate.h"
#import "TTTAttributedLabel.h"
#import "SLCalculateSpace.h"

@class TTTAttributedLabel;
@interface SLCommentCell()<TTTAttributedLabelDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *userIcon;
@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet UILabel *creatTime;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *content;
//点赞按钮
@property (weak, nonatomic) IBOutlet UIButton *zan;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineImgViewConstraint;

@end
@implementation SLCommentCell
- (IBAction)zanClck:(id)sender {
    //先将未到时间执行前的任务取消。
    _zan.enabled=NO;
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(zanBtnClick:) object:sender];
    [self performSelector:@selector(zanBtnClick:) withObject:sender afterDelay:0.2f];
}
-(void)zanBtnClick:(id)sender{
    if ([_Delegate respondsToSelector:@selector(didZanClicked::)]) {
        [_Delegate didZanClicked:_zan:self.indexpath];
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    self.userIcon.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecommendView)];
    [self.userIcon addGestureRecognizer:tapView];
    self.userIcon.layer.cornerRadius = 2.5;
    self.userIcon.layer.masksToBounds = YES;
    [self.lineImgViewConstraint setConstant:0.5];
}

- (void)setCommentModel:(SLCommentModel *)commentModel{
    _commentModel = commentModel;
    NSString *imageUrl = commentModel.user[@"avatar"];
    if (imageUrl.length>0) {
        [self.userIcon sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    }else{
        self.userIcon.image=[UIImage imageNamed:@"newPersonAvatar"];
    }
    self.nickName.text = commentModel.user[@"nickname"];
    self.creatTime.text = commentModel.created;
    //计算高度
    NSString *text;
    if (self.commentModel.reply_to_user) {
        text = [NSString stringWithFormat:@"回复%@:%@",commentModel.reply_to_user[@"nickname"],commentModel.content];
        //计算高度
//        [_content setAttributedText:str];
        [_content setLineBreakMode:NSLineBreakByTruncatingTail];
        _content.enabledTextCheckingTypes = NSTextCheckingTypeLink;
        [_content setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
             //设置可点击文字的范围
             NSRange boldRange = [[mutableAttributedString string] rangeOfString:commentModel.reply_to_user[@"nickname"] options:NSCaseInsensitiveSearch];
             //设定可点击文字的的大小
//             UIFont *boldSystemFont = [UIFont boldSystemFontOfSize:15];
            UIFont *boldSystemFont = [UIFont systemFontOfSize:15];
             CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
             if (font) {
                 //设置可点击文本的大小
                 [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:boldRange];
                 //设置可点击文本的颜色
                 [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[UIColorFromRGB(0xff5a5f) CGColor] range:boldRange];
                 CFRelease(font);
             }
            [self lineAndWordSetting:text Attributes:mutableAttributedString];
             return mutableAttributedString;
         }];
        NSRange linkRange = [text rangeOfString:commentModel.reply_to_user[@"nickname"]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@""]];
        NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionary];
        [linkAttributes setValue:[NSNumber numberWithBool:NO] forKey:(NSString *)kCTUnderlineStyleAttributeName];
        _content.linkAttributes = linkAttributes;
        //设置链接的url
        [_content addLinkToURL:url withRange:linkRange];
       }else{
        //_content.text = commentModel.content;
           
           [_content setText:commentModel.content afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
               [self lineAndWordSetting:commentModel.content Attributes:mutableAttributedString];
               return mutableAttributedString;
           }];
    }
    _content.delegate = self;
//    CGFloat contentHeight= [self.content.text boundingRectWithSize:CGSizeMake(_content.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
    //返回代理的高度
//    if ([_Delegate respondsToSelector:@selector(zanTextStrHeght::)]) {
//        [_Delegate zanTextStrHeght:contentHeight:self.indexpath];
//    }
 
    if ([[NSString stringWithFormat:@"%@", commentModel.user_id] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        _zan.hidden = NO;
    }else{
        _zan.hidden = YES;
    }
}
- (void)lineAndWordSetting:(NSString *)content Attributes:(NSMutableAttributedString *)mutableAttributedString{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = 13; //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:15], NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@0.0f
                          };
    NSRange contentRange = NSMakeRange(0, content.length-1);
    [mutableAttributedString addAttributes:dic range:contentRange];
}
- (void)tapRecommendView{
    if ([_Delegate respondsToSelector:@selector(didCommentUserIconClicked:)]) {
        [_Delegate didCommentUserIconClicked:self.indexpath];
    }
}
- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url{
    if ([_Delegate respondsToSelector:@selector(didClickUserNickName:)]) {
        [_Delegate didClickUserNickName:self.indexpath];
    }
}
//- (void)setFrame:(CGRect)frame
//{
//    frame.size.height -= 5;
//    [super setFrame:frame];
//}

@end
