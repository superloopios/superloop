//
//  SLTopicTextView.m
//  Superloop
//
//  Created by WangS on 16/11/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTopicTextView.h"

@implementation SLTopicTextView

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    [super canPerformAction:action withSender:sender];
    if(action == @selector(copy:)){
        return YES;
    }
    else if(action == @selector(selectAll:)){
        return YES;
    }
    return NO;
}

@end
