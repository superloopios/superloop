//
//  SLClassBtn.m
//  Superloop
//
//  Created by WangJiWei on 16/3/25.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLClassBtn.h"

@implementation SLClassBtn

// 不需要系统提供的高亮样式
- (void)setHighlighted:(BOOL)highlighted{
    
}
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:12];
        [self setTitleColor:SLColor(80, 80, 80) forState:UIControlStateNormal];
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    self.imageView.y = 0;
    self.imageView.width = self.width;
    self.imageView.height = self.width;
    self.imageView.centerX = self.width *0.5;
    
    self.titleLabel.width = self.width;
    self.titleLabel.centerX = self.width *0.5;
    self.titleLabel.centerY = (self.height - self.imageView.height) / 2 +self.imageView.height - 3;
    
}

@end
