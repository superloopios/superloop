//
//  SLTopicListTableViewCell.m
//  Superloop
//
//  Created by WangS on 16/11/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTopicListTableViewCell.h"
#import "SLTopicModel.h"
#import "SLTopicCollectionViewCell.h"
#import "MWPhotoBrowser.h"
#import <Photos/Photos.h>
#import "SLCalculateSpace.h"
#import "AppDelegate.h"
#import "SLIndexPathCollection.h"

#define cellWH  ((ScreenW - 50 - (cols - 1) * margin) / cols)
static NSInteger const cols = 3;
static CGFloat const margin = 5;
@interface SLTopicListTableViewCell ()<UICollectionViewDataSource,UICollectionViewDelegate,MWPhotoBrowserDelegate>
@property (nonatomic,strong) UIImageView *topicListIconImg;//图像
@property (nonatomic,strong) UILabel *topicListNicknameLab;//昵称
@property (nonatomic,strong) UILabel *topicListTimeLab;//时间
@property (nonatomic,strong) UIButton *topicListDeleteBtn;//删除按钮
@property (nonatomic,strong) UILabel *topicListTitleLab;//标题
@property (nonatomic,strong) UILabel *topicListContentLab;//内容
@property (nonatomic,strong) UICollectionView *topicListImgCollectionView;//图片
@property (nonatomic,strong) UIButton *topicListPraiseBtn;//点赞按钮
@property (nonatomic,strong) UIButton *topicListCommentBtn;//评论按钮
@property (nonatomic,strong) UIImageView *topicListLineImgView;//分割线
@property (nonatomic,strong) UILabel *topicListPraiseLab;//点赞个数
@property (nonatomic,strong) UILabel *topicListCommentLab;//评论个数
@property (nonatomic,strong) NSMutableArray *photos; //图片数组
@end

@implementation SLTopicListTableViewCell
- (UIImageView *)topicListIconImg{
    if (!_topicListIconImg) {
        _topicListIconImg = [[UIImageView alloc] init];
        _topicListIconImg.layer.cornerRadius = 2.5;
        _topicListIconImg.layer.masksToBounds = YES;
        [self addSubview:_topicListIconImg];
        _topicListIconImg.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconTapclick)];
        [_topicListIconImg addGestureRecognizer:tap];
    }
    return _topicListIconImg;
}
- (UILabel *)topicListNicknameLab{
    if (!_topicListNicknameLab) {
        _topicListNicknameLab = [[UILabel alloc] init];
        _topicListNicknameLab.font = [UIFont boldSystemFontOfSize:15];
        _topicListNicknameLab.textAlignment = NSTextAlignmentLeft;
        _topicListNicknameLab.textColor = UIColorFromRGB(0x2b2124);
        [self addSubview:_topicListNicknameLab];
        
        _topicListNicknameLab.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconTapclick)];
        [_topicListNicknameLab addGestureRecognizer:tap];
    }
    return _topicListNicknameLab;
}
- (UILabel *)topicListTimeLab{
    if (!_topicListTimeLab) {
        _topicListTimeLab = [[UILabel alloc] init];
        _topicListTimeLab.textColor = UIColorFromRGB(0xb2b1b1);
        _topicListTimeLab.font = [UIFont systemFontOfSize:13];
        _topicListTimeLab.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_topicListTimeLab];
    }
    return _topicListTimeLab;
}
- (UIButton *)topicListDeleteBtn{
    if (!_topicListDeleteBtn) {
        _topicListDeleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _topicListDeleteBtn.frame = CGRectMake(ScreenW - 50, 0, 50, 50);
        [_topicListDeleteBtn addTarget:self action:@selector(deleteBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_topicListDeleteBtn];
    }
    return _topicListDeleteBtn;
}
- (UILabel *)topicListTitleLab{
    if (!_topicListTitleLab) {
        _topicListTitleLab = [[UILabel alloc] init];
        _topicListTitleLab.numberOfLines = 2;
        [self addSubview:_topicListTitleLab];
    }
    return _topicListTitleLab;
}
- (UILabel *)topicListContentLab{
    if (!_topicListContentLab) {
        _topicListContentLab = [[UILabel alloc] init];
        _topicListContentLab.numberOfLines = 8;
        [self addSubview:_topicListContentLab];
    }
    return _topicListContentLab;
}
- (UICollectionView *)topicListImgCollectionView{
    if (!_topicListImgCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(50, 50);
        _topicListImgCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _topicListImgCollectionView.dataSource = self;
        _topicListImgCollectionView.delegate = self;
        _topicListImgCollectionView.scrollEnabled = NO;
        _topicListImgCollectionView.showsVerticalScrollIndicator = NO;
        _topicListImgCollectionView.showsHorizontalScrollIndicator = NO;
        _topicListImgCollectionView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_topicListImgCollectionView];
        [_topicListImgCollectionView registerNib:[UINib nibWithNibName:@"SLTopicCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionViewCells"];
    }
    return _topicListImgCollectionView;
}
- (UIButton *)topicListPraiseBtn{
    if (!_topicListPraiseBtn) {
        _topicListPraiseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_topicListPraiseBtn setImage:[UIImage imageNamed:@"support_normal"] forState:UIControlStateNormal];
        [_topicListPraiseBtn setImage:[UIImage imageNamed:@"support_sel"] forState:UIControlStateSelected];
        _topicListPraiseBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _topicListPraiseBtn.titleLabel.textColor = UIColorFromRGB(0x888888);
        [_topicListPraiseBtn addTarget:self action:@selector(topicListPraiseBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_topicListPraiseBtn];
    }
    return _topicListPraiseBtn;
}
- (UIButton *)topicListCommentBtn{
    if (!_topicListCommentBtn) {
        _topicListCommentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_topicListCommentBtn setImage:[UIImage imageNamed:@"CommentImg"] forState:UIControlStateNormal];
        _topicListCommentBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _topicListCommentBtn.titleLabel.textColor = UIColorFromRGB(0x888888);
        [_topicListCommentBtn addTarget:self action:@selector(topicListCommentBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_topicListCommentBtn];
    }
    return _topicListCommentBtn;
}
- (UILabel *)topicListPraiseLab{
    if (!_topicListPraiseLab) {
        _topicListPraiseLab = [[UILabel alloc] init];
        _topicListPraiseLab.font = [UIFont systemFontOfSize:12];
        _topicListPraiseLab.textColor = UIColorFromRGB(0x888888);
        _topicListPraiseLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_topicListPraiseLab];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topicListPraiseBtnClick)];
        _topicListPraiseLab.userInteractionEnabled = YES;
        [_topicListPraiseLab addGestureRecognizer:tap];
    }
    return _topicListPraiseLab;
}
- (UILabel *)topicListCommentLab{
    if (!_topicListCommentLab) {
        _topicListCommentLab = [[UILabel alloc] init];
        _topicListCommentLab.font = [UIFont systemFontOfSize:12];
        _topicListCommentLab.textColor = UIColorFromRGB(0x888888);
        _topicListCommentLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_topicListCommentLab];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topicListCommentBtnClick)];
        _topicListCommentLab.userInteractionEnabled = YES;
        [_topicListCommentLab addGestureRecognizer:tap];
    }
    return _topicListCommentLab;
}
- (UIImageView *)topicListLineImgView{
    if (!_topicListLineImgView) {
        _topicListLineImgView = [[UIImageView alloc] init];
        _topicListLineImgView.backgroundColor = UIColorFromRGB(0xebddd5);
        [self addSubview:_topicListLineImgView];
    }
    return _topicListLineImgView;
}
- (NSMutableArray *)photos{
    if (!_photos) {
        _photos = [NSMutableArray new];
    }
    return _photos;
}
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor cyanColor];
    }
    return self;
}
-(void)setTopicModel:(SLTopicModel *)topicModel{
    _topicModel = topicModel;
    
    [self.topicListIconImg sd_setImageWithURL:[NSURL URLWithString:topicModel.user[@"avatar"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    [self.topicListIconImg mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset(20);
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(30);
    }];
    self.topicListNicknameLab.text = topicModel.user[@"nickname"];
    [self.topicListNicknameLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset(20);
        make.left.mas_equalTo(self.topicListIconImg.mas_right).offset(10);
        make.width.mas_equalTo(ScreenW -40-100);
        make.height.mas_equalTo(18);
    }];
    self.topicListTimeLab.text = topicModel.created;
    [self.topicListTimeLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.topicListIconImg.mas_bottom);
        make.left.mas_equalTo(self.topicListIconImg.mas_right).offset(10);
        make.width.mas_equalTo(ScreenW -40-100);
        make.height.mas_equalTo(13);
    }];
    if ([[NSString stringWithFormat:@"%@", topicModel.user[@"id"]] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        [self.topicListDeleteBtn setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        self.topicListDeleteBtn.hidden = NO;
        [self.topicListDeleteBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top);
            make.right.mas_equalTo(self.mas_right);
            make.width.mas_equalTo(50);
            make.height.mas_equalTo(50);
        }];
    }else{
        self.topicListDeleteBtn.hidden = YES;
    }
    [SLCalculateSpace setLabelSpace:self.topicListTitleLab withValue:topicModel.title withFont:[UIFont boldSystemFontOfSize:20] lineSpace:13 wordSpace:@0.0f];
    [self.topicListTitleLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topicListIconImg.mas_bottom).offset(5);
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.height.mas_equalTo(topicModel.titleHeight+5);
    }];
    [SLCalculateSpace setLabelSpace:self.topicListContentLab withValue:topicModel.digest withFont:[UIFont systemFontOfSize:15] lineSpace:13 wordSpace:@0.0f];
    [self.topicListContentLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topicListTitleLab.mas_bottom);
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.height.mas_equalTo(topicModel.contentHeight+5);
    }];
    //图片
    if (topicModel.imgs.count >0) {
        [self addSubview:self.topicListImgCollectionView];
        [self.topicListImgCollectionView reloadData];
        [self.topicListImgCollectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.topicListContentLab.mas_bottom).offset(15);
            make.left.mas_equalTo(self.mas_left).offset(20);
            make.right.mas_equalTo(self.mas_right).offset(-20);
            make.height.mas_equalTo(topicModel.imgsHeight);
        }];
        [self.topicListPraiseLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.topicListImgCollectionView.mas_bottom).offset(0);
            make.right.mas_equalTo(self.mas_right).offset(-20);
            make.width.mas_equalTo(40);
            make.height.mas_equalTo(50);
        }];
        [self.topicListPraiseBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.topicListImgCollectionView.mas_bottom).offset(0);
            make.right.mas_equalTo(self.topicListPraiseLab.mas_left);
            make.width.mas_equalTo(17);
            make.height.mas_equalTo(50);
        }];
        [self.topicListCommentLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.topicListImgCollectionView.mas_bottom).offset(0);
            make.right.mas_equalTo(self.topicListPraiseBtn.mas_left).offset(-20);
            make.width.mas_equalTo(40);
            make.height.mas_equalTo(50);
        }];
        [self.topicListCommentBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.topicListImgCollectionView.mas_bottom).offset(0);
            make.right.mas_equalTo(self.topicListCommentLab.mas_left);
            make.width.mas_equalTo(17);
            make.height.mas_equalTo(50);
        }];
    }else{
        [self.topicListImgCollectionView removeFromSuperview];
        [self.topicListPraiseLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.topicListContentLab.mas_bottom).offset(0);
            make.right.mas_equalTo(self.mas_right).offset(-20);
            make.width.mas_equalTo(40);
            make.height.mas_equalTo(50);
        }];
        [self.topicListPraiseBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.topicListContentLab.mas_bottom).offset(0);
            make.right.mas_equalTo(self.topicListPraiseLab.mas_left);
            make.width.mas_equalTo(17);
            make.height.mas_equalTo(50);
        }];
        [self.topicListCommentLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.topicListContentLab.mas_bottom).offset(0);
            make.right.mas_equalTo(self.topicListPraiseBtn.mas_left).offset(-20);
            make.width.mas_equalTo(40);
            make.height.mas_equalTo(50);
        }];
        [self.topicListCommentBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.topicListContentLab.mas_bottom).offset(0);
            make.right.mas_equalTo(self.topicListCommentLab.mas_left);
            make.width.mas_equalTo(17);
            make.height.mas_equalTo(50);
        }];
    }
    if ([self.topicModel.has_thumbsuped isEqualToString:@"1"]) {
        self.topicListPraiseBtn.selected = YES;
    }else{
        self.topicListPraiseBtn.selected = NO;
    }
    if (topicModel.thumbups_cnt) {
        if ([topicModel.thumbups_cnt integerValue] > 9999) {
            self.topicListPraiseLab.text = [NSString stringWithFormat:@"%ld万",(long)[topicModel.thumbups_cnt integerValue]/10000];
        }else{
            self.topicListPraiseLab.text = topicModel.thumbups_cnt;
        }
    }else{
        self.topicListPraiseLab.text = [NSString stringWithFormat:@"0"];
    }
    if (topicModel.replies_cnt) {
        if ([topicModel.replies_cnt integerValue] > 9999) {
            self.topicListCommentLab.text = [NSString stringWithFormat:@"%ld万",(long)[topicModel.thumbups_cnt integerValue]/10000];
        }else{
            self.topicListCommentLab.text = topicModel.replies_cnt;
        }
    }else{
        self.topicListCommentLab.text = [NSString stringWithFormat:@"0"];
    }
    [self.topicListLineImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.mas_bottom).offset(-1);
        make.left.mas_equalTo(self.mas_left);
        make.right.mas_equalTo(self.mas_right);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)iconTapclick{
    if ([self.delegate respondsToSelector:@selector(SLTopicListTableViewCell:didSelectNicknameWithModel:indexPath:)]) {
        [self.delegate SLTopicListTableViewCell:self didSelectNicknameWithModel:self.topicModel indexPath:self.indexpath];
    }
}
- (void)deleteBtnClick{
    if ([self.delegate respondsToSelector:@selector(SLTopicListTableViewCell:didSelectDelegateBtnWithModel:deleteButtonClicked:indexPath:)]) {
        [self.delegate SLTopicListTableViewCell:self didSelectDelegateBtnWithModel:self.topicModel deleteButtonClicked:self.topicListDeleteBtn indexPath:self.indexpath];
    }
}
- (void)topicListPraiseBtnClick{
    self.topicListPraiseBtn.selected = !self.topicListPraiseBtn.selected;
    if ([self.delegate respondsToSelector:@selector(SLTopicListTableViewCell:didSelectPraiseBtnWithModel:praiseButtonClicked:praiseLabClicked:indexPath:)]) {
        [self.delegate SLTopicListTableViewCell:self didSelectPraiseBtnWithModel:self.topicModel praiseButtonClicked:self.topicListPraiseBtn praiseLabClicked:self.topicListPraiseLab indexPath:self.indexpath];
    }
}
- (void)topicListCommentBtnClick{
    if ([self.delegate respondsToSelector:@selector(SLTopicListTableViewCell:didSelectCommentBtnWithModel:commentButtonClicked:commentLab:indexPath:)]) {
        [self.delegate SLTopicListTableViewCell:self didSelectCommentBtnWithModel:self.topicModel commentButtonClicked:self.topicListCommentBtn commentLab:self.topicListCommentLab indexPath:self.indexpath];
    }
}
#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.topicModel.imgs.count>9) {
        return 9;
    }
    return self.topicModel.imgs.count;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.topicModel.imgs.count == 1) {
        return CGSizeMake(ScreenW - 50, 1.5 * cellWH);
    }else if (self.topicModel.imgs.count == 2 || self.topicModel.imgs.count == 4){
        return CGSizeMake((ScreenW - 50-5)/2.0, 1.5 * cellWH);
    }
    return CGSizeMake(cellWH, cellWH);
}
//每个UICollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SLTopicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCells" forIndexPath:indexPath];
    if (self.topicModel.imgs.count > 0) {
        NSString *imageUrl = [self.topicModel.imgs[indexPath.row][@"url"] stringByAppendingString:@"@!topic"];
        [cell setupCellWithImageURl:imageUrl];
    }
    return cell;
}
//每个UICollectionView的点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.photos removeAllObjects];
    for (NSDictionary *dict in self.topicModel.imgs) {
        [self.photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:dict[@"url"]]]];
    }
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    [browser setCurrentPhotoIndex:indexPath.row];
    browser.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    if (self.topicModel.imgs.count > 0) {
        if (indexPath.row<self.topicModel.imgs.count) {
            if ([self.delegate respondsToSelector:@selector(SLTopicListTableViewCell:didSelectCollectionByMWPhotoBrowser:WithModel:)]) {
                [self.delegate SLTopicListTableViewCell:self didSelectCollectionByMWPhotoBrowser:browser WithModel:self.topicModel];
            }
        }else{
            if ([self.delegate respondsToSelector:@selector(SLTopicListTableViewCell:didSelectBlankPhotoWithModel:indexPath:)]) {
                [self.delegate SLTopicListTableViewCell:self didSelectBlankPhotoWithModel:self.topicModel indexPath:self.indexpath];
            }
        }
    }
    
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (self.photos.count>9) {
        return 9;
    }
    return _photos.count;
}
#pragma 代理
- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count)
        return [self.photos objectAtIndex:index];
    return nil;
}
//- (void)setFrame:(CGRect)frame{
//    frame.size.height -= 5;
//    [super setFrame:frame];
//}
@end
