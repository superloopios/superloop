//
//  SLDetailView.h
//  Superloop
//
//  Created by WangS on 16/7/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLIndexPathCollection.h"
#import "SLTopicModel.h"
#import "SLCopyLab.h"
@class SLDetailView;
@protocol SLDetailViewDelegate <NSObject>
@optional
- (void)SLDetailView:(SLDetailView *)detailView didUserIconClicked:(SLTopicModel *)model;
- (void)SLDetailView:(SLDetailView *)detailView didPraiseButtonClicked:(UIButton *)button indexPath:(SLTopicModel *)model;
@end
@interface SLDetailView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UIButton *praiseBtn;
@property (weak, nonatomic) IBOutlet UILabel *titlelab;
@property (weak, nonatomic) IBOutlet UIImageView *cutImgView;
@property (weak, nonatomic) IBOutlet SLCopyLab *contentLab;
@property (weak, nonatomic) IBOutlet SLIndexPathCollection *imgCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) id<SLDetailViewDelegate>delegate;
@property (assign, nonatomic) CGFloat cellHeight;
- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource,UICollectionViewDelegate>)dataSourceDelegate Model:(SLTopicModel *)model;
@end
