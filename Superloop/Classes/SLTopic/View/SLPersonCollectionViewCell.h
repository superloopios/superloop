//
//  SLPersonCollectionViewCell.h
//  Superloop
//
//  Created by administrator on 16/7/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

//@class SLFans;
@class SLSearchUser;
@interface SLPersonCollectionViewCell : UICollectionViewCell

//@property(nonatomic,strong)SLFans *fansModel;
@property(nonatomic,strong)SLSearchUser *userModel;

@end
