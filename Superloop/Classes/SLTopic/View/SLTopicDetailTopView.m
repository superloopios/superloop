//
//  SLTopicDetailTopView.m
//  Superloop
//
//  Created by WangS on 16/11/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTopicDetailTopView.h"
#import "SLTopicModel.h"

@interface SLTopicDetailTopView ()
@property (nonatomic,strong) UIImageView *iconImgView;
@property (nonatomic,strong) UILabel *nicknameLab;
@property (nonatomic,strong) UIButton *shareBtn;
@property (nonatomic,strong) UIImageView *lineImgView;

@end

@implementation SLTopicDetailTopView

- (UIImageView *)iconImgView{
    if (!_iconImgView) {
        _iconImgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 7, 30, 30)];
        _iconImgView.layer.masksToBounds = YES;
        _iconImgView.layer.cornerRadius = 5;
        _iconImgView.userInteractionEnabled = YES;
        [self addSubview:_iconImgView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconAndNicknameClick)];
        [_iconImgView addGestureRecognizer:tap];
    }
    return _iconImgView;
}
- (UILabel *)nicknameLab{
    if (!_nicknameLab) {
        _nicknameLab = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.iconImgView.frame)+10, 15, ScreenW -20-80-CGRectGetWidth(self.iconImgView.frame), 14)];
        _nicknameLab.font = [UIFont boldSystemFontOfSize:14];
        _nicknameLab.textColor = UIColorFromRGB(0x2b2124);
        _nicknameLab.userInteractionEnabled = YES;
        [self addSubview:_nicknameLab];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconAndNicknameClick)];
        [_nicknameLab addGestureRecognizer:tap];
    }
    return _nicknameLab;
}
- (UIImageView *)lineImgView{
    if (!_lineImgView) {
        _lineImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 43.5, ScreenW, 0.5)];
        [self addSubview:_lineImgView];
    }
    return _lineImgView;
}
- (UIButton *)shareBtn{
    if (!_shareBtn) {
        _shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _shareBtn.frame = CGRectMake(ScreenW -10-30, 12, 40, 20);
        [_shareBtn setImage:[UIImage imageNamed:@"topicDetailMore"] forState:UIControlStateNormal];
        [_shareBtn addTarget:self action:@selector(shareBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_shareBtn];
    }
    return _shareBtn;
}
- (void)shareBtnClick:(UIButton *)shareBtn{
    if ([self.delegate respondsToSelector:@selector(SLTopicDetailTopView:didSelectWithButton:)]) {
        [self.delegate SLTopicDetailTopView:self didSelectWithButton:shareBtn];
    }
}
- (void)iconAndNicknameClick{
    if ([self.delegate respondsToSelector:@selector(SLTopicDetailTopView:didSelectNicknameWithModel:)]) {
        [self.delegate SLTopicDetailTopView:self didSelectNicknameWithModel:self.topicModel];
    }
}
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
- (void)setTopicModel:(SLTopicModel *)topicModel{
    _topicModel = topicModel;
    
    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:topicModel.user[@"avatar"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    self.nicknameLab.text = topicModel.user[@"nickname"];
    [self.shareBtn setImage:[UIImage imageNamed:@"topicDetailMore"] forState:UIControlStateNormal];
    self.lineImgView.backgroundColor = UIColorFromRGB(0xebddd5);
}
@end
