//
//  SLTableViewCells.h
//  Superloop
//
//  Created by 朱宏伟 on 16/5/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLTopicModel.h"
#import "SLIndexPathCollection.h"
@class SLTableViewCells;

@protocol SLTableViewCellsDelegate <NSObject>

- (void)didUserIconClicked:(NSIndexPath *)indexpath;
- (void)didPraiseButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath;

@optional
-(void)getButton:(UIButton *)button;

@end

@interface SLTableViewCells : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *naLabel;
@property (weak, nonatomic) IBOutlet UILabel *prjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *tnLbl;

@property (weak, nonatomic) IBOutlet UIButton *supportBtn;
//内容
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
//图片vew
@property (weak, nonatomic) IBOutlet SLIndexPathCollection *collectionView;
//时间
@property (weak, nonatomic) IBOutlet UILabel *teLabel;

@property (nonatomic, weak) id<SLTableViewCellsDelegate>Delegate;

@property (nonatomic , strong) NSIndexPath *indexpath;

@property (weak, nonatomic) IBOutlet UIImageView *userIcon;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineViewContract;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthConstraint;



- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource,UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath*)indexPath andModel:(SLTopicModel *)model;


@end

