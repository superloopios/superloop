//
//  SLImageCell.h
//  Superloop
//
//  Created by WangJiWei on 16/3/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLImageCell;
@protocol SLImageCellDelegate <NSObject>
-(void)didCancelClick:(NSIndexPath *)indexPath;
@end
@interface SLImageCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, weak)id<SLImageCellDelegate>Delegate;
@property (nonatomic , strong) NSIndexPath *indexPath;
@property (nonatomic , strong) UIButton *btn;
@end
