//
//  SLTableViewCells.m
//  Superloop
//
//  Created by 朱宏伟 on 16/5/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTableViewCells.h"
#import <UIImageView+WebCache.h>
@interface SLTableViewCells()
{
    CGFloat spacing;   //行间距
}
@property (nonatomic,assign)CGFloat widthSuportCount;

@end

@implementation SLTableViewCells

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.lineViewContract setConstant:0.5];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.userIcon.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecommendView)];
    [self.userIcon addGestureRecognizer:tapView];
    self.userIcon.layer.cornerRadius = 2.5;
    self.userIcon.layer.masksToBounds = YES;
    self.tnLbl.font = [UIFont boldSystemFontOfSize:14];
    self.contentLabel.font = [UIFont boldSystemFontOfSize:14];
  
}

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource,UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath andModel:(SLTopicModel *)model
{
    //标题
    self.tnLbl.text = model.title;
    self.naLabel.text = model.user[@"nickname"];
    //内容
    NSString *chooseContent = model.content ? model.content : model.digest;
    NSString *digest = [chooseContent stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    self.contentLabel.text = digest;
    spacing = 5.0f;
    [self setLineSpacing:spacing label:self.contentLabel];   //修改内容的行间距
    //时间
    self.teLabel.text = model.created;
    if ([model.has_thumbsuped isEqualToString:@"0"]) {
        _supportBtn.selected = NO;
    }else if([model.has_thumbsuped isEqualToString:@"1"])
    {
        _supportBtn.selected = YES;
    }
    [_supportBtn sizeToFit];
    [_supportBtn setTitle:[NSString stringWithFormat:@"%@", model.thumbups_cnt] forState:UIControlStateNormal];
    //根据文字计算按钮的显示宽度
//    CGFloat widthSuportCount =[_supportBtn.titleLabel boundingRectWithString:_supportBtn.titleLabel.text withSize:CGSizeMake(MAXFLOAT, 0) withFont:13].width;
//    self.widthSuportCount = widthSuportCount;
//    [self.widthConstraint setConstant:self.widthSuportCount + 40];
    NSString *imageUrl = model.user[@"avatar"];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    if (model.imgs >0) {
        self.collectionView.dataSource = dataSourceDelegate;
        self.collectionView.delegate = dataSourceDelegate;
        self.collectionView.indexPath = indexPath;
        [self.collectionView registerNib:[UINib nibWithNibName:@"SLTopicCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionViewCells"];
        [self.collectionView reloadData];

    }
    
}

#pragma mark -- 改变UIlable的行距
- (void)setLineSpacing:(CGFloat)spacingSpace label:(UILabel *)label
{
    if (label.text.length>0) {
        NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:label.text];
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:spacingSpace];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [label.text length])];
        [label setAttributedText:attributedString];
        [label sizeToFit];
    }
}

- (void)setFrame:(CGRect)frame
{
    frame.size.height -= 5;
    [super setFrame:frame];
}

//点赞的按钮被点击
- (IBAction)praise:(UIButton *)sender {
    
//    //先将未到时间执行前的任务取消。
//    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(praiseBtnClick:) object:sender];
//    [self performSelector:@selector(praiseBtnClick:) withObject:sender afterDelay:0.2f];
    if ([_Delegate respondsToSelector:@selector(didPraiseButtonClicked:indexPath:)]) {
        [_Delegate didPraiseButtonClicked:sender indexPath:self.indexpath];
    }}
//- (void)praiseBtnClick:(id)sender{
//
//    if ([_Delegate respondsToSelector:@selector(didPraiseButtonClicked:indexPath:)]) {
//        [_Delegate didPraiseButtonClicked:sender indexPath:self.indexpath];
//    }
//}
- (void)tapRecommendView
{
    if ([_Delegate respondsToSelector:@selector(didUserIconClicked:)]) {
        [_Delegate didUserIconClicked:self.indexpath];
    }
}

@end
