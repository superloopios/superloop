//
//  SLPhotoView.h
//  Superloop
//
//  Created by xiaowu on 16/11/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLPhotoView;
@protocol SLPhotoViewDelegate <NSObject>

- (void)photoView:(SLPhotoView *)photoView DidClickRemoveButton:(UIButton *)removeBtn;


@end

@interface SLPhotoView : UIView


@property(nonatomic, strong)UIImageView *photoImageView;
@property(nonatomic, strong)UIButton *removeBtn;
@property (nonatomic, weak) id <SLPhotoViewDelegate>delegate;

@end
