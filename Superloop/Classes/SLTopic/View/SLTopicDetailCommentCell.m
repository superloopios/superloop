//
//  SLTopicDetailCommentCell.m
//  Superloop
//
//  Created by WangS on 16/11/17.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTopicDetailCommentCell.h"
#import "SLCommentModel.h"
#import "TTTAttributedLabel.h"
#import "AppDelegate.h"

@interface SLTopicDetailCommentCell ()<TTTAttributedLabelDelegate>
@property (nonatomic,strong) UIImageView *iconImgView;
@property (nonatomic,strong) UILabel *nicknameLab;
@property (nonatomic,strong) UILabel *timeLab;
@property (nonatomic,strong) UIButton *deleteBtn;
@property (nonatomic,strong) TTTAttributedLabel *contentLab;
@property (nonatomic,strong) UIImageView *lineImgView;
@end

@implementation SLTopicDetailCommentCell
- (UIImageView *)iconImgView{
    if (!_iconImgView) {
        _iconImgView = [[UIImageView alloc] init];
        _iconImgView.layer.cornerRadius = 2.5;
        _iconImgView.layer.masksToBounds = YES;
        [self addSubview:_iconImgView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconTapClick)];
        _iconImgView.userInteractionEnabled = YES;
        [_iconImgView addGestureRecognizer:tap];
        [_iconImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(15);
            make.left.mas_equalTo(self.mas_left).offset(20);
            make.width.mas_equalTo(30);
            make.height.mas_equalTo(30);
        }];
    }
    return _iconImgView;
}
- (UILabel *)nicknameLab{
    if (!_nicknameLab) {
        _nicknameLab = [[UILabel alloc] init];
        _nicknameLab.textAlignment = NSTextAlignmentLeft;
        _nicknameLab.font = [UIFont systemFontOfSize:14];
        _nicknameLab.textColor = [UIColor blackColor];
        [self addSubview:_nicknameLab];
        [_nicknameLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.iconImgView.mas_top);
            make.left.mas_equalTo(self.iconImgView.mas_right).offset(20);
            make.width.mas_equalTo(ScreenW-150);
            make.height.mas_equalTo(14);
        }];
    }
    return _nicknameLab;
}
- (UILabel *)timeLab{
    if (!_timeLab) {
        _timeLab = [[UILabel alloc] init];
        _timeLab.textAlignment = NSTextAlignmentLeft;
        _timeLab.font = [UIFont systemFontOfSize:12];
        _timeLab.textColor = UIColorFromRGB(0x888888);
        [self addSubview:_timeLab];
        [_timeLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.iconImgView.mas_bottom);
            make.left.mas_equalTo(self.iconImgView.mas_right).offset(20);
            make.width.mas_equalTo(ScreenW-150);
            make.height.mas_equalTo(12);
        }];
    }
    return _timeLab;
}
- (TTTAttributedLabel *)contentLab{
    if (!_contentLab) {
        _contentLab = [[TTTAttributedLabel alloc] initWithFrame:CGRectZero];
        _contentLab.numberOfLines = 0;
        [self addSubview:_contentLab];
    }
    return _contentLab;
}
- (UIButton *)deleteBtn{
    if (!_deleteBtn) {
        _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteBtn addTarget:self action:@selector(deleteBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_deleteBtn];
        [_deleteBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top);
            make.right.mas_equalTo(self.mas_right);
            make.width.mas_equalTo(50);
            make.height.mas_equalTo(50);
        }];
    }
    return _deleteBtn;
}
- (UIImageView *)lineImgView{
    if (!_lineImgView) {
        _lineImgView = [[UIImageView alloc] init];
        [self addSubview:_lineImgView];
        [_lineImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.mas_bottom).offset(-0.5);
            make.left.mas_equalTo(self.mas_left).offset(70);
            make.right.mas_equalTo(self.mas_right).offset(-10);
            make.height.mas_equalTo(0.5);
        }];
    }
    return _lineImgView;
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
- (void)setCommentModel:(SLCommentModel *)commentModel{
    _commentModel = commentModel;
    
   [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:commentModel.user[@"avatar"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];

    self.nicknameLab.text = commentModel.user[@"nickname"];
    self.timeLab.text = commentModel.created;
    
    NSString *text;
    if (commentModel.reply_to_user) {
        text = [NSString stringWithFormat:@"回复%@:%@",commentModel.reply_to_user[@"nickname"],commentModel.content];
        //计算高度
        [self.contentLab setLineBreakMode:NSLineBreakByTruncatingTail];
        self.contentLab.enabledTextCheckingTypes = NSTextCheckingTypeLink;
        __weak typeof(self) weakSelf = self;
        [self.contentLab setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            //设置可点击文字的范围
            NSRange boldRange = [[mutableAttributedString string] rangeOfString:commentModel.reply_to_user[@"nickname"] options:NSCaseInsensitiveSearch];
            //设定可点击文字的的大小
            UIFont *boldSystemFont = [UIFont systemFontOfSize:15];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                //设置可点击文本的大小
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:boldRange];
                //设置可点击文本的颜色
                [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[UIColorFromRGB(0xff5a5f) CGColor] range:boldRange];
                CFRelease(font);
            }
            [weakSelf lineAndWordSetting:text Attributes:mutableAttributedString];
            return mutableAttributedString;
        }];
        NSRange linkRange = [text rangeOfString:commentModel.reply_to_user[@"nickname"]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@""]];
        NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionary];
        [linkAttributes setValue:[NSNumber numberWithBool:NO] forKey:(NSString *)kCTUnderlineStyleAttributeName];
        self.contentLab.linkAttributes = linkAttributes;
        //设置链接的url
        [self.contentLab addLinkToURL:url withRange:linkRange];
    }else{
        __weak typeof(self) weakSelf = self;
        [self.contentLab setText:commentModel.content afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
            [weakSelf lineAndWordSetting:commentModel.content Attributes:mutableAttributedString];
            return mutableAttributedString;
        }];
    }
    self.contentLab.delegate = self;
    [self.contentLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.iconImgView.mas_bottom);
        make.left.mas_equalTo(self.mas_left).offset(70);
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.height.mas_equalTo(commentModel.contentHeight);
    }];
    if ([[NSString stringWithFormat:@"%@", commentModel.user_id] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        self.deleteBtn.hidden = NO;
        [self.deleteBtn setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
    }else{
        self.deleteBtn.hidden = YES;
    }
    self.lineImgView.backgroundColor = UIColorFromRGB(0xa8d5b9);
}
- (void)iconTapClick{
    if ([self.delegate respondsToSelector:@selector(SLTopicDetailCommentCell:didSelectIconIndexPath:commentModel:)]) {
        [self.delegate SLTopicDetailCommentCell:self didSelectIconIndexPath:self.indexPath commentModel:self.commentModel];
    }
}
- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url{
    if ([self.delegate respondsToSelector:@selector(SLTopicDetailCommentCell:attributedLabel:commentModel:)]) {
        [self.delegate SLTopicDetailCommentCell:self attributedLabel:self.indexPath commentModel:self.commentModel];
    }
}
- (void)deleteBtnClick{
    if ([self.delegate respondsToSelector:@selector(SLTopicDetailCommentCell:didSelectDeleteBtn:indexPath:commentModel:)]) {
        [self.delegate SLTopicDetailCommentCell:self didSelectDeleteBtn:self.deleteBtn indexPath:self.indexPath commentModel:self.commentModel];
    }
}
- (void)lineAndWordSetting:(NSString *)content Attributes:(NSMutableAttributedString *)mutableAttributedString{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = 13; //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 3.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 3;
    paraStyle.tailIndent = 0;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:15], NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@0.0f
                          };
    NSRange contentRange = NSMakeRange(0, content.length);
    [mutableAttributedString addAttributes:dic range:contentRange];
}
@end
