//
//  SLSawBtn.h
//  Superloop
//
//  Created by 朱宏伟 on 16/6/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLSawBtn : UIButton

@property(nonatomic, strong)UIColor *textColor;

@end
