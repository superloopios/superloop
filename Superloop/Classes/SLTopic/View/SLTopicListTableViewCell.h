//
//  SLTopicListTableViewCell.h
//  Superloop
//
//  Created by WangS on 16/11/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLTopicModel;
@class SLTopicListTableViewCell;
@class MWPhotoBrowser;
@protocol SLTopicListTableViewCellDelegate <NSObject>
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectCollectionByMWPhotoBrowser:(MWPhotoBrowser *)browser WithModel:(SLTopicModel *)topicModel;//选择图片
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectNicknameWithModel:(SLTopicModel *)topicModel indexPath:(NSIndexPath *)indexpath;//点图像昵称跳转
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectDelegateBtnWithModel:(SLTopicModel *)topicModel deleteButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath;//点删除按钮
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectPraiseBtnWithModel:(SLTopicModel *)topicModel praiseButtonClicked:(UIButton *)button praiseLabClicked:(UILabel *)praiseLab indexPath:(NSIndexPath *)indexpath;//点点赞按钮
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectCommentBtnWithModel:(SLTopicModel *)topicModel commentButtonClicked:(UIButton *)button  commentLab:(UILabel *)commentLab indexPath:(NSIndexPath *)indexpath;//点评论按钮
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectBlankPhotoWithModel:(SLTopicModel *)topicModel indexPath:(NSIndexPath *)indexpath;//图片空白处
@end


@interface SLTopicListTableViewCell : UITableViewCell
@property (nonatomic,weak) id<SLTopicListTableViewCellDelegate>delegate;
@property (nonatomic,strong) SLTopicModel *topicModel;
@property (nonatomic,strong) NSIndexPath *indexpath;
@end
