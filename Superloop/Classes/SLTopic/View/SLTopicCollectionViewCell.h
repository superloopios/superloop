//
//  SLTopicCollectionViewCell.h
//  Superloop
//
//  Created by WangJiWei on 16/3/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLTopicCollectionViewCell : UICollectionViewCell
- (void)setupCellWithImageURl:(NSString *)url;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
