//
//  SLTopicCollectionViewCell.m
//  Superloop
//
//  Created by WangJiWei on 16/3/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import <UIImageView+WebCache.h>
#import "SLTopicCollectionViewCell.h"
@interface SLTopicCollectionViewCell()

@end
@implementation SLTopicCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}
- (void)setupCellWithImageURl:(NSString *)url{  // 这里没有用缓存
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    if (url) {
       [self.imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    }else{
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:url]];
    }
}

@end
