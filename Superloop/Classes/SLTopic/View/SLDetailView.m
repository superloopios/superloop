//
//  SLDetailView.m
//  Superloop
//
//  Created by WangS on 16/7/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLDetailView.h"
#import <UIImageView+WebCache.h>
#define  cellWH  ((ScreenW - 50 - (cols - 1) * margin) / cols)
@interface SLDetailView ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cutImgViewContraint;
@property (assign, nonatomic) CGFloat space; //行间距
@property (strong, nonatomic) SLTopicModel *model;
@end
@implementation SLDetailView
static NSInteger const cols = 3;
static CGFloat const margin = 5;
-(void)layoutSubviews{
    [super layoutSubviews];
    [self.cutImgViewContraint setConstant:0.5];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    self.iconImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecommendView)];
    [self.iconImg addGestureRecognizer:tapView];
    self.iconImg.layer.cornerRadius = 2.5;
    self.iconImg.layer.masksToBounds = YES;
    self.titlelab.font = [UIFont boldSystemFontOfSize:14];
    self.contentLab.font = [UIFont boldSystemFontOfSize:14];
    self.praiseBtn.layer.cornerRadius = 5;
    self.praiseBtn.layer.masksToBounds = YES;
    self.praiseBtn.layer.borderWidth = 0.5;
    self.praiseBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
}
- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource,UICollectionViewDelegate>)dataSourceDelegate Model:(SLTopicModel *)model{
    self.model=model;
    //标题
    self.titlelab.text = model.title;
    self.nameLab.text = model.user[@"nickname"];
    //内容
    NSString *chooseContent = model.content ? model.content : model.digest;
    self.contentLab.text = chooseContent;
    
    self.space = 5.0f;
    [self setLineSpacing:self.space label:self.contentLab];   //修改内容的行间距
    //时间
    self.timeLab.text = model.created;
    if ([model.has_thumbsuped isEqualToString:@"0"]) {
        _praiseBtn.selected = NO;
    }else if([model.has_thumbsuped isEqualToString:@"1"]){
        _praiseBtn.selected = YES;
    }
    [_praiseBtn setTitle:[NSString stringWithFormat:@"%@", model.thumbups_cnt] forState:UIControlStateNormal];
    NSString *imageUrl = model.user[@"avatar"];
    [self.iconImg sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    if (model.imgs >0) {
        self.imgCollectionView.dataSource = dataSourceDelegate;
        self.imgCollectionView.delegate = dataSourceDelegate;
        [self.imgCollectionView registerNib:[UINib nibWithNibName:@"SLTopicCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionViewCells"];
        [self.imgCollectionView reloadData];
    }
    [self modelHeight:model];
    
}
-(void)modelHeight:(SLTopicModel *)model{
    
    _model = model;
    
    //计算行高
    //头像高度
    self.cellHeight = 53;
    //图像距标题的高度
    self.cellHeight+=8;
    //渠道每行的高度
    CGFloat textMaxW = ScreenW - 50;
    NSString *titleContentextString = @"高度，高度";
    CGFloat titleFontSize = [titleContentextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} context:nil].size.height;
    //标题高度
    NSString *chooseContent = model.content ? model.content : model.digest;
    NSString *digest = [chooseContent stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    digest = [chooseContent stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    CGFloat titleHeight = [digest boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} context:nil].size.height;
   
    NSInteger titleLineNum = titleHeight / titleFontSize ;
   
    if (titleLineNum > 2) {
        titleLineNum = 2;
    }
    if (titleLineNum == 1) {
        self.cellHeight += 17;
    }
    self.cellHeight += titleLineNum * titleFontSize+(titleLineNum - 1) * 5;
    //分割线
    self.cellHeight+=15;
    //话题内容
    NSString *content = model.content ? model.content : model.digest;
    CGFloat digestHeight= [content boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} context:nil].size.height;
    NSInteger digestLineNum = digestHeight / titleFontSize;
    self.cellHeight+=digestLineNum * titleFontSize+(digestLineNum - 1) * 5;
    //话题内容距图片
    self.cellHeight+=4;
    //图片高度
    if (model.imgs.count > 0) {
        NSInteger rows=(model.imgs.count - 1) / 3 + 1;
        if (model.imgs.count == 1 || model.imgs.count==2) {
            self.cellHeight += 1.5 * cellWH + rows*5;
        }else if (model.imgs.count == 4) {
            self.cellHeight += 3 * cellWH + rows*5;
        }else if (model.imgs.count > 9) {
            self.cellHeight += 3 * cellWH + 2*5;
        }
        else{
            self.cellHeight += rows * cellWH + rows*5;
        }
        
    }
    //时间
    self.cellHeight += 36;
}


#pragma mark -- 改变UIlable的行距
- (void)setLineSpacing:(CGFloat)spacing label:(UILabel *)label{
    if (label.text.length>0) {
        NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:label.text];
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:spacing];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [label.text length])];
        [label setAttributedText:attributedString];
        [label sizeToFit];
    }
}
- (IBAction)didPraiseBtnClick:(id)sender {
    //先将未到时间执行前的任务取消
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(praiseBtnClick:) object:sender];
    [self performSelector:@selector(praiseBtnClick:) withObject:sender afterDelay:0.2f];
}
- (void)praiseBtnClick:(id)sender{
    if ([self.delegate respondsToSelector:@selector(SLDetailView:didPraiseButtonClicked:indexPath:)]) {
        [self.delegate SLDetailView:self didPraiseButtonClicked:sender indexPath:self.model];
    }
}
- (void)tapRecommendView{
    if ([self.delegate respondsToSelector:@selector(SLDetailView:didUserIconClicked:)]) {
        [self.delegate SLDetailView:self didUserIconClicked:self.model];
    }
}
//- (void)setFrame:(CGRect)frame{
//    frame.size.height -= 5;
//    [super setFrame:frame];
//}

@end
