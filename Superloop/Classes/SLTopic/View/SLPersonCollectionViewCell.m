//
//  SLPersonCollectionViewCell.m
//  Superloop
//
//  Created by administrator on 16/7/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLPersonCollectionViewCell.h"
#import <UIImageView+WebCache.h>
#import "SLSearchUser.h"
//#import "SLFans.h"
#define M_PI  3.14159265358979323846264338327950288
@interface SLPersonCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *intruductionLab;
@property (weak, nonatomic) IBOutlet UIImageView *tiltCrownImgView;
@property (weak, nonatomic) IBOutlet UIImageView *identificationImgView;

@end

@implementation SLPersonCollectionViewCell

//-(void)setFansModel:(SLFans *)fansModel
//{
//    _fansModel=fansModel;
//    
//    _iconImg.layer.cornerRadius = 35;
//    _iconImg.layer.masksToBounds = YES;
////    _iconImg.clipsToBounds = YES;
//    [_iconImg sd_setImageWithURL:[NSURL URLWithString:fansModel.avatar] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
//    _nameLab.text=fansModel.nickname;
//    _intruductionLab.text=fansModel.bio;
//    
//}
-(void)setUserModel:(SLSearchUser *)userModel{
    _userModel=userModel;
    
    _iconImg.layer.cornerRadius = 35;
    _iconImg.layer.masksToBounds = YES;
    //    _iconImg.clipsToBounds = YES;
    [_iconImg sd_setImageWithURL:[NSURL URLWithString:userModel.avatar] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    _nameLab.text=userModel.nickname;
    _intruductionLab.text=userModel.bio;
    
    //_tiltCrownImgView.transform=CGAffineTransformMakeRotation(-M_PI/4);
    
    if ([userModel.verification_status isEqualToString:@"2"]) {
        _identificationImgView.hidden=NO;
    }else{
        _identificationImgView.hidden=YES;
    }
    if ([userModel.role_type isEqualToString:@"1"]) {
        _tiltCrownImgView.hidden=NO;
    }else{
        _tiltCrownImgView.hidden=YES;
    }
    
}



@end
