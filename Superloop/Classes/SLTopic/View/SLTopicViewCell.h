//
//  SLTopicViewCell.h
//  Superloop
//
//  Created by WangJiWei on 16/3/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLTopicModel.h"
#import "SLIndexPathCollection.h"
#import "SLCommentBtn.h"
#import "SLSupportBtn.h"
@class SLTopicViewCell;
@protocol SLTopicViewCellDelegate <NSObject>

@optional
- (void)didUserIconClicked:(NSIndexPath *)indexpath;
- (void)didPraiseButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath;
- (void)didCommentButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath;
//点击删除按钮的代理事件
- (void)deleteTopicsButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath;
@end


@interface SLTopicViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet SLLabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *supportBtn;
@property (weak, nonatomic) IBOutlet UIButton *sawBtn;
@property (weak, nonatomic) IBOutlet SLLabel *contentLabel;
@property (weak, nonatomic) IBOutlet SLIndexPathCollection *collectionView;
@property (nonatomic, weak) id<SLTopicViewCellDelegate>Delegate;
@property (nonatomic , strong) NSIndexPath *indexpath;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstarins;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageConstraintWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *suportConstrainWidth;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;  //删除话题按钮

@property (weak, nonatomic) SLSupportBtn *supportBtns;
@property (weak, nonatomic) SLCommentBtn *sawBtns;


- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource,UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath*)indexPath andModel:(SLTopicModel *)model;

@end
