//
//  SLTopicFootView.m
//  Superloop
//
//  Created by WangJiWei on 16/3/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTopicFootView.h"
@interface SLTopicFootView()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topImgViewConstraint;

@end
@implementation SLTopicFootView
-(void)layoutSubviews{
    [super layoutSubviews];
    [self.topImgViewConstraint setConstant:0.5];
    
}
- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (IBAction)collectClick:(id)sender {
    
    if ([_Delegate respondsToSelector:@selector(didCollectButtonClicked:)]) {
        [_Delegate didCollectButtonClicked:sender];
    }
}
//- (IBAction)awardClick:(id)sender {
//    if ([_Delegate respondsToSelector:@selector(didAwardButtonClicked)]) {
//        [_Delegate didAwardButtonClicked];
//    }
//}
- (IBAction)commendClick:(id)sender {
    if ([_Delegate respondsToSelector:@selector(didCommendClicked)]) {
        [_Delegate didCommendClicked];
    }
}
- (IBAction)contactMeClick:(id)sender {
    if ([_Delegate respondsToSelector:@selector(didContactMeButtonClicked)]) {
        [_Delegate didContactMeButtonClicked];
    }
}

@end
