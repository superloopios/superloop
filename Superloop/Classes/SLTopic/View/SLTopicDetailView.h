//
//  SLTopicDetailView.h
//  Superloop
//
//  Created by WangS on 16/11/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLTopicDetailView;
@class SLTopicModel;
@class MWPhotoBrowser;

@protocol SLTopicDetailViewDelegate <NSObject>
- (void)SLTopicDetailView:(SLTopicDetailView *)topicDetailView didSelectCollectionByMWPhotoBrowser:(MWPhotoBrowser *)browser WithModel:(SLTopicModel *)topicModel;//选择图片
@end

@interface SLTopicDetailView : UIView
@property (nonatomic,weak)   id<SLTopicDetailViewDelegate>delegate;
@property (nonatomic,strong) SLTopicModel *topicModel;
@property (nonatomic,assign) CGFloat topicHeight;
@end
