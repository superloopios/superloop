//
//  SLPhotoView.m
//  Superloop
//
//  Created by xiaowu on 16/11/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLPhotoView.h"


#define imageWH ((screen_W - 30) / 3 - 7)
#define selfWH  (screen_W - 30) / 3

@interface SLPhotoView ()


@end

@implementation SLPhotoView

- (UIButton *)removeBtn{
    if (!_removeBtn) {
        _removeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_removeBtn setImage:[UIImage imageNamed:@"deletePhoto"] forState:UIControlStateNormal];
        [_removeBtn addTarget:self action:@selector(remove) forControlEvents:UIControlEventTouchUpInside];
        _removeBtn.frame = CGRectMake(selfWH - 21.5, 0, 21.5, 21.5);
        [self addSubview:_removeBtn];
    }
    return _removeBtn;
}
- (void)remove{
    if ([self.delegate respondsToSelector:@selector(photoView:DidClickRemoveButton:)]) {
        [self.delegate photoView:self DidClickRemoveButton:self.removeBtn];
    }
}

- (UIImageView *)photoImageView{
    if (!_photoImageView) {
        _photoImageView = [[UIImageView alloc] init];
        _photoImageView.frame = CGRectMake(0, 7, imageWH, imageWH);
        _photoImageView.backgroundColor = [UIColor lightGrayColor];
        _photoImageView.contentMode = UIViewContentModeScaleAspectFill;
        _photoImageView.clipsToBounds = YES;
        [self addSubview:_photoImageView];
    }
    return _photoImageView;
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.photoImageView];
        [self addSubview:self.removeBtn];
    }
    return self;
}


@end
