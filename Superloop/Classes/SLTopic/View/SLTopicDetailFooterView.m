//
//  SLTopicDetailFooterView.m
//  Superloop
//
//  Created by WangS on 16/11/4.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTopicDetailFooterView.h"
#import "SLTopicModel.h"

@interface SLTopicDetailFooterView ()

@end

@implementation SLTopicDetailFooterView

- (UIButton *)collectionBtn{
    if (!_collectionBtn) {
        _collectionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_collectionBtn addTarget:self action:@selector(collectionBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_collectionBtn];
               
        [_collectionBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(10);
            make.right.mas_equalTo(self.praiseBtn.mas_left).offset(-35);
            make.width.mas_equalTo(30);
            make.height.mas_equalTo(30);
        }];
    }
    return _collectionBtn;
}
- (UIButton *)praiseBtn{
    if (!_praiseBtn) {
        _praiseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_praiseBtn addTarget:self action:@selector(praiseBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_praiseBtn];
        [_praiseBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(14);
            make.right.mas_equalTo(self.praiseLab.mas_left);
            make.width.mas_equalTo(22);
            make.height.mas_equalTo(22);
        }];
    }
    return _praiseBtn;
}
- (UILabel *)praiseLab{
    if (!_praiseLab) {
        _praiseLab = [[UILabel alloc] init];
        _praiseLab.font = [UIFont systemFontOfSize:12];
        _praiseLab.textColor = UIColorFromRGB(0x888888);
        _praiseLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_praiseLab];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(praiseBtnClick)];
        _praiseLab.userInteractionEnabled = YES;
        [_praiseLab addGestureRecognizer:tap];
        [_praiseLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(10);
            make.right.mas_equalTo(self.commentBtn.mas_left);
            make.width.mas_equalTo(40);
            make.height.mas_equalTo(30);
        }];
    }
    return _praiseLab;
}
- (UIButton *)commentBtn{
    if (!_commentBtn) {
        _commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_commentBtn addTarget:self action:@selector(commentBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_commentBtn];
        [_commentBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(14);
            make.right.mas_equalTo(self.commentLab.mas_left);
            make.width.mas_equalTo(22);
            make.height.mas_equalTo(22);
        }];
    }
    return _commentBtn;
}
- (UILabel *)commentLab{
    if (!_commentLab) {
        _commentLab = [[UILabel alloc] init];
        _commentLab.font = [UIFont systemFontOfSize:12];
        _commentLab.textColor = UIColorFromRGB(0x888888);
        _commentLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_commentLab];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(commentBtnClick)];
        _commentLab.userInteractionEnabled = YES;
        [_commentLab addGestureRecognizer:tap];
        [_commentLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(10);
            make.right.mas_equalTo(self.callBtn.mas_left);
            make.width.mas_equalTo(40);
            make.height.mas_equalTo(30);
        }];
    }
    return _commentLab;
}
- (UIButton *)callBtn{
    if (!_callBtn) {
        _callBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_callBtn addTarget:self action:@selector(callBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_callBtn];
        [_callBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(10);
            make.right.mas_equalTo(self.mas_right).offset(-20);
            make.width.mas_equalTo(30);
            make.height.mas_equalTo(30);
        }];
    }
    return _callBtn;
}
- (UIButton *)backBtn{
    if (!_backBtn) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn addTarget:self action:@selector(backBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_backBtn];
        [_backBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(10);
            make.left.mas_equalTo(self.mas_left).offset(0);
            make.width.mas_equalTo(50);
            make.height.mas_equalTo(30);
        }];
    }
    return _backBtn;
}
- (UIImageView *)lineImgView{
    if (!_lineImgView) {
        _lineImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 0.5)];
        [self addSubview:_lineImgView];
    }
    return _lineImgView;
}
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self.backBtn setImage:[UIImage imageNamed:@"topicDetailRedBack"] forState:UIControlStateNormal];
        self.lineImgView.backgroundColor = UIColorFromRGB(0xebddd5);
        [self.callBtn setImage:[UIImage imageNamed:@"topicDetailCall"] forState:UIControlStateNormal];
        [self.commentBtn setImage:[UIImage imageNamed:@"topicDetailComment"] forState:UIControlStateNormal];
        [self.praiseBtn setImage:[UIImage imageNamed:@"topicDetailPraise_nor"] forState:UIControlStateNormal];
        [self.praiseBtn setImage:[UIImage imageNamed:@"topicDetailPraise_sel"] forState:UIControlStateSelected];
        [self.collectionBtn setImage:[UIImage imageNamed:@"topicDetailCollection_nor"] forState:UIControlStateNormal];
        [self.collectionBtn setImage:[UIImage imageNamed:@"topicDetailCollection_sel"] forState:UIControlStateSelected];
    }
    return self;
}
- (void)commentBtnClick{
    if ([self.delegate respondsToSelector:@selector(SLTopicDetailFooterView:didSelectWithCommentButton:label:)]) {
        [self.delegate SLTopicDetailFooterView:self didSelectWithCommentButton:self.commentBtn label:self.commentLab];
    }
}
- (void)callBtnClick:(UIButton *)callBtn{
    if ([self.delegate respondsToSelector:@selector(SLTopicDetailFooterView:didSelectWithCallButton:)]) {
        [self.delegate SLTopicDetailFooterView:self didSelectWithCallButton:callBtn];
    }
}
- (void)praiseBtnClick{
    if ([self.delegate respondsToSelector:@selector(SLTopicDetailFooterView:didSelectWithPraiseButton:label:topicModel:)]) {
        [self.delegate SLTopicDetailFooterView:self didSelectWithPraiseButton:self.praiseBtn label:self.praiseLab topicModel:self.topicModel];
    }
}

- (void)collectionBtnClick:(UIButton *)collectionBtn{
    if ([self.delegate respondsToSelector:@selector(SLTopicDetailFooterView:didSelectWithCollectionButton:)]) {
        [self.delegate SLTopicDetailFooterView:self didSelectWithCollectionButton:collectionBtn];
    }
}
- (void)backBtnClick:(UIButton *)backBtn{
    if ([self.delegate respondsToSelector:@selector(SLTopicDetailFooterView:didSelectWithBackButton:)]) {
        [self.delegate SLTopicDetailFooterView:self didSelectWithBackButton:backBtn];
    }
}
@end
