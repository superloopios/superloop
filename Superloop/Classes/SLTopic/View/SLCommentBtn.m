//
//  SLCommentBtn.m
//  Superloop
//
//  Created by 朱宏伟 on 16/6/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCommentBtn.h"

@implementation SLCommentBtn

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.font = [UIFont systemFontOfSize:13];
        [self setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    self.imageView.x = 0;
    
}

@end
