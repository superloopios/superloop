//
//  SLImageCell.m
//  Superloop
//
//  Created by WangJiWei on 16/3/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLImageCell.h"

@implementation SLImageCell
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _imageView = [[UIImageView alloc] init];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_imageView];
        self.clipsToBounds = YES;
        UIButton *btn = [[UIButton alloc] init];
        [btn setBackgroundImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        btn.imageView.contentMode = UIViewContentModeScaleToFill;
        btn.frame = CGRectMake(self.width * 0.75, 0, self.width * 0.25, self.width * 0.25);
        [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
        self.btn = btn;
        [self addSubview:btn];
    }
    return self;
}
- (void)btnClick
{
    if ([_Delegate respondsToSelector:@selector(didCancelClick:)]) {
        [_Delegate didCancelClick:_indexPath];
    }
}
- (void)layoutSubviews {
    [super layoutSubviews];
    _imageView.frame = self.bounds;
}

@end
