//
//  SLSupportBtn.h
//  Superloop
//
//  Created by 朱宏伟 on 16/6/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLSupportBtn : UIButton

@property(nonatomic,strong)UIImageView *imgView;
@property(nonatomic,strong)UILabel *titleLab;
@property(nonatomic,strong)NSString *imgStr;
@property(nonatomic,strong)NSString *titleLabStr;


@end
