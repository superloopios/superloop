//
//  SLTopicDetailTopView.h
//  Superloop
//
//  Created by WangS on 16/11/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLTopicModel;
@class SLTopicDetailTopView;
@protocol SLTopicDetailTopViewDelegate <NSObject>
- (void)SLTopicDetailTopView:(SLTopicDetailTopView *)topicDetailTopView didSelectNicknameWithModel:(SLTopicModel *)topicModel;//点图像昵称跳转
- (void)SLTopicDetailTopView:(SLTopicDetailTopView *)topicDetailTopView didSelectWithButton:(UIButton *)shareBtn;//分享
@end
@interface SLTopicDetailTopView : UIView
@property (nonatomic,weak) id<SLTopicDetailTopViewDelegate>delegate;
@property (nonatomic,strong) SLTopicModel *topicModel;
@end
