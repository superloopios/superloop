//
//  SLTopicFootView.h
//  Superloop
//
//  Created by WangJiWei on 16/3/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLTopicFootView;
@protocol SLTopicFootViewDelegate <NSObject>

- (void)didCollectButtonClicked:(UIButton *)collectionButton;
//- (void)didAwardButtonClicked;
- (void)didCommendClicked;
- (void)didContactMeButtonClicked;

@end
@interface SLTopicFootView : UIView
@property (weak, nonatomic) IBOutlet UIButton *commentCountButton;

@property (weak, nonatomic) IBOutlet UIButton *collecitonBtn;

@property (weak, nonatomic) IBOutlet UIButton *contantMeBtn;

@property (nonatomic, weak) id <SLTopicFootViewDelegate>Delegate;
@end
