//
//  SLTopicDetailFooterView.h
//  Superloop
//
//  Created by WangS on 16/11/4.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLTopicModel;
@class SLTopicDetailFooterView;
@class SLTopicModel;
@protocol SLTopicDetailFooterViewDelegate <NSObject>
- (void)SLTopicDetailFooterView:(SLTopicDetailFooterView *)topicDetailFooterView didSelectWithBackButton:(UIButton *)selectBtn;//返回
- (void)SLTopicDetailFooterView:(SLTopicDetailFooterView *)topicDetailFooterView didSelectWithCollectionButton:(UIButton *)selectBtn;//收藏
- (void)SLTopicDetailFooterView:(SLTopicDetailFooterView *)topicDetailFooterView didSelectWithCallButton:(UIButton *)selectBtn;//打电话
- (void)SLTopicDetailFooterView:(SLTopicDetailFooterView *)topicDetailFooterView didSelectWithPraiseButton:(UIButton *)selectBtn label:(UILabel *)selectLab topicModel:(SLTopicModel *)topicModel;//点赞
- (void)SLTopicDetailFooterView:(SLTopicDetailFooterView *)topicDetailFooterView didSelectWithCommentButton:(UIButton *)selectBtn label:(UILabel *)selectLab;//评论
@end
@interface SLTopicDetailFooterView : UIView
@property (nonatomic,weak)   id<SLTopicDetailFooterViewDelegate>delegate;
@property (nonatomic,strong) UIButton *collectionBtn;//收藏
@property (nonatomic,strong) UIButton *praiseBtn;//点赞
@property (nonatomic,strong) UIButton *commentBtn;//评论
@property (nonatomic,strong) UIButton *callBtn;//打电话
@property (nonatomic,strong) UIButton *backBtn;//返回
@property (nonatomic,strong) UIImageView *lineImgView;
@property (nonatomic,strong) UILabel * commentLab;//显示评论个数
@property (nonatomic,strong) UILabel * praiseLab;//显示点赞个数
@property (nonatomic,strong) SLTopicModel * topicModel;

@end
