//
//  SLTopicViewCell.m
//  Superloop
//
//  Created by WangJiWei on 16/3/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTopicViewCell.h"
#import <UIImageView+WebCache.h>
#import "SLValueUtils.h"
#import "UILabel+StringFrame.h"
#import "SLAPIHelper.h"
#import "AppDelegate.h"
#import "SLCalculateSpace.h"

@interface SLTopicViewCell()
{
     CGFloat spacing;   //行间距
}
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContract;
@property (weak, nonatomic) IBOutlet SLLabel *userName;
@property (weak, nonatomic) IBOutlet SLLabel *creatTime;
@property (weak, nonatomic) IBOutlet UIImageView *userIcon;

@property (nonatomic,assign)CGFloat sawWidth;
@property (nonatomic,assign)CGFloat supportBtnWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cutImgViewConstraint;

@end
@implementation SLTopicViewCell
-(void)layoutSubviews{
    [super layoutSubviews];
    //[self.heightContract setConstant:0.5];
    //[self.heightConstarins setConstant:0.5];
//    [self.suportConstrainWidth setConstant:_supportBtnWidth + 30];
//    [self.messageConstraintWidth setConstant:_sawWidth + 30];
    [self.cutImgViewConstraint setConstant:0.5];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    self.userIcon.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecommendView)];
    [self.userIcon addGestureRecognizer:tapView];
    self.userIcon.layer.cornerRadius = 2.5;
    self.userIcon.layer.masksToBounds = YES;
//    self.titleLabel.font = [UIFont boldSystemFontOfSize:20];
//    self.contentLabel.font = [UIFont systemFontOfSize:15];
}

#pragma mark -- 改变UIlable的行距
//- (void)setLineSpacing:(CGFloat)spacsing label:(UILabel *)label{
//    if (label.text.length>0) {
//        NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:label.text];
//        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//        [paragraphStyle setLineSpacing:spacsing];
//        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [label.text length]-1)];
//        [label setAttributedText:attributedString];
//        label.lineBreakMode = NSLineBreakByTruncatingTail;
//        [label sizeToFit];
//    }
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource,UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath andModel:(SLTopicModel *)model{
    
    //self.titleLabel.text = model.title;
    self.userName.text = model.user[@"nickname"];
    NSString *digest = [model.digest stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    digest = [model.digest stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    //self.contentLabel.text = digest;
    self.creatTime.text = model.created;
    if ([model.has_thumbsuped isEqualToString:@"0"]) {
        _supportBtn.selected = NO;
    }else{
        _supportBtn.selected = YES;
    }
    [SLCalculateSpace setLabelSpace:self.titleLabel withValue:model.title withFont:[UIFont boldSystemFontOfSize:20] lineSpace:13 wordSpace:@0.0f];
    [SLCalculateSpace setLabelSpace:self.contentLabel withValue:digest withFont:[UIFont systemFontOfSize:15] lineSpace:13 wordSpace:@0.0f];
//    spacing = 13.0f;
//    [self setLineSpacing:spacing label:self.titleLabel];
//    [self setLineSpacing:spacing label:self.contentLabel];   //修改内容的行间距
    
    _sawBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_sawBtn setTitle:[NSString stringWithFormat:@"%@", model.replies_cnt] forState:UIControlStateNormal];
    _supportBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_supportBtn setTitle:[NSString stringWithFormat:@"%@", model.thumbups_cnt] forState:UIControlStateNormal];
    //根据文字计算按钮的显示宽度
//    CGFloat sawWidth =[_sawBtn.titleLabel boundingRectWithString:_sawBtn.titleLabel.text withSize:CGSizeMake(MAXFLOAT, 0) withFont:13].width;
//    CGFloat supportBtnWidth =[_supportBtn.titleLabel boundingRectWithString:_supportBtn.titleLabel.text withSize:CGSizeMake(MAXFLOAT, 0) withFont:13].width;
//    self.sawWidth = sawWidth;
//    self.supportBtnWidth = supportBtnWidth;
    
    if ([[NSString stringWithFormat:@"%@", model.user[@"id"]] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        _deleteBtn.hidden = NO;
    }else{
        _deleteBtn.hidden = YES;
    }
    NSString *imageUrl = model.user[@"avatar"];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    self.collectionView.dataSource = dataSourceDelegate;
    self.collectionView.delegate = dataSourceDelegate;
    self.collectionView.indexPath = indexPath;
    [self.collectionView registerNib:[UINib nibWithNibName:@"SLTopicCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionViewCell"];
    [self.collectionView reloadData];
}
- (void)setFrame:(CGRect)frame{
    frame.size.height -= 5;
    [super setFrame:frame];
}

//评论按钮点击事件
- (IBAction)commentClick:(UIButton *)sender {
    if ([_Delegate respondsToSelector:@selector(didCommentButtonClicked:indexPath:)]) {
        [_Delegate didCommentButtonClicked:sender indexPath:self.indexpath];
    }
}

#pragma mark  ----- 删除话题的按钮的点击事件
- (IBAction)deleteClick:(UIButton *)sender {
    if ([_Delegate respondsToSelector:@selector(deleteTopicsButtonClicked:indexPath:)]) {
        [_Delegate deleteTopicsButtonClicked:sender indexPath:self.indexpath];
    }
}

//点赞的按钮被点击
- (IBAction)praise:(SLSupportBtn *)sender {
    if ([_Delegate respondsToSelector:@selector(didPraiseButtonClicked:indexPath:)]) {
        [_Delegate didPraiseButtonClicked:sender indexPath:self.indexpath];
    }
}
- (void)tapRecommendView{
    if ([_Delegate respondsToSelector:@selector(didUserIconClicked:)]) {
        [_Delegate didUserIconClicked:self.indexpath];
    }
}
@end
