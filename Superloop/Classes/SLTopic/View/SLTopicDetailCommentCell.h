//
//  SLTopicDetailCommentCell.h
//  Superloop
//
//  Created by WangS on 16/11/17.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLCommentModel;
@class SLTopicDetailCommentCell;
@protocol SLTopicDetailCommentCellDelegate <NSObject>
- (void)SLTopicDetailCommentCell:(SLTopicDetailCommentCell *)topicDetailCommentCell didSelectIconIndexPath:(NSIndexPath *)indexPath commentModel:(SLCommentModel *)commentModel;
- (void)SLTopicDetailCommentCell:(SLTopicDetailCommentCell *)topicDetailCommentCell didSelectDeleteBtn:(UIButton *)deleteBtn indexPath:(NSIndexPath *)indexPath commentModel:(SLCommentModel *)commentModel;
- (void)SLTopicDetailCommentCell:(SLTopicDetailCommentCell *)topicDetailCommentCell attributedLabel:(NSIndexPath *)indexPath commentModel:(SLCommentModel *)commentModel;
@end
@interface SLTopicDetailCommentCell : UITableViewCell
@property (nonatomic,weak) id<SLTopicDetailCommentCellDelegate>delegate;
@property (nonatomic,strong) SLCommentModel *commentModel;
@property (nonatomic,strong) NSIndexPath *indexPath;
@end
