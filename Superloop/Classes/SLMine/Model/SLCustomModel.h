//
//  SLCustomModel.h
//  Superloop
//
//  Created by xiaowu on 16/11/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLCustomModel : NSObject

@property(nonatomic, strong)NSString *leftTitle;
@property(nonatomic, strong)NSString *rightTitle;
@property(nonatomic, strong)UIColor  *leftColor;
@property(nonatomic, strong)UIColor  *rightColor;

@end
