//
//  SLCashCouponModel.h
//  Superloop
//
//  Created by xiaowu on 16/9/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

//amount (number): 代金券面值 ,
//expire_date (string): 代金券过期日期 ,
//left_amount (number): 代金券余额 ,
//receive_date (string): 代金券获得日期 ,
//voucher_code (string): 代金券Code ,
//voucher_status (integer): 代金券状态（1：正常，2：已用完，3：已过期） ,
//voucher_type (integer): 代金券类型（1：邀请奖励，2：抢券活动）
#import <Foundation/Foundation.h>

@interface SLCashCouponModel : NSObject

@property(nonatomic, strong)NSNumber *amount;

@property(nonatomic, copy)NSNumber *expire_date;

@property(nonatomic, strong)NSNumber *left_amount;

@property(nonatomic, copy)NSString *receive_date;

@property(nonatomic, copy)NSString *voucher_code;

@property(nonatomic, assign)NSInteger voucher_status;

@property(nonatomic, assign)NSInteger voucher_type;

@property(nonatomic, copy)NSString *created;

@end
