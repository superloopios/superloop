//
//  SLOtherEvaluateModel.h
//  Superloop
//
//  Created by WangS on 16/6/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLOtherEvaluateModel : NSObject


@property (nonatomic, strong) NSMutableDictionary *from_user;

@property (nonatomic, strong) NSMutableDictionary *to_user;

@property (nonatomic, strong) NSString *id;      //评价id
@property (nonatomic, strong) NSString *rate;    //评价星
@property (nonatomic, strong) NSString *rateContent;   // 评价内容
@property (nonatomic, strong) NSString *rateTime;    //时间
@property (nonatomic, strong) NSString *logId;    //



@end
