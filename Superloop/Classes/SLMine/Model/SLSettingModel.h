//
//  SLSettingModel.h
//  Superloop
//
//  Created by xiaowu on 16/8/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLSettingModel : NSObject

@property(nonatomic, copy)NSString *imageName;
@property(nonatomic, copy)NSString *title;
@property(nonatomic, copy)NSString *subTitle;
@property(nonatomic, assign)BOOL hasArrow;
@property(nonatomic, strong)UIColor *color;

//- (instancetype)initWithDict:(NSDictionary *)dict;

@end
