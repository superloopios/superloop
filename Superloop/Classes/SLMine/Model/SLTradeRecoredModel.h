//
//  SLTradeRecoredModel.h
//  Superloop
//
//  Created by WangS on 16/9/17.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLTradeRecoredModel : NSObject

@property (nonatomic,strong) NSNumber *amount;//总交易金额
@property (nonatomic,strong) NSNumber *balance;//余额
@property (nonatomic,copy) NSString *created;//交易创建时间
@property (nonatomic,assign) NSInteger id;//交易ID
@property (nonatomic,assign) NSInteger recordId;//通话ID
@property (nonatomic,assign) NSInteger status;//交易状态
@property (nonatomic,copy) NSString *tradeNo;//关联交易ID
@property (nonatomic,assign) NSInteger type;//交易类型
@property (nonatomic,copy) NSString *type_desc;//交易类型描述
@property (nonatomic,strong) NSNumber *voucher_amount;//所用代金券金额

/*
 amount (number, optional): 总交易金额 ,
 balance (number, optional): 余额 ,
 created (string, optional): 交易创建时间 ,
 id (integer, optional): 交易ID ,
 recordId (integer, optional): 通话ID ,
 status (integer, optional): 交易状态 ,
 tradeNo (string, optional): 关联交易ID ,
 type (integer, optional): 交易类型（0：微信支付，1：支付宝支付，2：通话主叫，3：通话被叫，4：阅读付费消息，5：提现 ,
 type_desc (string, optional): 交易类型描述 ,
 voucher_amount (number, optional): 所用代金券金额
 */
@end
