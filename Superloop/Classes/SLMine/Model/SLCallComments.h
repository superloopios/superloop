//
//  SLCallComments.h
//  Superloop
//
//  Created by WangJiwei on 16/4/27.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLCallComments : NSObject

//@property (nonatomic, strong) NSDictionary *from_user;
//@property (nonatomic, strong) NSDictionary *to_user;
//@property (nonatomic, strong) NSString *id;      //评论id
//@property (nonatomic, strong) NSString *rate;    //信用等级
//@property (nonatomic, strong) NSString *rateContent;   // 评价内容
//@property (nonatomic, strong) NSString *rateTime;    //时间
//@property (nonatomic, assign) CGFloat cellHeight;

@property (nonatomic,strong) NSNumber * anonymous_mode;//是否匿名
@property (nonatomic,strong) NSNumber * appraisal_id;//评价ID
@property (nonatomic,strong) NSNumber * appraisal_type;//评价类型（1：通话评价，2：付费消息评价)
@property (nonatomic,strong) NSNumber * call_log_id;//关联的通话记录ID（如果有）
@property (nonatomic,copy) NSString * from_avatar;//评价者头像
@property (nonatomic,copy) NSString * from_nickname;//评价者昵称
@property (nonatomic,strong) NSNumber * from_user_id;//评价者ID
@property (nonatomic,strong) NSNumber * rate;//评价值
@property (nonatomic,copy) NSString * rate_content;//评价内容
@property (nonatomic,copy) NSString * rate_time;//评价时间
@property (nonatomic,strong) NSNumber * rate_source;// 评价来源（1：用户，2：系统
@property (nonatomic,copy) NSString * reply_content;//评价回复内容
@property (nonatomic,copy) NSString * reply_time;//评价回复时间
@property (nonatomic,copy) NSString * to_avatar;//被评价者头像
@property (nonatomic,copy) NSString * to_nickname;//被评价者昵称
@property (nonatomic,strong) NSNumber * to_user_id;//被评价者ID

@property (nonatomic,assign) BOOL  isClose;//是否展开
@property (nonatomic,assign) BOOL  isReplyClose;//回复评价是否展开

@end
