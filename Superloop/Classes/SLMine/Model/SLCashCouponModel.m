//
//  SLCashCouponModel.m
//  Superloop
//
//  Created by xiaowu on 16/9/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCashCouponModel.h"

@implementation SLCashCouponModel

- (NSString *)created
{
    // 将时间戳字符串转成日期
    NSString *string = [NSString stringWithFormat:@"%ld",[_expire_date integerValue]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:string.doubleValue / 1000];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
    fmt.dateFormat = @"yyyy-MM-dd";
    NSString *dateStr = [fmt stringFromDate:date];
    NSDate *createdAtDate = [fmt dateFromString:dateStr];
    
    return [fmt stringFromDate:createdAtDate];
}

@end
