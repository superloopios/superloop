//
//  SLMyComment.m
//  Superloop
//
//  Created by WangJiwei on 16/4/25.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLMyComment.h"

@implementation SLMyComment
- (NSArray *)imgs{
    return _topic[@"imgs"];
}
- (NSString *)created
{
    // 将时间戳字符串转成日期
    NSString *string = [NSString stringWithFormat:@"%@", _reply[@"created"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:string.doubleValue / 1000];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *dateStr = [fmt stringFromDate:date];
    NSDate *createdAtDate = [fmt dateFromString:dateStr];
    
    if (createdAtDate.sl_isThisYear) { // 今年
        if (createdAtDate.sl_isYesterday) { // 昨天
            fmt.dateFormat = @"昨天 HH:mm:ss";
            return [fmt stringFromDate:createdAtDate];
        } else if (createdAtDate.sl_isToday) { // 今天
            NSCalendar *calendar = [NSCalendar sl_calendar];
            NSCalendarUnit unit = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
            NSDateComponents *cmps = [calendar components:unit fromDate:createdAtDate toDate:[NSDate date] options:0];
            
            if (cmps.hour >= 1) { // 时间间隔 >= 1小时
                return [NSString stringWithFormat:@"%zd小时前", cmps.hour];
            } else if (cmps.minute >= 1) { // 1小时 > 时间间隔 >= 1分钟
                return [NSString stringWithFormat:@"%zd分钟前", cmps.minute];
            } else { // 时间间隔 < 1分钟
                return @"刚刚";
            }
        } else {
            fmt.dateFormat = @"MM-dd HH:mm:ss";
            return [fmt stringFromDate:createdAtDate];
        }
    } else { // 不是今年
        return _created;
    }
    return _created;
}

#pragma mark ---- 计算cell的行高

- (CGFloat)cellHeight
{
    _cellHeight = 122;
     CGFloat textMaxW = ScreenW - 25;
    
    //内容高度
//    NSString *contentTextString = @"Text";
//    CGFloat contemtFontSize = [contentTextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]} context:nil].size.height;
    NSString *text;
    if ([self.type isEqualToString:@"1"]) {
        text = [NSString stringWithFormat:@"回复%@:%@",_user[@"nickname"],_reply[@"content"]];
    }else{
        text = _reply[@"content"];
    }
    CGFloat commentHeight = [text boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]} context:nil].size.height;
    if (commentHeight > 37) {
        commentHeight = 37;
    }
    _cellHeight += commentHeight;
    
//     CGFloat otherCommentHeight = [_content boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]} context:nil].size.height;
//    NSInteger newContentLineNum = otherCommentHeight / contemtFontSize;
//    
//    if (newContentLineNum > 3) {
//        newContentLineNum = 3;
//    }
//    _cellHeight += newContentLineNum * contemtFontSize + 11;
//    
//    _cellHeight += otherCommentHeight;
//    _cellHeight += 1;
    return _cellHeight;
}
@end
