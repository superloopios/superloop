//
//  SLOtherEvaluateModel.m
//  Superloop
//
//  Created by WangS on 16/6/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLOtherEvaluateModel.h"

@implementation SLOtherEvaluateModel


- (NSString *)rateTime
{
    
    // 将时间戳字符串转成日期
    //    NSString *time = [NSString stringWithFormat:@]
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_rateTime.doubleValue / 1000];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *dateStr = [fmt stringFromDate:date];
    NSDate *createdAtDate = [fmt dateFromString:dateStr];
    
    if (createdAtDate.sl_isThisYear) { // 今年
        if (createdAtDate.sl_isYesterday) { // 昨天
            fmt.dateFormat = @"昨天 HH:mm:ss";
            return [fmt stringFromDate:createdAtDate];
        } else if (createdAtDate.sl_isToday) { // 今天
            NSCalendar *calendar = [NSCalendar sl_calendar];
            NSCalendarUnit unit = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
            NSDateComponents *cmps = [calendar components:unit fromDate:createdAtDate toDate:[NSDate date] options:0];
            
            if (cmps.hour >= 1) { // 时间间隔 >= 1小时
                return [NSString stringWithFormat:@"%zd小时前", cmps.hour];
            } else if (cmps.minute >= 1) { // 1小时 > 时间间隔 >= 1分钟
                return [NSString stringWithFormat:@"%zd分钟前", cmps.minute];
            } else { // 时间间隔 < 1分钟
                return @"刚刚";
            }
        } else {
            fmt.dateFormat = @"MM-dd HH:mm:ss";
            return [fmt stringFromDate:createdAtDate];
        }
    } else { // 不是今年
        return _rateTime;
    }
    
    
    return _rateTime;
}



@end
