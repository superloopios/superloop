//
//  SLNormalQuestionModel.h
//  Superloop
//
//  Created by WangS on 16/6/6.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLNormalQuestionModel : NSObject

@property (nonatomic,copy) NSString *groupStr;

@property (nonatomic,strong) NSMutableArray *contentArr;

@property (nonatomic,assign) BOOL isHide;


@end
