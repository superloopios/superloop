//
//  SLMyComment.h
//  Superloop
//
//  Created by WangJiwei on 16/4/25.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLMyComment : NSObject
@property (nonatomic, copy) NSString *id;  //旧版本:评论话题的id ;   新版本被评论人的userid
@property (nonatomic, copy) NSString *title;  //话题的标题

@property (nonatomic, strong) NSDictionary *reply; //我的评论信息 (id, content, digest,created,user(id, nickname, avatar))
@property (nonatomic, strong) NSDictionary *topic;

@property (nonatomic, strong) NSDictionary *user;

//@property (nonatomic, copy) NSString *status; //被评论的话题是否删除  0/1

@property (nonatomic, copy) NSString *created;  // 评价时间
@property (nonatomic, assign) CGFloat cellHeight;  //cell高度
@property (nonatomic, copy) NSString *type;    // 0 代表话题   1  代表评论

@property (nonatomic, strong) NSArray *imgs; //图片数组
@end
