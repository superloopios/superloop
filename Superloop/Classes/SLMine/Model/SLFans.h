//
//  SLFans.h
//  Superloop
//
//  Created by 朱宏伟 on 16/5/6.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SLFans : NSObject

@property (nonatomic, copy) NSString *id;      //用户id
@property (nonatomic, copy) NSString *avatar;    //icon图
//@property (nonatomic, copy) NSString *cellphone;   // 评价内容
//@property (nonatomic, copy) NSString *cover;   // 评价内容
//@property (nonatomic, copy) NSString *location;    //时间
@property (nonatomic, copy) NSString *nickname;//名字
//@property (nonatomic, copy) NSString *username;
//@property (nonatomic, copy) NSString *username_can_modify;
//@property (nonatomic,strong)NSArray *work_history;
@property (nonatomic, copy) NSString *bio;//简介
//@property (nonatomic, copy) NSArray *education;
@property (nonatomic, copy) NSString *following;//关注

@end
