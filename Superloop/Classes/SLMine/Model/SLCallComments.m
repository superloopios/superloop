//
//  SLCallComments.m
//  Superloop
//
//  Created by WangJiwei on 16/4/27.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCallComments.h"
#import "AppDelegate.h"
@implementation SLCallComments

- (NSString *)rate_time{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_rate_time.doubleValue / 1000];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
    fmt.dateFormat = @"MM-dd HH:mm";
    NSString *dateStr = [fmt stringFromDate:date];
    return dateStr;
}
- (NSString *)reply_time{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_reply_time.doubleValue / 1000];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
    fmt.dateFormat = @"MM-dd HH:mm";
    NSString *dateStr = [fmt stringFromDate:date];
    return dateStr;
}
/*
- (CGFloat)cellHeight{
    
    NSString *idString = [NSString stringWithFormat:@"%@", _from_user[@"id"]];
    if ([idString isEqualToString:[NSString stringWithFormat:@"%@",ApplicationDelegate.userId]]) {
        //这是我给别人的评论
        _cellHeight = 65;
        CGFloat textMaxW = ScreenW - 55;
        CGFloat titleHeight = [self.rateContent boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
        _cellHeight += titleHeight;
        
    }else{
//        _cellHeight = 62;
//        CGFloat textMaxW = ScreenW - 20;
//        CGFloat titleHeight = [self.rateContent boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height+5;
//        _cellHeight += titleHeight;
        
        _cellHeight = 72;
        CGFloat standardHeight = [self getSpaceLabelHeight:@"高度，高度" withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-70];
        CGFloat titleHeight = [self getSpaceLabelHeight:self.rateContent withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-70];
        NSInteger count = titleHeight/standardHeight;
        if (count > 5) {
            count = 5;
        }
        _cellHeight += standardHeight * count;
        _cellHeight += 30+8+1+20+15;
    }
    return _cellHeight;
}

- (NSString *)rateTime{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_rateTime.doubleValue / 1000];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
    fmt.dateFormat = @"MM-dd HH:mm";
    NSString *dateStr = [fmt stringFromDate:date];
    return dateStr;
}
//计算UILabel的高度(带有行间距的情况)
- (CGFloat)getSpaceLabelHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = 23;
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f
                          };
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return size.height;
}
 */

@end
