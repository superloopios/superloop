//
//  SLOtherEvaluateTableViewCell.h
//  Superloop
//
//  Created by WangS on 16/6/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLOtherEvaluateModel;

@interface SLOtherEvaluateTableViewCell : UITableViewCell

@property (nonatomic,strong)SLOtherEvaluateModel *model;


@end
