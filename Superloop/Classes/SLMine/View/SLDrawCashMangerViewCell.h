//
//  SLDrawCashMangerViewCell.h
//  Superloop
//
//  Created by 朱宏伟 on 16/6/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLDrawCashMangerViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageLeft;

@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIImageView *rightImage;


@end
