//
//  SLCheckViewCell.m
//  Superloop
//
//  Created by WangJiwei on 16/5/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCheckViewCell.h"
#import "SLImageCollectionView.h"

#import "SLCheckImageCell.h"

@interface SLCheckViewCell()<UICollectionViewDataSource,UICollectionViewDelegate,SLCheckImageCellDelegate>

@property (weak, nonatomic) IBOutlet SLImageCollectionView *photoCollectionView;

@property (weak, nonatomic) IBOutlet UIButton *photoBtn;
@property (weak, nonatomic) IBOutlet UIButton *camerBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;

@end
@implementation SLCheckViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.photoCollectionView.dataSource=self;
    self.photoCollectionView.delegate=self;
    
    _photoBtn.layer.cornerRadius = 10;
    _photoBtn.layer.masksToBounds = YES;
    _photoBtn.layer.borderWidth = 1;
    _photoBtn.layer.borderColor = SLColor(181, 225, 219).CGColor;
    
    _camerBtn.layer.cornerRadius = 10;
    _camerBtn.layer.masksToBounds = YES;
    _camerBtn.layer.borderWidth = 1;
    _camerBtn.layer.borderColor = SLColor(181, 225, 219).CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (IBAction)photoBtnClick:(UIButton *)sender {
    if ([_Delegate respondsToSelector:@selector(didPictureClicked:)]) {
        [_Delegate didPictureClicked:_indexpath];
    }
}

- (IBAction)camerBtnClick:(UIButton *)sender {
    if ([_Delegate respondsToSelector:@selector(didcamerClicked:)]) {
        [_Delegate didcamerClicked:_indexpath];
    }
}

-(void)setImgArr:(NSMutableArray *)imgArr
{
    _imgArr=imgArr;
    
//    if (imgArr.count > 0&&imgArr.count<4) {
//        [_collectionViewHeight setConstant:110];
//    }else if (imgArr.count>3){
//        [_collectionViewHeight setConstant:110+110+10+10];
//    }
}



#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _imgArr.count;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    if (_imgArr.count>2) {
//        return CGSizeMake((ScreenW - 60) / 3, 110);
//    }
    return CGSizeMake((ScreenW - 45) / 2, 110);
    
}


//设置偏移量
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
   
        if (_imgArr.count>1) {
            
            return UIEdgeInsetsMake(0, 0, 0, 0);
      
        }
    return UIEdgeInsetsMake(0, (ScreenW+45)/8, 0, (ScreenW+45)/8);
    
}

//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SLImageCollectionView * cellCollectionView  = (SLImageCollectionView *)collectionView;
    SLCheckImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCell" forIndexPath:indexPath];
    cell.indexPath = indexPath;
    cell.collectionViewIndexPath = cellCollectionView.indexPath;
    cell.Delegate = self;
    
    cell.imageView.image = [UIImage imageWithData:_imgArr[indexPath.row]];
   
    return cell;
}





- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource,UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath count:(NSInteger)count
{
    if (count > 0) {
        self.photoCollectionView.hidden = NO;

    }else
    {
        self.photoCollectionView.hidden = YES;
    }
    
  
    self.photoCollectionView.backgroundColor = [UIColor whiteColor];
//    self.photoCollectionView.dataSource = dataSourceDelegate;
//    self.photoCollectionView.delegate = dataSourceDelegate;
    self.photoCollectionView.indexPath = indexPath;
    [self.photoCollectionView registerNib:[UINib nibWithNibName:@"SLCheckImageCell" bundle:nil] forCellWithReuseIdentifier:@"ImageCell"];

    [self.photoCollectionView reloadData];

}

- (void)didCancelClick:(NSIndexPath *)indexPath collectionViewIndexPath:(NSIndexPath *)collectionViewIndexPath
{
//    NSMutableArray *array = _pictures[collectionViewIndexPath.row];
//    UIImage *image = array[indexPath.row];
//    
//    
//    [array removeObject:image];
   
    //[self.tableView reloadData];
    [_imgArr removeObjectAtIndex:indexPath.row];
    self.deleteBlock(_imgArr);
}



@end
