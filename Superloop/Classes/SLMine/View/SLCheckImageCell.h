//
//  SLCheckImageCell.h
//  Superloop
//
//  Created by WangJiwei on 16/5/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLCheckImageCell;
@protocol SLCheckImageCellDelegate <NSObject>
-(void)didCancelClick:(NSIndexPath *)indexPath collectionViewIndexPath:(NSIndexPath *)collectionViewIndexPath;
@end
@interface SLCheckImageCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) id<SLCheckImageCellDelegate>Delegate;
@property (nonatomic , strong) NSIndexPath *indexPath;
@property (nonatomic , strong) NSIndexPath *collectionViewIndexPath;
@end
