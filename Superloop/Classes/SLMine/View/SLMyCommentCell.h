//
//  SLMyCommentCell.h
//  Superloop
//
//  Created by WangJiwei on 16/4/22.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLMyComment;
@class SLMyCommentCell;
@protocol slMyCommentCellDelegate <NSObject>
- (void)deleteButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath;
- (void)didtapRecommendViewClicked:(NSIndexPath *)indexpath;
- (void)didClickUserName:(NSString *)userId;
@end
@interface SLMyCommentCell : UITableViewCell
@property (nonatomic, strong) SLMyComment *model;
@property (nonatomic,weak)id<slMyCommentCellDelegate>delegate;
@property (nonatomic,strong)NSIndexPath *indexPath;
@end
