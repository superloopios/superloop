//
//  SLCheckViewCell.h
//  Superloop
//
//  Created by WangJiwei on 16/5/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLCheckViewCell;
@protocol SLCheckViewCellDelegate <NSObject>
- (void)didPictureClicked:(NSIndexPath *)indexpath;
- (void)didcamerClicked:(NSIndexPath *)indexpath;
@end
@interface SLCheckViewCell : UITableViewCell
@property (nonatomic, weak) id<SLCheckViewCellDelegate>Delegate;
@property (nonatomic , strong) NSIndexPath *indexpath;
@property (weak, nonatomic) IBOutlet UILabel *titltLabel;

@property (nonatomic,strong)NSMutableArray *imgArr;
@property (nonatomic,copy)void (^deleteBlock)(NSMutableArray *mArr);


- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource,UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath*)indexPath count:(NSInteger)count;


@end



