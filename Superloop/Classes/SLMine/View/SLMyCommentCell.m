//
//  SLMyCommentCell.m
//  Superloop
//
//  Created by WangJiwei on 16/4/22.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLMyCommentCell.h"
#import "SLMyComment.h"
#import <UIImageView+WebCache.h>
#import "TTTAttributedLabel.h"
@interface SLMyCommentCell()
@property (weak, nonatomic) IBOutlet UIImageView *iconView;  // 话题的第一个icon
//@property (weak, nonatomic) IBOutlet UILabel *nickName;
//@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *myCommentLabel; //我的评论

@property (weak, nonatomic) IBOutlet UILabel *topicTitle; //评论话题标题
//@property (weak, nonatomic) IBOutlet UIImageView *topicImage;

//@property (weak, nonatomic) IBOutlet UILabel *contentLable;
@property (weak, nonatomic) IBOutlet UIImageView *coverImage; //遮盖的view

@property (weak, nonatomic) IBOutlet UIImageView *meIconImage;  //我的头像

@property (weak, nonatomic) IBOutlet UILabel *nickNameLable; //我的昵称

@property (weak, nonatomic) IBOutlet UILabel *timeLable;  //时间lable

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vCurtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topicConstraint;


@property (weak, nonatomic) IBOutlet UIView *backView;

@end;
@implementation SLMyCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.iconView.layer.cornerRadius = 2.5;
    self.iconView.layer.masksToBounds = YES;
    self.iconView.contentMode  = UIViewContentModeScaleAspectFill;
    self.iconView.clipsToBounds = YES;
    
    self.meIconImage.layer.cornerRadius = 2.5;
    self.meIconImage.layer.masksToBounds = YES;
    
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecommendView)];
    [self.coverImage addGestureRecognizer:tapView];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)deleteComment:(id)sender {
    
    if([self.delegate respondsToSelector:@selector(deleteButtonClicked:indexPath:)]) {
        [_delegate deleteButtonClicked:sender indexPath:_indexPath];
    }

}

- (void)setModel:(SLMyComment *)model
{

    _model = model;
    [self.meIconImage sd_setImageWithURL:[NSURL URLWithString:model.reply[@"user"][@"avatar"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];//我的头像
    self.nickNameLable.text = model.reply[@"user"][@"nickname"];
    
    _topicTitle.text = model.topic[@"title"];
//    _myCommentLabel.text = model.reply[@"content"];
    //  给commentLabel赋值
    NSString *text;
    if ([model.type isEqualToString:@"1"]) {
        
        text = [NSString stringWithFormat:@"回复%@:%@",model.user[@"nickname"],model.reply[@"content"]];
        
        //计算高度
        //        [_content setAttributedText:str];
        [_myCommentLabel setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString)
         {
             
             //设置可点击文字的范围
             NSRange boldRange = [[mutableAttributedString string] rangeOfString:model.user[@"nickname"] options:NSCaseInsensitiveSearch];
             
             //设定可点击文字的的大小
             UIFont *boldSystemFont = [UIFont systemFontOfSize:15];
             CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
             
             if (font) {
                 
                 //设置可点击文本的大小
                 [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:boldRange];
                 
                 //设置可点击文本的颜色
                 [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[SLColor(0, 142, 255) CGColor] range:boldRange];
                 
                 CFRelease(font);
                 
             }
             return mutableAttributedString;
         }];
        NSRange linkRange = [text rangeOfString:model.user[@"nickname"]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@""]];
        NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionary];
        [linkAttributes setValue:[NSNumber numberWithBool:NO] forKey:(NSString *)kCTUnderlineStyleAttributeName];
        
        _myCommentLabel.linkAttributes = linkAttributes;
        //设置链接的url
        [_myCommentLabel addLinkToURL:url withRange:linkRange];
        
    }else{
        _myCommentLabel.text = model.reply[@"content"];
    }
    _myCommentLabel.delegate = self;
    _timeLable.text = model.created;  //时间
   // _nickName.text = model.reply[@"user"][@"nickname"];
    if (model.imgs.count>0) {
        self.iconView.hidden=NO;
        [_topicConstraint setConstant:67];
        //头像
//        NSLog(@"%@",model.imgs[0]);
        [self.iconView sd_setImageWithURL:[NSURL URLWithString:[[model.imgs firstObject] objectForKey:@"url"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    }else{
        self.iconView.hidden=YES;
        [_topicConstraint setConstant:10];
    }

}

- (void)setFrame:(CGRect)frame
{
    frame.size.height -= 5;
    [super setFrame:frame];
}

#pragma mark - 图片按钮点击的跳转事件
- (void)tapRecommendView{
    
    if ([_delegate respondsToSelector:@selector(didtapRecommendViewClicked:)]) {
        [_delegate didtapRecommendViewClicked:self.indexPath];
    }


}
- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url
{
    NSString *userId;
    if ([[self.model.user[@"id"] class] isSubclassOfClass:[NSNumber class]]) {
        userId = [self.model.user[@"id"] stringValue];
    }else{
        userId = self.model.user[@"id"];
    }
    
    if ([_delegate respondsToSelector:@selector(didClickUserName:)]) {
        [_delegate didClickUserName:userId];
    }
}
- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    [self.heightConstraint setConstant:0.5];
}
@end
