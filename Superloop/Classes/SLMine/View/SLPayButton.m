//
//  SLPayButton.m
//  Superloop
//
//  Created by 朱宏伟 on 16/5/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//  自定义的支付充值按钮对于位置的调整

#import "SLPayButton.h"

@implementation SLPayButton

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    
    self.imageView.x = ScreenW - 40;
    self.imageView.width = 22;
    self.imageView.height = 22;
    self.imageView.y = 10;
    
}

@end
