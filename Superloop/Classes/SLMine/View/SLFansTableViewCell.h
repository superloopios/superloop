//
//  SLFansTableViewCell.h
//  Superloop
//
//  Created by xiaowu on 16/11/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLFans;
@class SLFansTableViewCell;

@protocol SLFansTableViewCellDelegate <NSObject>
- (void)didFcsButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath;
@end

@interface SLFansTableViewCell : UITableViewCell
@property (nonatomic,   weak) id<SLFansTableViewCellDelegate>delegate;
@property (nonatomic, strong) NSIndexPath *indexpath;
@property (nonatomic, strong) SLFans *fanModel;

@end
