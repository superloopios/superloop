//
//  SLCustomTableViewCell.m
//  Superloop
//
//  Created by xiaowu on 16/11/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCustomTableViewCell.h"
#import "SLCustomModel.h"

@interface SLCustomTableViewCell ()

@property(nonatomic, strong)UILabel *titleLabel;
@property(nonatomic, strong)UILabel *subTitleLabel;
@property(nonatomic, strong)UIView *sepatorView;

@end

@implementation SLCustomTableViewCell

- (UIView *)sepatorView{
    if (!_sepatorView) {
        _sepatorView = [[UIView alloc] init];
        _sepatorView.backgroundColor = SLColor(220, 220, 220);
        [self.contentView addSubview:_sepatorView];
    }
    return _sepatorView;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.sepatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.height.equalTo(@0.5);
        }];
    }
    return self;
}
- (UILabel *)subTitleLabel{
    if (!_subTitleLabel) {
        _subTitleLabel = [[UILabel alloc] init];
        _subTitleLabel.font = [UIFont systemFontOfSize:14];
        _subTitleLabel.textColor = SLColor(128, 128, 128);
        [self.contentView addSubview:_subTitleLabel];
        [_subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-10);
            make.centerY.equalTo(self.mas_centerY);
        }];
    }
    return _subTitleLabel;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:14];
        _titleLabel.textColor = SLColor(88, 88, 88);
        [self.contentView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(10);
            make.centerY.equalTo(self.mas_centerY);
        }];
    }
    return _titleLabel;
}
- (void)setModel:(SLCustomModel *)model{
    _model = model;
    self.titleLabel.text = model.leftTitle;
    self.subTitleLabel.text = model.rightTitle;
    if (model.leftColor) {
        self.titleLabel.textColor = model.leftColor;
    }else{
        self.titleLabel.textColor = SLColor(88, 88, 88);
    }
    if (model.rightColor) {
        self.subTitleLabel.textColor = model.rightColor;
    }else{
        self.subTitleLabel.textColor = UIColorFromRGB(0x2b2124);
    }
}

@end
