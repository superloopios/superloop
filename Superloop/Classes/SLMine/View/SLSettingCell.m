//
//  SLSettingCell.m
//  Superloop
//
//  Created by xiaowu on 16/8/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSettingCell.h"
#import "SLSettingModel.h"

@interface SLSettingCell ()
@property(nonatomic, strong)UIImageView *enterIv;
@property(nonatomic, strong)UIImageView *iconIv;
@property(nonatomic, strong)UILabel *titleLabel;
@property(nonatomic, strong)UILabel *subTitleLabel;
@property (nonatomic, strong) UIView      * sepatorView;
@end

@implementation SLSettingCell

- (UIView *)sepatorView{
    if (!_sepatorView) {
        _sepatorView = [[UIView alloc] initWithFrame:CGRectMake(25, 59, screen_W-50, 0.5)];
        _sepatorView.backgroundColor = UIColorFromRGB(0xebddd5);
    }
    return _sepatorView;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self addSubview:self.sepatorView];
    }
    return self;
}
- (UILabel *)subTitleLabel{
    if (!_subTitleLabel) {
        _subTitleLabel = [[UILabel alloc] init];
        _subTitleLabel.font = [UIFont systemFontOfSize:14];
        _subTitleLabel.textColor = UIColorFromRGB(0x9e7f7b);
        
        [self.contentView addSubview:_subTitleLabel];
        [_subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-25);
            make.centerY.equalTo(self.mas_centerY);
            
        }];
    }
    return _subTitleLabel;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:17];
        _titleLabel.textColor = SLBlackTitleColor;
        _titleLabel.numberOfLines = 0;
        [self.contentView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(25);
            make.centerY.equalTo(self.mas_centerY);
        }];
    }
    return _titleLabel;
}

- (UIImageView *)enterIv{
    if (!_enterIv) {
        _enterIv = [[UIImageView alloc] init];
        _enterIv.image = [UIImage imageNamed:@"Down-Arrow-outline"];
        _enterIv.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:_enterIv];
        [_enterIv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).offset(-12);
            make.centerY.equalTo(self.mas_centerY);
            make.height.width.equalTo(@10);
        }];
    }
    return _enterIv;
}

- (UIImageView *)iconIv{
    if (!_iconIv) {
        _iconIv = [[UIImageView alloc] init];
        _iconIv.image = [UIImage imageNamed:@"Down-Arrow-outline"];
        _iconIv.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:_iconIv];
        [_iconIv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(12);
            make.centerY.equalTo(self.mas_centerY);
            make.height.width.equalTo(@19);
        }];
    }
    return _iconIv;
}

- (void)setModel:(SLSettingModel *)model{
    _model = model;
    self.enterIv.hidden = YES;
    
    if (model.imageName) {
        self.iconIv.image = [UIImage imageNamed:model.imageName];
    }
    
//        model.subTitle = @"未认证";
//    
//        model.subTitle = @"认证中";
//    
//        model.subTitle = @"已认证";
    if ([model.subTitle containsString:@"认证"]){
        self.subTitleLabel.textColor = UIColorFromRGB(0xff0000);
    }else{
        self.subTitleLabel.textColor = UIColorFromRGB(0x9e7f7b);
    }
    
    self.titleLabel.text = model.title;
    if (model.subTitle) {
        self.subTitleLabel.text = model.subTitle;
    }
    if (model.color) {
        self.sepatorView.backgroundColor = SLMainColor;
    }else{
        self.sepatorView.backgroundColor = UIColorFromRGB(0xebddd5);
    }
}

@end
