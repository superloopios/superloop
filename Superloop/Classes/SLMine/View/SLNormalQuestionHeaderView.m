//
//  SLNormalQuestionHeaderView.m
//  Superloop
//
//  Created by WangS on 16/6/6.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLNormalQuestionHeaderView.h"

@interface SLNormalQuestionHeaderView ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineViewheightConstraint;




@end

@implementation SLNormalQuestionHeaderView

-(void)layoutSubviews{
    [_lineViewheightConstraint setConstant:0.5];

}
-(instancetype)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        
        [_topViewContraint setConstant:0.5];

        [_viewHeightConstraint setConstant:0.5];
    }
    return self;
}






@end
