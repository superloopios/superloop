//
//  SLTradeTableViewCell.m
//  Superloop
//
//  Created by 张梦川 on 16/5/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTradeTableViewCell.h"

@implementation SLTradeTableViewCell

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.lineViewContract setConstant:0.5];
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
