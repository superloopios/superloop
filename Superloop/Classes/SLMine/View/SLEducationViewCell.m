//
//  SLEducationViewCell.m
//  Superloop
//
//  Created by WangJiwei on 16/4/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLEducationViewCell.h"

@implementation SLEducationViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Down-Arrow-outline"]];
    // Initialization code
}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}
//- (void)setFrame:(CGRect)frame
//{
//    frame.size.height -= 10;
//    
//    [super setFrame:frame];
//}

@end
