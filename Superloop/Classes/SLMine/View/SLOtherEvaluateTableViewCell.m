//
//  SLOtherEvaluateTableViewCell.m
//  Superloop
//
//  Created by WangS on 16/6/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLOtherEvaluateTableViewCell.h"

#import <UIImageView+WebCache.h>

#import "SLOtherEvaluateModel.h"

@interface SLOtherEvaluateTableViewCell ()

@property (nonatomic,strong)UIImageView *iconImg;
@property (nonatomic,strong)UIImageView *starImg;
@property (nonatomic,strong)UILabel *timeLab;
@property (nonatomic,strong)UILabel *contentLab;
@property (nonatomic,strong)UILabel *nameLab;

@property (nonatomic,strong)UILabel *constantLab;

@end

@implementation SLOtherEvaluateTableViewCell





-(void)setModel:(SLOtherEvaluateModel *)model{

    _model=model;


    _iconImg=[[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 60, 60)];
    _iconImg.layer.cornerRadius = 5;
    _iconImg.layer.masksToBounds = YES;
    [_iconImg sd_setImageWithURL:[NSURL URLWithString:model.from_user[@"avatar"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    [self.contentView addSubview:_iconImg];

    
    _nameLab=[[UILabel alloc] init];
    _nameLab.textColor=[UIColor blackColor];
    _nameLab.font=[UIFont systemFontOfSize:13];
    _nameLab.text=model.from_user[@"nickname"];
    [self.contentView addSubview:_nameLab];
   
    //根据计算文字的大小
    CGFloat nameLabWidth= [model.from_user[@"nickname"] boundingRectWithSize:CGSizeMake(ScreenW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13]} context:nil].size.width+10;
    if (nameLabWidth>120) {
        nameLabWidth=120;
    }
    [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView.mas_top).offset(10);
        make.left.mas_equalTo(_iconImg.mas_right).offset(10);
        make.width.mas_equalTo(nameLabWidth);
        make.height.mas_equalTo(20);
    }];
    
    _contentLab=[[UILabel alloc] init];
    _contentLab.font=[UIFont systemFontOfSize:13];
    _contentLab.text=@"评价了Ta";
    _contentLab.textColor=[UIColor grayColor];
    [self.contentView addSubview:_contentLab];
    
    [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView.mas_top).offset(10);
        make.left.mas_equalTo(_nameLab.mas_right);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(20);
    }];
    
   //星星
    for (int i=0; i<5; i++) {
        UIImageView *imgCredit_level=[[UIImageView alloc] initWithFrame:CGRectMake(nameLabWidth+80+60+i*12, 15, 10, 10)];
        imgCredit_level.image=[UIImage imageNamed:@"credit_level"];
        [self.contentView addSubview:imgCredit_level];
        
    }
    NSInteger numberOfRat=[model.rate integerValue];
    
    for (int i=0; i<numberOfRat; i++) {
        
        UIImageView *imgColor=[[UIImageView alloc] initWithFrame:CGRectMake(nameLabWidth+80+60+i*12, 15, 10, 10)];
        imgColor.image=[UIImage imageNamed:@"colorCredit_level"];
        [self.contentView addSubview:imgColor];
        
    }
    
    _timeLab=[[UILabel alloc] init];
    _timeLab.textColor=[UIColor grayColor];
    _timeLab.text=model.rateTime;
    _timeLab.font=[UIFont systemFontOfSize:13];
    [self.contentView addSubview:_timeLab];
    //根据计算文字的大小
    CGFloat timeLabWidth= [model.rateTime boundingRectWithSize:CGSizeMake(ScreenW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13]} context:nil].size.width+20;
    
    
    [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_nameLab.mas_bottom).offset(10);
        make.left.mas_equalTo(_iconImg.mas_right).offset(10);
        make.width.mas_equalTo(timeLabWidth);
        make.height.mas_equalTo(20);
    }];
    
    
    _contentLab=[[UILabel alloc] initWithFrame:CGRectMake(10, 70, ScreenW-20, 30)];
    _contentLab.font=[UIFont systemFontOfSize:12];
    _contentLab.textColor=[UIColor grayColor];
    _contentLab.numberOfLines=0;
    _contentLab.text=model.rateContent;
    [self.contentView addSubview:_contentLab];
    
    
}

- (void)setFrame:(CGRect)frame
{
    frame.size.height -= 5;
    [super setFrame:frame];
}

@end
