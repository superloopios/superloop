//
//  SLHeaderView.h
//  Superloop
//
//  Created by xiaowu on 16/8/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLImageRightBtn;
@class HMSegmentedControl;

@interface SLHeaderView : UIView


@property (nonatomic, strong) NSDictionary                  *userInfo;
@property (nonatomic, strong) UIButton                      *avatarbtn;
@property (nonatomic, strong) SLImageRightBtn               *reZhengbtn;
@property (nonatomic, strong) SLLabel                       *nickNamelabel;
@property (nonatomic, strong) UIImageView                   *sexImageView;
@property (nonatomic, strong) UIButton                      *fensiBtn;
@property (nonatomic, strong) UIButton                      *guanzhuBtn;
@property (nonatomic, strong) HMSegmentedControl            *segCtrl;
@property (nonatomic, strong) UIView                        *videoHeaderView;//视频头
@property (nonatomic, strong) UIImageView                   *backImageView;
@property (nonatomic,   copy) void(^changeHeaderBackImage)(NSString *);
@property (nonatomic,   copy) void(^enterFenSiVc)(NSString *);
-(void)pause;
-(void)play;
- (void)changeVideo;
- (void)changNickName:(NSString *)name;

@end
