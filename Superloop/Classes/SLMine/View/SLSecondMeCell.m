//
//  SLSecondMeCell.m
//  Superloop
//
//  Created by xiaowu on 16/8/17.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSecondMeCell.h"
#import "SLSettingModel.h"

@interface SLSecondMeCell ()
@property(nonatomic, strong)UIImageView *iconIv;
@property(nonatomic, strong)UIImageView *enterIv;
@property(nonatomic, strong)UILabel *titleLabel;
@property(nonatomic, strong)SLLabel *subTitleLabel;
@property(nonatomic, strong)UIView *sepeView;

@end

@implementation SLSecondMeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}
- (UIView *)sepeView{
    if (!_sepeView) {
        _sepeView = [[UIView alloc] init];
        _sepeView.backgroundColor = UIColorFromRGB(0xebddd5);
        [self.contentView addSubview:_sepeView];
    }
    return _sepeView;
}
- (SLLabel *)subTitleLabel{
    if (!_subTitleLabel) {
        _subTitleLabel = [[SLLabel alloc] init];
        _subTitleLabel.font = [UIFont systemFontOfSize:14];
        _subTitleLabel.textColor = UIColorFromRGB(0xb3907d);
        _subTitleLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_subTitleLabel];
        [_subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.mas_right).offset(-25);
            make.centerY.equalTo(self.mas_centerY);
            make.width.equalTo(@(screen_W-100));
        }];
    }
    return _subTitleLabel;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(25);
            make.centerY.equalTo(self.mas_centerY);
        }];
    }
    return _titleLabel;
}

- (UIImageView *)enterIv{
    if (!_enterIv) {
        _enterIv = [[UIImageView alloc] init];
        _enterIv.image = [UIImage imageNamed:@"Down-Arrow-outline"];
        _enterIv.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:_enterIv];
        [_enterIv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).offset(-25);
            make.centerY.equalTo(self.mas_centerY);
            make.height.width.equalTo(@10);
        }];
    }
    return _enterIv;
}

- (UIImageView *)iconIv{
    if (!_iconIv) {
        _iconIv = [[UIImageView alloc] init];
        _iconIv.image = [UIImage imageNamed:@"redErCode"];
        _iconIv.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iconIv];
        [_iconIv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).offset(-25);
            make.centerY.equalTo(self.mas_centerY);
        }];
    }
    return _iconIv;
}

- (void)setModel:(SLSettingModel *)model{
    _model = model;
    self.enterIv.hidden = !model.hasArrow;
    
    self.titleLabel.text = model.title;
    if ([model.title isEqualToString:@"基本信息"]||[model.title isEqualToString:@"编辑资料"]) {
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = [UIFont systemFontOfSize:17];
    }else{
        self.titleLabel.textColor = UIColorFromRGB(0x614b52);
        self.titleLabel.font = [UIFont systemFontOfSize:15];
    }
    if ([model.title containsString:@"二维码"]) {
        self.iconIv.hidden = NO;
    }else{
        self.iconIv.hidden = YES;
    }
    if (model.subTitle) {
        self.subTitleLabel.text = model.subTitle;
    }
    self.sepeView.frame = CGRectMake(25, 54.5, screen_W-50, 0.5);
}

@end
