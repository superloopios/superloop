//
//  SLRecieveEvaluateTableViewCell.h
//  Superloop
//
//  Created by WangS on 16/10/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLCallComments;
@class SLRecieveEvaluateTableViewCell;
@protocol SLRecieveEvaluateTableViewCellDelegate <NSObject>
//点击图像名字跳转到他人页
- (void)SLRecieveEvaluateTableViewCell:(SLRecieveEvaluateTableViewCell *)recieveEvaluateTableViewCell didSelectAtIndexWithModel:(SLCallComments *)model indexPath:(NSIndexPath *)indexPath;
//点击回复
- (void)SLRecieveEvaluateTableViewCell:(SLRecieveEvaluateTableViewCell *)recieveEvaluateTableViewCell didSelectAtIndexWithReplyModel:(SLCallComments *)model isPhoneOrMsg:(BOOL)isPhoneOrMsg indexPath:(NSIndexPath *)indexPath;
//点击展开UILabel
- (void)SLRecieveEvaluateTableViewCell:(SLRecieveEvaluateTableViewCell *)recieveEvaluateTableViewCell didSelectAtIndexWithLaunchModel:(SLCallComments *)model indexPath:(NSIndexPath *)indexPath;
//点击展开回复评价的UILabel
- (void)SLRecieveEvaluateTableViewCell:(SLRecieveEvaluateTableViewCell *)recieveEvaluateTableViewCell didSelectAtIndexWithReplyLaunchModel:(SLCallComments *)model indexPath:(NSIndexPath *)indexPath;
@end
@interface SLRecieveEvaluateTableViewCell : UITableViewCell
@property (nonatomic, weak) id<SLRecieveEvaluateTableViewCellDelegate>delegate;
@property (nonatomic, strong) SLCallComments *recieveComment;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, strong) NSIndexPath *indexPath;
@end
