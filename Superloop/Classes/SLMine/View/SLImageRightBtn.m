//
//  SLImageRightBtn.m
//  Superloop
//
//  Created by xiaowu on 16/8/15.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLImageRightBtn.h"

@implementation SLImageRightBtn

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //根据计算文字的大小
    CGFloat btnWidth= [self.titleLabel.text boundingRectWithSize:CGSizeMake(ScreenW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.width+2;
    self.titleLabel.width = btnWidth;
    self.titleLabel.x = 0;
    self.imageView.x = self.titleLabel.width + 3;
}

@end
