//
//  SLImageCollectionView.h
//  Superloop
//
//  Created by WangJiwei on 16/5/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLImageCollectionView : UICollectionView
/**
 *  当前的索引
 */
@property (nonatomic,strong) NSIndexPath *indexPath;

/**
 *  tag 当多个CollectionView 时 给CollectionView 添加tag
 */
@property (nonatomic, assign) NSInteger collectionViewTag;
@end
