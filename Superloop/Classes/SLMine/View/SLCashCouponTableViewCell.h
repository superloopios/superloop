//
//  SLCashCouponTableViewCell.h
//  Superloop
//
//  Created by xiaowu on 16/9/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLCashCouponModel;

@interface SLCashCouponTableViewCell : UITableViewCell

@property(nonatomic,strong)SLCashCouponModel *model;

@end
