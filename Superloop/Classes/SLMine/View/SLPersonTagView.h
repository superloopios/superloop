//
//  SLPersonTagView.h
//  Superloop
//
//  Created by xiaowu on 16/8/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLPersonTagView : UIView

@property(nonatomic, strong)NSMutableDictionary *userInfo;

@property (nonatomic ,copy) void(^introduceBlcok)(NSString *str);

@end
