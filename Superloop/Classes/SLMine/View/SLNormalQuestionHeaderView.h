//
//  SLNormalQuestionHeaderView.h
//  Superloop
//
//  Created by WangS on 16/6/6.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLNormalQuestionHeaderView : UIView



@property (weak, nonatomic) IBOutlet UILabel *titleLab;


@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;

@end
