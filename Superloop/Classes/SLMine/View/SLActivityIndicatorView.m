//
//  SLActivityIndicatorView.m
//  Superloop
//
//  Created by xiaowu on 16/9/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLActivityIndicatorView.h"

@interface SLActivityIndicatorView ()

@property(nonatomic, assign)double begintime;
@property(nonatomic, assign)double endtime;

@end

@implementation SLActivityIndicatorView

- (void)stopAnimating{
    
    self.endtime = [[NSDate date] timeIntervalSince1970];
    if (self.endtime-self.begintime < 0.7) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((0.7-(self.endtime-self.begintime)) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [super stopAnimating];
        });
    }else{
        [super stopAnimating];
    }
}
- (void)startAnimating{
    [super startAnimating];
    self.begintime = [[NSDate date] timeIntervalSince1970];
}

@end
