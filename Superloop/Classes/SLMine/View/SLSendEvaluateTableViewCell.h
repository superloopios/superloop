//
//  SLSendEvaluateTableViewCell.h
//  Superloop
//
//  Created by WangS on 16/10/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SLCallComments;
@class SLSendEvaluateTableViewCell;
@protocol SLSendEvaluateTableViewCellDelegate <NSObject>
//点击图像名字跳转到他人页
- (void)SLSendEvaluateTableViewCell:(SLSendEvaluateTableViewCell *)sendEvaluateTableViewCell didSelectAtIndexWithModel:(SLCallComments *)model indexPath:(NSIndexPath *)indexPath;
//点击展开UILabel
- (void)SLSendEvaluateTableViewCell:(SLSendEvaluateTableViewCell *)sendEvaluateTableViewCell didSelectAtIndexWithLaunchModel:(SLCallComments *)model indexPath:(NSIndexPath *)indexPath;
//点击展开回复评价的UILabel
- (void)SLSendEvaluateTableViewCell:(SLSendEvaluateTableViewCell *)sendEvaluateTableViewCell didSelectAtIndexWithReplyLaunchModel:(SLCallComments *)model indexPath:(NSIndexPath *)indexPath;
@end
@interface SLSendEvaluateTableViewCell : UITableViewCell
@property (nonatomic, weak) id<SLSendEvaluateTableViewCellDelegate>delegate;
@property (nonatomic, strong) SLCallComments *sendCallComment;
@property (nonatomic, strong) NSIndexPath *indexPath;
@end
