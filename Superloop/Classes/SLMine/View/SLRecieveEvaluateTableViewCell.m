//
//  SLRecieveEvaluateTableViewCell.m
//  Superloop
//
//  Created by WangS on 16/10/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLRecieveEvaluateTableViewCell.h"
#import "SLCallComments.h"
#import "AppDelegate.h"
#import "NSString+Utils.h"

@interface SLRecieveEvaluateTableViewCell ()

@property (nonatomic,strong) UIImageView *iconImgView;
@property (nonatomic,strong) SLLabel *nameLab;
@property (nonatomic,strong) SLLabel *evaluateTimeLab;
@property (nonatomic,strong) SLLabel *evaluateLab;
@property (nonatomic,strong) SLLabel *contentLab;
@property (nonatomic,strong) UIButton *evaluateLaunchBtn;
@property (nonatomic,strong) UIImageView *lineImgView;
@property (nonatomic,strong) UIImageView *msgOrPhoneImgView;
@property (nonatomic,strong) UIView *startView;
@property (nonatomic,strong) UIButton *replyBtn;
@property (nonatomic,strong) UIView *commentView;
@property (nonatomic,strong) SLLabel *commentLab;
@property (nonatomic,strong) UIButton *commentLaunchBtn;
@property (nonatomic,strong) SLLabel *commentTimeLab;
@property (nonatomic, assign) BOOL isBeyondFive;//超过5行
@property (nonatomic, assign) BOOL isOneLine;//只有1行
@property (nonatomic, assign) BOOL isReplyBeyondFive;//是否是回复评价的超过5行
@property (nonatomic, assign) BOOL isReplyOneLine;//是否是回复评价的只有1行
@property (nonatomic, assign) CGFloat recordReplyHeight;//记录回复评价的高度
@property (nonatomic, assign) BOOL isPhoneOrMsg;//判断是电话还是付费消息
@end
@implementation SLRecieveEvaluateTableViewCell

- (UIImageView *)iconImgView{
    if (!_iconImgView) {
        _iconImgView = [[UIImageView alloc] init];
        _iconImgView.layer.cornerRadius = 6;
        _iconImgView.layer.masksToBounds = YES;
        _iconImgView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
        [_iconImgView addGestureRecognizer:tap];
        [self addSubview:_iconImgView];
    }
    return _iconImgView;
}
- (SLLabel *)nameLab{
    if (!_nameLab) {
        _nameLab = [[SLLabel alloc] init];
        _nameLab.font = [UIFont boldSystemFontOfSize:15];
        _nameLab.textColor = UIColorFromRGB(0x2b2124);
        _nameLab.textAlignment = NSTextAlignmentLeft;
        _nameLab.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
        [_nameLab addGestureRecognizer:tap];
        [self addSubview:_nameLab];
    }
    return _nameLab;
}
- (SLLabel *)evaluateTimeLab{
    if (!_evaluateTimeLab) {
        _evaluateTimeLab = [[SLLabel alloc] init];
        _evaluateTimeLab.font = [UIFont systemFontOfSize:12];
        _evaluateTimeLab.textColor = UIColorFromRGB(0xa7a7a7);
        _evaluateTimeLab.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_evaluateTimeLab];
    }
    return _evaluateTimeLab;
}
- (SLLabel *)evaluateLab{
    if (!_evaluateLab) {
        _evaluateLab = [[SLLabel alloc] init];
        _evaluateLab.font = [UIFont systemFontOfSize:12];
        _evaluateLab.textColor = UIColorFromRGB(0x413548);
        _evaluateLab.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_evaluateLab];
    }
    return _evaluateLab;
}
- (UIView *)startView{
    if (!_startView) {
        _startView = [[UIView alloc] init];
        [self addSubview:_startView];
    }
    return _startView;
}
- (SLLabel *)contentLab{
    if (!_contentLab) {
        _contentLab = [[SLLabel alloc] init];
        _contentLab.font = [UIFont systemFontOfSize:15];
        _contentLab.textColor = UIColorFromRGB(0x747474);
        _contentLab.textAlignment = NSTextAlignmentCenter;
        _contentLab.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(replyClick)];
        [_contentLab addGestureRecognizer:tap];
        [self addSubview:_contentLab];
    }
    return _contentLab;
}
- (UIButton *)evaluateLaunchBtn{
    if (!_evaluateLaunchBtn) {
        _evaluateLaunchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_evaluateLaunchBtn addTarget:self action:@selector(evaluateBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_evaluateLaunchBtn setImage:[UIImage imageNamed:@"bottomRedArrow"] forState:UIControlStateNormal];
        [_evaluateLaunchBtn setImage:[UIImage imageNamed:@"topRedArrow"] forState:UIControlStateSelected];
        [self addSubview:_evaluateLaunchBtn];
    }
    return _evaluateLaunchBtn;
}
- (UIImageView *)msgOrPhoneImgView{
    if (!_msgOrPhoneImgView) {
        _msgOrPhoneImgView = [[UIImageView alloc] init];
        [self addSubview:_msgOrPhoneImgView];
    }
    return _msgOrPhoneImgView;
}
- (UIImageView *)lineImgView{
    if (!_lineImgView) {
        _lineImgView = [[UIImageView alloc] init];
        [self addSubview:_lineImgView];
    }
    return _lineImgView;
}
- (UIButton *)replyBtn{
    if (!_replyBtn) {
        _replyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_replyBtn setTitle:@"回复评价" forState:UIControlStateNormal];
        _replyBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_replyBtn addTarget:self action:@selector(replyClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_replyBtn];
    }
    return _replyBtn;
}
- (UIView *)commentView{
    if (!_commentView) {
        _commentView = [[UIView alloc] init];
        _commentView.backgroundColor = UIColorFromRGB(0xf7f7f7);
        _commentView.layer.cornerRadius = 5;
        _commentView.layer.masksToBounds = YES;
        [self addSubview:_commentView];
    }
    return _commentView;
}
- (SLLabel *)commentLab{
    if (!_commentLab) {
        _commentLab = [[SLLabel alloc] init];
        _commentLab.textAlignment = NSTextAlignmentCenter;
        [self.commentView addSubview:_commentLab];
    }
    return _commentLab;
}
- (UIButton *)commentLaunchBtn{
    if (!_commentLaunchBtn) {
        _commentLaunchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_commentLaunchBtn addTarget:self action:@selector(commentLaunchBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.commentView addSubview:_commentLaunchBtn];
    }
    return _commentLaunchBtn;
}
- (SLLabel *)commentTimeLab{
    if (!_commentTimeLab) {
        _commentTimeLab = [[SLLabel alloc] init];
        _commentTimeLab.font = [UIFont systemFontOfSize:12];
        _commentTimeLab.textColor = UIColorFromRGB(0xa7a7a7);
        _commentTimeLab.textAlignment = NSTextAlignmentRight;
        [self addSubview:_commentTimeLab];
    }
    return _commentTimeLab;
}

-(void)setRecieveComment:(SLCallComments *)recieveComment{
    _recieveComment = recieveComment;

    //时间
    self.evaluateTimeLab.text = recieveComment.rate_time;
    CGFloat evaluateTimeWidth= [recieveComment.rate_time boundingRectWithSize:CGSizeMake(ScreenW-12-32-10-35, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12]} context:nil].size.width+4;
    if ([recieveComment.anonymous_mode integerValue]== 1) {//匿名
        [self.iconImgView removeFromSuperview];
        [self.nameLab removeFromSuperview];
        //时间
        [self.evaluateTimeLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(21);
            make.left.mas_equalTo(self.mas_left).offset(12);
            make.width.mas_equalTo(evaluateTimeWidth);
            make.height.mas_equalTo(12);
        }];
        //评价
        if ([[NSString stringWithFormat:@"%@",self.userId] isEqualToString:[NSString stringWithFormat:@"%@",ApplicationDelegate.userId]]) {
            self.evaluateLab.text = @"匿名评价你";
        }else{
            self.evaluateLab.text = @"匿名评价Ta";
        }
        [self.evaluateLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.evaluateTimeLab.mas_bottom);
            make.left.mas_equalTo(self.evaluateTimeLab.mas_right).offset(4);
            make.width.mas_equalTo(ScreenW-12-32-10-35-evaluateTimeWidth-70);
            make.height.mas_equalTo(12);
        }];
        //星星
        [self.startView removeFromSuperview];
        [self addSubview:self.startView];
        [self.startView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(23);
            make.right.mas_equalTo(self.mas_right).offset(-35);
            make.width.mas_equalTo((11+3)*5);
            make.height.mas_equalTo(10);
        }];
        for (int i=0; i<5; i++) {
            UIImageView  *starImgView = [[UIImageView alloc] initWithFrame:CGRectMake((11+3)*i, 0, 11, 10)];
            starImgView.image = [UIImage imageNamed:@"evaluateStar"];
            [self.startView addSubview:starImgView];
        }
        for (int i=0; i<[recieveComment.rate integerValue]; i++) {
            UIImageView  *colorStarImgView = [[UIImageView alloc] initWithFrame:CGRectMake((11+3)*i, 0, 11, 10)];
            colorStarImgView.image = [UIImage imageNamed:@"evaluateColorStar"];
            [self.startView addSubview:colorStarImgView];
        }
        
    }else{
        [self addSubview:self.iconImgView];
        [self addSubview:self.nameLab];
        //图像
        [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:recieveComment.from_avatar] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
        [self.iconImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(20);
            make.left.mas_equalTo(self.mas_left).offset(12);
            make.width.mas_equalTo(32);
            make.height.mas_equalTo(32);
        }];
        
        //名字
        self.nameLab.text = recieveComment.from_nickname;
        [self.nameLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.iconImgView.mas_top);
            make.left.mas_equalTo(self.iconImgView.mas_right).offset(10);
            make.width.mas_equalTo(ScreenW-12-32-10-100);
            make.height.mas_equalTo(15);
        }];
        //时间
        [self.evaluateTimeLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.iconImgView.mas_bottom);
            make.left.mas_equalTo(self.iconImgView.mas_right).offset(10);
            make.width.mas_equalTo(evaluateTimeWidth);
            make.height.mas_equalTo(12);
        }];
        //评价
        if ([[NSString stringWithFormat:@"%@",self.userId] isEqualToString:[NSString stringWithFormat:@"%@",ApplicationDelegate.userId]]) {
            self.evaluateLab.text = @"评价你";
        }else{
            self.evaluateLab.text = @"评价Ta";
        }
        [self.evaluateLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.iconImgView.mas_bottom);
            make.left.mas_equalTo(self.evaluateTimeLab.mas_right).offset(4);
            make.width.mas_equalTo(ScreenW-12-32-10-35-evaluateTimeWidth-70);
            make.height.mas_equalTo(12);
        }];
        //星星
        [self.startView removeFromSuperview];
        [self addSubview:self.startView];
        [self.startView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(42);
            make.right.mas_equalTo(self.mas_right).offset(-35);
            make.width.mas_equalTo((11+3)*5);
            make.height.mas_equalTo(10);
        }];
        for (int i=0; i<5; i++) {
            UIImageView  *starImgView = [[UIImageView alloc] initWithFrame:CGRectMake((11+3)*i, 0, 11, 10)];
            starImgView.image = [UIImage imageNamed:@"evaluateStar"];
            [self.startView addSubview:starImgView];
        }
        for (int i=0; i<[recieveComment.rate integerValue]; i++) {
            UIImageView  *colorStarImgView = [[UIImageView alloc] initWithFrame:CGRectMake((11+3)*i, 0, 11, 10)];
            colorStarImgView.image = [UIImage imageNamed:@"evaluateColorStar"];
            [self.startView addSubview:colorStarImgView];
        }
        
    }
    //评论的内容
    self.contentLab.numberOfLines = [self calculateRows:recieveComment];
    [self setLabelSpace:self.contentLab withValue:recieveComment.rate_content withFont:[UIFont systemFontOfSize:15]];
    [self.contentLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.evaluateTimeLab.mas_bottom).offset(13);
        make.left.mas_equalTo(self.mas_left).offset(35);
        make.right.mas_equalTo(self.mas_right).offset(-35);
    }];
    
    //分割线
    if ([recieveComment.appraisal_type integerValue] == 1) {
        self.isPhoneOrMsg = YES;//Phone
        self.lineImgView.backgroundColor = UIColorFromRGB(0xff5a5f);
        self.msgOrPhoneImgView.image = [UIImage imageNamed:@"evaluatePhone"];
        [self.replyBtn setTitleColor:UIColorFromRGB(0xff5a5f) forState:UIControlStateNormal];
    }else{
        self.isPhoneOrMsg = NO;//Msg
        self.lineImgView.backgroundColor = UIColorFromRGB(0x999999);
        self.msgOrPhoneImgView.image = [UIImage imageNamed:@"evaluateMsg"];
        [self.replyBtn setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
    }
    
    //行数
    if (self.isBeyondFive) {
        [self addSubview:self.evaluateLaunchBtn];
        if (recieveComment.isClose) {
            self.evaluateLaunchBtn.selected = YES;
        }else{
            self.evaluateLaunchBtn.selected = NO;
        }
        [self.evaluateLaunchBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentLab.mas_bottom).offset(3);
            make.centerX.mas_equalTo(self.mas_centerX);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(30);
        }];
        
        [self.msgOrPhoneImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.evaluateLaunchBtn.mas_bottom).offset(-10);
            make.right.mas_equalTo(self.mas_right).offset(-21);
            make.width.mas_equalTo(26);
            make.height.mas_equalTo(26);
        }];
        [self.lineImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.evaluateLaunchBtn.mas_bottom).offset(3);
            make.left.mas_equalTo(self.mas_left);
            make.right.mas_equalTo(self.msgOrPhoneImgView.mas_left);
            make.height.mas_equalTo(0.5);
        }];
    }else{
        [self.evaluateLaunchBtn removeFromSuperview];
        
        if (self.isOneLine) {
            [self.msgOrPhoneImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.contentLab.mas_bottom).offset(1);
                make.right.mas_equalTo(self.mas_right).offset(-21);
                make.width.mas_equalTo(26);
                make.height.mas_equalTo(26);
            }];
            [self.lineImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.contentLab.mas_bottom).offset(14);
                make.left.mas_equalTo(self.mas_left);
                make.right.mas_equalTo(self.msgOrPhoneImgView.mas_left);
                make.height.mas_equalTo(0.5);
            }];
        }else{
            [self.msgOrPhoneImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.contentLab.mas_bottom).offset(7);
                make.right.mas_equalTo(self.mas_right).offset(-21);
                make.width.mas_equalTo(26);
                make.height.mas_equalTo(26);
            }];
            [self.lineImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.contentLab.mas_bottom).offset(20);
                make.left.mas_equalTo(self.mas_left);
                make.right.mas_equalTo(self.msgOrPhoneImgView.mas_left);
                make.height.mas_equalTo(0.5);
            }];
        }
    }
    //回复评价
    if ([NSString isEmptyStrings:recieveComment.reply_content]) {
        [self.commentTimeLab removeFromSuperview];
        [self.commentView removeFromSuperview];
        if ([[NSString stringWithFormat:@"%@",self.userId] isEqualToString:[NSString stringWithFormat:@"%@",ApplicationDelegate.userId]]) {
            if ([recieveComment.rate_source integerValue] == 1) {
                [self addSubview:self.replyBtn];
                [self.replyBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(self.lineImgView.mas_bottom).offset(15);
                    make.left.mas_equalTo(self.mas_left);
                    make.right.mas_equalTo(self.mas_right);
                    make.height.mas_equalTo(13);
                }];
            }else{
                [self.replyBtn removeFromSuperview];
            }
        }
    }else{
        [self.replyBtn removeFromSuperview];
        [self addSubview:self.commentView];
        self.commentLab.numberOfLines = [self calculateReplyRows:recieveComment];
        [self setReplyLabelSpace:self.commentLab withValue:[NSString stringWithFormat:@"回复:%@",recieveComment.reply_content]];
        //行数
        if (self.isReplyBeyondFive) {
            [self.commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.lineImgView.mas_bottom).offset(15);
                make.left.mas_equalTo(self.mas_left).offset(35);
                make.right.mas_equalTo(self.mas_right).offset(-35);
                make.height.mas_equalTo(self.recordReplyHeight+37);
            }];
            
            [self.commentView addSubview:self.commentLaunchBtn];
            [self.commentLaunchBtn setImage:[UIImage imageNamed:@"bottomRedArrow"] forState:UIControlStateNormal];
            [self.commentLaunchBtn setImage:[UIImage imageNamed:@"topRedArrow"] forState:UIControlStateSelected];
            if (recieveComment.isReplyClose) {
                self.commentLaunchBtn.selected = YES;
            }else{
                self.commentLaunchBtn.selected = NO;
            }
            [self.commentLaunchBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(self.commentView.mas_bottom).offset(-3);
                make.centerX.mas_equalTo(self.mas_centerX);
                make.width.mas_equalTo(100);
                make.height.mas_equalTo(20);
            }];
        }else{
            [self.commentLaunchBtn removeFromSuperview];
            if (self.isReplyOneLine) {
                [self.commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(self.lineImgView.mas_bottom).offset(15);
                    make.left.mas_equalTo(self.mas_left).offset(35);
                    make.right.mas_equalTo(self.mas_right).offset(-35);
                    make.height.mas_equalTo(self.recordReplyHeight+30-8);
                }];
            }else{
                [self.commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(self.lineImgView.mas_bottom).offset(15);
                    make.left.mas_equalTo(self.mas_left).offset(35);
                    make.right.mas_equalTo(self.mas_right).offset(-35);
                    make.height.mas_equalTo(self.recordReplyHeight+30);
                }];
            }
        }
        [self.commentLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.commentView.mas_top).offset(15);
            make.left.mas_equalTo(self.commentView.mas_left).offset(15);
            make.right.mas_equalTo(self.commentView.mas_right).offset(-15);
            make.height.mas_equalTo(self.recordReplyHeight);
        }];
        self.commentTimeLab.text = recieveComment.reply_time;
        [self addSubview:self.commentTimeLab];
        [self.commentTimeLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.commentView.mas_bottom).offset(15);
            make.right.mas_equalTo(self.mas_right).offset(-35);
            make.width.mas_equalTo(ScreenW-100);
            make.height.mas_equalTo(12);
        }];
    }
}
- (NSInteger)calculateRows:(SLCallComments *)model{
    CGFloat standardHeight = [self getSpaceLabelHeight:@"高度，高度" withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-70];
    CGFloat titleHeight = [self getSpaceLabelHeight:model.rate_content withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-70];
    NSInteger count = titleHeight/standardHeight;
    if (titleHeight/standardHeight>count) {
        count +=1;
    }
    if (count > 5) {
        self.isBeyondFive = YES;
        if (!model.isClose) {
            return 5;
        }else{
            return count;
        }
    }else{
        if (count == 1) {
            self.isOneLine = YES;
        }else{
            self.isOneLine = NO;
        }
        self.isBeyondFive = NO;
        return 5;
    }
}
- (NSInteger)calculateReplyRows:(SLCallComments *)model{
    CGFloat standardHeight = [self getSpaceLabelHeight:@"高度，高度" withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-100];
    CGFloat titleHeight = [self getSpaceLabelHeight:model.reply_content withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-100];
    NSInteger count = titleHeight/standardHeight;
    if (titleHeight/standardHeight>count) {
        count +=1;
    }
    if (count > 5) {
        self.isReplyBeyondFive = YES;
        if (!model.isReplyClose) {
            self.recordReplyHeight = standardHeight * 5;
            return 5;
        }else{
            self.recordReplyHeight = standardHeight * count;
            return count;
        }
    }else{
        if (count == 1) {
            self.isReplyOneLine = YES;
        }else{
            self.isReplyOneLine = NO;
        }
        self.isReplyBeyondFive = NO;
        self.recordReplyHeight = standardHeight * count;
        return 5;
    }
}

//回复评价设置行间距和字间距
- (void)setReplyLabelSpace:(UILabel*)label withValue:(NSString*)str {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = 8; //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSMutableAttributedString * mAttibuteStr = [[NSMutableAttributedString alloc] initWithString:str];
    [mAttibuteStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, 3)];
    
    if ([self.recieveComment.appraisal_type integerValue] == 1) {
        [mAttibuteStr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xff5a5f) range:NSMakeRange(0, 3)];
    }else{
        [mAttibuteStr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x81b888) range:NSMakeRange(0, 3)];
    }
    [mAttibuteStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(3, str.length-3)];
    [mAttibuteStr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x747474) range:NSMakeRange(3, str.length-3)];
    [mAttibuteStr addAttribute:NSParagraphStyleAttributeName value:paraStyle range:NSMakeRange(0, str.length-1)];
    [mAttibuteStr addAttribute:NSKernAttributeName value:@1.5f range:NSMakeRange(0, str.length-1)];
    label.attributedText = mAttibuteStr;
}
//给UILabel设置行间距和字间距
- (void)setLabelSpace:(UILabel*)label withValue:(NSString*)str withFont:(UIFont*)font {
    if (str) {
        NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
        paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
        paraStyle.alignment = NSTextAlignmentLeft;
        paraStyle.lineSpacing = 8; //设置行间距
        paraStyle.hyphenationFactor = 1.0;
        paraStyle.firstLineHeadIndent = 0.0;
        paraStyle.paragraphSpacingBefore = 0.0;
        paraStyle.headIndent = 0;
        paraStyle.tailIndent = 0;
        //设置字间距 NSKernAttributeName:@1.5f
        NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f
                              };
        NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
        label.attributedText = attributeStr;  
    }
    
}

//计算UILabel的高度(带有行间距的情况)
- (CGFloat)getSpaceLabelHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = 8;
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f
                          };
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return size.height;
}
- (void)setFrame:(CGRect)frame{
    frame.size.height -= 15;
    [super setFrame:frame];
}
- (void)commentLaunchBtnClick{
    self.recieveComment.isReplyClose = !self.recieveComment.isReplyClose;
    SLCallComments * model = self.recieveComment;
    if ([self.delegate respondsToSelector:@selector(SLRecieveEvaluateTableViewCell:didSelectAtIndexWithReplyLaunchModel:indexPath:)]) {
        [self.delegate SLRecieveEvaluateTableViewCell:self didSelectAtIndexWithReplyLaunchModel:model indexPath:self.indexPath];
    }
}
- (void)evaluateBtnClick{
    self.recieveComment.isClose = !self.recieveComment.isClose;
    SLCallComments * model = self.recieveComment;
    if ([self.delegate respondsToSelector:@selector(SLRecieveEvaluateTableViewCell:didSelectAtIndexWithLaunchModel:indexPath:)]) {
        [self.delegate SLRecieveEvaluateTableViewCell:self didSelectAtIndexWithLaunchModel:model indexPath:self.indexPath];
    }
}
- (void)tapClick{
    if ([self.delegate respondsToSelector:@selector(SLRecieveEvaluateTableViewCell:didSelectAtIndexWithModel:indexPath:)]) {
        [self.delegate SLRecieveEvaluateTableViewCell:self didSelectAtIndexWithModel:self.recieveComment indexPath:self.indexPath];
    }
}
- (void)replyClick{
    if ([[NSString stringWithFormat:@"%@",self.userId] isEqualToString:[NSString stringWithFormat:@"%@",ApplicationDelegate.userId]]) {
        if ([self.recieveComment.rate_source integerValue] == 1) {
            if ([NSString isEmptyStrings:self.recieveComment.reply_content]) {
                if ([self.delegate respondsToSelector:@selector(SLRecieveEvaluateTableViewCell:didSelectAtIndexWithReplyModel:isPhoneOrMsg:indexPath:)]) {
                    [self.delegate SLRecieveEvaluateTableViewCell:self didSelectAtIndexWithReplyModel:self.recieveComment isPhoneOrMsg:self.isPhoneOrMsg indexPath:self.indexPath];
                }
            }else{
               // [HUDManager showWarningWithText:@"评论只能回复一次"];
            }
        }else{
           // [HUDManager showWarningWithText:@"系统默认评论不能被回复"];
        }
    }
}
@end
