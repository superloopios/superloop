//
//  SLEducationViewCell.h
//  Superloop
//
//  Created by WangJiwei on 16/4/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLEducationViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *positionLabConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationLabConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *cutImgVIew;


@end
