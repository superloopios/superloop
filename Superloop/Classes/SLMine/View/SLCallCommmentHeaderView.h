//
//  SLCallCommmentHeaderView.h
//  Superloop
//
//  Created by WangS on 16/8/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLCallCommmentHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIView *starBackView;

@property (weak, nonatomic) IBOutlet UILabel *pointLab;

@end
