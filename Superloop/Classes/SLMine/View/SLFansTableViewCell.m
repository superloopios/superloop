//
//  SLFansTableViewCell.m
//  Superloop
//
//  Created by xiaowu on 16/11/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLFansTableViewCell.h"
#import "SLFans.h"
#import "NSString+Utils.h"

@interface SLFansTableViewCell ()

@property(nonatomic, strong)UIImageView *avatarImageView;
@property(nonatomic, strong)SLLabel *nickNameLabel;
@property(nonatomic, strong)SLLabel *bioLabel;
@property(nonatomic, strong)UIView *sepatorView;
@property(nonatomic, strong)UIButton *followBtn;

@end

@implementation SLFansTableViewCell

- (UIImageView *)avatarImageView{
    if (!_avatarImageView) {
        _avatarImageView = [[UIImageView alloc] init];
        _avatarImageView.layer.cornerRadius = 5;
        _avatarImageView.clipsToBounds = YES;
        [self.contentView addSubview:_avatarImageView];
        [_avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(11);
            make.left.equalTo(self).offset(10);
            make.width.height.mas_equalTo(48);
        }];
    }
    return _avatarImageView;
}

- (SLLabel *)nickNameLabel{
    if (!_nickNameLabel) {
        _nickNameLabel = [[SLLabel alloc] init];
        _nickNameLabel.font = [UIFont systemFontOfSize:17];
        _nickNameLabel.textColor = UIColorFromRGB(0x2b2124);
        [self.contentView addSubview:_nickNameLabel];
        [_nickNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.avatarImageView).offset(2);
            make.left.equalTo(self.avatarImageView.mas_right).offset(10);
            make.width.mas_equalTo(screen_W-10-10-48-70);
        }];
//        _nickNameLabel.backgroundColor = [UIColor redColor];
    }
    return _nickNameLabel;
}

- (SLLabel *)bioLabel{
    if (!_bioLabel) {
        _bioLabel = [[SLLabel alloc] init];
        _bioLabel.font = [UIFont systemFontOfSize:13];
        _bioLabel.textColor = UIColorFromRGB(0xb2b1b1);
        [self.contentView addSubview:_bioLabel];
        [_bioLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.nickNameLabel.mas_bottom).offset(8);
            make.left.equalTo(self.nickNameLabel);
            make.width.mas_equalTo(screen_W-10-10-48-70);
        }];
//        _bioLabel.backgroundColor = [UIColor redColor];
    }
    return _bioLabel;
}

- (UIView *)sepatorView{
    if (!_sepatorView) {
        _sepatorView = [[UIView alloc] init];
        [self.contentView addSubview:_sepatorView];
        [_sepatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.contentView.mas_bottom);
            make.left.right.equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
    }
    return _sepatorView;
}

- (UIButton *)followBtn{
    if (!_followBtn) {
        _followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_followBtn addTarget:self action:@selector(btnClck:) forControlEvents:UIControlEventTouchUpInside];
        [_followBtn setTitle:@"+ 关注" forState:UIControlStateNormal];
        [_followBtn setTitle:@"已关注" forState:UIControlStateSelected];
        [_followBtn setTitleColor:SLMainColor forState:UIControlStateNormal];
        [_followBtn setTitleColor:UIColorFromRGB(0xb2b1b1) forState:UIControlStateSelected];
        
        [self.contentView addSubview:_followBtn];
        [_followBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.right.equalTo(self.contentView.mas_right).offset(-10);
            make.height.mas_equalTo(40);
            make.width.equalTo(@60);
        }];
//        _followBtn.backgroundColor = [UIColor redColor];
    }
    return _followBtn;
}

- (void)setFanModel:(SLFans *)fanModel{
    _fanModel = fanModel;
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:fanModel.avatar] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
    self.nickNameLabel.text = fanModel.nickname;
    
    if ([NSString isRealString:fanModel.bio]) {
        self.bioLabel.text = fanModel.bio;
    }else{
        self.bioLabel.text=@"这个人很懒,什么也没写";
    }
    self.sepatorView.backgroundColor = SLColor(224, 224, 224);
    if ([fanModel.following isEqualToString:@"0"]){
        self.followBtn.selected = NO;
        self.followBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    }else{
        self.followBtn.selected = YES;
        self.followBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    }
    if (!fanModel.following) {
        self.followBtn.selected = NO;
        self.followBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    }
}

- (void)btnClck:(id)sender {
    
    if([self.delegate respondsToSelector:@selector(didFcsButtonClicked:indexPath:)]) {
        [self.delegate didFcsButtonClicked:sender indexPath:self.indexpath];
    }
    
}


@end
