//
//  SLTradeRecordWithVoucherTableViewCell.h
//  Superloop
//
//  Created by WangS on 16/9/17.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLTradeRecoredModel;

@interface SLTradeRecordWithVoucherTableViewCell : UITableViewCell
@property (nonatomic,strong) SLTradeRecoredModel *tradeModel;
@end
