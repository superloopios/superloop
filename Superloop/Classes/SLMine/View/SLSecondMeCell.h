//
//  SLSecondMeCell.h
//  Superloop
//
//  Created by xiaowu on 16/8/17.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLSettingModel;

@interface SLSecondMeCell : UITableViewCell

@property(nonatomic, strong)SLSettingModel *model;

@end
