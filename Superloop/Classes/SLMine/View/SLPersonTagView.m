//
//  SLPersonTagView.m
//  Superloop
//
//  Created by xiaowu on 16/8/16.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLPersonTagView.h"
#import "NSString+Utils.h"
#import "UILabel+StringFrame.h"
#import "SLTagLayout.h"
#import "SLPersonTagsCollectionViewCell.h"
#import "SLTabBarViewController.h"
#import "SLIntroduceViewController.h"
#import "SLMoreCategoryViewController.h"

@interface SLPersonTagView ()<UICollectionViewDataSource,UICollectionViewDelegate>
//简介的label
@property(nonatomic, strong)UILabel *introlabel;
//简介
@property (nonatomic, strong)UILabel *introduceLabel;

//主标签label
@property (nonatomic, strong)UILabel *majorLabel;
//付标签label
@property (nonatomic, strong)UILabel *secondLabel;

@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic, strong)SLTagLayout *layout;

@end

@implementation SLPersonTagView

- (UILabel *)secondLabel{
    if (!_secondLabel) {
        _secondLabel = [[UILabel alloc] init];
        
        _secondLabel.font = [UIFont boldSystemFontOfSize:14];
        _secondLabel.textColor = SLColor(119, 119, 119);
        [self addSubview:_secondLabel];
        [_secondLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.collectionView.mas_bottom).offset(15);
            make.left.equalTo(self).offset(24);
            make.height.equalTo(@14);
        }];
    }
    return _secondLabel;
}
- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        SLTagLayout *layout = [[SLTagLayout alloc]init];
        self.layout = layout;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        [self addSubview:_collectionView];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor redColor];
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SLPersonTagsCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"persontagLabelCell"];
    
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseprimaryTags)];
        [_collectionView addGestureRecognizer:ges];
    }
    return _collectionView;
}
- (void)chooseprimaryTags{
    
    SLMoreCategoryViewController *SLMoreCategoryVC = [[SLMoreCategoryViewController alloc] init];
    SLMoreCategoryVC.tagsType = @1;
    SLMoreCategoryVC.changeTags = ^(NSNumber *type,NSArray *_primaryarr){
//        NSLog(@"1111111111%@",_primaryarr);
        self.userInfo[@"primary_tags"] = _primaryarr;
        
//        NSLog(@"%@",self.userInfo[@"primary_tags"]);
        NSMutableArray *primary_arr = [NSMutableArray array];
        for (NSDictionary *dict in _primaryarr) {
            [primary_arr addObject:dict[@"name"]];
        }
//        self.collectionView.frame = CGRectMake(24, 9+14+8+22+14+8+introSize.height, screen_W-48, [self allocTags:primary_arr]);
        self.collectionView.height = [self allocTags:primary_arr];
        [self.collectionView reloadData];
        
    };
    UINavigationController *selectedController;
    
    selectedController = ((SLTabBarViewController *)[UIApplication sharedApplication].keyWindow.rootViewController).selectedViewController;
    SLMoreCategoryVC.userData = self.userInfo;
    [selectedController pushViewController:SLMoreCategoryVC animated:YES];
}

- (UILabel *)majorLabel{
    if (!_majorLabel) {
        _majorLabel = [[UILabel alloc] init];
        
        _majorLabel.font = [UIFont boldSystemFontOfSize:14];
        _majorLabel.textColor = SLColor(119, 119, 119);
        [self addSubview:_majorLabel];
        [_majorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.introduceLabel.mas_bottom).offset(20);
            make.left.equalTo(self).offset(24);
            make.height.equalTo(@14);
        }];
    }
    return _majorLabel;
}
- (UILabel *)introduceLabel{
    if (!_introduceLabel) {
        _introduceLabel = [[UILabel alloc] init];
        _introduceLabel.font = [UIFont systemFontOfSize:14];
        _introduceLabel.textColor = SLColor(84, 84, 84);
        _introduceLabel.numberOfLines = 0;
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enterBio)];
        _introduceLabel.userInteractionEnabled = YES;
        [_introduceLabel addGestureRecognizer:ges];
        [self addSubview:_introduceLabel];
    }
    return _introduceLabel;
}
- (void)enterBio{
    if (self.introduceBlcok) {
        self.introduceBlcok(@"changBio");
    }
    UINavigationController *selectedController;
    
    selectedController = ((SLTabBarViewController *)[UIApplication sharedApplication].keyWindow.rootViewController).selectedViewController;
    
    SLIntroduceViewController *introduce = [[SLIntroduceViewController alloc] init];
    introduce.intro = self.userInfo[@"bio"];
    __weak typeof(self) weakSelf = self;
    introduce.introduceBlcok = ^(NSString *str){
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.userInfo setValue:str forKey:@"bio"];
        if ([NSString isEmptyStrings:str]) {
            strongSelf.introduceLabel.text = @"您还没有填写简介";
        }else{
            strongSelf.introduceLabel.text = str;
        }
        
        CGSize introSize = [strongSelf.introduceLabel boundingRectWithString:str withSize:CGSizeMake(screen_W-48, MAXFLOAT)];
        strongSelf.introduceLabel.height = introSize.height;
//        [strongSelf calHeight:introSize.height];
        NSMutableArray *primary_arr = [NSMutableArray array];
        for (NSDictionary *dict in strongSelf.userInfo[@"primary_tags"]) {
            [primary_arr addObject:dict[@"name"]];
        }
        strongSelf.collectionView.frame = CGRectMake(24, 9+14+8+22+14+8+introSize.height, screen_W-48, [strongSelf allocTags:primary_arr]);
       
        //        [self.majorTableView reloadData];
    };
    [selectedController pushViewController:introduce animated:YES];
    
}

- (UILabel *)introlabel{
    if (!_introlabel) {
        _introlabel = [[UILabel alloc] init];
        _introlabel.font = [UIFont boldSystemFontOfSize:14];
        _introlabel.textColor = [UIColor blackColor];
        _introlabel.frame = CGRectMake(12, 9, 30, 14);
        [self addSubview:_introlabel];
    }
    return _introlabel;
}



- (void)setUserInfo:(NSMutableDictionary *)userInfo{
    _userInfo = userInfo;
    self.height = [self calAllViewHeight];
    self.backgroundColor = [UIColor grayColor];
    self.introlabel.text = @"简介";
    if ([NSString isEmptyStrings:self.userInfo[@"bio"]]) {
         self.introduceLabel.text = @"您还没有填写简介";
    }else{
         self.introduceLabel.text = self.userInfo[@"bio"];
    }
    CGSize introSize = [self.introduceLabel boundingRectWithString:self.introduceLabel.text withSize:CGSizeMake(screen_W-48, MAXFLOAT)];
    self.introduceLabel.frame = CGRectMake(24, CGRectGetMaxY(self.introlabel.frame)+8, screen_W-48, introSize.height);
    self.majorLabel.text = @"主标签";
    
    NSMutableArray *primary_arr = [NSMutableArray array];
    for (NSDictionary *dict in self.userInfo[@"primary_tags"]) {
        [primary_arr addObject:dict[@"name"]];
    }
    self.collectionView.frame = CGRectMake(24, 9+14+8+22+14+8+introSize.height, screen_W-48, [self allocTags:primary_arr]);
    self.secondLabel.text = @"副标签";

}
//- (CGFloat)calHeight{
//    
//}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.collectionView) {
        NSArray *arr = self.userInfo[@"primary_tags"];
        return arr.count;
    }else{
        NSArray *arr = self.userInfo[@"secondary_tags"];
        return arr.count;
    }
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:13]};
    NSString *title = [self removeStringSpace:self.userInfo[@"primary_tags"][indexPath.row][@"name"]];
    CGSize titlesize = [title boundingRectWithSize:CGSizeMake(screen_W-48, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
    return CGSizeMake(titlesize.width+24, 25);
}
//每个UICollectionView展s示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SLPersonTagsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"persontagLabelCell" forIndexPath:indexPath];
    cell.cTitle = [self removeStringSpace:self.userInfo[@"primary_tags"][indexPath.row][@"name"]];
    return cell;
    
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}
#pragma 去掉空格换行
- (NSString *)removeStringSpace:(NSString *)str{
    NSString *headerData = str;
    headerData = [headerData stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return headerData;
}

- (CGFloat)allocTags:(NSArray *)arr{
    
    CGFloat w = 0;//保存前一个label的宽以及前一个label距离屏幕边缘的距离
    CGFloat h = 25;//用来控制label距离父视图的高
    //    return 100;
    for (int i = 0; i < arr.count; i++) {
        
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:13]};
        CGSize titlesize = [arr[i] boundingRectWithSize:CGSizeMake(screen_W-48, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        CGFloat length = titlesize.width;
        //当label的位置超出屏幕边缘时换行  只是label所在父视图的宽度
        if( w + length + 24 > screen_W-48){
            w = 0; //换行时将w置为0
            h = h + 25 + 5;//距离父视图也变化
        }
        w = w + length + 24 +10;
    }
    return h;
}
- (CGFloat)calAllViewHeight{
    
    CGFloat height = 9+14+8+22+15+8+8+30;
    
    if ([[self.userInfo allKeys] containsObject:@"primary_tags"]) {
        NSMutableArray *primary_arr = [NSMutableArray array];
        for (NSDictionary *dict in self.userInfo[@"primary_tags"]) {
            [primary_arr addObject:dict[@"name"]];
        }
        height += [self allocTags:primary_arr];
    }else{
        height += 25;
    }
    
    if ([[self.userInfo allKeys] containsObject:@"secondary_tags"]) {
        NSMutableArray *second_arr = [NSMutableArray array];
        for (NSDictionary *dict in self.userInfo[@"secondary_tags"]) {
            [second_arr addObject:dict[@"name"]];
        }
        height += [self allocTags:second_arr];
    }else{
        height += 25;
    }
    if ([NSString isEmptyStrings:self.userInfo[@"bio"]]) {
        height+= 15;
    }else{
        CGSize introSize = [self.introduceLabel boundingRectWithString:self.introduceLabel.text withSize:CGSizeMake(screen_W-48, MAXFLOAT)];
        height+= introSize.height;
    }
    
    return height;
}
@end
