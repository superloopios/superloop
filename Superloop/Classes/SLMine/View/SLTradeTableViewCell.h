//
//  SLTradeTableViewCell.h
//  Superloop
//
//  Created by 张梦川 on 16/5/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//  交易记录

#import <UIKit/UIKit.h>

@interface SLTradeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLable;
@property (weak, nonatomic) IBOutlet UILabel *moneyLable;
@property (weak, nonatomic) IBOutlet UILabel *type_descLable;

@property (weak, nonatomic) IBOutlet UIImageView *rightImageView;

@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineViewContract;


@end
