//
//  SLCheckImageCell.m
//  Superloop
//
//  Created by WangJiwei on 16/5/9.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCheckImageCell.h"

@implementation SLCheckImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
   
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
}
- (IBAction)cancelImage:(id)sender {
    if ([_Delegate respondsToSelector:@selector(didCancelClick:collectionViewIndexPath:)]) {
        [_Delegate didCancelClick:_indexPath collectionViewIndexPath:_collectionViewIndexPath];
    }
}

@end
