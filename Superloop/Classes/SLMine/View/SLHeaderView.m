//
//  SLHeaderView.m
//  Superloop
//
//  Created by xiaowu on 16/8/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLHeaderView.h"
#import <UIImageView+WebCache.h>
#import <UIButton+WebCache.h>
#import "SLImageRightBtn.h"
#import "HMSegmentedControl.h"
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
#import "NSString+Utils.h"
#import "ColorUtility.h"
#import "NSString+HCD.h"
#import "HcdLoaderURLConnection.h"

#define VIDEO_CACHE @"videoCache"


@interface SLHeaderView ()<UIAlertViewDelegate,HCDLoaderURLConnectionDelegate>{
    //视频播放
    CGFloat     volume;
    BOOL        isAutomaticPlay;    //自动播放
    BOOL        openVoice;
    BOOL        loadPlayItemSuccess;
    BOOL        isRemoveKVO;        //是否需要移除KVO
    BOOL        isPlay;             //是否播放
    BOOL        isPauseBecomeActive;//后台进入前台
    BOOL        isPlaying;          //进入后台时正在播放
    CMTime      recordTime;         //进入后台播放时间
    CGFloat     loadProgress;
    BOOL        recordNetStatus;    //记录网络状态
    BOOL        FirstClickPlayBtn;  //是不是首次点击播放
    BOOL        isAlwayHidden;
}

@property (nonatomic, strong) UIImageView               *roleImageView;         //特权
@property (nonatomic, strong) UIImageView               *renZhengImageView;     //认证
@property (nonatomic, strong) UIView                    *xingView;
@property (nonatomic, strong) NSMutableArray            *xingArr;
@property (nonatomic,   copy) NSString                  *oldAvatarString;
@property (nonatomic,   copy) NSString                  *oldBackgroundString;
@property (nonatomic,   copy) NSString                  *oldVideoString;
@property (nonatomic, strong) UIButton                  *followBtn;
@property (nonatomic, strong) UIButton                  *followerBtn;
@property (nonatomic, strong) UIImageView               *otherBtnBackImageView;
@property (nonatomic, strong) UIButton                  *closeVoiceBtn;
@property (nonatomic, strong) UIButton                  *changeCoverBtn;
@property (nonatomic, strong) UIButton                  *playImgBtn;
@property (nonatomic, strong) AVPlayer                  *player;
@property (nonatomic, strong) AVPlayerLayer             *playerLayer;
@property (nonatomic, strong) AVPlayerItem              *playerItem;
@property (nonatomic, assign) CGFloat                   angle;
@property (nonatomic, strong) UIImageView               *loadingImageView;
@property (nonatomic, assign) CGAffineTransform         firstTransform;
@property (nonatomic, assign) BOOL                      loadSource;
@property (nonatomic, strong) UIAlertView               *alert;//网络提醒
@property (nonatomic, strong) HcdLoaderURLConnection    *resouerLoader;
@property (nonatomic, strong) AVURLAsset                *videoURLAsset;
@property (nonatomic, strong) AVPlayerItem              *currentPlayerItem;
@property (nonatomic, strong) AVAsset                   *videoAsset;

@end

@implementation SLHeaderView

- (UIAlertView *)alert{
    if (!_alert) {
        _alert = [[UIAlertView alloc] initWithTitle:@"您正在使用移动网络播放视频，将会消耗您的流量，是否继续？" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"继续播放", nil];
        
    }
    return _alert;
}

- (UIImageView *)loadingImageView{
    if (!_loadingImageView) {
        _loadingImageView = [[UIImageView alloc] init];
        _loadingImageView.image = [UIImage imageNamed:@"loadingVideo"];
        [self.videoHeaderView addSubview:_loadingImageView];
        
        [_loadingImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_top).offset(ceilf(3/4.0 * ScreenW)*0.5);
        }];
    }
    return _loadingImageView;
}

- (UIButton *)playImgBtn{
    if (!_playImgBtn) {
        _playImgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playImgBtn setBackgroundImage:[UIImage imageNamed:@"videoPlay"] forState:UIControlStateNormal];
        [_playImgBtn addTarget:self action:@selector(pressPlayButton) forControlEvents:UIControlEventTouchUpInside];
        
        
        [self addSubview:_playImgBtn];
        
        [_playImgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_top).offset(ceilf(3/4.0 * ScreenW)*0.5);
        }];
        _playImgBtn.hidden = YES;
    }
    return _playImgBtn;
}

- (UIButton *)closeVoiceBtn{
    if (!_closeVoiceBtn) {
        _closeVoiceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeVoiceBtn setImage:[UIImage imageNamed:@"voiceOpen"] forState:UIControlStateNormal];
        [_closeVoiceBtn setImage:[UIImage imageNamed:@"closeVoice"] forState:UIControlStateSelected];
        [self.otherBtnBackImageView addSubview:_closeVoiceBtn];
        [_closeVoiceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.otherBtnBackImageView).offset(0.5);
            make.bottom.equalTo(self.otherBtnBackImageView).offset(-0.5);
            make.width.equalTo(@(36));
            make.right.equalTo(self.otherBtnBackImageView.mas_right);
        }];
        _closeVoiceBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 6);
    }
    return _closeVoiceBtn;
}

- (UIButton *)changeCoverBtn{
    if (!_changeCoverBtn) {
        _changeCoverBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_changeCoverBtn setImage:[UIImage imageNamed:@"changBackImg"] forState:UIControlStateNormal];
        [self.otherBtnBackImageView addSubview:_changeCoverBtn];
        [_changeCoverBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.otherBtnBackImageView).offset(0.5);
            make.bottom.equalTo(self.otherBtnBackImageView).offset(-0.5);
            make.width.equalTo(@(44));
            make.left.equalTo(self.otherBtnBackImageView.mas_left);
        }];
    }
    return _changeCoverBtn;
}

- (UIImageView *)otherBtnBackImageView{
    if (!_otherBtnBackImageView) {
        _otherBtnBackImageView = [[UIImageView alloc] init];
        _otherBtnBackImageView.image = [UIImage imageNamed:@"blackBackImg"];
        
        _otherBtnBackImageView.userInteractionEnabled = YES;
    }
    return _otherBtnBackImageView;
}

- (NSMutableArray *)xingArr{
    if (!_xingArr) {
        _xingArr = [NSMutableArray array];
    }
    return _xingArr;
}

- (HMSegmentedControl *)segCtrl{
    if (!_segCtrl) {
        _segCtrl = [[HMSegmentedControl alloc] init];
        [self addSubview:_segCtrl];
    }
    return _segCtrl;
}

- (UIButton *)guanzhuBtn{
    if (!_guanzhuBtn) {
        _guanzhuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_guanzhuBtn setTitleColor:[ColorUtility colorWithHexString:@"ffa600"] forState:UIControlStateNormal];
        _guanzhuBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_guanzhuBtn];
    }
    return _guanzhuBtn;
}
- (UIButton *)followBtn{
    if (!_followBtn) {
        _followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_followBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _followBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_followBtn];
        [self.followBtn setTitle:@"关注" forState:UIControlStateNormal];
        [_followBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.guanzhuBtn.mas_left);
            make.centerY.height.equalTo(self.guanzhuBtn);
        }];
    }
    return _followBtn;
}
- (UIButton *)followerBtn{
    if (!_followerBtn) {
        _followerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_followerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _followerBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_followerBtn];
        [self.followerBtn setTitle:@"粉丝" forState:UIControlStateNormal];
        [_followerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.avatarbtn.mas_centerY).offset(-5);
            make.left.equalTo(self.avatarbtn.mas_right).offset(15);
            make.height.equalTo(@40);
        }];
    }
    return _followerBtn;
}

- (UIButton *)fensiBtn{
    if (!_fensiBtn) {
        _fensiBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_fensiBtn setTitleColor:[ColorUtility colorWithHexString:@"ff4e00"] forState:UIControlStateNormal];
        _fensiBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_fensiBtn];
    }
    return _fensiBtn;
}

- (UIButton *)reZhengbtn{
    if (!_reZhengbtn) {
        _reZhengbtn = [[SLImageRightBtn alloc] init];
        [self addSubview:_reZhengbtn];
        _reZhengbtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _reZhengbtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        [_reZhengbtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.xingView.mas_centerY).offset(1);
            make.left.equalTo(self.mas_centerX).offset(6.5);
            //            make.height.equalTo(@(12));
        }];
        
    }
    return _reZhengbtn;
}

- (UIImageView *)sexImageView{
    if (!_sexImageView) {
        _sexImageView = [[UIImageView alloc] init];
        [self addSubview:_sexImageView];
        [_sexImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.nickNamelabel.mas_centerY);
            make.left.equalTo(self.nickNamelabel.mas_right).offset(8);
        }];
    }
    return _sexImageView;
}

- (SLLabel *)nickNamelabel{
    if (!_nickNamelabel) {
        _nickNamelabel = [[SLLabel alloc] init];
        [self addSubview:_nickNamelabel];
        _nickNamelabel.font = [UIFont boldSystemFontOfSize:18];
        _nickNamelabel.textColor = [UIColor blackColor];
        _nickNamelabel.textAlignment = NSTextAlignmentCenter;
    }
    return _nickNamelabel;
}

- (UIImageView *)renZhengImageView{
    if (!_renZhengImageView) {
        _renZhengImageView = [[UIImageView alloc] init];
        _renZhengImageView.image = [UIImage imageNamed:@"identification"];
        [self addSubview:_renZhengImageView];
        [_renZhengImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.avatarbtn.mas_bottom).offset(-5);
            make.centerX.equalTo(self.avatarbtn.mas_right).offset(-5);
        }];
    }
    return _renZhengImageView;
}
- (UIImageView *)roleImageView{
    if (!_roleImageView) {
        _roleImageView = [[UIImageView alloc] init];
        _roleImageView.image = [UIImage imageNamed:@"tiltCrown"];
        [self addSubview:_roleImageView];
        [_roleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.avatarbtn.mas_top).offset(5);
            make.centerX.equalTo(self.avatarbtn.mas_left).offset(5);
        }];
    }
    return _roleImageView;
}

- (UIButton *)avatarbtn{
    if (!_avatarbtn) {
        _avatarbtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_avatarbtn addTarget:self action:@selector(avatarChange:) forControlEvents:UIControlEventTouchUpInside];
        [_avatarbtn setBackgroundImage:[UIImage imageNamed:@"newPersonAvatar"] forState:UIControlStateNormal];
        NSNumber* width = 0;
        width = @72;
        _avatarbtn.layer.cornerRadius = [width doubleValue]/2.0;
        _avatarbtn.clipsToBounds = YES;
    }
    return _avatarbtn;
}

- (UIImageView *)backImageView{
    if (!_backImageView) {
        CGRect frame = self.frame;
        frame.origin.y -= 80;
        frame.size.height = ceilf(3/4.0 * ScreenW)+80;
        _backImageView = [[UIImageView alloc] initWithFrame:frame];
        _backImageView.contentMode = UIViewContentModeScaleAspectFill;
        _backImageView.clipsToBounds = YES;
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changebackImage)];
        _backImageView.userInteractionEnabled = YES;
        [_backImageView addGestureRecognizer:ges];
        [self addSubview:_backImageView];
    }
    return _backImageView;
}
- (void)changebackImage{
    if (self.changeHeaderBackImage) {
        //cover为修改背景
        self.changeHeaderBackImage(@"cover");
    }
}
- (UIView *)videoHeaderView{
    if (!_videoHeaderView) {
        float videoHeight = ceilf(3/4.0 * ScreenW);
        _videoHeaderView = [[UIView alloc] init];
        [self addSubview:_videoHeaderView];
        
        [_videoHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self);
            make.height.equalTo(@(videoHeight));
        }];
        
    }
    return _videoHeaderView;
}
#pragma mark - 通知方法
-(void)automaticPlay{
    if ([NSString isRealString:self.userInfo[@"video_cover"]]) {
        openVoice = NO;
        recordNetStatus = NO;
        if (isAutomaticPlay) {
            [self pressPlayButton];
        }
        openVoice = YES;
    }
}
-(void)WANPlay{
    if ([NSString isRealString:self.userInfo[@"video_cover"]]) {
        recordNetStatus = YES;
        openVoice = YES;
        isAutomaticPlay = NO;
    }
    
}
-(void)pausePlayer{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.player pause];
        self.playImgBtn.hidden = NO;
        [self bringSubviewToFront:self.playImgBtn];
    });
}
- (void)playingEnd:(NSNotification *)notification{
    self.playImgBtn.hidden = NO;
    [self.player pause];
    [self.currentPlayerItem seekToTime:kCMTimeZero];
}
#pragma mark - 点击播放按钮
- (void)pressPlayButton{
    if (FirstClickPlayBtn) {
        [self loadPlay];
    }else{
        [self playOrPause];
    }
    
    if (openVoice) {
        self.closeVoiceBtn.selected = NO;
        self.player.volume = 1;
        openVoice = YES;
    }else{
        self.closeVoiceBtn.selected = YES;
        self.player.volume = 0;
        openVoice = NO;
    }
}
#pragma mark - 点击播放或者暂停
-(void)playOrPause{
    if (self.playImgBtn.isHidden) {
        self.playImgBtn.hidden = NO;
        [self.player pause];
        [self bringSubviewToFront:self.playImgBtn];
        self.loadingImageView.hidden = YES;
    }
    else{
        self.playImgBtn.hidden = YES;
        [self.player play];
        [self bringSubviewToFront:self.playImgBtn];
        if (!isAlwayHidden) {
             self.loadingImageView.hidden = NO;
        }
       
    }
}
//- (void)play{
//    if (!self.playImgBtn.hidden) {
//        self.playImgBtn.hidden = YES;
//        [self.player play];
//        [self bringSubviewToFront:self.playImgBtn];
//    }
//}
//-(void)pause{
//    dispatch_async(dispatch_get_main_queue(), ^{
//        if (self.playImgBtn.hidden) {
//            [self.player pause];
//            [self bringSubviewToFront:self.playImgBtn];
//        }
//    });
//}

#pragma mark - 点击声音
- (void)closeVoiceBtnClick:(UIButton *)btn{
    btn.selected = !btn.selected;
    if (btn.selected) {
        volume = 0;
        self.player.volume = 0;
        openVoice = NO;
    }else{
        volume = 1;
        self.player.volume = 1;
        openVoice = YES;
    }
}

- (void)loadPlay{
    isAutomaticPlay = NO;
    NSURL *url = [NSURL URLWithString:self.userInfo[@"video_cover"]];
    NSURLComponents *components = [[NSURLComponents alloc] initWithURL:url resolvingAgainstBaseURL:NO];
    components.scheme = @"streaming";
    NSURL *playUrl = [components URL];
    NSString *md5File = [NSString stringWithFormat:@"%@.mp4", [[playUrl absoluteString] stringToMD5]];
    //这里自己写需要保存数据的路径
    NSString *document = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).lastObject;
    NSString *cachePath =  [document stringByAppendingPathComponent:md5File];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:cachePath]) {
        NSURL *localURL = [NSURL fileURLWithPath:cachePath];
        [self playWithVideoUrl:localURL];
    } else {
        if (recordNetStatus) {
            [self.alert show];
        }else{
            [self playWithVideoUrl:url];
        }
    }
    FirstClickPlayBtn = NO;
    
}
- (void)playWithVideoUrl:(NSURL *)url{
    [self releasePlayer];
    NSString *str = [url absoluteString];
    if ([str hasPrefix:@"https"] || [str hasPrefix:@"http"]) {
        
        self.resouerLoader          = [[HcdLoaderURLConnection alloc] init];
        self.resouerLoader.delegate = self;
        NSURL *playUrl              = [self.resouerLoader getSchemeVideoURL:url];
        self.videoURLAsset          = [AVURLAsset URLAssetWithURL:playUrl options:nil];
        [self.videoURLAsset.resourceLoader setDelegate:self.resouerLoader queue:dispatch_get_main_queue()];
        self.currentPlayerItem      = [AVPlayerItem playerItemWithAsset:_videoURLAsset];
        
    } else {
        self.videoAsset = [AVURLAsset URLAssetWithURL:url options:nil];
        self.currentPlayerItem = [AVPlayerItem playerItemWithAsset:_videoAsset];
    }
    if (!self.player) {
        self.player = [AVPlayer playerWithPlayerItem:self.currentPlayerItem];
        float videoWidth = ScreenW;
        float videoHeight = ceilf(3/4.0 * ScreenW);
        self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
        self.playerLayer.frame = CGRectMake(0, 0, videoWidth, videoHeight);
        self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
        [self.videoHeaderView.layer addSublayer:self.playerLayer];
        UITapGestureRecognizer *playTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playOrPause)];
        [self.videoHeaderView addGestureRecognizer:playTap];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playingEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
        isPlay = YES;
    } else {
        [self.player replaceCurrentItemWithPlayerItem:self.currentPlayerItem];
    }
    
    [self.currentPlayerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    [self.currentPlayerItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionNew context:nil];
    [self.currentPlayerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew context:nil];
    [self.currentPlayerItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew context:nil];
    
    self.loadingImageView.hidden = NO;
    self.loadSource = YES;
    self.angle = 0;
    self.firstTransform = self.loadingImageView.transform;
    [self startAnimation];
    self.playImgBtn.hidden = YES;
}
- (void)releasePlayer {
    //    if (!self.currentPlayerItem) {
    //        return;
    //    }
    if (isRemoveKVO) {
        [self.currentPlayerItem removeObserver:self forKeyPath:@"status"];
        [self.currentPlayerItem removeObserver:self forKeyPath:@"loadedTimeRanges"];
        [self.currentPlayerItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
        [self.currentPlayerItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
        
        self.currentPlayerItem = nil;
        
        if (self.resouerLoader.task) {
            [self.resouerLoader.task cancel];
            self.resouerLoader.task = nil;
            self.resouerLoader = nil;
        }
    }
    
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    
    AVPlayerItem *playerItem = (AVPlayerItem *)object;
    if ([keyPath isEqualToString:@"status"]) {
        if ([playerItem status] == AVPlayerStatusReadyToPlay) {
            //status 点进去看 有三种状态
            CGFloat duration = playerItem.duration.value / playerItem.duration.timescale; //视频总时间
            NSLog(@"准备好播放了，总时间：%.2f", duration);//还可以获得播放的进度，这里可以给播放进度条赋值了
            
        } else if ([playerItem status] == AVPlayerStatusFailed || [playerItem status] == AVPlayerStatusUnknown) {
            [self.player pause];
            [self bringSubviewToFront:self.playImgBtn];
        }
        
    } else if ([keyPath isEqualToString:@"loadedTimeRanges"]) {  //监听播放器的下载进度
        
        NSArray *loadedTimeRanges = [playerItem loadedTimeRanges];
        CMTimeRange timeRange = [loadedTimeRanges.firstObject CMTimeRangeValue];// 获取缓冲区域
        float startSeconds = CMTimeGetSeconds(timeRange.start);
        float durationSeconds = CMTimeGetSeconds(timeRange.duration);
        NSTimeInterval timeInterval = startSeconds + durationSeconds;// 计算缓冲总进度
        CMTime duration = playerItem.duration;
        CGFloat totalDuration = CMTimeGetSeconds(duration);
        loadProgress = timeInterval / totalDuration;
        NSLog(@"下载进度：%.2f", timeInterval / totalDuration);
    } else if ([keyPath isEqualToString:@"playbackBufferEmpty"]) { //监听播放器在缓冲数据的状态
        NSLog(@"缓冲不足暂停了");
    } else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
        
        NSLog(@"缓冲达到可播放程度了");
        
        //由于 AVPlayer 缓存不足就会自动暂停，所以缓存充足了需要手动播放，才能继续播放
        if (!isPauseBecomeActive) {
            self.loadSource = NO;
            self.angle = 0;
            self.loadingImageView.transform = self.firstTransform;
            self.loadingImageView.hidden = YES;
            [self.player play];
            isPauseBecomeActive = YES;
            self.playImgBtn.hidden = YES;
            isAlwayHidden = YES;
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self playWithVideoUrl:[NSURL URLWithString:self.userInfo[@"video_cover"]]];
    }else{
        FirstClickPlayBtn = YES;
    }
}
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        FirstClickPlayBtn = YES;
        isAutomaticPlay = YES;
        self.backgroundColor = SLColor(248, 248, 248);
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(automaticPlay) name:@"automaticPlay" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pausePlayer) name:@"pausePlayer" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(WANPlay) name:@"WANPlay" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationWillEnterForegroundNotification object:[UIApplication sharedApplication]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationWillResignActiveNotification object:[UIApplication sharedApplication]];
    }
    return self;
}
- (void)applicationDidBecomeActive:(NSNotification *)notification{
    // [self.player seekToTime:recordTime];
    if ([NSString isRealString:self.userInfo[@"video_cover"]]) {
        
        isPauseBecomeActive = YES;
        if (self.player.rate == 0 && !isPlaying) {
            [self.player pause];
            self.playImgBtn.hidden = NO;
            [self bringSubviewToFront:self.playImgBtn];
        }else if(isPlaying){
            [self.player play];
        }
    }
}

- (void)applicationDidEnterBackground:(NSNotification *)notification{
    
    if ([NSString isRealString:self.userInfo[@"video_cover"]]) {
        
        if (self.player.rate == 0) {
            isPlaying = NO;
        }else if(self.player.rate == 1){
            isPlaying = YES;
        }
    }
    // recordTime = self.player.currentTime;
}


- (void)setUserInfo:(NSDictionary *)userInfo{
    
    _userInfo = userInfo;
    CGFloat backWidth;
    if ([userInfo[@"id"] isEqual:ApplicationDelegate.userId]) {
        if ([NSString isRealString:userInfo[@"video_cover"]]) {
            backWidth = 80;
        }else{
            backWidth = 46;
        }
    }else{
        backWidth = 46;
    }
    if ([NSString isRealString:userInfo[@"video_cover"]]) {
        self.closeVoiceBtn.hidden = NO;
        self.backImageView.frame = CGRectMake(0, 0, screen_W, ceilf(3/4.0 * ScreenW)) ;
        [self.backImageView sd_setImageWithURL:[NSURL URLWithString:userInfo[@"cover"]] placeholderImage:self.backImageView.image];
        self.videoHeaderView.hidden = NO;
        
        [self insertSubview:self.otherBtnBackImageView aboveSubview:self.videoHeaderView];
        [self.otherBtnBackImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self);
            make.width.equalTo(@(backWidth));
            make.height.equalTo(@25);
            make.bottom.equalTo(self.videoHeaderView.mas_bottom);
        }];
        [self bringSubviewToFront:self.playImgBtn];
        self.playImgBtn.hidden = NO;
        // [self insertSubview:self.playImgBtn aboveSubview:self.videoHeaderView];
        
    }else{
        self.playImgBtn.hidden = YES;
        self.closeVoiceBtn.hidden = YES;
        self.backImageView.hidden = NO;
        self.videoHeaderView.hidden = YES;
        CGRect frame = self.frame;
//        NSLog(@"%@",NSStringFromCGRect(frame));
        frame.origin.y -= 80;
        frame.size.height = ceilf(3/4.0 * ScreenW)+80;
        self.backImageView.frame = frame;
        if ([NSString isRealString:userInfo[@"cover"]]) {
            if (![userInfo[@"cover"] isEqualToString:self.oldBackgroundString]) {
                [self.backImageView sd_setImageWithURL:[NSURL URLWithString:userInfo[@"cover"]] placeholderImage:self.backImageView.image];
                [self insertSubview:self.otherBtnBackImageView aboveSubview:self.backImageView];
                [self.otherBtnBackImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.right.equalTo(self);
                    make.width.equalTo(@(backWidth));
                    make.height.equalTo(@25);
                    make.bottom.equalTo(self.backImageView.mas_bottom);
                }];
                self.oldBackgroundString = _userInfo[@"cover"];
                if (![userInfo[@"id"] isEqual:ApplicationDelegate.userId]) {
                    self.otherBtnBackImageView.hidden = YES;
                }
            }
        }else{
            self.backImageView.hidden = NO;
            self.videoHeaderView.hidden = YES;
            if ([userInfo[@"id"] isEqual:ApplicationDelegate.userId]) {
                self.backImageView.image = [UIImage imageNamed:@"perBackImage"];
            }else{
                self.backImageView.image = [UIImage imageNamed:@"otherPerBackImage"];
            }
            
            [self insertSubview:self.otherBtnBackImageView aboveSubview:self.backImageView];
            [self.otherBtnBackImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self);
                make.width.equalTo(@(backWidth));
                make.height.equalTo(@25);
                make.bottom.equalTo(self.backImageView.mas_bottom);
            }];
            if (![userInfo[@"id"] isEqual:ApplicationDelegate.userId]) {
                self.otherBtnBackImageView.hidden = YES;
            }
        }
    }
    if ([userInfo[@"id"] isEqual:ApplicationDelegate.userId]) {
        [self.changeCoverBtn addTarget:self action:@selector(changeCoverBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self.closeVoiceBtn addTarget:self action:@selector(closeVoiceBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    if ([NSString isRealString:userInfo[@"avatar"]]) {
        if (![userInfo[@"avatar"] isEqualToString:self.oldAvatarString]) {
            self.oldAvatarString = _userInfo[@"avatar"];
            if ([NSString isRealString:userInfo[@"video_cover"]]) {
                [self addSubview:self.avatarbtn];
                
                [self.avatarbtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.videoHeaderView.mas_bottom).offset(-26);
                    make.centerX.equalTo(self);
                    make.width.height.equalTo(@72);
                }];
            }else{
                [self insertSubview:self.avatarbtn aboveSubview:self.backImageView];
                [self.avatarbtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.backImageView.mas_bottom).offset(-26);
                    make.centerX.equalTo(self);
                    make.width.height.equalTo(@72);
                }];
            }
            [self.avatarbtn sd_setImageWithURL:[NSURL URLWithString:userInfo[@"avatar"]] forState:UIControlStateNormal placeholderImage:self.avatarbtn.imageView.image];
        }
    }else{
        
        if ([NSString isRealString:userInfo[@"video_cover"]]) {
            [self addSubview:self.avatarbtn];
            
            [self.avatarbtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.videoHeaderView.mas_bottom).offset(-26);
                make.centerX.equalTo(self);
                make.width.height.equalTo(@72);
            }];
        }else{
            [self insertSubview:self.avatarbtn aboveSubview:self.backImageView];
            [self.avatarbtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.backImageView.mas_bottom).offset(-26);
                make.centerX.equalTo(self);
                make.width.height.equalTo(@72);
            }];
        }
    }
    
    self.roleImageView.hidden = [userInfo[@"role_type"] isEqual:@0]?YES:NO;
    self.renZhengImageView.hidden = [userInfo[@"verification_status"] isEqual:@2]?NO:YES;
    self.nickNamelabel.text = userInfo[@"nickname"];
    CGSize labelSize = [userInfo[@"nickname"] boundingRectWithSize:CGSizeMake(MAXFLOAT,  MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:18]} context:nil].size;
    
    if (labelSize.width>screen_W-80) {
        [self.nickNamelabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.avatarbtn.mas_bottom).offset(2);
            make.height.equalTo(@(20));
            make.width.equalTo(@(screen_W-80));
        }];
    }else{
        [self.nickNamelabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.avatarbtn.mas_bottom).offset(2);
            make.height.equalTo(@(20));
            make.width.equalTo(@(labelSize.width+3));
        }];
    }
    
    if ([userInfo[@"gender"] isEqualToString:@"FEMALE"]) {
        self.sexImageView.image = [UIImage imageNamed:@"Female-symbol"];
    }else {
        self.sexImageView.image = [UIImage imageNamed:@"Male-Symbol"];
    }
    [_sexImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nickNamelabel.mas_centerY);
        make.left.equalTo(self.nickNamelabel.mas_right).offset(11);
    }];
    int countXing = 5;
    NSNumber *credit_level = userInfo[@"credit_level"];
    if (!_xingView) {
        _xingView = [[UIView alloc] init];
        [self addSubview:_xingView];
        [_xingView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.mas_centerX).offset(-6.5);
            make.top.equalTo(self.nickNamelabel.mas_bottom).offset(10);
            make.width.equalTo(@68);
            make.height.equalTo(@12);
        }];
    }else{
        for (UIImageView *imageView in self.xingArr) {
            [imageView removeFromSuperview];
        }
    }
    
    
    for (int i=0; i<countXing; i++) {
        if (i<[credit_level intValue]) {
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"evaluateColorStar"]];
            imageView.frame = CGRectMake(0+(2+12)*i, 0, 12, 12);
            [_xingView addSubview:imageView];
            [self.xingArr addObject:imageView];
        }else{
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"evaluateStar"]];
            imageView.frame = CGRectMake(0+(2+12)*i, 0, 12, 12);
            [_xingView addSubview:imageView];
            [self.xingArr addObject:imageView];
        }
    }
    NSString *rezhengTitle = @"";
    if ([userInfo[@"verification_status"] isEqual:@0]) {
        rezhengTitle = @"未认证";
        [self.reZhengbtn setImage:[UIImage imageNamed:@"grayMingpian"] forState:UIControlStateNormal];
        [_reZhengbtn setTitleColor:UIColorFromRGB(0x9e7b7f) forState:UIControlStateNormal];
    }else if ([userInfo[@"verification_status"] isEqual:@1]) {
        rezhengTitle = @"认证中";
        [self.reZhengbtn setImage:[UIImage imageNamed:@"grayMingpian"] forState:UIControlStateNormal];
        [_reZhengbtn setTitleColor:UIColorFromRGB(0x9e7b7f) forState:UIControlStateNormal];
    }else{
        rezhengTitle = @"已认证";
        [self.reZhengbtn setImage:[UIImage imageNamed:@"mingpian"] forState:UIControlStateNormal];
        [_reZhengbtn setTitleColor:SLMainColor forState:UIControlStateNormal];
    }
    [self.reZhengbtn setTitle:rezhengTitle forState:UIControlStateNormal];
    [self.followerBtn addTarget:self action:@selector(fensiBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *fensiTitle = [NSString stringWithFormat:@"%@",userInfo[@"followers"]];
    [self.fensiBtn setTitle:fensiTitle forState:UIControlStateNormal];
    CGFloat width1 = [fensiTitle boundingRectWithSize:CGSizeMake(screen_W, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size.width+10;
    [self.fensiBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.followerBtn.mas_centerY);
        make.left.equalTo(self.followerBtn.mas_right);
        make.height.equalTo(@40);
        make.width.equalTo(@(width1));
    }];
    
    NSString *guanzhuTitle = [NSString stringWithFormat:@"%@",userInfo[@"follows"]];
    CGFloat width = [guanzhuTitle boundingRectWithSize:CGSizeMake(screen_W, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size.width+10;
    [self.guanzhuBtn setTitle:guanzhuTitle forState:UIControlStateNormal];
    [self.followBtn addTarget:self action:@selector(guanzhuBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.guanzhuBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.avatarbtn.mas_centerY).offset(-5);
        make.right.equalTo(self.avatarbtn.mas_left).offset(-10);
        make.height.equalTo(@40);
        make.width.equalTo(@(width));
    }];
    
    [self.fensiBtn addTarget:self action:@selector(fensiBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.guanzhuBtn addTarget:self action:@selector(guanzhuBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
}
- (void)changeVideo{
    self.player = nil;
    [self.playerLayer removeFromSuperlayer];
    self.playImgBtn.hidden = NO;
    [self bringSubviewToFront:self.playImgBtn];
    [self insertSubview:self.avatarbtn aboveSubview:self.videoHeaderView];
    isRemoveKVO = YES;
    isPauseBecomeActive = NO;
    FirstClickPlayBtn = YES;
}
- (void)avatarChange:(UIButton *)btn{
    if (self.changeHeaderBackImage) {
        //cover为修改背景
        self.changeHeaderBackImage(@"avatar");
    }
}

- (void)changeCoverBtnClick:(UIButton *)btn{
    if (self.changeHeaderBackImage) {
        //cover为修改背景
        self.changeHeaderBackImage(@"cover");
    }
}
- (void)fensiBtnClick{
    if (self.enterFenSiVc) {
        //cover为修改背景
        self.enterFenSiVc(@"fensi");
    }
}
- (void)guanzhuBtnClick{
    if (self.enterFenSiVc) {
        //cover为修改背景
        self.enterFenSiVc(@"guanzhu");
    }
}
- (void)dealloc{
    NSLog(@"dealloc");
    self.playImgBtn = nil;
    //移除kvo
    if (isPlay) {
        [self.currentPlayerItem removeObserver:self forKeyPath:@"status" context:nil];
        [self.currentPlayerItem removeObserver:self forKeyPath:@"loadedTimeRanges" context:nil];
        [self.currentPlayerItem removeObserver:self forKeyPath:@"playbackBufferEmpty" context:nil];
        [self.currentPlayerItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp" context:nil];
        [self.player pause]; //暂停
        self.player = nil; //置空
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
- (void)startAnimation{
    CGAffineTransform endAngle = CGAffineTransformMakeRotation(self.angle * (M_PI /180.0f));
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.02 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        weakSelf.loadingImageView.transform = endAngle;
    } completion:^(BOOL finished) {
        if (self.loadSource) {
            weakSelf.angle += 10;
            [weakSelf startAnimation];
        }
    }];
}
- (void)changNickName:(NSString *)name{
    self.nickNamelabel.text = name;
    CGSize labelSize = [name boundingRectWithSize:CGSizeMake(MAXFLOAT,  MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:18]} context:nil].size;
    
    if (labelSize.width>screen_W-80) {
        [self.nickNamelabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.avatarbtn.mas_bottom).offset(2);
//            make.height.equalTo(@(15));
            make.width.equalTo(@(screen_W-80));
        }];
    }else{
        [self.nickNamelabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.avatarbtn.mas_bottom).offset(2);
//            make.height.equalTo(@(18));
            make.width.equalTo(@(labelSize.width+3));
        }];
    }
    [_sexImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nickNamelabel.mas_centerY);
        make.left.equalTo(self.nickNamelabel.mas_right).offset(11);
    }];
   
}

@end
