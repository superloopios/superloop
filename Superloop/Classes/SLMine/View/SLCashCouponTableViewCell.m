//
//  SLCashCouponTableViewCell.m
//  Superloop
//
//  Created by xiaowu on 16/9/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCashCouponTableViewCell.h"
#import "SLCashCouponModel.h"
#import "ColorUtility.h"

@interface SLCashCouponTableViewCell ()

@property(nonatomic, strong)UILabel *sourceLabel;
@property(nonatomic, strong)UILabel *timeLabel;
@property(nonatomic, strong)UILabel *priceLabel;
@property(nonatomic, strong)UILabel *cashLabel;
@property(nonatomic, strong)UIView *backView;
@property(nonatomic, strong)UIView *topView;
@property(nonatomic, strong)UIImageView *sepatorImageView;

@end

@implementation SLCashCouponTableViewCell

- (UIImageView *)sepatorImageView{
    if (!_sepatorImageView) {
        _sepatorImageView = [[UIImageView alloc] init];
        _sepatorImageView.contentMode = UIViewContentModeCenter;
        [self.backView addSubview:_sepatorImageView];
        [_sepatorImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.backView.mas_centerX);
            make.centerY.equalTo(self.priceLabel.mas_centerY);
            make.width.equalTo(@3);
            make.height.equalTo(@40);
        }];
        _sepatorImageView.clipsToBounds = YES;
    }
    return _sepatorImageView;
}

- (UILabel *)sourceLabel{
    if (!_sourceLabel) {
        _sourceLabel = [[UILabel alloc] init];
        _sourceLabel.font = [UIFont systemFontOfSize:13];
        _sourceLabel.textColor = [UIColor whiteColor];
        [self.topView addSubview:_sourceLabel];
        [_sourceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.topView.mas_left).offset(7);
            make.centerY.equalTo(self.topView.mas_centerY);
        }];
    }
    return _sourceLabel;
}
- (UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.font = [UIFont systemFontOfSize:13];
        _timeLabel.textColor = [UIColor whiteColor];
        [self.topView addSubview:_timeLabel];
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.topView).offset(-7);
            make.centerY.equalTo(self.topView.mas_centerY);
        }];
    }
    return _timeLabel;
}
- (UILabel *)priceLabel{
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.font = [UIFont systemFontOfSize:25];
        _priceLabel.textColor = [ColorUtility colorWithHexString:@"ffa200"];
        [self.topView addSubview:_priceLabel];
        [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.backView.mas_centerX).multipliedBy(0.5);
            make.top.equalTo(self.topView.mas_bottom).offset(15);
        }];
    }
    return _priceLabel;
}

- (UILabel *)cashLabel{
    if (!_cashLabel) {
        _cashLabel = [[UILabel alloc] init];
        _cashLabel.font = [UIFont systemFontOfSize:20];
        [self.topView addSubview:_cashLabel];
        [_cashLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.backView.mas_centerX).multipliedBy(1.5);
            make.centerY.equalTo(self.priceLabel.mas_centerY);
        }];
    }
    return _cashLabel;
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        [self.contentView addSubview:_backView];
        _backView.layer.cornerRadius = 5;
        _backView.clipsToBounds = YES;
        _backView.layer.borderColor=SLColor(200, 200, 200).CGColor;
        _backView.layer.borderWidth = 0.5;
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).offset(30);
            make.right.equalTo(self.contentView.mas_right).offset(-30);
            make.top.equalTo(self.contentView.mas_top);
            make.height.equalTo(@77);
        }];
    }
    return _backView;
}

- (UIView *)topView{
    if (!_topView) {
        _topView = [[UIView alloc] init];
        [self.backView addSubview:_topView];
        [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.backView.mas_left);
            make.right.equalTo(self.backView.mas_right);
            make.top.equalTo(self.backView.mas_top);
            make.height.equalTo(@20);
        }];
    }
    return _topView;
}

- (void)setModel:(SLCashCouponModel *)model{
    _model = model;
    self.backView.backgroundColor = [UIColor whiteColor];
    self.contentView.backgroundColor = SLColor(244, 244, 244);
    if (model.voucher_type == 1) {
        self.sourceLabel.text = @"邀请获得";
    }else if (model.voucher_type == 2){
        self.sourceLabel.text = @"抢券获得";
    }
    if (model.voucher_status == 1) {
        self.timeLabel.text = [NSString stringWithFormat:@"有效期至%@",model.created];
        self.priceLabel.text = [NSString stringWithFormat:@"%.2f元",[model.left_amount doubleValue]];
    }else if(model.voucher_status == 2){
        self.timeLabel.text = @"已使用";
        self.priceLabel.text = [NSString stringWithFormat:@"%.2f元",[model.amount doubleValue]];
    }else if(model.voucher_status == 3){
        self.timeLabel.text = @"已过期";
        self.priceLabel.text = [NSString stringWithFormat:@"%.2f元",[model.amount doubleValue]];
    }
    
    
    self.cashLabel.text = @"现金券";
    self.timeLabel.textColor = [UIColor whiteColor];
    
    if (model.voucher_status == 1) {
        self.topView.backgroundColor = UIColorFromRGB(0xffcb4f);
        self.cashLabel.textColor = UIColorFromRGB(0xff5a5f);
        self.priceLabel.textColor = [ColorUtility colorWithHexString:@"ffa200"];
        self.sepatorImageView.image = [UIImage imageNamed:@"dashSepator"];
    }else if(model.voucher_status == 2 || model.voucher_status == 3){
        self.topView.backgroundColor = UIColorFromRGB(0xead8b8);
        self.cashLabel.textColor = SLColor(203, 203, 203);
        self.priceLabel.textColor = SLColor(203, 203, 203);
        self.sepatorImageView.image = [UIImage imageNamed:@"grayDashSepator"];
    }
}

- (void)setFrame:(CGRect)frame
{
    
    frame.size.height -= 10;
    [super setFrame:frame];
}

@end
