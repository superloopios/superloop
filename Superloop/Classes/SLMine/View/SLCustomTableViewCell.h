//
//  SLCustomTableViewCell.h
//  Superloop
//
//  Created by xiaowu on 16/11/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SLCustomModel;

@interface SLCustomTableViewCell : UITableViewCell

@property(nonatomic, strong)SLCustomModel *model;

@end
