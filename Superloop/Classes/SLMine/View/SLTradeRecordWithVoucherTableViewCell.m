//
//  SLTradeRecordWithVoucherTableViewCell.m
//  Superloop
//
//  Created by WangS on 16/9/17.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLTradeRecordWithVoucherTableViewCell.h"
#import "SLTradeRecoredModel.h"

@interface SLTradeRecordWithVoucherTableViewCell ()

@property (nonatomic,strong) UIImageView *leftImageView;
@property (nonatomic,strong) UILabel *type_descLable;
@property (nonatomic,strong) UILabel *timeLable;
@property (nonatomic,strong) UIView *lineView;
@property (nonatomic,strong) UILabel *moneyLable;
@property (nonatomic,strong) UIImageView *voucherImg;
@property (nonatomic,strong) UILabel *voucherLable;
@property (nonatomic,strong) UIImageView *rightImageView;

@end

@implementation SLTradeRecordWithVoucherTableViewCell

- (void)setTradeModel:(SLTradeRecoredModel *)tradeModel{
    _tradeModel = tradeModel;
    
    switch (tradeModel.type) {
        case 0:
            //账户充值 微信
            self.leftImageView.image = [UIImage imageNamed:@"yiwancheng"];
            self.rightImageView.hidden = YES;
            break;
        case 1:
            //账户充值 支付宝
            self.leftImageView.image = [UIImage imageNamed:@"yiwancheng"];
            self.rightImageView.hidden = YES;
            break;
        case 2:
            //通话消费
            self.leftImageView.image = [UIImage imageNamed:@"yiwancheng"];
            self.rightImageView.hidden = YES;
            break;
        case 3:
            //通话收入
            self.leftImageView.image = [UIImage imageNamed:@"yiwancheng"];
            self.rightImageView.hidden = YES;
            break;
        case 4:
            //付费消息
            self.leftImageView.image = [UIImage imageNamed:@"yiwancheng"];
            self.rightImageView.hidden = YES;
            break;
        case 5:
            //提现
            self.leftImageView.image = [UIImage imageNamed:@"jinxingzhong"];
            self.rightImageView.hidden = NO;
            break;
        case 6:
            //后台直接奖励
            self.leftImageView.image = [UIImage imageNamed:@"yiwancheng"];
            self.rightImageView.hidden = YES;
            break;
        default:
            break;
    }
    self.type_descLable.text = tradeModel.type_desc;
    
    NSNumber *amount = tradeModel.amount;
    if (amount.doubleValue < 0) {
        self.moneyLable.textColor = SLColor(249, 7, 14);
        
        CGFloat voucherAmount = tradeModel.voucher_amount.doubleValue;
        if (voucherAmount > 0) {
            CGFloat moneyAmount = amount.doubleValue;
            self.moneyLable.font = [UIFont systemFontOfSize:13];
            if (moneyAmount + voucherAmount == 0) {
                self.moneyLable.text = [NSString stringWithFormat:@"-0.00"];
                
            }else{
                self.moneyLable.text = [NSString stringWithFormat:@"%.2f",moneyAmount + voucherAmount];
            }
            self.voucherLable.hidden = NO;
            self.voucherImg.hidden = NO;
            self.voucherLable.text = [NSString stringWithFormat:@"-%.2f",voucherAmount];
            CGFloat voucherLableWidth= [self.voucherLable.text boundingRectWithSize:CGSizeMake(ScreenW-150-20-14-8-20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13]} context:nil].size.width+2;
            [self.moneyLable mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.contentView.mas_top).offset(8);
                make.right.mas_equalTo(self.rightImageView.mas_left).offset(-8);
                make.width.mas_equalTo(ScreenW-150-20-14-8-20);
                make.height.mas_equalTo(13);
            }];
            [self.voucherLable mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-8);
                make.right.mas_equalTo(self.rightImageView.mas_left).offset(-8);
                make.width.mas_equalTo(voucherLableWidth);
                make.height.mas_equalTo(13);
            }];
            [self.voucherImg mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-7);
                make.right.mas_equalTo(self.voucherLable.mas_left).offset(-8);
                make.width.mas_equalTo(16);
                make.height.mas_equalTo(16);
            }];
            
        }else {
            self.voucherLable.hidden = YES;
            self.voucherImg.hidden = YES;
            self.moneyLable.font = [UIFont systemFontOfSize:16];
            self.moneyLable.text = [NSString stringWithFormat:@"%.2f",amount.doubleValue];
            [self.moneyLable mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.contentView.mas_top).offset(10);
                make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-13);
                make.right.mas_equalTo(self.rightImageView.mas_left).offset(-8);
                make.width.mas_equalTo(ScreenW-150-20-20);
            }];
            
        }
    }else{
        self.voucherLable.hidden = YES;
        self.voucherImg.hidden = YES;
        self.moneyLable.textColor = [UIColor blackColor];
        self.moneyLable.font = [UIFont systemFontOfSize:16];
        self.moneyLable.text = [NSString stringWithFormat:@"+%.2f",amount.doubleValue];
        [self.moneyLable mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView.mas_top).offset(10);
            make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-13);
            make.right.mas_equalTo(self.rightImageView.mas_left).offset(-8);
            make.width.mas_equalTo(ScreenW-150-20-20);
        }];
    }
    //时间
    NSString *time = [self dateForMat:@([tradeModel.created integerValue])];
    self.timeLable.text = time;
    
    self.lineView.backgroundColor = UIColorFromRGB(0xe8e8e8);
}
- (NSString *)dateForMat :(NSNumber *)dateNumber{
    // 将时间戳字符串转成日期
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:dateNumber.doubleValue / 1000];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *dateStr = [fmt stringFromDate:date];
    NSDate *createdAtDate = [fmt dateFromString:dateStr];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm";
    return [fmt stringFromDate:createdAtDate];
}


- (UIImageView *)leftImageView{
    if (!_leftImageView) {
        _leftImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_leftImageView];
        [_leftImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView.mas_top).offset(8);
            make.left.mas_equalTo(self.contentView.mas_left).offset(10);
            make.width.mas_equalTo(14);
            make.height.mas_equalTo(14);
        }];
    }
    return _leftImageView;
}

- (UILabel *)type_descLable{
    if (!_type_descLable) {
        _type_descLable = [[UILabel alloc] init];
        _type_descLable.font = [UIFont systemFontOfSize:16];
        _type_descLable.textColor = [UIColor blackColor];
        _type_descLable.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_type_descLable];
        [_type_descLable mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView.mas_top).offset(5);
            make.left.mas_equalTo(_leftImageView.mas_right).offset(5);
            make.width.mas_equalTo(119);
            make.height.mas_equalTo(20);
        }];
        
    }
    return _type_descLable;
}

- (UILabel *)timeLable{
    if (!_timeLable) {
        _timeLable = [[UILabel alloc] init];
        _timeLable.font = [UIFont systemFontOfSize:13];
        _timeLable.textColor = UIColorFromRGB(0x666666);
        _timeLable.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_timeLable];
        [_timeLable mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_type_descLable.mas_bottom).offset(2);
            make.left.mas_equalTo(self.contentView.mas_left).offset(10);
            make.width.mas_equalTo(150);
            make.height.mas_equalTo(20);
        }];
        
    }
    return _timeLable;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        [self.contentView addSubview:_lineView];
        [_lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-0.5);
            make.left.mas_equalTo(self.contentView.mas_left);
            make.right.mas_equalTo(self.contentView.mas_right);
            make.height.mas_equalTo(0.5);
        }];
    }
    return _lineView;
}
- (UILabel *)moneyLable{
    if (!_moneyLable) {
        _moneyLable = [[UILabel alloc] init];
        _moneyLable.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_moneyLable];
    }
    return _moneyLable;
}
- (UIImageView *)voucherImg{
    if (!_voucherImg) {
        _voucherImg = [[UIImageView alloc] init];
        _voucherImg.image = [UIImage imageNamed:@"voucherImg"];
        [self.contentView addSubview:_voucherImg];
    }
    return _voucherImg;
}

- (UILabel *)voucherLable{
    if (!_voucherLable) {
        _voucherLable = [[UILabel alloc] init];
        _voucherLable.font = [UIFont systemFontOfSize:13];
        _voucherLable.textColor = UIColorFromRGB(0xef7000);
        _voucherLable.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_voucherLable];
    }
    return _voucherLable;
}
- (UIImageView *)rightImageView{
    if (!_rightImageView) {
        _rightImageView = [[UIImageView alloc] init];
        _rightImageView.image = [UIImage imageNamed:@"right-Arrow-outline"];
        [self.contentView addSubview:_rightImageView];
        [_rightImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView.mas_top).offset(18);
            make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
            make.width.mas_equalTo(6);
            make.height.mas_equalTo(11);
        }];
    }
    return _rightImageView;
}
@end
