//
//  SLTradeRecordViewController.m
//  Superloop
//
//  Created by 张梦川 on 16/5/24.
//  Copyright © 2016年 Superloop. All rights reserved.
//  交易记录控制器

#import "SLTradeRecordViewController.h"
#import "SLTradeTableViewCell.h"
#import  <MJRefresh.h>
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "SLAPIHelper.h"
#import "SLDrawCashDetailViewController.h"
#import "SLTradeRecordWithVoucherTableViewCell.h"
#import "SLTradeRecoredModel.h"

@interface SLTradeRecordViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)NSString *trade_No;
@property (nonatomic,assign)NSInteger page;
@end

@implementation SLTradeRecordViewController{
    
    UITableView *_tableView;
    NSMutableArray *_arrList;
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLTradeRecordViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLTradeRecordViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.title = @"交易记录";
    [self setUpNav];
    self.page = 0;
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,WIDTH, HEIGHT-64) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    self.view.backgroundColor = SLColor(250, 250, 250);
    _tableView.backgroundColor = SLColor(250, 250, 250);
    _tableView.contentInset = UIEdgeInsetsMake(0, 0, 60, 0);
    [_tableView registerClass:[SLTradeRecordWithVoucherTableViewCell class] forCellReuseIdentifier:@"tradeCell"];
    _arrList = [[NSMutableArray alloc]init];
    [self setUpRefresh];
    [_tableView.mj_header beginRefreshing];
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"交易记录";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setUpRefresh
{
    _tableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadDataForHeader)];
    
    _tableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopics)];
    
}
/**
 * 加载更多搜索出来的用户列表
 */
- (void)loadMoreTopics
{
    self.page ++;
    [self loadData:NO];
}

-(void)loadDataForHeader{
    [self loadData:YES];
}
-(void)loadData:(BOOL)isheader{
    if (isheader) {
        self.page = 0;
    }
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"page"] = [NSString stringWithFormat:@"%ld",(long)self.page];
    parameters[@"pager"] = @20;
   [SLAPIHelper getUsersTransactions:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
//               NSArray *arr = data[@"result"];
         NSArray *arr = [SLTradeRecoredModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
       if (isheader) {
           if (_arrList) {
               [_arrList removeAllObjects];
               [_arrList addObjectsFromArray:arr];
           }
          
           if (_arrList.count < 20) {
           [_tableView.mj_header endRefreshing];
          [_tableView.mj_footer endRefreshingWithNoMoreData];
           }else{
               
               [_tableView.mj_header endRefreshing];
               [_tableView.mj_footer resetNoMoreData];
           }
       } else {
           // 结束刷新
           [_tableView.mj_footer endRefreshing];
           if (arr.count < 20) {
               [_tableView.mj_footer endRefreshingWithNoMoreData];
           }else{
             [_tableView.mj_footer resetNoMoreData];
           }
           [_arrList addObjectsFromArray:arr];
       }
       
       [_tableView reloadData];
       

   } failure:^(SLHttpRequestError *error) {
        [self showHUDmessage:@"网络错误!"];
   }];
  
  }


#pragma ---代理方法
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrList.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    /*
     static NSString *identif = @"tradeCell";
    SLTradeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identif];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SLTradeTableViewCell" owner:self options:nil]lastObject];
    }

    NSDictionary *dict = _arrList[indexPath.row];
    NSString *type = [NSString stringWithFormat:@"%@",dict[@"type"]];
     //操作状态
    NSString *status = [NSString stringWithFormat:@"%@",dict[@"status"]];
    
    if ([type isEqualToString:@"4"]) {
        //付费消息
        cell.leftImageView.image = [UIImage imageNamed:@"yiwancheng"];
         cell.type_descLable.text = dict[@"type_desc"];
    }
    if ([type isEqualToString:@"0"]) {
        //账户充值
        cell.leftImageView.image = [UIImage imageNamed:@"yiwancheng"];
        cell.type_descLable.text = dict[@"type_desc"];
        
    }
    
    if ([type isEqualToString:@"2"]) {
        //通话消费
        cell.leftImageView.image = [UIImage imageNamed:@"yiwancheng"];
        cell.type_descLable.text = dict[@"type_desc"];
        
    }
    
    if ([type isEqualToString:@"1"]) {
        //账户充值
        cell.leftImageView.image = [UIImage imageNamed:@"yiwancheng"];
        cell.type_descLable.text = dict[@"type_desc"];
    }
    if ([type isEqualToString:@"3"]) {
        //通话收入
        cell.leftImageView.image = [UIImage imageNamed:@"yiwancheng"];
        cell.type_descLable.text = dict[@"type_desc"];
    }
    
    if ([type isEqualToString:@"5"]) {
        //提现
        cell.leftImageView.image = [UIImage imageNamed:@"jinxingzhong"];
        cell.type_descLable.text = dict[@"type_desc"];
        cell.rightImageView.hidden = NO;
    }

    
    NSNumber *amount = dict[@"amount"];
    if (amount.doubleValue<0) {
        //颜色处理
        cell.moneyLable.textColor= SLColor(249, 7, 14);
        cell.moneyLable.text = [NSString stringWithFormat:@"%.2f",amount.doubleValue];
    }else{
        
        cell.moneyLable.text = [NSString stringWithFormat:@"+%.2f",amount.doubleValue];
    }
    
    
    NSNumber *balance = dict[@"balance"];//余额
    if (balance.doubleValue<0) {
        //颜色处理
        cell.moneyLable.textColor= SLColor(249, 7, 14);
        cell.moneyLable.text = [NSString stringWithFormat:@"%.2f",balance.doubleValue];
    }else{
        cell.moneyLable.textColor= [UIColor blackColor];
        cell.moneyLable.text = [NSString stringWithFormat:@"+%.2f",balance.doubleValue];
    }
    
    NSString *time = [self dateForMat:dict[@"created"]];
    
    cell.timeLable.text = time;
 */
    SLTradeRecordWithVoucherTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tradeCell"];
    SLTradeRecoredModel *model = _arrList[indexPath.row];
    cell.tradeModel = model;
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    /*
    static NSString *identif = @"tradeCell";
    SLTradeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identif];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SLTradeTableViewCell" owner:self options:nil]lastObject];
    }
    //当返回来的时候取消单元格的选中
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dict = _arrList[indexPath.row];
    NSString *type = [NSString stringWithFormat:@"%@",dict[@"type"]];
    
    if ([type isEqualToString:@"4"]) {
        //付费消息
        
    }
    if ([type isEqualToString:@"0"]) {
        //账户充值
       
    }
    
    if ([type isEqualToString:@"1"]) {
        //账户充值
     
        
    }
    if ([type isEqualToString:@"3"]) {
        //通话收入
      
      
    }
    
    if ([type isEqualToString:@"5"]) {
        //提现
        SLDrawCashDetailViewController *detailVc = [[SLDrawCashDetailViewController alloc] init];
        detailVc.trade_No = dict[@"tradeNo"];
        detailVc.dict = dict;
        [self.navigationController pushViewController:detailVc animated:YES];
        
    }
    */
    //当返回来的时候取消单元格的选中
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SLTradeRecoredModel *model = _arrList[indexPath.row];
    if (model.type == 5) {
        //提现
        SLDrawCashDetailViewController *detailVc = [[SLDrawCashDetailViewController alloc] init];
        detailVc.trade_No = model.tradeNo;
        NSLog(@"%@",model.tradeNo);//W00100405N1476608404979552907  测试
        [self.navigationController pushViewController:detailVc animated:YES];
    }

}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
}


@end
