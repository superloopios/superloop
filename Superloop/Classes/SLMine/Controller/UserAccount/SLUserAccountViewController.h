//
//  SLUserAccountViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/29.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLJavascriptBridgeEvent.h"
@interface SLUserAccountViewController : UIViewController

@property (nonatomic, copy)NSString *type;
@property (nonatomic,strong)NSDictionary *userData;
@property (nonatomic,strong)NSNumber *amount;
@property (nonatomic,assign)BOOL isFromCalling;
@end
