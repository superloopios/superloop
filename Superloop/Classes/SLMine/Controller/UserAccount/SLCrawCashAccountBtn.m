//
//  SLCrawCashAccountBtn.m
//  Superloop
//
//  Created by 朱宏伟 on 16/6/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCrawCashAccountBtn.h"

@implementation SLCrawCashAccountBtn

- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    self.titleLabel.x = self.width / 2 - self.titleLabel.width / 2;
  
    //图标
    self.imageView.x = self.titleLabel.x + self.titleLabel.width + 25;
    self.imageView.width = 6;
    self.imageView.height = 11;
    
}
@end
