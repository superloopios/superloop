//
//  SLPriceViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/29.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

//@interface SLPriceViewController : UITableViewController
@interface SLPriceViewController : UIViewController

@property (nonatomic,strong)NSDictionary *userData;

@property (nonatomic ,strong) void(^changePrice)(NSString *str);

@end
