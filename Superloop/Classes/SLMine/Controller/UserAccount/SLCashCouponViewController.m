//
//  SLCashCouponViewController.m
//  Superloop
//
//  Created by xiaowu on 16/9/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCashCouponViewController.h"
#import "SLCashCouponTableViewCell.h"
#import "SLCashCouponModel.h"
#import "SLAPIHelper.h"
#import <MJExtension.h>
#import "SLHistoryCashCouponsViewController.h"
#import "SLWebViewController.h"

@interface SLCashCouponViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UIButton *goBackBtn;
}

@property(nonatomic, strong)UITableView *tableView;

@property(nonatomic, strong)UIView *headerView;

@property(nonatomic, strong)NSMutableArray *cashCouponArr;

@property(nonatomic, strong)NSMutableArray *cashHistoryArr;

@property (nonatomic, strong) UIView *noNetworkOrDefaultView;
@property (nonatomic, strong)UILabel *tiplabel;
@property (nonatomic, strong)UILabel *bolangLabel;

@property (nonatomic, strong)UIImageView *swanImageview;

@end

@implementation SLCashCouponViewController

- (UILabel *)tiplabel{
    if (!_tiplabel) {
        _tiplabel = [[UILabel alloc] init];
        _tiplabel.font = [UIFont systemFontOfSize:17];
        _tiplabel.textAlignment = NSTextAlignmentCenter;
        [self.noNetworkOrDefaultView addSubview:_tiplabel];
        
    }
    return _tiplabel;
}

- (UILabel *)bolangLabel{
    if (!_bolangLabel) {
        _bolangLabel = [[UILabel alloc] init];
        _bolangLabel.font = [UIFont systemFontOfSize:12];
        _bolangLabel.textAlignment = NSTextAlignmentCenter;
        _bolangLabel.text = @"~~~";
        _bolangLabel.textColor = SLColor(213, 213, 213);
        [self.noNetworkOrDefaultView addSubview:_bolangLabel];
    }
    return _bolangLabel;
}

- (UIImageView *)swanImageview{
    if (!_swanImageview) {
        _swanImageview = [[UIImageView alloc] init];
        _swanImageview.image = [UIImage imageNamed:@"swan"];
        [self.noNetworkOrDefaultView addSubview:_swanImageview];
    }
    return _swanImageview;
}

- (UIView *)noNetworkOrDefaultView{
    if (!_noNetworkOrDefaultView) {
        _noNetworkOrDefaultView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screen_W, screen_H-64)];
        _noNetworkOrDefaultView.backgroundColor = SLColor(245, 245, 245);
        [self.tableView addSubview:_noNetworkOrDefaultView];
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpHistoryView)];
        [_noNetworkOrDefaultView addGestureRecognizer:ges];
    }
    return _noNetworkOrDefaultView;
}
- (void)jumpHistoryView{
    if ([self.tiplabel.text hasSuffix:@"历史现金券"]) {
        [self gobackVc];
    }
}
//其他制空页面
-(void)addNetWorkViews:(NSString *)str{
    [self.view addSubview:self.noNetworkOrDefaultView];
    CGSize tipLabelSize = [self.tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    self.tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        self.tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    self.tiplabel.attributedText = AttributedStr;
    
    self.bolangLabel.frame = CGRectMake(self.tiplabel.frame.origin.x+self.tiplabel.frame.size.width,self.tiplabel.frame.origin.y, 50, 10);
    
    self.swanImageview.frame = CGRectMake(ScreenW *0.5-96, self.tiplabel.frame.origin.y + 76, 192, 192);
    
}
- (void)removeNoNetWorkViews{
    [self.noNetworkOrDefaultView removeFromSuperview];
}


- (NSMutableArray *)cashCouponArr{
    if (!_cashCouponArr) {
        _cashCouponArr = [NSMutableArray array];
    }
    return _cashCouponArr;
}

- (NSMutableArray *)cashHistoryArr{
    if (!_cashHistoryArr) {
        _cashHistoryArr = [NSMutableArray array];
    }
    return _cashHistoryArr;
}


- (UIView *)headerView{
    if (!_headerView) {
        _headerView = [[UIView alloc] init];
        _headerView.frame = CGRectMake(0, 0, screen_W, 38);
//        _headerView.backgroundColor = [UIColor redColor];
        
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = SLColor(161, 161, 161);
        lineView.frame = CGRectMake(0, 18.5, screen_W, 1);
        [_headerView addSubview:lineView];
        UILabel *label = [[UILabel alloc] init];
        label.text = @"账户余额大于2元时可用";
        label.font = [UIFont systemFontOfSize:14];
        CGFloat labwidth = [label.text boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size.width+10;
        label.textAlignment = NSTextAlignmentCenter;
        label.frame = CGRectMake((screen_W-labwidth)/2, 0, labwidth, 38);
        label.backgroundColor = SLColor(244, 244, 244);
        [_headerView addSubview:label];
    }
    return _headerView;
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, screen_W, screen_H-64) style:UITableViewStylePlain];
        [_tableView registerClass:[SLCashCouponTableViewCell class] forCellReuseIdentifier:@"cashCouponCell"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
        _tableView.backgroundColor=SLColor(244, 244, 244);
        _tableView.rowHeight = 87;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"SLCashCouponViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLCashCouponViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = SLColor(244, 244, 244);
    [self setUpNav];
//    self.tableView.tableHeaderView = self.headerView;
    [self getData];
    [self addBottom];
}
- (void)addBottom{
    goBackBtn = [[UIButton alloc] init];
    goBackBtn.hidden = YES;
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"查看历史现金券>>"];
    NSRange strRange = {0,[str length]};
    [str addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:strRange];
    [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x999999) range:strRange];
    [goBackBtn setAttributedTitle:str forState:UIControlStateNormal];
    goBackBtn.height = 30;
    goBackBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    self.tableView.tableFooterView = goBackBtn;
    [goBackBtn addTarget:self action:@selector(gobackVc) forControlEvents:UIControlEventTouchUpInside];
}
- (void)gobackVc{
    SLHistoryCashCouponsViewController *vc = [[SLHistoryCashCouponsViewController alloc] init];
    vc.arr = self.cashHistoryArr;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setUpNav{
    
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"现金券明细";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    [self.view addSubview:navView];
    //
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 20, 60, 44);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [rightButton setTitle:@"使用规则" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [rightButton setFrame:CGRectMake(screen_W - 90, 20, 80, 44)];
    [rightButton addTarget:self
                    action:@selector(userRule)
          forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:rightButton];
    
    //添加分割线
    UIImageView *singleView = [[UIImageView alloc] init];
    singleView.backgroundColor = SLSepatorColor;
    singleView.frame = CGRectMake(0, 63.5, ScreenW, 0.5);
    [navView addSubview:singleView];
}
- (void)leftBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)userRule{
   
    SLWebViewController *vc=[[SLWebViewController alloc] init];
    vc.url=@"http://h5.superloop.com.cn/voucher/manual.html";
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.cashCouponArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLCashCouponTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cashCouponCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    SLCashCouponModel *model = self.cashCouponArr[indexPath.row];
    cell.model = model;
    return cell;
}
- (void)getData{
    [SLAPIHelper getCashCouponsSuccess:^(NSURLSessionDataTask *task, NSDictionary *data) {
        NSLog(@"%@",data);
        
        NSArray *arr = [SLCashCouponModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
        for (SLCashCouponModel *model in arr) {
            if (model.voucher_status == 1) {
                [self.cashCouponArr addObject:model];
            }else{
                [self.cashHistoryArr addObject:model];
                goBackBtn.hidden = NO;
            }
        }
       
        if (self.cashCouponArr.count == 0) {
            if (self.cashHistoryArr.count == 0) {
                [self addNetWorkViews:@"赶紧邀请好友获取现金券吧"];
            }else{
                [self addNetWorkViews:@"没有可用现金券,点击查看历史现金券"];
            }
        }
        [self.tableView reloadData];
    } failure:^(SLHttpRequestError *failure) {
        if (failure.httpStatusCode == -1) {
            kShowToast(@"请检查网络");
            
        }
    }];
}


@end
