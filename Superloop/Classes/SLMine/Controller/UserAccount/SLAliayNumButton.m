//
//  SLAliayNumButton.m
//  Superloop
//
//  Created by 朱宏伟 on 16/6/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLAliayNumButton.h"

@implementation SLAliayNumButton

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    self.titleLabel.x = ScreenW - _titleX;
    self.titleLabel.y = 2;
    self.titleLabel.width = 160;
    self.titleLabel.height = 40;
    //图标
    self.imageView.x = ScreenW - 20;
    self.imageView.width = 6;
    self.imageView.height = 11;
    self.imageView.y = 17;
    
}

@end
