//
//  SLAccessViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/21.
//  Copyright © 2016年 Superloop. All rights reserved.
//  返回支付选择控制器的入口

#import <UIKit/UIKit.h>

@interface SLAccessViewController : UIViewController
@property (nonatomic,strong)NSMutableDictionary *dict;
//是否是阿里支付或者是微信支付
@property (nonatomic,assign)BOOL isAlipay;
@property (nonatomic,strong)NSString *amount;
@end
