//
//  SLAccessViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/21.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLAccessViewController.h"

@implementation SLAccessViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = SLColor(255, 255, 255);
    [self setUpNav];
    [self setUpUI];
    
}
- (void)setUpUI
{
    //通过的是哪一种充值方式
    UILabel *lableStyle = [[UILabel alloc] init];
    lableStyle.textColor = SLBlackTitleColor;
    lableStyle.font = [UIFont systemFontOfSize:13];
    if (self.isAlipay) {
        lableStyle.text = [NSString stringWithFormat:@"通过支付宝充值"];
    } else{
        lableStyle.text = [NSString stringWithFormat:@"通过微信充值"];
    }
    [self.view addSubview:lableStyle];
    [lableStyle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(94);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView  setImage:[UIImage imageNamed:@"Checked-symbo"]];
    [self.view addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lableStyle.mas_bottom).offset(15);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.width.mas_equalTo(95);
        make.height.mas_equalTo(95);
    }];
    
    UILabel *lableMoney = [[UILabel alloc] init];
//    [self.dict[@"total_fee"] doubleValue]
    lableMoney.text = @"充值金额";
    [lableMoney setFont:[UIFont systemFontOfSize:13]];
    [lableMoney setTextColor:SLBlackTitleColor];
    [self.view addSubview:lableMoney];
    [lableMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imageView.mas_bottom).offset(15);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];
    //充值金额
    UILabel *chanrgeNumLabel = [[UILabel alloc] init];
    chanrgeNumLabel.text = [NSString stringWithFormat:@"%.2f",[self.dict[@"total_fee"] doubleValue]];
    [chanrgeNumLabel setFont:[UIFont boldSystemFontOfSize:25]];
    [chanrgeNumLabel setTextColor:SLMainColor];
    [self.view addSubview:chanrgeNumLabel];
    [chanrgeNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lableMoney.mas_bottom).offset(10);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];
    
    //充值金额
    UILabel *symbolMoneyLabel = [[UILabel alloc] init];
    symbolMoneyLabel.text = @"￥";
    [symbolMoneyLabel setFont:[UIFont systemFontOfSize:15]];
    [symbolMoneyLabel setTextColor:SLMainColor];
    [self.view addSubview:symbolMoneyLabel];
    [symbolMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(chanrgeNumLabel.mas_bottom).offset(-3);
        make.right.mas_equalTo(chanrgeNumLabel.mas_left).offset(2);
    }];
    //金额文字
    UILabel *lableJinE = [[UILabel alloc] init];
    lableJinE.text = @"充值后账户余额";
    lableJinE.font = [UIFont systemFontOfSize:14];
    lableJinE.textColor = UIColorFromRGB(0x888888);
    [self.view addSubview:lableJinE];
    [lableJinE mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(chanrgeNumLabel.mas_bottom).offset(60*picWScale);
        make.left.mas_equalTo(self.view.mas_left).offset(20);
    }];
    
    UILabel *lableQian = [[UILabel alloc] init];
    lableQian.textColor = UIColorFromRGB(0x81b888);
    lableQian.font = [UIFont systemFontOfSize:14];
    lableQian.text = [NSString stringWithFormat:@"¥%.2f",[self.amount doubleValue]];
    [self.view addSubview:lableQian];
    //金额数值x
    [lableQian mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lableJinE);
        make.right.mas_equalTo(self.view.mas_right).offset(-20);
    }];
    
    UIImageView *imageViewPloceHolderBttom = [[UIImageView alloc] init];
    imageViewPloceHolderBttom.backgroundColor = UIColorFromRGB(0x81b888);
    [self.view addSubview:imageViewPloceHolderBttom];
    [imageViewPloceHolderBttom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lableJinE.mas_bottom).offset(15);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(0.5);
    }];
    //订单号
    UILabel *lableDing = [[UILabel alloc] init];
    lableDing.font = [UIFont systemFontOfSize:14];
    lableDing.numberOfLines = 0;
    lableDing.textAlignment = NSTextAlignmentCenter;
    lableDing.textColor = UIColorFromRGB(0x888888);
    lableDing.text = [NSString stringWithFormat:@"订单号: "];
    [self.view addSubview:lableDing];
    [lableDing mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imageViewPloceHolderBttom.mas_bottom).offset(15);
        make.left.mas_equalTo(self.view.mas_left).offset(20);
    }];
    //订单号
    UILabel *lableNum = [[UILabel alloc] init];
    lableNum.textAlignment = NSTextAlignmentLeft;
    lableNum.textColor = SLBlackTitleColor;
    CGFloat numFont;
    if (screen_W>320) {
        numFont = 14;
    }else{
        numFont = 13;
    }
    lableNum.font = [UIFont systemFontOfSize:numFont];
    lableNum.text = [NSString stringWithFormat:@"%@",self.dict[@"out_trade_no"]];
    [self.view addSubview:lableNum];
    [lableNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(lableDing);
        make.left.mas_equalTo(lableDing.mas_right).offset(8);
    }];
    
    //最下面的按钮
    UIButton *btnSure = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnSure.layer.cornerRadius = 5;
    [btnSure setBackgroundColor:SLMainColor];
    [btnSure setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnSure setTitle:@"完成" forState:UIControlStateNormal];
    [btnSure addTarget:self action:@selector(backToMyAccount) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnSure];
    
    [btnSure mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.top.mas_equalTo(lableNum.mas_bottom).offset(60*picWScale);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.height.mas_equalTo(40);
    }];
    
}
- (void)setUpNav{
    
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"充值成功";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    [self.view addSubview:navView];
    //
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 20, 60, 44);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    //添加分割线
    UIImageView *singleView = [[UIImageView alloc] init];
    singleView.backgroundColor = SLSepatorColor;
    singleView.frame = CGRectMake(0, 63.5, ScreenW, 0.5);
    [navView addSubview:singleView];
    
}

- (void)leftBtnClick{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void) backToMyAccount
{
    UIViewController * viewVC = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 3];
    [self.navigationController popToViewController:viewVC animated:YES];
}
@end
