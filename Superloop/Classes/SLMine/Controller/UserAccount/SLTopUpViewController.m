//
//  SLTopUpViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/29.
//  Copyright © 2016年 Superloop. All rights reserved.
//   充值页面控制器

#import "SLTopUpViewController.h"
#import <Masonry/Masonry.h>
#import "WXApi.h"
#import <AFNetworking.h>
#import "AppDelegate.h"
#import <AlipaySDK/AlipaySDK.h>
#import "Order.h"
#import "DataSigner.h"
#import "PartenerConfig.h"
//#import <MBProgressHUD.h>
#import "AppDelegate.h"
#import "SLAccessViewController.h"
#import "SLPayButton.h"
#import "SLAPIHelper.h"
@interface SLTopUpViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) UIButton *selectButton;
@property (nonatomic,strong)UIView *views;
@property (nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,strong)UITextField *accountField;
@property (nonatomic,assign)float moveY;
@property (nonatomic,assign)float moveYs;
@property (nonatomic,strong)NSString *amountString;
@property (nonatomic,strong)NSString *tradeNO;
@property (nonatomic,strong)NSString *strId; //微信的订单号
@property (nonatomic,strong)NSString *aliayOrderStr; //支付宝的订单号
@property (nonatomic,strong)NSDictionary *resultDic; //支付宝支付的结果
@end
static NSString *const weixinPay = @"weixinPay";
@implementation SLTopUpViewController{
    
    NSMutableDictionary *_dict;
    UIButton *topUp;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLTopUpViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [HUDManager hideHUDView];
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLTopUpViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = SLColor(245, 245, 245);
    self.navigationItem.title = @"充值";
    [self setUpNav];
    [self setUpUI];
    
    //键盘处理
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(weixinPayCancel) name:@"weixinPaySuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aliPaySuccessed:) name:@"aliPaySuccessed" object:nil];
}

//自定义导航条
- (void)setUpNav{

    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"充值";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    [self.view addSubview:navView];
    //
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 20, 60, 44);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    //添加分割线
    UIImageView *singleView = [[UIImageView alloc] init];
    singleView.backgroundColor = SLSepatorColor;
    singleView.frame = CGRectMake(0, 63.5, ScreenW, 0.5);
    [navView addSubview:singleView];
    
}

- (void)leftBtnClick{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)upDateData{
    
    NSString *amount =_dict[@"amount"];
    //NSString *orderNo =_dict[@"prepayid"];
    NSString *orderNo = _dict[@"trade_no"];
    //支付成功需要，请求后台成功之后的同步结果问题跳转控制器
    [SLAPIHelper getAmountInfoSuccess:^(NSURLSessionDataTask *task, NSDictionary *data) {
        NSString *strAmount = [SLValueUtils stringFromObject:data[@"result"][@"account_amount"]];
        if (strAmount.length == 0) {
            strAmount = @"0.0";
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"chargeSuccess" object:nil];
        //支付成功需要，请求后台成功之后的同步结果问题跳转控制器
        SLAccessViewController *access = [[SLAccessViewController alloc] init];
        access.amount = strAmount;
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:amount forKey:@"total_fee"];
        [dict setValue:orderNo forKey:@"out_trade_no"];
        access.dict = dict;
        if (self.moneyStr) {
            self.moneyStr(strAmount);
        }
        
        
        [self.navigationController pushViewController:access animated:YES];
    } failure:^(SLHttpRequestError *failure) {
        
    }];
//    [SLAPIHelper getUsersData:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
//        
//        NSLog(@"%@",data);
//        
//        
//        
//    } failure:nil];
}


-(void)dealloc{
    NSLog(@"dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    //处理4s左右的机型键盘高度的判断
    CGRect textFrame = self.accountField.frame;
    float textY = textFrame.origin.y + textFrame.size.height;
    float bottomY = self.view.frame.size.height - textY;
//    if (iPhone5) {
//        float moveYs = keyboardRect.size.height - bottomY + 300+84;
//        self.moveYs = moveYs;
//        [UIView animateWithDuration:animationDuration animations:^{
//            //view的y轴上移
//            self.view.y += moveYs;
//            self.view.height -= moveYs;
//            
//        } completion:nil];
//    }
//    if (iPhone4 || iPhone4s) {
//        float moveY = keyboardRect.size.height - bottomY + 20+64;
//        self.moveY = moveY;
//        [UIView animateWithDuration:animationDuration animations:^{
//            //view的y轴上移
//            self.view.y += moveY ;
//            self.view.height -= moveY;
//            
//        } completion:nil];
//    }
    
}

//view被点击之后退出键盘编辑
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
    
}
- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    //处理4s左右的机型键盘高度隐藏的问题
    if (iPhone5) {
        [UIView animateWithDuration:animationDuration animations:^{
            //view的y轴上移
            self.view.y -= self.moveYs;
            self.view.height += self.moveYs;
            
        } completion:nil];
    }
    
    if (iPhone4 || iPhone4s){
        [UIView animateWithDuration:animationDuration animations:^{
            //view的y轴上移
            self.view.y -= self.moveY;
            self.view.height += self.moveY;
            
        } completion:nil];
    }
}

#pragma mark - 当用户点击了return返回编辑时候,退出键盘编辑，然后自动计算高度
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self.view endEditing:YES];
    
    return YES;
}

- (void)setUpUI
{
    
    UIView *wxView = [[UIView alloc] init];
    wxView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *wxImageView = [[UIImageView alloc] init];
    wxImageView.image = [UIImage imageNamed:@"wxPay"];
    [wxView addSubview:wxImageView];
    
    UILabel *wxLabel = [[UILabel alloc] init];
    wxLabel.text = @"微信支付";
    wxLabel.textColor = [UIColor grayColor];
    wxLabel.font = [UIFont systemFontOfSize:14];
    [wxView addSubview:wxLabel];
    
    SLPayButton *wxSel = [[SLPayButton alloc] init];
    [wxSel addTarget:self action:@selector(selectPayWay:) forControlEvents:UIControlEventTouchUpInside];
    wxSel.tag = 1000;
    [wxSel setImage:[UIImage imageNamed:@"select_nor"] forState:UIControlStateNormal];
    [wxSel setImage:[UIImage imageNamed:@"select_sel"] forState:UIControlStateSelected];
    
    [wxView addSubview:wxSel];
    [self.view addSubview:wxView];
    
    UIView *aliView = [[UIView alloc] init];
    aliView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *aliImageView = [[UIImageView alloc] init];
    aliImageView.image = [UIImage imageNamed:@"aliPay"];
    [aliView addSubview:aliImageView];
    
    
    UILabel *aliLabel = [[UILabel alloc] init];
    aliLabel.text = @"支付宝";
    aliLabel.textColor = [UIColor grayColor];
    aliLabel.font = [UIFont systemFontOfSize:14];
    [aliView addSubview:aliLabel];
    
    SLPayButton *aliSel = [[SLPayButton alloc] init];
    [aliSel addTarget:self action:@selector(selectPayWay:) forControlEvents:UIControlEventTouchUpInside];
    aliSel.tag = 2000;
    [aliSel setImage:[UIImage imageNamed:@"select_nor"] forState:UIControlStateNormal];
    [aliSel setImage:[UIImage imageNamed:@"select_sel"] forState:UIControlStateSelected];
    
    [aliView addSubview:aliSel];
    [self.view addSubview:aliView];
    
    
    UIView *accountview = [[UIView alloc] init];
    accountview.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc] init];
    label.text = @"金额(元)";
    label.font = [UIFont systemFontOfSize:20];
    [accountview addSubview:label];
    
    UITextField *accountField = [[UITextField alloc] init];
    self.accountField = accountField;
    accountField.delegate = self;
    accountField.font = [UIFont systemFontOfSize:15];
    accountField.placeholder = @"请输入金额";
    [accountview addSubview:accountField];
    [self.view addSubview:accountview];
    
    
    topUp = [UIButton buttonWithType:UIButtonTypeCustom];
    topUp.backgroundColor = SLColor(255, 255, 255);
    topUp.layer.cornerRadius = 6;
    topUp.layer.borderWidth = 0.5;
    topUp.layer.borderColor = SLColor(165, 165, 165).CGColor;
    topUp.layer.masksToBounds = YES;
    [topUp setTitle:@"充值" forState:UIControlStateNormal];
    [topUp setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [topUp addTarget:self action:@selector(topUp) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topUp];
    
    
    
    [wxView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(84);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(44);
    }];

    [wxImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wxView.mas_left).offset(10);
        make.top.mas_equalTo(wxView.mas_top).offset(5);
        make.bottom.mas_equalTo(wxView.mas_bottom).offset(-5);
        make.width.mas_equalTo(34);
    }];
    
    [wxLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wxImageView.mas_right).offset(30);
        make.centerY.mas_equalTo(wxView.mas_centerY);
    }];
    
    //微信
    [wxSel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ScreenW);
        make.height.mas_equalTo(44);
        make.right.mas_equalTo(wxView.mas_right).offset(-20);
        make.centerY.mas_equalTo(wxView.mas_centerY);
        
    }];
    
    [aliView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wxView.mas_bottom).offset(20);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(44);
    }];
    
    [aliImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(aliView.mas_left).offset(10);
        make.top.mas_equalTo(aliView.mas_top).offset(5);
        make.bottom.mas_equalTo(aliView.mas_bottom).offset(-5);
        make.width.mas_equalTo(34);
    }];
    
    [aliLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(aliImageView.mas_right).offset(30);
        make.centerY.mas_equalTo(aliView.mas_centerY);
    }];
    
    //阿里
    [aliSel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ScreenW);
        make.height.mas_equalTo(44);
        make.right.mas_equalTo(aliView.mas_right).offset(-20);
        make.centerY.mas_equalTo(aliView.mas_centerY);
        
    }];
    
    
    [accountview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(aliView.mas_bottom).offset(20);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(44);
    }];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(accountview.mas_left).offset(10);
        make.centerY.mas_equalTo(accountview.mas_centerY);
    }];
    
    [accountField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(label.mas_right).offset(20);
        make.centerY.mas_equalTo(accountview.mas_centerY);
        CGFloat margin = ScreenW - label.width - 10;
        make.width.mas_equalTo(margin);
    }];
    
    
    [topUp mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(accountview.mas_bottom).offset(40);
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.height.mas_equalTo(44);
    }];
    
    
}
- (void)selectPayWay:(UIButton *)sender
{
    _selectButton.selected = !_selectButton.selected;
    self.selectButton = sender;
    _selectButton.selected = !_selectButton.selected;
    
    if (_selectButton.tag == 1000 && _selectButton.selected == YES) {
        
        self.accountField.text = nil;
        
    }else{
        
        self.accountField.text = nil;
    }
    //在最后处理键盘结束编辑
    [self.view endEditing:YES];
    
}

#pragma mark -- 支付宝支付结果
-(void)aliPaySuccessed:(NSNotification*)notify{
    [SLAPIHelper aliPayVerify:self.tradeNO success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        [self loadData];
        
    } failure:^(SLHttpRequestError *error) {
        [HUDManager showWarningWithText:@"充值失败!"];
    }];
}

#pragma mark ---- 微信同步的结果成功之后请求服务器判断是否真实成功
- (void)weixinPayCancel{
    
    [SLAPIHelper wxPayVerify:_strId success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        
        if (data[@"result"]) {
            NSLog(@"成功: %@",data);
            //修改完成刷新h5页面
//            [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//                
//            }];
            [self upDateData];
            return;
        }
    } failure:^(SLHttpRequestError *error) {
        [HUDManager showWarningWithText:@"充值失败!"];
        return;
        
    }];
    
}

/**充值*/
- (void)topUp
{
    
    if (_selectButton.tag == 1000) {
        //选择了微信支付
        if (self.accountField.text.length == 0) {
            [HUDManager showWarningWithText:@"请输入金额"];
            return;
        }
        
        if (![WXApi isWXAppInstalled]) {
            NSLog(@"未安装微信客户端");
            return;
        }else
        {
            topUp.enabled = NO;
            [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow withText:@""];
           [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getOrderPayResult:) name:@"wxPay" object:nil];
            NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
#warning 价格修改
            NSString *amount = self.accountField.text;
            float amountCount = [amount floatValue] * 100;
            NSString *price = [NSString stringWithFormat:@"%.0f",amountCount];
            parameter[@"total_fee"] = price;  //总金额  Int
            NSLog(@"parameter--------%@",parameter);
            [SLAPIHelper wxPay:parameter method:Post success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                topUp.enabled = YES;
                [HUDManager hideHUDView];
                //调出来微信
                PayReq *wxreq = [[PayReq alloc] init];
                NSMutableString *stamp  = data[@"result"][@"params"][@"timestamp"];
                /** 商家向财付通申请的商家id */
                wxreq.openID = data[@"result"][@"params"][@"appid"];
                wxreq.partnerId = data[@"result"][@"params"][@"partnerid"];
                /** 预支付订单号 */
                wxreq.prepayId = data[@"result"][@"params"][@"prepayid"];
                
                _dict = [[NSMutableDictionary alloc] init];
                _dict[@"prepayid"] = data[@"result"][@"params"][@"prepayid"];
                //充值成功显示的订单号
                _dict[@"trade_no"]=data[@"result"][@"trade_no"];
                _dict[@"amount"] = amount;
                /** 随机串，防重发 */
                wxreq.nonceStr = data[@"result"][@"params"][@"noncestr"];
                /** 时间戳，防重发 */
                wxreq.timeStamp = stamp.intValue;
                /** 商家根据财付通文档填写的数据和签名 */
                wxreq.package = data[@"result"][@"params"][@"package"];
                /** 商家根据微信开放平台文档对数据做的签名 */
                wxreq.sign = data[@"result"][@"params"][@"sign"];
                NSLog(@"配置是appid=%@\npartid=%@\nprepayid=%@\nnoncestr=%@\ntimestamp=%ld\npackage=%@\nsign=%@",wxreq.openID,wxreq.partnerId,wxreq.
                      prepayId,wxreq.nonceStr,(long)wxreq.timeStamp,wxreq.package,wxreq.sign);
                [WXApi sendReq:wxreq];
                NSString *strId = [NSString stringWithFormat:@"%@",data[@"result"][@"params"][@"trade_no"]];
                self.strId = strId;
                
            } failure:^(SLHttpRequestError *error) {
                NSLog(@"httpStatusCode-----%ld",error.httpStatusCode);
                NSLog(@"slAPICode-----%ld",error.slAPICode);
                topUp.enabled = YES;
                [HUDManager hideHUDView];
                if (error.slAPICode == -1 && error.httpStatusCode == 500) {
                    kShowToast(@"请求失败");
                }
                
            }];
        }
    }else if (_selectButton.tag == 2000)
    {
        //以下是支付宝支付
        /*================需要填写商户app申请的===================*/
        if (self.accountField.text.length == 0) {
            [HUDManager showWarningWithText:@"请输入金额"];
            return;
        }
        // 获取支付参数
        NSString *amount = self.accountField.text;
        float amountCount = [amount floatValue];
        NSString *amountString = [NSString stringWithFormat:@"%.2f",amountCount];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        parameters[@"amount"] = amountString;
        
        [SLAPIHelper aliPay:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            NSLog(@"responseObject ==%@",data);
            //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
            NSString *appScheme = @"Superloop";
            
            self.amountString = amountString;
            NSDictionary *dict = data[@"result"];
            NSString *params = dict[@"params"];
            self.tradeNO = dict[@"trade_no"];
            //支付宝客服端的回调

            
            //网页h5支付的回调
            [[AlipaySDK defaultService] payOrder:params fromScheme:appScheme callback:^(NSDictionary *resultDic) {
                
                if ([resultDic[@"resultStatus"] isEqualToString:@"6001"]) {
                    [HUDManager showWarningWithText:@"充值取消"];
                    return;
                }

                if([resultDic[@"resultStatus"] isEqualToString:@"4000"]){
                    //充值失败
                    [HUDManager showWarningWithText:@"订单支付失败"];
                    return;
                }
                
                if([resultDic[@"resultStatus"] isEqualToString:@"6002"]){
                    //网络连接出错
                    [HUDManager showWarningWithText:@"网络连接出错"];
                    return;
                }
               
                [SLAPIHelper aliPayVerify:data[@"trade_no"] success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                    
                    if (dict) {
                        [self loadData];
                        return;
                        
                    }
                } failure:^(SLHttpRequestError *error) {
                    [HUDManager showWarningWithText:@"充值失败!"];
                    
                }];
                
            }];
        } failure:nil];
        
    }
    
}

#pragma mark - 获取充值之后的金额
- (void)loadData {
    NSDictionary *parameters =  @{@"id": ApplicationDelegate.userId};
    [SLAPIHelper getUsersData:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSLog(@"%@", data);
        
        NSString *strAmount = [SLValueUtils stringFromObject:data[@"result"][@"amount"]];
        if (strAmount.length == 0) {
            strAmount = @"0.0";
        }
        //支付成功需要，请求后台成功之后的同步结果问题跳转控制器
        SLAccessViewController *access = [[SLAccessViewController alloc] init];
        access.amount = strAmount;
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:self.amountString forKey:@"total_fee"];
        [dict setValue:self.tradeNO forKey:@"out_trade_no"];
        access.dict = dict;
        access.isAlipay = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"chargeSuccess" object:nil];
        //修改完成刷新h5页面
        if(self.moneyStr){
            self.moneyStr(strAmount);
        }
        
//        [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//            
//        }];
        [self.navigationController pushViewController:access animated:YES];
        
        
    } failure:nil];
}

#pragma mark - 验证不能输入文字等东西
- (BOOL)validateNumber{
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    int i = 0;
    while (i < self.accountField.text.length) {
        
        NSString * string = [self.accountField.text substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            [HUDManager showWarningWithText:@"请按照正确的格式输入!"];
            
            break;
        }
        
        i++;
    }
    
    return res;
}

- (void)getOrderPayResult:(NSNotification *)notification{
    
    if ([notification.object isEqualToString:@"success"])
        
    {
        [self alert:@"恭喜" msg:@"您已成功支付啦!"];
    }else
    {
        [self alert:@"提示" msg:@"支付失败"];
    }
    
}

- (void)alert:(NSString *)title msg:(NSString *)msg

{
    UIAlertView *alter = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alter show];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    //不能输入大于200的金额
    BOOL isHaveDian = YES;
    
    if ([textField.text rangeOfString:@"."].location == NSNotFound) {
        isHaveDian = NO;
    }
    if ([string length] > 0) {
        
        unichar single = [string characterAtIndex:0];//当前输入的字符
        if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
            //不能输入大于200的金额
            NSString *amount = [NSString stringWithFormat:@"%@%@",textField.text,string];
            float amountCount = [amount floatValue];
            if (amountCount > 5000) {
                
                return NO;
            }
            
            //首字母不能为0和小数点1
            if([textField.text length] == 0){
                if(single == '.') {
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            
            //输入的字符是否是小数点
            if (single == '.') {
                if(!isHaveDian)//text中还没有小数点
                {
                    return YES;
                    
                }else{
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                if (isHaveDian) {//存在小数点
                    //判断小数点的位数
                    NSRange ran = [textField.text rangeOfString:@"."];
                    if (range.location - ran.location <= 2) {
                        
                        return YES;
                        
                    }else{
                        return NO;
                    }
                }else{
                    return YES;
                }
            }
        }else{//输入的数据格式不正确
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }

}

@end
