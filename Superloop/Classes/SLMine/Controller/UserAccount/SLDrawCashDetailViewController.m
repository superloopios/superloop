//
//  SLDrawCashDetailViewController.m
//  Superloop
//
//  Created by 朱宏伟 on 16/6/2.
//  重构  xiaowu  16/11/08
//  Copyright © 2016年 Superloop. All rights reserved.
//   提现详情控制器

#import "SLDrawCashDetailViewController.h"
#import "SLAPIHelper.h"
#import "SLNormalQustionViewController.h"
#import "SLValueUtils.h"
#import "SLCustomTableViewCell.h"
#import "SLCustomModel.h"

@interface SLDrawCashDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong)UILabel             *drawTextLabel;
@property (nonatomic, strong)UITableView         *tableView;
@property (nonatomic, strong)NSMutableArray      *modelsArr;

@end

@implementation SLDrawCashDetailViewController


- (NSMutableArray *)modelsArr{
    if (!_modelsArr) {
        _modelsArr = [NSMutableArray array];
    }
    return _modelsArr;
}

- (UILabel *)drawTextLabel{
    if (!_drawTextLabel) {
        _drawTextLabel = [[UILabel alloc] init];
        _drawTextLabel.frame = CGRectMake(10, 0, 65, 52.5);
        _drawTextLabel.textAlignment = NSTextAlignmentCenter;
        _drawTextLabel.text = @"提现金额:";
        _drawTextLabel.textColor = UIColorFromRGB(0x888888);
        _drawTextLabel.font = [UIFont systemFontOfSize:14];
    }
    return _drawTextLabel;
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.frame = CGRectMake(0, 64, screen_W, screen_H-64);
        _tableView.backgroundColor = SLColor(245, 245, 245);
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[SLCustomTableViewCell class] forCellReuseIdentifier:@"customCell"];
        
    }
    return _tableView;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLDrawCashDetailViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLDrawCashDetailViewController.h"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNav];
    [self.view addSubview:self.tableView];
    //self.navigationItem.title = @"提现";
    self.view.backgroundColor = SLColor(245, 245, 245);
    
    //    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithImage:[UIImage imageNamed:@"barButton"] highImage:[UIImage imageNamed:@"barButton"] target:self action:@selector(rightBarButtonItemClick)];
    
    [self getTradeDetail];
    //    NSLog(@"%@",self.dict);
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"提现";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIButton *rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    [rightBtn setImage:[UIImage imageNamed:@"barButton"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBarButtonItemClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:rightBtn];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)getTradeDetail{
    
    NSDictionary *parameters = @{@"tradeNo":self.trade_No};
    [SLAPIHelper getWithdrawDetail:parameters method:Get success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        [self addFristModels:data];
        [self addSecondModels:data];
        [self addThreeModels:data];
//        NSLog(@"%@",data)
    } failure:^(SLHttpRequestError *error) {
        
    }];
}
- (void)addFristModels:(NSDictionary *)dict{
    NSMutableArray *arr = [NSMutableArray array];
    for (int i = 0; i<3; i++) {
//        NSLog(@"%d",i)
        SLCustomModel *model = [[SLCustomModel alloc] init];
        if (i == 0) {
            model.leftTitle = @"提现金额:";
            CGFloat amount = [dict[@"result"][@"amount"] doubleValue];
            model.rightTitle = [NSString stringWithFormat:@"￥%.2f",amount];
        }else if(i == 1){
            model.leftTitle = @"订单号:";
            model.rightTitle = dict[@"result"][@"trade_no"];
        }else if(i == 2){
            NSInteger startTime = [dict[@"result"][@"updated"] integerValue];
            model.leftColor = SLBlackTitleColor;
            model.leftTitle = [self getTimeStr:startTime format:@"yyyy年MM月dd日"];
            NSInteger status = [dict[@"result"][@"status"] integerValue];
            NSString *strStatus;
            if (status == 1) {
                strStatus = @"已申请";
            }else if (status == 2) {
                strStatus = @"被拒绝";
            }else if (status == 3) {
                strStatus = @"处理中";
            }else if (status == 4) {
                strStatus = @"交易已完成";
            }
            model.rightTitle = strStatus;
            model.rightColor = SLMainColor;
        }
        [arr addObject:model];
    }
    [self.modelsArr addObject:arr];
}
- (void)addSecondModels:(NSDictionary *)dict{
    NSMutableArray *arr = [NSMutableArray array];
    NSDictionary *detailDict = dict[@"result"][@"details"];
    if (detailDict[@"withdraw"]) {
        for (int i = 0; i<2; i++) {
            SLCustomModel *model = [[SLCustomModel alloc] init];
            if (i == 0) {
                model.leftTitle = @"转账金额:";
                CGFloat amount = [detailDict[@"withdraw"][@"amount"] doubleValue];
                model.rightTitle = [NSString stringWithFormat:@"￥%.2f",amount];
            }else if(i == 1){
                model.leftTitle = @"转账至支付宝:";
                model.rightTitle = detailDict[@"withdraw"][@"account"];
            }
            [arr addObject:model];
        }
        [self.modelsArr addObject:arr];
    }
    
}
- (void)addThreeModels:(NSDictionary *)dict{
    
    NSMutableArray *arr = [NSMutableArray array];
    NSDictionary *detailDict = dict[@"result"][@"details"];
    if (detailDict[@"refunds"]) {
        SLCustomModel *model = [[SLCustomModel alloc] init];
        model.leftTitle = @"退款金额:";
        NSDictionary *refundsDict = detailDict[@"refunds"];
        CGFloat amount = [refundsDict[@"amount"] doubleValue];
        model.rightTitle = [NSString stringWithFormat:@"￥%.2f",amount];
        [arr addObject:model];
        
        NSArray *refundsDetailsArr = refundsDict[@"details"];
        for (NSDictionary *refundsDetailDict in refundsDetailsArr) {
            SLCustomModel *model = [[SLCustomModel alloc] init];
            model.leftTitle = [NSString stringWithFormat:@"转账至%@:",refundsDetailDict[@"account"]];
            CGFloat amount = [refundsDetailDict[@"amount"] doubleValue];
            model.rightTitle = [NSString stringWithFormat:@"￥%.2f",amount];
            [arr addObject:model];
        }
        [self.modelsArr addObject:arr];
    }
    
    [self.tableView reloadData];
}
//右边导航按钮的点击事件
- (void)rightBarButtonItemClick{
    SLNormalQustionViewController * nomalVc = [[SLNormalQustionViewController alloc] init];
    [self.navigationController pushViewController:nomalVc animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customCell"];
    SLCustomModel *model = self.modelsArr[indexPath.section][indexPath.row];
    cell.model = model;
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.modelsArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSMutableArray *arr = self.modelsArr[section];
    return arr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 52.5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
// 时间转换
- (NSString *)getTimeStr:(NSInteger)startTime format:(NSString *)format{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:startTime/1000.0];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSTimeZone * zone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];//转为东八区
    [fmt setTimeZone:zone];
    fmt.dateFormat = format;
    NSString *dateStr = [fmt stringFromDate:date];
    NSDate *createdAtDate = [fmt dateFromString:dateStr];
    
    return [fmt stringFromDate:createdAtDate];
}
@end
