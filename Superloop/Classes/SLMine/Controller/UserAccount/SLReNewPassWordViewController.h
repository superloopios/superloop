//
//  SLReNewPassWordViewController.h
//  Superloop
//
//  Created by 朱宏伟 on 16/5/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//   设置支付密码控制器

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface SLReNewPassWordViewController : UIViewController
@property(nonatomic,copy)NSString *titleName;
@property(nonatomic,assign)BOOL isComeFromMessageVc; //是从消息控制器过来
@property(nonatomic,copy)void (^resetSuccessBlock)(BOOL resetSuccess);
@end
