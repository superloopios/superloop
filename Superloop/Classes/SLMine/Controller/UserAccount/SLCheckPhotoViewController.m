//
//  SLCheckPhotoViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/30.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLCheckPhotoViewController.h"
#import <Masonry.h>
#import "TZImagePickerController.h"
#import "SLCheckViewCell.h"
#import "SLImageCollectionView.h"
#import "SLCheckImageCell.h"

#import "AppDelegate.h"
#import <AliyunOSSiOS/OSSService.h>
#import "SLAPIHelper.h"
#import <AVFoundation/AVCaptureDevice.h>
#import <AVFoundation/AVMediaFormat.h>
#import "Utils.h"
#import "NSString+Utils.h"

@interface SLCheckPhotoViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate,TZImagePickerControllerDelegate,SLCheckViewCellDelegate,SLCheckImageCellDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *_selectedPhotosIcar;
    NSMutableArray *_selectedPhotosFace;
    NSMutableArray *_selectedPhotosCertificate;
    NSMutableArray *_pictures;
    __block  NSInteger _num;
    UIButton *_submitBtn;

}
@property (nonatomic, strong) NSData *data;
@property (nonatomic, assign) NSIndexPath *indexPath;

@property (nonatomic, copy)NSString *frontPhotoName;
@property (nonatomic, copy)NSString *backPhotoName;
@property (nonatomic, copy)NSString *frontFaceName;
@property (nonatomic, copy)NSString *professionalLicenseName;

@property (nonatomic,strong)UITextField *trueNameText;
@property (nonatomic,strong)UITextField *idCarTextField;
@property (nonatomic,strong)NSMutableArray *imgDataSources;
@property (nonatomic,strong)UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *photoArrays;
@property (nonatomic, strong) NSMutableArray *selectedPhotoNamesIcar;
@property (nonatomic, strong) NSMutableArray *selectedPhotoNamesFace;
@property (nonatomic, strong) NSMutableArray *selectedPhotoNamesCertificate;

@end

@implementation SLCheckPhotoViewController
-(NSMutableArray *)photoArrays{
    if (!_photoArrays) {
        _photoArrays=[NSMutableArray new];
    }
    return _photoArrays;
}
-(NSMutableArray *)selectedPhotoNamesIcar{
    if (!_selectedPhotoNamesIcar) {
        _selectedPhotoNamesIcar=[NSMutableArray new];
    }
    return _selectedPhotoNamesIcar;
}
-(NSMutableArray *)selectedPhotoNamesFace{
    if (!_selectedPhotoNamesFace) {
        _selectedPhotoNamesFace=[NSMutableArray new];
    }
    return _selectedPhotoNamesFace;
}
-(NSMutableArray *)selectedPhotoNamesCertificate{
    if (!_selectedPhotoNamesCertificate) {
        _selectedPhotoNamesCertificate=[NSMutableArray new];
    }
    return _selectedPhotoNamesCertificate;
}
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"SLCheckViewCell" bundle:nil] forCellReuseIdentifier:@"checkCell"];
        _tableView.frame = CGRectMake(0, 64, screen_W, screen_H - 64);
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _selectedPhotosIcar = [[NSMutableArray alloc] init];
    _selectedPhotosFace = [[NSMutableArray alloc] init];
    _selectedPhotosCertificate = [[NSMutableArray alloc] init];
    _pictures = [[NSMutableArray alloc] init];
    _imgDataSources=[[NSMutableArray alloc] init];
    [_pictures addObject:_selectedPhotosIcar];
    [_pictures addObject:_selectedPhotosFace];
    [_pictures addObject:_selectedPhotosCertificate];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"认证审核";
    
//    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@"提交" style:UIBarButtonItemStylePlain target:self action:@selector(submitBtnClick)];
    [self setUpNav];
    [self setUpUI];
    
    
    
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"认证审核";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    [self.view addSubview:navView];
    //
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 20, 60, 44);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    
    _submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    
    [_submitBtn setTitle:@"提交" forState:UIControlStateNormal];
    _submitBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_submitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(submit:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:_submitBtn];
    //添加分割线
    UIImageView *singleView = [[UIImageView alloc] init];
    singleView.backgroundColor = SLSepatorColor;
    singleView.frame = CGRectMake(0, 63.5, ScreenW, 0.5);
    [navView addSubview:singleView];
    
}
//发布按钮
- (void)submit:(UIButton *)sender{
    _submitBtn.enabled=NO;
    //先将未到时间执行前的任务取消。
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(submitBtnClick) object:sender];
    [self performSelector:@selector(submitBtnClick) withObject:nil afterDelay:0.3f];
    
}
- (void)leftBtnClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setUpUI
{
    /*************nameView*****************************/
    
    UIView *nameView = [[UIView alloc] init];
    nameView.frame = CGRectMake(0, 64, ScreenW, 92);
    nameView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *cutImageView = [[UIImageView alloc] init];
    cutImageView.backgroundColor = SLColor(240, 240, 240);
    [nameView addSubview:cutImageView];
    
    UIImageView *botImgeView = [[UIImageView alloc] init];
    botImgeView.backgroundColor = SLColor(240, 240, 240);
    [nameView addSubview:botImgeView];
    
    UILabel *trueName = [[UILabel alloc] init];
    trueName.text = @"真实姓名";
    trueName.font=[UIFont systemFontOfSize:15];
    trueName.textColor=SLColor(127, 127, 127);
    [nameView addSubview:trueName];
    
    UILabel *idCarLabel = [[UILabel alloc] init];
    idCarLabel.text = @"身份证号";
    idCarLabel.font=[UIFont systemFontOfSize:15];
    idCarLabel.textColor=SLColor(127, 127, 127);
    [nameView addSubview:idCarLabel];
    
    UITextField *trueNameText = [[UITextField alloc] init];
    trueNameText.placeholder = @"请输入您的真实姓名";
    self.trueNameText=trueNameText;
    trueNameText.font=[UIFont systemFontOfSize:14];

    [nameView addSubview:trueNameText];
    
    UITextField *idCarTextField = [[UITextField alloc] init];
    idCarTextField.placeholder = @"请输入您的身份证号";
    self.idCarTextField=idCarTextField;
    idCarTextField.font=[UIFont systemFontOfSize:14];

    [nameView addSubview:idCarTextField];
    
    self.tableView.tableHeaderView = nameView;

//    UIView *footView = [[UIView alloc] init];
//    footView.backgroundColor = [UIColor whiteColor];
//    footView.frame = CGRectMake(0, 0, ScreenW, 160);
    UIView *footView = [[UIView alloc] init];
    footView.backgroundColor = [UIColor whiteColor];
    footView.frame = CGRectMake(0, 0, ScreenW, 1);
    
//    UIButton *submit = [UIButton buttonWithType:UIButtonTypeCustom];
//    submit.backgroundColor = SLColor(240, 240, 240);
//    [submit setTitle:@"提交审核" forState:UIControlStateNormal];
//    [submit setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [submit addTarget:self action:@selector(submitBtnClick) forControlEvents:UIControlEventTouchUpInside];
//    submit.layer.cornerRadius = 6;
//    submit.layer.masksToBounds = YES;
//    [footView addSubview:submit];
    self.tableView.tableFooterView = footView;
    
    
    /*************nameView*****************************/
    
    
    [trueName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameView.mas_left).offset(10);
//        make.top.mas_equalTo(nameView.mas_top).offset(32);
        make.top.mas_equalTo(nameView.mas_top).offset(15);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(65);
    }];
    
    [idCarLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameView.mas_left).offset(10);
        make.bottom.mas_equalTo(nameView.mas_bottom).offset(-15);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(65);
    }];
    
    [cutImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(nameView.mas_top).offset(62);
        make.top.mas_equalTo(trueName.mas_bottom).offset(15);
        make.right.mas_equalTo(nameView.mas_right);
        make.left.mas_equalTo(trueName.mas_right);
        make.height.mas_equalTo(1);
    }];
    
//    [botImgeView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.mas_equalTo(nameView.mas_bottom);
//        make.right.mas_equalTo(nameView.mas_right);
//        make.left.mas_equalTo(trueName.mas_right);
//        make.height.mas_equalTo(1);
//    }];
    
    [trueNameText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(trueName.mas_right).offset(35);
//        make.top.mas_equalTo(nameView.mas_top).offset(32);
        make.top.mas_equalTo(nameView.mas_top).offset(15);
        make.right.mas_equalTo(nameView.mas_right);
        make.height.mas_equalTo(15);
    }];
    
    [idCarTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(idCarLabel.mas_right).offset(35);
        make.right.mas_equalTo(nameView.mas_right);
        make.bottom.mas_equalTo(nameView.mas_bottom).offset(-15);
        make.height.mas_equalTo(15);
    }];
//    [submit mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(footView.mas_top).offset(34);
//        make.centerX.mas_equalTo(footView.mas_centerX);
//        make.width.mas_equalTo(250);
//        make.height.mas_equalTo(44);
//    }];
//    
  
}

-(void)submitBtnClick
{
    if (![Utils checkUserIdCard:_idCarTextField.text]) {
        kShowToast(@"身份证号格式错误");
        _submitBtn.enabled=YES;
        return;
    }
    if ((_selectedPhotosIcar.count==2)&&(_selectedPhotosFace.count==1)&&(_trueNameText.text.length>0)&&(_selectedPhotosCertificate.count<=1)) {
    self.professionalLicenseName =@"";
#ifdef DEBUG
        NSString *endpoint = @"http://dev.oss.superloop.com.cn";
#else
        NSString *endpoint = @"http://private.superloop.com.cn";
#endif
        
    id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:@"UPhdoMR1eN1hnrdA" secretKey:@"ETuIdMVCT1mtiqs8f5KZ8dZnZLiXfG"];
    OSSClient *client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential];

    for (NSData *data in _selectedPhotosIcar) {
        [_imgDataSources addObject:data];
    }
    for (NSData *data in _selectedPhotosFace) {
        [_imgDataSources addObject:data];
    }
    for (NSData *data in _selectedPhotosCertificate) {
        [_imgDataSources addObject:data];
    }
        dispatch_async(dispatch_get_main_queue(), ^{
            UIWindow *win = [[UIApplication sharedApplication].windows objectAtIndex:0];
            [HUDManager showLoadingHUDView:win withText:@""];
        });
       
        for (int i=0; i<_imgDataSources.count; i++) {
            NSData *data = _imgDataSources[i];
            OSSPutObjectRequest *put = [OSSPutObjectRequest new];
#ifdef DEBUG
            put.bucketName = @"superloop-dev";
#else
            put.bucketName = @"superloop-private";
#endif
            
            
//            [self saveImage:_imgDataSources[i] withName:lower];
            
            put.objectKey = [NSString stringWithFormat:@"auth/%@/%@.jpg",ApplicationDelegate.userId,[NSString getMD5WithData:data]];

            switch (i) {
                case 0:
                    self.frontPhotoName=put.objectKey;

                    break;
                case 1:
                     self.backPhotoName=put.objectKey;
                    break;
                case 2:
                    self.frontFaceName=put.objectKey;
                    break;
//                case 3:
//                    //self.professionalLicenseName=put.objectKey;
//                    break;
                default:
                      self.professionalLicenseName = [self.professionalLicenseName stringByAppendingString:[@"&query_type=" stringByAppendingString:put.objectKey]];
                    break;
            }
            put.uploadingData = data;
            put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
            };
            OSSTask * putTask = [client putObject:put];
           
            [putTask continueWithBlock:^id(OSSTask *task) {
                if (!task.error) {
                    _num++;
                    if (_num==_imgDataSources.count) {
                        _num=0;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [HUDManager hideHUDView];
                            //[HUDManager showWarningWithText:@"上传图片成功"];
                        });
                        [self postDatas];

                    }

                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [HUDManager hideHUDView];
                        [HUDManager showWarningWithText:@"上传失败"];
                        _submitBtn.enabled=YES;
                    });
                    
                }
                return nil;
            }];
        }
        
    }else{
        _submitBtn.enabled=YES;
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请按照提示输入需上传照片张数以及有效的姓名和身份证号" delegate:self cancelButtonTitle:@"好" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)postDatas
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIWindow *win = [[UIApplication sharedApplication].windows objectAtIndex:0];
        [HUDManager showLoadingHUDView:win withText:@""];
    });
    //1.确定url
    // NSURL *url = [NSURL URLWithString:@"http://dev.superloop.com.cn/topics"];
    NSURL *url = [NSURL URLWithString:[URL_ROOT_PATIENT stringByAppendingString:@"/users/verification"]];
    //2.创建可变请求对象
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    //3.修改请求方法,POST是需要大写的
    request.HTTPMethod = @"POST";
    //设置请求头信息
    [request setValue:[NSString stringWithFormat:@"Basic %@",ApplicationDelegate.Basic] forHTTPHeaderField:@"Authorization"];
    //4.设置请求体
    NSString *httpBodyStr = [NSString stringWithFormat:@"real_name=%@&id_number=%@&id_front_photo=%@&id_back_photo=%@&id_front_face=%@%@", _trueNameText.text,_idCarTextField.text,_frontPhotoName,_backPhotoName, _frontFaceName, _professionalLicenseName];
    request.HTTPBody =[httpBodyStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"---------httpBodyStr-------%@",httpBodyStr);
    //    //发送请求之前先判断一下
    //    //5.发送请求
    NSURLSession *session=[NSURLSession sharedSession];
    NSURLSessionTask *task=[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error==nil) {
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            NSInteger responseStatusCode = [httpResponse statusCode];
            NSLog(@"responseStatusCode---------%ld",responseStatusCode);
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"dict-----------%@",dict);
            
            if (responseStatusCode==200) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"checked" object:self];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MobClick event:@"submit_audit_count"];
                    [HUDManager hideHUDView];
                    [HUDManager showWarningWithText:@"提交成功"];
                    if (self.changeVer) {
                        self.changeVer(@"认证中");
                    }
                    [self.navigationController popViewControllerAnimated:YES];
                    
                });
            }
            if(responseStatusCode==400 && [dict[@"code"] integerValue] == 1){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUDManager hideHUDView];
                    [HUDManager showWarningWithText:@"身份证号格式错误"];
                    _submitBtn.enabled=YES;
                });
            }
            if(responseStatusCode==409 && [dict[@"code"] integerValue] == 210){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUDManager hideHUDView];
                    [HUDManager showWarningWithText:@"该身份证号码已认证，请勿重复认证"];
                    _submitBtn.enabled=YES;
                });
            }
            
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUDManager hideHUDView];
                [HUDManager showWarningWithText:@"请求失败,请稍后重试"];
                _submitBtn.enabled=YES;
            });
            
        }
        
    }];
    [task resume];
 
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 __weak SLCheckViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"checkCell" forIndexPath:indexPath];
    cell.indexpath = indexPath;
    cell.Delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.imgArr=_pictures[indexPath.row];
//    __weak typeof(cell) weakCell = cell;
    __weak typeof(self) weakSelf = self;
    cell.deleteBlock=^(NSMutableArray *mArr){
//        weakCell.imgArr=mArr;
        
        [weakSelf.tableView reloadData];
    };
    
    NSMutableArray *array = _pictures[indexPath.row];
    NSInteger count = array.count;
    if (indexPath.row == 0) {
        cell.titltLabel.text = @"请上传身份证正反面照片(共2张)";
    }else if (indexPath.row == 1)
    {
        cell.titltLabel.text = @"请上传手持身份证的正面脸照(共1张)";

    }else
    {
        cell.titltLabel.text = @"请上传职业资格证明等其他材料(如无可不添)";
    }
    [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath count:count];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *array = _pictures[indexPath.row];
    NSInteger count = array.count;
    if (count > 0&&count<4) {
        return 225;
    }else if (count>3){
        return 225+110+10;
    }
    return 101;
}
//#pragma mark -- UICollectionViewDataSource
////定义展示的UICollectionViewCell的个数
//-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
//{
//    SLImageCollectionView * cellCollectionView  = (SLImageCollectionView *)collectionView;
//    NSMutableArray *array = _pictures[cellCollectionView.indexPath.row];
//    return array.count;
//}
////定义每个UICollectionView 的大小
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return CGSizeMake((ScreenW - 45) / 2, 110);
//}


////每个UICollectionView展示的内容
//-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    SLImageCollectionView * cellCollectionView  = (SLImageCollectionView *)collectionView;
//    SLCheckImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCell" forIndexPath:indexPath];
//    cell.indexPath = indexPath;
//    cell.collectionViewIndexPath = cellCollectionView.indexPath;
//    cell.Delegate = self;
//    cell.imageView.image = _pictures[cellCollectionView.indexPath.row][indexPath.row];
//    return cell;
//}

- (void)didcamerClicked:(NSIndexPath *)indexpath
{
    self.indexPath = indexpath;
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:imagePickerController animated:YES completion:^{}];

}
- (void)didPictureClicked:(NSIndexPath *)indexpath
{
    self.indexPath = indexpath;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger number = 0;
    if (indexpath.row == 0) {
        array = _selectedPhotosIcar;
        number = 2;
    }else if (indexpath.row == 1)
    {
        number = 1;
        array = _selectedPhotosFace;
    }else
    {
        number = 1;
        //number = 6;
        array = _selectedPhotosCertificate;
    }
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:number :array.count delegate:self ];
    [self presentViewController:imagePickerVc animated:YES completion:nil];
    
}
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets
{
    
    if (_indexPath.row == 0) {
        for (UIImage *img in photos) {
            NSData *data = UIImageJPEGRepresentation(img,0.01);
            [_selectedPhotosIcar addObject:data];
            [self.selectedPhotoNamesIcar addObject:[NSString getMD5WithData:data]];
        }
    }else if (_indexPath.row == 1)
    {
        for (UIImage *img in photos) {
            NSData *data = UIImageJPEGRepresentation(img,0.01);
            [_selectedPhotosFace addObject:data];
            [self.selectedPhotoNamesFace addObject:[NSString getMD5WithData:data]];
        }
    }else
    {
        for (UIImage *img in photos) {
            NSData *data = UIImageJPEGRepresentation(img,0.01);
            [_selectedPhotosCertificate addObject:data];
            [self.selectedPhotoNamesCertificate addObject:[NSString getMD5WithData:data]];
        }
    }
    [self.tableView reloadData];
}
#pragma mark - image picker delegte
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{}];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    if (_indexPath.row == 0) {
        NSData *data = UIImageJPEGRepresentation(image,0.01);
        [_selectedPhotosIcar addObject:data];
        [self.selectedPhotoNamesIcar addObject:[NSString getMD5WithData:data]];
    }else if (_indexPath.row == 1){
        NSData *data = UIImageJPEGRepresentation(image,0.01);
        [_selectedPhotosFace addObject:data];
        [self.selectedPhotoNamesFace addObject:[NSString getMD5WithData:data]];
    }else{
        NSData *data = UIImageJPEGRepresentation(image,0.01);
        [_selectedPhotosCertificate addObject:data];
        [self.selectedPhotoNamesCertificate addObject:[NSString getMD5WithData:data]];
    }
    [self.tableView reloadData];
   }
//- (void)didCancelClick:(NSIndexPath *)indexPath collectionViewIndexPath:(NSIndexPath *)collectionViewIndexPath
//{
//    NSMutableArray *array = _pictures[collectionViewIndexPath.row];
//    UIImage *image = array[indexPath.row];
//  
//    [array removeObject:image];
//    [self.tableView reloadData];
//}
#pragma mark - 保存图片至沙盒
- (void) saveImage:(UIImage *)currentImage withName:(NSString *)imageName
{
    
    NSData *imageData = UIImageJPEGRepresentation(currentImage, 0.01);
    // 获取沙盒目录
    NSString *fullPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:imageName];
    // 将图片写入文件
    
    [imageData writeToFile:fullPath atomically:NO];
    NSData *data = [NSData dataWithContentsOfFile:fullPath];
    self.data = data;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
}

@end
