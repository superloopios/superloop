//
//  SLReNewPassWordViewController.m
//  Superloop
//
//  Created by 朱宏伟 on 16/5/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLReNewPassWordViewController.h"
#import "AppDelegate.h"
#import "SLAPIHelper.h"
#import "SLMessagePayController.h"
#import "AESCrypt.h"
@interface SLReNewPassWordViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *orignPwd;
@property (weak,nonatomic) IBOutlet UITextField *myText;
@property (weak, nonatomic) IBOutlet UITextField *surePwdText;
@property (strong, nonatomic)  UIView *passwordView;
@property (strong, nonatomic)  UIView *surepasswordView;
@property (weak, nonatomic) IBOutlet UITextField *textPassWord;
@property (nonatomic, strong)NSMutableArray *passwordIndicatorArrary;
@property (nonatomic, strong)NSMutableArray *surepasswordIndicatorArrary;
@property (weak, nonatomic) IBOutlet UIButton *btnGetPassWord;
@property (assign, nonatomic)BOOL hasNetWork;  //判断是否有网络
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstrain; //顶部约束
@property (weak, nonatomic) IBOutlet UILabel *topPlaceHolderLable;
@property (weak, nonatomic) IBOutlet UITextField *textFiledTestPwd;
@property (weak, nonatomic) IBOutlet UILabel *testPwdLbl;
@property (nonatomic,strong)UIBarButtonItem * rightBtn;
@property (nonatomic,strong)UIButton *btnRight;
@end

@implementation SLReNewPassWordViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"SLReNewPassWordViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLReNewPassWordViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    if (self.titleName) {
//        self.navigationItem.title = self.titleName;
//    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setUpNav];
    _passwordIndicatorArrary = [NSMutableArray array];
    _surepasswordIndicatorArrary = [NSMutableArray array];
    _myText.tag = 301;
    _surePwdText.tag = 302;
    _textPassWord.keyboardType = UIKeyboardTypeNumberPad;
    _textPassWord.delegate = self;
    _surePwdText.delegate = self;
    _myText.delegate = self;
    _surePwdText.keyboardType = UIKeyboardTypeNumberPad;
//    [self initViewWithFrame:_myText.frame withTextField:_myText withView:_passwordView withArr:_passwordIndicatorArrary placeHolderStr:@"输入6位数字的新支付密码"];
//    [self initViewWithFrame:_surePwdText.frame withTextField:_surePwdText withView:_surepasswordView withArr:_surepasswordIndicatorArrary placeHolderStr:@"输入确认新支付密码"];

//    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [backButton setTitle:@"确认" forState:UIControlStateNormal];
//    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
//    [backButton sizeToFit];
//    [backButton addTarget:self action:@selector(btnSure:) forControlEvents:UIControlEventTouchUpInside];
//    // 设置按钮内容内边距
//    backButton.contentEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
//    self.rightBtn =[[UIBarButtonItem alloc] initWithTitle:@"确认" style:UIBarButtonItemStylePlain target:self action:@selector(btnSure:)];
//    NSDictionary *dict1 = @{
//                            NSFontAttributeName : [UIFont systemFontOfSize:16],
//                            NSForegroundColorAttributeName : [UIColor blackColor]
//                            };
//    [ self.rightBtn setTitleTextAttributes:dict1 forState:UIControlStateNormal];
//    self.navigationItem.rightBarButtonItem = _rightBtn;
    
    
//    _myText.hidden = YES;
//    _surePwdText.hidden= YES;
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
}
//设置导航条
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
//    nameLab.text=@"设置支付密码";
    nameLab.text=self.titleName;
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    [self.view addSubview:navView];
    //
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 20, 60, 44);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    //添加分割线
    UIImageView *singleView = [[UIImageView alloc] init];
    singleView.backgroundColor = [UIColor lightGrayColor];
    singleView.frame = CGRectMake(0, 63.5, ScreenW, 0.5);
    [navView addSubview:singleView];
    _btnRight=[UIButton buttonWithType:UIButtonTypeCustom];
    _btnRight.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    [_btnRight setTitle:@"确认" forState:UIControlStateNormal];
    _btnRight.titleLabel.font=[UIFont systemFontOfSize:15];
    [_btnRight setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnRight addTarget:self action:@selector(btnSure:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:_btnRight];
}

- (void)leftBtnClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)startTimer
{
    __block int timeout = 60; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout <= 0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [_btnGetPassWord setTitle:@"重新获取" forState:UIControlStateNormal];
                [_btnGetPassWord setTitle:@"重新获取" forState:UIControlStateDisabled];
                [_btnGetPassWord setEnabled:YES];
            });
        }else{
            NSString *strTime = [NSString stringWithFormat:@"%d秒", timeout];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [_btnGetPassWord setTitle:strTime forState:UIControlStateDisabled];
                [_btnGetPassWord setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
                [_btnGetPassWord setEnabled:NO];
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

//获取验证码
- (IBAction)btnGetPassWord:(UIButton *)sender {
    _btnGetPassWord.enabled = NO;
    //先将未到时间执行前的任务取消。
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(getMessageVertifer:) object:sender];
    [self performSelector:@selector(getMessageVertifer:) withObject:sender afterDelay:0.2f];
}

//确定按钮的点击事件
- (void)btnSure:(UIButton *)sender{
    _btnRight.enabled=NO;
    //先将未到时间执行前的任务取消。
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(postVertifer:) object:sender];
    [self performSelector:@selector(postVertifer:) withObject:sender afterDelay:0.2f];
    
}


#pragma mark ---设置校验
- (BOOL)vertifer{

    if (_textPassWord.text.length!=6||_surePwdText.text.length!=6) {
        [HUDManager showWarningWithText:@"验证码必须是六位"];
        _btnRight.enabled=YES;
        return NO;
    }
    if (![_myText.text isEqualToString:_surePwdText.text]){
        [HUDManager showWarningWithText:@"两次输入支付密码不相同"];
        _btnRight.enabled=YES;
        return NO;
    }
    if (_myText.text.length!=6||_surePwdText.text.length!=6) {
        [HUDManager showWarningWithText:@"支付密码必须是六位"];
        _btnRight.enabled=YES;
        return NO;
    }
    
    
    return YES;
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *textString = textField.text ;
    NSUInteger length = [textString length];
    
    BOOL bChange =YES;
    if (length >= 6)
        bChange = NO;
    
    if (range.length == 1) {
        bChange = YES;
    }
    return bChange;
}


// 密码输入框
//- (void)initViewWithFrame:(CGRect)rect withTextField:(UITextField *)textField withView:(UIView *)frontView withArr:(NSMutableArray *)pointArr
//{
//    
//    
//    UIView *view =[[UIView alloc]initWithFrame:rect];
//
//    
//    UIView *viewbefore = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, rect.size.height)];
//    frontView = viewbefore;
//    
//    UITextField *textF = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, WIDTH, rect.size.height)];
//    textF.tag = textField.tag;
//    textField = textF;
//    
//    //    pointArr = [[NSMutableArray alloc] init];
////    textField.delegate = self;
//    frontView.userInteractionEnabled = NO;
//    //    _passwordTextfield.hidden = YES;
//    [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
//    CGFloat width = (frontView.frame.size.width - 5) / 6;
//    for (int i = 0; i < 5; i++) {
//        UIImageView *lineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(width + (1 + width) * i, 1, 1, frontView.frame.size.height)];
//        lineImageView.backgroundColor = SLColor(232, 232, 232);
//        [frontView addSubview:lineImageView];
//    }
//    for (int i = 0; i < 6; i ++) {
//        UIImageView *dotImageView = [[UIImageView alloc] initWithFrame:CGRectMake((width - 10) / 2 + (width + 1) * i, (frontView.frame.size.height - 10) / 2, 10, 10 )];
//        dotImageView.layer.cornerRadius = 5;
//        dotImageView.layer.masksToBounds = YES;
//        dotImageView.hidden = YES;
//        dotImageView.backgroundColor = [UIColor blackColor];
//        [frontView addSubview:dotImageView];
//        [pointArr addObject:dotImageView];
//    }
//    [view addSubview:textField];
//    UIView *backview = [[UIView alloc]initWithFrame:CGRectMake(0, 1, rect.size.width, rect.size.height)];
//    backview.backgroundColor = [UIColor whiteColor];
//    backview.userInteractionEnabled = NO;
//    [view addSubview:backview];
//    [view addSubview:frontView];
//    [self.view addSubview:view];
//}

#pragma mark ---- 处理防止多次点击按钮的事件
- (void)getMessageVertifer:(id)sender
{
    if (!self.hasNetWork) {
        [HUDManager showWarningWithText:@"请检查网络状况"];
        return;
    }
    [_btnGetPassWord setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
    //重置验证码
    [SLAPIHelper reSetDealPwd:nil success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        //[_btnGetPassWord setEnabled:NO];
        [self startTimer];
        _btnGetPassWord.enabled = YES;
    } failure:^(SLHttpRequestError *failure) {
        _btnGetPassWord.enabled = YES;
        
        if (failure.httpStatusCode==429&&failure.slAPICode==10) {
            [HUDManager showWarningWithText:@"操作频繁,请稍后重试"];
        }else{
            
            [HUDManager showWarningWithText:@"获取验证码失败"];
        }
    }];
}


#pragma mark ---- 检测网络状态

- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetWork = NO;
        }else if ([netWorkStaus isEqualToString:@"wifi"]||[netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.hasNetWork = YES;
        }
    }
}

#pragma mark ---- 处理防止多次点击确认提交密码时候
- (void)postVertifer:(id)sender{
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow];
    NSMutableDictionary *parametersForDeal = [NSMutableDictionary dictionary];
    if ([self vertifer]) {
        //对应的加密密钥
        __block  NSString *security=@"";
        //获取AES加密密钥
        NSString *uuid= [[NSUUID UUID] UUIDString];
        //减去"-"
        NSString *tokenStr = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSDictionary *params=@{@"token":tokenStr};
        [SLAPIHelper getAESSecurity:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            security = data[@"result"];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSString *encryptCode = [AESCrypt encrypt:_textPassWord.text password:security];
                NSString *encryptPassword = [AESCrypt encrypt:_myText.text password:security];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [parametersForDeal setObject:encryptCode forKey:@"code"];
                    [parametersForDeal setObject:encryptPassword forKey:@"password"];
                    [parametersForDeal setObject:tokenStr forKey:@"token"];
                    [self encryptSuccess:parametersForDeal];
                    
                });
            });
        } failure:^(SLHttpRequestError *failure) {
            [HUDManager hideHUDView];
            _btnRight.enabled=YES;
            [HUDManager showWarningWithText:@"支付密码修改失败"];
        }];
    }
}

//加密成功后登陆
-(void)encryptSuccess:(NSDictionary *)parametersForDeal{
    
    [SLAPIHelper setDealPwd:parametersForDeal success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSUserDefaults  *stands = [NSUserDefaults standardUserDefaults];
        [stands setObject:_myText.text forKey:@"Sure"];
        [stands synchronize];
        [HUDManager showWarningWithText:@"支付密码重置成功!"];
        [HUDManager hideHUDView];
        if (self.resetSuccessBlock) {
            self.resetSuccessBlock(YES);
        }
        //更新支付密码状态
        [self resetDealpasswordstatus];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (self.isComeFromMessageVc) {
                //如果是从支付消息余额不足过来的
                SLMessagePayController *newHome;
                
                for (UIViewController *vc in self.navigationController.viewControllers) {
                    if ([vc isKindOfClass:[SLMessagePayController class]]) {
                        newHome = (SLMessagePayController *)vc;
                    }
                }
                [self.navigationController popToViewController:newHome animated:YES];
                
            }else{
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        });
        
    } failure:^(SLHttpRequestError *error) {
        _btnRight.enabled=YES;
        [HUDManager hideHUDView];
        [HUDManager showWarningWithText:@"支付密码修改失败!"];
    }];

}
- (void)resetDealpasswordstatus{
    NSDictionary *pargram = @{@"deal_password_status":@(1)};
    [SLAPIHelper resetUsers:pargram success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        //更改沙盒中支付状态
        NSDictionary *dict = [SLUserDefault getUserInfo];
        NSString *basic = dict[@"basic"];
        NSString *Password = dict[@"Password"];
        NSMutableDictionary *userInformation = [[NSMutableDictionary alloc] initWithDictionary: dict[@"userInformation"]];
        [userInformation setValue:@"1" forKey:@"deal_password_status"];
        NSDictionary *newUserInfo = @{@"basic": basic, @"userInformation": userInformation,@"Password": Password};
        [SLUserDefault setUserInfo:newUserInfo];
    } failure:^(SLHttpRequestError *failure) {
        
    }];
}

-(void)viewDidDisappear:(BOOL)animated{
    [HUDManager hideHUDView];
}
//-(void)textFieldDidChange :(UITextField *)theTextField{
//
//    NSLog(@"%ld", theTextField.text.length);
//    
//    if (theTextField.text.length > 6) {
//        theTextField.text = [ theTextField.text substringToIndex:6];
//    }
//    if (theTextField.tag ==301) {
//        _myText.text =theTextField.text;
//        for (UIImageView *dotImageView in _passwordIndicatorArrary)
//        {
//            dotImageView.hidden = YES;
//        }
//        
//        for (int i = 0; i< theTextField.text.length; i++)
//        {
//            ((UIImageView*)[_passwordIndicatorArrary objectAtIndex:i]).hidden = NO;
//        }
//    } else {
//        _surePwdText.text = theTextField.text;
//        for (UIImageView *dotImageView in _surepasswordIndicatorArrary)
//        {
//            dotImageView.hidden = YES;
//        }
//        for (int i = 0; i< theTextField.text.length; i++)
//        {
//            ((UIImageView*)[_surepasswordIndicatorArrary objectAtIndex:i]).hidden = NO;
//        }
//    }
//    
//    
//    
//    
//    
//    //    if (_passwordTextfield.text.length == 6) {
//    //        _passwordTextfield.enabled = NO;
//    //    }
//}

@end
