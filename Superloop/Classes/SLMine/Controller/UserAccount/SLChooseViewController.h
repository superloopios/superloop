//
//  SLChooseViewController.h
//  Superloop
//
//  Created by 朱宏伟 on 16/6/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLBaseTableViewController.h"
@interface SLChooseViewController :UIViewController

@property (nonatomic,strong)void(^drawCashAccount)(NSString *drawCashAccount);

@property (nonatomic,strong)NSString *accountStr;
@property (nonatomic,strong)NSString *name;


@end
  