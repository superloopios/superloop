//
//  SLAliyDrawCashNumViewController.h
//  Superloop
//
//  Created by 朱宏伟 on 16/6/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLAliyDrawCashNumViewController : UIViewController


@property (nonatomic,strong)void(^drawCashAccount)(NSString *drawCashAccount,NSString *drawName);
@property (nonatomic,strong)NSString *account;
@property (nonatomic,strong)NSString *name;
@end
