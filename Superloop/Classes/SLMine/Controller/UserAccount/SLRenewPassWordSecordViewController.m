//
//  SLRenewPassWordSecordViewController.m
//  Superloop
//
//  Created by 朱宏伟 on 16/5/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//  第一次设置交易密码

#import "SLRenewPassWordSecordViewController.h"
#import "AppDelegate.h"
#import "SLAPIHelper.h"
@interface SLRenewPassWordSecordViewController ()

@property (weak, nonatomic) IBOutlet UITextField *passWordTextFiled;


@property (weak, nonatomic) IBOutlet UITextField *sureTextFiled;

@property (weak, nonatomic) IBOutlet UIButton *btnSure;

@end

@implementation SLRenewPassWordSecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   self.navigationItem.title = @"设置支付密码";
    

}

//确认修改密码
- (IBAction)btnSureClick:(id)sender {
    //是第一次设置验证码
  
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:[NSString stringWithFormat:@"%@",_sureTextFiled.text]forKey:@"password"];
    if ([self vertifer]) {
       [SLAPIHelper setDealPwd:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
          // NSLog(@"设置密码成功---->>%@", data);
           NSUserDefaults *stands = [NSUserDefaults standardUserDefaults];
           [stands setObject:[NSString stringWithFormat:@"%@",_sureTextFiled.text] forKey:@"Sure"];
           [stands synchronize];
           dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
               
               [self.navigationController popViewControllerAnimated:YES];
               
           });

       } failure:^(SLHttpRequestError *error) {
           NSUserDefaults *stands = [NSUserDefaults standardUserDefaults];
           [stands setObject:[NSString stringWithFormat:@"%@",_sureTextFiled.text] forKey:@"Sure"];
           [stands synchronize];
           
          // NSLog(@"错误是%@",error);
       }];

    }
    

}


#pragma mark -- ---- 设置验证
- (BOOL)vertifer{
    
    if (![_passWordTextFiled.text isEqualToString:_sureTextFiled.text]) {
        //如果两个输入的密码数据是不相符合的情况
        [HUDManager showWarningWithText:@"密码不匹配!"];
        return NO;
    }
    
    
    
    return YES;
}





@end
