//
//  SLUserAccountViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/29.
//  Copyright © 2016年 Superloop. All rights reserved.
//   账户余额控制器
#import "SLUserAccountViewController.h"
#import <Masonry.h>
#import "SLTopUpViewController.h"
#import "SLDrawCashViewController.h"
#import  <AFNetworking.h>
#import "AppDelegate.h"
#import "SLValueUtils.h"
#import "SLReNewPassWordViewController.h"
#import "SLRenewPassWordSecordViewController.h"

#import "SLAPIHelper.h"
#import "SLCheckPhotoViewController.h"
#import "SLNavgationViewController.h"

@interface SLUserAccountViewController ()
@property (nonatomic, strong) UILabel *label;

@property (nonatomic,assign)BOOL isChecked; //是否去认证过了
//是否是已经设置过支付密码了(0是没有，1是有)
@property (nonatomic,strong) NSString *deal_password_status;

@end

@implementation SLUserAccountViewController


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLUserAccountViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLUserAccountViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = SLColor(255, 255, 255);
    [self setUpNav];
 
    if ([self.type isEqualToString:@"1"]) {
        // 左
        UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];

        [leftButton setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
        leftButton.imageEdgeInsets = UIEdgeInsetsMake(0, -50, 0, 0);
        [leftButton setFrame:CGRectMake(0, 0, 50, 30)];
        [leftButton addTarget:self
                       action:@selector(goback)
             forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
        
        self.navigationItem.leftBarButtonItem = leftItem;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkedOff) name:@"checked" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserInfo) name:@"chargeSuccess" object:nil];
    // 右
    [self addUI];
    [self getUserInfo];
   
}
- (void)getUserInfo{
    if (!ApplicationDelegate.userId) {
        return;
    }
    [SLAPIHelper getAmountInfoSuccess:^(NSURLSessionDataTask *task, NSDictionary *data) {
        self.label.text = [NSString stringWithFormat:@"%.2f",[data[@"result"][@"account_amount"] doubleValue]];
    } failure:^(SLHttpRequestError *failure) {
        
    }];
}
- (void)setUpNav{
    
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"账户余额";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    [self.view addSubview:navView];
    //
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 20, 60, 44);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    
    
    
    //添加分割线
    UIImageView *singleView = [[UIImageView alloc] init];
    singleView.backgroundColor = SLSepatorColor;
    singleView.frame = CGRectMake(0, 63.5, ScreenW, 0.5);
    [navView addSubview:singleView];
    
}

- (void)leftBtnClick{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)goback{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addUI
{
    UIImageView *accountView = [[UIImageView alloc] init];
    accountView.image = [UIImage imageNamed:@"money"];
    [self.view addSubview:accountView];
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"账户余额";
    label.textColor = UIColorFromRGB(0x81b888);
    label.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:label];
    
    UILabel *accountLabel = [[UILabel alloc] init];
    accountLabel.text = @"0";
    accountLabel.font = [UIFont systemFontOfSize:43];
    accountLabel.textColor = UIColorFromRGB(0x2b2124);
    accountLabel.text = [NSString stringWithFormat:@"%.2f",[self.amount doubleValue]];
    
    self.label = accountLabel;
    [self.view addSubview:accountLabel];
    
    UILabel *account = [[UILabel alloc] init];
    account.text = @"¥";
    account.font = [UIFont systemFontOfSize:21];
    account.textColor = UIColorFromRGB(0x2b2124);
    [self.view addSubview:account];
    
    UIButton *topUp = [UIButton buttonWithType:UIButtonTypeCustom];
    topUp.backgroundColor = SLMainColor;
    topUp.layer.cornerRadius = 6;
    topUp.titleLabel.font = [UIFont systemFontOfSize:14];
    [topUp setTitle:@"充值" forState:UIControlStateNormal];
    [topUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [topUp addTarget:self action:@selector(topUp) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topUp];
    
    UIButton *drawCash = [UIButton buttonWithType:UIButtonTypeCustom];
    drawCash.backgroundColor = [UIColor whiteColor];
    [drawCash setTitleColor:SLMainColor forState:UIControlStateNormal];
    drawCash.layer.borderColor = SLMainColor.CGColor;
    drawCash.layer.borderWidth = 0.5;
    drawCash.layer.cornerRadius = 6;
    [drawCash setTitle:@"提现" forState:UIControlStateNormal];
    drawCash.titleLabel.font = [UIFont systemFontOfSize:14];
    [drawCash addTarget:self action:@selector(drawCash) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:drawCash];
    //设置支付密码
    UIButton *btnSettingPassWord = [[UIButton alloc] init];
    btnSettingPassWord.backgroundColor = [UIColor clearColor];
    
    NSString *str = [NSString stringWithFormat:@"%@",self.userData[@"deal_password_status"]];
    btnSettingPassWord.titleLabel.font = [UIFont systemFontOfSize:14];
    if ([str isEqualToString:@"1"]) {
        //不是第一次登陆了
        [btnSettingPassWord setTitle:@"支付密码重置" forState:UIControlStateNormal];
    }else{
        //是第一次登陆
       [btnSettingPassWord setTitle:@"设置支付密码" forState:UIControlStateNormal];
    }
    
    [btnSettingPassWord setTitleColor:SLMainColor forState:UIControlStateNormal];;
    [btnSettingPassWord addTarget:self action:@selector(reNewPassWord) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnSettingPassWord];
    //右边的图片框显示
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageNamed:@"Down-Arrow-outline"];
    [self.view addSubview:imageView];
   
    [accountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(105);
        make.width.height.mas_equalTo(95);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(accountView.mas_bottom).offset(20);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        
    }];
    
    [accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label).offset(20);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    
    }];
    
    [account mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(accountLabel.mas_left);
        make.bottom.mas_equalTo(accountLabel.mas_bottom).offset(-6);
    }];
    
    if (iPhone4) {
        [topUp mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.view.mas_left).offset(20);
            make.right.mas_equalTo(self.view.mas_right).offset(-20);
            make.top.mas_equalTo(accountLabel.mas_bottom).offset(50);
            make.height.mas_equalTo(40);
            
        }];
    }else{
        [topUp mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.view.mas_left).offset(20);
            make.right.mas_equalTo(self.view.mas_right).offset(-20);
            make.top.mas_equalTo(accountLabel.mas_bottom).offset(50);
            make.height.mas_equalTo(40);
            
        }];
        
    }
   
    
    [drawCash mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(20);
        make.right.mas_equalTo(self.view.mas_right).offset(-20);
        make.top.mas_equalTo(topUp.mas_bottom).offset(25);
        make.height.mas_equalTo(40);
    }];

    
    //设置密码的布局代码
    [btnSettingPassWord mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.top.mas_equalTo(drawCash.mas_bottom).offset(10);
        
    }];
    
    //支付密码重置
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(btnSettingPassWord.mas_right).offset(5);
        make.centerY.mas_equalTo(btnSettingPassWord.mas_centerY).offset(0);
        make.width.mas_equalTo(6);
        make.height.mas_equalTo(11);
    }];
}

/**充值*/
- (void)topUp
{
    SLTopUpViewController *topUp = [[SLTopUpViewController alloc] init];
    
    __weak typeof(self) weakSelf = self;
    topUp.moneyStr= ^(NSString *str){
        
        weakSelf.label.text = [NSString stringWithFormat:@"%.2f",[str doubleValue]];
    };

    [self.navigationController pushViewController:topUp animated:YES];
    
}
/**提现*/
- (void)drawCash
{
    //判断如果没有认证需要先认证
    NSString *vertifer_Status = [NSString stringWithFormat:@"%@",_userData[@"verification_status"]];
    
    if (self.isChecked) {
        //如果是去认证完成返回到本控制器，提示认证中
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"提示" message:@"认证审核中,请耐心等待" delegate:self cancelButtonTitle:@"好" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    if ([vertifer_Status isEqualToString:@"2"]) {
        //已经认证才能操作提现
        SLDrawCashViewController *drawCash = [[SLDrawCashViewController alloc] init];
        [self.navigationController pushViewController:drawCash animated:YES];
        
    }else if ([vertifer_Status isEqualToString:@"0"]){
        
       UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先认证审核" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"去认证", nil];
        [alert show];
    }else if ([vertifer_Status isEqualToString:@"1"]){
        
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"提示" message:@"认证审核中,请耐心等待" delegate:self cancelButtonTitle:@"好" otherButtonTitles:nil, nil];
        [alert show];
    }
    

}

//重置密码

- (void)reNewPassWord{
    
    NSString *str = [NSString stringWithFormat:@"%@",self.userData[@"deal_password_status"]];

      if ([str isEqualToString:@"1"]) {
        //说明不是第一次登陆了
        SLReNewPassWordViewController *reNewFirstVc = [[SLReNewPassWordViewController alloc] init];
        reNewFirstVc.titleName = @"支付密码重置";
        
        [self.navigationController pushViewController:reNewFirstVc animated:YES];

    }else{
        //是第一次登陆
        SLReNewPassWordViewController *reNewFirstVc = [[SLReNewPassWordViewController alloc] init];
        reNewFirstVc.titleName = @"设置支付密码";
        [self.navigationController pushViewController:reNewFirstVc animated:YES];
    }
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {

        SLCheckPhotoViewController *vc=[[SLCheckPhotoViewController alloc] init];

        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
    }
}

#pragma mark --- 审核认证完成
- (void)checkedOff{
    
    self.isChecked = YES;
    
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
@end
