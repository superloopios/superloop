//
//  SLPriceViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/29.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLPriceViewController.h"
#import <Masonry.h>
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "SLAPIHelper.h"
@interface SLPriceViewController ()<UITextFieldDelegate,UITableViewDelegate>
@property (nonatomic,strong) UITextField *priceText;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) UILabel *priceLabel;
@property (nonatomic,strong) UITableView *tableView;
@end

@implementation SLPriceViewController

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, ScreenW, ScreenH) style:UITableViewStylePlain];
        _tableView.delegate=self;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLPriceViewController"];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLPriceViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.view.backgroundColor = [UIColor whiteColor];
//    self.navigationItem.title = @"设置通话费用";
    [self setUpNav];
    [self setUpUI];
    [self loadData];
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"设置通话费用";
    
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)loadData
{
    [SLAPIHelper getPriceData:ApplicationDelegate.userId success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
       // NSLog(@"%@", data);
        self.price =  [NSString stringWithFormat:@"%@",data[@"result"][@"price"]];
       // NSLog(@"__ %@",self.price);
        _priceLabel.text = [NSString stringWithFormat:@"%@",self.price];
        [self.tableView reloadData];
    } failure:nil];
   
}
- (void)setUpUI
{
    UIView *backgroundView = [[UIView alloc] init];
    backgroundView.frame = self.view.bounds;
    backgroundView.height -= 247;
    self.tableView.tableFooterView = backgroundView;

    
    UIImageView *topCutImage = [[UIImageView alloc] init];
    topCutImage.backgroundColor = [UIColor grayColor];
    [backgroundView addSubview:topCutImage];
    
    UILabel *cellLabel = [[UILabel alloc] init];
    cellLabel.text = @"我当前收取的通话费用为:";
    cellLabel.font = [UIFont systemFontOfSize:18];
    cellLabel.textColor = SLColor(98, 98, 98);
    [backgroundView addSubview:cellLabel];
    
    UILabel *priceLabel = [[UILabel alloc] init];
    priceLabel.textAlignment = NSTextAlignmentRight;
    self.priceLabel = priceLabel;
    priceLabel.font = [UIFont systemFontOfSize:22];
    priceLabel.textColor = SLColor(0, 0, 90);
    [backgroundView addSubview:priceLabel];
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"元/分";
    label.font = [UIFont systemFontOfSize:18];
    label.textColor = SLColor(98, 98, 98);
    [backgroundView addSubview:label];
    
    UIImageView *midCutImage = [[UIImageView alloc] init];
    midCutImage.backgroundColor = [UIColor grayColor];
    [backgroundView addSubview:midCutImage];
    
    UITextField *accountLabelLeft = [[UITextField alloc] init];
    accountLabelLeft.delegate =self;
    accountLabelLeft.keyboardType = UIKeyboardTypeNumberPad;
//    accountLabelLeft.placeholder = @"更改通话费用";
    self.priceText = accountLabelLeft;
     accountLabelLeft.font = [UIFont systemFontOfSize:20];
    NSDictionary *placeHolderDic = @{NSForegroundColorAttributeName:[UIColor lightGrayColor], NSFontAttributeName:[UIFont systemFontOfSize:15]};
    accountLabelLeft .attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"更改通话费用" attributes:placeHolderDic];
    accountLabelLeft.textAlignment = NSTextAlignmentCenter;
    accountLabelLeft.textColor =[UIColor blackColor];
    accountLabelLeft.backgroundColor = SLColor(247, 247, 247);
    [backgroundView addSubview:accountLabelLeft];
    
    UITextField *accountLabelTop = [[UITextField alloc] init];
    accountLabelTop.textAlignment = NSTextAlignmentCenter;
    [backgroundView addSubview:accountLabelTop];
    UIImageView *accountImage = [[UIImageView alloc] init];
    accountImage.backgroundColor = [UIColor grayColor];
    [backgroundView addSubview:accountImage];
    
//    UILabel *accountLabelRight = [[UILabel alloc] init];
//    accountLabelRight.font = [UIFont systemFontOfSize:18];
//    accountLabelRight.text = @"元/分钟";
//    accountLabelRight.textColor = SLColor(98, 98, 98);
//    [backgroundView addSubview:accountLabelRight];
    
    UIImageView *botCutImage = [[UIImageView alloc] init];
    botCutImage.backgroundColor = SLColor(20, 106, 90);
    [backgroundView addSubview:botCutImage];
    
    UIButton *saveSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    saveSetting.backgroundColor = SLColor(239, 239, 239);
    saveSetting.layer.cornerRadius = 5;
    saveSetting.layer.masksToBounds = YES;
    [saveSetting setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [saveSetting addTarget:self action:@selector(savePrice) forControlEvents:UIControlEventTouchUpInside];
    [saveSetting setTitle:@"保存设置" forState:UIControlStateNormal];
    [backgroundView addSubview:saveSetting];
    
    
    /**
     添加约束
     */
    
//    [topCutImage mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(backgroundView.mas_top).offset(15);
//        make.left.mas_equalTo(backgroundView.mas_left).offset(10);
//        make.right.mas_equalTo(backgroundView.mas_right).offset(-10);
//        make.height.mas_equalTo(0.5);
//    }];
    //最上边的lbale
    [cellLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX).mas_offset(-30);
        make.top.mas_equalTo(self.view.mas_top).offset(110);
    }];
    
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(cellLabel.mas_right).mas_offset(3);
        make.top.mas_equalTo(self.view.mas_top).offset(107);
    }];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(priceLabel.mas_right);
       make.top.mas_equalTo(self.view.mas_top).offset(110);
    }];
    
//    [midCutImage mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(priceLabel.mas_bottom).offset(70);
//        make.left.mas_equalTo(topCutImage.mas_left);
//        make.right.mas_equalTo(topCutImage.mas_right);
//        make.height.mas_equalTo(0.5);
//    }];
    
    [accountLabelLeft mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(backgroundView.mas_left);
        make.right.mas_equalTo(backgroundView.mas_right);
        make.height.mas_equalTo(46);
        make.top.mas_equalTo(cellLabel.mas_bottom).offset(20);
    }];
    
//    [accountImage mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(accountLabelLeft.mas_right);
//        make.bottom.mas_equalTo(accountLabelLeft.mas_bottom);
//        make.height.mas_equalTo(0.5);
//        make.width.mas_equalTo(60);
//    }];
//    
//    [accountLabelTop mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(accountLabelLeft.mas_right);
//        make.bottom.mas_equalTo(accountImage.mas_top);
//        make.width.mas_equalTo(accountImage.mas_width);
//    }];
//    
////    [accountLabelRight mas_makeConstraints:^(MASConstraintMaker *make) {
////        make.left.mas_equalTo(accountImage.mas_right);
////        make.top.mas_equalTo(midCutImage.mas_bottom).offset(15);
////    }];
//    
//    [botCutImage mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(topCutImage.mas_left);
//        make.right.mas_equalTo(topCutImage.mas_right);
//        make.height.mas_equalTo(0.5);
//        make.top.mas_equalTo(accountLabelLeft.mas_bottom).offset(15);
//    }];
    
    [saveSetting mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(backgroundView.mas_left).mas_offset(30);
        make.right.mas_equalTo(backgroundView.mas_right).mas_offset(-30);
        make.height.mas_equalTo(44);
        make.top.mas_equalTo(accountLabelLeft.mas_bottom).offset(50);
    }];
  
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}
- (void)savePrice
{
    NSDictionary *parameters = @{@"price":self.priceText.text};
    NSString *strVerification = [NSString stringWithFormat:@"%@",_userData[@"verification_status"]];
    //判断认证状态
    
    if ([strVerification isEqualToString:@"2"]) {
        //已认证
        if ([self.priceText.text doubleValue] > 100) {
            [self addTitle:@"您最多可设置100元，如您希望更高的收费金额，请您联系客服"];
            return;
        }
        
        if ([self.priceText.text doubleValue] < 2){
            [self addTitle:@"请至少设置2元的金额"];
            
            return;
        }
        
    }else{
        //未认证，认证中
        if ([self.priceText.text doubleValue] > 10) {
            [self addTitle:@"您可设置的最高金额为10元，如您希望更高的收费，请进行认证审核"];
            return;
        }
        
        if ([self.priceText.text doubleValue] < 2){
            
            [self addTitle:@"请至少设置2元的金额"];
            return;
        }
        
        
    }
    [SLAPIHelper resetUsers:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSString *string = [NSString stringWithFormat:@"%@",self.priceText.text];
       // NSLog(@"%@",string);
        NSUserDefaults *stands = [NSUserDefaults standardUserDefaults];
        [stands setObject:string forKey:@"price"];
        //        [ApplicationDelegate.userInformation setObject:self.priceText.text forKey:@"price"];
        //在这跟新h5的数据刷新
        if (self.changePrice) {
            self.changePrice([NSString stringWithFormat:@"%@元/次",string]);
        }
                
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager showWarningWithText:@"请求失败"];
    }];
    
}


#pragma mark ----- 设置验证字符串只能输入数字
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHaveDian = YES;
    
    if ([textField.text rangeOfString:@"."].location == NSNotFound) {
        isHaveDian = NO;
    }
    
    
    if ([string length] > 0) {
        
        unichar single = [string characterAtIndex:0];//当前输入的字符
        if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
          
            //首字母不能为0和小数点1
            if([textField.text length] == 0){
                if(single == '.') {
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            
            //输入的字符是否是小数点
            if (single == '.') {
                if(!isHaveDian)//text中还没有小数点
                {
//                    isHaveDian = YES;
                    return YES;
                    
                }else{
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                if (isHaveDian) {//存在小数点
                    
                    //判断小数点的位数
                    NSRange ran = [textField.text rangeOfString:@"."];
                    if (range.location - ran.location <= 1) {
                        return YES;
                    }else{
                        return NO;
                    }
                }else{
                    return YES;
                }
            }
        }else{//输入的数据格式不正确
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }
}


- (void)addTitle:(NSString *)title{
    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:title delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil, nil];
    [alert show];
    

}

@end
