//
//  SLDrawCashDetailViewController.h
//  Superloop
//
//  Created by 朱宏伟 on 16/6/2.
//  重构  xiaowu  16/11/08
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface SLDrawCashDetailViewController : BaseViewController

@property (nonatomic,strong)NSString *trade_No;

@end
