//
//  SLHistoryCashCouponsViewController.m
//  Superloop
//
//  Created by xiaowu on 16/9/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLHistoryCashCouponsViewController.h"
#import "SLCashCouponTableViewCell.h"
#import "SLCashCouponModel.h"

@interface SLHistoryCashCouponsViewController ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic, strong)UITableView *tableView;

@end

@implementation SLHistoryCashCouponsViewController

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 74, screen_W, screen_H-64) style:UITableViewStylePlain];
        [_tableView registerClass:[SLCashCouponTableViewCell class] forCellReuseIdentifier:@"cashCouponCell"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor=SLColor(244, 244, 244);
        _tableView.rowHeight = 87;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = SLColor(244, 244, 244);
    [self setUpNav];
    [self.tableView reloadData];
}

- (void)setUpNav{
    
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"历史现金券";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    [self.view addSubview:navView];
    //
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 20, 60, 44);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    
    //添加分割线
    UIImageView *singleView = [[UIImageView alloc] init];
    singleView.backgroundColor = SLSepatorColor;
    singleView.frame = CGRectMake(0, 63.5, ScreenW, 0.5);
    [navView addSubview:singleView];
}
- (void)leftBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLCashCouponTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cashCouponCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    SLCashCouponModel *model = self.arr[indexPath.row];
    cell.model = model;
    return cell;
}

@end
