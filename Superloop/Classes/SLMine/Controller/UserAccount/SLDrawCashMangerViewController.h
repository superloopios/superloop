//
//  SLDrawCashMangerViewController.h
//  Superloop
//
//  Created by 朱宏伟 on 16/6/7.
//  Copyright © 2016年 Superloop. All rights reserved.
// 管理提现账号

#import <UIKit/UIKit.h>


@interface SLDrawCashMangerViewController : UIViewController

@property (nonatomic,strong)NSString *account;
@property (nonatomic,strong)NSString *name;

@property (nonatomic,assign)BOOL isComeFromAccountManger;

@property (nonatomic,strong)void(^drawCashAccountWithName)(NSString *drawCashAccount,NSString *drawName); //提现账号和姓名
@end
