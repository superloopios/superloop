//
//  SLAliyDrawCashNumViewController.m
//  Superloop
//
//  Created by 朱宏伟 on 16/6/2.
//  Copyright © 2016年 Superloop. All rights reserved.
//  阿里的支付账号

#import "SLAliyDrawCashNumViewController.h"
#import "UITextField+SLTextFiled.h"
#import "SLAPIHelper.h"
#import "SLDrawCashViewController.h"
@interface SLAliyDrawCashNumViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textFiledNum;

@property (weak, nonatomic) IBOutlet UITextField *textFiledName;

@property (weak, nonatomic) IBOutlet UIButton *sureBtn;

@property (nonatomic,strong) NSString *accountstr;
@property (nonatomic,strong) NSString *strName;

@end

@implementation SLAliyDrawCashNumViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"SLAliyDrawCashNumViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLAliyDrawCashNumViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNav];
    self.navigationItem.title = @"管理支付宝账号";
    [self setupUI];
    self.accountstr = [SLUserDefault getStrAccout];
    
    self.strName = [SLUserDefault getStrAccountName];
    
    if (self.account.length > 0) {
        self.textFiledNum.userInteractionEnabled = NO;
        self.textFiledName.userInteractionEnabled = NO;
        self.textFiledNum.text = [NSString stringWithFormat:@"%@",self.account];
        self.textFiledName.text = [NSString stringWithFormat:@"%@",self.name];
        self.sureBtn.hidden = YES;
    }
    
    if (self.accountstr.length >0) {
        self.textFiledNum.userInteractionEnabled = NO;
        self.textFiledName.userInteractionEnabled = NO;
        self.textFiledNum.text = [NSString stringWithFormat:@"%@",self.accountstr];
        self.textFiledName.text = [NSString stringWithFormat:@"%@",self.strName];
        self.sureBtn.hidden = YES;
        
    }
    
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"管理支付宝账号";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}

- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupUI{
    
    UIImageView *imageViewUserName=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 30)];
    imageViewUserName.backgroundColor = [UIColor whiteColor];
    self.textFiledNum.leftView=imageViewUserName;
    self.textFiledNum.leftViewMode=UITextFieldViewModeAlways;
    
    UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 30)];
    imgView.backgroundColor = [UIColor whiteColor];
    self.textFiledName.leftView = imgView;
    self.textFiledName.leftViewMode=UITextFieldViewModeAlways; //此处用来设置leftview的显示
    
    
}

- (void)submit{
    self.sureBtn.enabled = NO;
    
    NSDictionary *parameters = @{@"alipay_account":self.textFiledNum.text,@"real_name":self.textFiledName.text};
    [SLAPIHelper setAliayNum:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        
        //请求成功返回账号
        self.drawCashAccount(self.textFiledNum.text,self.textFiledName.text);
        
        [HUDManager showWarningWithText:@"设置账号成功"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"settedAccount" object:self];
        
        SLDrawCashViewController *newHome;

        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isKindOfClass:[SLDrawCashViewController class]]) {
                newHome = (SLDrawCashViewController *)vc;
            }
        }
        [self.navigationController popToViewController:newHome animated:YES];
        
        
    } failure:^(SLHttpRequestError *failure) {
        NSLog(@"+++++%ld",(long)failure.slAPICode);
        self.sureBtn.enabled = YES;
    }];
}
- (IBAction)sureClick:(id)sender {
    
    [self submit];
    
}


@end
