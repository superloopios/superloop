//
//  SLChooseViewController.m
//  Superloop
//
//  Created by 朱宏伟 on 16/6/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//  选择提现账号控制器

#import "SLChooseViewController.h"
#import "SLDrawCashMangerViewController.h"
#import "SLCrawCashAccountBtn.h"
#import "SLDrawCashMangerViewCell.h"
#import "SLAliyDrawCashNumViewController.h"
#import "SLAPIHelper.h"
#import "UILabel+StringFrame.h"
#import "SLValueUtils.h"
static NSString *const indentifiers = @"cells";
@interface SLChooseViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic)SLCrawCashAccountBtn *btnGoAdd;

@property (nonatomic,strong)NSString *strAccount;

@property (nonatomic,strong)NSString *account;//账号
@property (nonatomic,strong)NSString *strName; //名字

@property (nonatomic,strong)SLCrawCashAccountBtn *btnSetting;

@property(nonatomic, weak)UITableView *tableView;

@end

@implementation SLChooseViewController

- (UITableView *)tableView
{
    if (!_tableView) {
        UITableView *tableview = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, screen_W, screen_H-64) style:UITableViewStylePlain];
        [self.view addSubview:tableview];
        tableview.dataSource = self;
        tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableview.delegate = self;
        _tableView = tableview;
    }
    return _tableView;
}


- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //self.title = @"选择提现账号";
    [self setUpNav];
    self.account = [SLUserDefault getStrAccout];
    self.strName = [SLUserDefault getStrAccountName];
    if (self.account.length == 0) {
        
        [self getAccountHistory];
        
    }
    [self.tableView registerNib:[UINib nibWithNibName:@"SLDrawCashMangerViewCell" bundle:nil] forCellReuseIdentifier:indentifiers];

   
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"选择提现账号";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    [self.view addSubview:navView];
    //
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 20, 60, 44);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    //添加分割线
    UIImageView *singleView = [[UIImageView alloc] init];
    singleView.backgroundColor = SLSepatorColor;
    singleView.frame = CGRectMake(0, 63.5, ScreenW, 0.5);
    [navView addSubview:singleView];
    
}
- (void)leftBtnClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getAccountHistory{
    
    NSDictionary *parameters = nil;
    [SLAPIHelper getWithdrawAccountHistorys:parameters method:Get success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        
        self.strAccount = data[@"result"][@"withdraw_account"];
        if (self.strAccount.length > 0) {
            //有账号
            
            
        }else{
          //如果没有设置提现账号
            self.btnSetting = [[SLCrawCashAccountBtn alloc] init];
            self.btnSetting.frame = CGRectMake(0, 0, screen_W, 44);
            self.btnSetting.backgroundColor = [UIColor lightGrayColor];
            [_btnSetting setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [_btnSetting setTitle:@"未添加提现账号，去添加" forState:UIControlStateNormal];
            [_btnSetting setImage:[UIImage imageNamed:@"right-Arrow-outline"] forState:UIControlStateNormal];
            [_btnSetting addTarget:self action:@selector(btnGoToAdd:) forControlEvents:UIControlEventTouchUpInside];
            [self.tableView addSubview:_btnSetting];
            
        }

        [self.tableView reloadData];
        
        NSLog(@"这数据是%@",self.strAccount);
        
    } failure:^(SLHttpRequestError *failure) {
        if (failure.httpStatusCode == 404) {
            //如果没有设置提现账号
            self.btnSetting = [[SLCrawCashAccountBtn alloc] init];
            self.btnSetting.frame = CGRectMake(0, 0, screen_W, 44);
            //        btnSetting.backgroundColor = [UIColor lightGrayColor];
            [_btnSetting setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [_btnSetting setTitle:@"未添加提现账号，去添加" forState:UIControlStateNormal];
            [_btnSetting setImage:[UIImage imageNamed:@"right-Arrow-outline"] forState:UIControlStateNormal];
            [_btnSetting addTarget:self action:@selector(btnGoToAdd:) forControlEvents:UIControlEventTouchUpInside];
            [self.tableView addSubview:_btnSetting];
            
        }
        NSLog(@"%ld",(long)failure.httpStatusCode);
        
    }];
    
    
}


//- (void)viewWillLayoutSubviews{
//    
//    [super viewWillLayoutSubviews];
//    [_btnSetting mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.view.mas_top).mas_offset(74);
//        make.centerX.mas_equalTo(self.view.mas_centerX);
//        make.width.mas_equalTo(ScreenW - 2 *30);
//        make.height.mas_equalTo(40);
//        
//    }];
//    
//}

- (void)btnGoToAdd:(UIButton *)sender {
    
    SLDrawCashMangerViewController *drawCashMangerVc = [[SLDrawCashMangerViewController alloc] init];
    drawCashMangerVc.drawCashAccountWithName = ^(NSString *drawCashAccount,NSString *drawName){
        self.accountStr = drawCashAccount;
        self.strName = drawName;
        
    };
    [self.navigationController pushViewController:drawCashMangerVc animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.strAccount.length > 0 || self.account.length > 0) {
        return 1;
    }else{
        
        return 0;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:
            if(indexPath.row == 0){
                if (self.strAccount.length > 0 ||self.account.length >0) {
                    SLDrawCashMangerViewCell *cell =[tableView dequeueReusableCellWithIdentifier:indentifiers forIndexPath:indexPath];
                    cell.lblName.text = @"支付宝";
                    cell.imageLeft.image = [UIImage imageNamed:@"aliPay"];
                    cell.rightImage.image = [UIImage imageNamed:@"right-Arrow-outline"];
                    
                    if (self.accountStr.length >0 ) {
                        cell.rightImage.hidden =YES;
                        cell.lblTitle.text = [NSString stringWithFormat:@"%@",self.accountStr];
                    }
                    
                    if (self.name.length >0) {
                        cell.lblName.text = [NSString stringWithFormat:@"%@",self.name];
                    }
                    
                    if (self.account.length >0 ) {
                        cell.rightImage.hidden =YES;
                        cell.lblTitle.text = [NSString stringWithFormat:@"%@",self.account];
                    }
                    
                    if (self.strName.length > 0) {
                        cell.lblName.text = [NSString stringWithFormat:@"%@",self.strName];
                    }
                    
                    return cell;
                }
            }
            
            break;
            
        default:
            break;
    }
    
    
    return nil;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.strAccount.length >0 ) {
        
        self.drawCashAccount(self.accountStr);
    }
    if (self.account.length >0 ) {
        self.drawCashAccount(self.account);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    switch (indexPath.section) {
        case 0:
            if(indexPath.row == 0){
            if (self.strAccount.length > 0 || self.account.length >0) {
                return 60;
                }
              }
            break;
            
        default:
            break;
    }
    return 0;
}

@end
