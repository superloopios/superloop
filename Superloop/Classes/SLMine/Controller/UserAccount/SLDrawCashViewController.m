//
//  SLDrawCashViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/29.
//  Copyright © 2016年 Superloop. All rights reserved.
//  提现详情控制器

#import "SLDrawCashViewController.h"
#import <Masonry.h>
#import "SLAliayNumButton.h"
#import "SLAliyDrawCashNumViewController.h"
#import "SLAPIHelper.h"
#import "SLDrawCashMangerViewController.h"
#import "SLChooseViewController.h"
#import "UILabel+StringFrame.h"
#import "SLValueUtils.h"
@interface SLDrawCashViewController ()<UITextFieldDelegate>
//@property (nonatomic, strong) UIButton *wxSelBtn;
@property (nonatomic, strong) UIButton *aliSelBtn;

@property (nonatomic,strong)NSString *aliayNum;
@property (nonatomic,strong)UITextField *accountField;

@property (nonatomic,strong)NSString *strAccount;
@property (nonatomic,strong)NSString *strAccountName;

@property (nonatomic,strong)NSString *selectedAccountStr;

@property (nonatomic,strong)SLAliayNumButton *aliSel;

@property (nonatomic,strong)NSString *accountStr;
@end

@implementation SLDrawCashViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [MobClick beginLogPageView:@"SLDrawCashViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLDrawCashViewController"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = SLColor(233, 233, 233);
    //self.navigationItem.title = @"提现";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setedupAccount) name:@"settedAccount" object:nil];
    [self getAccountHistory];
    [self setUpNav];
    [self setUpUI];
    
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"提现";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    [self.view addSubview:navView];
    //
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 20, 60, 44);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    //添加分割线
    UIImageView *singleView = [[UIImageView alloc] init];
    singleView.backgroundColor = SLSepatorColor;
    singleView.frame = CGRectMake(0, 63.5, ScreenW, 0.5);
    [navView addSubview:singleView];
    
}
- (void)leftBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- 第一次设置了支付账号
- (void)setedupAccount{
    NSDictionary *parameters = nil;
    [SLAPIHelper getWithdrawAccountHistorys:parameters method:Get success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSString *account = data[@"result"][@"withdraw_account"];
        NSString *accountName = data[@"result"][@"real_name"];
        self.strAccount = [SLValueUtils stringFromObject:account];
        self.strAccountName = [SLValueUtils stringFromObject:accountName];
        self.selectedAccountStr = [SLValueUtils stringFromObject:account];
        [self.aliSel setTitle:[NSString stringWithFormat:@"%@",self.strAccount] forState:UIControlStateNormal];
        self.aliSel.titleX = [self.aliSel.titleLabel boundingRectWithString:self.strAccount withSize:CGSizeMake(MAXFLOAT, 0) withFont:14].width + 30;
        [SLUserDefault setStrAccount:self.strAccount];
        [SLUserDefault setStrAccountName:self.strAccountName];
        
    } failure:^(SLHttpRequestError *failure) {
        
        NSLog(@"%ld",(long)failure.httpStatusCode);
        
    }];
}
- (void)setUpUI
{
    
    //显示上面第一行的占位view
    UIView *vw = [[UIView alloc] init];
    vw.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:vw];
    // 显示金额
    UIView *accountview = [[UIView alloc] init];
    accountview.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:accountview];
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"金额(元)";
    label.font = [UIFont systemFontOfSize:15];
    [accountview addSubview:label];
    
    UITextField *accountField = [[UITextField alloc] init];
    accountField.keyboardType = UIKeyboardTypeDecimalPad;
    accountField.font = [UIFont systemFontOfSize:15];
    accountField.placeholder = @"请输入提现金额";
    self.accountField = accountField;
    accountField.returnKeyType = UIReturnKeyDone;
    accountField.delegate = self;
    [accountview addSubview:accountField];
    //支付宝
    UIView *aliView = [[UIView alloc] init];
    aliView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:aliView];
    
    //    UIImageView *aliImageView = [[UIImageView alloc] init];
    //    aliImageView.image = [UIImage imageNamed:@"aliPay"];
    //    [aliView addSubview:aliImageView];
    
    UILabel *aliLabel = [[UILabel alloc] init];
    aliLabel.text = @"提现账号";
    aliLabel.textColor = [UIColor blackColor];
    aliLabel.font = [UIFont systemFontOfSize:14];
    [aliView addSubview:aliLabel];
    
    UILabel *toAccount = [[UILabel alloc] init];
    toAccount.text = @"提现至账号:";
    toAccount.textColor = [UIColor grayColor];
    toAccount.font = [UIFont systemFontOfSize:14];
    [aliView addSubview:toAccount];
    //添加一个可以点击的按钮以方便触发点击跳转选择事件
    SLAliayNumButton *aliSel = [[SLAliayNumButton alloc] init];
    [aliSel setTitle:@"选择" forState:UIControlStateNormal];
    self.aliSel = aliSel;
    [aliSel.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [aliSel setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    aliSel.titleX = 60;
    [aliSel addTarget:self action:@selector(selectAlipayNum:) forControlEvents:UIControlEventTouchUpInside];
    aliSel.tag = 2000;
    [aliSel setImage:[UIImage imageNamed:@"right-Arrow-outline"] forState:UIControlStateNormal];
    [aliSel setImage:[UIImage imageNamed:@"right-Arrow-outline"] forState:UIControlStateSelected];
    [aliView addSubview:aliSel];
    
    //    UITextField *aliAccountField = [[UITextField alloc] init];
    //    aliAccountField.placeholder = @"请输入支付宝账号";
    //    aliAccountField.textAlignment = NSTextAlignmentLeft;
    //    aliAccountField.backgroundColor = [UIColor lightGrayColor];
    //    [aliView addSubview:aliAccountField];
    
    
    //    //微信
    //    UIView *wxView = [[UIView alloc] init];
    //    wxView.backgroundColor = [UIColor whiteColor];
    //
    //    UIImageView *wxImageView = [[UIImageView alloc] init];
    //    wxImageView.image = [UIImage imageNamed:@"wxPay"];
    //    [wxView addSubview:wxImageView];
    //
    //    UILabel *wxLabel = [[UILabel alloc] init];
    //    wxLabel.text = @"微信支付(即将开通)";
    //    wxLabel.textColor = [UIColor grayColor];
    //    wxLabel.font = [UIFont systemFontOfSize:14];
    //    [wxView addSubview:wxLabel];
    //    [self.view addSubview:wxView];
    
    
    //占位view
    UIView *placeHolder = [[UIView alloc] init];
    placeHolder.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:placeHolder];
    //提现按钮
    UIButton *topUp = [UIButton buttonWithType:UIButtonTypeCustom];
    topUp.backgroundColor = SLColor(236, 236, 236);
    [topUp setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    topUp.layer.cornerRadius = 6;
    topUp.layer.masksToBounds = YES;
    [topUp setTitle:@"提现" forState:UIControlStateNormal];
    [topUp addTarget:self action:@selector(drawCash) forControlEvents:UIControlEventTouchUpInside];
    [placeHolder addSubview:topUp];
    
    //设置支付密码
    UIButton *btnSettingPassWord = [[UIButton alloc] init];
    btnSettingPassWord.backgroundColor = [UIColor clearColor];
    [btnSettingPassWord setTitle:@"管理提现账号" forState:UIControlStateNormal];
    [btnSettingPassWord.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [btnSettingPassWord setTitleColor:SLColor(0, 117, 218) forState:UIControlStateNormal];
    [btnSettingPassWord addTarget:self action:@selector(mangerPassWord) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnSettingPassWord];
    //右边的图片框显示
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageNamed:@"blue-right-Arrow-outline"];
    [self.view addSubview:imageView];
    
    /**
     添加约束
     */
    [vw mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(64);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(0);
        
    }];
    
    //输入金额
    [accountField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(label.mas_right).offset(20);
        make.right.mas_equalTo(self.view.mas_right).offset(-6);
        make.centerY.mas_equalTo(accountview.mas_centerY);
    }];
    [accountview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(vw.mas_bottom).offset(2);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(45);
    }];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(accountview.mas_left).offset(10);
        make.width.mas_equalTo(60);
        make.centerY.mas_equalTo(accountview.mas_centerY);
    }];
    
    //支付宝
    [aliView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(accountview.mas_bottom).offset(6);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(45);
    }];
    
    //按钮覆盖的选择进入下一步的
    [aliSel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ScreenW);
        make.height.mas_equalTo(44);
        make.right.mas_equalTo(aliView.mas_right).offset(0);
        make.centerY.mas_equalTo(aliView.mas_centerY);
        
    }];
    
    
    //    [aliImageView mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.left.mas_equalTo(aliView.mas_left).offset(10);
    //        make.top.mas_equalTo(aliView.mas_top).offset(5);
    //        make.bottom.mas_equalTo(aliView.mas_bottom).offset(-10);
    //        make.width.height.mas_equalTo(32);
    //    }];
    
    [aliLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(aliView.mas_left).offset(10);
        make.top.mas_equalTo(aliView.mas_top).offset(13);
    }];
    
    //    [aliAccountField mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.left.mas_equalTo(aliLabel.mas_right).offset(10);
    //        make.centerY.mas_equalTo(aliLabel.mas_centerY);
    //        make.width.mas_equalTo(200);
    //    }];
    //    //微信
    //    [wxView mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.top.mas_equalTo(aliView.mas_bottom).offset(6);
    //        make.left.mas_equalTo(self.view.mas_left);
    //        make.right.mas_equalTo(self.view.mas_right);
    //        make.height.mas_equalTo(45);
    //    }];
    //
    //    [wxImageView mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.left.mas_equalTo(wxView.mas_left).offset(10);
    //        make.top.mas_equalTo(wxView.mas_top).offset(5);
    //        make.bottom.mas_equalTo(wxView.mas_bottom).offset(-5);
    //        make.width.mas_equalTo(34);
    //    }];
    //
    //    [wxLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.left.mas_equalTo(wxImageView.mas_right).offset(30);
    //        make.centerY.mas_equalTo(wxView.mas_centerY);
    //    }];
    
    //占位的view的显示
    [placeHolder mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(aliView.mas_bottom).offset(2);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        
    }];
    //设置密码的布局代码
    [btnSettingPassWord mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.top.mas_equalTo(placeHolder.mas_top).offset(10);
        
    }];
    
    //支付密码重置
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(btnSettingPassWord.mas_right).offset(5);
        make.centerY.mas_equalTo(btnSettingPassWord.mas_centerY).offset(0);
        make.width.mas_equalTo(6);
        make.height.mas_equalTo(11);
    }];
    
    //提现按钮
    [topUp mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(placeHolder.mas_top).offset(54);
        make.left.mas_equalTo(placeHolder.mas_left).offset(10);
        make.right.mas_equalTo(placeHolder.mas_right).offset(-10);
        make.height.mas_equalTo(44);
    }];
    
    
}

#pragma mark ------ 选择阿里支付方法
- (void)selectAlipayNum:(UIButton *)btn{
    
    btn.selected = !btn.selected;
    SLChooseViewController *vc = [[SLChooseViewController alloc] init];
    vc.accountStr = self.strAccount;
    vc.name = self.strAccountName;
    
    __weak typeof(self)weakSelf = self;
    
    vc.drawCashAccount = ^(NSString *drawCashAccount){
        weakSelf.selectedAccountStr = drawCashAccount;
        [self.aliSel setTitle:[NSString stringWithFormat:@"%@",drawCashAccount] forState:UIControlStateNormal];
        self.aliSel.titleX = [self.aliSel.titleLabel boundingRectWithString:drawCashAccount withSize:CGSizeMake(MAXFLOAT, 0) withFont:14].width + 30;
    };
    
    [self.navigationController pushViewController:vc animated:YES];
    
}


#pragma mark ---  提现按钮点击事件
- (void)drawCash {
    if (self.accountField.text.length == 0) {
        [HUDManager showWarningWithText:@"请输入提现金额"];
        return;
    }
    if (self.selectedAccountStr.length == 0) {
        [HUDManager showWarningWithText:@"请先选择提现账号"];
        return;
    }
    
    double numAccount = [self.accountField.text doubleValue];
#ifdef DEBUG
    if (numAccount <= 0) {
        [HUDManager showWarningWithText:@"提现操作失败，金额必须大于0"];
        return;
    }
#else
    if (numAccount < 100) {
        [HUDManager showWarningWithText:@"提现金额不能小于100元"];
        return;
    }
    
    if (numAccount > 50000) {
        
        [HUDManager showWarningWithText:@"提现金额不能大于50000元"];
        return;
    }
#endif
    NSString *str = [NSString stringWithFormat:@"%.2f",numAccount];
    NSDictionary *parameters = @{@"amount":str};
    NSLog(@"=======%@",parameters);
    
    [SLAPIHelper requestWithdraw:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        [HUDManager showWarningWithText:@"提现申请成功"];
        [self.navigationController popViewControllerAnimated:YES];
        NSLog(@"数据是%@",data);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"chargeSuccess" object:nil];
        
    } failure:^(SLHttpRequestError *failure) {
        
        if (failure.httpStatusCode == 400) {
            if (failure.slAPICode == 1) {
                [HUDManager showWarningWithText:@"金额输入错误"];
            }else if (failure.slAPICode == 427) {
                
                [HUDManager showWarningWithText:@"提现失败，每天只能提现一次"];
            }
        }
        if (failure.slAPICode == 501){
            
            [HUDManager showWarningWithText:@"余额不足"];
        }
        NSLog(@"另外的状态吗是%ld",(long)failure.httpStatusCode);
        NSLog(@"状态吗是%ld",(long)failure.slAPICode);
        
    }];
    
}


#pragma mark ----- 设置验证字符串只能输入数字（包含小数点后面的两位数字）
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHaveDian = YES;
    
    if ([textField.text rangeOfString:@"."].location == NSNotFound) {
        isHaveDian = NO;
    }
    
    
    if ([string length] > 0) {
        
        unichar single = [string characterAtIndex:0];//当前输入的字符
        if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
            //            //不能输入大于200的金额
            //            NSString *amount = [NSString stringWithFormat:@"%@%@",textField.text,string];
            //            float amountCount = [amount floatValue];
            //            if (amountCount > 200) {
            //
            //                return NO;
            //            }
            //首字母不能为0和小数点1
            if([textField.text length] == 0){
                if(single == '.') {
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            
            //输入的字符是否是小数点
            if (single == '.') {
                if(!isHaveDian)//text中还没有小数点
                {
                    //                  isHaveDian = YES;
                    return YES;
                    
                }else{
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                if (isHaveDian) {//存在小数点
                    
                    //判断小数点的位数
                    NSRange ran = [textField.text rangeOfString:@"."];
                    if (range.location - ran.location <= 2) {
                        return YES;
                    }else{
                        return NO;
                    }
                }else{
                    return YES;
                }
            }
        }else{//输入的数据格式不正确
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }
}


//账号管理按钮的点击事件
- (void)mangerPassWord{
    
    SLDrawCashMangerViewController *accoutManger = [[SLDrawCashMangerViewController alloc] init];
    accoutManger.account = self.strAccount;
    accoutManger.name = self.strAccountName;
    accoutManger.isComeFromAccountManger = YES;
    [self.navigationController pushViewController:accoutManger animated:YES];
}


- (void)getAccountHistory{
    
    NSDictionary *parameters = nil;
    [SLAPIHelper getWithdrawAccountHistorys:parameters method:Get success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSString *account = data[@"result"][@"withdraw_account"];
        NSString *accountName = data[@"result"][@"real_name"];
        self.strAccount = [SLValueUtils stringFromObject:account];
        self.strAccountName = [SLValueUtils stringFromObject:accountName];
        [SLUserDefault setStrAccount:self.strAccount];
        [SLUserDefault setStrAccountName:self.strAccountName];
        //        NSLog(@"数据是%@",data);
        
    } failure:^(SLHttpRequestError *failure) {
        [SLUserDefault removeStrAccount];
        [SLUserDefault removeStrAccountName];
        
    }];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder]; return YES;
}


- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
