//
//  SLDrawCashMangerViewController.m
//  Superloop
//
//  Created by 朱宏伟 on 16/6/7.
//  Copyright © 2016年 Superloop. All rights reserved.
//  管理提现账号

#import "SLDrawCashMangerViewController.h"
#import "SLDrawCashMangerViewCell.h"
#import "SLAliyDrawCashNumViewController.h"
#import "SLValueUtils.h"
static NSString *const  indentifiers = @"cells";

@interface SLDrawCashMangerViewController()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)NSString *drawCashAccount; //账号

@property (nonatomic,strong)NSString *drawName; //姓名

@property(nonatomic, weak)UITableView *tableView;

@end
@implementation SLDrawCashMangerViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLDrawCashMangerViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLDrawCashMangerViewController"];
}
- (UITableView *)tableView
{
    if (!_tableView) {
        UITableView *tableview = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, screen_W, screen_H-64) style:UITableViewStylePlain];
        [self.view addSubview:tableview];
        tableview.dataSource = self;
        tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableview.delegate = self;
        _tableView = tableview;
    }
    return _tableView;
}
- (void)viewDidLoad{
    
    [super viewDidLoad];
    [self setUpNav];
    self.drawCashAccount = [SLUserDefault getStrAccout];
    self.drawName = [SLUserDefault getStrAccountName];
    [self.tableView registerNib:[UINib nibWithNibName:@"SLDrawCashMangerViewCell" bundle:nil] forCellReuseIdentifier:indentifiers];
    self.title = @"管理提现账号";
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return 1;
    
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"管理提现账号";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLDrawCashMangerViewCell *cell =[tableView dequeueReusableCellWithIdentifier:indentifiers forIndexPath:indexPath];
    switch (indexPath.section) {
        case 0:{
            if(indexPath.row == 0){ // 支付宝账号
                cell.lblName.text = @"支付宝";
                cell.lblTitle.text = @"添加账号";
                
                cell.imageLeft.image = [UIImage imageNamed:@"aliPay"];
                if (self.drawName.length > 0 ) {
                    cell.lblName.text = [NSString stringWithFormat:@"%@",self.drawName];
                }
                
                if (self.drawCashAccount.length > 0) {
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",self.drawCashAccount];
                }
                
                if (self.account.length > 0) {
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",self.account];
                }
                if (self.name.length > 0) {
                    cell.lblName.text = [NSString stringWithFormat:@"%@",self.name];
                }
                
                cell.lblTitle.textColor = [UIColor lightGrayColor];
                cell.rightImage.image = [UIImage imageNamed:@"right-Arrow-outline"];
                
                if (!self.isComeFromAccountManger && self.account.length != 0) {
                    cell.rightImage.hidden = YES;
                }
                return cell;
                break;
            }
        }
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch (indexPath.section) {
        case 0:{
            if(indexPath.row == 0){ // 支付宝账号
                SLAliyDrawCashNumViewController *drawCashVc = [[SLAliyDrawCashNumViewController alloc] init];
                
                drawCashVc.drawCashAccount = ^(NSString *drawCashAccount,NSString *drawName){
                    self.drawCashAccount = drawCashAccount;
                    self.drawName = drawName;
                };
                drawCashVc.account = self.account;
                drawCashVc.name = self.name;
                if (self.drawCashAccountWithName) {
                    self.drawCashAccountWithName(self.drawCashAccount,self.drawName);
                }
                
                [self.navigationController pushViewController:drawCashVc animated:YES];
            }
            break;
        }
            
        default:
            break;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
}



@end
