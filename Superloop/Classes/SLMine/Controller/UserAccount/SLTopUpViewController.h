//
//  SLTopUpViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/29.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLTopUpViewController : UIViewController
@property (nonatomic,strong)SLJavascriptBridgeEvent *event;

@property (nonatomic,strong)void (^moneyStr)(NSString *moneyStr);
@property (nonatomic,assign)BOOL isComeFromMessageVc; //消息充值push过来的
@end
