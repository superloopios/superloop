//
//  SLFansTableViewController.h
//  Superloop
//
//  Created by 朱宏伟 on 16/5/4.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

//@interface SLFansTableViewController : UITableViewController
@interface SLFansTableViewController : UIViewController
@property(nonatomic,copy)NSString *userId;
@property(nonatomic,copy)NSString *isfollow;
@property (nonatomic,weak)UIView *noNetWorksView; //没有网络的遮盖view
@property (nonatomic,weak)UIView *noCoutView; //没有网络的遮盖view
@end
