//
//  SLNewEducationViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//
//#import "SLBaseTableViewController.h"
#import <UIKit/UIKit.h>
@interface SLNewEducationViewController : UIViewController

@property (nonatomic,strong)void(^eduCationBlcok)(NSString *scollStr,NSString *startTimeStr,NSString *endTimeStr,NSString *profissionStr,NSString *projectStr);
@property (nonatomic,strong) NSString  *scollStr;
@property (nonatomic,strong) NSString  *startTimeStr;
@property (nonatomic,strong) NSString  *endTimeStr;
@property (nonatomic,strong) NSString  *profissionStr;
@property (nonatomic,strong) NSString  *projectStr;

@property(nonatomic,strong)SLJavascriptBridgeEvent *event;


@property(nonatomic,assign)BOOL isSelectRow;


@property (nonatomic, strong) NSString *educationID;

@property (nonatomic,copy)void (^changeEducationBlock)(NSDictionary *educationDict,BOOL isCreate);

@end
