//
//  SLNewExperienceViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLNewExperienceViewController.h"
#import <Masonry.h>
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "TFDatePickerView.h"
#import <UIKit/UIKit.h>
#import "SLLocationViewController.h"
#import "SLAPIHelper.h"
@interface SLNewExperienceViewController ()<TFDatePickerViewDelegate,UITableViewDataSource,UITableViewDelegate>{
    UIButton *_submitBtn;
}
@property (nonatomic, strong)NSArray *dataArray;
@property (nonatomic, strong)UILabel *fromLabel;
@property (nonatomic, strong)UILabel *toLabel;
@property (nonatomic, strong)UITextField *companyNameTextField;
@property (nonatomic, strong)UITextField *positionTextField;
@property (nonatomic, strong)UILabel *locationLable;
@property (nonatomic, assign)BOOL isTo2;
@property (nonatomic, strong)TFDatePickerView *pickerView;
@property (nonatomic, strong) NSString *locationAdress;
@property(nonatomic, weak)UITableView *tableView;

@end

@implementation SLNewExperienceViewController

//static NSString *const postName = @"postName";
static NSString *const postName = @"SaveExperience";

- (UITableView *)tableView{
    if (!_tableView) {
        UITableView *tableview = [[UITableView alloc] init];
        tableview.delegate = self;
        tableview.dataSource = self;
        tableview.frame = CGRectMake(0, 64, screen_W, screen_H - 64);
        [tableview registerNib:[UINib nibWithNibName:@"SLEducationViewCell" bundle:nil] forCellReuseIdentifier:@"experienceCell"];
        //tableview.frame = CGRectMake(0, 64, screen_W, screen_H - 64);
        [self.view addSubview:tableview];
        _tableView = tableview;
    }
    return _tableView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
//    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(30, 27, ScreenW-80, 30)];
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"工作经历";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    _submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    [_submitBtn setTitle:@"保存" forState:UIControlStateNormal];
    _submitBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_submitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(saveExperience) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:_submitBtn];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNav];
    //收起键盘
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到view上
    [self.companyNameTextField addGestureRecognizer:tapGestureRecognizer];
    [self.positionTextField addGestureRecognizer:tapGestureRecognizer];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    self.navigationItem.title = @"工作经历";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(saveExperience)];
    _dataArray = @[@"公司名称",@"开始日期",@"结束日期",@"职位",@"地点"];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 0.5)];
    imageView.backgroundColor = [UIColor lightGrayColor];
    self.tableView.tableFooterView = imageView;
    self.tableView.backgroundColor = SLColor(240, 240, 240);
}

- (void)saveExperience
{
    
    if ([self verticafStting]) {
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:self.companyNameTextField.text forKey:@"company"];
        if (_fromLabel.text.length>0) {
            [parameters setObject:_fromLabel.text forKey:@"start_date"];
        }if (_toLabel.text.length>0) {
            if ([_toLabel.text isEqualToString:@"至今"]) {
            [parameters setObject:@"0000-00-00"forKey:@"end_date"];
            }else{
            [parameters setObject:_toLabel.text forKey:@"end_date"];
            }
        }if (_positionTextField.text.length>0) {
            [parameters setObject:_positionTextField.text forKey:@"position"];
        }if (_locationLable.text.length>0) {
            [parameters setObject:_locationLable.text forKey:@"location"];
        }
        if (self.isSelectRow) {
            
            [parameters setObject:@([_experienceID integerValue]) forKey:@"id"];
            [SLAPIHelper reviseWorkHistory:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                
                //内容保存成功通知刷新保存之后的结果控制器
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"ChangeExperience" object:self userInfo:parameters];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeData" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:postName object:self userInfo:parameters];
                //修改完成刷新h5页面
//                [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//                    
//                }];
                [self.navigationController popViewControllerAnimated:YES];
                
                
            } failure:^(SLHttpRequestError *failure) {
                [HUDManager showWarningWithText:@"保存失败"];
            }];
            
            
        }else{
            
            
            [SLAPIHelper addWorkHistory:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                
                //发送通知，通知上一级控制器刷新控件
                [[NSNotificationCenter defaultCenter] postNotificationName:postName object:self userInfo:parameters];
                
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeData" object:nil];
                //修改完成刷新h5页面
//                [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//                    
//                }];
                [self.navigationController popViewControllerAnimated:YES];
            } failure:^(SLHttpRequestError *failure) {
                [HUDManager showWarningWithText:@"保存失败"];
            }];
        }
    }
}

// 验证参数
- (BOOL)verticafStting{
    if (self.companyNameTextField.text.length<2 ||self.companyNameTextField.text.length>50) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入公司名字长度为2至50位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        return NO;
    }
    if (self.positionTextField.text.length>0) {
        if (self.positionTextField.text.length<2 ||self.positionTextField.text.length>50) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入职位名称长度为2至50位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [alertView show];
            return NO;
        }
    }
    return YES;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"experience"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"experience"];
        
        //cell的选中背景色
        UIView *selBg = [[UIView alloc]init];
        selBg.backgroundColor = SLColor(230, 230, 230);
        cell.selectedBackgroundView = selBg;
        
        cell.textLabel.text = _dataArray[indexPath.row];
        switch (indexPath.row) {
            case 0:
            {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                UITextField *nameField = [[UITextField alloc] init];
                nameField.placeholder = @"请输入公司名称";
                nameField.textAlignment = NSTextAlignmentRight;
                self.companyNameTextField = nameField;
                self.companyNameTextField.text=self.companyStr;
                [cell addSubview:nameField];
                [nameField mas_makeConstraints:^(MASConstraintMaker *make) {
                    //                    make.width.mas_equalTo(200);
                    make.width.mas_equalTo(ScreenW-50-50);
                    make.right.mas_equalTo(cell.mas_right).offset(-5);
                    make.top.mas_equalTo(cell.mas_top);
                    make.bottom.mas_equalTo(cell.mas_bottom);
                }];
                break;
            }
            case 1:
            {
                cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Down-Arrow-outline"]];
                UILabel *label = [[UILabel alloc] init];
                label.textAlignment = NSTextAlignmentRight;
                label.font = [UIFont systemFontOfSize:14];
                label.textColor = [UIColor blackColor];
                label.numberOfLines = 1;
                self.fromLabel = label;
                self.fromLabel.text=self.startTimeStr;
                [cell addSubview:label];
                
                [label mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(150);
                    make.right.mas_equalTo(cell.mas_right).offset(-30);
                    make.top.mas_equalTo(cell.mas_top);
                    make.bottom.mas_equalTo(cell.mas_bottom);
                }];
                
                break;
            }
            case 2:
            {
                cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Down-Arrow-outline"]];
                UILabel *label = [[UILabel alloc] init];
                label.textAlignment = NSTextAlignmentRight;
                label.font = [UIFont systemFontOfSize:14];
                label.textColor = [UIColor blackColor];
                label.numberOfLines = 1;
                self.toLabel = label;
                self.toLabel.text=self.endTimeStr;
                [cell addSubview:label];
                
                [label mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(150);
                    make.right.mas_equalTo(cell.mas_right).offset(-30);
                    make.top.mas_equalTo(cell.mas_top);
                    make.bottom.mas_equalTo(cell.mas_bottom);
                }];
                
                break;
            }
            case 3:
            {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                UITextField *nameField = [[UITextField alloc] init];
                nameField.placeholder = @"请输入职位";
                nameField.textAlignment = NSTextAlignmentRight;
                self.positionTextField = nameField;
                self.positionTextField.text=self.jobStr;
                [cell addSubview:nameField];
                [nameField mas_makeConstraints:^(MASConstraintMaker *make) {
                    //                    make.width.mas_equalTo(200);
                    make.width.mas_equalTo(ScreenW-50-50);
                    make.right.mas_equalTo(cell.mas_right).offset(-5);
                    make.top.mas_equalTo(cell.mas_top);
                    make.bottom.mas_equalTo(cell.mas_bottom);
                }];
                break;
            }
            case 4:
            {
                cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Down-Arrow-outline"]];
                UILabel *nameField = [[UILabel alloc] init];
                nameField.textAlignment = NSTextAlignmentRight;
                nameField.font = [UIFont systemFontOfSize:14];
                nameField.textColor = [UIColor blackColor];
                nameField.numberOfLines = 1;
                self.locationLable = nameField;
                self.locationLable.text=self.locationStr;
                [cell addSubview:nameField];
                
                [nameField mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(200);
                    make.right.mas_equalTo(cell.mas_right).offset(-30);
                    make.top.mas_equalTo(cell.mas_top);
                    make.bottom.mas_equalTo(cell.mas_bottom);
                }];
                break;
            }
            default:
                break;
        }
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //每次回来之前取消选中
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    // 监听日期选择器的滚动
    if (indexPath.row == 1) {
        [self.tableView endEditing:YES];
        self.pickerView = [TFDatePickerView tfDatePickerViewWithDatePickerMode:UIDatePickerModeDate Delegate:self];
        self.pickerView.toToday.hidden = YES;
        [self.pickerView tf_show];
        self.isTo2 = NO;
    }
    
    if (indexPath.row == 2)
    {
        [self.tableView endEditing:YES];
        self.pickerView = [TFDatePickerView tfDatePickerViewWithDatePickerMode:UIDatePickerModeDate Delegate:self];
        [self.pickerView tf_show];
        self.isTo2 = YES;
    }
    
    if (indexPath.row == 4) {
        
        __weak typeof(self)weakSelf = self;
        SLLocationViewController *locationVc = [[SLLocationViewController alloc] init];
        locationVc.isMineBasicInfoLocation = YES;
        locationVc.tag = 2000;
        locationVc.valueBlcok = ^(NSString *str){
            NSLog(@"%@",str);
            weakSelf.locationAdress = str;
            weakSelf.locationLable.text = str;
        };
        [self.navigationController pushViewController:locationVc animated:YES];
    }
}

#warning 新换的时间选择器代理方法 TFDatePickerViewDelegate
- (BOOL)submitWithSelectedDate:(NSDate *)selectedDate{
    NSDateFormatter *outputFormatter= [[NSDateFormatter alloc] init];
    outputFormatter.dateFormat = @"YYYY-MM-dd";
    NSString *str= [outputFormatter stringFromDate:selectedDate];
    if (!_isTo2) {
        self.fromLabel.text = str;
    }
    if (_isTo2) {
        self.toLabel.text = str;
    }
    return YES;
}
//至今按钮点击的代理事件
- (void)submitToday:(UIButton *)sender{
        if (_isTo2) {
        self.toLabel.text = @"至今";
    }
}

-(void)keyboardHide:(UITapGestureRecognizer*)tap{
    [self.companyNameTextField resignFirstResponder];
    [self.positionTextField resignFirstResponder];
}
-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
