//
//  SLHarassmentViewController.h
//  Superloop
//
//  Created by 张梦川 on 16/5/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
@interface SLHarassmentViewController : BaseViewController
@property (nonatomic,strong) SLJavascriptBridgeEvent *event;
@property (nonatomic,copy) NSString *isSwithOn;
@property (nonatomic ,strong) void(^changeSuccess)(NSString *str);
@end
