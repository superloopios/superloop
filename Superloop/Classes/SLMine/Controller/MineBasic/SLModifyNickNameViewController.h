//
//  SLModifyNickNameViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//   修改昵称控制器

#import <UIKit/UIKit.h>

@interface SLModifyNickNameViewController : UIViewController
@property (nonatomic ,strong) void(^nickNameBlcok)(NSString *str);
@property (nonatomic, strong) NSString *nickName;

@property (nonatomic,strong) SLJavascriptBridgeEvent *event;

@end
