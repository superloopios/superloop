//
//  SLReplyEvaluateViewController.m
//  Superloop
//
//  Created by WangS on 16/10/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLReplyEvaluateViewController.h"
#import "SLTextView.h"
#import "SLAPIHelper.h"

@interface SLReplyEvaluateViewController ()<UITextViewDelegate>
@property (nonatomic,strong)SLTextView *replyTextView;
@end

@implementation SLReplyEvaluateViewController

- (SLTextView *)replyTextView{
    if (!_replyTextView) {
        _replyTextView = [[SLTextView alloc] initWithFrame:CGRectMake(12, 64, ScreenW-24, ScreenH-64)];
        _replyTextView.placeholder = @"请输入回复内容";
        _replyTextView.font = [UIFont systemFontOfSize:15];
        _replyTextView.delegate = self;
        [self.view addSubview:_replyTextView];
    }
    return _replyTextView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xf4f4f4);
    [self setUpNav];
    self.replyTextView.textColor = UIColorFromRGB(0x000000);
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backBtn setTitle:@"取消" forState:UIControlStateNormal];
    backBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"回复评价";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIButton *rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    [rightBtn setTitle:@"提交" forState:UIControlStateNormal];
    rightBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(sendCommend:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:rightBtn];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.replyTextView resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)sendCommend:(UIButton *)btn{
    btn.enabled = NO;
    NSDictionary *params = @{@"appraisal_id":self.appraisal_id,@"reply_content":self.replyTextView.text};
    
    if (self.isPhoneOrMsg) {//打电话回复
        [SLAPIHelper replyCallsEvaluate:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            if (self.replySuccess) {
                self.replySuccess(YES);
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"replyEvaluateSuccess" object:self];
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } failure:^(SLHttpRequestError *failure) {
            btn.enabled = YES;
            [HUDManager showWarningWithText:@"回复评论失败"];
        }];
    }else{//付费消息回复
        [SLAPIHelper replyMsgEvaluate:params success:^(NSURLSessionDataTask *task, NSDictionary *data) {
            if (self.replySuccess) {
                self.replySuccess(YES);
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"replyEvaluateSuccess" object:self];
            [self dismissViewControllerAnimated:YES completion:nil];
        } failure:^(SLHttpRequestError *failure) {
            btn.enabled = YES;
            [HUDManager showWarningWithText:@"回复评论失败"];
        }];
    }
    
   
    
}
-(void)textViewDidChange:(UITextView *)textView{
    [self setSpace:self.replyTextView withValue:self.replyTextView.text withFont:[UIFont systemFontOfSize:15]];
}
//设置行间距和字间距
- (void)setSpace:(SLTextView*)replyTV withValue:(NSString*)str withFont:(UIFont*)font {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = 8; //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@0.0f};
    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
    replyTV.attributedText = attributeStr;
}


@end
