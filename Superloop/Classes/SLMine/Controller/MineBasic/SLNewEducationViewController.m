//
//  SLNewEducationViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLNewEducationViewController.h"
#import <Masonry.h>
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "TFDatePickerView.h"
#import "SLAPIHelper.h"
@interface SLNewEducationViewController ()<TFDatePickerViewDelegate,UITableViewDelegate,UITableViewDataSource>{
    UIButton *_submitBtn;
}

@property (nonatomic, strong)NSArray *dataArray;
@property (nonatomic, strong)UILabel *fromLabel;
@property (nonatomic, strong)UILabel *toLabel;
@property (nonatomic, strong)UITextField *schoolNameTextField;
@property (nonatomic, strong)UITextField *degreeTextField;
@property (nonatomic, strong)UITextField *specialTextField;
@property (nonatomic, strong)TFDatePickerView *pickerView;
@property (nonatomic,assign)BOOL isTo2;
@property(nonatomic, weak)UITableView *tableView;

@end

@implementation SLNewEducationViewController

- (UITableView *)tableView{
    if (!_tableView) {
        UITableView *tableview = [[UITableView alloc] init];
        tableview.delegate = self;
        tableview.dataSource = self;
        //        tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableview.frame = CGRectMake(0, 64, screen_W, screen_H - 64);
        [tableview registerNib:[UINib nibWithNibName:@"SLEducationViewCell" bundle:nil] forCellReuseIdentifier:@"experienceCell"];
        tableview.frame = CGRectMake(0, 64, screen_W, screen_H - 64);
        [self.view addSubview:tableview];
        _tableView = tableview;
    }
    return _tableView;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}


- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"教育背景";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    _submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    [_submitBtn setTitle:@"保存" forState:UIControlStateNormal];
    _submitBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_submitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(saveEducation) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:_submitBtn];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"教育背景";
    //收起键盘
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到view上
    [self.schoolNameTextField addGestureRecognizer:tapGestureRecognizer];
    [self.degreeTextField addGestureRecognizer:tapGestureRecognizer];
    [self.specialTextField addGestureRecognizer:tapGestureRecognizer];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    [self setUpNav];
    //NSLog(@"----_educationID------%@",_educationID);
//     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(saveEducation)];
    
    _dataArray = @[@"学校名称",@"入学时间",@"毕业时间",@"学历/学位",@"专业名称"];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 0.5)];
    imageView.backgroundColor = [UIColor lightGrayColor];
    self.tableView.tableFooterView = imageView;
}

#pragma mark - Table view data source

- (void)saveEducation
{
        if ([self vertifString]) {
            
            NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
            [parameters setObject:_schoolNameTextField.text forKey:@"institution"];
            if (_fromLabel.text.length>0) {
                [parameters setObject:_fromLabel.text forKey:@"start_date"];
            }if (_toLabel.text.length>0) {
                if ([_toLabel.text isEqualToString:@"至今"]) {
                    [parameters setObject:@"0000-00-00"forKey:@"end_date"];
                }else{
                    [parameters setObject:_toLabel.text forKey:@"end_date"];
                }
            }if (_specialTextField.text.length>0) {
                [parameters setObject:_specialTextField.text forKey:@"department"];
            }if (_degreeTextField.text.length>0) {
                [parameters setObject:_degreeTextField.text forKey:@"degree"];
            }
            
            
//            NSDictionary *parameters = @{@"institution":self.schoolNameTextField.text,@"start_date":_fromLabel.text,@"end_date":_toLabel.text, @"department":self.specialTextField.text, @"degree":self.degreeTextField.text};
//            [manager POST:[URL_ROOT_PATIENT stringByAppendingString:@"/users/educations"]  parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
//
//                NSLog(@"%@",responseObject);
            
                
                
//                if (_eduCationBlcok) {
//                    _eduCationBlcok(_schoolNameTextField.text,_fromLabel.text,_toLabel.text,_degreeTextField.text,_specialTextField.text);
//                }
           // __weak typeof(self) weakSelf = self;
            if (self.isSelectRow) {
                
                [parameters setObject:@([_educationID integerValue])forKey:@"id"];

                [SLAPIHelper reviseEducation:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                    
                    //内容修改成功通知上个控制器
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ChangeEducation" object:self userInfo:parameters];
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeData" object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveEducation" object:self userInfo:parameters];
                    //修改完成刷新h5页面
//                    [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//                        
//                    }];
                    [self.navigationController popViewControllerAnimated:YES];

                    
                } failure:^(SLHttpRequestError *error) {
                    [HUDManager showWarningWithText:@"保存失败"];
                }];
            
            }else{
            [SLAPIHelper addEducation:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                //内容保存成功通知刷新保存之后的结果控制器
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveEducation" object:self userInfo:parameters];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeData" object:nil];
                //修改完成刷新h5页面
//                [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//                    
//                }];
                [self.navigationController popViewControllerAnimated:YES];
            } failure:^(SLHttpRequestError *failure) {
                [HUDManager showWarningWithText:@"保存失败"];
            }];
            
        }
    }
}

//验证

- (BOOL)vertifString{
    
    if (self.specialTextField.text.length>0) {
    
        if (self.specialTextField.text.length<2 ||self.specialTextField.text.length>50) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入专业名称长度为2至50位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [alertView show];
            return NO;
        }
        
    }
    if (self.degreeTextField.text.length>0) {
        if (self.degreeTextField.text.length<2 ||self.degreeTextField.text.length>50) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入学历/学位名称长度为2至50位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [alertView show];
            return NO;
        }
    }
    if (self.schoolNameTextField.text.length<2 ||self.schoolNameTextField.text.length>50) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入学校名称长度为2至50位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        return NO;
    }

    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"education"];
   
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"education"];
        
        //cell的选中背景色
        UIView *selBg = [[UIView alloc]init];
        selBg.backgroundColor = SLColor(230, 230, 230);
        cell.selectedBackgroundView = selBg;
        
        cell.textLabel.text = _dataArray[indexPath.row];
        switch (indexPath.row) {
            case 0:
            {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
       
                UITextField *nameField = [[UITextField alloc] init];
                nameField.placeholder = @"请输入学校名称";
                nameField.textAlignment = NSTextAlignmentRight;
                self.schoolNameTextField = nameField;
                self.schoolNameTextField.text = self.scollStr;
                [cell addSubview:nameField];
                [nameField mas_makeConstraints:^(MASConstraintMaker *make) {
//                    make.width.mas_equalTo(200);
                    make.width.mas_equalTo(ScreenW-50-50);
                    make.right.mas_equalTo(cell.mas_right).offset(-5);
                    make.top.mas_equalTo(cell.mas_top);
                    make.bottom.mas_equalTo(cell.mas_bottom);
                }];
                break;
            }
            case 1:
            {
                cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Down-Arrow-outline"]];
                UILabel *label = [[UILabel alloc] init];
                //            label.text = @"2016-01";
                label.textAlignment = NSTextAlignmentRight;
                label.font = [UIFont systemFontOfSize:14];
                label.textColor = [UIColor blackColor];
                label.numberOfLines = 1;
                self.fromLabel = label;
                [cell addSubview:label];
                self.fromLabel.text = self.startTimeStr;
                [label mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(150);
                    make.right.mas_equalTo(cell.mas_right).offset(-30);
                    make.top.mas_equalTo(cell.mas_top);
                    make.bottom.mas_equalTo(cell.mas_bottom);
                }];
                
                break;
            }
            case 2:
            {
                cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Down-Arrow-outline"]];
                UILabel *label = [[UILabel alloc] init];
                label.text = @"至今";
                label.textAlignment = NSTextAlignmentRight;
                label.font = [UIFont systemFontOfSize:14];
                label.textColor = [UIColor blackColor];
                label.numberOfLines = 1;
                self.toLabel = label;
                self.toLabel.text = self.endTimeStr;
                [cell addSubview:label];
                
                [label mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(150);
                    make.right.mas_equalTo(cell.mas_right).offset(-30);
                    make.top.mas_equalTo(cell.mas_top);
                    make.bottom.mas_equalTo(cell.mas_bottom);
                }];
                
                break;
            }
            case 3:
            {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                UITextField *nameField = [[UITextField alloc] init];
                nameField.placeholder = @"请输入学位";
                nameField.textAlignment = NSTextAlignmentRight;
                self.degreeTextField = nameField;
                self.degreeTextField.text = self.profissionStr;
                [cell addSubview:nameField];
                [nameField mas_makeConstraints:^(MASConstraintMaker *make) {
//                    make.width.mas_equalTo(200);
                    make.width.mas_equalTo(ScreenW-50-50);
                    make.right.mas_equalTo(cell.mas_right).offset(-5);
                    make.top.mas_equalTo(cell.mas_top);
                    make.bottom.mas_equalTo(cell.mas_bottom);
                }];
                break;
            }
            case 4:
            {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                UITextField *nameField = [[UITextField alloc] init];
                nameField.placeholder = @"请输入专业名称";
                nameField.textAlignment = NSTextAlignmentRight;
                self.specialTextField = nameField;
                self.specialTextField.text = self.projectStr;
                [cell addSubview:nameField];
                [nameField mas_makeConstraints:^(MASConstraintMaker *make) {
//                    make.width.mas_equalTo(200);
                    make.width.mas_equalTo(ScreenW-50-50);
                    make.right.mas_equalTo(cell.mas_right).offset(-5);
                    make.top.mas_equalTo(cell.mas_top);
                    make.bottom.mas_equalTo(cell.mas_bottom);
                }];
                break;
            }
            default:
                break;
        }
        
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //每次回来之前取消选中
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // 创建一个UITextField
    UITextField *field = [[UITextField alloc]init];
    [self.view addSubview:field];
    
    // 将字符串转化成NSDate对象
    NSString *time = @"2016-01-01";
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yyyy-MM-dd";

  
    // 监听日期选择器的滚动
    if (indexPath.row == 1) {
        [self.tableView endEditing:YES];
        self.pickerView.tag = 1000;
        //处理时间选择器的确定和取消按钮的时间选择器
        self.pickerView = [TFDatePickerView tfDatePickerViewWithDatePickerMode:UIDatePickerModeDate Delegate:self];
        self.pickerView.toToday.hidden = YES;
        [self.pickerView tf_show];
        self.isTo2 = NO;
    }
    if (indexPath.row == 2)
    {
        [self.tableView endEditing:YES];
        self.pickerView = [TFDatePickerView tfDatePickerViewWithDatePickerMode:UIDatePickerModeDate Delegate:self];
        [self.pickerView tf_show];
        self.isTo2 = YES;
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}
#warning 新换的时间选择器代理方法 TFDatePickerViewDelegate
- (BOOL)submitWithSelectedDate:(NSDate *)selectedDate{
    NSDateFormatter *outputFormatter= [[NSDateFormatter alloc] init];
    outputFormatter.dateFormat = @"YYYY-MM-dd";
    NSString *str= [outputFormatter stringFromDate:selectedDate];
    if (!_isTo2) {
        self.fromLabel.text = str;
    }
    if (_isTo2) {
        self.toLabel.text = str;
    }
    return YES;
}
//至今按钮点击的代理事件
- (void)submitToday:(UIButton *)sender{
    if (_isTo2) {
        self.toLabel.text = @"至今";
    }
}

-(void)keyboardHide:(UITapGestureRecognizer*)tap{
        [self.schoolNameTextField resignFirstResponder];
        [self.degreeTextField resignFirstResponder];
        [self.specialTextField resignFirstResponder];
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
