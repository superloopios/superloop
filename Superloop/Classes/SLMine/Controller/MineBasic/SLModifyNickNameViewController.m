//
//  SLModifyNickNameViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLModifyNickNameViewController.h"
#import <Masonry.h>
#import "AppDelegate.h"
#import <AFNetworking.h>
#import "SLAPIHelper.h"
@interface SLModifyNickNameViewController()<UIAlertViewDelegate,UITextFieldDelegate>
@property (nonatomic, strong)UITextField *nickTextField;
@end
@implementation SLModifyNickNameViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"修改昵称";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"修改昵称";
    [self setUpNav];
//    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(ensureChangeNickname)];
    
    
    [self setUpUI];
}
- (void)setUpUI
{
    self.view.backgroundColor = SLColor(255, 255, 255);
    UIView *mainView = [[UIView alloc] init];
    mainView.frame = CGRectMake(0, 74, self.view.width, 50);
    mainView.backgroundColor = SLColor(238, 238, 238);
    [self.view addSubview:mainView];
   
    UITextField *nickTextField = [[UITextField alloc] init];
    nickTextField.placeholder = @"请输入新的昵称";
    //根据需求需要在这里显示原有的昵称
    nickTextField.text = self.nickName;
    nickTextField.delegate = self;
    nickTextField.frame = CGRectMake(20, 5, self.view.frame.size.width, 44);
    nickTextField.font = [UIFont systemFontOfSize:13];
    self.nickTextField = nickTextField;
    self.nickTextField.backgroundColor = SLColor(238, 238, 238);
    
    [mainView addSubview:nickTextField];

    UILabel *nickLabel = [[UILabel alloc] init];
    nickLabel.frame = CGRectMake(20,mainView.frame.origin.y + 50, self.view.frame.size.width-40, 30);
    nickLabel.numberOfLines=0;
//    nickLabel.text = @"*请勿使用空格或特殊字符";
      NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:@"*昵称只能由中文、英文、数字、字母、下划线、横线组成"];
    NSRange redRange = NSMakeRange([[noteStr string] rangeOfString:@"*"].location, [[noteStr string] rangeOfString:@"*"].length);
    [noteStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:redRange];
    [nickLabel setAttributedText:noteStr];
    nickLabel.font = [UIFont systemFontOfSize:12];
    nickLabel.textAlignment = NSTextAlignmentCenter;


    [self.view addSubview:nickLabel];
    UIImageView *cutImageView = [[UIImageView alloc] init];
    cutImageView.backgroundColor = SLColor(183, 183, 183);
    cutImageView.frame = CGRectMake(0, mainView.frame.size.height -1, self.view.frame.size.width, 0.5);
    [mainView addSubview:cutImageView];
    UIImageView *cutImageView1 = [[UIImageView alloc] init];
    cutImageView1.backgroundColor = SLColor(183, 183, 183);
    cutImageView1.frame = CGRectMake(0, 0 , self.view.frame.size.width, 0.5);
    [mainView addSubview:cutImageView1];
    UIButton *ensureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    ensureBtn.frame =CGRectMake(75, nickLabel.frame.origin.y +70, self.view.frame.size.width-150, 45);
    ensureBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    ensureBtn.layer.cornerRadius = 5;
    ensureBtn.layer.masksToBounds = YES;
    [ensureBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [ensureBtn setTitle:@"确认修改" forState:UIControlStateNormal];
    ensureBtn.backgroundColor = SLColor(238, 238, 238);
    [ensureBtn addTarget:self action:@selector(ensureChangeNickname) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:ensureBtn];
    
    
    

    
}
- (void)ensureChangeNickname
{
    
    if (self.nickTextField.text.length<1) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"昵称不能为空" delegate:self cancelButtonTitle:@"好" otherButtonTitles: nil];
        [alert show];
    }else if (self.nickTextField.text.length>32){
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"昵称过长,暂不支持" delegate:self cancelButtonTitle:@"好" otherButtonTitles: nil];
        [alert show];
    }else{
        if ([self judgeModifyNickName:self.nickTextField.text]) {
            
            NSDictionary *parameters = @{@"nickname":self.nickTextField.text};
        
            [SLAPIHelper resetUsers:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeData" object:nil];                
                if (_nickNameBlcok) {
                    _nickNameBlcok(self.nickTextField.text);
                }
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [HUDManager showWarningWithText:@"修改成功"];
                });
                [self.navigationController popViewControllerAnimated:YES];
              
            } failure:^(SLHttpRequestError *failure) {
                if (failure.slAPICode==20) {
                    [HUDManager showWarningWithText:@"修改失败,昵称中包含违禁词语"];
                }else{
                 [HUDManager showWarningWithText:@"请求失败"];
                }
            }];
            
            
        }else{
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请按照要求输入昵称" delegate:self cancelButtonTitle:@"好" otherButtonTitles: nil];
            [alert show];
        }
        
  
    }
   
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
//    if ([self isContainsEmoji:string]) {
//        
//        return NO;
//    }
    
    
    return YES;
}

-(BOOL)judgeModifyNickName:(NSString *)string{
    NSString *regex = @"[\\w,_,\\-,0x4e00-0x9fa6]{1,50}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
   return [pred evaluateWithObject:string];
}


#pragma mark  -- -禁止输入emoj
//- (BOOL)isContainsEmoji:(NSString *)string {
//    __block BOOL isEomji = NO;
//    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
//     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
//         const unichar hs = [substring characterAtIndex:0];
//         if (0xd800 <= hs && hs <= 0xdbff) {
//             if (substring.length > 1) {
//                 const unichar ls = [substring characterAtIndex:1];
//                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
//                 if (0x1d000 <= uc && uc <= 0x1f77f) {
//                     isEomji = YES;
//                 }
//             }
//         } else if (substring.length > 1) {
//             const unichar ls = [substring characterAtIndex:1];
//             if (ls == 0x20e3) {
//                 isEomji = YES;
//             }
//         } else {
//             if (0x2100 <= hs && hs <= 0x27ff && hs != 0x263b) {
//                 isEomji = YES;
//             } else if (0x2B05 <= hs && hs <= 0x2b07) {
//                 isEomji = YES;
//             } else if (0x2934 <= hs && hs <= 0x2935) {
//                 isEomji = YES;
//             } else if (0x3297 <= hs && hs <= 0x3299) {
//                 isEomji = YES;
//             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50|| hs == 0x231a ) {
//                 isEomji = YES;
//             }
//         }
//     }];
//    return isEomji;
//}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.nickTextField endEditing:YES];
}

@end
