//  送出的评价
//  SLSendEvaluationController.m
//  Superloop
//
//  Created by xiaowu on 16/8/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSendEvaluationController.h"
#import <Masonry.h>
#import <AFNetworking.h>
#import "AppDelegate.h"
#import <MJExtension.h>
#import "SLCallComments.h"
#import "SLAPIHelper.h"
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "HomePageViewController.h"
#import "SLCallCommmentHeaderView.h"
#import "SLSendEvaluateTableViewCell.h"
#import "AppDelegate.h"
#import "NSString+Utils.h"

@interface SLSendEvaluationController ()<UITableViewDelegate,UITableViewDataSource,SLSendEvaluateTableViewCellDelegate>

@property (nonatomic,assign) NSInteger toOtherPage;
@property (nonatomic,strong) UITableView *myTableView;
@property (nonatomic,strong) NSMutableArray *sendDataSources;
@property (nonatomic,strong) UIView *haveNoDataView; //制空页
@end

@implementation SLSendEvaluationController
#pragma mark - 懒加载
- (NSMutableArray *)sendDataSources{
    if (!_sendDataSources) {
        _sendDataSources = [NSMutableArray new];
    }
    return _sendDataSources;
}
- (UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, ScreenW, ScreenH-64) style:UITableViewStylePlain];
        _myTableView.dataSource = self;
        _myTableView.delegate = self;
        _myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTableView.backgroundColor = SLColor(240, 240, 240);
        [self.view addSubview:_myTableView];
        [_myTableView registerClass:[SLSendEvaluateTableViewCell class] forCellReuseIdentifier:@"sendCell"];
    }
    return _myTableView;
}
-(NSInteger)toOtherPage{
    if (!_toOtherPage) {
        _toOtherPage=0;
    }
    return _toOtherPage;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLSendEvaluationController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLSendEvaluationController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = SLColor(240, 240, 240);
    [self setUpNav];
    [self setUpRefresh];
    [self.myTableView.mj_header beginRefreshing];
    [self.myTableView.mj_footer setHidden:YES];
}

- (void)setUpRefresh{
    self.myTableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    self.myTableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"送出的评价";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
    
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 加载数据
- (void)loadData{
    self.toOtherPage=0;
    NSDictionary *paraSend = @{@"type":@2, @"userId":ApplicationDelegate.userId,@"page":@"0",@"pager":@"20"};
    [SLAPIHelper getUsersAppraisals:paraSend success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        
        NSArray *otherCommentArr= [SLCallComments mj_objectArrayWithKeyValuesArray:data[@"result"][@"results"]];
        self.sendDataSources=(NSMutableArray *)otherCommentArr;
        
        if (otherCommentArr.count == 0) {
            if (!self.haveNoDataView) {
                [self addNotLoginViews:@"暂无通话评价"];
            }
        }else{
            [self.haveNoDataView removeFromSuperview];
        }
        [self.myTableView reloadData];
        if (otherCommentArr.count<20) {
            [self.myTableView.mj_header endRefreshing];
            [self.myTableView.mj_footer setHidden:YES];
        }else{
            [self.myTableView.mj_footer setHidden:NO];
            [self.myTableView.mj_header endRefreshing];
            [self.myTableView.mj_footer resetNoMoreData];
        }
    } failure:^(SLHttpRequestError *error) {
        [self.myTableView.mj_header endRefreshing];
    }];
}

-(void)loadMoreData{
    self.toOtherPage ++;
    NSDictionary *paraSend = @{@"type":@2, @"userId":ApplicationDelegate.userId,@"page":[NSString stringWithFormat:@"%ld",(long)self.toOtherPage],@"pager":@"20"};
    [SLAPIHelper getUsersAppraisals:paraSend success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        
        NSArray *otherCommentArr= [SLCallComments mj_objectArrayWithKeyValuesArray:data[@"result"][@"results"]];
        [self.sendDataSources addObjectsFromArray:otherCommentArr];
        [self.myTableView reloadData];
        if (otherCommentArr.count<20) {
            [self.myTableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            [self.myTableView.mj_footer endRefreshing];
        }
    } failure:^(SLHttpRequestError *error) {
        [self.myTableView.mj_footer endRefreshing];
    }];
}
#pragma mark - tableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.sendDataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLSendEvaluateTableViewCell *sendCell = [tableView dequeueReusableCellWithIdentifier:@"sendCell"];
    sendCell.sendCallComment = self.sendDataSources[indexPath.row];
    sendCell.delegate = self;
    sendCell.indexPath = indexPath;
    sendCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return sendCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLCallComments *model = self.sendDataSources[indexPath.row];
    return [self calculateHeight:model];
}
#pragma mark - SLSendEvaluateTableViewCellDelegate
- (void)SLSendEvaluateTableViewCell:(SLSendEvaluateTableViewCell *)sendEvaluateTableViewCell didSelectAtIndexWithLaunchModel:(SLCallComments *)model indexPath:(NSIndexPath *)indexPath{
    [self.sendDataSources replaceObjectAtIndex:indexPath.row withObject:model];
    [self.myTableView reloadData];
}
- (void)SLSendEvaluateTableViewCell:(SLSendEvaluateTableViewCell *)sendEvaluateTableViewCell didSelectAtIndexWithModel:(SLCallComments *)model indexPath:(NSIndexPath *)indexPath{
    HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
    otherPerson.userId = [NSString stringWithFormat:@"%ld",[model.to_user_id integerValue]];
    [self.navigationController pushViewController:otherPerson animated:YES];
}
- (void)SLSendEvaluateTableViewCell:(SLSendEvaluateTableViewCell *)sendEvaluateTableViewCell didSelectAtIndexWithReplyLaunchModel:(SLCallComments *)model indexPath:(NSIndexPath *)indexPath{
    [self.sendDataSources replaceObjectAtIndex:indexPath.row withObject:model];
    [self.myTableView reloadData];
}
#pragma mark - 计算cell高度
- (CGFloat)calculateHeight:(SLCallComments *)model{
    CGFloat cellHeight;
    cellHeight = 59;
    CGFloat standardHeight = [self getSpaceLabelHeight:@"高度，高度" withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-70];
    CGFloat titleHeight = [self getSpaceLabelHeight:model.rate_content withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-70];
    NSInteger count = titleHeight/standardHeight;
    if (titleHeight/standardHeight>count) {
        count +=1;
    }
    if (count > 5) {
        if (!model.isClose) {
            count = 5;
        }
        cellHeight += 30+8+1+15+26+15;
    }else{
        cellHeight += 15+26+15+15+5;
    }
    cellHeight += standardHeight * count ;
    
    //回复评论
    if (![NSString isEmptyStrings:model.reply_content]) {
        CGFloat replyStandardHeight = [self getSpaceLabelHeight:@"高度，高度" withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-100];
        CGFloat replyTitleHeight = [self getSpaceLabelHeight:model.reply_content withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-100];
        NSInteger replyCount = replyTitleHeight/replyStandardHeight;
        if (replyTitleHeight/replyStandardHeight>replyCount) {
            replyCount +=1;
        }
        if (replyCount > 5) {
            if (!model.isReplyClose) {
                replyCount = 5;
            }
            cellHeight += 30+22;
            
        }else{
            if (replyCount == 1) {
                cellHeight += 30+17-8;
            }else{
                cellHeight += 30+17;
            }
        }
        cellHeight += replyStandardHeight * replyCount ;
    }
    return cellHeight;
}
//计算UILabel的高度(带有行间距的情况)
- (CGFloat)getSpaceLabelHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = 8;
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f
                          };
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return size.height;
}


// 没有内容
-(void)addNotLoginViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0,64, ScreenW, ScreenH-64)];
    
    UILabel *tiplabel = [[UILabel alloc]init];
    
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(500,40) withFont:17];
    
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 40);
    if (ScreenW == 200) {
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width , 40);
    }else{
        
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width + 20 , 40);
    }
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentLeft;
    tiplabel.numberOfLines = 0;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    
    tiplabel.attributedText = AttributedStr;
    
    
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    _haveNoDataView  = contentView;
    [self.view addSubview:contentView];
}

@end
