//
//  SLMineBasicInfoViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//   我的控制器
#import "SLSuperNumberViewController.h"
#import "SLIntroduceViewController.h"
#import "SLEducationViewController.h"
#import "SLExperienceViewController.h"
#import "SLModifyNickNameViewController.h"
#import "SLMineBasicInfoViewController.h"
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "SLLocationViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "SLAPIHelper.h"
@interface SLMineBasicInfoViewController()<UIActionSheetDelegate,CLLocationManagerDelegate>{
    //后台返回的是 经纬度需要特殊的处理下
    CLLocationManager* _locationManager;
    CLGeocoder *_geocoder;
}
@property (nonatomic, strong)SLBaseModel *nickName;
@property (nonatomic, strong)SLBaseModel *male;
@property (nonatomic, strong)SLBaseModel *place;
@property (nonatomic, strong)SLBaseModel *introduce;
@property (nonatomic, strong)SLBaseModel *experience;
@property (nonatomic, strong)SLBaseModel *education;
@property (nonatomic, strong)SLBaseModel *superNumber;
@property (nonatomic, strong) NSDictionary *resultDict;
@property (nonatomic, strong) NSString *str;
@property (nonatomic,strong)NSString *intro;
@property (nonatomic,strong)NSString *username;
@property (nonatomic,strong)__block NSArray *workArray;
@property (nonatomic,strong)__block NSArray *educationArray;

@end
@implementation SLMineBasicInfoViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"基本信息";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"基本信息";
    self.tableView.rowHeight = 55;
    //[self getDatas];
    [self setUpNav];
    [self loadData];
//    [self loadTableView];
    [self setupLocation];
//    [self getDatas];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateData:) name:@"SaveEducation" object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateData:) name:@"ChangeEducation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateData:) name:@"SaveExperience" object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateData:) name:@"ChangeExperience" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteEducationDatas:) name:@"deleteEducation" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteExperienceDatas:) name:@"deleteExperience" object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeEducationArr:) name:@"changeEducationArr" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeExperienceArr:) name:@"changeExperienceArr" object:nil];
    
}
// 删除
-(void)deleteEducationDatas:(NSNotification *)notif{
    if (notif.object!=nil) {
        [self getDatas];
    }

}
-(void)deleteExperienceDatas:(NSNotification *)notif{
    if (notif.object!=nil) {
        [self getDatas];
    }
}
-(void)getDatas{
    NSDictionary *parameters =  @{@"id":ApplicationDelegate.userId};
    [SLAPIHelper getUsersData:parameters success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        NSDictionary *dict=data[@"result"];
        self.educationArray=dict[@"education"];
        self.workArray=dict[@"work_history"];
        if (self.educationArray.count>0) {
            self.education.rightTitle=self.educationArray[0][@"institution"];
        }else{
            self.education.rightTitle=@"";
        }
        if (self.workArray.count>0) {
            self.experience.rightTitle=self.workArray[0][@"company"];
        }else{
            self.experience.rightTitle=@"";
        }
        [self.tableView reloadData];
    } failure:^(SLHttpRequestError *failure) {
        
    }];

}


- (void)loadData
{
    if (self.userData) {
        self.resultDict =self.userData;
        [self loadTableView];
        [self.tableView reloadData];
        //数据请求成功之后就开始定位
        [_locationManager startUpdatingLocation];
    }
}
- (void)loadTableView
{
    SLBaseModel *superNumber = [SLBaseModel modelWithTitle:@"超级号" type:SLSettingCellTypeLabel];
    superNumber.rightTitle = self.resultDict[@"username"];
    //判断是否修改过
    _superNumber = superNumber;
    
    SLBaseModel *nickName = [SLBaseModel modelWithTitle:@"昵称" type:SLSettingCellTypeLabel];
    nickName.rightTitle = self.resultDict[@"nickname"];
    
  
    
    _nickName = nickName;
    
    SLBaseModel *male = [SLBaseModel modelWithTitle:@"性别" type:SLSettingCellTypeLabel];
    if (!self.resultDict[@"gender"]) {
         male.rightTitle = @"男";
    }else{
        
        if ([self.resultDict[@"gender"] isEqualToString:@"MALE"]) {
            male.rightTitle = @"男";
        }else
        {
            male.rightTitle = @"女";
        }

        
    }
    
    _male = male;
    SLBaseModel *place = [SLBaseModel modelWithTitle:@"所在地" type:SLSettingCellTypeLabel];

    NSString *everPlaceLocation = self.resultDict[@"location"];
    place.rightTitle = everPlaceLocation ;
    
    _place = place;
    SLBaseModel *introduce = [SLBaseModel modelWithTitle:@"简介" type:SLSettingCellTypeLabel];
    introduce.rightTitle = self.resultDict[@"bio"];
    
    _introduce = introduce;
    
    SLBaseModel *experience = [SLBaseModel modelWithTitle:@"工作经历" type:SLSettingCellTypeLabel];
    _workArray = self.resultDict[@"work_history"];

    if (_workArray.count > 0) {
        experience.rightTitle = _workArray[0][@"company"];
    }
    
    _experience = experience;
    
    SLBaseModel *educationModel = [SLBaseModel modelWithTitle:@"教育背景" type:SLSettingCellTypeLabel];
    _educationArray = self.resultDict[@"education"];

 
    if (_educationArray.count > 0) {
        
        educationModel.rightTitle = _educationArray[0][@"institution"];
    
    }
    
    _education = educationModel;
    
    SLBaseGroup *group1= [[SLBaseGroup alloc]init];
    group1.cells =@[superNumber,nickName, male, place, introduce, experience, educationModel];
    _dataArray = @[group1];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.y = 44;
  
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self changeInformationWithModeltag:1 string:@"男" withCityId:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeSex" object:@"男"];
    }else if (buttonIndex == 1){
        [self changeInformationWithModeltag:1 string:@"女" withCityId:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeSex" object:@"女"];
    }
    
}
- (void)changeInformationWithModeltag:(NSInteger)cellTag string:(NSString *)string withCityId:(NSString *)cityId
{
    switch (cellTag) {
        case 0:
            
            _nickName.rightTitle = string;
            break;
        case 1:
        {
            static NSString *male = @"";
            _male.rightTitle = string;
            if ([string isEqualToString:@"男"]) {
                male = @"MALE";
            }else
            {
                male = @"FEMALE";
            }
            NSDictionary *parameters = @{@"gender":male};
            [self resetUserInfo:parameters];
            break;
        }
        case 2:
        {
            _place.rightTitle = string;
             NSDictionary *parameters = @{@"location_code":cityId};
            [self resetUserInfo:parameters];
            break;
        }
        case 3:
            _introduce.rightTitle = string;
            break;
        case 4:
            _experience.rightTitle = string;
            break;
        case 5:
            _education.rightTitle = string;

            break;
        case 6:
            _superNumber.rightTitle = string;
            break;
        default:
            
            break;
    }
    [self.tableView reloadData];
}

// 网络处理方法
-(void)resetUserInfo:(NSDictionary *)paramter{
    
    [SLAPIHelper resetUsers:paramter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        //修改完成刷新h5页面
        [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeData" object:nil];
//        [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//            
//        }];
    } failure:nil];
}


#pragma mark - Private methods
- (void)setupLocation {
    //初始化位置信息
    _locationManager = [[CLLocationManager alloc]init];
    _geocoder        = [[CLGeocoder alloc]init];
    
    //如果没有授权则请求用户授权
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined){
        [_locationManager requestWhenInUseAuthorization];
    }
    
    //设置代理
    _locationManager.delegate        = self;
    //设置定位精度
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //定位频率,每隔多少米定位一次
    CLLocationDistance distance      = 10.0;//十米定位一次
    _locationManager.distanceFilter  = distance;
    
    
}

/**
 *  @brief  将经纬度转换为位置
 *
 *  @param latitude
 *  @param longitude
 */
-(void)getAddressByLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude{
    //反地理编码
    CLLocation *location=[[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    [_geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if (error != nil) {
            NSLog(@"获取当前位置失败!");
        } else {
            [HUDManager hideHUDView];
            CLPlacemark *placemark=[placemarks firstObject];
            NSString* titleStr;
            NSLog(@"%@",placemark.administrativeArea);//省份
            NSLog(@"%@",placemark.locality);//城市
            NSLog(@" name %@",placemark.name);//县
            NSLog(@"addressDictionary %@",placemark.addressDictionary);
            NSLog(@" subAdministrativeArea %@",placemark.subAdministrativeArea);
            NSLog(@" subLocality %@",placemark.subLocality);//县
            titleStr = placemark.name;
            if (titleStr) {
                
            }
        }
    }];
}

- (void)upDateData:(NSNotification *)notif{
    if (notif.userInfo !=nil) {
        [self getDatas];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        //        SLSuperNumberViewController *superNumber = [[SLSuperNumberViewController alloc] init];
        //        superNumber.username = self.username.length > 0 ? self.username : self.resultDict[@"username"];
        //        superNumber.superNumberBlcok = ^(NSString *str){
        //            [self changeInformationWithModeltag:6 string:str withCityId:nil];
        //            self.username = str;
        //        };
        //        superNumber.event = self.event;
        //        [self.navigationController pushViewController:superNumber animated:YES];
    }else if (indexPath.row == 1){
        SLModifyNickNameViewController *modifyNickname = [[SLModifyNickNameViewController alloc] init];
        //根据需求需要传入nickName
        modifyNickname.nickName = self.str.length > 0 ? self.str : self.resultDict[@"nickname"];
        
        modifyNickname.nickNameBlcok = ^(NSString *str){
            [self changeInformationWithModeltag:0 string:str withCityId:nil];
            self.str = str;
        };
        modifyNickname.event=self.event;
        
        [self.navigationController pushViewController:modifyNickname animated:YES];
    }else if (indexPath.row == 2){
        
        UIActionSheet *sheetMale= [[UIActionSheet alloc] initWithTitle:@"性别修改" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"男",@"女", nil];
        sheetMale.tag = 2000;
        [sheetMale showInView:self.view];
        
    }else if (indexPath.row == 3){
        
        SLLocationViewController *location = [[SLLocationViewController alloc] init];
        location.isMineBasicInfoLocation = YES;
        location.tag = 2000;
        location.event = self.event;
        
        location.valueWithCityIdBlcok = ^(NSString *str,NSString *cityId){
            [self changeInformationWithModeltag:2 string:str withCityId:cityId];
             
        };
        [self.navigationController pushViewController:location animated:YES];
        
    }else if (indexPath.row == 4){
        
        SLIntroduceViewController *introduce = [[SLIntroduceViewController alloc] init];
        //若果之前是有数据的就展示之前得数据
//        introduce.event = self.event;
        introduce.intro = self.intro.length > 0 ? self.intro : self.resultDict[@"bio"];
        introduce.introduceBlcok= ^(NSString *str){
            [self changeInformationWithModeltag:3 string:str withCityId:nil];
            self.intro = str;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeData" object:nil];
        };
        [self.navigationController pushViewController:introduce animated:YES];
        
    }else if (indexPath.row == 5){
        
        SLExperienceViewController *experience = [[SLExperienceViewController alloc] init];
        experience.event =self.event;
        experience.experienceArray = [_workArray mutableCopy];
        experience.experienceBlcok= ^(NSString *str){
            [self changeInformationWithModeltag:4 string:str withCityId:nil];
        };
        
        [self.navigationController pushViewController:experience animated:YES];
        
    }else if (indexPath.row == 6){
        SLEducationViewController *education = [[SLEducationViewController alloc] init];
        education.educationArray = [_educationArray mutableCopy];
        
//        education.event=self.event;
        
        education.educationBlcok= ^(NSString *str){
            [self changeInformationWithModeltag:5 string:str withCityId:nil];
        };
        
        [self.navigationController pushViewController:education animated:YES];
        
    }
}

@end
