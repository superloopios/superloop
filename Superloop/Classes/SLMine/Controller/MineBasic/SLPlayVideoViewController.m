//
//  SLPlayVideoViewController.m
//  Superloop
//
//  Created by WangS on 16/9/5.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLPlayVideoViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AliyunOSSiOS/OSSService.h>
#import "AppDelegate.h"
#import "SLAPIHelper.h"

@interface SLPlayVideoViewController ()
@property (nonatomic,strong) UIButton *uploadBtn;
@property (nonatomic,strong) NSData *data;
@end

@implementation SLPlayVideoViewController{
    AVPlayer *player;
    AVPlayerLayer *playerLayer;
    AVPlayerItem *playerItem;
    UIImageView* playImg;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
-(BOOL)prefersStatusBarHidden{
    return true;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    [self setUpNav];
    
    float videoWidth = ScreenW;
    float videoHeight = ceilf(3/4.0 * ScreenW);
    
    AVAsset *movieAsset = [AVURLAsset URLAssetWithURL:self.videoURL options:nil];
    playerItem = [AVPlayerItem playerItemWithAsset:movieAsset];
    player = [AVPlayer playerWithPlayerItem:playerItem];
    
    playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
    playerLayer.frame = CGRectMake(0, 44, videoWidth, videoHeight);
    playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    [self.view.layer addSublayer:playerLayer];
    
    UITapGestureRecognizer *playTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playOrPause)];
    [self.view addGestureRecognizer:playTap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playingEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    
    playImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
    playImg.center = CGPointMake(videoWidth/2, 64+80);
    [playImg setImage:[UIImage imageNamed:@"videoPlay"]];
    [playerLayer addSublayer:playImg.layer];
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 44)];
    navView.backgroundColor=[UIColor blackColor];
    [self.view addSubview:navView];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 0, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"closeWindow"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 7, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"视频预览";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor whiteColor];
    [navView addSubview:nameLab];
    
    UIButton *uploadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    uploadBtn.frame = CGRectMake(0, 0, 306, 56);
    uploadBtn.center =CGPointMake(ScreenW/2.0, 44+ceilf(3/4.0 * ScreenW)+36+56);
    [uploadBtn setTitle:@"上传背景视频" forState:UIControlStateNormal];
    uploadBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    [uploadBtn setTitleColor:UIColorFromRGB(0x17ae98) forState:UIControlStateNormal];
    [uploadBtn makeCornerRadius:5 borderColor:UIColorFromRGB(0x17ae98) borderWidth:1];
    [uploadBtn addTarget:self action:@selector(uploadBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.uploadBtn = uploadBtn;
    [self.view addSubview:uploadBtn];
    
    UIButton *retryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    retryBtn.frame = CGRectMake(0, 0, 306, 56);
    retryBtn.center =CGPointMake(ScreenW/2.0, CGRectGetMaxY(uploadBtn.frame)+26+56);
    [retryBtn setTitle:@"重拍" forState:UIControlStateNormal];
    retryBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    [retryBtn setTitleColor:UIColorFromRGB(0x17ae98) forState:UIControlStateNormal];
    [retryBtn makeCornerRadius:5 borderColor:UIColorFromRGB(0x17ae98) borderWidth:1];
    [retryBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:retryBtn];
    
}
-(void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)uploadBtnClick{
    self.uploadBtn.enabled = NO;
    [self publishToAlibaba:self.videoURL];//上传
    [self saveRecordedFile:self.videoURL];//保存
}
-(void)playOrPause{
    if (playImg.isHidden) {
        playImg.hidden = NO;
        [player pause];
        
    }else{
        playImg.hidden = YES;
        [player play];
    }
}

- (void)pressPlayButton{
    [playerItem seekToTime:kCMTimeZero];
    [player play];
}

- (void)playingEnd:(NSNotification *)notification{
    if (playImg.isHidden) {
        [self pressPlayButton];
    }
}
- (void)saveRecordedFile:(NSURL *)recordedFile {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        ALAssetsLibrary *assetLibrary = [[ALAssetsLibrary alloc] init];
        [assetLibrary writeVideoAtPathToSavedPhotosAlbum:recordedFile
                                         completionBlock:
         ^(NSURL *assetURL, NSError *error) {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 //[HUDManager hideHUDView];
                 
//                 NSString *title;
//                 NSString *message;
                 
                 if (error != nil) {
                     [HUDManager showAlertWithText:@"保存相册失败"];
//                     title = @"保存相册失败";
//                     message = [error localizedDescription];
                 }
                 else {
//                     title = @"保存相册成功";
//                     message = nil;
                 }
                 
//                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                                 message:message
//                                                                delegate:nil
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil];
//                 [alert show];
             });
         }];
    });
}
//上传视频到阿里云服务器
- (void)publishToAlibaba:(NSURL *)upDataUrl{
    
    if (!ApplicationDelegate.Basic) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
        alert.tag=100;
        [alert show];
        return;
    }
    if (self.videoURL>0) {
        [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow withText:@""];

#ifdef DEBUG
        NSString *endpoint = @"http://dev.oss.superloop.com.cn";
#else
        NSString *endpoint = @"http://oss.superloop.com.cn";
#endif
        id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:@"UPhdoMR1eN1hnrdA" secretKey:@"ETuIdMVCT1mtiqs8f5KZ8dZnZLiXfG"];
        OSSClient *client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential];
        
        OSSPutObjectRequest *put = [OSSPutObjectRequest new];
#ifdef DEBUG
        put.bucketName = @"superloop-dev";
#else
        put.bucketName = @"superloop";
#endif
        NSString *uuid= [[NSUUID UUID] UUIDString];
        //减去"-"
        NSString *str = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
        //变小写
        NSString *lower = [str lowercaseString];
        
        put.objectKey = [NSString stringWithFormat:@"%@/%@.mp4",ApplicationDelegate.userId,lower];
       
        NSLog(@"put.objectKey----------%@",put.objectKey);
        put.uploadingFileURL = upDataUrl;
        //put.uploadingData = upData; // 直接上传NSData
        //put.contentMd5 = [OSSUtil base64Md5ForData:self.upData]; // 如果是二进制数据
        put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
        };
        OSSTask * putTask = [client putObject:put];
        [putTask continueWithBlock:^id(OSSTask *task) {
            if (!task.error) {
                dispatch_async(dispatch_get_main_queue(), ^{
//                    [HUDManager hideHUDView];
                    [self publishImageToAlibaba:upDataUrl videoName:put.objectKey];
                });
                
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.uploadBtn.enabled = YES;
                    [HUDManager hideHUDView];
                    [HUDManager showWarningWithText:@"上传失败，请稍后重试"];
                });
            }
            return nil;
        }];
    }
    
}

- (void)publishImageToAlibaba:(NSURL *)upDataUrl videoName:(NSString *)videoName{
    
    NSString *uuid= [[NSUUID UUID] UUIDString];
    //减去"-"
    NSString *str = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    //变小写
    NSString *lower = [str lowercaseString];
    
    UIImage *firstImg = [self getVideoFirstPic:upDataUrl];
    [self saveImage:firstImg withName:lower];
    
    NSString *imgName =[NSString stringWithFormat:@"%@/%@.jpg",ApplicationDelegate.userId,lower];
    
#ifdef DEBUG
        NSString *endpoint = @"http://dev.oss.superloop.com.cn";
#else
        NSString *endpoint = @"http://oss.superloop.com.cn";
#endif
        id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:@"UPhdoMR1eN1hnrdA" secretKey:@"ETuIdMVCT1mtiqs8f5KZ8dZnZLiXfG"];
        OSSClient *client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential];
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            UIWindow *win = [[UIApplication sharedApplication].windows objectAtIndex:0];
//            [HUDManager showLoadingHUDView:win withText:@"正在上传中"];
//        });
    
            OSSPutObjectRequest *put = [OSSPutObjectRequest new];
#ifdef DEBUG
            put.bucketName = @"superloop-dev";
#else
            put.bucketName = @"superloop";
#endif
  
    put.objectKey = imgName;
    put.uploadingData = self.data; // 直接上传NSData
    OSSTask * putTask = [client putObject:put];
    [putTask continueWithBlock:^id(OSSTask *task) {
        if (!task.error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self uploadService:videoName video_cover:imgName];
            });
            
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.uploadBtn.enabled = YES;
                [HUDManager hideHUDView];
                [HUDManager showWarningWithText:@"上传失败，请稍后重试"];
            });
        }
        return nil;
    }];
}

-(void)uploadService:(NSString *)objectKey video_cover:(NSString *)imgName{
    NSDictionary *parameters = @{@"video_cover":objectKey,@"cover":imgName};
    [SLAPIHelper introduce:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUDManager hideHUDView];
            [HUDManager showWarningWithText:@"上传成功"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"resetUserVideo" object:self];
            //[self.navigationController popViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
        });
        
    } failure:^(SLHttpRequestError *error) {
        NSLog(@"error ------  %@",error);
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUDManager hideHUDView];
            [HUDManager showWarningWithText:@"上传失败"];
            self.uploadBtn.enabled = YES;
        });
    }];
    
}
// 获取视频的第一帧
- (UIImage *)getVideoFirstPic:(NSURL *)url{
    // 获取资源类
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    // 视频中截图类
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    //设置时间为0秒
    CMTime time = CMTimeMakeWithSeconds(0, 10);
    // 取出视频在0秒时候的图片
    CGImageRef image = [generator copyCGImageAtTime:time actualTime:nil error:nil];
    UIImage *thumb = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    return thumb;
}
#pragma mark - 保存图片至沙盒
- (void) saveImage:(UIImage *)currentImage withName:(NSString *)imageName{
    NSData *imageData = UIImageJPEGRepresentation(currentImage, 0.01);
    // 获取沙盒目录
    NSString *fullPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:imageName];
    // 将图片写入文件
    [imageData writeToFile:fullPath atomically:NO];
    NSData *data = [NSData dataWithContentsOfFile:fullPath];
    self.data = data;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [HUDManager hideHUDView];
    [player pause];
    player = nil;
}

@end
