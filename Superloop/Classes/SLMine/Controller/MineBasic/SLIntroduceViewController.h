//
//  SLIntroduceViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLJavascriptBridgeEvent.h"
#import "SLBaseTableViewController.h"

@interface SLIntroduceViewController : UIViewController
@property (nonatomic ,strong) void(^introduceBlcok)(NSString *str);
@property (nonatomic , strong) NSString *intro;
//@property (nonatomic,strong) SLJavascriptBridgeEvent *event;
@end
