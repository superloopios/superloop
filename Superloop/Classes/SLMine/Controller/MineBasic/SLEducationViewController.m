//
//  SLEducationViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "SLNewEducationViewController.h"
#import "SLEducationViewController.h"
#import "SLEducationViewCell.h"
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "SLAPIHelper.h"
static NSString *const SaveEducation = @"SaveEducation";
@interface SLEducationViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UIButton *_submitBtn;
}


@property (nonatomic,strong) NSString  *scollStr;
@property (nonatomic,strong) NSString  *startTimeStr;
@property (nonatomic,strong) NSString  *endTimeStr;
@property (nonatomic,strong) NSString  *profissionStr;
@property (nonatomic,strong) NSString  *projectStr;
@property(nonatomic, weak)UITableView *tableView;

@property(nonatomic,assign)NSInteger recordCellIndex;

@end
@implementation SLEducationViewController

- (UITableView *)tableView{
    if (!_tableView) {
        UITableView *tableview = [[UITableView alloc] init];
        tableview.delegate = self;
        tableview.dataSource = self;
        //        tableview.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableview.frame = CGRectMake(0, 64, screen_W, screen_H - 64);
        [tableview registerNib:[UINib nibWithNibName:@"SLEducationViewCell" bundle:nil] forCellReuseIdentifier:@"experienceCell"];
       // tableview.frame = CGRectMake(0, 64, screen_W, screen_H - 64);
        [self.view addSubview:tableview];
        _tableView = tableview;
    }
    return _tableView;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}


- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
//    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(30, 27, ScreenW-80, 30)];
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"教育背景";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    _submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    [_submitBtn setTitle:@"编辑" forState:UIControlStateNormal];
    _submitBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_submitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(deleteEducation) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:_submitBtn];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getDatas];
    [self setUpNav];
    [self setUpUI];
    self.view.backgroundColor = SLColor(240, 240, 240);
//    self.navigationItem.title = @"编辑教育信息";
    self.tableView.backgroundColor = SLColor(240, 240, 240);
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStyleDone target:self action:@selector(deleteEducation)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateData:) name:SaveEducation object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateData:) name:@"ChangeEducation" object:nil];
    
}
- (void)deleteEducation
{
    [self.tableView setEditing:!self.tableView.isEditing animated:YES];
    if (self.tableView.isEditing) {
        [_submitBtn setTitle:@"取消" forState:UIControlStateNormal];
    }else
    {
        [_submitBtn setTitle:@"编辑" forState:UIControlStateNormal];
    }
   
}


- (void)setUpUI
{
    UIView *addView= [[UIView alloc] init];
    addView.frame = CGRectMake(0, 0, self.view.width, 60);
    self.tableView.tableFooterView = addView;
    UIButton *addEducation = [UIButton buttonWithType:UIButtonTypeCustom];
    addEducation.backgroundColor = [UIColor whiteColor];
    [addEducation setTitle:@"+添加教育背景" forState:UIControlStateNormal];
    [addEducation setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    addEducation.layer.cornerRadius = 5;
    addEducation.layer.masksToBounds = YES;
    addEducation.layer.borderColor = SLColor(165, 165, 165).CGColor;
    addEducation.layer.borderWidth = 0.5;
    addEducation.frame = CGRectMake(10, 10, self.view.width - 20, 40);
    [addEducation addTarget:self action:@selector(addEducation) forControlEvents:UIControlEventTouchUpInside];
    [addView addSubview:addEducation];
    
}
- (void)addEducation
{
    SLNewEducationViewController *newEdu =[[SLNewEducationViewController alloc] init];
//    newEdu.event=self.event;
    [self.navigationController pushViewController:newEdu animated:YES];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _educationArray.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SLEducationViewCell *educationCell = [tableView dequeueReusableCellWithIdentifier:@"experienceCell"];
    if (!educationCell) {
        educationCell = [[SLEducationViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"experienceCell"];
    }
    
    if (((NSString *)(_educationArray[indexPath.row][@"start_date"])).length>0&&((NSString *)(_educationArray[indexPath.row][@"end_date"])).length>0) {
       educationCell.timeLabel.text = [NSString stringWithFormat:@"%@-%@", _educationArray[indexPath.row][@"start_date"], _educationArray[indexPath.row][@"end_date"]];
    }else{
    educationCell.timeLabel.text = @"";
    }
    
    if (((NSString *)(_educationArray[indexPath.row][@"institution"])).length>0) {
        educationCell.nameLabel.text = _educationArray[indexPath.row][@"institution"];
    }else{
        educationCell.nameLabel.text = @"";

    }
    if (((NSString *)(_educationArray[indexPath.row][@"degree"])).length>0) {
        educationCell.positionLabel.text = _educationArray[indexPath.row][@"degree"];
    }else{
        educationCell.positionLabel.text = @"";

    }
    if (((NSString *)(_educationArray[indexPath.row][@"department"])).length>0) {
        educationCell.locationLabel.text = _educationArray[indexPath.row][@"department"];
    }else{
        educationCell.locationLabel.text = @"";

    }
    NSString *locationStr=_educationArray[indexPath.row][@"department"];
    
    if (locationStr.length>0) {
        educationCell.cutImgVIew.hidden=NO;
        //根据计算文字的大小
        CGFloat positionWidth= [_educationArray[indexPath.row][@"degree"] boundingRectWithSize:CGSizeMake(ScreenW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.width+2;
        
        CGFloat locationWidth= [locationStr boundingRectWithSize:CGSizeMake(ScreenW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.width+2;
        if (positionWidth>220) {
            positionWidth=220;
        }
        if (locationWidth+positionWidth+30>ScreenW){
            locationWidth=ScreenW-positionWidth-30;
        }
        [educationCell.positionLabConstraint setConstant:ScreenW-positionWidth-30];
        [educationCell.locationLabConstraint setConstant:ScreenW-positionWidth-locationWidth-40];
    }else{
        educationCell.cutImgVIew.hidden=YES;
        [educationCell.positionLabConstraint setConstant:10];
    }
    return educationCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 删除模型
    NSDictionary *parameters = @{@"id":_educationArray[indexPath.row][@"id"]};
    [SLAPIHelper deleteEducation:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
       // NSLog(@"%@",data);
        
        [self.educationArray removeObjectAtIndex:indexPath.row];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //内容修改成功通知上个控制器
            [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteEducation" object:self.educationArray];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeData" object:nil];
            //修改完成刷新h5页面
//            [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//                
//            }];
        });
       
        // 刷新
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
    } failure:nil];
}


- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SLNewEducationViewController *newEdu = [[SLNewEducationViewController alloc] init];

    newEdu.scollStr = _educationArray[indexPath.row][@"institution"];
    newEdu.startTimeStr = _educationArray[indexPath.row][@"start_date"];
    newEdu.endTimeStr = _educationArray[indexPath.row][@"end_date"];
    newEdu.profissionStr = _educationArray[indexPath.row][@"degree"];
    newEdu.projectStr = _educationArray[indexPath.row][@"department"];
   
    newEdu.educationID = _educationArray[indexPath.row][@"id"];

    newEdu.isSelectRow=YES;


    [self.navigationController pushViewController:newEdu animated:YES];
}
-(void)getDatas{
    NSDictionary *parameters =  @{@"id":ApplicationDelegate.userId};
    [SLAPIHelper getUsersData:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSDictionary *dict = data[@"result"];
        NSArray *educationArray = dict[@"education"];
        NSMutableArray *mutableArr=[[NSMutableArray alloc] init];
        [mutableArr addObjectsFromArray:educationArray];
        self.educationArray=mutableArr;
        [self.tableView reloadData];
    } failure:nil];
}
- (void)upDateData:(NSNotification *)notif{
    if (notif.userInfo!=nil) {
        [self getDatas];
    }
}

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
