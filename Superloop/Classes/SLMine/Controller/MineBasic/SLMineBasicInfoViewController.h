//
//  SLMineBasicInfoViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/11.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "SLBaseTableViewController.h"
#import "SLJavascriptBridgeEvent.h"
@interface SLMineBasicInfoViewController : SLBaseTableViewController
@property (nonatomic,strong)SLJavascriptBridgeEvent *event;
@property(nonatomic,strong)NSDictionary *userData;
@end
