//
//  SLNewPersonTagCollectionViewCell.h
//  Superloop
//
//  Created by xiaowu on 16/11/22.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLNewPersonTagCollectionViewCell : UICollectionViewCell

@property(nonatomic, copy)NSString *cTitle;

@property (nonatomic, copy)NSString *type;

@end
