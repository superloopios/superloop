//
//  SLBioCell.m
//  Superloop
//
//  Created by xiaowu on 16/8/18.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLBioCell.h"
#import "NSString+Utils.h"
#import "AppDelegate.h"
#import "ColorUtility.h"

@interface SLBioCell ()

@property(nonatomic, strong)UILabel *introLabel;

@property(nonatomic, strong)SLLabel *subTitleLabel;

@property(nonatomic, strong)UIImageView *enterIv;

@property(nonatomic, assign)BOOL isBeyondFive;

@property(nonatomic, assign)BOOL isclose;

@property(nonatomic, strong)UIButton *moreDataBtn;

@property(nonatomic, strong)UIView *sepeView;


@end

@implementation SLBioCell

- (UIView *)sepeView{
    if (!_sepeView) {
        _sepeView = [[UIView alloc] init];
        _sepeView.backgroundColor = UIColorFromRGB(0xebddd5);
        [self.contentView addSubview:_sepeView];
    }
    return _sepeView;
}
- (UIButton *)moreDataBtn{
    if (!_moreDataBtn) {
        _moreDataBtn = [[UIButton alloc] init];
        [_moreDataBtn setImage:[UIImage imageNamed:@"Down_arrow"] forState:UIControlStateNormal];
        [_moreDataBtn setImage:[UIImage imageNamed:@"closeArrow"] forState:UIControlStateSelected];
        _moreDataBtn.selected = NO;
        _moreDataBtn.hidden = YES;
        [self.contentView addSubview:_moreDataBtn];
        [_moreDataBtn addTarget:self action:@selector(moreDataBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreDataBtn;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.isclose = YES;
    }
    return self;
}
- (SLLabel *)subTitleLabel{
    if (!_subTitleLabel) {
        _subTitleLabel = [[SLLabel alloc] init];
        _subTitleLabel.numberOfLines = 0;
        _subTitleLabel.font = [UIFont systemFontOfSize:16];
        _subTitleLabel.textColor = UIColorFromRGB(0x9f9f9f);
        [self.contentView addSubview:_subTitleLabel];
    }
    return _subTitleLabel;
}
- (UILabel *)introLabel{
    if (!_introLabel) {
        _introLabel = [[UILabel alloc] init];
        _introLabel.font = [UIFont systemFontOfSize:16];
        _introLabel.textColor = SLMainColor;
        [self.contentView addSubview:_introLabel];
        [_introLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(25);
            make.top.equalTo(self).offset(15);
            make.height.equalTo(@16);
        }];
    }
    return _introLabel;
}

- (UIImageView *)enterIv{
    if (!_enterIv) {
        _enterIv = [[UIImageView alloc] init];
        
        _enterIv.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:_enterIv];
        [_enterIv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(12);
            make.right.equalTo(self).offset(-25);
            make.width.height.equalTo(@20);
        }];
    }
    return _enterIv;
}
- (void)setDict:(NSDictionary *)dict{
    _dict = dict;
//    self.backgroundColor = [UIColor redColor];
    self.introLabel.text = @"简介";
    self.enterIv.image = [UIImage imageNamed:@"editIntro"];
    if ([dict[@"id"] isEqual:ApplicationDelegate.userId]) {
        self.enterIv.hidden = NO;
        if ([NSString isEmptyStrings:dict[@"bio"]]) {
            self.subTitleLabel.text = @"去填写简介";
            self.subTitleLabel.textColor = UIColorFromRGB(0x9f9f9f);
        }else{
            self.subTitleLabel.text = dict[@"bio"];
            [self setLabelSpace:self.subTitleLabel withValue:self.subTitleLabel.text withFont:[UIFont systemFontOfSize:16]];
            self.subTitleLabel.textColor = SLBlackTitleColor;
        }
    }else{
        self.enterIv.hidden = YES;
        if ([NSString isEmptyStrings:dict[@"bio"]]) {
            self.subTitleLabel.text = @"尚未填写简介";
            self.subTitleLabel.textColor = UIColorFromRGB(0x9f9f9f);
        }else{
            self.subTitleLabel.text = dict[@"bio"];
            [self setLabelSpace:self.subTitleLabel withValue:self.subTitleLabel.text withFont:[UIFont systemFontOfSize:16]];
            self.subTitleLabel.textColor = SLBlackTitleColor;
        }
    }
    
    
    [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(25);
        make.top.equalTo(self).offset(40);
        make.width.equalTo(@(screen_W-50));
    }];
    self.subTitleLabel.numberOfLines = [self modelHeight:dict];
    if (self.isBeyondFive) {
        self.moreDataBtn.hidden = NO;
        [self.moreDataBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.subTitleLabel.mas_bottom).offset(10);
            make.height.equalTo(@25);
            make.width.equalTo(@150);
            make.centerX.equalTo(self);
        }];
        self.moreDataBtn.imageView.contentMode = UIViewContentModeCenter;
    }else{
        self.moreDataBtn.hidden = YES;
    }
    [self.sepeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.left.equalTo(self).offset(25);
        make.right.equalTo(self).offset(-25);
        make.height.equalTo(@0.5);
    }];
}

-(NSInteger)modelHeight:(NSDictionary *)dict{
    CGFloat textMaxW = ScreenW - 50;
    NSString *titleContentextString = @"高度，高度";
    CGFloat titleFontSize = [titleContentextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:16]} context:nil].size.height;
    NSString *chooseContent = @"";
    if ([[dict allKeys] containsObject:@"bio"]) {
        chooseContent = dict[@"bio"];
    }else{
        chooseContent = @"没有简介";
    }
    if([NSString isEmptyStrings:chooseContent]){
        chooseContent = @"没有简介";
    }
    
    CGFloat titleHeight = [chooseContent boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:16],  NSKernAttributeName:@1.5f} context:nil].size.height;
    NSInteger titleLineNum = titleHeight / titleFontSize;
    
    if (self.isclose) {
        if (titleLineNum > 5) {
            titleLineNum = 5;
            self.isBeyondFive = YES;
        }else{
            self.isBeyondFive = NO;
        }
    }else{
        self.isBeyondFive = YES;
    }
    
    return titleLineNum;
}

- (void)setLabelSpace:(UILabel*)label withValue:(NSString*)str withFont:(UIFont*)font {
    
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paraStyle.lineSpacing = 6; //设置行间距
    
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f
                          };
    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
    label.attributedText = attributeStr;
}
- (void)moreDataBtn:(UIButton *)btn{
    btn.selected = !btn.selected;
    self.isclose = !self.isclose;
    if (self.seeMore) {
        self.seeMore(@"1");
    }
}

@end
