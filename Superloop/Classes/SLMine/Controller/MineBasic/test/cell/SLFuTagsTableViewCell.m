//
//  SLFuTagsTableViewCell.m
//  Superloop
//
//  Created by xiaowu on 16/8/18.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLFuTagsTableViewCell.h"
#import "SLTagLayout.h"
#import "SLNewPersonTagCollectionViewCell.h"
#import "AppDelegate.h"

@interface SLFuTagsTableViewCell ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic, strong)UILabel *tagLabel;
//主标签
@property (nonatomic,strong) UICollectionView *collectionView;

@property (nonatomic, strong)SLTagLayout *layout;

@property (nonatomic, strong)UIView *seperatorView;

@property(nonatomic, strong)UIImageView *enterIv;

@property (nonatomic, strong)UILabel *noMajorLabel;

@property (nonatomic, strong)NSMutableArray *tagXArr;

@end


@implementation SLFuTagsTableViewCell

- (NSMutableArray *)tagXArr{
    if (!_tagXArr) {
        _tagXArr = [NSMutableArray array];
    }
    return _tagXArr;
}

- (UILabel *)noMajorLabel{
    if (!_noMajorLabel) {
        _noMajorLabel = [[UILabel alloc] init];
        _noMajorLabel.text = @"去选择我喜欢的标签";
        _noMajorLabel.textColor = UIColorFromRGB(0x9f9f9f);
        _noMajorLabel.font = [UIFont systemFontOfSize:14];
        _noMajorLabel.frame = CGRectMake(0, 0, 150, 25);
        [self.collectionView addSubview:_noMajorLabel];
    }
    return _noMajorLabel;
}

- (UIImageView *)enterIv{
    if (!_enterIv) {
        _enterIv = [[UIImageView alloc] init];
        _enterIv.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:_enterIv];
        [_enterIv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).offset(-25);
            make.centerY.equalTo(self.collectionView.mas_centerY);
        }];
    }
    return _enterIv;
}
- (UIView *)seperatorView{
    if (!_seperatorView) {
        _seperatorView = [[UIView alloc] init];
        _seperatorView.backgroundColor = UIColorFromRGB(0xebddd5);
        [self.contentView addSubview:_seperatorView];
    }
    return _seperatorView;
}


- (UILabel *)tagLabel{
    if (!_tagLabel) {
        _tagLabel = [[UILabel alloc] init];
        _tagLabel.font = [UIFont systemFontOfSize:16];
        _tagLabel.textColor = UIColorFromRGB(0x4f5359);
        [self.contentView addSubview:_tagLabel];
        [_tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(25);
            make.top.equalTo(self.contentView);
        }];
    }
    return _tagLabel;
}
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        SLTagLayout *layout = [[SLTagLayout alloc]init];
        self.layout = layout;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SLNewPersonTagCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"newPersonTag"];
        [self.contentView addSubview:_collectionView];
    }
    return _collectionView;
}
- (CGFloat)allocTags:(NSMutableArray *)arr{
    CGFloat w = 0;//保存前一个label的宽以及前一个label距离屏幕边缘的距离
    CGFloat h = 30;//用来控制label距离父视图的高
    //    return 100;
    [self.tagXArr removeAllObjects];
    for (int i = 0; i < arr.count; i++) {
        
        NSString *str = [self removeStringSpace:arr[i]];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
        CGSize titlesize = [str boundingRectWithSize:CGSizeMake(screen_W-48, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        CGFloat length = titlesize.width;
        //当label的位置超出屏幕边缘时换行  只是label所在父视图的宽度
        //20 为label以外的
        if( w + length + 20 > screen_W-60){
            w = 0; //换行时将w置为0
            h = h + 30 + 10;//距离父视图也变化
        }
        [self.tagXArr addObject:@(w)];
        w = w + length + 20 +5;
    }
    return h;
}
#pragma mark - 去掉空格换行
- (NSString *)removeStringSpace:(NSString *)str{
    NSString *headerData = str;
    headerData = [headerData stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return headerData;
}
#pragma mark - collectionView代理
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSArray *arr = self.dict[@"secondary_tags"];
    return arr.count;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
    NSString *title = [self removeStringSpace:self.dict[@"secondary_tags"][indexPath.row][@"name"]];
    CGSize titlesize = [title boundingRectWithSize:CGSizeMake(screen_W-60, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
    return CGSizeMake(titlesize.width+20, 30);
}
//每个UICollectionView展s示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SLNewPersonTagCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"newPersonTag" forIndexPath:indexPath];
    cell.x = [self.tagXArr[indexPath.row] doubleValue];
    cell.cTitle = [self removeStringSpace:self.dict[@"secondary_tags"][indexPath.row][@"name"]];
    cell.type = @"second";
    return cell;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}
- (void)setDict:(NSDictionary *)dict{
    _dict = dict;
    if ([dict[@"id"] isEqual:ApplicationDelegate.userId]) {
        self.tagLabel.text = @"我喜欢的";
        self.noMajorLabel.text = @"去选择我喜欢的标签";
        self.enterIv.hidden = NO;
    }else{
        self.tagLabel.text = @"Ta喜欢的";
        self.noMajorLabel.text = @"未选择任何标签";
        self.enterIv.hidden = YES;
    }
    NSMutableArray *primary_arr = [NSMutableArray array];
    for (NSDictionary *d in dict[@"secondary_tags"]) {
        [primary_arr addObject:d[@"name"]];
    }
    if (primary_arr.count == 0) {
        self.noMajorLabel.hidden = NO;
    }else{
        self.noMajorLabel.hidden = YES;
    }
   
    self.collectionView.frame = CGRectMake(25, 30, screen_W-60, [self allocTags:primary_arr]);
    [self.collectionView reloadData];
    self.collectionView.userInteractionEnabled = NO;
//    [self.seperatorView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.equalTo(self);
//        make.left.equalTo(self).offset(25);
//        make.right.equalTo(self).offset(-25);
//        make.height.equalTo(@0.5);
//    }];
    self.enterIv.image = [UIImage imageNamed:@"Down-Arrow-outline"];
}

@end
