//
//  SLTagsTableViewCell.h
//  Superloop
//
//  Created by xiaowu on 16/8/18.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLTagsTableViewCell : UITableViewCell

@property(nonatomic,strong)NSDictionary *dict;

@end
