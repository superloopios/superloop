//
//  SLNewPersonTagCollectionViewCell.m
//  Superloop
//
//  Created by xiaowu on 16/11/22.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLNewPersonTagCollectionViewCell.h"

@interface SLNewPersonTagCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *tagLabel;

@end

@implementation SLNewPersonTagCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 15;
    self.layer.masksToBounds = NO;
}
- (void)setCTitle:(NSString *)cTitle{
    _cTitle = cTitle;
    self.tagLabel.text = cTitle;
}
- (void)setType:(NSString *)type{
    _type = type;
    if ([type isEqualToString:@"primary"]) {
        //        self.backgroundColor = UIColorFromRGB(0xf7e3df);
        self.layer.borderWidth = 0.6;
        self.layer.borderColor = UIColorFromRGB(0xf56b59).CGColor;
        self.tagLabel.textColor = UIColorFromRGB(0xffffff);
        self.backgroundColor = UIColorFromRGB(0xf56b59);
    }else{
        //        self.backgroundColor = SLColor(235, 235, 235);
        self.layer.borderWidth = 0.6;
        self.layer.borderColor = UIColorFromRGB(0xb3907d).CGColor;
        self.tagLabel.textColor = UIColorFromRGB(0x9e7f7b);
        self.backgroundColor = UIColorFromRGB(0xffffff);
    }
}

@end
