//
//  SLBioCell.h
//  Superloop
//
//  Created by xiaowu on 16/8/18.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLBioCell : UITableViewCell

@property(nonatomic,strong)NSDictionary *dict;
@property(nonatomic,copy)void (^seeMore)(NSString *str);

@end
