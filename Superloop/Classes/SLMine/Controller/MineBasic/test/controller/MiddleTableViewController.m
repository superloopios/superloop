//
//  MiddleTableViewController.m
//  微博个人主页
//
//  Created by zenglun on 16/5/4.
//  Copyright © 2016年 chengchengxinxi. All rights reserved.
//

#import "MiddleTableViewController.h"
#import "SLTopicViewCell.h"
#import "AppDelegate.h"
#import "SLTopicCollectionViewCell.h"
#import "MWPhoto.h"
#import "MWPhotoBrowser.h"
#import "SLTopicDetailViewController.h"
#import "HomePageViewController.h"
#import "SLAPIHelper.h"
#import "SLFirstViewController.h"
#import "SLNavgationViewController.h"
#import "SLTopicListTableViewCell.h"

@interface MiddleTableViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, SLTopicViewCellDelegate,UIAlertViewDelegate,UICollectionViewDelegateFlowLayout,SLTopicDetailViewControllerDelegate,MWPhotoBrowserDelegate,SLTopicListTableViewCellDelegate>

@property (nonatomic, strong) NSMutableArray *photos; //图片数组
@property (nonatomic,strong)UIButton *button;  //话题删除按钮
@property (nonatomic,assign)NSIndexPath *indexPath; //话题删除按钮得的索引
@property (nonatomic,strong) SLTopicModel *deleteModel;  //模型
@property (nonatomic,strong) SLTopicModel *selectedModel;//选中的model
@property (nonatomic,assign)NSInteger selectedIndex; //选中的index
@end

@implementation MiddleTableViewController

static NSInteger const cols = 3;
static CGFloat const margin = 5;
static NSString *const indertifers = @"myCollectionview";
#define cellWH  ((ScreenW - 50 - (cols - 1) * margin) / cols)
-(NSMutableArray *)photos{
    if (!_photos) {
        _photos=[NSMutableArray new];
    }
    return _photos;
}
- (void)setSlDataSouresArr:(NSMutableArray *)slDataSouresArr{
    _slDataSouresArr = slDataSouresArr;
    [self.tableView reloadData];
    
    [self resetContentInset];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"MiddleTableViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"MiddleTableViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    [self.tableView registerNib:[UINib nibWithNibName:@"SLTopicViewCell" bundle:nil] forCellReuseIdentifier:@"TableViewCell"];
    [self.tableView registerClass:[SLTopicListTableViewCell class] forCellReuseIdentifier:@"TableViewCell"];
    self.tableView.backgroundColor = SLColor(240, 240, 240);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.slDataSouresArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SLTopicListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewCell" forIndexPath:indexPath];
    SLTopicModel *model=self.slDataSouresArr[indexPath.section];
    if (!ApplicationDelegate.Basic) {
        model.has_thumbsuped=[NSString stringWithFormat:@"0"];
    }
    cell.topicModel = model;
    cell.indexpath = indexPath;
    cell.delegate = self;
    self.indexsPath = indexPath;
    /*
    SLTopicViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    SLTopicModel *model=self.slDataSouresArr[indexPath.section];
    if (!ApplicationDelegate.Basic) {
        model.has_thumbsuped=[NSString stringWithFormat:@"0"];
    }
    cell.Delegate = self;
    cell.indexpath = indexPath;
    self.indexsPath = indexPath;
    [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath andModel:model];
     */
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLTopicModel *model = self.slDataSouresArr[indexPath.section];
    return model.cellHeight;
}

#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    SLIndexPathCollection *cellCollectionView = (SLIndexPathCollection *)collectionView;
    SLTopicModel *model = self.slDataSouresArr[cellCollectionView.indexPath.section];
    if (model.imgs.count == 1||model.imgs.count == 2||model.imgs.count == 4) {
        return model.imgs.count;
    }
    return (model.imgs.count+2)/3*3;
    
    
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    SLIndexPathCollection *cellCollectionView = (SLIndexPathCollection *)collectionView;
    SLTopicModel *model = self.slDataSouresArr[cellCollectionView.indexPath.section];
    if (model.imgs.count == 1) {
        return CGSizeMake(screen_W- 50, cellWH*1.5);
    }else if (model.imgs.count == 2 || model.imgs.count == 4){
        return CGSizeMake((screen_W- 50-5)/2.0, cellWH*1.5);
    }
    return CGSizeMake(cellWH, cellWH);
}
//每个UICollectionView展s示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    SLIndexPathCollection * cellCollectionView  = (SLIndexPathCollection *)collectionView;
    
    SLTopicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];
    SLTopicModel *Model = self.slDataSouresArr[cellCollectionView.indexPath.section];
    if (Model.imgs.count > 0) {
        if (indexPath.row<Model.imgs.count) {
            NSString *imageUrl = [Model.imgs[indexPath.row][@"url"] stringByAppendingString:@"@!topic"];
            [cell setupCellWithImageURl:imageUrl];
            //              [cell setupCellWithPlaceHolderImageURl:Model.imgs[indexPath.row][@"url"]];
            
        }else{
            [cell setupCellWithImageURl:nil];
        }
    }
    return cell;
    //    }
    
}
//每个UICollectionView的点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    SLIndexPathCollection * cellCollectionView  = (SLIndexPathCollection *)collectionView;
    SLTopicCollectionViewCell *cell = (SLTopicCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    self.cellCollectionViewCell = cell;
    SLTopicModel *Model = self.slDataSouresArr[cellCollectionView.indexPath.section];
    self.model = Model;
    self.index = indexPath;
    if (indexPath.row < Model.imgs.count) {
        [self.photos removeAllObjects];
        for (NSDictionary *dict in Model.imgs) {
            [self.photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:dict[@"url"]]]];
        }
        [self slPhotoBrowser:indexPath withPhotos:self.photos];
        
    }else{
        SLTopicViewCell *cell = (SLTopicViewCell *)collectionView.superview.superview;
        NSIndexPath *cellIndexPatch = [self.tableView indexPathForCell:cell];
        [self tableView:self.tableView didSelectRowAtIndexPath:cellIndexPatch];
    }
    //    }
    
}
- (void)slPhotoBrowser:(NSIndexPath *)indexPath withPhotos:(NSMutableArray *)photoes{
    self.photos = photoes;
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    [browser setCurrentPhotoIndex:indexPath.row];
    browser.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:browser animated:YES completion:nil];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (_photos.count>9) {
        return 9;
    }
    return self.photos.count;
}
#pragma 代理
- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    //    if (collectionView == _collectionView) {
    //        return UIEdgeInsetsMake(0, 15, 0, 15);
    //    }
    return UIEdgeInsetsZero;
}


- (void)request_data {
    // 如果需要请求网络数据, 再调用tableView的reloadData之后，调用下面resetContentInset方法可以消除底部多余空白
}

#pragma mark - Private
- (void)resetContentInset {
//    [self.tableView layoutIfNeeded];
   
    CGFloat headerHeight;
//    if (screen_W ==320) {
        headerHeight = ceilf(3/4.0 * ScreenW)+130;
//    } else if(screen_W == 375) {
//        headerHeight = 267.5;
//    } else{
//        headerHeight = 267.5 *1.1;
//    }
    if (self.tableView.contentSize.height < kScreenHeight + 136) {  // 136 = 200
        if (self.slDataSouresArr.count>0) {
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, kScreenHeight+headerHeight-64-self.tableView.contentSize.height, 0);
        }else{
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, kScreenHeight-64-40, 0);
        }
        
    } else {
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 55, 0);
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //每次回来之前取消选中
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SLTopicDetailViewController *topicDetail = [[SLTopicDetailViewController alloc] init];
    SLTopicModel *model = self.slDataSouresArr[indexPath.section];
    topicDetail.model = model;
    self.selectedModel = model;
    topicDetail.delegate = self;
    [self.navigationController pushViewController:topicDetail animated:YES];
}

#pragma mark -
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectPraiseBtnWithModel:(SLTopicModel *)topicModel praiseButtonClicked:(UIButton *)button praiseLabClicked:(UILabel *)praiseLab indexPath:(NSIndexPath *)indexpath{
    if (!ApplicationDelegate.Basic) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
        alert.tag=401;
        [alert show];
        return;
    }
    SLTopicModel *model = self.slDataSouresArr[indexpath.section];
    [self didPraiseBtnClicked:button praiseLab:praiseLab model:model];
}
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectCollectionByMWPhotoBrowser:(MWPhotoBrowser *)browser WithModel:(SLTopicModel *)topicModel{
    [self presentViewController:browser animated:YES completion:nil];
}
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectDelegateBtnWithModel:(SLTopicModel *)topicModel deleteButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath{
    SLTopicModel *model = self.slDataSouresArr[indexpath.section];
    if ([[NSString stringWithFormat:@"%@", model.user[@"id"]] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"真的要删除吗?" delegate:self cancelButtonTitle:@"不了" otherButtonTitles:@"好", nil];
        self.button = button;
        self.indexPath = indexpath;
        self.deleteModel = model;
        alert.tag = 400;
        [alert show];
    }
}
- (void)SLTopicListTableViewCell:(SLTopicListTableViewCell *)topicListTableViewCell didSelectCommentBtnWithModel:(SLTopicModel *)topicModel commentButtonClicked:(UIButton *)button commentLab:(UILabel *)commentLab indexPath:(NSIndexPath *)indexpath{
    //每次回来之前取消选中
    [self.tableView deselectRowAtIndexPath:indexpath animated:YES];
    [self tableView:self.tableView didSelectRowAtIndexPath:indexpath];
}
/*
#pragma SLTopicViewCellDelegate
//点赞的按钮被点击的代理
-(void)didPraiseButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath{
    if (!ApplicationDelegate.Basic) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
        alert.tag=401;
        [alert show];
        return;
    }
    SLTopicModel *model = self.slDataSouresArr[indexpath.section];
    [self didPraiseBtnClicked:button model:model];

}
//评论
-(void)didCommentButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath{
    //每次回来之前取消选中
    [self.tableView deselectRowAtIndexPath:indexpath animated:YES];
    [self tableView:self.tableView didSelectRowAtIndexPath:indexpath];
}
//删除话题
-(void)deleteTopicsButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath{
    SLTopicModel *model = self.slDataSouresArr[indexpath.section];
    if ([[NSString stringWithFormat:@"%@", model.user[@"id"]] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"真的要删除吗?" delegate:self cancelButtonTitle:@"不了" otherButtonTitles:@"好", nil];
        self.button = button;
        self.indexPath = indexpath;
        self.deleteModel = model;
        alert.tag = 400;
        [alert show];
    }
}
*/
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag ==400) {
        if (buttonIndex==1) {
            [self deleteTopic:self.indexPath withMode:self.deleteModel withButton:self.button];
        }
    }else if (alertView.tag ==401) {
        if (buttonIndex==1) {
            [self loginAction];
        }
    }
}
// 登录
-(void)loginAction{
    SLFirstViewController *vc=[[SLFirstViewController alloc] init];
    SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
//    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}
#pragma mark --- 删除话题的点击事件
- (void)deleteTopic:(NSIndexPath *)indexpath withMode:(SLTopicModel *)model withButton:(UIButton *)button{
    
    [SLAPIHelper deleteTopics:model.id success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        button.selected = !button.selected;
        // 删除成功
        [self.slDataSouresArr removeObjectAtIndex:indexpath.section];
        //[self deleteTopicAtIndexPath:indexpath.section];
        [self.tableView reloadData];
        
    } failure:^(SLHttpRequestError *error) {
        if (error.httpStatusCode==404&&error.slAPICode==12) {
            [HUDManager showWarningWithText:@"删除失败，话题已被删除!"];
        }else{
            [HUDManager showWarningWithText:@"删除失败!"];
        }
    }];

}

//-(void)deleteTopicAtIndexPath:(NSInteger)index{
//    
//    NSData *configData = [[NSUserDefaults standardUserDefaults] objectForKey:self.topicKey];
//    if (configData) {
//        NSDictionary *configDict = [NSKeyedUnarchiver unarchiveObjectWithData:configData];
//        
//        NSArray *arr =configDict[@"result"];
//        NSMutableArray *tipcsArr=[[NSMutableArray alloc] init];
//        [tipcsArr addObjectsFromArray:arr];
//        [tipcsArr removeObjectAtIndex:index];
//        
//        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
//        [dict setObject:tipcsArr forKey:@"result"];
//        
//        NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:dict];
//        if (seg.selectedSegmentIndex == 0) {
//            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:[URL_ROOT_PATIENT stringByAppendingString:@"/topics/popular"]];
//        }else if (seg.selectedSegmentIndex==1) {
//            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:[URL_ROOT_PATIENT stringByAppendingString:@"/users/follows/topics"]];
//        }else{
//            [[NSUserDefaults standardUserDefaults] setObject:configData forKey:[URL_ROOT_PATIENT stringByAppendingString:@"/topics"]];
//        }
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
//}

- (void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickPraise:(SLTopicModel *)model{
    self.selectedModel.has_thumbsuped =model.has_thumbsuped;
    self.selectedModel.thumbups_cnt = model.thumbups_cnt;
    [self.tableView reloadData];
}
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didClickComment:(SLTopicModel *)model{
    self.selectedModel.replies_cnt=model.replies_cnt;
    [self.tableView reloadData];
}
-(void)SLTopicDetailViewController:(SLTopicDetailViewController *)topicDetailView didSelectModel:(SLTopicModel *)model{
    [self.slDataSouresArr removeObjectAtIndex:self.selectedIndex];
    NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:self.slDataSouresArr];
    [SLUserDefault setPersonTopic:ApplicationDelegate.userId andData:configData];
    [self.tableView reloadData];
//    [self SLTopicView:self.hotBaseTopicView deleteTopicAtIndexPath:self.selectedIndex];
}

//点赞的按钮被点击的代理
//-(void)didPraiseBtnClicked:(UIButton *)btn model:(SLTopicModel *)model{
-(void)didPraiseBtnClicked:(UIButton *)btn praiseLab:(UILabel *)praiseLab model:(SLTopicModel *)model{

    __block  NSInteger count = [model.thumbups_cnt integerValue];
    NSDictionary *parameter = @{@"id":model.id};
    if ([model.has_thumbsuped isEqualToString:@"0"]) {
        btn.enabled=NO;
        [SLAPIHelper dGood:parameter method:Post success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            btn.selected = YES;
            count ++ ;
//            [btn setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
//            [btn.titleLabel sizeToFit];
            if (count>9999) {
                praiseLab.text = [NSString stringWithFormat:@"%ld万", (long)count/10000];
            }else{
                praiseLab.text = [NSString stringWithFormat:@"%ld", (long)count];
            }
            model.thumbups_cnt=[NSString stringWithFormat:@"%ld",count];
            model.has_thumbsuped=[NSString stringWithFormat:@"%d",1];
            [HUDManager showWarningWithText:@"点赞成功!"];
            btn.enabled=YES;
        } failure:^(SLHttpRequestError *error) {
            btn.enabled=YES;
            if (error.httpStatusCode==404&&error.slAPICode==12) {
                [HUDManager showWarningWithText:@"点赞失败，话题已被删除!"];
            }else if (error.httpStatusCode==409&&error.slAPICode==1) {
                [HUDManager showWarningWithText:@"你已赞过该话题，请勿重复点赞!"];
                btn.selected = YES;
                count ++ ;
//                [btn setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
//                [btn.titleLabel sizeToFit];
                if (count>9999) {
                    praiseLab.text = [NSString stringWithFormat:@"%ld万", (long)count/10000];
                }else{
                    praiseLab.text = [NSString stringWithFormat:@"%ld", (long)count];
                }
                model.thumbups_cnt=[NSString stringWithFormat:@"%ld",count];
                model.has_thumbsuped=[NSString stringWithFormat:@"%d",1];
            }
            else{
                [HUDManager showWarningWithText:@"点赞失败!"];
            }
        }];
    }
    
    if([model.has_thumbsuped isEqualToString:@"1"]){
        //取消单赞
        btn.enabled=NO;
        [SLAPIHelper dGood:parameter method:Delete success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            btn.selected = NO;
            count --;
//            [btn setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
            if (count>9999) {
                praiseLab.text = [NSString stringWithFormat:@"%ld万", (long)count/10000];
            }else{
                praiseLab.text = [NSString stringWithFormat:@"%ld", (long)count];
            }
            model.thumbups_cnt=[NSString stringWithFormat:@"%ld",(long)count];
            model.has_thumbsuped=[NSString stringWithFormat:@"%d",0];
            [HUDManager showWarningWithText:@"点赞取消!"];
            btn.enabled=YES;
        } failure:^(SLHttpRequestError *error) {
            btn.enabled=YES;
            if (error.httpStatusCode==404&&error.slAPICode==12) {
                [HUDManager showWarningWithText:@"点赞取消失败，话题已被删除!"];
            }else if (error.httpStatusCode==409&&error.slAPICode==1) {
                btn.selected = NO;
                [HUDManager showWarningWithText:@"你已取消点赞，请勿重复操作!"];
                count --;
//                [btn setTitle:[NSString stringWithFormat:@"%ld", (long)count] forState:UIControlStateNormal];
                if (count>9999) {
                    praiseLab.text = [NSString stringWithFormat:@"%ld万", (long)count/10000];
                }else{
                    praiseLab.text = [NSString stringWithFormat:@"%ld", (long)count];
                }
                model.thumbups_cnt=[NSString stringWithFormat:@"%ld",(long)count];
                model.has_thumbsuped=[NSString stringWithFormat:@"%d",0];
            }else{
                [HUDManager showWarningWithText:@"点赞取消失败!"];
            }
        }];
    }
}


@end
