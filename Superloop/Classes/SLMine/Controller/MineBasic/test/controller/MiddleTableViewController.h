//
//  MiddleTableViewController.h
//  微博个人主页
//
//  Created by zenglun on 16/5/4.
//  Copyright © 2016年 chengchengxinxi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@class SLTopicCollectionViewCell;
@class SLTopicModel;

@interface MiddleTableViewController : BaseTableViewController

@property (nonatomic,strong) NSMutableArray *slDataSouresArr;
@property (nonatomic,strong) NSIndexPath *indexsPath;
@property (nonatomic,strong)SLTopicCollectionViewCell * cellCollectionViewCell;
@property (nonatomic,assign)NSIndexPath *index;
@property (nonatomic, strong) SLTopicModel *model;
@property (nonatomic, copy)NSString *userId;

@end
