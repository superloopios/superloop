//
//  HomePageViewController.h
//  微博个人主页
//
//  Created by zenglun on 16/5/4.
//  Copyright © 2016年 chengchengxinxi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomePageViewController : UIViewController

@property(nonatomic ,copy)NSString *userId;

@property (nonatomic,assign) BOOL isFromQRCode;
@property (nonatomic,copy) void(^rankingFollowBlock)(BOOL isFollow);//排行榜

@property (nonatomic, copy ) void(^didClickFollowBtn)(UIButton *btn);

@end
