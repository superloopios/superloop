//
//  HomePageViewController.m
//  微博个人主页
//
//  Created by zenglun on 16/5/4.
//  Copyright © 2016年 chengchengxinxi. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>
#import "HomePageViewController.h"
#import "BaseTableViewController.h"
#import "LeftTableViewController.h"
#import "MiddleTableViewController.h"
#import "RightTableViewController.h"
#import "SLFansTableViewController.h"
#import "SLRecordVideoViewController.h"
#import "SLCallingViewController.h"
#import "SLFirstViewController.h"
#import "SLNavgationViewController.h"
#import "SLQRCodeViewController.h"
#import "SLMineBasicInfoViewController.h"

#import "SLActionView.h"
#import "HMSegmentedControl.h"
#import "MWPhotoBrowser.h"
#import "MWPhoto.h"
#import "SLSawBtn.h"
#import "SLHeaderView.h"
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "SLShareView.h"

#import "SLShareModel.h"
#import "SLCallComments.h"
#import "SLTopicModel.h"

#import "SPUtil.h"
#import "SLMessageManger.h"
#import <AliyunOSSiOS/OSSService.h>
#import "SLAPIHelper.h"
#import "NSString+Utils.h"
#import "ColorUtility.h"
#import "AppDelegate.h"
#import "Utils.h"
#import <AVFoundation/AVFoundation.h>

@interface HomePageViewController () <TableViewScrollingProtocol,UIActionSheetDelegate,MWPhotoBrowserDelegate,UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,SLActionViewDelegate> {
    BOOL               _stausBarColorIsBlack;
    UIButton       *   whitebackBtn;
    UIButton       *   whiteShareBtn;
    SLLabel        *   nameLab;
    SLSawBtn       *   followBtn;
    UIButton       *   callBtn;
    UIButton       *   shareBtn;
    UIImageView    *   cutImgView;
    UIButton       *   backBtn;
    UIImageView    *   replaceImageView;
    SLLabel        *   priceLable;
    
    //视频播放
    AVPlayer       *   player;
    AVPlayerLayer  *   playerLayer;
    AVPlayerItem   *   playerItem;
    UIButton       *   playImgBtn;
    BOOL               automaticPlay;
}
@property (nonatomic, strong) NSDictionary              *    userInfo;
@property (nonatomic, weak  ) UIView                    *    navView;
@property (nonatomic, strong) HMSegmentedControl        *    segCtrl;
@property (nonatomic, strong) SLHeaderView              *    headerView;
@property (nonatomic, strong) NSArray                   *    titleList;
@property (nonatomic, weak  ) UIViewController          *    showingVC;
// 存储每个tableview在Y轴上的偏移量
@property (nonatomic, strong) NSMutableDictionary       *    offsetYDict;
@property (nonatomic, strong) NSArray                   *    tableViewArr;
@property (nonatomic, strong) NSArray                   *    childArr;
@property (nonatomic, copy  ) NSString                  *    avatarOrCover;
@property (nonatomic, strong) NSData                    *    data;
@property (nonatomic, strong) UIView                    *    hasNoReciveDatasView;
@property (nonatomic, strong) UIView                    *    hasNoTopicsDatasView;
@property (nonatomic, strong) NSURL                     *    videoURL;
@property (nonatomic, strong) SLActionView              *    actionView;
@property (nonatomic, strong) NSMutableArray            *    photos;
@property (nonatomic, strong) UIAlertView               *    alert;
@property (nonatomic, strong) UIView                    *    popView;
@property (nonatomic, strong) UIButton                  *    followButton;
@property (nonatomic, strong) UIActivityIndicatorView   *    activeView;
@property (nonatomic, assign) BOOL                           isNotFirstLoadReceive;
@property (nonatomic, assign) NSInteger                      receivePage;
@property (nonatomic, assign) NSInteger                      topicPage;
@property (nonatomic, assign) CGFloat                        headerHeight;
@property (nonatomic, assign) BOOL                           isChangeAvatar;
//是否加载过话题
@property (nonatomic, assign) BOOL                           hasloadMeTopic;
//是否加载过话题
@property (nonatomic, assign) BOOL                           hasLoadUserInfo;
//是否正在加载个人信息
@property (nonatomic, assign) BOOL                           hasLoadingUserInfo;
//是否正在加载话题
@property (nonatomic, assign) BOOL                           hasLoadingTopic;
//是否正在加载收到的评价
@property (nonatomic, assign) BOOL                           hasLoadingReceive;
@property (nonatomic, assign) BOOL                           needLoadData;
//是否有话题
@property (nonatomic, assign) BOOL                           hasTopicCache;
//是否有通话评价
@property (nonatomic, assign) BOOL                           hasReceiveCache;
//是否正在加载个人信息
@property (nonatomic, assign) BOOL                           alreadyLoadTopic;
//是否第一次刷新页面,如果不是则不自动播放视频
@property (nonatomic, assign) BOOL                           isFirstLoadInfo;
@end

@implementation HomePageViewController

- (SLActionView *)actionView{
    if (!_actionView) {
        _actionView = [[SLActionView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _actionView.delegate = self;
        [self.view addSubview:_actionView];
    }
    return _actionView;
}

- (UIActivityIndicatorView *)activeView{
    if (!_activeView) {
        _activeView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        
        _activeView.frame = CGRectMake(screen_W-60, 20, 60, 44);;
        
        _activeView.hidesWhenStopped = YES;
        [self.view addSubview:_activeView];
    }
    return _activeView;
}

- (UIAlertView *)alert{
    if (!_alert) {
        _alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
    }
    return _alert;
}

-(NSMutableArray *)photos{
    if (!_photos) {
        _photos=[NSMutableArray new];
    }
    return _photos;
}

- (SLHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[SLHeaderView alloc] init];
        CGFloat height = 0;
        
        height = ceilf(3/4.0 * ScreenW)+130+40;
        
        _headerView.frame = CGRectMake(0, 0, screen_W, height);
        
        _headerView.backImageView.image = [UIImage imageNamed:@"perBackImage"];
        [_headerView.avatarbtn setImage:[UIImage imageNamed:@"newPersonAvatar"] forState:UIControlStateNormal];
        
        __weak typeof(self) weakSelf = self;
        
        
        _headerView.enterFenSiVc = ^(NSString *type){
            
            if ([type isEqualToString:@"fensi"]) {
                SLFansTableViewController *fansVc = [[SLFansTableViewController alloc] init];
                fansVc.userId = weakSelf.userId;
                [weakSelf.navigationController pushViewController:fansVc animated:YES];
            }else{
                SLFansTableViewController *fansVc = [[SLFansTableViewController alloc] init];
                fansVc.userId = weakSelf.userId;
                fansVc.isfollow = @"guanzhu";
                [weakSelf.navigationController pushViewController:fansVc animated:YES];
            }
        };
    }
    return _headerView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    [self hasTopicCacheInfo];
    [self hasReceiveCacheInfo];
    self.isFirstLoadInfo = YES;
    self.receivePage = 0;
    self.headerHeight = ceilf(3/4.0 * ScreenW)+130;
    [self initLoadingInfo];
    self.isNotFirstLoadReceive = YES;
    self.hasLoadUserInfo = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    _titleList = @[@"主页", @"话题", @"评价"];
    [self addNoti];
    [self configNav];
    [self addController];
    [self getCacheInfo];
    [self addHeaderView];
    [self segmentedControlChangedValue:_segCtrl];
    [self addBottomView];
    [self addActivityIndicatorView];
    if (self.isFromQRCode) {
        NSMutableArray *mArr = [[NSMutableArray alloc]initWithArray:self.navigationController.viewControllers];
        for (UIViewController *vc in mArr) {
            if ([vc isKindOfClass:[SLQRCodeViewController class]]) {
                [mArr removeObject:vc];
                break;
            }
        }
        self.navigationController.viewControllers = mArr;
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [MobClick beginLogPageView:@"HomePageViewController"];
    if (ApplicationDelegate.userId) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }else{
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"HomePageViewController"];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if ([NSString isRealString:self.userInfo[@"video_cover"]]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pausePlayer" object:self];
    }
}

- (void)initLoadingInfo{
    self.hasLoadingUserInfo = NO;
    self.hasLoadingReceive = NO;
    self.hasLoadingTopic = NO;
}
- (void)addActivityIndicatorView{
    
}
- (void)getCacheInfo{
    NSData *data= [SLUserDefault getPersonInfo:self.userId];
    NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (dict) {
        self.userInfo = dict;
        [self addTopView];
        LeftTableViewController *vc1 = self.childArr[0];
        
        vc1.myInfo = dict;
        if (!ApplicationDelegate.Basic) {
            followBtn.selected = NO;
        }else{
            if ([self.userInfo[@"following"] isEqual:@0]) {
                followBtn.selected = NO;
            }else{
                followBtn.selected = YES;
            }
        }
        nameLab.text=self.userInfo[@"nickname"];
    }
}
- (void)getCacheTopicInfo{
    NSData *data= [SLUserDefault getPersonTopic:self.userId];
    NSArray *arr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (arr.count>0) {
        self.hasTopicCache = YES;
        MiddleTableViewController *vc1 = self.childArr[1];
        vc1.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:arr];
    }
}
- (void)hasTopicCacheInfo{
    NSData *data= [SLUserDefault getPersonTopic:self.userId];
    NSArray *arr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (arr.count>0) {
        self.hasTopicCache = YES;
    }
}
- (void)getCacheReceiveInfo{
    NSData *data= [SLUserDefault getPersonReceive:self.userId];
    NSArray *arr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (arr.count>0) {
        RightTableViewController *vc1 = self.childArr[2];
        vc1.receiveDataSources = [SLCallComments mj_objectArrayWithKeyValuesArray:arr];
    }
}
- (void)hasReceiveCacheInfo{
    NSData *data= [SLUserDefault getPersonReceive:self.userId];
    NSArray *arr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (arr.count>0) {
        self.hasReceiveCache = YES;
    }
}
- (void)addBottomView{
    NSString *userId = [NSString stringWithFormat:@"%@",self.userId];
    NSString *personId = [NSString stringWithFormat:@"%@",ApplicationDelegate.userId];
    if (![userId isEqualToString:personId]) {
        CGFloat width = 105*picWScale;
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, screen_H-49, screen_W, 49)];
        [self.view addSubview:bottomView];
        bottomView.backgroundColor = [UIColor lightGrayColor];
        UIImageView *bottomcutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 0.5)];
        bottomcutImgView.backgroundColor=UIColorFromRGB(0xebddd5);
        [bottomView addSubview:bottomcutImgView];
        followBtn = [[SLSawBtn alloc] init];
        followBtn.frame = CGRectMake(0, 0.5, width, 48.5);
        [followBtn setTextColor:SLColor(67, 67, 67)];
        followBtn.backgroundColor = UIColorFromRGB(0xfafafa);
        [followBtn setTitle:@"关注Ta" forState:UIControlStateNormal];
        [followBtn setTitle:@"已关注" forState:UIControlStateSelected];
        
        [followBtn setImage:[UIImage imageNamed:@"want"] forState:UIControlStateNormal];
        [followBtn setImage:[UIImage imageNamed:@"selectWant"] forState:UIControlStateSelected];
        [bottomView addSubview:followBtn];
        [followBtn addTarget:self action:@selector(followUser) forControlEvents:UIControlEventTouchUpInside];
        //        followBtn.selected =
        if ([self.userInfo[@"following"] isEqual:@1]) {
            followBtn.selected = YES;
        }else{
            followBtn.selected = NO;
        }
        if (!ApplicationDelegate.userId) {
            followBtn.selected = NO;
        }
        
        
        SLSawBtn *sendMessageBtn = [[SLSawBtn alloc] init];
        sendMessageBtn.frame = CGRectMake(width, 0.5, width, 48.5);
        [sendMessageBtn setTextColor:SLColor(67, 67, 67)];
        sendMessageBtn.backgroundColor = [UIColor whiteColor];
        [sendMessageBtn setTitle:@"发消息" forState:UIControlStateNormal];
        [sendMessageBtn setImage:[UIImage imageNamed:@"xiaoxi"] forState:UIControlStateNormal];
        [bottomView addSubview:sendMessageBtn];
        [sendMessageBtn addTarget:self action:@selector(sendMessage) forControlEvents:UIControlEventTouchUpInside];
        
        
        callBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImageView *imV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Phone-receiver-silhouette"]];
        imV.contentMode = UIViewContentModeCenter;
        imV.frame = CGRectMake(10, 0, 20, 48.5);
        [callBtn addSubview:imV];
        callBtn.frame = CGRectMake(width*2, 0.5, screen_W-2*width, 48.5);
        
        callBtn.backgroundColor = SLMainColor;
        
        
        //富文本样式
        priceLable = [[SLLabel alloc] init];
        priceLable.textColor = [UIColor whiteColor];
        
        
        priceLable.frame = CGRectMake(CGRectGetMaxX(imV.frame)+6, 0, screen_W-2*width-30, 48.5);
        [callBtn addSubview:priceLable];
        NSString *aString;
        NSInteger len = 0;
        NSString *price = [NSString stringWithFormat:@"%@",self.userInfo[@"price"]];
        if ([NSString isEmptyStrings:price]) {
            aString = [NSString stringWithFormat:@"立刻拨打2元/分钟"];
            len = 2;
        }else{
            aString = [NSString stringWithFormat:@"立刻拨打%@元/分钟",self.userInfo[@"price"]];
            len = price.length+1;
        }
        NSInteger size = screen_W == 320 ?12:15;
        if (screen_W == 375) {
            size = 14;
        }
        if (len == 4 && screen_W == 320) {
            size = 11;
        }
        priceLable.font = [UIFont systemFontOfSize:size];
        NSMutableAttributedString * aAttributedString = [[NSMutableAttributedString alloc] initWithString:aString];
        
        CGFloat boldSize = screen_W == 320 ? 14:18;
        if (screen_W == 375) {
            boldSize = 16;
        }
        //        [aAttributedString addAttribute:NSFontAttributeName             //文字字体
        //                                  value:[UIFont systemFontOfSize:size]
        //                                  range:NSMakeRange(0, 4)];
        
        
        [aAttributedString addAttribute:NSFontAttributeName             //文字字体
                                  value:[UIFont boldSystemFontOfSize:boldSize]
                                  range:NSMakeRange(4, len)];
        //        [aAttributedString addAttribute:NSFontAttributeName             //文字字体
        //                                  value:[UIFont systemFontOfSize:size]
        //                                  range:NSMakeRange(6+len, 3)];
        priceLable.attributedText = aAttributedString;
        [bottomView addSubview:callBtn];
        
        [callBtn addTarget:self action:@selector(callUser) forControlEvents:UIControlEventTouchUpInside];
    }
}
- (void)callUser{
    
    if (!ApplicationDelegate.Basic) {
        //判断是否是登录成功的状态
        [self.alert show];
        return;
    }
    if ([[self.userInfo[@"dnd_status"] stringValue] isEqualToString:@"1"]) {
        kShowToast(@"对方已经屏蔽所有来电");
        return;
    }
    if ([[self.userInfo[@"blockedMe"] stringValue] isEqualToString:@"1"]) {
        kShowToast(@"操作失败\n对方已将你屏蔽");
        return;
    }
    NSString *uid = [NSString stringWithFormat:@"%@",self.userId];
    [self loadPriceData:uid];
}
// 获取用户信息
- (void)loadPriceData:(NSString *)uid{
    if (!uid) {
        return;
    }
    callBtn.enabled = NO;
    [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow
                          withText:@""];
    [SLAPIHelper getPriceData:uid success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        [HUDManager hideHUDView];
        // NSLog(@"%@", data[@"result"]);
        SLCallingViewController *callView = [[SLCallingViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:callView];
        callView.user = data[@"result"];
        callBtn.enabled = YES;
        [self.navigationController presentViewController:nav animated:YES completion:nil];
        
    } failure:^(SLHttpRequestError *failure) {
        [HUDManager hideHUDView];
        callBtn.enabled = YES;
        if (failure.slAPICode==-1) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [HUDManager showWarningWithText:@"请检查网络状况"];
            });
        }
    }];
}
- (void)followUser{
    if (!ApplicationDelegate.Basic) {
        //判断是否是登录成功的状态
        [self.alert show];
        return;
    }
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSString *para = [NSString stringWithFormat:@"%@",self.userId];
    [parameter setObject:para forKey:@"user_id"];
    
    followBtn.enabled = NO;
    
    if (!followBtn.selected) {
        [SLAPIHelper getfollow:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            followBtn.enabled = YES;
            followBtn.selected = !followBtn.selected;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"isUnfollow" object:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeUserData" object:self];
            if (self.rankingFollowBlock) {
                self.rankingFollowBlock(YES);
            }
            [HUDManager showWarningWithText:@"关注成功"];
            if (self.didClickFollowBtn) {
                self.didClickFollowBtn(followBtn);
            }
        } failure:^(SLHttpRequestError *error) {
            if (error.slAPICode == 35 || error.slAPICode == 34) {
                
            }else{
                [HUDManager showWarningWithText:@"关注失败"];
            }
            followBtn.enabled = YES;
            
        }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"确认要取消关注吗？" message:nil delegate:self cancelButtonTitle:@"不了" otherButtonTitles:@"好", nil];
        alert.tag = 100;
        [alert show];
        
    }
}

- (void)sendMessage{
    if (!ApplicationDelegate.Basic) {
        //判断是否是登录成功的状态
        [self.alert show];
        return;
    }
    NSString *otherUserID = [NSString stringWithFormat:@"%@",self.userId];
    YWPerson *person = [[YWPerson alloc] initWithPersonId:otherUserID.description];
    [self.navigationController setNavigationBarHidden:YES];
    ApplicationDelegate.imTitlename = self.userInfo[@"nickname"];
    [[SLMessageManger sharedInstance] openConversationViewControllerWithPerson:person fromNavigationController:self.navigationController];
}

#pragma mark - BaseTabelView Delegate
- (void)tableViewScroll:(UITableView *)tableView offsetY:(CGFloat)offsetY{
    if (offsetY<0) {
        replaceImageView.hidden = NO;
        whiteShareBtn.hidden = YES;
        shareBtn.hidden = YES;
    }else{
        replaceImageView.hidden = YES;
        whiteShareBtn.hidden = NO;
        shareBtn.hidden = NO;
    }
    replaceImageView.layer.transform = CATransform3DMakeRotation(offsetY/15*M_PI/6, 0, 0, 1);
    if (offsetY > self.headerHeight - topBarHeight) {
        if (![_headerView.superview isEqual:self.view]) {
            [self.view insertSubview:_headerView belowSubview:_navView];
        }
        CGRect rect = self.headerView.frame;
        rect.origin.y = topBarHeight - self.headerHeight;
        self.headerView.frame = rect;
    } else {
        if (![_headerView.superview isEqual:tableView]) {
            for (UIView *view in tableView.subviews) {
                if ([view isKindOfClass:[UIImageView class]]) {
                    [tableView insertSubview:_headerView belowSubview:view];
                    break;
                }
            }
        }
        CGRect rect = self.headerView.frame;
        rect.origin.y = 0;
        self.headerView.frame = rect;
    }
    
    
    if (offsetY>0) {
        if (offsetY < 64) {
            offsetY = 64;
        }
        CGFloat alpha = (offsetY - 64) / 100;
        //CGFloat alpha = offsetY/128;
        if (alpha >= 0.8) {
            //            whitebackBtn.hidden = YES;
            //            whiteShareBtn.hidden = YES;
            [whitebackBtn setImage:[UIImage imageNamed:@"blackBack"] forState:UIControlStateNormal];
            if ([self.userId isEqual: ApplicationDelegate.userId]) {
                [whiteShareBtn setImage:[UIImage imageNamed:@"blackThirdPoint"] forState:UIControlStateNormal];
            }else{
                [whiteShareBtn setImage:[UIImage imageNamed:@"blackThirdPoint"] forState:UIControlStateNormal];
            }
        }else{
            //            whitebackBtn.hidden = NO;
            //            whiteShareBtn.hidden = NO;
            [whitebackBtn setImage:[UIImage imageNamed:@"whileBack"] forState:UIControlStateNormal];
            if ([self.userId isEqual: ApplicationDelegate.userId]) {
                [whiteShareBtn setImage:[UIImage imageNamed:@"whileThirdPoint"] forState:UIControlStateNormal];
            }else{
                [whiteShareBtn setImage:[UIImage imageNamed:@"whileThirdPoint"] forState:UIControlStateNormal];
            }
        }
        if (alpha>=1.0) {
            alpha=1.0;
        }
        self.navView.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:alpha];
        cutImgView.backgroundColor = [UIColor colorWithRed:148/255.0 green:148/255.0 blue:148/255.0 alpha:alpha];
        nameLab.textColor = [UIColor colorWithWhite:0 alpha:alpha];
    }
}

- (void)tableViewDidEndDragging:(UITableView *)tableView offsetY:(CGFloat)offsetY {
    //    _headerView.canNotResponseTapTouchEvent = NO;  这四行被屏蔽内容，每行下面一行的效果一样
    _headerView.userInteractionEnabled = YES;
    
    NSString *addressStr = [NSString stringWithFormat:@"%p", _showingVC];
    if (offsetY > self.headerHeight - topBarHeight) {
        [self.offsetYDict enumerateKeysAndObjectsUsingBlock:^(NSString  *key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if ([key isEqualToString:addressStr]) {
                _offsetYDict[key] = @(offsetY);
            } else if ([_offsetYDict[key] floatValue] <= self.headerHeight - topBarHeight) {
                _offsetYDict[key] = @(self.headerHeight - topBarHeight);
            }
        }];
    } else {
        if (offsetY <= self.headerHeight - topBarHeight) {
            
            [self.offsetYDict enumerateKeysAndObjectsUsingBlock:^(NSString  *key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                _offsetYDict[key] = @(offsetY);
            }];
        }
    }
}

- (void)tableViewDidEndDecelerating:(UITableView *)tableView offsetY:(CGFloat)offsetY {
    //    _headerView.canNotResponseTapTouchEvent = NO; 这四行被屏蔽内容，每行下面一行的效果一样
    _headerView.userInteractionEnabled = YES;
    replaceImageView.hidden = YES;
    
    if (self.needLoadData) {
        [self getData];
        whiteShareBtn.hidden = YES;
        shareBtn.hidden = YES;
    }
    NSString *addressStr = [NSString stringWithFormat:@"%p", _showingVC];
    if (offsetY > self.headerHeight - topBarHeight) {
        [self.offsetYDict enumerateKeysAndObjectsUsingBlock:^(NSString  *key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if ([key isEqualToString:addressStr]) {
                _offsetYDict[key] = @(offsetY);
            } else if ([_offsetYDict[key] floatValue] <= self.headerHeight - topBarHeight) {
                _offsetYDict[key] = @(self.headerHeight - topBarHeight);
            }
        }];
    } else {
        if (offsetY <= self.headerHeight - topBarHeight) {
            [self.offsetYDict enumerateKeysAndObjectsUsingBlock:^(NSString  *key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                _offsetYDict[key] = @(offsetY);
            }];
        }
    }
}

- (void)tableViewWillBeginDecelerating:(UITableView *)tableView offsetY:(CGFloat)offsetY {
    //    _headerView.canNotResponseTapTouchEvent = YES; 这四行被屏蔽内容，每行下面一行的效果一样
    _headerView.userInteractionEnabled = NO;
    if (offsetY<=-60) {
        self.needLoadData = YES;
    }else{
        self.needLoadData = NO;
    }
    
}

- (void)tableViewWillBeginDragging:(UITableView *)tableView offsetY:(CGFloat)offsetY {
    //    _headerView.canNotResponseTapTouchEvent = YES; 这四行被屏蔽内容，每行下面一行的效果一样
    _headerView.userInteractionEnabled = NO;
}

#pragma mark - Private
- (void)configNav {
    //self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    [self.view addSubview:navView];
    
    //    backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    //    backBtn.frame=CGRectMake(0, 20, 60, 44);
    //    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    //    [backBtn setImage:[UIImage imageNamed:@"blackBack"] forState:UIControlStateNormal];
    //    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    //    [navView addSubview:backBtn];
    
    //    shareBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    //    shareBtn.frame=CGRectMake(screen_W-60, 20, 60, 44);
    //    [navView addSubview:shareBtn];
    
    nameLab=[[SLLabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor = [UIColor colorWithWhite:0 alpha:0];
    [navView addSubview:nameLab];
    
    cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    //cutImgView.backgroundColor=[UIColor colorWithRed:0.667 green:0.667 blue:0.667 alpha:0];
    [navView addSubview:cutImgView];
    
    whitebackBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    whitebackBtn.frame=CGRectMake(0, 20, 60, 44);
    whitebackBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [whitebackBtn setImage:[UIImage imageNamed:@"whileBack"] forState:UIControlStateNormal];
    [whitebackBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    //    [self.view addSubview:whitebackBtn];
    [navView addSubview:whitebackBtn];
    
    whiteShareBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    whiteShareBtn.frame=CGRectMake(screen_W-60, 20, 60, 44);
    if ([self.userId isEqual: ApplicationDelegate.userId]) {
        [whiteShareBtn setImage:[UIImage imageNamed:@"whileThirdPoint"] forState:UIControlStateNormal];
        [whiteShareBtn addTarget:self action:@selector(openPopView) forControlEvents:UIControlEventTouchUpInside];
        //        [shareBtn setImage:[UIImage imageNamed:@"Share"] forState:UIControlStateNormal];
        //        [shareBtn addTarget:self action:@selector(shareClick) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        [whiteShareBtn setImage:[UIImage imageNamed:@"whileThirdPoint"] forState:UIControlStateNormal];
        [whiteShareBtn addTarget:self action:@selector(openPopView) forControlEvents:UIControlEventTouchUpInside];
        //        [shareBtn setImage:[UIImage imageNamed:@"blackThirdPoint"] forState:UIControlStateNormal];
        //        [shareBtn addTarget:self action:@selector(shareBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    [navView addSubview:whiteShareBtn];
    //    [self.view addSubview:whiteShareBtn];
    
    replaceImageView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"loading"]];
    replaceImageView.frame=CGRectMake(screen_W-60, 20, 60, 44);
    replaceImageView.contentMode = UIViewContentModeCenter;
    [self.view addSubview:replaceImageView];
    replaceImageView.hidden = YES;
    
    _navView = navView;
}

- (void)addController {
    LeftTableViewController *vc1 = [[LeftTableViewController alloc] init];
    vc1.delegate = self;
    vc1.NoNetWork = NO;
    //    vc1.myloadView.hidden = NO;
    
    MiddleTableViewController *vc2 = [[MiddleTableViewController alloc] init];
    vc2.tableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreData)];
    vc2.delegate = self;
    RightTableViewController *vc3 = [[RightTableViewController alloc] init];
    
    vc3.tableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreData)];
    vc3.delegate = self;
    vc3.userId = [NSString stringWithFormat:@"%@",self.userId];
    self.tableViewArr = @[vc1.tableView,vc2.tableView,vc3.tableView];
    [self addChildViewController:vc1];
    [self addChildViewController:vc2];
    [self addChildViewController:vc3];
    self.childArr = @[vc1,vc2,vc3];
    vc1.userId = self.userId;
    vc2.userId = self.userId;
    vc3.userId = self.userId;
    [vc2.tableView.mj_footer setHidden:YES];
    [vc3.tableView.mj_footer setHidden:YES];
}


- (void)addHeaderView {
    
    self.headerView.frame = CGRectMake(0, 0, kScreenWidth, self.headerHeight+switchBarHeight);
    self.headerView.segCtrl.frame = CGRectMake(0, self.headerView.height-40, screen_W, 40);
    self.segCtrl = self.headerView.segCtrl;
    
    _segCtrl.sectionTitles = _titleList;
    _segCtrl.backgroundColor = [UIColor whiteColor];
    _segCtrl.selectionIndicatorHeight = 2.0f;
    _segCtrl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    _segCtrl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
    _segCtrl.titleTextAttributes = @{NSForegroundColorAttributeName : SLBlackTitleColor, NSFontAttributeName : [UIFont boldSystemFontOfSize:15]};
    _segCtrl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : SLMainColor, NSFontAttributeName : [UIFont boldSystemFontOfSize:15]};
    _segCtrl.selectionIndicatorColor = SLMainColor;
    _segCtrl.selectedSegmentIndex = 0;
    _segCtrl.borderType = HMSegmentedControlBorderTypeBottom;
    _segCtrl.borderColor = [UIColor lightGrayColor];
    _segCtrl.borderWidth = 0.5;
    
    [_segCtrl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
}


- (void)segmentedControlChangedValue:(HMSegmentedControl*)sender {
    [_showingVC.view removeFromSuperview];
    BaseTableViewController *newVC = self.childViewControllers[sender.selectedSegmentIndex];
    if (!newVC.view.superview) {
        [self.view addSubview:newVC.view];
        newVC.view.frame = self.view.bounds;
    }
    
    NSString *nextAddressStr = [NSString stringWithFormat:@"%p", newVC];
    CGFloat offsetY = [_offsetYDict[nextAddressStr] floatValue];
    newVC.tableView.contentOffset = CGPointMake(0, offsetY);
    
    [self.view insertSubview:newVC.view belowSubview:self.navView];
    
    if (offsetY <= self.headerHeight - topBarHeight) {
        [newVC.view addSubview:self.headerView];
        for (UIView *view in newVC.view.subviews) {
            if ([view isKindOfClass:[UIImageView class]]) {
                [newVC.view insertSubview:self.headerView belowSubview:view];
                break;
            }
        }
        CGRect rect = self.headerView.frame;
        rect.origin.y = 0;
        self.headerView.frame = rect;
    }  else {
        [self.view insertSubview:self.headerView belowSubview:_navView];
        CGRect rect = self.headerView.frame;
        rect.origin.y = topBarHeight - self.headerHeight;
        self.headerView.frame = rect;
    }
    _showingVC = newVC;
    if (sender.selectedSegmentIndex == 1) {
        
        if (!self.hasloadMeTopic) {
            [self getCacheTopicInfo];
            //            UITableView *tableview = self.tableViewArr[1];
            //            [tableview.mj_header beginRefreshing];
            [self getData];
        }
    }else if (sender.selectedSegmentIndex == 2){
        if (self.isNotFirstLoadReceive) {
            [self getCacheReceiveInfo];
            //            UITableView *tableview = self.tableViewArr[2];
            //            [tableview.mj_header beginRefreshing];
            [self getData];
        }
        
    }else if (sender.selectedSegmentIndex == 0){
        if (!self.hasLoadUserInfo) {
            //            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self getData];
            //            });
            
        }
        
    }
}

#pragma mark - Getter/Setter
- (NSMutableDictionary *)offsetYDict {
    if (!_offsetYDict) {
        _offsetYDict = [NSMutableDictionary dictionary];
        for (BaseTableViewController *vc in self.childViewControllers) {
            NSString *addressStr = [NSString stringWithFormat:@"%p", vc];
            _offsetYDict[addressStr] = @(CGFLOAT_MIN);
        }
    }
    return _offsetYDict;
}
- (void)backBtnClick{
    if (ApplicationDelegate.userId) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        SLFirstViewController *vc = [[SLFirstViewController alloc] init];
        SLNavgationViewController *nav = [[SLNavgationViewController alloc] initWithRootViewController:vc];
        [UIApplication sharedApplication].keyWindow.rootViewController = nav;
    }
    
}

- (void)getData{
    whiteShareBtn.hidden = YES;
    shareBtn.hidden = YES;
    self.needLoadData = NO;
    [self.activeView startAnimating];
    if (self.segCtrl.selectedSegmentIndex == 2) {
        if (!self.hasLoadingReceive) {
            RightTableViewController *vc1 = self.childArr[2];
            
            if (vc1.receiveDataSources.count == 0) {
                vc1.myloadView.hidden = NO;
            }
            NSDictionary *paraRecieve = @{@"type":@1, @"userId":self.userId,@"page":@"0",@"pager":@"20"};
            [SLAPIHelper getUsersAppraisals:paraRecieve success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                vc1.myloadView.hidden = YES;
                self.hasLoadingReceive = NO;
                [self.activeView stopAnimating];
                whiteShareBtn.hidden = NO;
                shareBtn.hidden = NO;
                
                NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data[@"result"][@"results"]];
                [SLUserDefault setPersonReceive:self.userId andData:configData];
                UITableView *tableView = self.tableViewArr[2];
                
                self.isNotFirstLoadReceive = NO;
                NSArray *meCommentArr = [SLCallComments mj_objectArrayWithKeyValuesArray:data[@"result"][@"results"]];
                RightTableViewController *vc1 = self.childArr[2];
                vc1.receiveDataSources = [NSMutableArray arrayWithArray:meCommentArr];
                
                if (meCommentArr.count == 0) {
                    [vc1 addNetWorkViews:@"暂无通话评价"];
                }else{
                    self.hasReceiveCache = YES;
                    [vc1.noNetworkOrDefaultView removeFromSuperview];
                }
                self.receivePage = 1;
                if (meCommentArr.count<20) {
                    [tableView.mj_footer setHidden:YES];
                }else{
                    [tableView.mj_footer setHidden:NO];
                    [tableView.mj_footer resetNoMoreData];
                }
                
            } failure:^(SLHttpRequestError *error) {
                [self.activeView stopAnimating];
                self.hasLoadingReceive = NO;
                vc1.myloadView.hidden = YES;
                whiteShareBtn.hidden = NO;
                shareBtn.hidden = NO;
            }];
        }
        
    }else if (self.segCtrl.selectedSegmentIndex == 1) {
        if (!self.hasLoadingTopic) {
            self.alreadyLoadTopic = YES;
            self.topicPage=0;
            self.hasloadMeTopic = YES;
            self.hasLoadingTopic = NO;
            
            NSString *user = self.userId;
            MiddleTableViewController *vc1 = self.childArr[1];
            if (vc1.slDataSouresArr.count == 0) {
                vc1.myloadView.hidden = NO;
            }
            
            NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@/topics",user]];
            NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.topicPage],@"pager":@"20"};
            [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                [self.activeView stopAnimating];
                whiteShareBtn.hidden = NO;
                shareBtn.hidden = NO;
                vc1.myloadView.hidden = YES;
                self.topicPage=1;
                UITableView *tableView = self.tableViewArr[1];
                
                MiddleTableViewController *vc1 = self.childArr[1];
                vc1.slDataSouresArr = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
                
                if (vc1.slDataSouresArr.count == 0) {
                    [vc1 addNetWorkViews:@"这里什么也没有！"];
                }else{
                    [vc1.noNetworkOrDefaultView removeFromSuperview];
                    self.hasTopicCache = YES;
                }
                if (vc1.slDataSouresArr.count<20) {
                    
                    [tableView.mj_footer setHidden:YES];
                }else{
                    [tableView.mj_footer setHidden:NO];
                    
                    [tableView.mj_footer resetNoMoreData];
                }
                NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data[@"result"]];
                [SLUserDefault setPersonTopic:self.userId andData:configData];
                
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            }  failure:^(SLHttpRequestError *error) {
                // 结束刷新
                vc1.myloadView.hidden = YES;
                [self.activeView stopAnimating];
                whiteShareBtn.hidden = NO;
                shareBtn.hidden = NO;
                self.hasLoadingTopic = NO;
                
            }];
        }
        
        
    }else  if (self.segCtrl.selectedSegmentIndex == 0) {
        if (!self.hasLoadingUserInfo) {
            LeftTableViewController *vc1 = self.childArr[0];
            if (!vc1.myInfo) {
                vc1.myloadView.hidden = YES;
            }
            NSLog(@"self.userId%@",self.userId);
            [SLAPIHelper getUsersData:@{@"id":self.userId} success:^(NSURLSessionDataTask *task, NSDictionary *data) {
                [self.activeView stopAnimating];
                whiteShareBtn.hidden = NO;
                shareBtn.hidden = NO;
                vc1.myloadView.hidden = YES;
                self.hasLoadingUserInfo = NO;
                self.hasLoadUserInfo = YES;
                
                self.userInfo = data[@"result"];
                [self addTopView];
                vc1.myInfo = data[@"result"];
                if (self.isFirstLoadInfo) {
                    [self getNetWorkStatus];
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
                    self.isFirstLoadInfo = NO;
                }
                
                
                if (!ApplicationDelegate.Basic) {
                    followBtn.selected = NO;
                }else{
                    if ([self.userInfo[@"following"] isEqual:@0]) {
                        followBtn.selected = NO;
                    }else{
                        followBtn.selected = YES;
                    }
                }
                
                if ([self.userId isEqual:ApplicationDelegate.userId]) {
                    NSDictionary *dict = [SLUserDefault getUserInfo];
                    NSString *basic = dict[@"basic"];
                    NSString *Password = dict[@"Password"];
                    NSDictionary *newUserInfo = @{@"basic": basic, @"userInformation": data[@"result"],@"Password": Password};
                    [SLUserDefault setUserInfo:newUserInfo];
                }
                
                NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data[@"result"]];
                [SLUserDefault setPersonInfo:self.userId andData:configData];
                
                //            followBtn.selected =
                //            if (self.isFirstLoad) {
                //                [self addMajorTableView];
                //            }else{
                //                [self updateHeaderFrame];
                //            }
                //            [self.collectionView reloadData];
                //            [self.secondCollectionView reloadData];
                //            [self addPersonInfo];
                
                nameLab.text=self.userInfo[@"nickname"];
                
                
                
            } failure:^(SLHttpRequestError *failure) {
                [self.activeView stopAnimating];
                whiteShareBtn.hidden = NO;
                shareBtn.hidden = NO;
                self.hasLoadingUserInfo = NO;
                vc1.myloadView.hidden = YES;
            }];
        }
        
    }
}
- (void)getMoreData{
    if (self.segCtrl.selectedSegmentIndex == 2) {
        NSString *page = [NSString stringWithFormat:@"%ld",self.receivePage];
        NSDictionary *paraRecieve = @{@"type":@1, @"userId":self.userId,@"page":page,@"pager":@"20"};
        [SLAPIHelper getUsersAppraisals:paraRecieve success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            
            UITableView *tableView = self.tableViewArr[2];
            
            NSArray *meCommentArr = [SLCallComments mj_objectArrayWithKeyValuesArray:data[@"result"][@"results"]];
            RightTableViewController *vc1 = self.childArr[2];
            [vc1.receiveDataSources addObjectsFromArray:meCommentArr];
            
            if ( meCommentArr.count <20) {
                [tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [tableView.mj_footer endRefreshing];
            }
        } failure:^(SLHttpRequestError *error) {
            UITableView *tableView = self.tableViewArr[2];
            [tableView.mj_footer endRefreshing];
        }];
    }else if (self.segCtrl.selectedSegmentIndex == 1) {
        
        NSString *user = self.userId;
        
        NSString *topicUrl = [URL_ROOT_PATIENT stringByAppendingString:[NSString stringWithFormat:@"/users/%@/topics",user]];
        NSDictionary *parameters = @{@"page":[NSString stringWithFormat:@"%ld",(long)self.topicPage],@"pager":@"20"};
        [SLAPIHelper getTopicsData:parameters url:topicUrl success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            
            self.topicPage++;
            UITableView *tableView = self.tableViewArr[1];
            
            MiddleTableViewController *vc1 = self.childArr[1];
            NSArray *arr = [SLTopicModel mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [vc1.slDataSouresArr addObjectsFromArray:arr];
            [tableView reloadData];
            
            if (arr.count<20) {
                [tableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [tableView.mj_footer endRefreshing];
            }
            
            
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        }  failure:^(SLHttpRequestError *error) {
            // 结束刷新
            
            UITableView *tableView = self.tableViewArr[1];
            [tableView.mj_footer endRefreshing];
            
        }];
    }
}
- (void)addTopView{
    self.headerView.userInfo = self.userInfo;
    //    NSLog(@"%@",self.userInfo);
    __weak typeof(self)weakSelf = self;
    NSString *aString;
    NSInteger len = 0;
    NSString *price = [NSString stringWithFormat:@"%@",self.userInfo[@"price"]];
    if ([NSString isEmptyStrings:price]) {
        aString = [NSString stringWithFormat:@"立刻拨打2元/分钟"];
        len = 2;
    }else{
        aString = [NSString stringWithFormat:@"立刻拨打%@元/分钟",self.userInfo[@"price"]];
        len = price.length+1;
    }
    
    NSMutableAttributedString * aAttributedString = [[NSMutableAttributedString alloc] initWithString:aString];
    //    NSInteger size = screen_W == 320 ?13:15;
    CGFloat boldSize = screen_W == 320 ? 14:18;
    if (screen_W == 375) {
        boldSize = 16;
    }
    //    [aAttributedString addAttribute:NSFontAttributeName             //文字字体
    //                              value:[UIFont systemFontOfSize:size]
    //                              range:NSMakeRange(0, 4)];
    
    
    [aAttributedString addAttribute:NSFontAttributeName             //文字字体
                              value:[UIFont boldSystemFontOfSize:boldSize]
                              range:NSMakeRange(4, len)];
    //    [aAttributedString addAttribute:NSFontAttributeName             //文字字体
    //                              value:[UIFont systemFontOfSize:size]
    //                              range:NSMakeRange(6+len, 3)];
    priceLable.attributedText = aAttributedString;
    //    [callBtn setTitle:price forState:UIControlStateNormal];
    
    if ([self.userId isEqual:ApplicationDelegate.userId]) {
        self.headerView.changeHeaderBackImage = ^(NSString *type){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            if ([type isEqualToString:@"cover"]) {
                
                strongSelf.actionView.titleArr = @[@"取消",@"从相册选择",@"拍照",@"小视频"];
                strongSelf.actionView.tag = 1;
                [strongSelf.view addSubview:strongSelf.actionView];
                
                //                UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"更改封面" delegate:strongSelf cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册选择",@"录视频",nil];
                //                sheet.tag = 1;
                //                [sheet showInView:weakSelf.view];
            }else{
                strongSelf.actionView.tag = 2;
                strongSelf.actionView.titleArr = @[@"取消",@"从相册选择",@"拍照"];
                [strongSelf.view addSubview:strongSelf.actionView];
                //                UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"更改头像" delegate:strongSelf cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册选择", nil];
                //                sheet.tag = 2;
                //                [sheet showInView:weakSelf.view];
            }
        };
    }else{
        self.headerView.changeHeaderBackImage = ^(NSString *type){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            if ([type isEqualToString:@"cover"]) {
                
            }else{
                [strongSelf.photos removeAllObjects];
                NSString *avatar = weakSelf.userInfo[@"avatar"];
                NSArray *array = [avatar componentsSeparatedByString:@"@"];
                avatar = array[0];
                [weakSelf.photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:avatar]]];
                MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:strongSelf];
                browser.isFromOtherVc = YES;
                [browser setCurrentPhotoIndex:0];
                browser.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                [strongSelf presentViewController:browser animated:YES completion:nil];
            }
        };
    }
    
}
- (void)actionSheetButtonClickedAtIndex:(NSInteger)index{
    if (self.actionView.tag == 1) {
        self.isChangeAvatar=NO;
        if (index == 0) {
            
        }else if (index == 1) {
            
            self.avatarOrCover = @"cover";
            [self chooseImgae:index];
        }else if (index == 2 ){
            self.avatarOrCover = @"cover";
            [self chooseImgae:index];
            
        }else if (index == 3){
            [self chooseVideo:index];
        }
    }else if(self.actionView.tag == 2){
        self.isChangeAvatar=YES;
        if (index == 2 | index == 1) {
            self.avatarOrCover = @"avatar";
            [self chooseImgae:index];
        }
    }
    [self.actionView dismiss];
}

//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
//
//    if (actionSheet.tag == 1) {
//        self.isChangeAvatar=NO;
//        if (buttonIndex == 0 | buttonIndex == 1) {
//            [self chooseImgae:buttonIndex];
//            self.avatarOrCover = @"cover";
//        }else if (buttonIndex == 2 | buttonIndex == 3){
//            [self chooseVideo:buttonIndex];
//        }
//    }else if(actionSheet.tag == 2){
//        self.isChangeAvatar=YES;
//        if (buttonIndex == 0 | buttonIndex == 1) {
//            self.avatarOrCover = @"avatar";
//            [self chooseImgae:buttonIndex];
//        }
//    }
//}
- (void)chooseVideo:(NSInteger)buttonIndex{
    if (buttonIndex == 3) {
        SLRecordVideoViewController *vc = [[SLRecordVideoViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:nav animated:YES completion:nil];
    }
}


- (void)chooseImgae:(NSInteger)buttonIndex{
    NSUInteger sourceType = 0;
    switch (buttonIndex) {
        case 2:
            sourceType = UIImagePickerControllerSourceTypeCamera;
            break;
        case 1:
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            break;
    }
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    
    imagePickerController.allowsEditing = YES;
    
    imagePickerController.sourceType = sourceType;
    
    [self presentViewController:imagePickerController animated:YES completion:^{}];
    
}
//验证图片是否包含二维码
- (BOOL)validateImgByQRcode:(UIImage *)image{
    //返回的UIImage
    CIImage *ciImage = [CIImage imageWithCGImage:[image CGImage]];
    CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:nil];
    NSArray *arr = [detector featuresInImage:ciImage];
    if (arr.count > 0){
        return YES;
    }else{
        return NO;
    }
}
#pragma mark - image picker delegte
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [picker dismissViewControllerAnimated:YES completion:^{}];
    
#warning  注意这里面应该是编辑过之后的图片, 并非是未编辑选择的原图片
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    //验证图片是否包含二维码
    if ([self validateImgByQRcode:image]) {
        [HUDManager showWarningWithText:@"不能上传包含二维码的图片"];
    }else{
        // 保存图片至本地，方法见下文
        [self saveImage:image withName:@"currentImage.png"];
        //上传图片
        dispatch_async(dispatch_get_main_queue(), ^{
            UIWindow *win = [[UIApplication sharedApplication].windows objectAtIndex:0];
            
            [HUDManager showLoadingHUDView:win withText:@"正在上传中"];
        });
#ifdef DEBUG
        NSString *endpoint = @"http://dev.oss.superloop.com.cn";
#else
        NSString *endpoint = @"http://oss.superloop.com.cn";
#endif
        id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:@"UPhdoMR1eN1hnrdA" secretKey:@"ETuIdMVCT1mtiqs8f5KZ8dZnZLiXfG"];
        OSSClient *client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential];
        NSString *uuidStr1 = [[NSUUID UUID] UUIDString];
        [self saveImage:image withName:uuidStr1];
        OSSPutObjectRequest *put = [OSSPutObjectRequest new];
#ifdef DEBUG
        put.bucketName = @"superloop-dev";
#else
        put.bucketName = @"superloop";
#endif
        NSString *uuid= [[NSUUID UUID] UUIDString];
        //减去"-"
        NSString *str = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
        //变小写
        NSString *lower = [str lowercaseString];
        put.objectKey = [NSString stringWithFormat:@"%@/superloop_%@.jpg",ApplicationDelegate.userId,lower];
        put.uploadingData = _data;
        put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
        };
        OSSTask * putTask = [client putObject:put];
        [putTask continueWithBlock:^id(OSSTask *task) {
            if (!task.error) {
                NSDictionary *parameters;
                if ([self.avatarOrCover isEqualToString:@"cover"]) {
                    parameters = @{@"video_cover":@"",self.avatarOrCover:put.objectKey};
                }else{
                    parameters = @{self.avatarOrCover:put.objectKey};
                }
                
                [SLAPIHelper introduce:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [HUDManager hideHUDView];
                        [HUDManager showWarningWithText:@"修改成功"];
                        [self reloadUserDataNotChangeAvatar];
                        if (self.isChangeAvatar) {
                            [self setLoginIM];
                            [self.headerView.avatarbtn setImage:[UIImage imageWithData:_data] forState:UIControlStateNormal];
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"changeAvatar" object:nil];
                            
                        }else{
                            self.headerView.backImageView.image = [UIImage imageWithData:_data];
                            self.headerView.backImageView.hidden = NO;
                            self.headerView.videoHeaderView.hidden = YES;
                        }
                        
                    });
                    
                } failure:^(SLHttpRequestError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [HUDManager hideHUDView];
                        [HUDManager showWarningWithText:@"修改失败"];
                    });
                }];
                
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUDManager hideHUDView];
                    [HUDManager showWarningWithText:@"上传图片失败"];
                });
            }
            return nil;
        }];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - 保存图片至沙盒
- (void) saveImage:(UIImage *)currentImage withName:(NSString *)imageName
{
    NSData *imageData = UIImageJPEGRepresentation(currentImage, 0.01);
    // 获取沙盒目录
    NSString *fullPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:imageName];
    // 将图片写入文件
    [imageData writeToFile:fullPath atomically:NO];
    NSData *data = [NSData dataWithContentsOfFile:fullPath];
    self.data = data;
}

- (void)setLoginIM{
    NSDictionary *dict = [SLUserDefault getUserInfo];
    if (dict) {
        NSString *password = @"";
        if ([dict objectForKey:@"Password"]) {
            //            password = [AESCrypt decrypt:[dict objectForKey:@"Password"] password:@"chaojiquan"];
            password = [dict objectForKey:@"Password"];
            __weak typeof(self) weakSelf = self;
            [[SLMessageManger sharedInstance] callThisAfterISVAccountLoginSuccessWithYWLoginId:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]
                                                                                      passWord:[dict objectForKey:@"Password"]
                                                                               preloginedBlock:^{
                                                                                   // 显示会话列表页面
                                                                               } successBlock:^{
                                                                                   
                                                                                   [[SPUtil sharedInstance] setWaitingIndicatorShown:NO withKey:weakSelf.description];
                                                                                   
                                                                               } failedBlock:^(NSError *aError) {
                                                                                   
                                                                               }];
        }
    }
}

- (void)shareBtnClick{
    [self openPopView];
}
-(void)openPopView{
    if (!_popView) {
        UIView *popView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_W, screen_H)];
        popView.alpha=1;
        self.popView=popView;
        UIImageView *blackBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(screen_W-125, 65, 110, 80)];
        blackBackgroundView.image=[UIImage imageNamed:@"popView"];
        blackBackgroundView.userInteractionEnabled=YES;
        [popView addSubview:blackBackgroundView];
        
        //        UIView *blackBackgroundView=[[UIView alloc] initWithFrame:CGRectMake(screen_W-120, 70, 110, 80)];
        //        blackBackgroundView.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        //        blackBackgroundView.layer.borderWidth=0.5;
        //        blackBackgroundView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //        blackBackgroundView.layer.cornerRadius=2.5;
        //        blackBackgroundView.clipsToBounds=YES;
        
        UIButton *shareButton=[UIButton buttonWithType:UIButtonTypeCustom];
        shareButton.frame=CGRectMake(0, 0, 110, 40);
        [shareButton setTitle:@"分享" forState:UIControlStateNormal];
        [shareButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        shareButton.titleLabel.font=[UIFont systemFontOfSize:15];
        [shareButton addTarget:self action:@selector(shareClick) forControlEvents:UIControlEventTouchUpInside];
        [blackBackgroundView addSubview:shareButton];
        
        UIImageView *lineImgView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 40.5, 90, 0.5)];
        lineImgView.backgroundColor=[UIColor lightGrayColor];
        [blackBackgroundView addSubview:lineImgView];
        
        UIButton *followButton=[UIButton buttonWithType:UIButtonTypeCustom];
        followButton.frame=CGRectMake(0, 40, 110, 40);
        
        self.followButton=followButton;
        [followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        followButton.titleLabel.font=[UIFont systemFontOfSize:15];
        if ([self.userId isEqual:ApplicationDelegate.userId]) {
            [followButton addTarget:self action:@selector(editBaseInfo) forControlEvents:UIControlEventTouchUpInside];
            [self.followButton setTitle:@"编辑个人资料" forState:UIControlStateNormal];
        }else{
            [followButton addTarget:self action:@selector(followBtnClick) forControlEvents:UIControlEventTouchUpInside];
            [self.followButton setTitle:@"移出黑名单" forState:UIControlStateSelected];
            [self.followButton setTitle:@"加入黑名单" forState:UIControlStateNormal];
        }
        
        [blackBackgroundView addSubview:followButton];
        if ([self.userInfo[@"blocked"] integerValue]) {
            
            self.followButton.selected = YES;
            
        }else{
            
            self.followButton.selected = NO;
        }
        
    }
    
    [self.view addSubview:self.popView];
    
}
- (void)editBaseInfo{
    [self closePopView];
    SLMineBasicInfoViewController *MineBasicInfoView = [[SLMineBasicInfoViewController alloc] init];
    MineBasicInfoView.userData = self.userInfo;
    [self.navigationController pushViewController:MineBasicInfoView animated:YES];
}
//-(void)touch:(UIGestureRecognizer *)gesture{
//    [self closePopView];
//}
-(void)closePopView{
    [self.popView removeFromSuperview];
}

-(void)shareClick{
    [self closePopView];
    NSString *idStr = self.userId;
    NSString *urlStr = [KEY_Share_User_url stringByAppendingString:[NSString stringWithFormat:@"id=%@",idStr]];
    
    SLShareModel *shareModel=[[SLShareModel alloc] init];
    shareModel.shareTitle= self.userInfo[@"nickname"];
    shareModel.shareContent=[NSString stringWithFormat:@"%@",[SLValueUtils stringFromObject:self.userInfo[@"bio"]]];
    shareModel.shareUrl=urlStr;
    shareModel.shareImage=self.headerView.avatarbtn.imageView.image;
    
    SLShareView *shareView = [[SLShareView alloc] init];
    shareView.frame = CGRectMake(0, 0, ScreenW, ScreenH+190);
    [shareView showViewWithModel:shareModel viewController:self];
    [[UIApplication sharedApplication].keyWindow addSubview:shareView];
    
    //    [[SLShareView ShareView] showViewWithModel:shareModel viewController:self];
    //    [[UIApplication sharedApplication].keyWindow addSubview:[SLShareView ShareView]];
}
-(void)followBtnClick{
    [self closePopView];
    if (!ApplicationDelegate.Basic) {
        //判断是否是登录成功的状态
        [self.alert show];
        return;
    }
    NSString *othersID = self.userInfo[@"id"];
    NSDictionary *parameters = @{@"id":[NSString stringWithFormat:@"%@", othersID]};
    if (self.followButton.selected) {
        //取消拉黑该用户
        YWPerson *person = [[YWPerson alloc] initWithPersonId:[NSString stringWithFormat:@"%@",othersID] appKey:[YWAPI sharedInstance].appKey];
        [[[SLMessageManger sharedInstance].ywIMKit.IMCore getContactService] removePersonFromBlackList:person withResultBlock:^(NSError *error, YWPerson *person) {
            BOOL isSuccess = (error == nil ||error.code == 0);
            if(isSuccess) {
                
                [SLAPIHelper cancelBlack:parameters method:Delete success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                    self.followButton.selected = !self.followButton.selected;
                    // 取消im里面的取消拉黑功能
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [HUDManager showWarningWithText:@"移出黑名单成功"];
                    });
                    //发送通知,取消拉黑
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"isUnfollow" object:self];
                    
                    
                } failure:^(SLHttpRequestError *error) {
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [HUDManager showWarningWithText:@"移出黑名单失败"];
                    });
                }];
            }else{
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [HUDManager showWarningWithText:@"移出黑名单失败"];
                });
            }
        }];
        
    }
    else{
        //拉黑该用户
        YWPerson *person = [[YWPerson alloc] initWithPersonId:[NSString stringWithFormat:@"%@",othersID] appKey:[YWAPI sharedInstance].appKey];
        [[[SLMessageManger sharedInstance].ywIMKit.IMCore getContactService] addPersonToBlackList:person withResultBlock:^(NSError *error, YWPerson *person) {
            if (error == nil ||error.code == 0) {
                
                [SLAPIHelper cancelBlack:parameters method:Post success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                    self.followButton.selected = !self.followButton.selected;
                    //发送通知,加入拉黑
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"isUnfollow" object:self];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [HUDManager showWarningWithText:@"加入黑名单成功"];
                    });
                } failure:^(SLHttpRequestError *error) {
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [HUDManager showWarningWithText:@"加入黑名单失败"];
                    });
                }];
                
            }else{
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [HUDManager showWarningWithText:@"加入黑名单失败"];
                });
            }
        }];
    }
}

- (void)addNoti{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetSecondMeData:) name:@"resetSecondMeData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isLoginSuccessToLoadData:) name:@"isLoginSuccessToLoadData" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadUserData) name:@"resetSecondMeUserData" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetUserVideo) name:@"resetUserVideo" object:nil];
    //回复评价成功刷新页面
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(replyEvaluateSuccess) name:@"replyEvaluateSuccess" object:nil];
    
}
#pragma mark - 刷新数据
- (void)replyEvaluateSuccess{
    [self getData];
}
- (void)isLoginSuccessToLoadData:(NSNotification *)noti{
    self.hasloadMeTopic = NO;
    self.hasLoadUserInfo = NO;
    [self getData];
}

- (void)resetSecondMeData:(NSNotification *)noti{
    [self reloadUserData];
}
- (void)reloadUserData{
    [SLAPIHelper getUsersData:@{@"id":self.userId} success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        self.userInfo = data[@"result"];
        //        self.headerView.userInfo = self.userInfo;
        NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data[@"result"]];
        [SLUserDefault setPersonInfo:self.userId andData:configData];
        if ([self.userId isEqual:ApplicationDelegate.userId]) {
            //                                        NSLog(@"%@",[SLUserDefault getUserInfo]);
            NSDictionary *dict = [SLUserDefault getUserInfo];
            NSString *basic = dict[@"basic"];
            NSString *Password = dict[@"Password"];
            NSDictionary *newUserInfo = @{@"basic": basic, @"userInformation": data[@"result"],@"Password": Password};
            [SLUserDefault setUserInfo:newUserInfo];
        }
        if ([self.userInfo[@"gender"] isEqualToString:@"FEMALE"]) {
            self.headerView.sexImageView.image = [UIImage imageNamed:@"Female-symbol"];
        }else {
            self.headerView.sexImageView.image = [UIImage imageNamed:@"Male-Symbol"];
        }
        LeftTableViewController *vc1 = self.childArr[0];
        vc1.myInfo = data[@"result"];
        nameLab.text=self.userInfo[@"nickname"];
        
        [self.headerView changNickName:self.userInfo[@"nickname"]];
    } failure:^(SLHttpRequestError *failure) {
        
    }];
}
- (void)resetUserVideo{
    [SLAPIHelper getUsersData:@{@"id":self.userId} success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        self.userInfo = data[@"result"];
        self.headerView.userInfo = self.userInfo;
        [self.headerView changeVideo];
        NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data[@"result"]];
        [SLUserDefault setPersonInfo:self.userId andData:configData];
        if ([self.userId isEqual:ApplicationDelegate.userId]) {
            //                                        NSLog(@"%@",[SLUserDefault getUserInfo]);
            NSDictionary *dict = [SLUserDefault getUserInfo];
            NSString *basic = dict[@"basic"];
            NSString *Password = dict[@"Password"];
            NSDictionary *newUserInfo = @{@"basic": basic, @"userInformation": data[@"result"],@"Password": Password};
            [SLUserDefault setUserInfo:newUserInfo];
        }
    } failure:^(SLHttpRequestError *failure) {
        
    }];
}
- (void)reloadUserDataNotChangeAvatar{
    [SLAPIHelper getUsersData:@{@"id":self.userId} success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        self.userInfo = data[@"result"];
        self.headerView.userInfo = self.userInfo;
        NSData *configData = [NSKeyedArchiver archivedDataWithRootObject:data[@"result"]];
        [SLUserDefault setPersonInfo:self.userId andData:configData];
        if ([self.userId isEqual:ApplicationDelegate.userId]) {
            //                                        NSLog(@"%@",[SLUserDefault getUserInfo]);
            NSDictionary *dict = [SLUserDefault getUserInfo];
            NSString *basic = dict[@"basic"];
            NSString *Password = dict[@"Password"];
            NSDictionary *newUserInfo = @{@"basic": basic, @"userInformation": data[@"result"],@"Password": Password};
            [SLUserDefault setUserInfo:newUserInfo];
        }
        
    } failure:^(SLHttpRequestError *failure) {
        
    }];
}

#pragma mark - HZPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (_photos.count>9) {
        return 9;
    }
    return _photos.count;
}

#pragma 代理
- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 100) {
        if (buttonIndex == 1) {
            NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
            NSString *para = [NSString stringWithFormat:@"%@",self.userId];
            [parameter setObject:para forKey:@"user_id"];
            [SLAPIHelper cancelFollow:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                followBtn.selected = !followBtn.selected;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"isUnfollow" object:self];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeUserData" object:self];
                if (self.rankingFollowBlock) {
                    self.rankingFollowBlock(NO);
                }
                [HUDManager showWarningWithText:@"关注取消"];
                if (self.didClickFollowBtn) {
                    self.didClickFollowBtn(followBtn);
                }
                followBtn.enabled = YES;
            } failure:^(SLHttpRequestError *error) {
                [HUDManager showWarningWithText:@"关注取消失败"];
                followBtn.enabled = YES;
            }];
        }else{
            followBtn.enabled = YES;
        }
    }else{
        if (buttonIndex==1) {
            SLFirstViewController *vc = [[SLFirstViewController alloc] init];
            SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
            [self presentViewController:nav animated:YES completion:nil];
        }
    }
    
}
// 没有内容
-(void)addNotDatas:(NSString *)str tableView:(UITableView *)tableView index:(NSInteger)index{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, self.headerHeight+40, ScreenW , ScreenH)];
    SLLabel *tiplabel = [[SLLabel alloc]init];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentCenter;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    tiplabel.attributedText = AttributedStr;
    [contentView addSubview:tiplabel];
    SLLabel *bolangLabel = [[SLLabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    if (index==1) {
        self.hasNoTopicsDatasView = contentView;
    }else if (index==2){
        self.hasNoReciveDatasView = contentView;
    }
    [contentView addSubview:imgView];
    [tableView addSubview:contentView];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self closePopView];
}
-(void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"automaticPlay" object:self];
        }else if ([netWorkStaus isEqualToString:@"wan"] || [netWorkStaus isEqualToString:@"Unknown"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"WANPlay" object:self];
        }
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
