//
//  BaseTableViewController.m
//  微博个人主页
//
//  Created by zenglun on 16/5/4.
//  Copyright © 2016年 chengchengxinxi. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@property (nonatomic, assign) CGFloat headerHeight;

@property (nonatomic, strong)UILabel *bolangLabel;

@property (nonatomic, strong)UIImageView *swanImageview;


@end

@implementation BaseTableViewController

- (UILabel *)tiplabel{
    if (!_tiplabel) {
        _tiplabel = [[UILabel alloc] init];
        _tiplabel.font = [UIFont systemFontOfSize:17];
        _tiplabel.textAlignment = NSTextAlignmentCenter;
        [self.noNetworkOrDefaultView addSubview:_tiplabel];
        
    }
    return _tiplabel;
}

- (UILabel *)bolangLabel{
    if (!_bolangLabel) {
        _bolangLabel = [[UILabel alloc] init];
        _bolangLabel.font = [UIFont systemFontOfSize:12];
        _bolangLabel.textAlignment = NSTextAlignmentCenter;
        _bolangLabel.text = @"~~~";
        _bolangLabel.textColor = SLColor(213, 213, 213);
        [self.noNetworkOrDefaultView addSubview:_bolangLabel];
    }
    return _bolangLabel;
}

- (UIImageView *)swanImageview{
    if (!_swanImageview) {
        _swanImageview = [[UIImageView alloc] init];
        _swanImageview.image = [UIImage imageNamed:@"swan"];
        [self.noNetworkOrDefaultView addSubview:_swanImageview];
    }
    return _swanImageview;
}

- (UIView *)noNetworkOrDefaultView{
    if (!_noNetworkOrDefaultView) {
        _noNetworkOrDefaultView = [[UIView alloc]initWithFrame:CGRectMake(0, self.headerHeight+42, screen_W, screen_H-64-49-20)];
        _noNetworkOrDefaultView.backgroundColor = SLColor(245, 245, 245);
        [self.tableView addSubview:_noNetworkOrDefaultView];
    }
    return _noNetworkOrDefaultView;
}
//其他制空页面
-(void)addNetWorkViews:(NSString *)str{
    [self.tableView addSubview:self.noNetworkOrDefaultView];
    CGSize tipLabelSize = [self.tiplabel boundingRectWithString:str withSize:CGSizeMake(300,20) withFont:17];
    self.tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 20);
    if (ScreenH == 200) {
        self.tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width, 20);
    }
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    self.tiplabel.attributedText = AttributedStr;
    
    self.bolangLabel.frame = CGRectMake(self.tiplabel.frame.origin.x+self.tiplabel.frame.size.width,self.tiplabel.frame.origin.y, 50, 10);
    
    self.swanImageview.frame = CGRectMake(ScreenW *0.5-96, self.tiplabel.frame.origin.y + 76, 192, 192);
    
}

- (UIView *)myloadView{
    if (!_myloadView) {
        _myloadView =[[UIView alloc] init];
        _myloadView.frame=CGRectMake(0, self.headerHeight+42, screen_W, 40);
        _myloadView.backgroundColor = [UIColor clearColor];
        [self.tableView addSubview:_myloadView];
        _myloadView.hidden = YES;
        UILabel *tipLabel =[[UILabel alloc] init];
        tipLabel.frame = _myloadView.bounds;
        tipLabel.font = [UIFont systemFontOfSize:14];
        tipLabel.textColor = [UIColor grayColor];
        tipLabel.textAlignment = NSTextAlignmentCenter;
        tipLabel.text = @"正在加载...";
        [_myloadView addSubview:tipLabel];
    }
    return _myloadView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@初始化", self);

    self.headerHeight = ceilf(3/4.0 * ScreenW)+130;

    self.tableView.showsHorizontalScrollIndicator  = NO;
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, self.headerHeight + switchBarHeight)];
    headerView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableHeaderView = headerView;
    
    if (self.tableView.contentSize.height < kScreenHeight + self.headerHeight - topBarHeight ) {
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, kScreenHeight + self.headerHeight - topBarHeight - self.tableView.contentSize.height, 0);
    }
}

- (void)dealloc {
    NSLog(@"%@销毁", self);
}

#pragma mark - ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    if ([self.delegate respondsToSelector:@selector(tableViewScroll:offsetY:)]) {
        [self.delegate tableViewScroll:self.tableView offsetY:offsetY];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    if ([self.delegate respondsToSelector:@selector(tableViewWillBeginDragging:offsetY:)]) {
        [self.delegate tableViewWillBeginDragging:self.tableView offsetY:offsetY];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    if ([self.delegate respondsToSelector:@selector(tableViewWillBeginDecelerating:offsetY:)]) {
        [self.delegate tableViewWillBeginDecelerating:self.tableView offsetY:offsetY];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    CGFloat offsetY = scrollView.contentOffset.y;
    if ([self.delegate respondsToSelector:@selector(tableViewDidEndDragging:offsetY:)]) {
        [self.delegate tableViewDidEndDragging:self.tableView offsetY:offsetY];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    if ([self.delegate respondsToSelector:@selector(tableViewDidEndDecelerating:offsetY:)]) {
        [self.delegate tableViewDidEndDecelerating:self.tableView offsetY:offsetY];
    }
}




@end
