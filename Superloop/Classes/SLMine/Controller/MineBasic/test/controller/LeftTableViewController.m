//
//  LeftTableViewController.m
//  微博个人主页
//
//  Created by zenglun on 16/5/4.
//  Copyright © 2016年 chengchengxinxi. All rights reserved.
//

#import "LeftTableViewController.h"
#import "SLBioCell.h"
#import "SLTagsTableViewCell.h"
#import "SLFuTagsTableViewCell.h"
#import "SLSettingModel.h"
#import "SLSecondMeCell.h"
#import <AliyunOSSiOS/OSSService.h>
#import "AppDelegate.h"
#import "SLAPIHelper.h"
#import "SLIntroduceViewController.h"
#import "SLMoreCategoryViewController.h"
#import "SLCreateQRCodeViewController.h"
#import "SLMineBasicInfoViewController.h"
#import "NSString+Utils.h"

@interface LeftTableViewController ()

//分组数据
@property(nonatomic, strong)NSArray *sectionArr;
//是否超过五行
@property(nonatomic, assign)BOOL isBeyondFive;
//是否关闭查看更多
@property(nonatomic, assign)BOOL isclose;


@end

@implementation LeftTableViewController


- (void)setNoNetWork:(BOOL)NoNetWork{
    _NoNetWork = NoNetWork;
    [self.tableView reloadData];
    [self resetContentInset];
}

- (void)setMyInfo:(NSDictionary *)myInfo{
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _myInfo = myInfo;
        [self addPersonInfo];
        self.myloadView.hidden = YES;
        [self.tableView reloadData];
        [self resetContentInset];
//    });
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"LeftTableViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"LeftTableViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[SLBioCell class] forCellReuseIdentifier:@"BioCell"];
    [self.tableView registerClass:[SLTagsTableViewCell class] forCellReuseIdentifier:@"TagsCell"];
    [self.tableView registerClass:[SLFuTagsTableViewCell class] forCellReuseIdentifier:@"FuTagsCell"];
    [self.tableView registerClass:[SLSecondMeCell class] forCellReuseIdentifier:@"secondmeCell"];
    self.tableView.backgroundColor = SLColor(241, 241, 241);
    
    self.isclose = YES;
    self.myloadView.hidden = YES;
    [self addPersonInfo];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (self.myInfo) {
//    if (self.hasNetWork) {
//        return 0;
//    }else{
        return 3+self.sectionArr.count;
//    }
    
//    }else{
//        return 0;
//    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        SLBioCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BioCell"];
        
        if (!cell) {
            cell = [[SLBioCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"BioCell"];
        }
        __weak typeof(self) weakSelf = self;
        cell.seeMore = ^(NSString *str){
            weakSelf.isclose = !weakSelf.isclose;
            [weakSelf.tableView reloadData];
        };
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.dict = self.myInfo;
        return cell;
    }else if (indexPath.row == 1) {
        
        SLTagsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TagsCell"];
        if (!cell) {
            cell = [[SLTagsTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"TagsCell"];
        }
        cell.dict = self.myInfo;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else if (indexPath.row == 2) {
        SLFuTagsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FuTagsCell"];
        if (!cell) {
            cell = [[SLFuTagsTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"FuTagsCell"];
        }
        cell.dict = self.myInfo;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else{
        SLSecondMeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"secondmeCell"];
        if (!cell) {
            cell = [[SLSecondMeCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"secondmeCell"];
        }
        if ([self.myInfo[@"id"] isEqual:ApplicationDelegate.userId]) {
            if (indexPath.row==3||indexPath.row==4) {
                cell.selectionStyle = UITableViewCellSelectionStyleDefault;
            }else{
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
        }else{
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.backgroundColor = [UIColor whiteColor];
        cell.model = self.sectionArr[indexPath.row-3];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        if (indexPath.row == 0) {
            CGFloat height = 0;
            if (self.isBeyondFive) {
                height = 40;
            }
            return [self modelHeight:self.myInfo]+40+15+height;
        }else if (indexPath.row == 1){
            NSMutableArray *primary_arr = [NSMutableArray array];
            for (NSDictionary *d in self.myInfo[@"primary_tags"]) {
                [primary_arr addObject:d[@"name"]];
            }
            CGFloat height = [self allocTags:primary_arr];
            return 75+height;
        }else if (indexPath.row == 2){
            NSMutableArray *primary_arr = [NSMutableArray array];
            for (NSDictionary *d in self.myInfo[@"secondary_tags"]) {
                [primary_arr addObject:d[@"name"]];
            }
            CGFloat height = [self allocTags:primary_arr];
            return 60+height;
        }
        return 55;
}

- (void)request_data {
    // 如果需要请求网络数据, 再调用tableView的reloadData之后，调用下面resetContentInset方法可以消除底部多余空白
}

#pragma mark - Private
- (void)resetContentInset {

    CGFloat headerHeight;
    headerHeight = ceilf(3/4.0 * ScreenW)+130;

    if (self.tableView.contentSize.height < kScreenHeight + 136) {  // 136 = 200
        if (self.myInfo) {
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, kScreenHeight+headerHeight-64-self.tableView.contentSize.height, 0);
        }else{
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, kScreenHeight-64-40, 0);
        }
        
    } else {
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 70, 0);
    }
}
- (CGFloat)allocTags:(NSMutableArray *)arr{
    CGFloat w = 0;//保存前一个label的宽以及前一个label距离屏幕边缘的距离
    CGFloat h = 30;//用来控制label距离父视图的高
    
    for (int i = 0; i < arr.count; i++) {
        
        NSString *str = [self removeStringSpace:arr[i]];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
        CGSize titlesize = [str boundingRectWithSize:CGSizeMake(screen_W-48, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        CGFloat length = titlesize.width;
        //当label的位置超出屏幕边缘时换行  只是label所在父视图的宽度
        //20 为label以外的
        if( w + length + 20 > screen_W-60){
            w = 0; //换行时将w置为0
            h = h + 30 + 10;//距离父视图也变化
        }
        w = w + length + 20 +5;
    }
    return h;
}
#pragma mark - 去掉空格换行
- (NSString *)removeStringSpace:(NSString *)str{
    NSString *headerData = str;
    headerData = [headerData stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return headerData;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (![self.myInfo[@"id"] isEqual:ApplicationDelegate.userId]) {
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        [self enterBio];
    }else if (indexPath.row == 1){
        [self chooseTags:@1];
    }else if (indexPath.row == 2){
        [self chooseTags:@2];
    }else if (indexPath.row == 3){
        SLMineBasicInfoViewController *MineBasicInfoView = [[SLMineBasicInfoViewController alloc] init];
        MineBasicInfoView.userData = self.myInfo;
        [self.navigationController pushViewController:MineBasicInfoView animated:YES];
    }else if (indexPath.row == 4){
        SLCreateQRCodeViewController *vc = [[SLCreateQRCodeViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)addPersonInfo{
    
    NSString *superID = @"";
    if ([[self.myInfo allKeys] containsObject:@"username"]) {
        superID = self.myInfo[@"username"];
    }
    NSString *location = @"";
    if ([[self.myInfo allKeys] containsObject:@"location"]) {
        location = self.myInfo[@"location"];
    }
    NSString *workName = @"";
    NSString *position = @"";
    if ([[self.myInfo allKeys] containsObject:@"work_history"]) {
        NSArray *arr = self.myInfo[@"work_history"];
        if (arr.count > 0) {
            NSDictionary *dict = arr[0];
            if ([[dict allKeys] containsObject:@"company"]) {
                workName = arr[0][@"company"];
            }
            if ([[dict allKeys] containsObject:@"position"]) {
                position = arr[0][@"position"];
            }
        }
    }
    NSString *institution = @"";
    NSString *degree = @"";
    if ([[self.myInfo allKeys] containsObject:@"education"]) {
        NSArray *arr = self.myInfo[@"education"];
        if (arr.count > 0) {
            NSDictionary *dict = arr[0];
            if ([[dict allKeys] containsObject:@"institution"]) {
                institution = arr[0][@"institution"];
            }
            if ([[dict allKeys] containsObject:@"degree"]) {
                degree = arr[0][@"degree"];
            }
        }
    }
    NSArray *dictArr1;
   
    if (![self.myInfo[@"id"] isEqual:ApplicationDelegate.userId]) {
         dictArr1 = @[@{@"imageName":@"",@"title":@"基本信息",@"subTitle":@"",@"hasArrow":@NO},@{@"imageName":@"",@"title":@"超级号",@"subTitle":superID,@"hasArrow":@NO},@{@"imageName":@"",@"title":@"所在地",@"subTitle":location,@"hasArrow":@NO},@{@"imageName":@"",@"title":@"公司",@"subTitle":workName,@"hasArrow":@NO},@{@"imageName":@"",@"title":@"职位",@"subTitle":position,@"hasArrow":@NO},@{@"imageName":@"",@"title":@"毕业于",@"subTitle":institution,@"hasArrow":@NO},@{@"imageName":@"",@"title":@"学历",@"subTitle":degree,@"hasArrow":@NO}];
    }
    else{
        dictArr1 = @[@{@"imageName":@"",@"title":@"编辑资料",@"subTitle":@"",@"hasArrow":@YES},@{@"imageName":@"",@"title":@"我的二维码",@"subTitle":@"",@"hasArrow":@NO},@{@"imageName":@"",@"title":@"超级号",@"subTitle":superID,@"hasArrow":@NO},@{@"imageName":@"",@"title":@"所在地",@"subTitle":location,@"hasArrow":@NO},@{@"imageName":@"",@"title":@"公司",@"subTitle":workName,@"hasArrow":@NO},@{@"imageName":@"",@"title":@"职位",@"subTitle":position,@"hasArrow":@NO},@{@"imageName":@"",@"title":@"毕业于",@"subTitle":institution,@"hasArrow":@NO},@{@"imageName":@"",@"title":@"学历",@"subTitle":degree,@"hasArrow":@NO}];
    }
   
    self.sectionArr = [SLSettingModel mj_objectArrayWithKeyValuesArray:dictArr1];
}
- (void)enterBio{
    SLIntroduceViewController *introduce = [[SLIntroduceViewController alloc] init];
    introduce.intro = self.myInfo[@"bio"];
    [self.navigationController pushViewController:introduce animated:YES];
}
#pragma mark - 选择标签
- (void)chooseTags:(NSNumber *)num{
    SLMoreCategoryViewController *SLMoreCategoryVC = [[SLMoreCategoryViewController alloc] init];
    SLMoreCategoryVC.tagsType = num;
    SLMoreCategoryVC.userData = self.myInfo;
    [self.navigationController pushViewController:SLMoreCategoryVC animated:YES];
}

-(CGFloat)modelHeight:(NSDictionary *)dict{
    CGFloat textMaxW = ScreenW - 50;
    NSString *titleContentextString = @"高度，高度";
    CGFloat titleFontSize = [titleContentextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:16]} context:nil].size.height;
    NSString *chooseContent = @"";
    if ([[dict allKeys] containsObject:@"bio"]) {
        chooseContent = dict[@"bio"];
    }else{
        chooseContent = @"没有简介";
    }
    if([NSString isEmptyStrings:chooseContent]){
        chooseContent = @"没有简介";
    }
    CGFloat titleHeight = [chooseContent boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:16],NSKernAttributeName:@1.5f} context:nil].size.height;
    NSInteger titleLineNum = titleHeight / titleFontSize;
    if (self.isclose) {
        if (titleLineNum > 5) {
            titleLineNum = 5;
            self.isBeyondFive = YES;
        }else{
            self.isBeyondFive = NO;
        }
    }
    
    return titleFontSize*titleLineNum+6*(titleLineNum-1);
}


@end
