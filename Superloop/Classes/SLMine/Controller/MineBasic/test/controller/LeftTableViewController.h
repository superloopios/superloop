//
//  LeftTableViewController.h
//  微博个人主页
//
//  Created by zenglun on 16/5/4.
//  Copyright © 2016年 chengchengxinxi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@interface LeftTableViewController : BaseTableViewController

@property(nonatomic,strong)NSDictionary *myInfo;
@property (nonatomic, copy)NSString *userId;
@property (nonatomic, assign)BOOL NoNetWork;


@end
