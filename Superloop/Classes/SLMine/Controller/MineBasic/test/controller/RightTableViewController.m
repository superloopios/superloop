//
//  RightTableViewController.m
//  微博个人主页
//
//  Created by zenglun on 16/5/4.
//  Copyright © 2016年 chengchengxinxi. All rights reserved.
//

#import "RightTableViewController.h"
#import "SLCallComments.h"
#import "HomePageViewController.h"
#import "SLRecieveEvaluateTableViewCell.h"
#import "SLActionView.h"
#import "SLReplyEvaluateViewController.h"
#import "AppDelegate.h"
#import "NSString+Utils.h"

@interface RightTableViewController ()<SLRecieveEvaluateTableViewCellDelegate,SLActionViewDelegate>
@property (nonatomic,strong) SLActionView *actionView;
@property (nonatomic,strong) SLCallComments *callModel;//回复的那个model
@property (nonatomic,assign) BOOL isPhoneOrMsg;//判断是电话还是付费消息
@end

@implementation RightTableViewController
static NSString *ID = @"recieveCell";


- (SLActionView *)actionView{
    if (!_actionView) {
        _actionView = [[SLActionView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _actionView.titleArr = @[@"取消",@"回复",];
        _actionView.delegate = self;
        [[UIApplication sharedApplication].keyWindow addSubview:_actionView];
    }
    return _actionView;
}

- (void)setReceiveDataSources:(NSMutableArray *)receiveDataSources{
    _receiveDataSources = receiveDataSources;
    [self.tableView reloadData];
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self resetContentInset];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"RightTableViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"RightTableViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[SLRecieveEvaluateTableViewCell class] forCellReuseIdentifier:ID];
    self.tableView.backgroundColor = SLColor(240, 240, 240);
    self.tableView.tableFooterView = [[UIView alloc] init];
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    SLCallComments *model = self.receiveDataSources[indexPath.row];
    return [self calculateHeight:model];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.receiveDataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SLRecieveEvaluateTableViewCell  *recieveCell = [tableView dequeueReusableCellWithIdentifier:ID];
    recieveCell.userId = self.userId;
    recieveCell.selectionStyle = UITableViewCellSelectionStyleNone;
    recieveCell.recieveComment =  self.receiveDataSources[indexPath.row];
    recieveCell.indexPath = indexPath;
    recieveCell.backgroundColor=[UIColor whiteColor];
    recieveCell.delegate = self;
    return recieveCell;
}


- (void)request_data {
    // 如果需要请求网络数据, 再调用tableView的reloadData之后，调用下面resetContentInset方法可以消除底部多余空白
}

#pragma mark - Private
- (void)resetContentInset {
    //    [self.tableView layoutIfNeeded];
    CGFloat headerHeight;
    //    if (screen_W ==320) {
    headerHeight = ceilf(3/4.0 * ScreenW)+130;
    //    } else if(screen_W == 375) {
    //        headerHeight = 267.5;
    //    } else{
    //        headerHeight = 267.5 *1.1;
    //    }
    if (self.tableView.contentSize.height < kScreenHeight + 136) {  // 136 = 200
        if (self.receiveDataSources.count>0) {
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, kScreenHeight+headerHeight-64-self.tableView.contentSize.height, 0);
        }else{
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, kScreenHeight-64-40, 0);
        }
        
    } else {
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 55, 0);
    }
}
- (CGFloat)calculateHeight:(SLCallComments *)model{
    CGFloat cellHeight;
    if ([model.anonymous_mode integerValue] == 1) {//匿名
        cellHeight = 46;
    }else{
        cellHeight = 65;
    }
    CGFloat standardHeight = [self getSpaceLabelHeight:@"高度，高度" withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-70];
    CGFloat titleHeight = [self getSpaceLabelHeight:model.rate_content withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-70];
    NSInteger count = titleHeight/standardHeight;
    if (titleHeight/standardHeight>count) {
        count +=1;
    }
    if (count > 5) {
        if (!model.isClose) {
            count = 5;
        }
        if ([[NSString stringWithFormat:@"%@",self.userId] isEqualToString:[NSString stringWithFormat:@"%@",ApplicationDelegate.userId]]) {
            cellHeight += 30+8+1+35+15;
        }else{
            cellHeight += 30+8+1+35+15-23;
        }
        
    }else{
        if ([[NSString stringWithFormat:@"%@",self.userId] isEqualToString:[NSString stringWithFormat:@"%@",ApplicationDelegate.userId]]) {
            if ([model.rate_source integerValue] == 1) {
                cellHeight += 20+1+35+15;
                
            }else{
                cellHeight += 20+1+20+15;
            }
        }else{
            cellHeight += 20+1+35+15-17;
        }
    }
    cellHeight += standardHeight * count ;
    
    //回复评论
    if (![NSString isEmptyStrings:model.reply_content]) {
        CGFloat replyStandardHeight = [self getSpaceLabelHeight:@"高度，高度" withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-100];
        CGFloat replyTitleHeight = [self getSpaceLabelHeight:model.reply_content withFont:[UIFont systemFontOfSize:15] withWidth:ScreenW-100];
        NSInteger replyCount = replyTitleHeight/replyStandardHeight;
        if (replyTitleHeight/replyStandardHeight>replyCount) {
            replyCount +=1;
        }
        if (replyCount > 5) {
            if (!model.isReplyClose) {
                replyCount = 5;
            }
            cellHeight += 30+22;
            
        }else{
            if (replyCount == 1) {
                cellHeight += 30+17-8;
            }else{
                cellHeight += 30+17;
            }
        }
        cellHeight += replyStandardHeight * replyCount ;
        if (![[NSString stringWithFormat:@"%@",self.userId] isEqualToString:[NSString stringWithFormat:@"%@",ApplicationDelegate.userId]]) {
            cellHeight += 15;
        }
    }
    return cellHeight;
}
//计算UILabel的高度(带有行间距的情况)
- (CGFloat)getSpaceLabelHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = 8;
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f
                          };
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return size.height;
}
#pragma mark - cell代理事件
- (void)SLRecieveEvaluateTableViewCell:(SLRecieveEvaluateTableViewCell *)recieveEvaluateTableViewCell didSelectAtIndexWithModel:(SLCallComments *)model indexPath:(NSIndexPath *)indexPath{
    HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
    otherPerson.userId = [NSString stringWithFormat:@"%ld",[model.from_user_id integerValue]];
    [self.navigationController pushViewController:otherPerson animated:YES];
}
- (void)SLRecieveEvaluateTableViewCell:(SLRecieveEvaluateTableViewCell *)recieveEvaluateTableViewCell didSelectAtIndexWithReplyModel:(SLCallComments *)model isPhoneOrMsg:(BOOL)isPhoneOrMsg indexPath:(NSIndexPath *)indexPath{
    self.callModel = model;
    self.actionView.titleArr = @[@"取消",@"回复"];
    self.isPhoneOrMsg = isPhoneOrMsg;
    [[UIApplication sharedApplication].keyWindow addSubview:self.actionView];
}
- (void)SLRecieveEvaluateTableViewCell:(SLRecieveEvaluateTableViewCell *)recieveEvaluateTableViewCell didSelectAtIndexWithLaunchModel:(SLCallComments *)model indexPath:(NSIndexPath *)indexPath{
    [self.receiveDataSources replaceObjectAtIndex:indexPath.row withObject:model];
    [self.tableView reloadData];
}

- (void)SLRecieveEvaluateTableViewCell:(SLRecieveEvaluateTableViewCell *)recieveEvaluateTableViewCell didSelectAtIndexWithReplyLaunchModel:(SLCallComments *)model indexPath:(NSIndexPath *)indexPath{
    [self.receiveDataSources replaceObjectAtIndex:indexPath.row withObject:model];
    [self.tableView reloadData];
}
#pragma mark - SLActionViewDelegate代理事件
- (void)actionSheetButtonClickedAtIndex:(NSInteger)index{
    [self.actionView dismiss];
    if (index == 1) {
        SLReplyEvaluateViewController *vc = [[SLReplyEvaluateViewController alloc] init];
        vc.appraisal_id = [NSString stringWithFormat:@"%ld",[self.callModel.appraisal_id integerValue]];
        vc.isPhoneOrMsg = self.isPhoneOrMsg;
        vc.replySuccess = ^(BOOL isSuccess){
            [self.tableView reloadData];
        };
        [self presentViewController:vc animated:YES completion:nil];
    }
}

@end
