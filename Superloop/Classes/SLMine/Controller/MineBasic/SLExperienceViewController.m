//
//  SLExperienceViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "SLEducationViewCell.h"
#import "SLExperienceViewController.h"
#import "SLNewExperienceViewController.h"
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "SLAPIHelper.h"

@interface SLExperienceViewController()<UITableViewDelegate,UITableViewDataSource>{
    UIButton *_submitBtn;
}

@property (nonatomic, strong) NSDictionary *resultDict;

@property(nonatomic,assign)NSInteger recordCellIndex;

@property(nonatomic, weak)UITableView *tableView;

@end

@implementation SLExperienceViewController

//static NSString *const postName = @"postName";
static NSString *const postName = @"SaveExperience";

- (UITableView *)tableView{
    if (!_tableView) {
        UITableView *tableview = [[UITableView alloc] init];
        tableview.delegate = self;
        tableview.dataSource = self;
//        tableview.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableview.frame = CGRectMake(0, 64, screen_W, screen_H - 64);
        [tableview registerNib:[UINib nibWithNibName:@"SLEducationViewCell" bundle:nil] forCellReuseIdentifier:@"experienceCell"];
        //tableview.frame = CGRectMake(0, 64, screen_W, screen_H - 64);
        [self.view addSubview:tableview];
        _tableView = tableview;
    }
    return _tableView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}


- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"工作经历";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    _submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    [_submitBtn setTitle:@"编辑" forState:UIControlStateNormal];
    _submitBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_submitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(deleteExperience) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:_submitBtn];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getDatas];
    //self.navigationItem.title = @"工作经历";
    [self setUpNav];
    self.tableView.backgroundColor = SLColor(240, 240, 240);
    
    
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStyleDone target:self action:@selector(deleteExperience)];
    [self setUpUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateData:) name:postName object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateData:) name:@"ChangeExperience" object:nil];
}


- (void)deleteExperience
{
    [self.tableView setEditing:!self.tableView.isEditing animated:YES];
    if (self.tableView.isEditing) {
        [_submitBtn setTitle:@"取消" forState:UIControlStateNormal];
    }else
    {
         [_submitBtn setTitle:@"编辑" forState:UIControlStateNormal];
    }
}
- (void)setUpUI
{
    UIView *addView= [[UIView alloc] init];
    addView.frame = CGRectMake(0, 0, self.view.width, 60);
    self.tableView.tableFooterView = addView;
    UIButton *addExperience = [UIButton buttonWithType:UIButtonTypeCustom];
    addExperience.backgroundColor = [UIColor whiteColor];
    [addExperience setTitle:@"+添加工作经历" forState:UIControlStateNormal];
    [addExperience setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    addExperience.layer.cornerRadius = 5;
    addExperience.layer.masksToBounds = YES;
    addExperience.layer.borderColor = SLColor(165, 165, 165).CGColor;
    addExperience.layer.borderWidth = 0.5;
    addExperience.frame = CGRectMake(10, 10, self.view.width - 20, 40);
    [addExperience addTarget:self action:@selector(addExperience) forControlEvents:UIControlEventTouchUpInside];
    [addView addSubview:addExperience];
}
- (void)addExperience
{
    SLNewExperienceViewController *newExperience = [[SLNewExperienceViewController alloc] init];
    newExperience.event =self.event;
    [self.navigationController pushViewController:newExperience animated:YES];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _experienceArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SLEducationViewCell *educationCell = [tableView dequeueReusableCellWithIdentifier:@"experienceCell"];
    
    
    if (((NSString *)(_experienceArray[indexPath.row][@"start_date"])).length>0&&((NSString *)(_experienceArray[indexPath.row][@"end_date"])).length>0) {
        educationCell.timeLabel.text = [NSString stringWithFormat:@"%@-%@", _experienceArray[indexPath.row][@"start_date"], _experienceArray[indexPath.row][@"end_date"]];
    }else{
        educationCell.timeLabel.text = @"";
    }
    if (((NSString *)(_experienceArray[indexPath.row][@"company"])).length>0) {
        educationCell.nameLabel.text = _experienceArray[indexPath.row][@"company"];
    }else{
        educationCell.nameLabel.text = @"";
        
    }
    if (((NSString *)(_experienceArray[indexPath.row][@"position"])).length>0) {
        educationCell.positionLabel.text = _experienceArray[indexPath.row][@"position"];
    }else{
        educationCell.positionLabel.text = @"";
        
    }
    if (((NSString *)(_experienceArray[indexPath.row][@"location"])).length>0) {
        educationCell.locationLabel.text = _experienceArray[indexPath.row][@"location"];
    }else{
        educationCell.locationLabel.text = @"";
        
    }
    NSString *locationStr=_experienceArray[indexPath.row][@"location"];
    
    if (locationStr.length>0) {
        educationCell.cutImgVIew.hidden=NO;
        //根据计算文字的大小
        CGFloat positionWidth= [_experienceArray[indexPath.row][@"position"] boundingRectWithSize:CGSizeMake(ScreenW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.width+2;
        
        CGFloat locationWidth= [locationStr boundingRectWithSize:CGSizeMake(ScreenW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.width+2;
        if (positionWidth>220) {
            positionWidth=220;
        }
        if (locationWidth+positionWidth+30>ScreenW){
            locationWidth=ScreenW-positionWidth-30;
        }
        [educationCell.positionLabConstraint setConstant:ScreenW-positionWidth-30];
        [educationCell.locationLabConstraint setConstant:ScreenW-positionWidth-locationWidth-40];
    }else{
        educationCell.cutImgVIew.hidden=YES;
        [educationCell.positionLabConstraint setConstant:10];
    }
    return educationCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *parameters = @{@"id":_experienceArray[indexPath.row][@"id"]};
    
    [SLAPIHelper deleteWorkHistory:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSLog(@"%@",data);
        //删除请求成功之后删除对应的模型的单元格
        [self.experienceArray removeObjectAtIndex:indexPath.row];
        //内容保存成功通知刷新保存之后的结果控制器
        [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteExperience" object:self.experienceArray];
         [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeData" object:nil];
        //修改完成刷新h5页面
//        [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//            
//        }];
        // 刷新
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        } failure:^(SLHttpRequestError *failure) {
        
    }];
    
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    SLNewExperienceViewController *newExperience = [[SLNewExperienceViewController alloc] init];
   
    //newExperience.event =self.event;
    newExperience.companyStr=_experienceArray[indexPath.row][@"company"];
    newExperience.startTimeStr=_experienceArray[indexPath.row][@"start_date"];
    newExperience.endTimeStr=_experienceArray[indexPath.row][@"end_date"];
    newExperience.jobStr=_experienceArray[indexPath.row][@"position"];
    newExperience.locationStr=_experienceArray[indexPath.row][@"location"];
    
    newExperience.isSelectRow=YES;
    newExperience.experienceID=_experienceArray[indexPath.row][@"id"];
    _recordCellIndex=indexPath.row;
    [self.navigationController pushViewController:newExperience animated:YES];
    
}
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}
-(void)getDatas{
    NSDictionary *parameters =  @{@"id":ApplicationDelegate.userId};
    [SLAPIHelper getUsersData:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        NSDictionary *dict = data[@"result"];
        //_experienceArray = dict[@"work_history"];
        NSArray *experienceArray = dict[@"work_history"];
        NSMutableArray *mutableArr=[[NSMutableArray alloc] init];
        [mutableArr addObjectsFromArray:experienceArray];
        self.experienceArray=mutableArr;
        [self.tableView reloadData];
    } failure:nil];
}

- (void)upDateData:(NSNotification *)notif{

    if (notif.userInfo!=nil) {
        [self getDatas];
    }
}


- (void)dealloc{
   
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
