//
//  SLNewExperienceViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLNewExperienceViewController : UIViewController
@property (nonatomic,strong) SLJavascriptBridgeEvent *event;


@property (nonatomic,strong)void(^introduceBlcok)(NSString *str);


@property (nonatomic,strong) NSString  *companyStr;
@property (nonatomic,strong) NSString  *startTimeStr;
@property (nonatomic,strong) NSString  *endTimeStr;
@property (nonatomic,strong) NSString  *jobStr;
@property (nonatomic,strong) NSString  *locationStr;

@property(nonatomic,assign)BOOL isSelectRow;

@property (nonatomic, strong) NSString *experienceID;


@end
