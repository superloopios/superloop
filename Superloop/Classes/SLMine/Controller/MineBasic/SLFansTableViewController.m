//
//  SLFansTableViewController.m
//  Superloop
//
//  Created by 朱宏伟 on 16/5/4.
//  重构  by  xiaowu  16/11/2
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLFansTableViewController.h"
#import "SLFansTableViewCell.h"
#import "SLRefreshHeader.h"
#import "SLRefreshFooter.h"
#import "AppDelegate.h"
#import "SLFans.h"
#import "SLAPIHelper.h"
#import "SLFirstViewController.h"
#import "HomePageViewController.h"
#import "SLNavgationViewController.h"
#import "SLTabBarViewController.h"
#import <MJExtension/MJExtension.h>
#import "SLDefaultView.h"

@interface SLFansTableViewController ()<SLFansTableViewCellDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,assign) NSInteger page;
@property (nonatomic,strong) NSMutableDictionary *parameters;
@property (nonatomic,strong) NSMutableArray *fans;
@property (nonatomic,assign) NSInteger followPage;
@property (nonatomic,strong) UIAlertView *alert;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) SLDefaultView *defaultView;
@property (nonatomic,strong) NSIndexPath *selectedIndexPath;
@property (nonatomic,strong) UIButton *selectedBtn;
@end

@implementation SLFansTableViewController

static NSString *const indtifier = @"indtifier";
- (NSInteger)page{
    if (!_page) {
        _page = 0;
    }
    return _page;
}

-(NSInteger)followPage{
    if (!_followPage) {
        _followPage=0;
    }
    return _followPage;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, ScreenW, ScreenH-64) style:UITableViewStylePlain];
        _tableView.dataSource=self;
        _tableView.delegate=self;
        _tableView.backgroundColor = SLColor(240, 240, 240);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (SLDefaultView *)defaultView{
    if (!_defaultView) {
        _defaultView = [[SLDefaultView alloc] init];
        _defaultView.frame = CGRectMake(0, 64, screen_W, screen_H-64);
    }
    return _defaultView;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLFansTableViewController"];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLFansTableViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.view.backgroundColor=SLColor(240, 240, 240);
    
    _alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
    //登录成功刷新页面
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isLoginSuccessToLoadData) name:@"isLoginSuccessToLoadData" object:nil];
    [self setupSubViews];
    [self setUpRefresh];
    [self setUpNav];
    [self.tableView.mj_header beginRefreshing];
    [self.tableView.mj_footer setHidden:YES];
    
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
    
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    if (self.isfollow) {
        nameLab.text=@"关注";
    } else {
        nameLab.text=@"粉丝";
    }
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)setupSubViews{
    UIView *footView=[[UIView alloc] init];
    self.tableView.tableFooterView=footView;
    [self.tableView registerClass:[SLFansTableViewCell class] forCellReuseIdentifier:indtifier];
}

- (void)setUpRefresh{
    self.tableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
    self.tableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreFans)];
}
-(void)isLoginSuccessToLoadData{
    [self.tableView.mj_header beginRefreshing];
    [self.tableView.mj_footer setHidden:YES];
}
//获取到数据
- (void)getData{
    self.page=0;
    self.followPage=0;
    [self.tableView.mj_footer endRefreshing];
    NSDictionary *parameters = @{@"id":self.userId,@"pager":@10,@"page":@(0)};
    if (self.isfollow) {
        [SLAPIHelper userFollows:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            NSArray *moreFans = [SLFans mj_objectArrayWithKeyValuesArray:data[@"result"]];
            
            if (moreFans.count == 0) {
                [self.tableView.mj_header setHidden:YES];
                [self addNotLoginViews:@"这里什么也没有"];
            }else{
                [self.tableView.mj_header setHidden:NO];
                [_defaultView removeFromSuperview];
            }
            self.fans =(NSMutableArray*)moreFans;
            [self.tableView reloadData];
            if (self.fans.count<10) {
                [self.tableView.mj_header endRefreshing];
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [self.tableView.mj_header endRefreshing];
                [self.tableView.mj_footer resetNoMoreData];
            }
            [self.tableView.mj_footer setHidden:NO];
        } failure:^(SLHttpRequestError *error) {
            [self.tableView.mj_header endRefreshing];
        }];
    } else {
        [SLAPIHelper getFollowers:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            NSArray *moreFans = [SLFans mj_objectArrayWithKeyValuesArray:data[@"result"]];
            self.fans =(NSMutableArray*)moreFans;
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"newFansNumber"]) {
                
                if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"newFansNumber"] integerValue] > 0) {
                    SLTabBarViewController *vc;
                    SLNavgationViewController *navVc;
                    if([[UIApplication sharedApplication].keyWindow.rootViewController isKindOfClass:[SLTabBarViewController class]]){
                        vc = (SLTabBarViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
                        navVc = vc.viewControllers[4];
                        navVc.tabBarItem.badgeValue = nil;
                    }
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"newFansNumber"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"clearNewFans" object:nil];
                }
            }
            
            if ([self.userId integerValue]==[ApplicationDelegate.userId integerValue]) {
                if (moreFans.count == 0) {
                    [self.tableView.mj_header setHidden:YES];
                    [self addNotLoginViews:@"还没有人关注你，活跃起来吧！"];
                }else{
                    [self.tableView.mj_header setHidden:NO];
                    [_defaultView removeFromSuperview];
                }
            }else{
                if (moreFans.count == 0) {
                    [self.tableView.mj_header setHidden:YES];
                    [self addNotLoginViews:@"这里什么也没有"];
                }else{
                    [self.tableView.mj_header setHidden:NO];
                    [_defaultView removeFromSuperview];
                }
            }
            [self.tableView reloadData];
            [self.tableView.mj_header endRefreshing];
            if (self.fans.count<10) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [self.tableView.mj_footer resetNoMoreData];
            }
            [self.tableView.mj_footer setHidden:NO];
        } failure:^(SLHttpRequestError *error) {
            [self.tableView.mj_header endRefreshing];
        }];
    }
}

//下拉加载更多的数据
- (void)loadMoreFans{
    [self.tableView.mj_footer setHidden:NO];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    [parameter setObject:self.userId forKey:@"id"];
    [parameter setObject:@10 forKey:@"pager"];
    if (self.isfollow) {
        self.followPage++;
        [parameter setObject:[NSString stringWithFormat:@"%ld", (long)self.followPage] forKey:@"page"];
        
        [SLAPIHelper userFollows:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            NSArray *moreFans = [SLFans mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [self.fans addObjectsFromArray:moreFans];
            [self.tableView reloadData];
            if ( moreFans.count <10) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [self.tableView.mj_footer endRefreshing];
            }
        } failure:^(SLHttpRequestError *error) {
            [self.tableView.mj_footer endRefreshing];
        }];
    }else{
        self.page ++;
        [parameter setObject:[NSString stringWithFormat:@"%ld", (long)self.page] forKey:@"page"];
        [SLAPIHelper getFollowers:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            NSArray *moreFans = [SLFans mj_objectArrayWithKeyValuesArray:data[@"result"]];
            [self.fans addObjectsFromArray:moreFans];
            [self.tableView reloadData];
            if ( moreFans.count <10) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [self.tableView.mj_footer endRefreshing];
            }
        } failure:^(SLHttpRequestError *error) {
            [self.tableView.mj_footer endRefreshing];
        }];
    }
    
}
- (void)didFcsButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath{
    if (!ApplicationDelegate.Basic) {
        //判断是否是登录成功的状态
        [_alert show];
        return;
    }
    SLFans *fan = self.fans[indexpath.row];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSString *para = [NSString stringWithFormat:@"%@",fan.id];
    [parameter setObject:para forKey:@"user_id"];
    if ([fan.id integerValue]==[ApplicationDelegate.userId integerValue]) {
        [HUDManager showWarningWithText:@"不能点自己"];
        return;
    }
    button.userInteractionEnabled = NO;
    if (button.selected) {
        self.selectedIndexPath = indexpath;
        self.selectedBtn = button;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"确认要取消关注吗？" message:nil delegate:self cancelButtonTitle:@"不了" otherButtonTitles:@"好", nil];
        alert.tag = 100;
        [alert show];
    }else{
        [SLAPIHelper getfollow:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeUserData" object:nil];
            fan.following=[NSString stringWithFormat:@"%d",1];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"isUnfollow" object:self];
            button.userInteractionEnabled = YES;
            button.selected = !button.selected;
            button.titleLabel.font = [UIFont systemFontOfSize:14];
            [HUDManager showWarningWithText:@"关注成功"];
        } failure:^(SLHttpRequestError *error) {
            button.selected = !button.selected;
            if (error.slAPICode == 35 || error.slAPICode == 34) {
               
            }else{
                 [HUDManager showWarningWithText:@"关注失败"];
            }
            button.userInteractionEnabled = YES;
        }];
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fans.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SLFansTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indtifier forIndexPath:indexPath];
    SLFans *fan = self.fans[indexPath.row];
    cell.fanModel = fan;
    cell.delegate = self;
    cell.indexpath = indexPath;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    SLFans *fans = self.fans[indexPath.row];
    if ([[NSString stringWithFormat:@"%@", fans.id] isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        HomePageViewController *myView = [[HomePageViewController alloc] init];
        myView.userId=ApplicationDelegate.userId;
        [self.navigationController pushViewController:myView animated:YES];
    }else{
        HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
        otherPerson.userId = fans.id;
        [self.navigationController pushViewController:otherPerson animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

- (void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            //没有网络的情况下给出提示
            [self addNetWorkViews:@"世界上最遥远的距离就是－－妹有网"];
            [self.tableView.mj_header setHidden:YES];
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            [_defaultView removeFromSuperview];
            [self.tableView.mj_header setHidden:NO];
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            [_defaultView removeFromSuperview];
            [self.tableView.mj_header setHidden:NO];
        }
    }
    
    
}
// 没有网的制空页面
-(void)addNetWorkViews:(NSString *)str{
    [self.view addSubview:self.defaultView];
    self.defaultView.titleStr = str;
}

// 没有搜索到内容
-(void)addNotLoginViews:(NSString *)str{
    [self.view addSubview:self.defaultView];
    self.defaultView.titleStr = str;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100) {
        if (buttonIndex == 1) {
            SLFans *fan = self.fans[self.selectedIndexPath.row];
            NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
            NSString *para = [NSString stringWithFormat:@"%@",fan.id];
            [parameter setObject:para forKey:@"user_id"];
            [SLAPIHelper cancelFollow:parameter success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeUserData" object:nil];
                fan.following=[NSString stringWithFormat:@"%d",0];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"isUnfollow" object:self];
                self.selectedBtn.userInteractionEnabled = YES;
                self.selectedBtn.selected = !self.selectedBtn.selected;
                self.selectedBtn.titleLabel.font = [UIFont systemFontOfSize:17];
                [HUDManager showWarningWithText:@"关注取消"];
            } failure:^(SLHttpRequestError *error) {
                [HUDManager showWarningWithText:@"关注取消失败"];
                self.selectedBtn.userInteractionEnabled = YES;
            }];
        }else{
            self.selectedBtn.userInteractionEnabled = YES;
        }
        
    }else{
        if (buttonIndex==1) {
            SLFirstViewController *vc = [[SLFirstViewController alloc] init];
            SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
            [self.navigationController presentViewController:nav animated:YES completion:nil];
        }
    }
    
}
- (void)dealloc{
    NSLog(@"dealloc");
}

@end
