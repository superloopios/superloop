//
//  SLMeViewController.m
//  Superloop
//
//  Created by xiaowu on 16/8/10.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLMeViewController.h"
#import "SLWalletViewController.h"
#import "SLFansTableViewController.h"
#import "SLTabBarViewController.h"
#import "SLSettingTableViewController.h"
#import "SLPriceViewController.h"
#import "SLCheckPhotoViewController.h"
#import "SLMyCommentViewController.h"
#import "SLCollectionTopicViewController.h"
#import "SLUserAccountViewController.h"
#import "SLSendEvaluationController.h"
#import "SLFirstViewController.h"
#import "HomePageViewController.h"
#import "SLNavgationViewController.h"
#import "SLUpdateAvatarAndNicknameViewController.h"

#import "AppDelegate.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+Extension.h"
#import "SLAPIHelper.h"
#import "NSString+Utils.h"

#import "SLSettingModel.h"

#import "SLSettingCell.h"
#import "SLActivityIndicatorView.h"

#define SEPATORCOLOR  UIColorFromRGB(0xebddd5)


@interface SLMeViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>

@property (nonatomic, strong) UITableView          *   tableView;
@property (nonatomic, strong) UIView               *   headerView;
@property (nonatomic, strong) UIImageView          *   headerImageView;
@property (nonatomic, strong) UIImageView          *   erWeiMaIv;
@property (nonatomic, strong) UILabel              *   nickNameLabel;
@property (nonatomic, strong) UILabel              *   positionLabel;
@property (nonatomic, strong) UIImageView          *   roleIv;              //特权
@property (nonatomic, strong) UIImageView          *   enterIv;
@property (nonatomic, strong) NSDictionary         *   userInfo;
@property (nonatomic, strong) NSArray              *   sectionArr;
@property (nonatomic, strong) UIView               *   backView;
@property (nonatomic, strong) UILabel              *   loginLabel;
@property (nonatomic,   copy) NSString             *   oldAvatarString;
@property (nonatomic, strong) UIButton             *   followBtn;           //关注数量btn
@property (nonatomic, strong) UIButton             *   fanBtn;
@property (nonatomic, strong) UIView               *   sepatorView;         //粉丝数量btn
@property (nonatomic, strong) UILabel              *   tipLab;              //新粉丝数量提醒
@property (nonatomic, assign) BOOL                     hasLogin;
@property (nonatomic, assign) BOOL                     hasNetWork;
@property (nonatomic, assign) BOOL                     isLoadIngData;
@property (nonatomic, assign) double                   endtime;
@property (nonatomic, strong) SLActivityIndicatorView   *   activeView;
@property (nonatomic, strong) UIView               *   firstsepatorView;//粉丝下的view

@end

@implementation SLMeViewController

- (UIView *)firstsepatorView{
    if (!_firstsepatorView) {
        _firstsepatorView = [[UIView alloc] initWithFrame:CGRectMake(25, 59, screen_W-50, 0.5)];
        _firstsepatorView.backgroundColor = SLMainColor;
    }
    return _firstsepatorView;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLMeViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLMeViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addHeaderView];
    
    [self setUpNav];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.tableHeaderView = self.headerView;
    
    [self headerViewAddTempInfo];
    [self addTableList];
    [self addNoti];
    if (ApplicationDelegate.userId) {
        [self getUserData];
    }
    [self getNetWorkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetWorkStatus) name:@"netWorkChange" object:nil];
    
}
- (UILabel *)loginLabel{
    if (!_loginLabel) {
        _loginLabel = [[UILabel alloc] init];
        _loginLabel.textColor = [UIColor whiteColor];
        _loginLabel.text = @"点击登录";
        _loginLabel.font = [UIFont boldSystemFontOfSize:20];
        [self.headerView addSubview:_loginLabel];
        [_loginLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.headerImageView.mas_right).offset(17);
            make.centerY.equalTo(self.headerView);
        }];
    }
    return _loginLabel;
}


- (UILabel *)tipLab{
    if (!_tipLab) {
        _tipLab = [[UILabel alloc] init];
        _tipLab.font = [UIFont systemFontOfSize:8];
        _tipLab.backgroundColor = [UIColor redColor];
        _tipLab.textColor = [UIColor whiteColor];
        _tipLab.textAlignment = NSTextAlignmentCenter;
        _tipLab.clipsToBounds = YES;
    }
    return _tipLab;
}
- (UIImageView *)erWeiMaIv{
    if (!_erWeiMaIv) {
        _erWeiMaIv = [[UIImageView alloc] init];
        _erWeiMaIv.image = [UIImage imageNamed:@"redErCode"];
        _erWeiMaIv.contentMode = UIViewContentModeCenter;
        [self.headerView addSubview:_erWeiMaIv];
    }
    return _erWeiMaIv;
}
- (UIImageView *)enterIv{
    if (!_enterIv) {
        _enterIv = [[UIImageView alloc] init];
        _enterIv.image = [UIImage imageNamed:@"Down-Arrow-outline"];
        _enterIv.contentMode = UIViewContentModeCenter;
        [self.headerView addSubview:_enterIv];
    }
    return _enterIv;
}
- (UIImageView *)roleIv{
    if (!_roleIv) {
        _roleIv = [[UIImageView alloc] init];
        _roleIv.image = [UIImage imageNamed:@"crown"];
        _roleIv.contentMode = UIViewContentModeCenter;
        [self.headerView addSubview:_roleIv];
    }
    return _roleIv;
}

- (UILabel *)positionLabel{
    if (!_positionLabel) {
        _positionLabel = [[UILabel alloc] init];
        _positionLabel.textColor = SLColor(88, 88, 88);
        _positionLabel.x = self.headerImageView.x + self.headerImageView.width + 13;
        _positionLabel.y = 65;
        _positionLabel.font = [UIFont systemFontOfSize:14];
        [self.headerView addSubview:_positionLabel];
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginClick)];
        [_positionLabel addGestureRecognizer:ges];
    }
    return _positionLabel;
}

- (UILabel *)nickNameLabel{
    if (!_nickNameLabel) {
        _nickNameLabel = [[UILabel alloc] init];
        _nickNameLabel.textColor = SLBlackTitleColor;
        
        _nickNameLabel.x = self.headerImageView.x + self.headerImageView.width + 13;
        _nickNameLabel.y = 35;
        _nickNameLabel.font = [UIFont boldSystemFontOfSize:20];
        [self.headerView addSubview:_nickNameLabel];
    }
    return _nickNameLabel;
}
- (UIImageView *)headerImageView{
    if (!_headerImageView) {
        _headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(25, 20, 73, 73)];
//        _headerImageView.layer.cornerRadius = 5;
//        _headerImageView.clipsToBounds = YES;
        _headerImageView.image = [UIImage imageNamed:@"newPersonAvatar"];
        if ([[ApplicationDelegate.userInformation allKeys] containsObject:@"avatar"]) {
            [_headerImageView sd_setImageWithURL:[NSURL URLWithString:ApplicationDelegate.userInformation[@"avatar"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"] options:SDWebImageRetryFailed];
        }
        [self.headerView addSubview:_headerImageView];
    }
    return _headerImageView;
}
- (UIView *)headerView{
    if (!_headerView) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_W, 115)];
        _headerView.backgroundColor = [UIColor whiteColor];
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginClick)];
        [_headerView addGestureRecognizer:ges];
        UIView *sepatorView = [[UIView alloc] initWithFrame:CGRectMake(25, 114, screen_W-50, 0.5)];
        sepatorView.backgroundColor = SEPATORCOLOR;
        [_headerView addSubview:sepatorView];
        
    }
    return _headerView;
}
- (UIButton *)fanBtn{
    if (!_fanBtn) {
        _fanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _fanBtn.frame = CGRectMake(screen_W/2, 0, screen_W/2, 60);
        _fanBtn.backgroundColor = [UIColor whiteColor];
        [_fanBtn setTitleColor:SLBlackTitleColor forState:UIControlStateNormal];
        _fanBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_fanBtn setTitle:@"粉丝" forState:UIControlStateNormal];
        [_fanBtn addTarget:self action:@selector(fanBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _fanBtn;
}
- (UIButton *)followBtn{
    if (!_followBtn) {
        _followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _followBtn.frame = CGRectMake(0, 0, screen_W/2, 60);
        _followBtn.backgroundColor = [UIColor whiteColor];
        [_followBtn setTitleColor:SLBlackTitleColor forState:UIControlStateNormal];
        _followBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_followBtn setTitle:@"关注" forState:UIControlStateNormal];
        [_followBtn addTarget:self action:@selector(followBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _followBtn;
}
- (UIView *)sepatorView{
    if (!_sepatorView) {
        _sepatorView = [[UIView alloc] init];
        _sepatorView.frame = CGRectMake(0, 18, 0.5, 24);
        _sepatorView.backgroundColor = SEPATORCOLOR;
    }
    return _sepatorView;
}
- (void)loginClick{
    if (!ApplicationDelegate.userId) {
        [self loginSystem];
    }else{
        if (self.hasNetWork) {
            HomePageViewController *meSec = [[HomePageViewController alloc] init];
            meSec.userId = ApplicationDelegate.userId;
            [self.navigationController pushViewController:meSec animated:YES];
        }else{
            kShowToast(@"网络出错,请检查网络状况");
        }
        
    }
}
- (void)loginSystem{
    SLFirstViewController *vc=[[SLFirstViewController alloc] init];
    SLNavgationViewController *nav=[[SLNavgationViewController alloc] initWithRootViewController:vc];
//    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, ScreenW, ScreenH-113) style:UITableViewStylePlain];
        [_tableView registerClass:[SLSettingCell class] forCellReuseIdentifier:@"settingCell"];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = SLColor(244, 244, 244);
        [self.view addSubview:_tableView];
        _tableView.tableFooterView = [[UIView alloc] init];
    }
    return _tableView;
}



- (void)addHeaderView{
    [self.headerView addSubview:self.headerImageView];
    [self.headerView addSubview:self.loginLabel];
    self.positionLabel.text = @"点此查看或编辑个人资料";
    [self.positionLabel sizeToFit];
    if (ApplicationDelegate.userId) {
        self.loginLabel.hidden = YES;
        self.nickNameLabel.hidden = NO;
    }
    self.erWeiMaIv.frame = CGRectMake(screen_W - 48, (self.headerView.height-20)/2, 20, 20);
    
    self.enterIv.frame = CGRectMake(screen_W - 22, (self.headerView.height-10)/2, 10, 10);
    self.roleIv.hidden = YES;
}
- (void)addNoti{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearInfo) name:@"exitSuperLoop" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserData) name:@"getUserData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearInfo) name:@"ClearMessage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearInfo) name:@"jumptoHome" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserData) name:@"amountChange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserData) name:@"isLoginSuccessToLoadData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserData) name:@"chargeSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserData) name:@"changeAvatar" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserData) name:@"isUnfollow" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hasNewFans) name:@"hasNewFans" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearTipLabNum) name:@"clearNewFans" object:nil];
}

- (void)clearInfo{
    NSArray *firstarr = self.sectionArr[1];
    SLSettingModel *ammountModel = firstarr[0];
    ammountModel.subTitle = @"";
    NSArray *arr = self.sectionArr[2];
    SLSettingModel *verModel = arr[0];
    verModel.subTitle = @"";
    SLSettingModel *priceModel = arr[1];
    priceModel.subTitle = @"";
    [self.tableView reloadData];
    self.headerImageView.image = [UIImage imageNamed:@"newPersonAvatar"];
    self.loginLabel.hidden = NO;
    self.nickNameLabel.hidden = YES;
    self.roleIv.hidden = YES;
    self.hasLogin = NO;
    self.oldAvatarString = @"";
    [self.fanBtn setTitle:[NSString stringWithFormat:@"粉丝"] forState:UIControlStateNormal];
    [self.followBtn setTitle:[NSString stringWithFormat:@"关注"] forState:UIControlStateNormal];
    self.tipLab.hidden = YES;
    
}
- (void)addTableList{
    NSArray *dictArr0 = @[@{@"imageName":@"",@"title":@"",@"subTitle":@"",@"hasArrow":@NO}];
    NSArray *dictArr1 = @[@{@"imageName":@"",@"title":@"我的钱包",@"subTitle":@"",@"hasArrow":@YES},
                          @{@"imageName":@"",@"title":@"我的收藏",@"subTitle":@"",@"hasArrow":@YES},
                          @{@"imageName":@"",@"title":@"话题评论",@"subTitle":@"",@"hasArrow":@YES},
                          @{@"imageName":@"",@"title":@"送出的评价",@"subTitle":@"",@"hasArrow":@YES,@"color":SLMainColor}
                        ];
    
    NSArray *dictArr2 = @[@{@"imageName":@"",@"title":@"审核认证",@"subTitle":@"",@"hasArrow":@YES},
                          @{@"imageName":@"",@"title":@"设置通话收费金额",@"subTitle":@"",@"hasArrow":@YES,@"color":SLMainColor}
                          ];
    NSArray *dictArr3 = @[@{@"imageName":@"",@"title":@"设置",@"subTitle":@"",@"hasArrow":@YES},
                          @{@"imageName":@"",@"title":@"联系我们",@"subTitle":@"(工作日10:00-19:00)",@"hasArrow":@NO,@"color":SLMainColor}
                          ];
    NSArray *dictArr4 = @[@{@"imageName":@"",@"title":@"",@"subTitle":@"",@"hasArrow":@NO},
                          ];
    NSArray *arr0 = [SLSettingModel mj_objectArrayWithKeyValuesArray:dictArr0];
    NSArray *arr1 = [SLSettingModel mj_objectArrayWithKeyValuesArray:dictArr1];
    NSArray *arr2 = [SLSettingModel mj_objectArrayWithKeyValuesArray:dictArr2];
    NSArray *arr3 = [SLSettingModel mj_objectArrayWithKeyValuesArray:dictArr3];
    NSArray *arr4 = [SLSettingModel mj_objectArrayWithKeyValuesArray:dictArr4];
    self.sectionArr = @[arr0,arr1,arr2,arr3,arr4];
    [self.tableView reloadData];
    
}
- (void)getUserData{
    if (!ApplicationDelegate.userId) {
        return;
    }
    self.isLoadIngData = YES;
    [self.activeView startAnimating];
    
    [SLAPIHelper getUsersData:@{@"id":ApplicationDelegate.userId} success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        NSLog(@"%@",data);
        self.userInfo = data[@"result"];
        self.isLoadIngData = NO;
        self.hasLogin  = YES;
        [self headerViewAddInfo];
        [self reloadTableview];
        [self.activeView stopAnimating];
        self.endtime = [[NSDate date] timeIntervalSince1970];
    } failure:^(SLHttpRequestError *failure) {
        [self.activeView stopAnimating];
        self.endtime = [[NSDate date] timeIntervalSince1970];
        self.isLoadIngData = NO;
    }];
}
- (void)reloadTableview{
    
    NSNumber *fanNum = (NSNumber *)self.userInfo[@"followers"];
    [self.fanBtn setTitle:[NSString stringWithFormat:@"粉丝  %ld",(long)[fanNum integerValue]] forState:UIControlStateNormal];
    NSNumber *followNum = (NSNumber *)self.userInfo[@"follows"];
    [self.followBtn setTitle:[NSString stringWithFormat:@"关注  %ld",(long)[followNum integerValue]] forState:UIControlStateNormal];
    NSArray *firstarr = self.sectionArr[1];
    SLSettingModel *accountmodel = firstarr[0];
//    NSLog(@"%@",self.userInfo);
    NSNumber *account = self.userInfo[@"amount"];
    accountmodel.subTitle = [NSString stringWithFormat:@"￥%.2f",[account doubleValue]];
    
    NSArray *arr = self.sectionArr[2];
    SLSettingModel *model = arr[0];
    NSNumber *verNum = self.userInfo[@"verification_status"];
    if ([verNum isEqual:@0]) {
        model.subTitle = @"未认证";
    }else if ([verNum isEqual:@1]) {
        model.subTitle = @"认证中";
    }else if ([verNum isEqual:@2]) {
        model.subTitle = @"已认证";
    }
    
    SLSettingModel *model1 = arr[1];
    NSNumber *price = self.userInfo[@"price"];
    model1.subTitle = [NSString stringWithFormat:@"%@元/分",price];
    [self.tableView reloadData];
    [self addTipLabel];
}
- (void)headerViewAddTempInfo{
    if (ApplicationDelegate.userId) {
        self.loginLabel.hidden = YES;
        if ([[ApplicationDelegate.userInformation allKeys] containsObject:@"nickname"]) {
            NSString *nickName = ApplicationDelegate.userInformation[@"nickname"];
            CGSize nicknameSize = [nickName sizeWithFont:[UIFont boldSystemFontOfSize:20] maxSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
            CGFloat width = nicknameSize.width;
            if (width > screen_W-188-30) {
                width = screen_W-188-30;
            }
            self.nickNameLabel.hidden = NO;
            self.nickNameLabel.height = nicknameSize.height;
            self.nickNameLabel.width = width;
            self.nickNameLabel.text = nickName;
        }
        
//        if ([[ApplicationDelegate.userInformation allKeys] containsObject:@"work_position"]) {
//            NSString *positionName = ApplicationDelegate.userInformation[@"work_position"];
//            CGSize work_positionSize = [positionName sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
//            CGFloat work_positionWidth = work_positionSize.width;
//            if (work_positionWidth > screen_W-188) {
//                work_positionWidth = screen_W-188;
//            }
//            
//            self.positionLabel.y = 65;
//            self.positionLabel.hidden = NO;
//            self.positionLabel.height = work_positionSize.height;
//            self.positionLabel.width = work_positionWidth;
//            self.positionLabel.text = positionName;
//        }
    }
}
- (void)headerViewAddInfo{
    
    self.loginLabel.hidden = YES;
    if (![self.userInfo[@"avatar"] isEqualToString:self.oldAvatarString]) {
        [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:self.userInfo[@"avatar"]] placeholderImage:[UIImage imageNamed:@"newPersonAvatar"]];
        self.oldAvatarString = self.userInfo[@"avatar"];
    }
    NSString *nickName = self.userInfo[@"nickname"];
    CGSize nicknameSize = [nickName sizeWithFont:[UIFont boldSystemFontOfSize:20] maxSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
    CGFloat width = nicknameSize.width;
    if (width > screen_W-188-30) {
        width = screen_W-188-30;
    }
    self.nickNameLabel.hidden = NO;
    self.nickNameLabel.height = nicknameSize.height;
    self.nickNameLabel.width = width;
    self.nickNameLabel.text = nickName;
    
//    NSString *positionName = self.userInfo[@"work_position"];
//    CGSize work_positionSize = [positionName sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
//    CGFloat work_positionWidth = work_positionSize.width;
//    if (work_positionWidth > screen_W-188) {
//        work_positionWidth = screen_W-188;
//    }
//    self.positionLabel.y = 65;
//    self.positionLabel.hidden = NO;
//    self.positionLabel.height = work_positionSize.height;
//    self.positionLabel.width = work_positionWidth;
//    self.positionLabel.text = positionName;
    self.roleIv.frame = CGRectMake(self.nickNameLabel.x+width+12, self.nickNameLabel.y, nicknameSize.height, nicknameSize.height);
    if ([[self.userInfo[@"role_type"] stringValue] isEqualToString:@"1"]) {
        self.roleIv.hidden = NO;
    }else{
        self.roleIv.hidden = YES;
    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *arr = self.sectionArr[indexPath.section];
    SLSettingModel *model = arr[indexPath.row];
    
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mineFollowCell"];
        [cell addSubview:self.fanBtn];
        [cell addSubview:self.followBtn];
        [self.fanBtn addSubview:self.sepatorView];
        [cell addSubview:self.firstsepatorView];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"settingCell"];
        SLSettingCell *normolCell = (SLSettingCell *)cell;
        normolCell.model = model;
        if (indexPath.section == 4) {
            cell.userInteractionEnabled = NO;
        }else{
            cell.userInteractionEnabled = YES;
        }
    }
    cell.backgroundColor = [UIColor whiteColor];
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *arr = self.sectionArr[section];
    return arr.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sectionArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 1 && indexPath.section == 3) {
        NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"4009-010-616"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        return;
    }
    if (indexPath.row == 0 && indexPath.section == 3){
        SLSettingTableViewController *settingVc = [[SLSettingTableViewController alloc] init];
        settingVc.changeNav = ^(NSString *test){
            [self.tabBarController setSelectedIndex:0];
        };
        settingVc.infoDict = self.userInfo;
        [self.navigationController pushViewController:settingVc animated:YES];
        return;
    }
    if (!self.hasNetWork) {
        kShowToast(@"网络出错,请检查网络状况");
        return;
    }
    if (!ApplicationDelegate.userId) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
//        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [alert show];
        return;
    }
    SLSettingModel *model = self.sectionArr[indexPath.section][indexPath.row];
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
//            SLUserAccountViewController *userAccount = [[SLUserAccountViewController alloc] init];
//            userAccount.userData = self.userInfo;
//            [self.navigationController pushViewController:userAccount animated:YES];
            SLWalletViewController *userWallet = [[SLWalletViewController alloc] init];
            userWallet.userData = self.userInfo;
            [self.navigationController pushViewController:userWallet animated:YES];
        }else if (indexPath.row == 1) {
            SLCollectionTopicViewController *collection = [[SLCollectionTopicViewController alloc] init];
            collection.isFromMeController = YES;
            [self.navigationController pushViewController:collection animated:YES];
        }else if (indexPath.row == 2) {
            SLMyCommentViewController *myComment = [[SLMyCommentViewController alloc] init];
            [self.navigationController pushViewController:myComment animated:YES];
            
        }else if (indexPath.row == 3) {
            SLSendEvaluationController *callComment = [[SLSendEvaluationController alloc] init];
            callComment.credit = self.userInfo[@"credit_level"];
            [self.navigationController pushViewController:callComment animated:YES];
        }
    }else if(indexPath.section == 2){
        if (indexPath.row == 0) {
            if ([model.subTitle isEqualToString:@"未认证"]) {
                [self initAlertView];
            }
        }else if (indexPath.row == 1){
            SLPriceViewController *price = [[SLPriceViewController alloc] init];
            price.userData = self.userInfo;
            __weak typeof(self) weakSelf = self;
            price.changePrice = ^(NSString *price){
                __strong typeof(weakSelf) strongSelf = weakSelf;
                model.subTitle = price;
                [strongSelf.tableView reloadData];
            };
            [self.navigationController pushViewController:price animated:YES];
        }
    }
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    UILabel *nameLab=[[UILabel alloc] init];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"我";
    
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    [nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(navView);
        make.centerY.equalTo(navView).offset(10);
    }];
    self.activeView = [[SLActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activeView.hidesWhenStopped = YES;
//    [_activeView startAnimating];
    [navView addSubview:self.activeView];
    [self.activeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(nameLab);
        make.right.equalTo(nameLab.mas_left).offset(-5);
    }];
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    
}
-(void)initAlertView{
    _backView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH)];
    _backView.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    _backView.hidden=NO;
    [[UIApplication sharedApplication].keyWindow addSubview:_backView];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftBtnClick)];
    _backView.userInteractionEnabled=YES;
    [_backView addGestureRecognizer:tap];
    
    UIView *littleBackView=[[UIView alloc] initWithFrame:CGRectMake((ScreenW-260)/2, (ScreenH-262)/2, 260, 262)];
    littleBackView.backgroundColor=SLColor(232, 232, 232);
    [_backView addSubview:littleBackView];
    
    UIView *headView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 260, 36)];
    headView.backgroundColor=SLColor(241, 241, 241);
    [littleBackView addSubview:headView];
    
    UIView *headVcutView=[[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(headView.frame)-0.5, 260, 0.5)];
    headVcutView.backgroundColor=[UIColor lightGrayColor];
    [headView addSubview:headVcutView];
    
    UILabel *alertLab=[[UILabel alloc] initWithFrame:CGRectMake(0, 8, 260, 20)];
    alertLab.textAlignment=NSTextAlignmentCenter;
    alertLab.font=[UIFont systemFontOfSize:15];
    alertLab.text=@"温馨提示";
    alertLab.textColor=[UIColor blackColor];
    [headView addSubview:alertLab];
    
    NSArray *optionArr=@[@"清晰的个人头像",@"完整的简介和标签",@"关注用户≥10位",@"粉丝数量≥10位",@"与两个认证用户互相关注"];
    for (int i=0; i<optionArr.count; i++) {
        UIImageView *pointImgView=[[UIImageView alloc] initWithFrame:CGRectMake(55, 36+9+4+(4+21)*i, 4, 4)];
        pointImgView.image=[UIImage imageNamed:@"certificationPoint"];
        [littleBackView addSubview:pointImgView];
        
        UILabel *optionLab=[[UILabel alloc] initWithFrame:CGRectMake(66, 36+9+(13+12)*i, 260-65, 13)];
        optionLab.textAlignment=NSTextAlignmentLeft;
        optionLab.text=optionArr[i];
        optionLab.font=[UIFont systemFontOfSize:13];
        [littleBackView addSubview:optionLab];
    }
    UILabel *redLab=[[UILabel alloc] initWithFrame:CGRectMake(30, 175, 260-60, 30)];
    redLab.text=@"请确保您已具备上述条件,再进行认证\n否则我们将不予通过";
    redLab.numberOfLines=2;
    redLab.textAlignment=NSTextAlignmentCenter;
    redLab.textColor=SLColor(198, 0, 0);
    redLab.font=[UIFont systemFontOfSize:12];
    [littleBackView addSubview:redLab];
    
    UIView *btnVcutView=[[UIView alloc] initWithFrame:CGRectMake(0, 262-40.5, 260, 0.5)];
    btnVcutView.backgroundColor=[UIColor lightGrayColor];
    [littleBackView addSubview:btnVcutView];
    
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 262-40, 260/2, 40);
    leftBtn.backgroundColor=SLColor(241, 241, 241);
    leftBtn.titleLabel.font=[UIFont systemFontOfSize:14];
    [leftBtn setTitle:@"暂不认证" forState:UIControlStateNormal];
    [leftBtn setTitleColor:SLColor(198, 0, 0) forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [littleBackView addSubview:leftBtn];
    UIButton *rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(260/2, 262-40, 260/2, 40);
    rightBtn.backgroundColor=SLColor(241, 241, 241);
    rightBtn.titleLabel.font=[UIFont systemFontOfSize:14];
    [rightBtn setTitle:@"去认证" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [littleBackView addSubview:rightBtn];
    
    UIView *hView=[[UIView alloc] initWithFrame:CGRectMake(260/2-0.5, 262-30, 0.5, 20)];
    hView.backgroundColor=[UIColor lightGrayColor];
    [littleBackView addSubview:hView];
}
-(void)leftBtnClick{
    _backView.hidden=YES;
}
-(void)rightBtnClick{
    _backView.hidden=YES;
    SLCheckPhotoViewController *check = [[SLCheckPhotoViewController alloc] init];
    __weak typeof(self) weakSelf = self;
    check.changeVer = ^(NSString *text){
        SLSettingModel *model = self.sectionArr[2][0];
        __strong typeof(weakSelf) strongSelf = weakSelf;
        model.subTitle = text;
        [strongSelf.tableView reloadData];
    };
    [self.navigationController pushViewController:check animated:YES];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self loginSystem];
    }
}
-(void)getNetWorkStatus{
    NSString *netWorkStaus = ApplicationDelegate.netWorkStatus;
    if (netWorkStaus) {
        if ([netWorkStaus isEqualToString:@"noNetWork"]) {
            self.hasNetWork=NO;
        }else if ([netWorkStaus isEqualToString:@"wifi"]) {
            self.hasNetWork=YES;
        }else if ([netWorkStaus isEqualToString:@"wan"]||[netWorkStaus isEqualToString:@"Unknown"]) {
            self.hasNetWork=YES;
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
//    NSLog(@"%.2f",scrollView.contentOffset.x);
    if (scrollView.contentOffset.y < -50) {
        if (!self.isLoadIngData) {
            double nowtime = [[NSDate date] timeIntervalSince1970];
            if (nowtime-self.endtime>3) {
                 [self getUserData];
            }
        }
    }
}
- (void)fanBtnClick{
    if (!ApplicationDelegate.userId) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
        //        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [alert show];
        return;
    }
    SLFansTableViewController *fansVc = [[SLFansTableViewController alloc] init];
    fansVc.userId = ApplicationDelegate.userId;
    [self.navigationController pushViewController:fansVc animated:YES];
}

- (void)followBtnClick{
    if (!ApplicationDelegate.userId) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
        //        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [alert show];
        return;
    }
    SLFansTableViewController *fansVc = [[SLFansTableViewController alloc] init];
    fansVc.userId = ApplicationDelegate.userId;
    fansVc.isfollow = @"guanzhu";
    [self.navigationController pushViewController:fansVc animated:YES];
}
- (void)hasNewFans{
    [self getUserData];
    [self addTipLabel];
}
- (void)addTipLabel{
    
//    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"newFansNumber"])
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"newFansNumber"]) {
        [self.fanBtn addSubview:self.tipLab];
        self.tipLab.hidden = YES;
        NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"newFansNumber"];
        if ([str integerValue] > 0) {
            self.tipLab.hidden = NO;
            if ([str integerValue] > 99) {
                str = @"99+";
            }
            self.tipLab.text = str;
            CGFloat width =  [str boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:8]} context:nil].size.width+5;
            if (width<13) {
                width = 13;
            }
            
            self.tipLab.layer.cornerRadius = width/2.0;
            
            [self.tipLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.fanBtn.titleLabel.mas_right);
                make.bottom.equalTo(self.fanBtn.titleLabel.mas_top).offset(5);
                make.height.width.mas_equalTo(width);
            }];
        }
        
    }
    
}
- (void)clearTipLabNum{
    self.tipLab.hidden = YES;
}
@end
