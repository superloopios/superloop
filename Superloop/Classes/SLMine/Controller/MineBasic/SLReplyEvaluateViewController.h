//
//  SLReplyEvaluateViewController.h
//  Superloop
//
//  Created by WangS on 16/10/13.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLReplyEvaluateViewController : UIViewController
@property (nonatomic,copy) NSString * appraisal_id;
@property (nonatomic,copy) void (^replySuccess)(BOOL isSuccess);
@property (nonatomic, assign) BOOL isPhoneOrMsg;//判断是电话还是付费消息

@end
