//
//  SLSuperNumberViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/27.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLSuperNumberViewController.h"
#import <Masonry.h>
#import "AppDelegate.h"
#import <AFNetworking.h>
#import "SLAPIHelper.h"
@interface SLSuperNumberViewController ()<UIAlertViewDelegate>
@property (nonatomic, strong)UITextField *superTextField;
@property (nonatomic,copy)NSString *msg;
@property (nonatomic,assign)BOOL isChange;
@property (nonatomic,strong)UIAlertView *alert;
@end

@implementation SLSuperNumberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"修改超级号";

    
    [self setUpUI];
}

- (void)setUpUI
{
    self.view.backgroundColor = SLColor(255, 255, 255);
    UIView *mainView = [[UIView alloc] init];
    mainView.frame = CGRectMake(0, 15, self.view.width, 50);
    mainView.backgroundColor = SLColor(238, 238, 238);
    [self.view addSubview:mainView];
    
    
    
    UITextField *nickTextField = [[UITextField alloc] init];
    nickTextField.placeholder = @"请输入超级号，可由数字、字母及下划线组成";
    //根据需求需要在这里显示原有的昵称
    //    nickTextField.text = self.nickName;
    nickTextField.frame = CGRectMake(20, 5, self.view.frame.size.width, 44);
    nickTextField.font = [UIFont systemFontOfSize:13];
    self.superTextField = nickTextField;
    self.superTextField.backgroundColor = SLColor(238, 238, 238);
    
    [mainView addSubview:nickTextField];
    
    UILabel *nickLabel = [[UILabel alloc] init];
    nickLabel.frame = CGRectMake(70,mainView.frame.origin.y + 50, self.view.frame.size.width-140, 20);
    //    nickLabel.text = @"*请勿使用空格或特殊字符";
    NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:@"*超级号只能更改一次"];
    NSRange redRange = NSMakeRange([[noteStr string] rangeOfString:@"*"].location, [[noteStr string] rangeOfString:@"*"].length);
    [noteStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:redRange];
    [nickLabel setAttributedText:noteStr];
    nickLabel.font = [UIFont systemFontOfSize:12];
    nickLabel.textAlignment = NSTextAlignmentCenter;
    
    
    [self.view addSubview:nickLabel];
    UIImageView *cutImageView = [[UIImageView alloc] init];
    cutImageView.backgroundColor = SLColor(183, 183, 183);
    cutImageView.frame = CGRectMake(0, mainView.frame.size.height -2, self.view.frame.size.width, 1);
    [mainView addSubview:cutImageView];
    UIImageView *cutImageView1 = [[UIImageView alloc] init];
    cutImageView1.backgroundColor = SLColor(183, 183, 183);
    cutImageView1.frame = CGRectMake(0, 0 , self.view.frame.size.width, 1);
    [mainView addSubview:cutImageView1];
    UIButton *ensureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    ensureBtn.frame =CGRectMake(75, nickLabel.frame.origin.y +70, self.view.frame.size.width-150, 45);
    ensureBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    ensureBtn.layer.cornerRadius = 5;
    ensureBtn.layer.masksToBounds = YES;
    [ensureBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [ensureBtn setTitle:@"确认修改" forState:UIControlStateNormal];
    ensureBtn.backgroundColor = SLColor(238, 238, 238);
    [ensureBtn addTarget:self action:@selector(ensureChangeSuperNum) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:ensureBtn];

}
- (void)ensureChangeSuperNum
{
    if ([self isVerfier] == NO) {
    
        return;
    }
    
    if ([self isChineseCharacterAndLettersAndNumbersAndUnderScore:self.superTextField.text]) {
       
        _msg=@"超级号只能修改一次,请确认是否需要修改";
        self.isChange=YES;
    }else{
        _msg=@"超级号只能由数字、字母及下划线组成,不能为手机号";
        self.isChange=NO;
    }
    if (self.isChange) {
        _alert=[[UIAlertView alloc] initWithTitle:@"提示" message:_msg delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"好", nil];
    }else{
    _alert=[[UIAlertView alloc] initWithTitle:@"提示" message:_msg delegate:self cancelButtonTitle:@"好" otherButtonTitles:nil, nil];
    }
    
    [_alert show];
   
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Basic %@",ApplicationDelegate.Basic] forHTTPHeaderField:@"Authorization"];
//    NSDictionary *parameters = @{@"username":self.superTextField.text};
//    
//    if ([self isVerfier]) {
//       
//        [manager POST:[URL_ROOT_PATIENT stringByAppendingString:@"/users"]  parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
//            NSLog(@"%@",responseObject);
//            
//            NSUserDefaults *stand = [NSUserDefaults standardUserDefaults];
//            [stand setObject:@"1" forKey:@"name"];
//            [stand synchronize];
//            
//        } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
//        
//        }];
//        
//        if (_superNumberBlcok) {
//            _superNumberBlcok(self.superTextField.text);
//        }
//
//    }
//    
//    //修改完成刷新h5页面
//    [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//        
//    }];
//    [self.navigationController popViewControllerAnimated:YES];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        
        NSDictionary *parameters = @{@"username":self.superTextField.text};
        
       
        [SLAPIHelper resetUsers:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            NSUserDefaults *stand = [NSUserDefaults standardUserDefaults];
            [stand setObject:@"1" forKey:@"name"];
            [stand synchronize];
            if (_superNumberBlcok) {
                _superNumberBlcok(self.superTextField.text);
            }

//            //修改完成刷新h5页面
//            [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//                
//            }];
            [self.navigationController popViewControllerAnimated:YES];
        
        } failure:nil];
        
        
        
    }

}


/*
 *  判断字符串  字母 数字 以及下划线,以及不能是手机号
 *  中文字符((a >= 0x4e00 && a <= 0x9fa6))
 */
-(BOOL)isChineseCharacterAndLettersAndNumbersAndUnderScore:(NSString *)string
{
    //手机号正则
    NSString *cellPhoneStr=@"^((13[0-9])|(15[0-9])|(18[0-9])|(17[0-9]))\\d{8}$";
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cellPhoneStr];
    
    if ([pred evaluateWithObject:string]) {
        return NO;
    }else{
    NSUInteger len = [string length];
    for(int i=0;i<len;i++)
    {
        unichar a=[string characterAtIndex:i];
        if(!((isalpha(a))
             ||(isalnum(a))
             ||((a=='_'))
             ||(a=='@')
             ||(a=='.')
             ||(a=='-')
             ))
            return NO;
    }
    return YES;
  }
}




#pragma mark - 设置超级号只能限制输入一次
- (BOOL)isVerfier{
    
//    NSUserDefaults *stand = [NSUserDefaults standardUserDefaults];
//   NSString *str = [stand objectForKey:@"name"];
    
    if (self.superTextField.text.length < 4 || self.superTextField.text.length > 16) {

       [HUDManager showWarningWithText:@"请输入4-16位的超级号"];
        return NO;
    }
    
    
    return YES;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}
@end