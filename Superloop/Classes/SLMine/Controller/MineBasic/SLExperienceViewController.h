//
//  SLExperienceViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLBaseTableViewController.h"

@interface SLExperienceViewController : UIViewController
@property (nonatomic ,strong) void(^experienceBlcok)(NSString *str);
@property (nonatomic, strong) NSMutableArray *experienceArray;
@property (nonatomic,strong) SLJavascriptBridgeEvent *event;;
@end
