//
//  SLRecordVideoViewController.h
//  Superloop
//
//  Created by WangS on 16/9/5.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "UIView+Tools.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface SLRecordVideoViewController : UIViewController

@property (nonatomic,copy)void (^uploadSuccess)(BOOL isSuccess,NSURL *videoUrl);

@end
