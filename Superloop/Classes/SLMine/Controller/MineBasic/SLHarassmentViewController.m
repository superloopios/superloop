//
//  SLHarassmentViewController.m
//  Superloop
//
//  Created by 张梦川 on 16/5/23.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLHarassmentViewController.h"
#import "SLAPIHelper.h"

@interface SLHarassmentViewController ()

@end

@implementation SLHarassmentViewController{
    UISwitch *_switch;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLHarassmentViewController"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLHarassmentViewController"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNav];
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 64, ScreenW, 50)];
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(20, 15, 130, 20)];
    lable.font = [UIFont systemFontOfSize:14];
    lable.text = @"屏蔽来电";
    UISwitch *swith = [[UISwitch alloc]initWithFrame:CGRectMake(ScreenW - 80, 10, 50, 30)];
    [swith addTarget:self action:@selector(SelectAction:) forControlEvents:UIControlEventValueChanged];
    _switch = swith;
    if ([self.isSwithOn isEqualToString:@"1"]) {
        [_switch setOn:YES];
    }else{
       [_switch setOn:NO];
    }
    [view addSubview:swith];
    view.backgroundColor = [UIColor whiteColor];
    [view addSubview:lable];
    
    UILabel *tipLable = [[UILabel alloc]initWithFrame:CGRectMake(20, 114, ScreenW - 40, 50)];
    tipLable.text = @"启用该功能后，您将不能接听来自超级圈用户的电话，如需接听电话请关闭该功能。";
    tipLable.font = [UIFont systemFontOfSize:14];
    tipLable.textColor = [UIColor lightGrayColor];
    tipLable.numberOfLines = 0;
    [self.view addSubview:view];
    [self.view addSubview:tipLable];
    self.view.backgroundColor = SLColor(229, 230, 231);
    
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"屏蔽来电";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}


// 添加接口
-(void)SelectAction:(UISwitch *)swith{
    
    if (swith.isOn) {
        [self setUserPhoneModel:1];
    } else {
        [self setUserPhoneModel:0];
    
    }

}

-(void)setUserPhoneModel:(int)isOn {
    //[SLUserDefault setswithIsOn:isOn];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"dnd_status"] = [NSString stringWithFormat:@"%d",isOn];
    [SLAPIHelper introduce:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        if (self.changeSuccess) {
            self.changeSuccess(@"success");
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeAvatar" object:nil];
    } failure:^(SLHttpRequestError *error) {
        [self showHUDmessage:@"修改失败"];
    }];
    
}




@end
