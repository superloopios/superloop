//
//  SLMyCommentViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/22.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLMyCommentViewController.h"
#import "SLMyCommentCell.h"
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "SLMyCommentCell.h"
#import "SLMyComment.h"
#import <MJExtension.h>
#import "SLTopicDetailViewController.h"
#import "SLTopicModel.h"
#import "SLAPIHelper.h"
#import  <MJRefresh.h>
#import "SLRefreshHeader.h"
#import "HomePageViewController.h"
#import "SLRefreshFooter.h"
static NSInteger const cols = 3;
static CGFloat const margin = 5;
#define  cellWH  ((ScreenW - 70 - (cols - 1) * margin) / cols)

@interface SLMyCommentViewController ()<slMyCommentCellDelegate,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (nonatomic,strong) NSMutableArray *commentArray;
@property (nonatomic,strong) UIAlertView *alert;
@property (nonatomic,strong) NSMutableArray *SLTopicModelArray;
@property (nonatomic,assign) NSInteger page;
@property (nonatomic,strong) NSIndexPath *indexpath;
@property (nonatomic,strong) UIButton *button;
@end

@implementation SLMyCommentViewController{
    
    UITableView *_tableView;
}
-(UIAlertView *)alert{
    if (!_alert) {
        _alert=[[UIAlertView alloc] initWithTitle:@"确认删除该条评论么?" message:nil delegate:self cancelButtonTitle:@"不了" otherButtonTitles:@"好的", nil];
    }
    return _alert;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLMyCommentViewController"];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [HUDManager hideHUDView];
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLMyCommentViewController"];
}
-(NSInteger)page{
    if (!_page) {
        _page=0;
    }
    return _page;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =SLColor(233, 233, 233);
    [self setUpNav];
    
    _tableView =[[UITableView alloc]init];
    _tableView.frame = CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor=SLColor(233, 233, 233);
    _tableView.separatorStyle =UITableViewCellSeparatorStyleNone;
    [_tableView registerNib:[UINib nibWithNibName:@"SLMyCommentCell" bundle:nil] forCellReuseIdentifier:@"myCommentCell"];
    [self.view addSubview:_tableView];
    [self setUpRefresh];
    //[self loadData];
    [_tableView.mj_header beginRefreshing];
    [_tableView.mj_footer setHidden:YES];
}

- (void)setUpNav
{
    
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"我的评论";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
//点击昵称跳转
- (void)didClickUserName:(NSString *)userId{
    
    if ([userId isEqualToString:[NSString stringWithFormat:@"%@", ApplicationDelegate.userId]]) {
        HomePageViewController *myView = [[HomePageViewController alloc] init];
        // myView.tag = 1000;
        myView.userId=ApplicationDelegate.userId;
        [self.navigationController pushViewController:myView animated:YES];
    }else
    {
        HomePageViewController *otherPerson = [[HomePageViewController alloc] init];
        otherPerson.userId = userId;
        [self.navigationController pushViewController:otherPerson animated:YES];
    }
    
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setUpRefresh
{
    _tableView.mj_header = [SLRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    _tableView.mj_footer = [SLRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreDatas)];
}
- (void)loadData
{
    self.page=0;
    NSDictionary *params=@{@"version":@"1",@"page":[NSString stringWithFormat:@"%ld",self.page],@"pager":@"20"};
    [SLAPIHelper getTopicComment:params success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        self.commentArray = [SLMyComment mj_objectArrayWithKeyValuesArray:data[@"result"]];
        // 刷新
        NSLog(@"%@-------",data[@"result"]);
        
        if (self.commentArray.count == 0) {
            if (!_noDataView) {
                [self addNotLoginViews:@"这里什么也没有"];
            }
            
        }else{
            
            [_noDataView removeFromSuperview];
            
        }
        [_tableView reloadData];
        if (self.commentArray.count<20) {
            [_tableView.mj_header endRefreshing];
            
            [_tableView.mj_footer endRefreshingWithNoMoreData];
            
        }else{
            [_tableView.mj_header endRefreshing];
            [_tableView.mj_footer resetNoMoreData];
        }
        [_tableView.mj_footer setHidden:NO];

    } failure:^(SLHttpRequestError *failure) {
        [_tableView.mj_header endRefreshing];
    }];
    
}
-(void)loadMoreDatas{
    [_tableView.mj_footer setHidden:NO];

    self.page++;
    NSDictionary *params=@{@"version":@"1",@"page":[NSString stringWithFormat:@"%ld",self.page],@"pager":@"20"};
    [SLAPIHelper getTopicComment:params success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
       NSArray *commentArray = [SLMyComment mj_objectArrayWithKeyValuesArray:data[@"result"]];
        NSLog(@"%@",data[@"result"]);
        [self.commentArray addObjectsFromArray:commentArray];
         // 刷新
        [_tableView reloadData];
        
        if ( commentArray.count <20) {
            
            [_tableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            // 结束刷新
            [_tableView.mj_footer endRefreshing];
        }
        
    } failure:^(SLHttpRequestError *failure) {
        [_tableView.mj_footer endRefreshing];
    }];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _commentArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SLMyComment *model = self.commentArray[indexPath.row];
    return model.cellHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SLMyCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCommentCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.commentArray[indexPath.row];
    cell.delegate = self;
    cell.indexPath = indexPath;
//    NSLog(@"%@",cell.model);
//    UIView *delView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, 0, 200, cell.frame.size.height)];
//    delView.backgroundColor = [UIColor redColor];
    return cell;
}

- (void)didtapRecommendViewClicked:(NSIndexPath *)indexpath{
    //每次回来之前取消选中
    [_tableView deselectRowAtIndexPath:indexpath animated:YES];
    UITableViewCell *cell = [_tableView cellForRowAtIndexPath:indexpath];
    cell.userInteractionEnabled = NO;
    SLMyComment *model = self.commentArray[indexpath.row];
    SLTopicDetailViewController *topicDetail = [[SLTopicDetailViewController alloc] init];
    //获取话题内容
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:model.topic[@"id"] forKey:@"id"];
    
    [SLAPIHelper getTopicContent:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
        // 刷新
        cell.userInteractionEnabled = YES;
        SLTopicModel *topicModel  =  [SLTopicModel mj_objectWithKeyValues:data[@"result"]];
        [_tableView reloadData];
        //点击时候计算好cell的内容frame
        //头像高度
        topicDetail.cellHeight = 45;
        // 标题高度
        CGFloat textMaxW = ScreenW - 50;
        NSString *content = model.reply[@"content"];
        if (content.length == 0) {
            topicDetail.cellHeight = 45;
        }
        
        //渠道每行的高度
        NSString *titleContentextString = @"Text";
        CGFloat titleFontSize = [titleContentextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
        CGFloat titleHeight = [self.title boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
        NSInteger newLineNum = titleHeight / titleFontSize;
        if (newLineNum > 2) {
            newLineNum = 2;
        }
        topicDetail.cellHeight += newLineNum * titleFontSize + 11;
        //内容高度
        NSString *contentTextString = @"Text";
        CGFloat contemtFontSize = [contentTextString boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
        
        CGFloat contentHeight= [content boundingRectWithSize:CGSizeMake(textMaxW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
        NSInteger newContentLineNum = contentHeight / contemtFontSize;
        topicDetail.cellHeight += newContentLineNum * contemtFontSize  +11 + (newContentLineNum - 1) * 5;
        //图片高度
        if (topicModel.imgs.count > 0) {
            
            topicDetail.cellHeight += ((topicModel.imgs.count - 1) / 3 + 1) * cellWH + (((topicModel.imgs.count - 1) / 3 + 1) - 1) * margin + 8;
        }
        else
        {
//            topicDetail.cellHeight += 50;
        }
        // 底部工具条
        topicDetail.cellHeight += 89;
        
        topicDetail.myComment = model;
        topicDetail.model = topicModel;
        [self.navigationController pushViewController:topicDetail animated:YES];
    } failure:^(SLHttpRequestError *failure) {
        cell.userInteractionEnabled = YES;
        kShowToast(@"该话题已删除");
    }];
}
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    SLMyComment *model = self.commentArray[indexPath.row];
//    
//    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
//    [parameters setObject:model.reply[@"id"] forKey:@"id"];
//    
//    [SLAPIHelper deleteTopicComment:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
//        // 刷新
//        [self.commentArray removeObjectAtIndex:indexPath.row];
//        [_tableView reloadData];
//    } failure:^(SLHttpRequestError *error) {
//        [HUDManager showWarningWithText:@"删除失败!"];
//    }];
//    
//}
//- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return @"删除";
//}

- (void)deleteButtonClicked:(UIButton *)button indexPath:(NSIndexPath *)indexpath{
    self.indexpath = indexpath;
    self.button=button;
    [self.alert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        [HUDManager showLoadingHUDView:[UIApplication sharedApplication].keyWindow withText:@""];
        self.button.selected = !self.button.selected;
        SLMyComment *model = self.commentArray[self.indexpath.row];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:model.reply[@"id"] forKey:@"id"];
        [SLAPIHelper deleteTopicComment:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            [HUDManager hideHUDView];
            [self.commentArray removeObjectAtIndex:self.indexpath.row];
            if (self.commentArray.count == 0) {
                
                if (!_noDataView) {
                    [self addNotLoginViews:@"这里什么也没有"];
                    
                } }else{
                    
                    [_noDataView removeFromSuperview];
                    
                }
            [_tableView reloadData];
        } failure:^(SLHttpRequestError *error) {
            [HUDManager hideHUDView];
            [HUDManager showWarningWithText:@"删除失败!"];
        }];
  
    }
}


// 没有内容
-(void)addNotLoginViews:(NSString *)str{
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, ScreenW , ScreenH-64 )];
    
    UILabel *tiplabel = [[UILabel alloc]init];
    //    tiplabel.backgroundColor= [UIColor redColor];
    CGSize tipLabelSize = [tiplabel boundingRectWithString:str withSize:CGSizeMake(500,40) withFont:17];
    
    tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 142,tipLabelSize.width, 40);
    if (ScreenW == 200) {
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width , 40);
    }else{
        
        tiplabel.frame = CGRectMake(ScreenW *0.5-tipLabelSize.width *0.5, 62,tipLabelSize.width + 20 , 40);
    }
    contentView.backgroundColor = SLColor(245, 245, 245);
    tiplabel.font = [UIFont systemFontOfSize:17];
    tiplabel.textAlignment = NSTextAlignmentLeft;
    tiplabel.numberOfLines = 0;
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedStr addAttribute:NSFontAttributeName
     
                          value:[UIFont systemFontOfSize:17.0]
     
                          range:NSMakeRange(2, 2)];
    
    //    [AttributedStr addAttribute:NSForegroundColorAttributeName
    //
    //                          value:SLColor(91, 133, 189)
    //
    //                          range:NSMakeRange(2, 2)];
    
    tiplabel.attributedText = AttributedStr;
    
    
    [contentView addSubview:tiplabel];
    UILabel *bolangLabel = [[UILabel alloc]initWithFrame:CGRectMake(tiplabel.frame.origin.x+tiplabel.frame.size.width,tiplabel.frame.origin.y, 50, 10)];
    bolangLabel.text = @"~~~";
    bolangLabel.font = [UIFont systemFontOfSize:12];
    bolangLabel.textColor = SLColor(213, 213, 213);
    [contentView addSubview:bolangLabel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenW *0.5-96, tiplabel.frame.origin.y + 76, 192, 192)];
    imgView.image = [UIImage imageNamed:@"swan"];
    [contentView addSubview:imgView];
    _noDataView  = contentView;
    [self.view addSubview:contentView];
    
}


@end
