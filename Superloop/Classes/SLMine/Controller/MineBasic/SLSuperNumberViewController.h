//
//  SLSuperNumberViewController.h
//  Superloop
//
//  Created by WangJiwei on 16/4/27.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLJavascriptBridgeEvent.h"
@interface SLSuperNumberViewController : UIViewController
@property (nonatomic ,strong) void(^superNumberBlcok)(NSString *str);
//@property (nonatomic,strong) SLJavascriptBridgeEvent *event;
@property (nonatomic,strong) NSString *username;
@end
