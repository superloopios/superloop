//
//  SLIntroduceViewController.m
//  Superloop
//
//  Created by WangJiwei on 16/4/12.
//  已进行修改  by  xiaowu  on  16/11/09
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLIntroduceViewController.h"
#import "SLTextView.h"
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "SLAPIHelper.h"
@interface SLIntroduceViewController()<UIAlertViewDelegate,UITextViewDelegate>
{
    UIButton *_submitBtn;
}
@property (nonatomic, strong) SLTextView        *introduceTextView;
@property (nonatomic, strong) UIAlertView       *alert;

@end
@implementation SLIntroduceViewController



- (SLTextView *)introduceTextView{
    if (!_introduceTextView) {
        _introduceTextView = [[SLTextView alloc] initWithFrame:CGRectMake(17, 70, screen_W - 34, screen_H-70)];
        _introduceTextView.delegate=self;
        _introduceTextView.scrollEnabled = YES;
        _introduceTextView.autocorrectionType = UITextAutocorrectionTypeYes;
        _introduceTextView.placeholder = @"介绍一下自己";
        _introduceTextView.text = self.intro;
        _introduceTextView.textColor = SLBlackTitleColor;
        [self.view addSubview:self.introduceTextView];
    }
    return _introduceTextView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"编辑简介";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    _submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    [_submitBtn setTitle:@"完成" forState:UIControlStateNormal];
    _submitBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_submitBtn setTitleColor:SLMainColor forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(ensuerChange) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:_submitBtn];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUpNav];
    //键盘处理
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    _alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"你输入的内容已达到2047个字符的最大限制" delegate:self cancelButtonTitle:@"好" otherButtonTitles:nil, nil];
    [self.introduceTextView becomeFirstResponder];
}


- (void)ensuerChange{
    
    if (self.introduceTextView.text.length<=2047) {
        NSDictionary *parameters = @{@"bio":self.introduceTextView.text};
        NSLog(@"-parameters----%@",parameters);
        [SLAPIHelper introduce:parameters success:^(NSURLSessionDataTask *task,  NSDictionary *data) {
            //修改完成刷新h5页面
            [[NSNotificationCenter defaultCenter] postNotificationName:@"resetSecondMeData" object:nil];
//            [self.event callHandler:@"reload" data:nil responseCallback:^(NSDictionary *responseData) {
//            }];
            if (_introduceBlcok) {
                _introduceBlcok(self.introduceTextView.text);
            }
            [self.navigationController popViewControllerAnimated:YES];

        } failure:^(SLHttpRequestError *failure) {
            NSLog(@"failure------%@",failure);
            [HUDManager showWarningWithText:@"保存失败"];
        }];
       
    }else{
        [_alert show];
    }
}
////键盘即将显示修改textView的高度
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    CGRect keyboardEndRect = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.introduceTextView.frame = CGRectMake(17, 70, screen_W - 34, screen_H-70);
    [self.view layoutIfNeeded];
    [self.view layoutSubviews];
}
//键盘即将隐藏textView的高度
- (void)keyboardWillHide:(NSNotification *)notification
{
    self.introduceTextView.frame = CGRectMake(17, 70, screen_W - 34, screen_H-70);
    [self.view layoutIfNeeded];
    [self.view layoutSubviews];
}

@end
