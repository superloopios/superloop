//
//  SLWalletViewController.m
//  Superloop
//
//  Created by xiaowu on 16/9/12.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLWalletViewController.h"
#import "ColorUtility.h"
#import "SLUserAccountViewController.h"
#import "SLCashCouponViewController.h"
#import "SLAPIHelper.h"
#import "SLTradeRecordViewController.h"

@interface SLWalletViewController (){
    UILabel *amountLabel;
    UILabel *cashCouponLabel;
    UILabel *amountLogoLabel;
    UILabel *cashLogoLabel;
    UIImageView *amountArrowImageView;
    UIImageView *cashArrowImageView;
    UIView *amountView;
    UIView *cashView;
    UILabel *topAmountLabel;
    UILabel *topamountLogoLabel;
    NSNumber *amount;
}
@end

@implementation SLWalletViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"SLWalletViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLWalletViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    amount = @0;
    // Do any additional setup after loading the view.
    self.view.backgroundColor = SLColor(244, 244, 244);
    [self setUpNav];
    [self setUpUi];
    [self getData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData) name:@"chargeSuccess" object:nil];
}
- (void)setUpUi{
    UILabel *amountCashLabel = [[UILabel alloc] init];
    amountCashLabel.text = @"总金额";
    amountCashLabel.font = [UIFont systemFontOfSize:15];
    amountCashLabel.textColor = [UIColor blackColor];
    [self.view addSubview:amountCashLabel];
    [amountCashLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(79);
        make.centerX.equalTo(self.view);
    }];
    for (int i = 0; i<2; i++) {
        UIView *view = [[UIView alloc] init];
        view.frame = CGRectMake(0, 160+i*55, screen_W, 45);
        view.tag = i;
        [self.view addSubview:view];
        view.backgroundColor = [UIColor whiteColor];
        [self addSubview:view];
    }
    topAmountLabel = [[UILabel alloc] init];
    topAmountLabel.textColor = SLMainColor;
    topAmountLabel.font = [UIFont systemFontOfSize:35];
    [self.view addSubview:topAmountLabel];
    topAmountLabel.text = [NSString stringWithFormat:@"%.2f",[self.userData[@"amount"] doubleValue]];
    [topAmountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(amountCashLabel.mas_bottom).offset(5);
        make.centerX.equalTo(self.view);
    }];
    
    topamountLogoLabel = [[UILabel alloc] init];
    topamountLogoLabel.textColor = SLMainColor;
    topamountLogoLabel.font = [UIFont systemFontOfSize:21];
    [self.view addSubview:topamountLogoLabel];
    topamountLogoLabel.text = @"¥";
    [topamountLogoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(topAmountLabel.mas_bottom).offset(-3);
        make.right.equalTo(topAmountLabel.mas_left);
    }];
}
- (void)addSubview:(UIView *)view{
    UILabel *titleLabel = [[UILabel alloc] init];
    [view addSubview:titleLabel];
    if (view.tag == 0) {
        titleLabel.text = @"账户余额";
    }else if (view.tag == 1) {
        titleLabel.text = @"现金券余额";
    }
    titleLabel.font = [UIFont systemFontOfSize:15];
    titleLabel.textColor = [ColorUtility colorWithHexString:@"888888"];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(12);
        make.height.equalTo(view);
        make.top.equalTo(view);
    }];
    UIImageView *imgV = [[UIImageView alloc] init];
    imgV.image = [UIImage imageNamed:@"Down-Arrow-outline"];
    [view addSubview:imgV];
    imgV.contentMode = UIViewContentModeCenter;
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-12);
        make.height.equalTo(view);
        make.top.equalTo(view);
        make.width.equalTo(@5);
    }];
    
    UILabel *label = [[UILabel alloc] init];
    label.textColor = SLMainColor;
    label.font = [UIFont systemFontOfSize:22];
    label.textAlignment = NSTextAlignmentRight;
    [view addSubview:label];
    if (view.tag == 0) {
        amountLabel = label;
        label.text = @"";
    }
    if (view.tag == 1) {
        cashCouponLabel = label;
        label.text = @"";
    }
    CGFloat width = [label.text boundingRectWithSize:CGSizeMake(screen_W, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:22]} context:nil].size.width+2;
    if (width>screen_W-150) {
        width = screen_W-150;
    }
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(imgV.mas_left).offset(-20);
        make.height.equalTo(view);
        make.top.equalTo(view);
        make.width.equalTo(@(width));
    }];
    
    UILabel *cashlabel = [[UILabel alloc] init];
    cashlabel.textColor = SLMainColor;
    cashlabel.font = [UIFont systemFontOfSize:19];
    cashlabel.text = @"¥";
    [view addSubview:cashlabel];
    [cashlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(label.mas_left);
        make.bottom.equalTo(view).offset(-10);
    }];
    if (view.tag == 0) {
        amountLogoLabel = cashlabel;
        amountArrowImageView = imgV;
        amountView = view;
    }else if (view.tag == 1) {
        cashLogoLabel = cashlabel;
        cashArrowImageView = imgV;
        cashView = view;
    }
    if (view.tag == 0) {
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpAccountVc)];
        [view addGestureRecognizer:ges];
    }else if (view.tag == 1) {
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpCashCouponVc)];
        [view addGestureRecognizer:ges];
    }
}
- (void)jumpAccountVc{
    SLUserAccountViewController *userAccount = [[SLUserAccountViewController alloc] init];
    userAccount.userData = self.userData;
    userAccount.amount = amount;
    [self.navigationController pushViewController:userAccount animated:YES];
}
- (void)jumpCashCouponVc{
    SLCashCouponViewController *vc = [[SLCashCouponViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setUpNav{
    
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"我的钱包";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    [self.view addSubview:navView];
    //
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 20, 60, 44);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [leftBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:leftBtn];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [rightButton setTitle:@"交易记录" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [rightButton setFrame:CGRectMake(screen_W - 90, 20, 80, 44)];
    [rightButton addTarget:self
                    action:@selector(tradeAction)
          forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:rightButton];
    
    //添加分割线
    UIImageView *singleView = [[UIImageView alloc] init];
    singleView.backgroundColor =SLSepatorColor;
    singleView.frame = CGRectMake(0, 63.5, ScreenW, 0.5);
    [navView addSubview:singleView];
}
- (void)leftBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)tradeAction{
    SLTradeRecordViewController *tradeVC = [[SLTradeRecordViewController alloc]init];
    [self.navigationController pushViewController:tradeVC animated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)getData{
    [SLAPIHelper getAmountInfoSuccess:^(NSURLSessionDataTask *task, NSDictionary *data) {
        amount = data[@"result"][@"account_amount"];
        amountLabel.text = [NSString stringWithFormat:@"%.2f",[data[@"result"][@"account_amount"] doubleValue]];
        cashCouponLabel.text = [NSString stringWithFormat:@"%.2f",[data[@"result"][@"voucher_amount"] doubleValue]];
        topAmountLabel.text = [NSString stringWithFormat:@"%.2f",[data[@"result"][@"total_amount"] doubleValue]];
        CGFloat width = [amountLabel.text boundingRectWithSize:CGSizeMake(screen_W, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:22]} context:nil].size.width+2;
        if (width>screen_W-150) {
            width = screen_W-150;
        }
        [amountLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(amountArrowImageView.mas_left).offset(-20);
            make.height.equalTo(amountView);
            make.top.equalTo(amountView);
            make.width.equalTo(@(width));
        }];
        
        [amountLogoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(amountLabel.mas_left);
            make.bottom.equalTo(amountView).offset(-10);
        }];
        
        CGFloat cashWidth = [cashCouponLabel.text boundingRectWithSize:CGSizeMake(screen_W, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:22]} context:nil].size.width+2;
        if (cashWidth>screen_W-150) {
            cashWidth = screen_W-150;
        }
        [cashCouponLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(cashArrowImageView.mas_left).offset(-20);
            make.height.equalTo(cashView);
            make.top.equalTo(cashView);
            make.width.equalTo(@(cashWidth));
        }];
        
        [cashLogoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(cashCouponLabel.mas_left);
            make.bottom.equalTo(cashView).offset(-10);
        }];
    } failure:^(SLHttpRequestError *failure) {
        
    }];

    
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
