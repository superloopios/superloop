//
//  SLRePasswordViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/11.
//  Copyright © 2016年 Superloop. All rights reserved.

#import "SLRePasswordViewController.h"
#import "UMSocial.h"
#import "AppDelegate.h"
@interface SLRePasswordViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *oldPassword;
@property (weak, nonatomic) IBOutlet UITextField *nextPassword;
@property (weak, nonatomic) IBOutlet UITextField *repeadPassword;


@end

@implementation SLRePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.oldPassword becomeFirstResponder];
    self.oldPassword.delegate = self;
    self.nextPassword.delegate = self;
    self.repeadPassword.delegate = self;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(save)];
}
- (void)save
{
    [self.view endEditing:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.oldPassword) [self.nextPassword becomeFirstResponder];
    if (textField == self.nextPassword) [self.repeadPassword becomeFirstResponder];
    if (textField == self.repeadPassword) [self save];
    return YES;
}


@end
