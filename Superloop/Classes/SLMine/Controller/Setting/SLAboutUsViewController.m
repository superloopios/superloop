//
//  SLAboutUsViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLAboutUsViewController.h"
//define this constant if you want to use Masonry without the 'mas_' prefix
//#define MAS_SHORTHAND

//define this constant if you want to enable auto-boxing for default syntax
//#define MAS_SHORTHAND_GLOBALS
#import <Masonry.h>

#import "SLNormalQustionViewController.h"
#import "SLWebViewController.h"

@interface SLAboutUsViewController ()
@property (nonatomic, strong)UIView *headView;
@end

@implementation SLAboutUsViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLAboutUsViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLAboutUsViewController"];
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"关于";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor = SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setUpNav];
    self.tableView.y = 64;
    self.tableView.height = screen_H - 64;
    SLBaseModel *judge = [SLBaseModel modelWithTitle:@"去评分" type:SLSettingCellTypeArrow];
   // SLBaseModel *attention = [SLBaseModel modelWithTitle:@"关注微博" type:SLSettingCellTypeArrow];
    SLBaseModel *normalQuestion = [SLBaseModel modelWithTitle:@"常见问题" type:SLSettingCellTypeArrow];
    SLBaseGroup *group = [[SLBaseGroup alloc] init];
//    group.cells = @[judge, attention, normalQuestion];
    group.cells = @[judge, normalQuestion];

    _dataArray = @[group];
    self.tableView.tableHeaderView = self.headView;
    self.tableView.backgroundColor = SLColor(240, 240, 240);
    self.tableView.rowHeight = 60;
    
    [self addFooterView];
}
- (void)addFooterView{
    UILabel *protocolLabel = [[UILabel alloc] init];
    protocolLabel.text = @"超级圈科技 版权所有";
    protocolLabel.font = [UIFont systemFontOfSize:14];
    protocolLabel.textColor = SLColor(120, 120, 120);
    [self.view insertSubview:protocolLabel aboveSubview:self.tableView];
    [protocolLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_headView);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-25);
    }];
    
    UIButton *userAgreementBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [userAgreementBtn setTitle:@"超级圈用户协议" forState:UIControlStateNormal];
    userAgreementBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [userAgreementBtn setTitleColor:UIColorFromRGB(0x0472ee) forState:UIControlStateNormal];
    [self.view insertSubview:userAgreementBtn aboveSubview:self.tableView];
    [userAgreementBtn addTarget:self action:@selector(userAgreeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [userAgreementBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_headView);
        make.bottom.mas_equalTo(protocolLabel.mas_top).offset(-10);
        make.width.equalTo(@100);
        make.height.equalTo(@30);
    }];
}
- (void)userAgreeBtnClick{
    SLWebViewController *vc=[[SLWebViewController alloc] init];
    vc.url=@"http://m.superloop.com.cn/agreement.html";
    [self.navigationController pushViewController:vc animated:YES];
}

//布局headview
- (UIView *)headView
{
    if (!_headView) {
        _headView = [[UIView alloc] init];
//        _headView.backgroundColor = [UIColor redColor];
        _headView.frame = CGRectMake(0, 64, self.tableView.width, 180);
        UIImageView *logoView = [[UIImageView alloc] init];
        logoView.image = [UIImage imageNamed:@"super_logo"];
        [_headView addSubview:logoView];
        
        UIImageView *textLogoView = [[UIImageView alloc] init];
        textLogoView.image = [UIImage imageNamed:@"noMatter"];
        [_headView addSubview:textLogoView];
        
        UILabel *labelL = [[UILabel alloc] init];
        labelL.text = [NSString stringWithFormat:@"V%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
        labelL.font = [UIFont systemFontOfSize:18];
        [_headView addSubview:labelL];

        [logoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_headView).offset(25);
            make.width.height.mas_equalTo(62.5);
            make.centerX.mas_equalTo(_headView);
            
        }];
//        textLogoView.backgroundColor = [UIColor grayColor];
        textLogoView.contentMode = UIViewContentModeCenter;
        [textLogoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(logoView.mas_bottom).offset(15);
            make.left.right.equalTo(_headView);
            make.height.mas_equalTo(20);
        }];
        
        [labelL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(_headView);
            make.top.mas_equalTo(textLogoView.mas_bottom).offset(20);
        }];
    }
    return _headView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row==1) {
        SLNormalQustionViewController *vc=[SLNormalQustionViewController alloc];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        NSString  *str;
        
        str = [NSString  stringWithFormat:@"itms-apps://itunes.apple.com/app/id%d",1130659569];
        
        [[UIApplication  sharedApplication] openURL:[NSURL  URLWithString:str]];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
