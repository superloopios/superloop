//
//  SLSettingTableViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/3.
//  Copyright © 2016年 Superloop. All rights reserved.
//
#import "NSObject+FileManager.h"
#import "SLSettingTableViewController.h"
#import "SLBaseViewCell.h"
#import "SLRePasswordViewController.h"
#import "SLAboutUsViewController.h"
#import "SLQuestionUsViewController.h"
#import "SLTabBarViewController.h"
#import "AppDelegate.h"
#import "SLMessageManger.h"
#import "SLFirstViewController.h"
#import "SLTabBarViewController.h"
#import "SLHarassmentViewController.h"
#import "SLAPIHelper.h"
#import "SLNavgationViewController.h"
#import "SLActionView.h"

#import "SLForgetPassWordViewController.h"
@interface SLSettingTableViewController ()<UIActionSheetDelegate,UITabBarControllerDelegate,SLActionViewDelegate>{
    SLBaseModel *clean;
}
/** 缓存尺寸*/
@property(nonatomic ,assign) NSString *totalString;

@property (nonatomic,strong)SLFirstViewController *loginVc;

@property (nonatomic,strong)SLActionView *actionView;
@end

@implementation SLSettingTableViewController

- (SLActionView *)actionView{
    if (!_actionView) {
        _actionView = [[SLActionView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _actionView.delegate = self;
        [self.view addSubview:_actionView];
    }
    return _actionView;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLSettingTableViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLSettingTableViewController"];
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"设置";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    [self setUpNav];
    //监听到登录的返回按钮点击发送过来的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backComeLogin) name:@"login" object:nil];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.frame = CGRectMake(0, 64, ScreenW, ScreenH - 64);
    [self loadTableView];
    [self.tableView reloadData];
    // 获取cachePath文件缓存
   self.tableView.backgroundColor = SLColor(244, 244, 244);
    [self getFileCacheSizeWithPath:self.cachePath completion:^(NSInteger total) {
        
        if (total / 1024 > 1000) {
            _totalString = [NSString stringWithFormat:@"%.1fM", total / 1024.0 / 1024.0];
        }else
        {
            _totalString = [NSString stringWithFormat:@"%ldK", total / 1024];
        }
        clean.rightTitle = self.totalString;
        [self.tableView reloadData];
        [self changeCell];
    }];
    
   
    self.navigationItem.title = @"设置";
//    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 64);
//    [self loadTableView];
    
}
- (void)loadTableView
{
   // __weakSelf
//    __weak typeof(self) weakSelf = self;
    SLBaseModel *replacePassword = [SLBaseModel modelWithTitle:@"修改密码" type:SLSettingCellTypeArrow];
    SLBaseModel *closeCall = [SLBaseModel modelWithTitle:@"屏蔽来电" type:SLSettingCellTypeArrow];
    clean = [SLBaseModel modelWithTitle:@"清理缓存" type:SLSettingCellTypeLabel];
    clean.hasActiveView = YES;
    clean.rightTitle = self.totalString;
    SLBaseModel *question = [SLBaseModel modelWithTitle:@"意见反馈" type:SLSettingCellTypeArrow];
    SLBaseModel *aboutUs = [SLBaseModel modelWithTitle:@"关于我们" type:SLSettingCellTypeArrow];
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 80)];
    UIButton *logoutBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, 35, ScreenW - 40, 45)];
    logoutBtn.layer.cornerRadius = 6;
    logoutBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    logoutBtn.layer.masksToBounds = YES;
    logoutBtn.backgroundColor = SLMainColor;
    [logoutBtn setTitle:@"退出当前账号" forState: UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [logoutBtn addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    if (ApplicationDelegate.userId) {
        logoutBtn.hidden = NO;
    }else{
        logoutBtn.hidden = YES;
    }
    SLBaseGroup *group1 = [[SLBaseGroup alloc] init];
    SLBaseGroup *group2 = [[SLBaseGroup alloc] init];
    group1.cells =@[replacePassword,closeCall];
    group2.cells =@[clean, question, aboutUs];
    
    
    _dataArray = @[group1,group2];
    // 调整组之间间距
    //    self.tableView.sectionHeaderHeight = 0;
    [footerView addSubview:logoutBtn];
//    self.tableView.contentInset = UIEdgeInsetsMake(-25, 0, 0, 0);
    self.tableView.tableFooterView = footerView;
    self.tableView.rowHeight = 60;
//    self.tableView.y = 44;
//    self.tableView.height = screen_H - 44;
}

- (void)logout
{
    self.actionView.titleArr = @[@"取消",@"退出登录"];
    self.actionView.tag = 2;
    [self.view addSubview:self.actionView];
}


#pragma mark ----- 在退出登录之后需要展示登录的login页面，不要显示我的界面了
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (actionSheet.tag == 2) {
        if (buttonIndex == 0) {
            __weak typeof(self) weakSelf = self;
            [self removeCacheWithCompletion:^{
                weakSelf.totalString=[NSString stringWithFormat:@"%dK",0];
                clean.rightTitle = self.totalString;
                clean.hasActiveView = NO;
                [weakSelf.tableView reloadData];
//                [self loadTableView];
            }];
        }
    }
}

- (void)clearAllUserDefaultsData
{
    
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    
    NSDictionary* dict = [defs dictionaryRepresentation];
    for(id key in dict) {
        if (![key isEqualToString:appIsFirst] && ![key isEqualToString:strAccountName]&& ![key isEqualToString:SLNetWorkStatus] &&![key isEqualToString:strAccout] && ![key isEqualToString:@"VoiceType"]&& ![key isEqualToString:location]&&![key isEqualToString:@"guideSave"]&&![key isEqualToString:ContactStatus]&&![key isEqualToString:LoginStutas]&&![key isEqualToString:AllowAddContactStatus]) {
            [defs removeObjectForKey:key];
            [defs synchronize];
        }
    }
//    NSLog(@"%@",[SLUserDefault getUserInfo]);
    
    
}

- (void)clearCacheUserDefaultsData
{
    
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    
    NSDictionary* dict = [defs dictionaryRepresentation];
    NSString *followsTopicKey = [URL_ROOT_PATIENT stringByAppendingString:@"/users/follows/topics"];
    NSString *popularTopicKey = [URL_ROOT_PATIENT stringByAppendingString:@"/topics/popular"];
    NSString *topicKey = [URL_ROOT_PATIENT stringByAppendingString:@"/topics"];
    for(id key in dict) {
        if ([key isEqualToString:followsTopicKey] ||[key isEqualToString:popularTopicKey]||[key isEqualToString:topicKey] ) {
            [defs removeObjectForKey:key];
            [defs synchronize];
        }
    }
    [self removeCacheWithCacheName:@"cachepersonTopicIDs"];
    [self removeCacheWithCacheName:@"cachepersonReceiveCommentIDs"];
}
- (void)removeCacheWithCacheName:(NSString *)cacheName{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSString *cacheStrs = [defs objectForKey:cacheName];
    if (cacheStrs) {
        NSArray *ids = [cacheStrs componentsSeparatedByString:@","];
        for (NSString *ID in ids) {
            [defs removeObjectForKey:ID];
        }
        [defs removeObjectForKey:cacheName];
    }
}

- (void)backComeLogin{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dealloc{
    NSLog(@"dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0 && indexPath.section == 0) {
        if (ApplicationDelegate.userId) {
            SLForgetPassWordViewController *rePwd = [[SLForgetPassWordViewController alloc] init];
            [self.navigationController pushViewController:rePwd animated:YES];
        }else{
            SLFirstViewController *vc = [[SLFirstViewController alloc] init];
            SLNavgationViewController *nav = [[SLNavgationViewController alloc] initWithRootViewController:vc];
            [self presentViewController:nav animated:YES completion:nil];
        }
        
    }else if (indexPath.row == 1 && indexPath.section == 0){
        if (ApplicationDelegate.userId) {
            SLHarassmentViewController *harassment = [[SLHarassmentViewController alloc] init];
            __weak typeof(self) weakSelf = self;
            harassment.changeSuccess = ^(NSString *str){
                [weakSelf getUserData];
            };
            harassment.isSwithOn=[NSString stringWithFormat:@"%@",self.infoDict[@"dnd_status"]];
            [self.navigationController pushViewController:harassment animated:YES];
        }else{
            SLFirstViewController *vc = [[SLFirstViewController alloc] init];
            SLNavgationViewController *nav = [[SLNavgationViewController alloc] initWithRootViewController:vc];
//            vc.isComeFromSettingVc = YES;
            [self presentViewController:nav animated:YES completion:nil];
        }
        
    }else if (indexPath.row == 0 && indexPath.section == 1){
        self.actionView.titleArr = @[@"取消",@"清理"];
        self.actionView.tag = 1;
        [self.view addSubview:self.actionView];
//        UIActionSheet *sheetClean= [[UIActionSheet alloc] initWithTitle:@"确定清理缓存" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"确定", nil];
//        sheetClean.tag = 2;
//        [sheetClean showInView:self.view];
    }else if (indexPath.row == 1 && indexPath.section == 1){
        if (ApplicationDelegate.userId) {
            SLQuestionUsViewController *questionUs = [[SLQuestionUsViewController alloc] init];
            [self.navigationController pushViewController:questionUs animated:YES];
        }else{
            SLFirstViewController *vc = [[SLFirstViewController alloc] init];
            SLNavgationViewController *nav = [[SLNavgationViewController alloc] initWithRootViewController:vc];
//            vc.isComeFromSettingVc = YES;
            [self presentViewController:nav animated:YES completion:nil];
        }
        
    }else if (indexPath.row == 2 && indexPath.section == 1){
        SLAboutUsViewController *aboutUs = [[SLAboutUsViewController alloc] init];
        [self.navigationController pushViewController:aboutUs animated:YES];
    }
}

- (void)getUserData{
    
    [SLAPIHelper getUsersData:@{@"id":ApplicationDelegate.userId} success:^(NSURLSessionDataTask *task, NSDictionary *data) {
        self.infoDict = data[@"result"];
    } failure:^(SLHttpRequestError *failure) {
        
    }];
}
- (void)changeCell{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    SLBaseViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    [cell.activeView stopAnimating];
    cell.activeView.hidden = YES;
}
- (void)actionSheetButtonClickedAtIndex:(NSInteger)index{
    if (self.actionView.tag == 1) {
        if (index == 1) {
            __weak typeof(self) weakSelf = self;
            [self removeCacheWithCompletion:^{
                [weakSelf clearCacheUserDefaultsData];
                weakSelf.totalString=[NSString stringWithFormat:@"%dK",0];
                clean.rightTitle = self.totalString;
                clean.hasActiveView = NO;
                [weakSelf.tableView reloadData];
                //                [self loadTableView];
            }];
        }
    }else if (self.actionView.tag == 2) {
        if (index == 1) {
            //退出登录
            //            ApplicationDelegate.isLogin = NO;
            [SLUserDefault removeUserInfo];
            
            ApplicationDelegate.Basic = NULL;
            ApplicationDelegate.userInformation = NULL;
            ApplicationDelegate.userId = NULL;
            ApplicationDelegate.isLogin = NO;
            [[SLMessageManger sharedInstance] logout];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ClearMessage" object:self];
            ApplicationDelegate.titleStr=nil;
            ApplicationDelegate.contentStr=nil;
            ApplicationDelegate.publishPhotosArr=nil;
            
            //            //展示登录界面控制器
            [self.navigationController popViewControllerAnimated:YES];
            self.tabBarController.hidesBottomBarWhenPushed = NO;
            [self clearAllUserDefaultsData];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"exitSuperLoop" object:nil];
            SLNavgationViewController *nav = [[SLNavgationViewController alloc] initWithRootViewController:[[SLFirstViewController alloc] init]];
            [UIApplication sharedApplication].keyWindow.rootViewController = nav;
            
        }
    }
    [self.actionView dismiss];
}

@end
