//
//  SLNormalQustionViewController.m
//  Superloop
//
//  Created by WangS on 16/6/6.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLNormalQustionViewController.h"

#import "SLNormalQuestionHeaderView.h"

#import "SLNormalQuestionModel.h"
#import "UILabel+StringFrame.h"

@interface SLNormalQustionViewController ()<UITableViewDataSource,UITableViewDelegate>


@property (nonatomic,strong)NSMutableArray *dataSources;

@property (nonatomic,strong)UITableView *myTableView;
@property (nonatomic,assign)BOOL  btnIsClick;

@property (nonatomic,strong)NSMutableArray *heightForCellArray;


@end

@implementation SLNormalQustionViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLNormalQustionViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLNormalQustionViewController"];
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"常见问题";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSMutableArray *)heightForCellArray{
    
    if (_heightForCellArray == nil) {
        _heightForCellArray = [NSMutableArray array];
    }
    
    return _heightForCellArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];


    self.view.backgroundColor=SLColor(240, 240, 240);

    _btnIsClick=NO;
    [self setUpNav];
    //self.title= @"常见问题";
    [self getData];
    [self initTableView];
}

-(void)getData
{
    NSArray *groupArray=@[@"超级圈是什么?",@"如何使用超级圈?",@"如何成为超级圈超人?",@"在超级圈的交易收取手续费么?",@"在超级圈提现的过程是怎么样的?",@"为什么要实名认证审核?"];
    
    NSArray *contentArray=@[@" A:超级圈是一个以技能/经验的交易和分享为主题的具备一定社交和社区属性的综合性平台，通过主题讨论、私信约谈、电话联系等方式达成用户之间的交流和分享。当前按需经济和分享经济发展迅猛,把个人闲置资产投入按需经济已经成为许多人的一种赚钱方式甚至工作方式。而我们的业务独树一帜的方面在于，我们的用户投入的是知识、经验、时间等，旨在通过我们的平台让需求方能便捷的获取需要的知识和经验，让服务方更好的利用赋闲时间分享技能获得物质和精神层面的双重满足，以此将人与人之间原本低效甚至无效的社交转换为高效的、有益的社交,从而提高社会运行效率,解放生产力。",@"  A:在超级圈可以成为两种角色，一种是需求者、一种是超人。需求者可以通过搜索问题/用户、话题讨论、私信交谈、电话咨询等方式，寻找到需要的人或问题的答案；超人通过在超级圈内展现自身的才能、让需求者能够以付费的方式联系到自己，从而实现知识技能的变现。",@"  A:超级圈是一个自我展示的平台，一般来讲，进入超级圈就已经是超人的一员，但是，想让别人更多地关注到人从而给自身带来更多的收益，建议进行实名认证，实名认证是个人信用的强力保证。另外可在相关的话题内多分享自己的知识和见解，以此获得更多的曝光机会。",@"  A:您在超级圈交易产生的收益，根据相关法律规定，平台将收取其中的的10%用做平台的运营费用（包括但不限于通话时运营商收取的基本通话费、提现时第三方平台收取的提现手续费用）等。",@"  A:您在超级圈钱包的余额可能包含以下部分：1.收入：指您在超级圈通过接听电话和收费消息获得的收入；2.充值余额：指您充值进入超级圈钱包的金额的余额。其中：您的收入将转账至您指定的账号（目前只支持支付宝账号）。根据中国法律，充值的余额将根据您的充值来源以退款的方式返回到您的充值账户。",@"  A:为了保证用户的权益，我们鼓励用户进行实名认证审核，经过超级圈审核的用户，将享有更灵活的通话资费设置权利以及官方的信用保证。"];
    
    for (int i=0; i<groupArray.count; i++) {
        SLNormalQuestionModel *model=[[SLNormalQuestionModel alloc] init];
        model.groupStr=groupArray[i];
        [model.contentArr addObject:contentArray[i]];
        [self.dataSources addObject:model];
    }
    
    
    [self.myTableView reloadData];
}


-(void)initTableView
{
    self.myTableView.dataSource=self;
    self.myTableView.delegate=self;
    self.myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.myTableView.frame = CGRectMake(0, 64, ScreenW, ScreenH-64);
    [self.view addSubview:self.myTableView];
    [self.myTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
//    UIView *footView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 0.01)];
//    footView.backgroundColor=SLColor(159, 159, 159);
//    self.myTableView.tableFooterView=footView;
}

#pragma mark - UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    SLNormalQuestionModel *model=self.dataSources[section];
    if (!model.isHide) {
        return 0;

    }
      return model.contentArr.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataSources.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.backgroundColor=SLColor(240, 240, 240);
    SLNormalQuestionModel *model=self.dataSources[indexPath.section];
    cell.textLabel.text=model.contentArr[indexPath.row];
    cell.textLabel.font=[UIFont systemFontOfSize:14];
    cell.textLabel.numberOfLines=0;

    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SLNormalQuestionHeaderView *headerView=[[[NSBundle mainBundle] loadNibNamed:@"SLNormalQuestionHeaderView" owner:nil options:nil] firstObject];
    SLNormalQuestionModel *model=self.dataSources[section];
    
    headerView.titleLab.text=model.groupStr;
    headerView.titleLab.font=[UIFont systemFontOfSize:15];
    headerView.tag = 100+section;  //标记
    headerView.backgroundColor=[UIColor whiteColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapgr:)];
    headerView.userInteractionEnabled = YES;
    [headerView addGestureRecognizer:tap];
    //判断是否隐藏
    if(!model.isHide)
    {
        headerView.imgArrow.image = [UIImage imageNamed:@"up-Arrow-outline"];
    }else{
        headerView.imgArrow.image = [UIImage imageNamed:@"down_arrow"];
    }

    
    return headerView;
}

-(void)tapgr:(UITapGestureRecognizer *)tap
{
    SLNormalQuestionModel *groupModel = self.dataSources[tap.view.tag-100];
    //开关取反
    groupModel.isHide = !groupModel.isHide;
    
    NSIndexSet *set = [NSIndexSet indexSetWithIndex:tap.view.tag-100];  //实例化段的集合
    
    //重新刷新段
    [self.myTableView reloadSections:set withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SLNormalQuestionModel *model=self.dataSources[indexPath.section];
    NSString *str=model.contentArr[indexPath.row];
    CGFloat strHeight= [str boundingRectWithSize:CGSizeMake(ScreenW-20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height+20;
    return strHeight;
//    if (indexPath.section == 0) {
//        
//        return 230;
//        
//    }else if(indexPath.section == 1) {
//        
//        return 103;
//    }
//    
//    if (indexPath.section == 2) {
//        return 100;
//    }
//    
//    if (indexPath.section == 3) {
//        return 110;
//    }
//    
//    if (indexPath.section == 4) {
//        
//        return 130;
//        
//    }
//    if (indexPath.section == 5) {
//        
//        return 70;
//        
//    }
//    return 0;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}

#pragma mark -懒加载
-(NSMutableArray *)dataSources
{
    if (!_dataSources) {
        _dataSources=[NSMutableArray new];
    }
    return _dataSources;
}

-(UITableView *)myTableView
{
    if (!_myTableView) {
        _myTableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH) style:UITableViewStylePlain];
    }
    return _myTableView;
}

@end
