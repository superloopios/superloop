//
//  SLQuestionUsViewController.m
//  Superloop
//
//  Created by WangJiWei on 16/3/8.
//  Copyright © 2016年 Superloop. All rights reserved.
//  意见反馈

#import "SLQuestionUsViewController.h"
#import "SLTextView.h"
#import "SLImageCell.h"
#import "TZImagePickerController.h"
#import "SLAPIHelper.h"
#import "AppDelegate.h"
#import <AliyunOSSiOS/OSSService.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#import "SLActionView.h"
#import "NSString+Utils.h"
#import "SLNavgationViewController.h"
#import "SLFirstViewController.h"


@interface SLQuestionUsViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UICollectionViewDataSource,UICollectionViewDelegate,TZImagePickerControllerDelegate,SLImageCellDelegate,SLActionViewDelegate,UIAlertViewDelegate>
{
    UICollectionView *_collectionView;
    CGFloat _itemWH;
    CGFloat _margin;
    NSMutableArray *_selectedPhotos;
    NSInteger _num;
    UIButton *_submitBtn;
}
@property (nonatomic, strong) UIButton *addImageBtn;
@property (nonatomic, strong) SLTextView *questionTextField;
@property(nonatomic, strong) NSData *data;
@property (nonatomic, copy)NSString *imagName;

@property (nonatomic,strong)SLActionView *actionView;

@property (nonatomic, strong) NSMutableArray *photoArrays;
@property (nonatomic, strong) NSMutableArray *photoNameArrays;
@end

@implementation SLQuestionUsViewController

-(NSMutableArray *)photoArrays{
    if (!_photoArrays) {
        _photoArrays=[NSMutableArray new];
    }
    return _photoArrays;
}
-(NSMutableArray *)photoNameArrays{
    if (!_photoNameArrays) {
        _photoNameArrays=[NSMutableArray new];
    }
    return _photoNameArrays;
}
- (SLActionView *)actionView{
    if (!_actionView) {
        _actionView = [[SLActionView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _actionView.delegate = self;
        
    }
    return _actionView;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MobClick beginLogPageView:@"SLQuestionUsViewController"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SLQuestionUsViewController"];
}
- (void)setUpNav{
    UIView *navView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 60, 44);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *nameLab=[[UILabel alloc] initWithFrame:CGRectMake(60, 27, ScreenW-120, 30)];
    nameLab.textAlignment=NSTextAlignmentCenter;
    nameLab.text=@"意见反馈";
    nameLab.font=[UIFont systemFontOfSize:18];
    nameLab.textColor=[UIColor blackColor];
    [navView addSubview:nameLab];
    
    _submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame=CGRectMake(ScreenW-60, 20, 60, 44);
    [_submitBtn setTitle:@"发送" forState:UIControlStateNormal];
    _submitBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [_submitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(sendQuestion:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:_submitBtn];
    
    UIImageView *cutImgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.5, ScreenW, 0.5)];
    cutImgView.backgroundColor=SLSepatorColor;
    [navView addSubview:cutImgView];
    [self.view addSubview:navView];
}

- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configCollectionView];
    [self setUpUI];
    _selectedPhotos = [NSMutableArray array];
    self.view.backgroundColor=SLColor(240, 240, 240);
    self.tableView.backgroundColor = SLColor(240, 240, 240);
    //self.navigationItem.title = @"意见反馈";
    [self setUpNav];
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发送" style:UIBarButtonItemStyleDone target:self action:@selector(sendQuestion:)];
    
}

- (void)setUpUI
{
    SLTextView *questionTextField = [[SLTextView alloc] init];
    questionTextField.backgroundColor=[UIColor whiteColor];
    questionTextField.placeholder = @"请输入您要反馈的内容";
    self.questionTextField = questionTextField;
    questionTextField.alwaysBounceVertical = YES;
    questionTextField.frame = CGRectMake(0, 68, self.view.width, 200);
    [self.view addSubview:questionTextField];
    
//    UIButton *addImageBtn = [[UIButton alloc] init];
    UIButton *addImageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    addImageBtn.backgroundColor=[UIColor whiteColor];
    addImageBtn.frame = CGRectMake(4, 288, _itemWH, _itemWH);
    [addImageBtn setImage:[UIImage imageNamed:@"addImage"] forState:UIControlStateNormal];
    [addImageBtn addTarget:self action:@selector(addPictures) forControlEvents:UIControlEventTouchUpInside];
    self.addImageBtn = addImageBtn;
    [self.view addSubview:addImageBtn];
    //    self.tableView.tableFooterView = addImageBtn;
    
}
- (void)configCollectionView {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    _margin = 4;
    _itemWH  = (self.view.width - 5 * _margin) / 4;
    layout.itemSize = CGSizeMake(_itemWH, _itemWH);
    layout.minimumInteritemSpacing = _margin;
    layout.minimumLineSpacing = _margin;
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(_margin, 288, self.view.width - 2 * _margin, self.view.height - 288) collectionViewLayout:layout];
    _collectionView.backgroundColor =[UIColor clearColor];
    _collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, -2);
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    [self.view addSubview:_collectionView];
    [_collectionView registerClass:[SLImageCell class] forCellWithReuseIdentifier:@"SLImageCell"];
}
- (void)addPictures{
    [self.questionTextField resignFirstResponder];
    self.actionView.titleArr = @[@"取消",@"从相册选择",@"拍照"];
    [self.view addSubview:self.actionView];
}
- (void)actionSheetButtonClickedAtIndex:(NSInteger)index{
    NSUInteger sourceType = 0;
    switch (index) {
            case 2:
            // 相机
            sourceType = UIImagePickerControllerSourceTypeCamera;
            break;
            case 1:
            // 相册
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            break;
            
            case 0:
            [self.actionView dismiss];
            return;
    }
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        
        imagePickerController.delegate = self;
        
        imagePickerController.allowsEditing = YES;
        
        imagePickerController.sourceType = sourceType;
        
        [self presentViewController:imagePickerController animated:YES completion:^{}];
    }else
    {
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:3 :_selectedPhotos.count delegate:self ];
        
//        TZImagePickerController *imagePickerVc =[[TZImagePickerController alloc] initWithMaxImagesCount:3 delegate:self];
       
        [self presentViewController:imagePickerVc animated:YES completion:nil];
        
    }
    [self.actionView dismiss];
}

- (void)sendQuestion:(UIButton *)sender{
    if (!ApplicationDelegate.Basic) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"请先登录再进行操作" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
        alert.tag = 100;
        [alert show];
        _submitBtn.enabled=YES;
        return;
    }
    [self sending];
    //先将未到时间执行前的任务取消。
    //    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(sending:) object:sender];
    //    [self performSelector:@selector(sending:) withObject:sender afterDelay:0.2f];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==100) {
        if (buttonIndex==1) {
            SLFirstViewController *vc = [[SLFirstViewController alloc] init];
            SLNavgationViewController *nav = [[SLNavgationViewController alloc] initWithRootViewController:vc];
            [self presentViewController:nav animated:YES completion:nil];
        }
    }
}
//-(void)sending:(UIButton *)sender{
//    sender.enabled = NO;
- (void)sending{
    _submitBtn.enabled = NO;
    if (self.questionTextField.text.length<1 ||self.questionTextField.text.length > 4095) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入反馈内容长度为1至4095位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        _submitBtn.enabled = YES;
        return;
    }
    if (_selectedPhotos.count>0) {
        self.imagName = @"";
#ifdef DEBUG
        NSString *endpoint = @"http://dev.oss.superloop.com.cn";
#else
        NSString *endpoint = @"http://oss.superloop.com.cn";
#endif
        id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:@"UPhdoMR1eN1hnrdA" secretKey:@"ETuIdMVCT1mtiqs8f5KZ8dZnZLiXfG"];
        OSSClient *client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIWindow *win = [[UIApplication sharedApplication].windows objectAtIndex:0];
            [HUDManager showLoadingHUDView:win withText:@"正在上传中"];
        });
        for (int i=0;i<self.photoArrays.count;i++) {
            OSSPutObjectRequest *put = [OSSPutObjectRequest new];
            //put.bucketName = @"superloop";
#ifdef DEBUG
            put.bucketName = @"superloop-dev";
            
#else
            put.bucketName = @"superloop";
#endif
            
            //            [self saveImage:image withName:lower];
            put.objectKey = [NSString stringWithFormat:@"%@/%@.jpg",ApplicationDelegate.userId,self.photoNameArrays[i]];
            self.imagName = [self.imagName stringByAppendingString:[@"&imgs=" stringByAppendingString:put.objectKey]];
            put.uploadingData = self.photoArrays[i];
            put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
            };
            OSSTask * putTask = [client putObject:put];
            [putTask continueWithBlock:^id(OSSTask *task) {
                if (!task.error) {
                    NSLog(@"upload object success!");
                    _num++;
                    if (_num==_selectedPhotos.count) {
                        _num=0;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [HUDManager hideHUDView];
                        });
                        [self postDatas:_submitBtn];
                    }
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _submitBtn.enabled = YES;
                        [HUDManager showWarningWithText:@"上传失败"];
                    });
                    
                }
                return nil;
            }];
        }
        
    }else{
        [self postDatas:_submitBtn];
    }
    
}

-(void)postDatas:(UIButton *)sender{
    //1.确定url
    // NSURL *url = [NSURL URLWithString:@"http://dev.superloop.com.cn/topics"];
    NSURL *url = [NSURL URLWithString:[URL_ROOT_PATIENT stringByAppendingString:@"/users/feedback"]];
    //2.创建可变请求对象
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    //3.修改请求方法,POST是需要大写的
    request.HTTPMethod = @"POST";
    //设置请求头信息
    [request setValue:[NSString stringWithFormat:@"Basic %@",ApplicationDelegate.Basic] forHTTPHeaderField:@"Authorization"];
    //4.设置请求体
    NSString *textViewUTF8 = (NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(nil,
                                                                                                  (CFStringRef)self.questionTextField.text, nil,
                                                                                                  (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
    
    if (!(self.imagName.length>0)) {
        self.imagName=@"";
    }
    NSString *httpBodyStr = [NSString stringWithFormat:@"device_type=%@&device_model=%@&os_type=%@&os_version=%@&content=%@%@", [UIDevice currentDevice].model,[self getCurrentDeviceModel],[UIDevice currentDevice].systemName,[UIDevice currentDevice].systemVersion, textViewUTF8, self.imagName];
    //    NSString *httpBodyStr = [NSString stringWithFormat:@"device_type=%@&device_model=%@&os_type=%@&os_version=%@&content=%@%@", [UIDevice currentDevice].model,[self getCurrentDeviceModel],[UIDevice currentDevice].systemName,[UIDevice currentDevice].systemVersion, self.questionTextField.text, self.imagName];
    request.HTTPBody =[httpBodyStr dataUsingEncoding:NSUTF8StringEncoding];
    //发送请求之前先判断一下
    //5.发送请求
    NSLog(@"---%@--%@",httpBodyStr,url);
    NSURLSession *session=[NSURLSession sharedSession];
    NSURLSessionTask *task=[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error==nil) {
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            NSInteger responseStatusCode = [httpResponse statusCode];
            if (responseStatusCode==200) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUDManager showWarningWithText:@"反馈成功,非常感谢"];
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    sender.enabled = YES;
                    [HUDManager showWarningWithText:@"反馈失败"];
                });
            }
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                sender.enabled = YES;
                
                [HUDManager showWarningWithText:@"反馈失败"];
            });
            
        }
        
    }];
    [task resume];
    
}
//获得设备型号
-(NSString *)getCurrentDeviceModel
{
    int mib[2];
    size_t len;
    char *machine;
    
    mib[0] = CTL_HW;
    mib[1] = HW_MACHINE;
    sysctl(mib, 2, NULL, &len, NULL, 0);
    machine = malloc(len);
    sysctl(mib, 2, machine, &len, NULL, 0);
    
    NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
    free(machine);
    
    if ([platform isEqualToString:@"iPhone1,1"]) return @"iPhone 2G (A1203)";
    if ([platform isEqualToString:@"iPhone1,2"]) return @"iPhone 3G (A1241/A1324)";
    if ([platform isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS (A1303/A1325)";
    if ([platform isEqualToString:@"iPhone3,1"]) return @"iPhone 4 (A1332)";
    if ([platform isEqualToString:@"iPhone3,2"]) return @"iPhone 4 (A1332)";
    if ([platform isEqualToString:@"iPhone3,3"]) return @"iPhone 4 (A1349)";
    if ([platform isEqualToString:@"iPhone4,1"]) return @"iPhone 4S (A1387/A1431)";
    if ([platform isEqualToString:@"iPhone5,1"]) return @"iPhone 5 (A1428)";
    if ([platform isEqualToString:@"iPhone5,2"]) return @"iPhone 5 (A1429/A1442)";
    if ([platform isEqualToString:@"iPhone5,3"]) return @"iPhone 5c (A1456/A1532)";
    if ([platform isEqualToString:@"iPhone5,4"]) return @"iPhone 5c (A1507/A1516/A1526/A1529)";
    if ([platform isEqualToString:@"iPhone6,1"]) return @"iPhone 5s (A1453/A1533)";
    if ([platform isEqualToString:@"iPhone6,2"]) return @"iPhone 5s (A1457/A1518/A1528/A1530)";
    if ([platform isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus (A1522/A1524)";
    if ([platform isEqualToString:@"iPhone8,1"]) return @"iPhone 6s (A1633/A1688/A1691/A1700)";
    if ([platform isEqualToString:@"iPhone8,2"]) return @"iPhone 6s Plus (A1634/A1687/A1690/A1699)";
    if ([platform isEqualToString:@"iPhone7,2"]) return @"iPhone 6 (A1549/A1586)";
    if ([platform isEqualToString:@"iPhone8,4"]) return @"iPhone SE";
    if ([platform isEqualToString:@"iPhone9,1"]) return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,2"]) return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"i386"])      return @"iPhone Simulator";
    if ([platform isEqualToString:@"x86_64"])    return @"iPhone Simulator";
    return platform;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}
-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSUInteger sourceType = 0;
    switch (buttonIndex) {
            case 0:
            // 相机
            sourceType = UIImagePickerControllerSourceTypeCamera;
            break;
            case 1:
            // 相册
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            break;
            
            case 2:
            
            return;
    }
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        
        imagePickerController.delegate = self;
        
        imagePickerController.allowsEditing = YES;
        
        imagePickerController.sourceType = sourceType;
        
        [self presentViewController:imagePickerController animated:YES completion:^{}];
    }else
    {
//        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:3 :_selectedPhotos.count delegate:self ];
//         TZImagePickerController *imagePickerVc =[[TZImagePickerController alloc] initWithMaxImagesCount:3 delegate:self];
//        [self presentViewController:imagePickerVc animated:YES completion:nil];
        
    }
}
- (void)imagePickerControllerDidCancel:(TZImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{}];
    
}
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets infos:(NSArray<NSDictionary *> *)infos{
    [_selectedPhotos addObjectsFromArray:photos];
    [_collectionView reloadData];
    for (UIImage *img in photos) {
        NSData *data = UIImageJPEGRepresentation(img,0.01);
        [self.photoArrays addObject:data];
        [self.photoNameArrays addObject:[NSString getMD5WithData:data]];
    }
    NSInteger photoCount = _selectedPhotos.count;
    if (photoCount == 3) {
        self.addImageBtn.frame = CGRectMake(0,0,0,0);
    }else
    {
        self.addImageBtn.frame = CGRectMake(_margin + (photoCount % 4) * (_margin + _itemWH) , 288 + (photoCount / 4) * (_margin + _itemWH), _itemWH, _itemWH);
    }
    
    _collectionView.contentSize = CGSizeMake(0, ((_selectedPhotos.count + 2) / 3 ) * (_margin + _itemWH));
}

#pragma mark - image picker delegte
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{}];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSData *data = UIImageJPEGRepresentation(image,0.01);
    [self.photoArrays addObject:data];
    [self.photoNameArrays addObject:[NSString getMD5WithData:data]];
    [_selectedPhotos addObject:image];
    [_collectionView reloadData];
    NSInteger photoCount = _selectedPhotos.count;
    if (photoCount == 3) {
        self.addImageBtn.frame = CGRectMake(0,0,0,0);
    }else
    {
        self.addImageBtn.frame = CGRectMake(_margin + (photoCount % 4) * (_margin + _itemWH) , 288 + (photoCount / 4) * (_margin + _itemWH), _itemWH, _itemWH);
    }
    _collectionView.contentSize = CGSizeMake(0, ((_selectedPhotos.count + 2) / 3 ) * (_margin + _itemWH));
    
    _collectionView.contentSize = CGSizeMake(0, ((_selectedPhotos.count + 2) / 3 ) * (_margin + _itemWH));
}

#pragma mark - 保存图片至沙盒


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}
#pragma mark UICollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _selectedPhotos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SLImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SLImageCell" forIndexPath:indexPath];
    
    cell.indexPath = indexPath;
    
    cell.imageView.image = _selectedPhotos[indexPath.row];
    cell.Delegate = self;
    return cell;
}

- (void)didCancelClick:(NSIndexPath *)indexPath
{
    UIImage *image = _selectedPhotos[indexPath.row];
    [_selectedPhotos removeObject:image];
    [self.photoNameArrays removeObjectAtIndex:indexPath.row];
    [self.photoArrays removeObjectAtIndex:indexPath.row];
    [_collectionView reloadData];
    NSInteger photoCount = _selectedPhotos.count;
    self.addImageBtn.frame = CGRectMake(_margin + (photoCount % 4) * (_margin + _itemWH) , 288 + (photoCount / 4) * (_margin + _itemWH), _itemWH, _itemWH);
}

@end
