//
//  SLSettingTableViewController.h
//  Superloop
//
//  Created by WangJiWei on 16/3/3.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import "SLBaseTableViewController.h"

@interface SLSettingTableViewController : SLBaseTableViewController

@property (nonatomic, copy) void(^changeNav)(NSString *);

@property(nonatomic, strong)NSDictionary *infoDict;

@end
