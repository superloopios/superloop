//
//  TZPhotoPickerController.h
//  TZImagePickerController
//
//  Created by 谭真 on 15/12/24.
//  Copyright © 2015年 谭真. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TZAlbumModel;
@interface TZPhotoPickerController : UIViewController

@property (nonatomic, strong) TZAlbumModel *model;
@property (nonatomic, strong) NSMutableArray *selectedPhotoArr;
@property (nonatomic, assign)NSInteger selectCount; //已经选择的图片数量
@end
