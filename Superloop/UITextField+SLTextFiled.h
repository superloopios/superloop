//
//  UITextField+SLTextFiled.h
//  Superloop
//
//  Created by 朱宏伟 on 16/6/3.
//  Copyright © 2016年 Superloop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (SLTextFiled)
- (NSRange) selectedRange;
- (void) setSelectedRange:(NSRange) range;
@end
